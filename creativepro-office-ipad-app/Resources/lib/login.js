Login = {}

Login.tryLogin = function(userid,password) {
	tryLoginAtServer(userid,password);
}

Login.tryAutoLogin = function() {
	var db = Titanium.Database.open('cpo_ipad');
	var rows = db.execute("SELECT * FROM cpo_login LIMIT 1");
	
	while (rows.isValidRow()) {
		var userid   = rows.fieldByName('Email');
		var password = rows.fieldByName('Password');
		var authKey  = rows.fieldByName('AuthKey');
		var userType = rows.fieldByName('UserType');
		var permissions = rows.fieldByName('Permissions');
		rows.next();
	}		
	db.close();
	
	/*
	 * Try logging in if we have a userid
	 */
	if (userid != '' && userid != null) {
		tryLoginAtServer(userid,password,true);	
	} else {
		Login.showLoginScreen();		
	}		
}

Login.showLoginScreen = function() {
	Ti.include('ui/Login.js');
	LoginView.init();
	AppInit.loginWindow.add(LoginView.view);
	AppInit.loginWindow.open();		
}

function tryLoginAtServer(userid,password,autoLogin) {	
	Ti.App.fireEvent('showIndicator',{title: 'Logging in...'});
	
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Sorry! We could not find your account.');
			Login.showLoginScreen();
		} else {
			Globals.AUTH_KEY  = jdata.AuthKey;
			Globals.USER_TYPE = jdata.UserType;
			Globals.PERMISSIONS = jdata.Permissions;
			jdata.Email = userid;
			jdata.Password = password;
			if (autoLogin != true) {
				saveCredentials(jdata);
			}	   		
			
			/*
			 * NOW GET SOME APP DATA
			 */
    		DataApp.getData();
    	}	
	};
	ajax.open('POST', Globals.SITE_URL+'login/tryLogin/1');
	ajax.send({
		'userid' : userid,
		'password' : password
	});
}

function saveCredentials(jdata) {
	var email    = jdata.Email;
	var password = jdata.Password;
	var authKey  = jdata.AuthKey;
    var userType = jdata.UserType;
    var permissions = jdata.Permissions;
    
    var db = Titanium.Database.open('cpo_ipad');
    db.execute('DELETE FROM cpo_login');
    db.execute('INSERT INTO cpo_login (Email, Password, AuthKey, Permissions, UserType) values (?,?,?,?,?)', email, password, authKey, permissions, userType);
	db.close();
}

Login.findPassword = function(email) {
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function(email) {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Sorry! We could not find your account.');
		} else {
			alert('Your new password has been emailed to you.');
    	}	
	};
	ajax.open('POST', Globals.SITE_URL+'login/findPassword/1');
	ajax.send({
		'email': email
	});
}
