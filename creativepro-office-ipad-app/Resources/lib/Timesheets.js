TimesheetsUtils = {}

TimesheetsUtils.format = {
	elapsedTimeDecimal : function(elapsedTime) {
		var elapsedTimeArray = elapsedTime.split(':');
		var hour = elapsedTimeArray[0];
		var min  = elapsedTimeArray[1];
		var sec  = elapsedTimeArray[2];
	
		if (sec.substr(0,1) == '0') {
	        sec = sec.substr(1,1);
	    }
	    if (min.substr(0,1) == '0') {
	        min = min.substr(1,1);
	    }
	    if (hour.substr(0,1) == '0') {
	        hour = hour.substr(1,1);
	    }
	
	    sec  = parseInt(sec);
		min  = parseInt(min);
		hour = parseInt(hour);
			
		if (sec>29) { min = min+1; }
		var minDecimal = parseFloat(min/60);
		var elapsedTimeDecimal = parseFloat(hour+minDecimal);
		elapsedTimeDecimal = elapsedTimeDecimal.toFixed(2);
		
		return elapsedTimeDecimal;
	},
	elapsedTimeSeconds : function(elapsedTime) {
		var elapsedTimeArray = elapsedTime.split(':');
		var hour = elapsedTimeArray[0];
		var min  = elapsedTimeArray[1];
		var sec  = elapsedTimeArray[2];
	
		if (sec.substr(0,1) == '0') {
	        sec = sec.substr(1,1);
	    }
	    if (min.substr(0,1) == '0') {
	        min = min.substr(1,1);
	    }
	    if (hour.substr(0,1) == '0') {
	        hour = hour.substr(1,1);
	    }

	    sec  = parseInt(sec);
		min  = parseInt(min);
		hour = parseInt(hour);
			
		var elapsedSeconds = parseInt( (hour*3600) + (min*60) + sec );
		Ti.API.info(elapsedSeconds);
		return elapsedSeconds;
	},
	niceTimeFromMilliseconds : function(ms) {
	    var total_seconds = Math.round(ms / 1000);
	    var minutes = Math.floor(total_seconds / 60);
	    var seconds = Math.round(total_seconds - (minutes * 60) - 0.499);
	    var hours   = Math.floor(minutes / 60); 
	    
	    minutes = Math.round(minutes - (hours * 60));
    
	    var timeString;
	 	if (hours < 1) {
	 		hours = '00';
	 	} else if (hours < 10) {
	 		hours = '0' + hours;
	 	}
	 	if (minutes < 1) {
	 		minutes = '00';
	 	} else if (minutes < 10) {
	 		minutes = '0' + minutes;
	 	}
	 	if (seconds < 10) {
	 		seconds = '0' + seconds;
	 	} else if (seconds > 59) {
	 		seconds = '00';
	 	}
	    return hours + ':' + minutes + ':' + seconds;
	}
}

TimesheetsUtils.data = {
	saveTimesheetEntry : function(data) {
		var appDB = Ti.Database.open('cpo_ipad');
	
		var timesheetID  = data.TimesheetID;
		var projectID    = data.ProjectID;
		var projectTitle = data.ProjectTitle;
		var taskID       = data.TaskID;
		var taskTitle    = data.TaskTitle;
		var time         = data.Time;
		var billable     = data.Billable;
		var comments     = data.Comments;
		var elapsedTime  = data.ElapsedTime;
		var action       = data.Action;
		var dateTime     = data.DateTime;
	
		if (timesheetID > 0 && action == 'stop') {
			/*
			 * Update an existing entry
			 */
			var sql  = "UPDATE cpo_timesheet SET ProjectID = '"+projectID+"', ProjectTitle = '"+projectTitle+"', ";
			    sql += "TaskID = '"+taskID+"', TaskTitle = '"+taskTitle+"', Stop = '"+time+"', ElapsedTime = '"+elapsedTime+"', ";
			    sql += "DateClockEnd = '"+dateTime+"', Billable = '"+billable+"', Comments = '"+comments+"' ";
			    sql += "WHERE ID = '"+timesheetID+"';";
			appDB.execute(sql);    
		} else if (timesheetID == null && action == 'start') {
			var sql = "INSERT INTO cpo_timesheet (ProjectID, ProjectTitle, TaskID, TaskTitle, Start, ElapsedTime, DateClockStart, DateClockEnd, Billable, Comments) values (?,?,?,?,?,?,?,?,?,?)";
			appDB.execute(sql, projectID, projectTitle, taskID, taskTitle, time, elapsedTime, dateTime, dateTime, billable, comments);
			timesheetID = appDB.lastInsertRowId;
		}	
		appDB.close();
		
		return timesheetID;
	},
	saveTimesheetEntryToServer : function(data) {
		AI._show({message: 'Saving...'});
		var timesheetID  = data.TimesheetID;
		var projectID    = data.ProjectID;
		var projectTitle = data.ProjectTitle;
		var taskID       = data.TaskID;
		var taskTitle    = data.TaskTitle;
		var time         = data.Time;
		var billable     = data.Billable;
		var comments     = data.Comments;
		var elapsedTime  = data.ElapsedTime;
		var action       = data.Action;
		var dateTime     = data.DateTime;
	
		var timesheetAJAX = Titanium.Network.createHTTPClient();
		timesheetAJAX.onload = function() {
			var data = this.responseText;
			var jdata = JSON.parse(data);
			if (jdata.Error == '1') {
				alert('Uh oh! Data retrieval error.');
			} else {
			}	
			AI._hide();
		}	
		timesheetAJAX.open('POST',Globals.SITE_URL+'timesheets/TimesheetView/saveTimesheetRecordFromJobTimer');
		timesheetAJAX.send({
			'authKey':     Globals.AUTH_KEY,
			'action':      'saveFromDesktop',
	        'projectID':   projectID,
	        'taskID':      taskID,
	        'taskTitle':   taskTitle,
	        'comments':    comments,
	        'billable':    billable,
	        'elapsedTime': elapsedTime,
	        'dateTime':    dateTime
		});
	}
}
