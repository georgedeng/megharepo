var DataProjects = {}

DataProjects.getProjects = function() {
	Ti.App.fireEvent('showIndicator',{title: 'Getting projects...'});
	
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Projects.dataStore = jdata;
			Projects.loaded = true;
			Ti.App.fireEvent('projectsLoaded');
			Ti.App.fireEvent('hideIndicator');
		}	
	}
	AJAX.open('POST',Globals.SITE_URL+'projects/ProjectView/getProjectList/0/user/0/json/0/0/0');
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});	
}

DataProjects.getProject = function(projectID) {
	Ti.App.fireEvent('showIndicator',{title: 'Getting project...'});
	
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Ti.App.fireEvent('hideIndicator');
			Ti.App.fireEvent('projectLoaded', {
				projectObj: jdata	
			});
		}	
	}
	AJAX.open('POST',Globals.SITE_URL+'projects/ProjectDetail/getProjectDetail/'+projectID+'/json/0');
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});	
}
