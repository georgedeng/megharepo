var DataMessages = {}

DataMessages.getMessage = function() {
	Ti.App.fireEvent('showIndicator',{title: 'Getting message...'});
	
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Projects.dataStore = jdata;
			Projects.loaded = true;
			Ti.App.fireEvent('messageLoaded');
			Ti.App.fireEvent('hideIndicator');
		}	
	}
	AJAX.open('POST',Globals.SITE_URL+'projects/ProjectView/getProjectList/0/user/0/json/0/0/0');
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});	
}

DataMessages.getMessagesForSomething = function(itemID,itemType,eventName) {
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Ti.App.fireEvent(eventName, {
				messageObj: jdata	
			});
		}	
	}
	AJAX.open('POST',Globals.SITE_URL+'messaging/Messages/getMessages/'+itemID+'/'+itemType+'/0/1/0/json/0/0');
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});	
}

DataMessages.saveMessage = function(data,eventName) {
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Ti.App.fireEvent(eventName, {
				messageObj: jdata	
			});
		}	
	}	
	AJAX.open('POST',Globals.SITE_URL+'messaging/Messages/saveMessage');
	AJAX.send({
		'authKey'     : Globals.AUTH_KEY,
		'toIDs'       : data.toIDs,
		'toType'      : data.toType,
		'toLanguage'  : data.toLanguage,
		'message'     : data.message,
		'itemType'    : data.itemType,
		'itemID'      : data.itemID,
		'fromType'    : data.fromType
	});	 
}
