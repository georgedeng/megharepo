var DataSearch = {}

DataSearch.getSearchBarResults = function(searchTerm) {
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = '{"SearchResults":'+this.responseText+'}';
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			var searchResults = jdata.SearchResults;
			if (searchResults.length>0) {
				Ti.App.fireEvent('searchResultsFound');
				SearchResults.dataStore = searchResults;
			}
		}	
	}
	AJAX.open('POST',Globals.SITE_URL+'search/SiteSearch/siteSearchAutocomplete/0');
	AJAX.send({
		'term' : searchTerm,
		'authKey': Globals.AUTH_KEY
	});	 
}
