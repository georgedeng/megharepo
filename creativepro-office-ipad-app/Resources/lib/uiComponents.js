UI = {};

UI.tableRowBtnBarSlide = function(styleObj) {
	var toggleButtonTop = Math.floor( (styleObj.height - styleObj.icon.height) / 2);
	
	var btnToggle = Ti.UI.createButton({
		top: toggleButtonTop,
		right: Math.floor( (styleObj.width - styleObj.icon.width) / 2),
		width: styleObj.icon.width,
		height: styleObj.icon.height,
		id: styleObj.btnId,
		backgroundImage: styleObj.icon.image
	});
	
	var lblBtnBar = Ti.UI.createLabel({
		height: styleObj.height,
		backgroundColor: styleObj.backgroundColor,
		width: styleObj.width,
		left: 0,
		id: styleObj.barId,
		toggleButton: btnToggle
	});
	
	lblBtnBar.add(btnToggle);
	
	if (styleObj.badge && styleObj.badge > 0) {
		btnBadge = UI.alertBadge(styleObj.badge,styleObj.btnId,'mdBlue');
		btnBadge.right = Math.floor( (styleObj.width - 20) / 2),
		btnBadge.top = toggleButtonTop - 18;
		lblBtnBar.add(btnBadge);
	}
	
	return lblBtnBar;
}

UI.tableRowBtnBarVertical = function(styleObj) {
	var btnBar = Ti.UI.createView({
		width: styleObj.width,
		height: styleObj.height,
		backgroundColor: styleObj.backgroundColor,
		
	});
	
	var fieldBorder = Ti.UI.createLabel({
		width: 1,
		height: styleObj.height,
		backgroundColor: styleObj.borderColor
	});
	
	btnBar.width = styleObj.width;
	btnBar.height = styleObj.height;
	fieldBorder.height = styleObj.height;
	
	if (styleObj.align == 'right') {
		btnBar.right = 0;
		fieldBorder.left = 0;
	} else {
		btnBar.left = 0;
		fieldBorder.right = 0;
	}
	
	/*
	 * Button icons
	 */
	var btnHeight = Math.floor(styleObj.height / styleObj.labels.length);
	
	var btn, btnTop = 0, btnPaddingLeft, btnPaddingTop, btnBadge;
	for(a=0;a<=styleObj.labels.length-1;a++) {
		btnPaddingLeft = Math.floor( (styleObj.width - styleObj.labels[a].width) / 2 );
		btnPaddingTop  = Math.floor( (btnHeight - styleObj.labels[a].height) / 2 );
		btnTopFinal = parseInt(btnTop+btnPaddingTop);
		
		btn = Ti.UI.createButton({
			left: btnPaddingLeft,
			backgroundImage: styleObj.labels[a].image,
			height: styleObj.labels[a].height,
			width: styleObj.labels[a].width,
			top: btnTopFinal,
			id: styleObj.labels[a].id
		});
		btnTop = parseInt(btnTop+btnHeight);
		
		if (styleObj.labels[a].badge) {
			btnBadge = Ti.UI.createLabel({
				text: '4',
			    textAlign: "center",
			    height: 20,
			    width: 20,
			    font: {
			        fontWeight: "light",
			        fontSize: 12
			    },
			    backgroundColor: "red",
			    borderColor: "white",
			    color: "white",
			    borderRadius: 10,
			    borderWidth: 2,
			    left: -10
			});
		
			btnBar.add(btnBadge);
		}
		
		btnBar.add(btn);
	}
	
	btnBar.add(fieldBorder);
	return btnBar;
}

UI.tableRowBtnBarHorizontal = function(styleObj) {
	var btnBarContainer = Ti.UI.createLabel({
		height: parseInt(styleObj.height+8),
		width: parseInt(styleObj.width+8)
	});
	
	var btnBar = Ti.UI.createLabel({
		backgroundColor: styleObj.backgroundColor,
		borderColor: styleObj.borderColor,
		borderRadius: 5,
		height: styleObj.height,
		width: styleObj.width,
		right: 0
	});
	
	btnBarContainer.add(btnBar);
	
	if (styleObj.align == 'right') {
		btnBarContainer.right = 0;
	} else {
		btnBarContainer.left = 0;
	}
	
	if (styleObj.valign == 'top') {
		btnBarContainer.top = 0;
		btnBar.top = -3;
	} else {
		btnBarContainer.bottom = 0;
		btnBar.bottom = -3;
	}
	
	/*
	 * Button icons
	 */
	var btnHeight = Math.floor(styleObj.height / styleObj.labels.length);
	var btnWidth = Math.floor(styleObj.width / styleObj.labels.length);
	
	var btn, btnLeft = 0, btnPaddingLeft, btnPaddingTop, btnBadge;
	for(a=0;a<=styleObj.labels.length-1;a++) {
		btnPaddingLeft = Math.floor( (btnWidth - styleObj.labels[a].width) / 2 );
		btnPaddingTop  = Math.floor( (styleObj.height - styleObj.labels[a].height) / 2 );
		btnLeftFinal = parseInt(btnLeft+btnPaddingLeft);
		
		btn = Ti.UI.createButton({
			left: btnLeftFinal,
			backgroundImage: styleObj.labels[a].image,
			height: styleObj.labels[a].height,
			width: styleObj.labels[a].width,
			top: btnPaddingTop,
			id: styleObj.labels[a].id
		});
		btnLeft = parseInt(btnLeft+btnWidth);
		
		if (styleObj.labels[a].badge && styleObj.labels[a].badge > 0) {
			btnBadge = UI.alertBadge(styleObj.labels[a].badge,styleObj.labels[a].id);
			btnBadge.left = parseInt(btnLeftFinal - 4);
			btnBadge.top = 5;
			btnBarContainer.add(btnBadge);
		}
		
		btnBar.add(btn);
	}
	
	return btnBarContainer;
}	

UI.alertBadge = function(badgeText,id,color) {
	if (color == '') {
		color = 'red';
	}
	var badge = Ti.UI.createLabel({
		text: badgeText,
	    textAlign: "center",
	    height: 21,
	    width: 21,
	    font: Styles.type('badge'),
	    borderColor: "white",
	    color: "white",
	    borderRadius: 11,
	    borderWidth: 2,
	    backgroundGradient: Styles.gradient(color),
	    id: id
	});
	
	return badge;
}

UI.colorBar = function(barObj) {
	switch (barObj.color) {
		case 'blue':
			colorHex = '#ebf4fd';
			break;	
	}
	
	if (!barObj.width) {
		barObj.width = Ti.UI.FILL
	}
	
	var lblBar = Ti.UI.createLabel({
		width: barObj.width,
		height: 30,
		left: barObj.left,
		backgroundColor: colorHex,
		borderRadius: 8,
		top: barObj.top
	});
	
	var lblText = Ti.UI.createLabel({
		top: 3,
		text: barObj.text,
		left: 20
	});
	
	lblBar.add(lblText);
	return lblBar;
}

UI.createFileContainer = function(fileObj,top,left) {
	var lblFile = Ti.UI.createLabel({
		width: 50,
		height: 50,
		backgroundColor: '#eee',
		font: Styles.type('subText'),
		text: fileObj.FileNameActual
	});
	return lblFile;
}
