DataApp = {}
DataApp.ProjectsForSelect = {}

DataApp.getData = function() {
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			DataApp.ProjectsForSelect = jdata;
			AppInit.spinUpApp();
		}	
		Ti.App.fireEvent('hideIndicator');
			
	}	
	AJAX.open('POST',Globals.SITE_URL+'projects/ProjectView/getProjectsForSelect');
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});
}