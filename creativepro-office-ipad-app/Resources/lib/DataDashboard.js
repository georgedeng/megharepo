DataDashboard = {};

DataDashboard.getActivity = function() {
	Ti.App.fireEvent('showIndicator',{title: 'Loading dashboard...'});
	var AJAX = Titanium.Network.createHTTPClient();
		
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Ti.App.fireEvent('activityLoaded', {
				activtyObj: jdata	
			});
		}	
	}
	
	AJAX.open('POST',Globals.SITE_URL+'activity/AppActivity/getAppActivity/json/0');
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});		
}

DataDashboard.getStats = function() {
	var AJAX = Titanium.Network.createHTTPClient();
		
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Ti.App.fireEvent('hideIndicator');
			Ti.App.fireEvent('statsLoaded', {
				statsObj: jdata	
			});
		}	
	}
	var dateStart = '2012-09-01';
	var dateEnd = '2012-12-31';
	AJAX.open('POST',Globals.SITE_URL+'dashboard/DashboardOwners/getOfficeStats/'+dateStart+'/'+dateEnd+'/json/0');
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});		
}
