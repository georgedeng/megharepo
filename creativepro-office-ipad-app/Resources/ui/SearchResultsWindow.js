SearchResults = {}
SearchResults.dataStore = {}
SearchResults.ui = {}

SearchResults.renderResultsTable = function() {
	SearchResults.ui.tblResults = Ti.UI.createTableView({
		width: Ti.UI.FILL,
		left: 0,
		top: 0,
		backgroundImage: 'images/bgSideMenu.png',
		backgroundRepeat: true,
		separatorColor: '#23292c'
	});
	var rows = [];
	for(a=0;a<=SearchResults.dataStore.length-1;a++) {
		var searchData = SearchResults.dataStore[a];
		var iconFile = Styles.itemTypeIcons(searchData.type);
		
		var viewRow = Ti.UI.createView({
			width: 300,
			height: 50,
			backgroundImage: 'images/bgSideMenu.png'
		});
		var lblRowIcon = Ti.UI.createLabel({
			width: 24,
			height: 24,
			left: 5,
			backgroundImage: 'images/'+iconFile
		});
		var lblRowText = Ti.UI.createLabel({
			text: searchData.label,
			font: { 
		    	fontSize: 15,
		    	fontWeight: 'bold',
		    	fontFamily: 'franklin'
		    },
		    width: 240,
		    left: 35,
		    color: 'white',
		    shadowColor: 'black',
		    shadowOffset:{ x: 1, y: 1 }
		});
		viewRow.add(lblRowIcon,lblRowText);
		
		rows[a] = Ti.UI.createTableViewRow({
			width: 300,
			height: 50,
			itemID: searchData.id,
			type: searchData.type,
			icon: searchData.icon
		});
		
		rows[a].add(viewRow);
	}
	SearchResults.ui.tblResults.setData(rows);
	SearchResults.addEventListeners();
	return SearchResults.ui.tblResults;
}

SearchResults.addEventListeners = function() {
	SearchResults.ui.tblResults.addEventListener('click', function(e) {
		var row = e.row;
		var rowIndex = e.index;
		
		Ti.App.fireEvent('openModal', {
			values: [row.type,'view',row.itemID,null]
		});
	});
}
