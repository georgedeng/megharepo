ProjectsModal = {};
ProjectsModal.ui = {};
ProjectsModal.ui.buttons = {};
ProjectsModal.dataStore = {};
ProjectsModal.action;
ProjectsModal.projectID;

ProjectsModal.init = function() {
	ProjectsModal.win = Ti.UI.createWindow({ 
		barColor: Styles.modalToolbarColor,
		backgroundColor: Styles.appBGColor
	});
	
	if (ProjectsModal.projectID > 0) {
		DataProjects.getProject(ProjectsModal.projectID);
	}
	
	/*
	 * Create out UI views and buttons.
	 */
	ProjectsModal.createButtons();
	ProjectsModal.createUI.createViewView();
	ProjectsModal.createUI.createEditView();
	
	if (ProjectsModal.action == 'edit' || ProjectsModal.action == 'add') {
		ProjectsModal.openEditView();
	} else if (ProjectsModal.action == 'view') {
		if (ProjectsModal.dataStore) {
			ProjectsModal.openViewView();
		} else {
			Ti.App.addEventListener('projectLoaded', function(e) {
				ProjectsModal.dataStore = e.projectObj;
				ProjectsModal.openViewView();
			});
		}	
	}
}	

ProjectsModal.createUI = {
	createViewView : function() {
		ProjectsModal.viewView = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: Ti.UI.FILL,
			top: 0
		});
	
		var lbl = Ti.UI.createLabel({
			text: 'view mode ghfjkg ghjkfld hdfjklghfj kglfhjgkl fghjkfl hj hjfklgj gkl fjsklhgj fklghfdj kglhjhgfj klghfdsj khgj fklhgdfjs klghjklgfgjs fkdldj klhgfj klgfhsj gklfdgjkfljdklgfdhgjks fdlhjk',
			top: 0,
			width: Ti.UI.FILL,
			height: Ti.UI.SIZE
		});
		
		ProjectsModal.viewView.add(lbl);
		ProjectsModal.win.add(ProjectsModal.viewView);
	},
	createEditView : function() {
		ProjectsModal.viewEdit = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: Ti.UI.FILL,
			top: 0
		});
		
		var lbl = Ti.UI.createLabel({
			text: 'edit mode',
			top: 0,
			width: Ti.UI.FILL,
			height: 30
		});
		
		ProjectsModal.viewEdit.add(lbl);
		ProjectsModal.win.add(ProjectsModal.viewEdit);
	}
}

ProjectsModal.openViewView = function() {
	ProjectsModal.win.title = ProjectsModal.dataStore.Title;
	ProjectsModal.win.setLeftNavButton(ProjectsModal.ui.buttons.btnEdit);
	ProjectsModal.win.setRightNavButton(ProjectsModal.ui.buttons.btnDone);
	ProjectsModal.viewView.show();
	ProjectsModal.viewEdit.hide();
	
	//ProjectsModal.win.animate({view: ProjectsModal.viewView, transition: Ti.UI.ANIMATION_CURVE_EASE_IN});
}

ProjectsModal.openEditView = function() {
	var winTitle = 'Edit '+ProjectsModal.dataStore.Title;
	if (ProjectsModal.action == 'add') {
		winTitle = 'Add new project';
	}
	
	ProjectsModal.win.title = winTitle;
	ProjectsModal.win.setLeftNavButton();
	ProjectsModal.win.setRightNavButton(ProjectsModal.ui.buttons.btnDone);
	
	ProjectsModal.viewView.hide();
	ProjectsModal.viewEdit.show();
	
	//ProjectsModal.win.animate({view: ProjectsModal.viewEdit, transition: Ti.UI.ANIMATION_CURVE_EASE_IN});
}	

ProjectsModal.createButtons = function() {
	ProjectsModal.ui.buttons.btnDone = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	ProjectsModal.ui.buttons.btnDone.addEventListener('click', function() {
		if (ProjectsModal.action == 'editFromView') {
			ProjectsModal.action = 'view';
			ProjectsModal.win.title = ProjectsModal.dataStore.Title;
			ProjectsModal.win.setLeftNavButton(ProjectsModal.ui.buttons.btnEdit);
			
			ProjectsModal.viewView.show();
			ProjectsModal.viewEdit.hide();
			
			//ProjectsModal.win.animate({view: ProjectsModal.viewView, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});		
		} else {
			/*
			 * Just close the window
			 */
			ProjectsModal.win.close();
		}
	});
	
	ProjectsModal.ui.buttons.btnEdit = Ti.UI.createButton({
		title: 'Edit',
		image: 'images/icon-bar-top-edit.png',
		style: Titanium.UI.iPhone.SystemButtonStyle.BORDERED
	});
	ProjectsModal.ui.buttons.btnEdit.addEventListener('click', function() {
		ProjectsModal.action = 'editFromView';
		ProjectsModal.openEditView();
	});
}
