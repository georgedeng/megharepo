Dashboard = {};
Dashboard.ui = {};
Dashboard.dataStore = {};

Dashboard.win = Ti.UI.createWindow({
	width: Ti.UI.FILL,
	height: '100%',
	backgroundColor: '#f7f4f4'
});

Dashboard.init = function() {	
	/*
	 * Activity stream column
	 */
	Dashboard.ui.viewActivityStream = Ti.UI.createView({
		top: 0,
		left: 0,
		width: '60%',
		height: '100%',
		backgroundColor: '#fff',
	});
	
	var lblActivityStream = Ti.UI.createLabel({
		top: 0,
		width: Ti.UI.FILL,
		height: 50,
		textAlign: 'center',
		text: 'Activity Stream',
		color: '#fff',
		font: Styles.type('heading'),
		backgroundColor: '#3d474a',
		shadowColor: '#000',
	    shadowOffset:{ x:-1, y:-1}
	});
	
	Dashboard.ui.tblActivityStream = Ti.UI.createTableView({
		top: 50
	});
	
	Dashboard.ui.viewActivityStream.add(lblActivityStream,Dashboard.ui.tblActivityStream);
	
	/*
	 * Default task window, will display task summaries
	 */	
	Dashboard.ui.winTasks = Ti.UI.createWindow({
		left: '60%',
		top: '50%',
		width: Ti.UI.FILL,
		height: '50%'
	});
	
	/*
	 * Stats panels
	 */
	Dashboard.ui.viewStats = Ti.UI.createView({
		top: 0,	left: '60%', width: Ti.UI.FILL,	height: 480,
		backgroundColor: '#000'
	});
	
	var wViewDashboardChart = Ti.UI.createWebView({  
	    width: Ti.UI.FILL,
	    height: 430,
	    left: 0,  
	    top: 0,  
	    url: 'ui/chartDashboard.html',
	    touchEnabled: true
	}); 
	
	var viewDashboardStats = Ti.UI.createView({
		width: Ti.UI.FILL,
	    height: 370,
	    left: 0,  
	    top: 0
	});
	
	var lblTemp = Ti.UI.createLabel({
		width: 200,
		height: 50,
		backgroundColor: '#eee',
		text: 'Hi there'
	});
	
	viewDashboardStats.add(lblTemp);
	
	var scrollStats = Ti.UI.createScrollableView({
		top: 50, width: Ti.UI.FILL,	height: 430,
		views: [wViewDashboardChart, viewDashboardStats],		
		backgroundColor: '#f7f4f4',
		showPagingControl: true,
		pagingControlColor: '#bddbf9',
		pagingControlHeight: 30
	}); 
	
	var lblStatsHeader = Ti.UI.createLabel({
		top: 0,
		height: 50,
		width: Ti.UI.FILL,
		backgroundColor: '#3d474a'
	});
	
	var lblStatsTitle = Ti.UI.createLabel({
		top: 0,
		width: 300,
		height: 50,
		left: 50,
		text: 'Stats',
		color: '#fff',
		font: Styles.type('heading'),
		backgroundColor: '#3d474a',
		shadowColor: '#000',
	    shadowOffset:{ x:-1, y:-1}
	});
	
	var lblStatsIcon = Ti.UI.createLabel({
		width: 32,
		height: 32,
		left: 10,
		backgroundImage: 'images/reports.png'
	});
	
	lblStatsHeader.add(lblStatsTitle,lblStatsIcon);
	Dashboard.ui.viewStats.add(lblStatsHeader,scrollStats);
		
	/*
	 * Add everything to our main window.
	 */	
	Dashboard.win.add(Dashboard.ui.viewActivityStream,Dashboard.ui.winTasks,Dashboard.ui.viewStats);
	
	/*
	 * Set up out event handlers.
	 */
	Dashboard.addEventListeners();
}

Dashboard.createActivityUI = function() {
	var data = [];
	var activity = Dashboard.dataStore.activity;
	for(var a=0;a<=activity.length-1;a++) {
		var avatarImage = activity[a].AvatarURL;
		if (activity[a].AvatarURL == '') {
			avatarImage = 'images/stockAvatar46.png';
		}
		
		var activityText = activity[a].NameFirst+' '+activity[a].NameLast;
		
		var word = 'a';
		if (activity[a].ItemType == 'invoice' || activity[a].ItemType == 'expense' || activity[a].ItemType == 'event') {
			word = 'an';
		}
		
		switch (activity[a].Activity) {
			case 'create':
				activityText += ' created a new';
				break;
			case 'delete':
				activityText += ' deleted '+word;
				break;
			case 'update':
				activityText += ' edited '+word;
				break;
			case 'invoice payment':
				activityText += ' entered a payment for '+word;
				break;	
			case 'update status':
				activityText += ' updated the status for '+word;
				break;			
		}
		activityText += ' '+activity[a].ItemType+'.';
		
		var lblAvatar = Ti.UI.createImageView({
			left: 10,
			top: 12,
			width: 46,
			height: 46,
			image: avatarImage,
			borderRadius: 4		
		});
		
		var lblActivityUser = Ti.UI.createLabel({
			left: 70,
			top: 6,
			width: 310,
			color: '#999',
			font: Styles.type('subText'),
			text: activityText	
		});
		
		var lblActivityTitle = Ti.UI.createLabel({
			touchEnabled: false,
			left: 70,
			top: 22,
			width: 310,
			height: 20,
			font: Styles.type('h3'),
			text: Utils.txtToField(activity[a].ItemTitle)
		});
		
		var lblItemType;
		switch(activity[a].ItemType) {
			case 'project':
				lblItemType = Styles.badge('Project','blue');
				break;
			case 'invoice':
				lblItemType = Styles.badge('Invoice','green');
				break;
			case 'expense':
				lblItemType = Styles.badge('Expense','dk-green');
				break;
			case 'event':
				lblItemType = Styles.badge('Event','purple');
				break;
			case 'comment':
				lblItemType = Styles.badge('Comment','orange');
				break;			
			case 'task':
				lblItemType = Styles.badge('Task','red');
				break;	
			case 'client':
				lblItemType = Styles.badge('Client','gray');
				break;		
		}	
		lblItemType.left = 70;
		lblItemType.top = 48;
		
		var dateActivity = moment(activity[a].DateActivity, 'YYYY-MM-DD h:mm:ss');
		var monthDayActivity = dateActivity.format('MMM D');
		var timeActivity = dateActivity.format('h:mm a');
		
		var lblTime = Ti.UI.createLabel({
			top: 14,
			font: Styles.type('subText'),
			color: '#999',
			text: timeActivity,
			left: 390
		}); 
		
		var lblDate = Ti.UI.createLabel({
			top: 32,
			width: 100,
			font: Styles.type('h1'),
			color: '#7ebcf7',
			text: monthDayActivity,
			left: 390
		});		
		
		var rowView = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: '100%',
			touchEnabled: true
		});
		
		rowView.add(
			lblAvatar,
			lblItemType,
			lblActivityUser,
			lblActivityTitle,
			lblDate,
			lblTime
		);
		
		data[a] = Ti.UI.createTableViewRow({ 
			touchEnabled: true,
			backgroundColor: 'white',
			hasChild: false,
			height: 70,
			className: 'activity',
			itemID: activity[a].ItemID,
			itemType: activity[a].ItemType,
			activity: activity[a].Activity,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		
		data[a].add(
			rowView
		);
	}	
	Dashboard.ui.tblActivityStream.data = data;
}	

Dashboard.createTasksUI = function() {
	/*
	 * Task overview window
	 */
	Dashboard.ui.viewTasksOverview = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		backgroundColor: '#f7f4f4',
		top: 0,
		left: 0
	});
	
	Dashboard.ui.lblTasksHeader = Ti.UI.createLabel({
		top: 0,
		height: 50,
		width: Ti.UI.FILL,
		backgroundColor: '#3d474a'
	});
	
	Dashboard.ui.lblTasksTitle = Ti.UI.createLabel({
		top: 0,
		width: 300,
		height: 50,
		left: 50,
		text: 'Your tasks',
		color: '#fff',
		font: Styles.type('heading'),
		backgroundColor: '#3d474a',
		shadowColor: '#000',
	    shadowOffset:{ x:-1, y:-1}
	});
	
	Dashboard.ui.lblTaskIcon = Ti.UI.createLabel({
		width: 32,
		height: 32,
		left: 10,
		backgroundImage: 'images/tasks.png'
	});
	Dashboard.ui.lblTasksHeader.add(Dashboard.ui.lblTasksTitle,Dashboard.ui.lblTaskIcon);
	Dashboard.ui.viewTasksOverview.add(Dashboard.ui.lblTasksHeader);
	
	/*
	 * Task list window
	 */
	Dashboard.ui.viewTaskList = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		left: 0
	});
	
	Dashboard.ui.lblTasksHeaderList = Ti.UI.createLabel({
		top: 0,
		height: 50,
		width: Ti.UI.FILL,
		backgroundColor: '#3d474a'
	});
	
	Dashboard.ui.lblTasksTitleList = Ti.UI.createLabel({
		top: 0,
		width: 300,
		height: 50,
		left: 50,
		text: 'Your tasks',
		color: '#fff',
		font: Styles.type('heading'),
		backgroundColor: '#3d474a',
		shadowColor: '#000',
	    shadowOffset:{ x:-1, y:-1}
	});
	
	Dashboard.ui.lblTaskIconList = Ti.UI.createLabel({
		width: 32,
		height: 32,
		left: 10,
		backgroundImage: 'images/tasks.png'
	});
	
	Dashboard.ui.tblTaskList = Ti.UI.createTableView({
		separatorColor:'#fff',
		top: 50,
		left: 0
	});
	Dashboard.ui.lblTasksHeaderList.add(Dashboard.ui.lblTasksTitleList,Dashboard.ui.lblTaskIconList);
	Dashboard.ui.viewTaskList.add(Dashboard.ui.lblTasksHeaderList,Dashboard.ui.tblTaskList);
	
	Dashboard.ui.lblTasksHeaderList.addEventListener('click', function(e) {
		Dashboard.ui.winTasks.animate({view: Dashboard.ui.viewTasksOverview, transition: Ti.UI.ANIMATION_CURVE_EASE_IN});
	});
	
	/*
	 * Render task overview labels.
	 */
	var taskLabels = Tasks.utils.parseDigestView(Dashboard.dataStore.taskDigest);
	var lblTitle,lblCount,top = 70;
	for(a in taskLabels) {
		var title = taskLabels[a].Title+' ('+taskLabels[a].Total+')';
		lblIcon = Ti.UI.createLabel({
			width: 16,
			height: 16,
			left: 18,
			top: top+3,
			backgroundImage: 'images/'+taskLabels[a].IconSmall
		});
		
		lblTitle = Ti.UI.createLabel({
			color: '#000',
			left: '60%',
			text: title,
			id: a,
			width: Ti.UI.FILL,
			height: 20,
			left: 50,
			top: top,
			font: Styles.type('h1'),
			color: '#62aef7',
			shadowColor: '#fff',
			shadowOffset:{ x:0, y:-1},
			headerIcon: 'images/'+taskLabels[a].IconBig
		});
		top = top + 35;
		Dashboard.ui.viewTasksOverview.add(lblIcon,lblTitle);
		
		lblTitle.addEventListener('click', function(e) {			
			Dashboard.ui.winTasks.animate({view: Dashboard.ui.viewTaskList, transition: Ti.UI.ANIMATION_CURVE_EASE_IN});
			Dashboard.ui.lblTasksTitleList.text = e.source.text;
			Dashboard.ui.lblTaskIconList.backgroundImage = e.source.headerIcon;
			
			var rowData = [], tasks = Dashboard.dataStore.taskDigest[e.source.id], lblTitle;
			for (var b in tasks) {
				task = tasks[b];
				
				lblTitle = Ti.UI.createLabel({
					left: 15,
					top: 21,
					font: Styles.type('h3'),
					text: task.Title,
					width: Ti.UI.FILL,
					height: 20
				});
				
				lblDueDate = Ti.UI.createLabel({
					left: 15,
					top: 4,
					text: task.DateEndRender+' '+task.StatusText,
					font: Styles.type('subText'),
					color: '#62aef7',
					shadowColor: '#fff',
					shadowOffset:{ x:0, y:-1}
				});
				
				lblProject = Ti.UI.createLabel({
					left: 15,
					top: 41,
					font: Styles.type('subText'),
					color: '#999',
					text: task.ProjectTitle,
					width: Ti.UI.FILL
				});
				
				rowData[b] = Ti.UI.createTableViewRow({ 
					backgroundColor: '#f7f4f4',
					hasChild: false,
					height: 62,
					taskID: task.TaskID,
					selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
				});
				
				rowData[b].add(lblTitle,lblProject,lblDueDate);
			}	
			Dashboard.ui.tblTaskList.setData(rowData);
		});
	}
	
	Dashboard.ui.winTasks.animate({view: Dashboard.ui.viewTasksOverview, transition: Ti.UI.ANIMATION_CURVE_EASE_IN});
}	

Dashboard.addEventListeners = function() {
	Ti.App.addEventListener('showDashboardTasks', function(e) {
		Dashboard.ui.winTasks.animate({view: Dashboard.ui.viewTaskList, transition: Ti.UI.ANIMATION_CURVE_EASE_IN});
	});
	
	Ti.App.addEventListener('activityLoaded', function(e) {
		Dashboard.dataStore.activity = e.activtyObj;
		Dashboard.createActivityUI();
	});
	
	Ti.App.addEventListener('statsLoaded', function(e) {
		Ti.App.fireEvent('renderChartDashboard', {
			chartData: e.statsObj
		});
	});	
	
	/*
	 * Events coming from the graph webview.
	 */
	Ti.App.addEventListener('showDashboardTasks', function(e) {
		MenuView.switchWindows('tasks');
	});	
	
	Ti.App.addEventListener('showDashboardInvoices', function(e) {
		MenuView.switchWindows('invoices');
	});	
	
	Ti.App.addEventListener('showDashboardHours', function(e) {
		MenuView.switchWindows('timesheets');
	});	
	
	Dashboard.ui.tblActivityStream.addEventListener('click', function(e) {
		var row = e.row;
		var rowIndex = e.index;
		var itemID = row.itemID;
		if (row.activity != 'delete') {
			switch (row.itemType) {
				case 'project':
					Ti.App.fireEvent('openModal', {
						values: ['project','view',itemID,null]
					});
					break;
				case 'task':
					Ti.App.fireEvent('openModal', {
						values: ['task','view',itemID,null]
					});
					break;	
				case 'invoice':
					Ti.App.fireEvent('openModal', {
						values: ['invoice','view',itemID,null]
					});
					break;	
				case 'client':
					Ti.App.fireEvent('openModal', {
						values: ['client','view',itemID,null]
					});
					break;	
				case 'expense':
					Ti.App.fireEvent('openModal', {
						values: ['expense','view',itemID,null]
					});
					break;	
				case 'message':
					Ti.App.fireEvent('openModal', {
						values: ['message','view',itemID,null]
					});
					break;		
			}
		}	
	});		
}	

Dashboard.data = {
	populateActivityTable : function() {
		DataDashboard.getActivity();
	},
	populateTasksTable : function() {
		DataTasks.getTaskDigestView();
	},
	populateStats : function() {
		DataDashboard.getStats();
	},
	getDashboardData : function() {
		Dashboard.data.populateActivityTable();
		Dashboard.data.populateTasksTable();
		Dashboard.data.populateStats();		
	}
}	