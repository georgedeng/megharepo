Ledger = {};
Ledger.win = Ti.UI.createWindow({
	barColor: '#94b735',
	title: 'Ledger',
	backgroundColor: '#eeefe1',
	zIndex: 10
});

Ledger.init = function() {
	var lblText = Ti.UI.createLabel({
		text: 'Hi there - I am the ledger window.',
		color: '#000',
		top: 0,
		left: 0
	});
	
	Ledger.win.add(lblText);
}