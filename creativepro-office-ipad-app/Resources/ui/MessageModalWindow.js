MessageModal = {};
MessageModal.ui = {};
MessageModal.ui.buttons = {};
MessageModal.dataStore = {};
MessageModal.messageID;

MessageModal.init = function() {
	MessageModal.win = Ti.UI.createWindow({ 
		barColor: Styles.modalToolbarColor,
		backgroundColor: Styles.appBGColor
	});
	
	if (MessageModal.messageID > 0) {
		//DataTasks.getTask(TasksModal.taskID);
	}
	MessageModal.createButtons();
	
	if (MessageModal.dataStore) {
		MessageModal.openViewView();
	} else {
		Ti.App.addEventListener('messageLoaded', function(e) {
			MessageModal.dataStore = e.messageObj;
			MessageModal.openViewView();
		});
	}	
}	

MessageModal.openViewView = function() {
	MessageModal.viewView = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		top: 0
	});
	
	var lbl = Ti.UI.createLabel({
		text: 'view mode',
		top: 0,
		width: Ti.UI.FILL,
		height: 30
	});
	
	MessageModal.viewView.add(lbl);
	MessageModal.win.title = 'Message';
	MessageModal.win.setRightNavButton(MessageModal.ui.buttons.btnDone);
	MessageModal.win.animate({view: MessageModal.viewView, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
}

MessageModal.createButtons = function() {
	MessageModal.ui.buttons.btnDone = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	MessageModal.ui.buttons.btnDone.addEventListener('click', function() {
		TasksModal.win.close();
	});
}
