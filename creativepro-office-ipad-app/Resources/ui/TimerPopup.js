Timer = {};
Timer.ui = {};

Timer.init = function() {
	var now = moment();	
	
	Timer.timesheetID = null;
	Timer.timerText = '00:00:00';
	Timer.clockInterval;
	Timer.timePickerValue = '';
	Timer.datePickerValue = now.format('YYYY-MM-DD');
	Timer.timerControl = 'start';
	Timer.timerAction  = 'start';
	Timer.projectID = '';
	Timer.taskID = '';
	
	Timer.popup = Ti.UI.iPad.createPopover({ 
		height: 400, 
		width: 310,
		title: now.format('MMM D, YYYY'),
		arrowDirection: Ti.UI.iPad.POPOVER_ARROW_DIRECTION_LEFT
	});
	
	Timer.createUI();
	Timer.attachEventHandlers();
}

Timer.createUI = function() {
	var pickerData = [];
	pickerData.push(Ti.UI.createPickerRow({ title: '' }));
	
	var timerView = Ti.UI.createScrollView({ 
		contentWidth: 310, 
		contentHeight: 700, 
		top: 0, 
		showVerticalScrollIndicator: false, 
		showHorizontalScrollIndicator: false,
		backgroundColor: Styles.appBGColor
	});
	
	/*
	 * TOP TOOLBAR
	 */
	Timer.ui.btnClear = Ti.UI.createButton({
		image: 'images/icon-bar-top-refresh.png'
	});
	
	Timer.ui.btnCalendar = Ti.UI.createButton({
		image: 'images/icon-bar-top-calendar.png'
	});
	
	var btnSpace = Ti.UI.createButton({
		systemButton: Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
	});
	
	var toolBar = Ti.UI.iOS.createToolbar({
		left: 0,
		top: 0,
		height: 25,
		width: 310,
		barColor: Styles.navBarColor,
		items: [Timer.ui.btnClear,btnSpace,Timer.ui.btnCalendar]
	});
	
	/*
	 * TIMER LABEL
	 */
	Timer.ui.lblTimer = Ti.UI.createLabel({
	    text: Timer.timerText,
	    font: { 
	    	fontSize: 70,
	    	fontWeight: 'bold',
	    	fontFamily: 'franklin'
	    },
	    textAlign: 'center',
	    color: '#fff',
	    top: 50,
	    left: 5,
	    width: 300,
	    height: 80,
	    borderRadius: 8,
	    shadowColor: '#000000',
	    shadowOffset:{ x:2, y:2},
	    backgroundGradient: Styles.gradient('dkGray')
	});
	
	/*
	 * PROJECT & TASK COMBO BOXES
	 */
	var tr = Ti.UI.create2DMatrix();
	tr = tr.rotate(90);
	
	Timer.ui.btnDropProject = Ti.UI.createButton({
		style: Ti.UI.iPhone.SystemButton.DISCLOSURE,
		transform: tr
	});
	
	Timer.ui.btnDropTask = Ti.UI.createButton({
		style: Ti.UI.iPhone.SystemButton.DISCLOSURE,
		transform: tr,
		visible: false
	});
	
	Timer.ui.fieldProject = Ti.UI.createTextField({
		hintText: 'Select project',
		editable: false,
		height: 40,
		width: 300,
		top: 136,
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		rightButton: Timer.ui.btnDropProject,
		rightButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS
	});
	
	Timer.ui.fieldTask = Ti.UI.createTextField({
		hintText: 'Select task',
		editable: false,
		height: 40,
		width: 300,
		top: 182,
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		rightButton: Timer.ui.btnDropTask,
		rightButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS,
		keyboardType: Ti.UI.KEYBOARD_DEFAULT,
		returnKeyType: Ti.UI.RETURNKEY_DONE
	});
	
	Timer.ui.txtComments = Ti.UI.createTextArea({
		value: 'Comments',
		color: '#bfbdbd',
		font: { fontSize: 17 },
		width: 300,
		height: 50,
		borderColor: '#ccc',
		borderRadius: 5,
		top: 228,
		keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType: Titanium.UI.RETURNKEY_DONE,  
    	keyboardToolbarHeight: 43
	});
	
	var viewBillable = Ti.UI.createView({
		width: 300,
		height: 35,
		top: 284,
		left: 10
	});
	
	var lblBillable = Ti.UI.createLabel({
		font: { fontSize: 20 },
		color: '#333',
		shadowColor:'#fff',
	    shadowOffset:{ x:1, y:1},
		text: 'Billable',
		left: 90
	});
	
	Timer.ui.chkBillable = Ti.UI.createSwitch({
		value: true,
		left: 0
	});
	
	viewBillable.add(lblBillable,Timer.ui.chkBillable);
	
	/*
	 * START/STOP SAVE BUTTONS
	 */
	Timer.ui.btnTimer = Ti.UI.createButton({
		title: 'Start',
		color: '#567201',
		backgroundImage: 'none',
		borderRadius: 8,
		borderColor: '#9acc01',
		borderWidth: 1,
		font: {
			fontSize: 22,
			fontWeight: 'bold'			
		},
		selectedColor: 'none',
		selectedImage: 'none',
	    backgroundGradient: Styles.gradient('green'),
		height: 50,
		width: 200,
		top: 325,
		left: 5
	});
	
	Timer.ui.btnSave = Ti.UI.createButton({
		title: 'Save',
		color: '#a3a1a1',
		borderColor: '#ccc9c9',
		enabled: false,
		backgroundImage: 'none',
		borderRadius: 8,
		borderWidth: 1,
		font: {
			fontSize: 22,
			fontWeight: 'bold'			
		},
	    backgroundGradient: Styles.gradient('mdGray'),
		height: 50,
		width: 90,
		top: 325,
		left: 215		
	});
	
	/*
	 * CALENDAR PICKER
	 */
	Timer.ui.viewCalPicker = Ti.UI.createView({
		height: 260,
		bottom: -260
	});
		
	Timer.ui.calPicker = Ti.UI.createPicker({
		type:Ti.UI.PICKER_TYPE_DATE
	});	
		
	Timer.ui.btnCancelCal = Ti.UI.createButton({
		title: 'Cancel',
		style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
	});	
		
	Timer.ui.btnDoneCal = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	var toolbarCalPicker = Titanium.UI.iOS.createToolbar({
		items: [Timer.ui.btnCancelCal, btnSpace, Timer.ui.btnDoneCal],
		top: 0,
		height: 43,
		backgroundColor: Styles.toolbarColor,
		backgroundImage: 'none'
	});
	
	Timer.ui.calPicker.top = 43;
	
	/*
	 * PROJECT PICKER
	 */	
	Timer.ui.viewProjectPicker = Ti.UI.createView({
		height: 260,
		bottom: -260
	});
	
	Timer.ui.projectPicker = Ti.UI.createPicker();
	Timer.ui.projectPicker.add(pickerData);
		
	Timer.ui.btnCancelProject = Ti.UI.createButton({
		title: 'Cancel',
		style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
	});
			
	Timer.ui.btnDoneProject = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	var toolbarProjectPicker = Titanium.UI.iOS.createToolbar({
		items: [Timer.ui.btnCancelProject, btnSpace, Timer.ui.btnDoneProject],
		top: 0,
		height: 43,
		backgroundColor: Styles.toolbarColor,
		backgroundImage: 'none'
	});
	
	Timer.ui.projectPicker.top = 44;
	
	/*
	 * TASK PICKER
	 */	
	Timer.ui.viewTaskPicker = Ti.UI.createView({
		height: 260,
		bottom: -260
	});
	
	Timer.ui.taskPicker = Ti.UI.createPicker();
	Timer.ui.taskPicker.add(pickerData);
		
	Timer.ui.btnCancelTask = Ti.UI.createButton({
		title: 'Cancel',
		style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
	});
			
	Timer.ui.btnDoneTask = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	var toolbarTaskPicker = Titanium.UI.iOS.createToolbar({
		items: [Timer.ui.btnCancelTask, btnSpace, Timer.ui.btnDoneTask],
		top: 0,
		height: 43,
		backgroundColor: Styles.toolbarColor,
		backgroundImage: 'none'
	});
	
	Timer.ui.taskPicker.top = 44;
	
	/*
	 * TIMER PICKER
	 */
	Timer.ui.viewTimePicker = Ti.UI.createView({
		height: 260,
		bottom: -260
	});
	
	Timer.ui.timePicker = Ti.UI.createPicker({
		type: Ti.UI.PICKER_TYPE_COUNT_DOWN_TIMER
	});
	
	Timer.ui.btnCancelTime = Ti.UI.createButton({
		title: 'Cancel',
		style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
	});	
		
	Timer.ui.btnDoneTime = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	var toolbarTimePicker = Titanium.UI.iOS.createToolbar({
		items: [Timer.ui.btnCancelTime, btnSpace, Timer.ui.btnDoneTime],
		top: 0,
		height: 43,
		backgroundColor: Styles.toolbarColor,
		backgroundImage: 'none'
	});
	
	Timer.ui.timePicker.top = 43;
	
	/*
	 * ADD EVERYTHING TO OUR POPOVER
	 */
	Timer.ui.viewCalPicker.add(toolbarCalPicker,Timer.ui.calPicker);
	Timer.ui.viewProjectPicker.add(toolbarProjectPicker,Timer.ui.projectPicker);
	Timer.ui.viewTaskPicker.add(toolbarTaskPicker,Timer.ui.taskPicker);
	Timer.ui.viewTimePicker.add(toolbarTimePicker, Timer.ui.timePicker);
	
	timerView.add(
		toolBar,
		Timer.ui.lblTimer,
		Timer.ui.fieldProject,
		Timer.ui.fieldTask,
		Timer.ui.txtComments,
		viewBillable,
		Timer.ui.btnTimer,
		Timer.ui.btnSave
	);
	Timer.popup.add(
		timerView,
		Timer.ui.viewCalPicker,
		Timer.ui.viewProjectPicker,
		Timer.ui.viewTaskPicker,
		Timer.ui.viewTimePicker
	);
}

Timer.attachEventHandlers = function() {	
	Timer.ui.lblTimer.addEventListener('click', function(e) {
		Timer.ui.viewTimePicker.animate(Styles.poPickerSlideUp);
		Timer.ui.txtComments.blur();
		if (Timer.ui.lblTimer.text != '00:00:00') {
			var elapsedMS = TimesheetsUtils.formate.elapsedTimeDecimal(Timer.ui.lblTimer.text)*60*60*1000;
			Timer.ui.timePicker.countDownDuration = elapsedMS;
		}			
	}); 
	
	Timer.ui.btnCalendar.addEventListener('click', function(e){
		var now = moment();
 		var thisYear  = parseInt(now.format('YYYY'));
 		var thisDate  = now.format('DD');
 		var thisMonth = parseInt(now.format('MM')-1); 
 		var lastYear = thisYear - 1;
 		
 		var valueArray = Timer.datePickerValue.split('-');
 		var valueYear  = valueArray[0];
 		var valueMonth = parseInt(valueArray[1]-1);
 		var valueDate  = valueArray[2];
 		
 		var minDate = new Date();
			minDate.setFullYear(lastYear);
			minDate.setMonth(0);
			minDate.setDate(1);
			
		var maxDate = new Date();
			maxDate.setYear(thisYear);
			maxDate.setMonth(thisMonth);
			maxDate.setDate(thisDate);
		
		var value = new Date();
			value.setYear(valueYear);
			value.setMonth(valueMonth);
			value.setDate(valueDate);
			
		Timer.ui.calPicker.minDate = minDate;
		Timer.ui.calPicker.maxDate = maxDate;
		Timer.ui.calPicker.value = value;	
		Timer.ui.viewCalPicker.animate(Styles.poPickerSlideUp);
		Timer.ui.txtComments.blur();
	});
	
	Timer.ui.btnClear.addEventListener('click', function(e) {
 		Timer.utils.clearTimer();
 	});
 	
 	/*
 	 * PICKER EVENTS
 	 */
 	Timer.ui.calPicker.addEventListener('change', function(e) {
		var raw = e.value.toLocaleString();
		var date = moment(raw);
		Timer.datePickerValue = date.format('YYYY-MM-DD');
		Timer.popup.title = date.format('MMM D, YYYY');
	});
	
	Timer.ui.btnCancelCal.addEventListener('click', function(e) {
		Timer.ui.viewCalPicker.animate(Styles.poPickerSlideDown);
		Timer.popup.title = moment().format('MMM D, YYYY');
	});
		
	Timer.ui.btnDoneCal.addEventListener('click', function(e) {
		Timer.ui.viewCalPicker.animate(Styles.poPickerSlideDown);
	});
	
	/*
	 * PROJECT PICKER
	 */
	
	Timer.ui.fieldProject.addEventListener('focus', function(e) {
		Timer.ui.viewProjectPicker.animate(Styles.poPickerSlideUp);
		this.blur();
	});
	
	Timer.ui.btnDropProject.addEventListener('click', function(e) {
		Timer.ui.viewProjectPicker.animate(Styles.poPickerSlideUp);
		Timer.ui.fieldProject.blur();
		Timer.ui.txtComments.blur();
	});
	
	Timer.ui.btnDoneProject.addEventListener('click', function(e) {
		var projectID = Timer.ui.projectPicker.getSelectedRow(0).value;
		Timer.projectID = projectID;
		
		var taskSelectObj = {
			pickerObj: Timer.ui.taskPicker,
			fieldButton: Timer.ui.btnDropTask
		}
		DataTasks.getTasksForSelect(projectID,taskSelectObj);		
		Timer.ui.viewProjectPicker.animate(Styles.poPickerSlideDown);
		
		Timer.ui.fieldProject.value = Utils.txtToField(Timer.ui.projectPicker.getSelectedRow(0).title);
	});	
	
	Timer.ui.btnCancelProject.addEventListener('click', function(e) {
		Timer.ui.viewProjectPicker.animate(Styles.poPickerSlideDown);
	});
	
	Timer.data.populateProjects(Timer.ui.projectPicker);
	
	/*
	 * TASK PICKER
	 */
	Timer.ui.btnDropTask.addEventListener('click', function(e) {
		Timer.ui.viewTaskPicker.animate(Styles.poPickerSlideUp);
		Timer.ui.fieldTask.blur();
		Timer.ui.txtComments.blur();
	});
	
	Timer.ui.btnCancelTask.addEventListener('click', function(e) {
		Timer.ui.viewTaskPicker.animate(Styles.poPickerSlideDown);
	});
		
	Timer.ui.btnDoneTask.addEventListener('click', function(e) {		
		var taskTitle;
		if (typeof Timer.ui.taskPicker.getSelectedRow(0) == 'undefined') {
			taskTitle = '';
		} else {
			if (Timer.ui.taskPicker.getSelectedRow(0).value == '00') {
				taskTitle = '';
			} else {
				taskTitle = Timer.ui.taskPicker.getSelectedRow(0).title;
				Timer.taskID = Timer.ui.taskPicker.getSelectedRow(0).value;
			}	
		}
		taskTitle = Utils.trim(taskTitle);
		Timer.ui.fieldTask.value = Utils.txtToField(taskTitle);
		Timer.ui.viewTaskPicker.animate(Styles.poPickerSlideDown);
	});
	
	/*
	 * TIMER PICKER
	 */
	Timer.ui.btnCancelTime.addEventListener('click', function(e) {
		Timer.ui.viewTimePicker.animate(Styles.poPickerSlideDown);
	});
		
	Timer.ui.btnDoneTime.addEventListener('click', function(e) {
		if (Timer.timePickerValue != '' && Timer.timePickerValue != null) {
			Timer.ui.lblTimer.text = Timer.timePickerValue;	
		}		
		Timer.ui.viewTimePicker.animate(Styles.poPickerSlideDown);
	});
	
	Timer.ui.timePicker.addEventListener('change', function(e) {
		var raw = e.value.toLocaleString();
		var rawArray = raw.split(' ');
		var timeArray = rawArray[3].split(':');
		var ampm    = rawArray[4];
		var hours   = parseInt(timeArray[0]);
		var minutes = timeArray[1];
		
		if (ampm == 'AM') {
			if (hours == 12) {	hours = '00'; }
		} else {
			hours = parseInt(hours + 12);
		}
		hours = hours+'';
		if (hours.length<2) { hours = '0'+hours; }
		if (minutes.length<2) { minutes = '0'+minutes; }
		Timer.timePickerValue = hours+':'+minutes+':00';		
		
		if (Timer.timerControl == 'start') {
			Timer.utils.toggleSaveButton(true);
		}
	});
	
	/*
	 * COMMENTS BOX
	 */
	Timer.ui.txtComments.addEventListener('focus', function(e) {
		this.color = '#000';
		if (this.value == 'Comments') {
			this.value = '';	
		}
	});
	
	Timer.ui.btnTimer.addEventListener('click', function(e) {	
		var start = new Date().getTime();
		
		if (Timer.timePickerValue.length>0) {
			
			var timePickerValueSeconds = parseInt(TimesheetsUtils.format.elapsedTimeSeconds(Timer.timePickerValue)*1000);
			start = parseInt(start-timePickerValueSeconds);
		}
		
		var data = Timer.utils.getFormData();
		
		if (Timer.timerControl == 'start') {
			Timer.timerAction  = 'start';
			Timer.timerControl = 'stop';
			this.backgroundGradient = Styles.gradient('red');
		    this.borderColor = '#f21413';
			this.color = '#ffffff';			
			this.title = 'Stop';
			data.ElapsedTime = '';	
			Timer.ui.btnClear.enabled = false;
			Timer.utils.toggleSaveButton(false);
						
			Timer.clockInterval = setInterval(function() {
			    var newTime = new Date().getTime();
			    var niceTime = TimesheetsUtils.format.niceTimeFromMilliseconds(newTime - start);			    
			    Timer.timerText = niceTime;
			    Timer.ui.lblTimer.text = niceTime;
			    Timer.timePickerValue = niceTime;
			    Ti.App.fireEvent('timerIncrement', {
					time: niceTime
				});
			}, 1000);	
			
			Ti.App.fireEvent('timerStart', {
				values:[Timer.ui.fieldProject.value, Timer.ui.fieldTask.value]
			});
			
			//globals.tabs.currentTab.badge = 'T';
			//globals.badgeTriggers.timers = 1;
			//globals.utils.setAppBadge();
		} else {
			Timer.timerAction  = 'stop';
			Timer.timerControl = 'start';
			this.title = 'Start';
			this.backgroundGradient = Styles.gradient('green');
		    this.borderColor = '#9acc01';
			this.color = '#567201';	
			data.ElapsedTime = TimesheetsUtils.format.elapsedTimeDecimal(Timer.timerText);	
			clearInterval(Timer.clockInterval);
			
			Timer.utils.toggleSaveButton(true);
			Timer.ui.btnClear.enabled = true;
			
			//globals.tabs.currentTab.badge = null;
			//globals.badgeTriggers.timers = 0;
			//globals.utils.setAppBadge();
		}
		data.Action = Timer.timerAction;		
		Timer.timesheetID = TimesheetsUtils.data.saveTimesheetEntry(data);
	});
	
	Timer.ui.btnSave.addEventListener('click', function(e) {
		/*
		 * Check some required stuff first
		 */
		if (Timer.ui.fieldProject.value == '') {
			var alert = Ti.UI.createAlertDialog({
				message: 'Please select a project first.',
				ok: 'Okay',
				title: 'Oops!'
			}).show();
		} else {
			var data = Timer.utils.getFormData();
			TimesheetsUtils.data.saveTimesheetEntry(data);
			TimesheetsUtils.data.saveTimesheetEntryToServer(data);
			Timer.utils.clearTimer();
		}	
	});
}

Timer.utils = {
	toggleSaveButton : function(state) {
		if (state == true) {
			/*
			 * Turn button ON
			 */
			Timer.ui.btnSave.backgroundGradient = Styles.gradient('ltBlue');
			Timer.ui.btnSave.enabled = true;
			Timer.ui.btnSave.borderColor = '#3787d3';
			Timer.ui.btnSave.color = '#2873ba';
		} else {
			Timer.ui.btnSave.backgroundGradient = Styles.gradient('mdGray');
			Timer.ui.btnSave.color = '#a3a1a1';
			Timer.ui.btnSave.borderColor = '#ccc9c9';
			Timer.ui.btnSave.enabled = false;
		}
	},
	clearTimer : function() {
		Timer.ui.lblTimer.text = '00:00:00';
		Timer.ui.timerText = '00:00:00';
		Timer.timesheetID = null;
		Timer.ui.fieldProject.value = '';
		Timer.ui.projectPicker.setSelectedRow(0,0,false);
		Timer.ui.taskPicker.setSelectedRow(0,0,false);
		Timer.ui.timePicker.setCountDownDuration(0);
		Timer.ui.fieldTask.value = '';
		Timer.ui.txtComments.value = '';
		Timer.timePickerValue = '';
		Timer.ui.btnDropTask.visible = false;
		Timer.utils.toggleSaveButton(false);
		
		var now = moment();
		Timer.datePickerValue = now.format('YYYY-MM-DD');
		Timer.popup.title = now.format('MMM D, YYYY');
		
		Ti.App.fireEvent('timerClear');
	},
	getFormData : function() {
		var start = new Date().getTime();
		var billable = '0';
		
		if (Timer.ui.chkBillable.value == true) {
			billable = '1';
		}
		
		var comments = '';
		if (Timer.ui.txtComments.value != 'Comments') {
			comments = Utils.escapeString(Timer.ui.txtComments.value);
		}
		/*
		if (typeof Timer.ui.projectPicker.getSelectedRow(0) == 'undefined') {
			projectID = '';
		} else {
			projectID = Timer.ui.projectPicker.getSelectedRow(0).value;
		}
		
		if (typeof Timer.ui.taskPicker.getSelectedRow(0) == 'undefined') {
			taskID = '';
		} else {
			taskID = Timer.ui.taskPicker.getSelectedRow(0).value;
		}
		*/
		
		var data = {
			TimesheetID:      Timer.timesheetID,
			ProjectID:        Timer.projectID,
			ProjectTitle:     Timer.ui.fieldProject.value,
			TaskID:           Timer.taskID,
			TaskTitle:        Timer.ui.fieldTask.value,
			Time:             start,
			Billable:         billable,
			Comments:         comments,
			DateTime:         Timer.datePickerValue,
			ElapsedTime:      TimesheetsUtils.format.elapsedTimeDecimal(Timer.ui.lblTimer.text)
		}
		
		return data;
	}
}

Timer.data = {
	populateProjects : function(selectObj) {
		var pickerData = [], projectObj;
		for (var projectIndex = 0; projectIndex < (DataApp.ProjectsForSelect.length); projectIndex++ ) {
			projectObj = DataApp.ProjectsForSelect[projectIndex];
			pickerData.push(Ti.UI.createPickerRow({title: projectObj.Title, value: projectObj.ProjectID}));
		}
		selectObj.add(pickerData);
		selectObj.selectionIndicator = true;
	},
	setTimerProjectTask : function(projectID,taskID) {
		Timer.ui.projectPicker.setSelectedRow(0,0,false);
		Timer.ui.taskPicker.setSelectedRow(0,0,false);
		
		if (typeof projectID != 'undefined') {
			Timer.projectID = projectID;
			
			/*
			 * Get project title
			 */
			for(a=0;a<=DataApp.ProjectsForSelect.length-1;a++) {
				var project = DataApp.ProjectsForSelect[a];
				if (project.ProjectID == projectID) {
					Timer.ui.fieldProject.value = project.Title;
					Timer.ui.projectPicker.setSelectedRow(0,a,false);
				}
			}
			
			/*
			 * Get tasks for this project.
			 */
			var taskSelectObj = {
				pickerObj: Timer.ui.taskPicker,
				fieldButton: Timer.ui.btnDropTask
			}
			
			if (typeof taskID != 'undefined') {
				Timer.taskID = taskID;
				DataTasks.getTasksForSelect(projectID,taskSelectObj,taskID);
				Ti.App.addEventListener('tasksLoadedWithSelectedTask', function(e) {
					Ti.API.info(e);
				});	
			} else {
				DataTasks.getTasksForSelect(projectID,taskSelectObj);
			}	
		}
	}	
}
