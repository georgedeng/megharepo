DatePicker = {}
DatePicker.ui = {}
DatePicker.pickerValue;

DatePicker.init = function(configObj) {
	var now = moment();
			
	DatePicker.ui.calPicker = Ti.UI.createPicker({
		type:Ti.UI.PICKER_TYPE_DATE,
		displayObj: configObj.displayObj,
		dataObj: configObj.dataObj
	});	
	
	DatePicker.ui.popup = Ti.UI.iPad.createPopover({ 
		height: 220, 
		width: 290,
		title: now.format('MMM D, YYYY')
	});
	
	DatePicker.ui.popup.add(DatePicker.ui.calPicker);
	DatePicker.addEventListeners();
	DatePicker.ui.popup.show({ view: configObj.view });
	DatePicker.ui.popup.arrowDirection = configObj.arrowDirection;	
	
	if (configObj.initialDate != '') {
		DatePicker.pickerValue = configObj.initialDate;
		var valueArray = configObj.initialDate.split('-');
 		var valueYear  = valueArray[0];
 		var valueMonth = parseInt(valueArray[1]-1);
 		var valueDate  = valueArray[2];
		
		var date = new Date();
			date.setYear(valueYear);
			date.setMonth(valueMonth);
			date.setDate(valueDate);
		
		DatePicker.ui.calPicker.value = date;	
	} else {
		DatePicker.pickerValue = now.format('YYYY-MM-DD');
	}	
}

DatePicker.addEventListeners = function() {
	DatePicker.ui.calPicker.addEventListener('change', function(e) {
		var raw = e.value.toLocaleString();
		var date = moment(raw);
		var serverDateFormat = date.format('YYYY-MM-DD');
		var displayDateFormat = date.format('MMM D, YYYY');
		
		DatePicker.pickerValue = serverDateFormat;
		this.displayObj.date = serverDateFormat;
		this.displayObj.value = displayDateFormat;
		DatePicker.ui.popup.title = displayDateFormat;
	});
}
