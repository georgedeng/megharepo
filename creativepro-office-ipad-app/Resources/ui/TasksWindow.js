Tasks = {};
Tasks.loaded = false;
Tasks.dataStore = {};
Tasks.ui = {};
Tasks.projectID;

Tasks.win = Ti.UI.createWindow();

Tasks.init = function() {
	Tasks.ui.quickAddBar = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: 55,
		top: 0,
		left: 0,
		backgroundGradient: Styles.gradient('dkGray')
	});
	
	Tasks.ui.quickAddTextbox = Ti.UI.createTextField({
		color: '#333',
		width: 400,	height: 38,	top: 7,	left: 30,
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		hintText: 'Task quick add',
		clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS
	});	
	
	Tasks.ui.quickAddDateTextbox = Ti.UI.createTextField({
		color: '#333',
		width: 150,	height: 38, top: 7,	left: 440,
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		hintText: 'Due today',
		enabled: false,
		date: ''
	});
	
	Tasks.ui.quickAddSave = Ti.UI.createButton({
		title: 'Save',
		color: '#2873ba',
		borderColor: '#3787d3',
		backgroundImage: 'none',
		borderRadius: 8,
		borderWidth: 1,
		font: { fontSize: 16, fontWeight: 'bold' },
	    backgroundGradient: Styles.gradient('ltBlue'),
		height: 36,
		width: 60,
		top: 8,
		left: 600		
	});
	
	Tasks.ui.quickAddBar.add(Tasks.ui.quickAddTextbox,Tasks.ui.quickAddDateTextbox,Tasks.ui.quickAddSave);
	
	Tasks.ui.tblTasks = Ti.UI.createTableView({
		width: Ti.UI.FILL,
		left: 0,
		top: 50,
		editable: true
	});	
	
	Tasks.win.add(Tasks.ui.quickAddBar,Tasks.ui.tblTasks);
	Tasks.addEventListeners();
}

Tasks.createUI = function() {
	var data = [];
	var tasks = Tasks.dataStore.MilestonesTasks;
	var m,t,count = 0,row,sectionData = [];
	
	Tasks.ui.quickAddTextbox.value = '';
	Tasks.ui.quickAddDateTextbox.value = '';
	
	for(var a=0;a<=tasks.length-1;a++) {
		m = tasks[a];
		
		sectionData[count] = Ti.UI.createTableViewSection();
		var viewHeader = Styles.tableSectionHeader(55);
		var lblHeaderTitle = Ti.UI.createLabel({
			top: 0,
			left: 30,
			width: '100%',
			height: 40,
			text: m.Title,
			font: Styles.type('h2'),
		    color: '#333',
		    shadowColor: '#fff',
		    shadowOffset: {x:0, y:1}
		});
		
		var dateString = '';
		if (m.Date_Start != '') {
			dateString += m.Date_Start;
		}
		if (m.Date_End != '') {
			dateString += ' to '+m.Date_End;
		}
		if (m.NameFirst != '') {
			dateString += ' Created by '+m.NameFirst+' '+m.NameLast;
		}
		var lblHeaderData = Ti.UI.createLabel({
			font: Styles.type('subText'),
			color: '#666',
			text: dateString,
			top: 31,
			left: 30,
			shadowColor: '#fff',
		    shadowOffset: {x:0, y:1}
		});
		
		viewHeader.add(lblHeaderTitle,lblHeaderData);
		sectionData[count].headerView = viewHeader;
		
		for(var b=0;b<=m.Tasks.length-1;b++) {
			t = m.Tasks[b];
			
			var viewLeft = Ti.UI.createView({
				width: '70%',
				left: 60
			});
			
			var viewRight = Ti.UI.createView({
				width: '30%',
				right: 30
			});
		
			var lblTitle = Ti.UI.createLabel({
				font: Styles.type('h3'),
			    text: Utils.txtToField(t.Title),
			    height: 28,
			    left: 75,
			    top: 6
			});
			var dateString = '';
			if (t.DateString != '') {
				dateString += t.DateString
			}
			if (t.CreatorName != '') {
				dateString += ' '+t.CreatorName;
			}
			var lblDates = Ti.UI.createLabel({
				font: Styles.type('subText'),
				color: '#666',
				text: dateString,
				top: 33,
				left: 75,
				shadowColor: '#fff',
			    shadowOffset: {x:0, y:1}
			});
			
			var lblChkVisible = false, checked = false;
			if (t.StatusMachine == 'status_completed') {
				lblChkVisible = true;
				checked = true;
			}
			
			var lblCheckbox = Ti.UI.createLabel({
				width: 60,
				height: 60,
				borderRadius: 8,
				borderWidth: 1,
				left: 0,
				checked: checked,
				id: 'taskCheckbox',
				borderColor: '#e8e7d6',
				backgroundGradient: Styles.gradient('ltGray')
			});
						
			var lblCheck = Ti.UI.createLabel({
				backgroundImage: 'images/check-mark-64.png',
				id: 'taskCheckmark',
				visible: lblChkVisible,
				checked: checked,
				width: 64,
				height: 64,
				left: 0
			});		
			
			var lblStatus = Utils.getStatusBadge(t.StatusMachine,t.StatusHuman);
			lblStatus.left = 75;
			lblStatus.top = 62;
			lblStatus.id = 'status';
			
			var lblHours = Ti.UI.createLabel({
				font: Styles.type('bigText'),
				color: '#7ebcf7',
				textAlign: 'right',
				text: t.HoursActual+' hrs',
				top: 6,
				width: '100%',
				height: 48,
				right: 0
			});
			
			var priIcon = Utils.getPriorityIcon(t.Priority);
			var lblPriorityIcon = Ti.UI.createLabel({
				backgroundImage: 'images/'+priIcon,
				top: 62,
				width: 14,
				height: 14,
				left: 100
			});
			
			var lblPriorityText = Ti.UI.createLabel({
				font: Styles.type('badge'), 
				text: t.PriorityText+' priority',
				color: '#000',
				top: 62,
				width: 100,
				height: 14,
				left: 120
			});
			
			viewLeft.add(lblCheckbox,lblCheck,lblTitle,lblDates,lblStatus);
			viewRight.add(lblHours,lblPriorityIcon,lblPriorityText);
			
			var styleObj = {
				backgroundColor: '#23292c',
				height: 100,
				width: 30,
				icon: { image: 'images/arrow-right-small.png', width: 14, height: 20 },
				barId: 'lblBtnBar',
				btnId: 'lblBtnBarIcon'
			}				
			var lblBtnBar = UI.tableRowBtnBarSlide(styleObj);
		
			row = Ti.UI.createTableViewRow({ 
				backgroundColor: 'white',
				hasChild: false,
				projectID: t.ProjectID,
				taskID: t.TaskID,
				milestoneIndex: a,
				taskIndex: b,
				taskTitle: t.Title,
				height: 100,
				className: 'task',
				selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
			});
		
			row.add(viewLeft,viewRight,lblBtnBar);
			sectionData[count].add(row);
		}	
		count++;
	}
	Tasks.ui.tblTasks.setData(sectionData);
}		

Tasks.addEventListeners = function() {
	Ti.App.addEventListener('tasksLoaded', function(e) {
		Tasks.createUI();
	});
	
	Ti.App.addEventListener('taskDigestLoaded', function(e) {
		if (e.digestView != true) { 
			Tasks.createUI();
		}	
		Dashboard.dataStore.taskDigest = e.taskObj;
		Dashboard.createTasksUI();
	});
	
	Tasks.ui.quickAddDateTextbox.addEventListener('click', function() {
		var configObj = {
			displayObj: Tasks.ui.quickAddDateTextbox,
			initialDate: Tasks.ui.quickAddDateTextbox.date,
			arrowDirection: Ti.UI.iPad.POPOVER_ARROW_DIRECTION_TOP,
			view: this
		}
		DatePicker.init(configObj);
	});
	
	Tasks.ui.quickAddSave.addEventListener('click', function() {
		Tasks.data.saveTaskQuickAdd();
	});
	
	Tasks.ui.tblTasks.addEventListener('delete', function(e) {
       	var task = e.rowData.taskTitle
       	var row = e.row;
       	var rowIndex = e.index;
 
	    var alertYesNo = Titanium.UI.createAlertDialog({
	        message: 'Are you sure you want to delete this task: ' + task,
	        buttonNames: ['Yes','No']
	    });
 
	    alertYesNo.addEventListener('click', function(e) {
	        if (e.index == 0) { 
	        	Ti.API.info('delete that sucker');
	        } else if (e.index == 1) { 
	        	/*
	        	 * Put the row back since it will be removed from the view.
	        	 */
	        	Tasks.ui.tblTasks.insertRowAfter(rowIndex-1, row);
	        }	 
	    });
	    alertYesNo.show();
	});
	
	Tasks.ui.tblTasks.addEventListener('click', function(e) {
		var row = e.row;
		var rowdata = e.rowData;
		var rowIndex = e.index;
		if (e.source.id == 'files') {
			/*
			 * We've hit the FILES button.
			 */
			alert('getting files');
			/*
			MapModalWindow.init(e.source.pObj);
			MapModalWindow.win.open({
				modal: true,
				modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
			});
			*/
		} else if (e.source.id == 'messages') {
			/*
			 * We've hit the MESSAGES button
			 */
			alert('messages');
		} else if (e.source.id == 'timer_tasks') {
			/*
			 * We've hit the TIMER button
			 */
			Timer.popup.show({ view: e.source.getParent() });
			Timer.data.setTimerProjectTask(row.projectID,row.taskID);
		} else if (e.source.id == 'lblBtnBar' || e.source.id == 'lblBtnBarIcon') {			
			var lbl;
			if (e.source.id == 'lblBtnBarIcon') {
				lbl = e.source.getParent();
			} else {
				lbl = e.source;
			}
			
			if (Projects.openedButtonBar != false) {
				Projects.openedButtonBar.animate({ width: 30, opacity: 1 });
				Projects.openedButtonBar.toggleButton.backgroundImage = 'images/arrow-right-small.png';				
			}
						
			if (lbl.opened == true) {
				/*
				 * Close the button bar
				 */		
				lbl.animate({ width: 30, opacity: 1 });
				Projects.openedButtonBar = false;
				lbl.opened = false;
				lbl.toggleButton.backgroundImage = 'images/arrow-right-small.png';
			} else {
				/*
				 * Open the button bar
				 */
				Projects.openedButtonBar.opened = false;
				lbl.animate({ width: 200, opacity: 0.9 });	
				lbl.opened = true;
				lbl.toggleButton.backgroundImage = 'images/arrow-left-small.png';
				Projects.openedButtonBar = lbl;
			}			
			
			var btnMessages = Ti.UI.createButton({
				backgroundImage: 'images/icon-bar-bottom-dashboard-2.png',
				width: 34,
				height: 34,
				left: 30,
				id: 'messages'
			});
			
			var btnFiles = Ti.UI.createButton({
				backgroundImage: 'images/icon-bar-bottom-dashboard-2.png',
				width: 34,
				height: 34,
				left: 70,
				id: 'files'
			});
			
			var btnTimer3 = Ti.UI.createButton({
				backgroundImage: 'images/icon-bar-bottom-clock.png',
				width: 34,
				height: 34,
				left: 110,
				id: 'timer_tasks'
			});
			
			lbl.add(btnMessages,btnFiles,btnTimer3);
		} 
		else if (e.source.id == 'taskCheckbox' || e.source.id == 'taskCheckmark') {
			/*
			 * Marking task as completed.
			 */
			var checkbox = e.source;
			if (e.source.id == 'taskCheckmark') {
				checkbox = e.source.getParent().children[1];
			}
			var lblStatus = e.source.getParent().children[4], checkMark;
				
			if (checkbox.checked == true) {
				checkMark = checkbox.getParent().children[1];
				checkbox.checked = false;
				checkMark.checked = false;
				checkMark.visible = false;
				
				/*
				 * Incomplete (but in progress)
				 */
				DataTasks.updateTaskStatus(row.taskID,1);
				Styles.badge('In Progress','blue',true,lblStatus);
			} else {
				checkMark = checkbox.getParent().children[1];
				checkbox.checked = true;
				checkMark.checked = true;
				checkMark.visible = true;
				
				/*
				 * Complete
				 */
				DataTasks.updateTaskStatus(row.taskID,2);
				Styles.badge('Completed','green',true,lblStatus);
			}
		} else {
			/*
			 * Open the task detail window
			 */
			var taskObj = Tasks.dataStore.MilestonesTasks[row.milestoneIndex].Tasks[row.taskIndex];
			var taskID = row.taskID;
			Ti.App.fireEvent('openModal', {
				values: ['task','view',taskID,null]
			});
		}
	});
}	

Tasks.data = {
	populateTasksTable : function(projectID) {
		Tasks.projectID = projectID;
		DataTasks.getTasks(projectID);
	},
	saveTaskQuickAdd : function() {
		if (Tasks.ui.quickAddTextbox.value != '') {
			var data = {
				action: 'add',
				projectID: Tasks.projectID,
				taskID: 0,
				milestoneID: 0,
				projectID: Tasks.projectID,
				taskTitle: Tasks.ui.quickAddTextbox.value,
				taskEstimatedTime: '',
				taskDateStart: '0000-00-00',
				taskDateEnd: Tasks.ui.quickAddDateTextbox.date,
				priority: '',
				taskTags: '',
				description: ''
			}
			DataTasks.saveTask(data);
		}
		Tasks.ui.quickAddTextbox.value = '';
		Tasks.ui.quickAddDateTextbox.value = '';
		Tasks.ui.quickAddDateTextbox.date = '';
	}
}

Tasks.utils = {
	parseDigestView : function(jsonObject) {
		var totalTasksOverdue, totalTasksToday, totalTasksTomorrow, totalTasksThisWeek, totalTasksNextWeek, totalTasksIncomplete = '0', totalTasksArchive = '0';
	    if (jsonObject.TasksOverdue) {
	        totalTasksOverdue  = jsonObject.TasksOverdue.length;
	    }
	    if (jsonObject.TasksToday) {
	        totalTasksToday    = jsonObject.TasksToday.length;
	    }
	    if (jsonObject.TasksTomorrow) {
	        totalTasksTomorrow = jsonObject.TasksTomorrow.length;
	    }
	    if (jsonObject.TasksThisWeek) {
	        totalTasksThisWeek = jsonObject.TasksThisWeek.length;
	    }
	    if (jsonObject.TasksNextWeek) {
	        totalTasksNextWeek = jsonObject.TasksNextWeek.length;
	    } 
	    if (jsonObject.TasksIncomplete) {
	        totalTasksIncomplete = jsonObject.TasksIncomplete;
	    }  
	    if (jsonObject.TasksArchive) {
	        totalTasksArchive = jsonObject.TasksArchive;
	    }  
	    
	    var taskTotalObj = {
	    	TasksOverdue:    { Title: 'Overdue tasks', Total: totalTasksOverdue, IconBig: 'iconWarningBig.png', IconSmall: 'iconWarningSmall.png' },
	    	TasksToday:      { Title: 'Tasks due today', Total: totalTasksToday, IconBig: 'iconCalendarDay.png', IconSmall: 'iconCalendarDaySmall.png'  },
	    	TasksTomorrow:   { Title: 'Tasks due tomorrow', Total: totalTasksTomorrow, IconBig: 'iconCalendarDay.png', IconSmall: 'iconCalendarDaySmall.png' },
	    	TasksThisWeek:   { Title: 'Tasks this week', Total: totalTasksThisWeek, IconBig: 'iconCalendarDay.png', IconSmall: 'iconCalendarDaySmall.png' },
	    	TasksNextWeek:   { Title: 'Tasks next week', Total: totalTasksNextWeek, IconBig: 'iconCalendarDay.png', IconSmall: 'iconCalendarDaySmall.png' }
	    }
	    
	    return taskTotalObj;
	}
}
