Projects = {};
Projects.loaded = false;
Projects.dataStore = {};
Projects.ui = {};
Projects.openedButtonBar = false;

Projects.win = Ti.UI.createWindow();

Projects.init = function() {
	Projects.ui.tblProjects = Ti.UI.createTableView({
		width: Ti.UI.FILL,
		left: 0,
		top: 0,
		editable: true
	});	
	
	Projects.win.add(Projects.ui.tblProjects);
	Projects.addEventListeners();
}

Projects.createUI = function() {
	var data = [];
	var projects = Projects.dataStore.Projects;
	var p;
	
	for(var a=0;a<=projects.length-1;a++) {
		p = projects[a];
				
		var viewLeft = Ti.UI.createView({
			width: '50%',
			left: 60
		});
		
		var viewRight = Ti.UI.createView({
			width: '50%',
			right: 30
		});
		
		var lblTitle = Ti.UI.createLabel({
			font: Styles.type('h3'),
		    text: p.Title,
		    height: 28,
		    left: 0,
		    top: 6
		});
			
		var lblClient = Ti.UI.createLabel({
			font: Styles.type('subText'),
			color: '#999',
			text: p.Company.toUpperCase(),
			top: 33,
			left: 0,
			height: 18
		});
			
		var lblStatus;
		switch(p.StatusMachine) {
			case 'status_on_hold':
				lblStatus = Styles.badge(p.StatusHuman,'yellow');
				break;
			case 'status_in_progress':
				lblStatus = Styles.badge(p.StatusHuman,'blue');
				break;
			case 'status_past_due':
				lblStatus = Styles.badge(p.StatusHuman,'red');
				break;	
			case 'status_completed':
				lblStatus = Styles.badge(p.StatusHuman,'green');
				break;		
		}	
		lblStatus.left = 0;
		lblStatus.top = 60;
		
		var lblInvoiced = Ti.UI.createLabel({
			font: Styles.type('subText'),
			text: 'Invoiced',
			color: '#999',
			textAlign: 'right',
			top: 45,
			width: '100%',
			right: 0
		});
			
		var lblInvoicedTotal = Ti.UI.createLabel({
			font: Styles.type('bigText'),
			color: '#7ebcf7',
			textAlign: 'right',
			text: p.InvoiceTotalHuman,
			top: 6,
			width: '100%',
			height: 48,
			right: 0
		});
		
		var iconLblDates = Ti.UI.createLabel({
			backgroundImage: 'images/iconCalendarLtGray.png',
			width: 16,
			height: 16,
			top: 6,
			left: 10
		});
			
		var lblDates = Ti.UI.createLabel({
			font:{ fontSize: 12 },
			text: p.Date_Range,
			top: 8,
			left: 33,
			height: 18
		});
		/*
		var styleObj = {
			backgroundColor: '#f6f6ec',
			borderColor: '#e8e7d6',
			align: 'right',
			width: 30,
			height: 90,
			labels: [
				{ image: 'images/iconCalendar.png', width: 16, height: 16, id: 'calendar', badge: '3' },
				{ image: 'images/iconDisk.png', width: 16, height: 16, id: 'files' },
				{ image: 'images/box_opened_brown.png', width: 16, height: 16, id: 'goober' }
			]
		}
		var btnBar = UI.tableRowBtnBarVertical(styleObj);
		*/
		var styleObj = {
			backgroundColor: '#23292c',
			height: 100,
			width: 30,
			icon: { image: 'images/arrow-right-small.png', width: 14, height: 20 },
			barId: 'lblBtnBar',
			btnId: 'lblBtnBarIcon',
			badge: p.TasksOverdue
		}				
		var lblBtnBar = UI.tableRowBtnBarSlide(styleObj);
		
		viewLeft.add(lblTitle,lblClient,lblStatus);
		viewRight.add(lblInvoiced,lblInvoicedTotal);
		
		data[a] = Ti.UI.createTableViewRow({ 
			backgroundColor: 'white',
			hasChild: false,
			projectID: p.ProjectID,
			projectTitle: p.Title,
			height: 100,
			className: 'project',
			alerts: {
				tasksOverdue: p.TasksOverdue
			},
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		
		data[a].add(viewLeft,viewRight,lblBtnBar);
	}	
	Projects.ui.tblProjects.data = data;
}

Projects.addEventListeners = function() {
	Ti.App.addEventListener('projectsLoaded', function(e) {
		Projects.createUI();
	});
	
	Projects.ui.tblProjects.addEventListener('delete', function(e) {
       	var project = e.rowData.projectTitle
       	var row = e.row;
       	var rowIndex = e.index;
 
	    var alertYesNo = Titanium.UI.createAlertDialog({
	        message: 'Are you sure you want to delete this project: ' + project,
	        buttonNames: ['Yes','No']
	    });
 
	    alertYesNo.addEventListener('click', function(e) {
	        if (e.index == 0) { 
	        	Ti.API.info('delete that sucker');
	        } else if (e.index == 1) { 
	        	/*
	        	 * Put the row back since it will be removed from the view.
	        	 */
	        	Ti.API.info(rowIndex);
	        	Projects.ui.tblProjects.insertRowBefore(rowIndex, row);
	        	Ti.API.info('NO DONT');
	        }	 
	    });
	    alertYesNo.show();
	});
	
	Projects.ui.tblProjects.addEventListener('click', function(e) {
		var row = e.row;
		var rowdata = e.rowData;
		var rowIndex = e.index;
		if (e.source.id == 'files') {
			/*
			 * We've hit the FILES button.
			 */
			alert('getting files');
			/*
			MapModalWindow.init(e.source.pObj);
			MapModalWindow.win.open({
				modal: true,
				modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
			});
			*/
		} else if (e.source.id == 'tasks') {
			/*
			 * We've hit the TASKS button
			 */
			var data = {
				projectID: row.projectID,
				winTitle: row.projectTitle
			}
			MenuView.switchWindows('tasks',data);
		} else if (e.source.id == 'timer') {
			/*
			 * We've hit the TIMER button
			 */
			Timer.popup.show({ view: e.source.getParent() });
			Timer.data.setTimerProjectTask(row.projectID);
		} else if (e.source.id == 'lblBtnBar' || e.source.id == 'lblBtnBarIcon') {			
			var lbl;
			if (e.source.id == 'lblBtnBarIcon') {
				lbl = e.source.getParent();
			} else {
				lbl = e.source;
			}
			
			if (Projects.openedButtonBar != false) {
				Projects.openedButtonBar.animate({ width: 30, opacity: 1 });
				Projects.openedButtonBar.toggleButton.backgroundImage = 'images/arrow-right-small.png';				
			}
						
			if (lbl.opened == true) {	
				/*
				 * Close the button bar
				 */		
				lbl.animate({ width: 30, opacity: 1 });
				Projects.openedButtonBar = false;
				lbl.opened = false;
				lbl.toggleButton.backgroundImage = 'images/arrow-right-small.png';
			} else {
				/*
				 * Open the button bar
				 */
				Projects.openedButtonBar.opened = false;
				lbl.animate({ width: 200, opacity: 0.9 });	
				lbl.opened = true;
				lbl.toggleButton.backgroundImage = 'images/arrow-left-small.png';
				Projects.openedButtonBar = lbl;
			}			
			
			var btnTasks = Ti.UI.createButton({
				backgroundImage: 'images/icon-bar-bottom-tasks.png',
				width: 34,
				height: 34,
				left: 30,
				id: 'tasks'
			});
			
			var lblTaskAlert = Ti.UI.createLabel();
			if (row.alerts.tasksOverdue > 0) {
				lblTaskAlert = UI.alertBadge(row.alerts.tasksOverdue);
				lblTaskAlert.top = 22;
				lblTaskAlert.left = 50;
				lblTaskAlert.id = 'tasksBadge';
				lblTaskAlert.touchEnabled = false;
			}	
			
			var btnFiles = Ti.UI.createButton({
				backgroundImage: 'images/icon-bar-bottom-dashboard-2.png',
				width: 34,
				height: 34,
				left: 70,
				id: 'files'
			});
			
			var btnTimer2 = Ti.UI.createButton({
				backgroundImage: 'images/icon-bar-bottom-clock.png',
				width: 34,
				height: 34,
				left: 110,
				id: 'timer'
			});
			
			lbl.add(btnTasks,lblTaskAlert,btnFiles,btnTimer2);
		} else {	
			/*
			 * Open the project detail window
			 */
			Ti.App.fireEvent('openModal', {
				values: ['project','view',null,Projects.dataStore.Projects[e.index]]
			});
		}
	});
}	

Projects.data = {
	populateProjectTable : function() {
		if (Projects.loaded === false) {
			DataProjects.getProjects();
		}
	}	
}