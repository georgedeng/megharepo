Ti.include(
	'lib/database.js', 
	'lib/login.js', 
	'lib/styles.js', 
	'lib/utilities.js',
	'lib/DataApp.js',
	'lib/activityIndicator.js'
);
var moment = require('lib/moment');
require('lib/requirePatch').monkeypatch(this);

var AppInit = {};
var Globals = {};
var AI = Utils.activityIndicator();

Globals.logged = false;
Globals.SITE_URL = 'http://dev.mycpohq.com/';
Globals.AUTH_KEY;
Globals.USER_TYPE;
Globals.currentWin = 'dashboard';
Globals.historyWin = '';

Database.createDatabase();

/*
 * Login window
 */
AppInit.loginWindow = Ti.UI.createWindow({
	title: 'Login',
	backgroundColor: '#eeefe1'
});

Login.tryAutoLogin();

AppInit.spinUpApp = function() {
	/*
	 * Includes
	 */
	Ti.include(
		'ui/MenuWindow.js',
		'ui/DashboardWindow.js',
		'ui/ProjectsWindow.js',
		'ui/TasksWindow.js',
		'ui/LedgerWindow.js',
		'ui/InvoicesWindow.js',
		'ui/ExpensesWindow.js',
		'ui/TimesheetsWindow.js',
		'ui/ReportsWindow.js',
		'ui/TimerPopup.js',
		'ui/SearchResultsWindow.js',
		'ui/DatePickerPopup.js',
		'ui/TasksModalWindow.js',
		'ui/ProjectsModalWindow.js',
		'ui/MessageModalWindow.js',
		'ui/DocumentWindow.js',
		'lib/DataTasks.js',
		'lib/DataProjects.js',
		'lib/DataSearch.js',
		'lib/DataDashboard.js',
		'lib/DataMessages.js',
		'lib/Timesheets.js',
		'lib/uiComponents.js',
		'lib/windowProps.js'
	);
	MenuView.init();
	Dashboard.init();
	Projects.init();
	Tasks.init();
	Ledger.init();
	Expenses.init();
	Invoices.init();
	Timesheets.init();
	Reports.init();
	Timer.init();
	DocWindow.init();
	
	AppInit.windowToggled = false;
	
	AppInit.windowAnimateRight = Ti.UI.createAnimation({
		left: 300,
		curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
		duration: 200
	});
	
	AppInit.windowAnimateLeft = Ti.UI.createAnimation({
		left: 0,
		curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
		duration: 200
	});
	
	/*
	 * Define our main windows.
	 */
	AppInit.mainWindow = Ti.UI.createWindow({
		width: Ti.UI.FILL,
		height: Ti.UI.FILL
	});
	AppInit.detailWindow = Ti.UI.createWindow({
		barColor: '#94b735',
		backgroundColor: '#eeefe1',
		width: Ti.UI.FILL,
		height: Ti.UI.FILL
	});
			
	AppInit.detailNav = Ti.UI.iPhone.createNavigationGroup({
		window: AppInit.detailWindow,
		left: 0
	});
	AppInit.mainWindow.add(AppInit.detailNav);
	
	var btnToggleWindow = Ti.UI.createButton({
 		image: 'images/icon-bar-top-list.png',
 		width: 80
 	});
 	AppInit.detailWindow.setLeftNavButton(btnToggleWindow);
 	
 	btnToggleWindow.addEventListener('click', function() {
 		if (AppInit.windowToggled == true) {
 			AppInit.mainWindow.animate(AppInit.windowAnimateLeft);
 			AppInit.windowToggled = false;
 		} else {
 			AppInit.mainWindow.animate(AppInit.windowAnimateRight);
 			AppInit.windowToggled = true;
 		}
 	});
 	
	/*
	 * Open our dashboard window
	 */
	MenuView.switchWindows('dashboard');
	AppInit.mainWindow.open();
	
	Ti.Gesture.addEventListener('orientationchange',function(e) {   
    	Ti.API.info('the orientation changed to  '+e.orientation);
    	if (e.orientation == 3) {
    		
    	}
	});
}










