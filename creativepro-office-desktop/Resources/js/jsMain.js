var app = {
	init : function() {
		$('#noNetworkContainer').hide();
		database.createDatabase();
		
		$('.chkTrigger').click(function() {
			var chk = $(this).prev();
			//var chk = $('#timerBillable');
			var state = chk.attr('checked');
			if (state == 'checked') {
				chk.removeAttr('checked');
			} else {
				chk.attr('checked', 'checked');
			}
		});
		
		/*
		 * Set up check for network connectivity.
		 */
		
		Ti.API.info('hey');
		IS_ONLINE = true;
		if (IS_ONLINE == true) {
			IS_ONLINE = true;
			app.controls();
			login.tryAutoLogin(); 
			Ti.API.info('hey');
		} else {
			/*
			 * Redirect to no-network screen.
			 */
			$('#noNetworkContainer').show();
			$('#buttonTryNetwork').click(function() {
				app.init();
			});
		}	
	},
	controls : function() {
		var currentWindow = Ti.UI.getCurrentWindow();
		
		Ti.UI.getCurrentWindow().addEventListener(Ti.MINIMIZED, function(event) {
	        currentWindow.hide();
	    });
		
		/*
		 * Tray icons and stuff like that
		 */		
		tray = Ti.UI.addTray("app://images/icon16.png", function(evt){
		    if (evt.getType() == 'clicked') {
		      if (!currentWindow.isVisible()){
		        currentWindow.show();
		        currentWindow.unminimize();
		      }
		    }
		});
		
		tray.setHint("CreativePro Office");
		
		var logout = Ti.UI.createMenuItem("Log out", function() {
			login.logout();
		});
		
		var help = Ti.UI.createMenuItem("Help", function() {
			Ti.Platform.openURL('http://www.mycpohq.com/contact');
		});		
		
		var spacer = Ti.UI.createSeparatorMenuItem();
						
		var about = Ti.UI.createMenuItem("About CreativePro Office", function() {
	        Ti.UI.showDialog({
		        url:'app://about.html',
		        width: 400,
		        height:200,
		        resizable:false,
		        maximizable:false,
		        minimizable:false
		    });
		});
		
		var quit = Ti.UI.createMenuItem("Quit CreativePro Office", function() {
			Ti.App.exit();
		});
		
		var trayMenu = Ti.UI.createMenu();
		trayMenu.appendItem(logout);
		trayMenu.appendItem(help);
		trayMenu.appendItem(spacer);
		trayMenu.appendItem(about);
		trayMenu.appendItem(quit);
		tray.setMenu(trayMenu);
		
		/*
		 * This is how you do a little growl-like message...
		 * if you're into that sort of thing.
		 */	
		 
		 /*	
		var notif = Ti.Notification.createNotification(Ti.UI.createWindow());
		notif.setTitle("New message");
		notif.setMessage("How are you doing?");
		notif.show();
		*/
				
		$('#btnMenuTasks').click(function() {
			app.hideAllWidgets();
		});
		
		$('#btnMenuTimer').click(function() {
			app.hideAllWidgets();
			$('#widgetContent_timer').show();
		});
		
		$('#btnMenuInvoices').click(function() {
			app.hideAllWidgets();
		});
		
		$('#btnMenuCalendar').click(function() {
			app.hideAllWidgets();
		});
		
		$('#btnMenuAccount').click(function() {
			app.hideAllWidgets();
			widgetLoaderObj.loadWidgetContent('account');
		});
				
		$('#buttonCloseAbout').click(function() {
			$('#widgetContent_about').hide();
		});
	},
	spinUp : function() {
		$('#appContent').show();
		$('#loginContainer').hide();
		$('#mainMenuContainer').show();
		$('#footerBar').show();
		
		/*
		 * Get projects from server
		 */
		
		getAppData.getProjectList();	
		widgetLoaderObj.loadWidgetContent('timer');
		$('#taskPickerTimer').simpleCombo();
	},
	hideAllWidgets : function() {
		$('#widgetContent_timer').hide();
		$('#widgetContent_account').hide();
		$('#widgetContent_about').hide();
	}
}

var widgetLoaderObj = {
	loadWidgetContent : function(widgetName,widgetUser) {
		switch(widgetName) {
			case 'timer':
				timer.loadWidgetTimer();
				widgetLoaded_timer = true;
				break;
			case 'tasks':
                tasks.loadWidgetTasks();
                widgetLoaded_tasks = true;
                break;
            case 'account':
                account.loadWidgetAccount();
                widgetLoaded_account = true;
                break;    
		}
	}
}
			
$(document).ready(function() {
	app.init();
});