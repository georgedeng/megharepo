<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TeamUpdate extends Controller {
	function __construct()
	{
		parent::Controller();	
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
		$this->load->model('team/Team','',true);
		$this->load->model('contacts/Contacts','',true);
        $this->load->model('common/app_data','AppData',TRUE);

        $this->load->library('application/CommonContacts');
	}
	
	function _init() {
		$data['page']              = 'team';
		$data['pageTitle']         = lang('team_my_team');
		$data['wysiwyg']           = 0;
		$data['map']               = 0;
		$data['jsFileArray']       = array('jsTeam.js','jsContacts.js','jsSettings.js');
		$data['pageIconClass']     = 'iconPageTeam';
		$data['pageLayoutClass']   = 'noRightColumn';
        $data['teamMemberInfo']    = $this->commoncontacts->getContacts($this->session->userdata('accountUserid'),NULL,NULL,TRUE,FALSE,FALSE,'json',1);
		
        $data['rolesPermissions']  = TeamUpdate::getRolesPermissions('json',1);

        /*
		 * Create tag string for people
		 */
		$tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'people');
		$data['tags'] = makeTagJSON($tagArray);

        /*
         * Can we even add any more team members?
         */
        $data['maxTeamMembers'] = 0;
        $teamCountArray = $this->Contacts->getNumberOfContacts($this->session->userdata('accountUserid'),'team');
        $teamCount = $teamCountArray['Contacts'];
        if ($teamCount >= getAccountLevelSetting('teamMembers')) {
            $data['maxTeamMembers'] = 1;
        }

        noCache();
		return $data;
	}

	function index() {
		$data = TeamUpdate::_init();
		$this->load->view('team/TeamUpdate',$data);
	}
	
	function getRolesPermissions($renderType='json',$return=0) {
		$rolesPermissionsArray = $this->Team->getRolesPermissions($this->session->userdata('accountUserid'));
		if ($rolesPermissionsArray == false) {
			return 0;
		} else {
			if ($renderType == 'json') {
				if ($return == 1) {
					return json_encode($rolesPermissionsArray);
				} else {
					echo json_encode($rolesPermissionsArray);
				}
			}
		}
	}

    function saveRolesPermissions() {
        $roleArray['action']        = $_POST['action'];
        $roleArray['roleID']        = $_POST['roleID'];
        $roleArray['roleName']      = fieldToDB($this->input->post('roleName', TRUE));
        $roleArray['permString']    = $_POST['permString'];
        $roleArray['userid']        = $this->session->userdata('userid');
        $roleArray['accountUserid'] = $this->session->userdata('accountUserid');

        if (!empty($roleArray['roleName'])) {
            $roleID = $this->Team->saveRolesPermissions($roleArray);
            echo '{"Message":"'.lang('team_role_saved').'","RoleID":"'.$roleID.'"}';
        } else {
            echo '{"Message":"'.lang('error_save').'"}';
        }
    }

    function deleteRolesPermissions($roleID) {
        $this->Team->deleteRolesPermissions($roleID);
        echo TeamUpdate::getRolesPermissions('json',1);
    }
}	
?>