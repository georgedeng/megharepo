<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AdminUsers extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('administration/Admin_users','',true);
	}

    function _init() {
        $data['page']                  = 'adminUsers';
		$data['pageTitle']             = 'Administration: Bugs';
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsAdminBugs.js');
		$data['pageIconClass']         = 'iconPageAdministration';
		$data['pageLayoutClass']       = 'withRightColumn';
        $data['rightColumnComponents'] = array('menuAdministration');

        return $data;
    }

	function index() {
        $data = AdminUsers::_init();
		$this->load->view('administration/AdminBugs',$data);
	}
}
?>