<?
class AdminUserMetrics extends Controller {
    function __construct()
	{
		parent::Controller();	
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('administration/Admin_user_metrics','UserMetrics',true);
		include(APPPATH.'libraries/pchart/pData.class');  
		include(APPPATH.'libraries/pchart/pChart.class');
        include(APPPATH.'libraries/OpenFlashCharts/open-flash-chart.php');

        $this->load->helper('chart');
	}

    function _init() {
        $data['page']                  = 'adminUsers';
		$data['siteName']              = 'CreativePro Office';
		$data['pageTitle']             = 'Administration: User Metrics';
		$data['wysiwyg']               = 0;
		$data['jsFileArray']           = array('jsAdminUserMetrics.js');
		$data['pageIconClass']         = 'iconPageAdministration';
		$data['pageLayoutClass']       = 'withRightColumn';
        $data['rightColumnComponents'] = array('menuAdministration');

        /*
         * Get some cumulative account information
         */
        $data['cumulativeStats'] = $this->UserMetrics->getCumulativeStats();

        $data['payingPercentage'] = number_format((($data['cumulativeStats']['TotalPayingUsers']/$data['cumulativeStats']['TotalUsers'])*100),1);
        return $data;
    }
	
	function index() {
        $data = AdminUserMetrics::_init();
		$this->load->view('administration/AdminUserMetrics', $data);
	}

    function reportNewAccountsByDate($reportYear,$renderType='json',$return=0) {
        $colorChartArray = colorChartArray();

        $reportArray = $this->UserMetrics->reportNewAccountsByDate($reportYear);

        /*
         * Open Flash Charts
         */
        $chart = new open_flash_chart();
        $title = new title('New accounts by month ('.$reportYear.')');
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );
        $chart->set_title( $title );
        $chart->set_bg_colour(CHART_BG_COLOR);

        $barStack = new bar_stack();
        $barStack->set_colours(array($colorChartArray[0],$colorChartArray[1],$colorChartArray[2],$colorChartArray[3]));
        $barStack->set_tooltip('#val# new accounts<br>Total: #total#');
        $barStack->set_on_show(new bar_on_show('fade-in',0,0));

        /*
         * Set chart data
         */
        $maxValues = array();
        for($a=1;$a<=12;$a++) {
            $accountsFree = 0;
            $accountsSolo = 0;
            $accountsShop = 0;
            $accountsTeam = 0;
            foreach($reportArray as $reportItem) {
                if ($reportItem['MonthNumber'] == $a) {
                    switch ($reportItem['AccountLevel']) {
                        case 0:
                            $accountsFree = intval($reportItem['Accounts']);
                            break;
                        case 1:
                            $accountsSolo = intval($reportItem['Accounts']);
                            break;
                        case 2:
                            $accountsShop = intval($reportItem['Accounts']);
                            break;
                        case 3:
                            $accountsTeam = intval($reportItem['Accounts']);
                            break;
                    }
                }
            }
            $barStack->append_stack( array( $accountsFree, $accountsSolo, $accountsShop, $accountsTeam ) );
            $maxValues[$a-1] = $accountsFree+$accountsSolo+$accountsShop+$accountsTeam;
        }

        $barStack->set_keys(
            array(
                new bar_stack_key( $colorChartArray[0], 'Free', 13 ),
                new bar_stack_key( $colorChartArray[1], 'Solo $5/mo', 13 ),
                new bar_stack_key( $colorChartArray[2], 'Shop $15/mo', 13 ),
                new bar_stack_key( $colorChartArray[3], 'Team $30/mo', 13 )
                )
    );

        $x_labels = new x_axis_labels();
        $x_labels->set_colour( '#333' );

        $x = new x_axis();
        $x->set_labels_from_array(array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'));
        $x->set_grid_colour('#eeeeee');
        $chart->set_x_axis( $x );

        $chart->add_element( $barStack );

        $x_labels = new x_axis_labels();

        $y = new y_axis();
        $yMax = max($maxValues)+20;
        $yInc = ceil($yMax/5);

        $y->set_range(0,$yMax,$yInc);
        $y->set_grid_colour('#eeeeee');
        $y->set_label_text( "#val#" );

        $t = setChartTooltipStyle();

        $chart->set_y_axis($y);
        $chart->set_tooltip($t);

        if ($renderType == 'json') {
            if ($return == 0) {
                echo $chart->toPrettyString();
            } else {
                return json_encode($resultsArray);
            }
        } elseif ($renderType == 'array') {
            return $resultsArray;
        }
    }

    function reportAccountCountPieByDate($reportYear,$renderType='json',$return=0) {
        $colorChartArray = colorChartArray();
        $reportArray = $this->UserMetrics->reportAccountsPieByDate($reportYear,TRUE);

        $chart = new open_flash_chart();
        $pieData = array();
        $newColorArray = array();
        $a=0;
        $colorIndex=0;
        $completeTotal = 0;
        foreach($reportArray as $reportItem) {
            $total = floatval($reportItem['Accounts']);

            switch ($reportItem['AccountLevel']) {
                case 0:
                    $label = 'Free ('.$total.')';
                    break;
                case 1:
                    $label = 'Solo ('.$total.')';
                    break;
                case 2:
                    $label = 'Shop ('.$total.')';
                    break;
                case 3:
                    $label = 'Team ('.$total.')';
                    break;
            }
            $label = entities_to_quotes(stripslashes($label));
            $tmp = new pie_value($total,"");
            $tmp->set_label($label,'#333333',11);
            if (!isset($colorChartArray[$colorIndex])) {
                $colorIndex=0;
            }
            $newColorArray[$a] = $colorChartArray[$colorIndex];
            $pieData[] = $tmp;
            $a++;
            $colorIndex++;
            $completeTotal = $completeTotal + $total;
        }

        $title = new title('Active accounts ('.$reportYear.')');
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );

        $pie = new pie();
        $pie->radius(100);
        $pie->set_start_angle(80);
        $pie->gradient_fill();
        $pie->add_animation( new pie_fade() );
        $pie->set_tooltip('#label#<br>#val# of #total# (#percent#)');
        $pie->colours($newColorArray);
        $pie->set_values($pieData);

        $t = setChartTooltipStyle();

        $chart = new open_flash_chart();
        $chart->set_title( $title );
        $chart->add_element( $pie );
        $chart->set_tooltip($t);
        $chart->set_bg_colour(CHART_BG_COLOR);

        if ($renderType == 'json') {
            if ($return == 0) {
                echo $chart->toString();
            } else {
                return json_encode($resultsArray);
            }
        } elseif ($renderType == 'array') {
            return $resultsArray;
        }
    }

    function reportAccountIncomePieByDate($reportYear,$renderType='json',$return=0) {
        $colorChartArray = colorChartArray();

        $colorChartArray = colorChartArray();
        $reportArray = $this->UserMetrics->reportAccountsPieByDate($reportYear,TRUE);

        $chart = new open_flash_chart();
        $pieData = array();
        $newColorArray = array();
        $a=0;
        $colorIndex=0;
        $completeTotal = 0;
        $label = '';
        foreach($reportArray as $reportItem) {
            if ($reportItem['AccountLevel'] > 0) {
                $total = floatval($reportItem['TotalPrice']);

                switch ($reportItem['AccountLevel']) {
                    case 1:
                        $label = 'Solo'.Chr(10).'$'.number_format($reportItem['TotalPrice'],2);
                        break;
                    case 2:
                        $label = 'Shop'.Chr(10).'$'.number_format($reportItem['TotalPrice'],2);
                        break;
                    case 3:
                        $label = 'Team'.Chr(10).'$'.number_format($reportItem['TotalPrice'],2);
                        break;
                }
                $label = entities_to_quotes(stripslashes($label));
                $tmp = new pie_value($total,"");
                $tmp->set_label($label,'#333333',11);
                if (!isset($colorChartArray[$colorIndex])) {
                    $colorIndex=0;
                }
                $newColorArray[$a] = $colorChartArray[$colorIndex+1];
                $pieData[] = $tmp;
                $a++;
                $colorIndex++;
                $completeTotal = $completeTotal + $total;
            }
        }

        $title = new title('Account value ('.$reportYear.' $'.number_format($completeTotal,2).')');
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );

        $pie = new pie();
        $pie->radius(100);
        $pie->set_start_angle(110);
        $pie->gradient_fill();
        $pie->add_animation( new pie_fade() );
        $pie->set_tooltip('#label#<br>#val# of #total# (#percent#)');
        $pie->colours($newColorArray);
        $pie->set_values($pieData);

        $t = setChartTooltipStyle();

        $chart = new open_flash_chart();
        $chart->set_title( $title );
        $chart->add_element( $pie );
        $chart->set_tooltip($t);
        $chart->set_bg_colour(CHART_BG_COLOR);

        if ($renderType == 'json') {
            if ($return == 0) {
                echo $chart->toString();
            } else {
                return json_encode($resultsArray);
            }
        } elseif ($renderType == 'array') {
            return $resultsArray;
        }
    }
	
	function createLoginChart() {		
		$chartFile = 'userLogins.png';
		
		/*
		 * Get data 
		 */
		$dataArray = $this->UserMetrics->getDataLoginChart();
		
		// dataSet definition   
		$dataSet = new pData;  
		
		$dataSet->AddPoint($dataArray['data'],'data');  
		$dataSet->AddPoint($dataArray['label'],'label');  
		$dataSet->AddAllSeries();  
		$dataSet->SetAbsciseLabelSerie('label');  

		// Initialise the graph  
		$chart = new pChart(460,270); 
		
		// Draw the pie chart  
		$chart->setFontProperties(FONT_DIR.'arial.ttf',9);
		$chart->setShadowProperties(2,2,200,200,200);  
		$chart->drawBasicPieGraph($dataSet->GetData(),$dataSet->GetDataDescription(),120,140,110,PIE_PERCENTAGE,255,255,255);  
		$chart->drawPieLegend(280,15,$dataSet->GetData(),$dataSet->GetDataDescription(),250,250,250);  
 		$chart->Render(ADMIN_CHART_DIR.$chartFile);
 		echo ADMIN_CHART_PATH.$chartFile;
	}
	
	function createSignupChart() {
		$chartFile = 'userSignups.png';
		
		/*
		 * Get data 
		 */
		$dataArray = $this->UserMetrics->getDataLoginSignup();
		// dataSet definition   
		$dataSet = new pData;  
		$dataSet->AddPoint($dataArray['data'],'Signups');  
		$dataSet->AddPoint($dataArray['label'],'Years');  
		$dataSet->AddSerie('Signups');
		$dataSet->SetAbsciseLabelSerie('Years');  
		$dataSet->SetYAxisName('New signups');  
 
		// Initialise the graph  
		$chart = new pChart(460,270);
 		//$chart->setImageMap(TRUE,"chart1");
		
		$chart->setFontProperties(FONT_DIR.'arial.ttf',9);
		$chart->setGraphArea(80,30,480,230); 
		$chart->drawGraphArea(255,255,255,TRUE);
		$chart->drawScale($dataSet->GetData(),$dataSet->GetDataDescription(),SCALE_START0,0,0,0,TRUE,0,0,TRUE);     
		$chart->drawGrid(4,TRUE);
		
		$chart->drawBarGraph($dataSet->GetData(),$dataSet->GetDataDescription(),FALSE); 
		$chart->Render(ADMIN_CHART_DIR.$chartFile);  
		echo ADMIN_CHART_PATH.$chartFile;
	}
}	
?>