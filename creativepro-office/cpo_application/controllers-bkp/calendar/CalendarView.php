<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CalendarView extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
		$this->load->model('calendar/Calendar_view','',true);
        $this->load->model('clients/Client_view','',true);
        $this->load->model('contacts/Contacts','',true);
        $this->load->model('common/app_data','AppData',TRUE);

		$this->load->library('application/CommonAppData');
        $this->load->library('application/CommonCalendar');
        $this->load->helper('download');

        include(APPPATH.'libraries/icalendar/iCalcreator.class.php');
	}

	function _init($month=NULL,$year=NULL) {
		global $subMenuArray,$colorCSSArray;
		$data['page']                  = 'calendar';
		$data['pageTitle']             = lang('calendar');
		$data['pageSubTitle']          = '';
		$data['wysiwyg']               = 1;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('');
		$data['pageIconClass']         = 'iconPageCalendar';
		$data['pageLayoutClass']       = 'withRightColumn';
		$data['rightColumnComponents'] = array('calendarSidebar','tagRender','feedExportSidebar');
		$data['tagRenderArray']        = $this->commonappdata->renderTags('calendar','array');

        /*
		 * Create tag string for calendar
		 */
		$tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'calendar');
		$data['tags'] = makeTagJSON($tagArray);
        $data['pageButtons'] = array();
        array_push($data['pageButtons'], '<button class="buttonExpand buttonEditMain action" id="buttonAddEvent" title="'.lang('calendar_new').'"><span class="add">'.lang('calendar_new').'</span></button>');
        array_push($data['pageButtons'], '<button class="buttonExpand buttonPrint green" id="buttonprint" title="'.lang('button_print').'"><span class="print">'.lang('button_print').'</span></button>');
        
		$data['calendars']	     = CalendarView::getCalendars($this->session->userdata('userid'),$this->session->userdata('accountUserid'),'json',1);
		$data['agendaDate']      = translateDateString(date('Y-m-d'),'daytext_monthtext_daynum');
		$data['colorCSSArray']   = $colorCSSArray;
        $data['clients']         = $this->Client_view->getClientsForSelect();
        $data['contacts']        = $this->Contacts->getContactsForSelect();

        if (empty($month) && empty($year)) {
            $month = date('n')-1;
            $year = date('Y');
        }

		$data['defaultMonth']    = $month;
		$data['defaultYear']     = $year;

		/*
		 * Create RSS Feed Links
		 */
		$calendarFeedLink  = site_url('rss/Rss').'/createRSSFeed/'.$this->session->userdata('persAuthString').'/calendar';
        $icalFeedLink      = site_url('calendar/CalendarViewExternal/makeIcalendar').'/'.$this->session->userdata('persAuthString');
		$calendarFeedTitle = lang('calendar_your_calendars');

		$data['rssFeeds'] = array(
			array('Link' => $calendarFeedLink, 'Title' => $calendarFeedTitle, 'Type' => 'calendar')
		);

        $data['icalFeeds'] = array(
			array('Link' => $icalFeedLink, 'Title' => $calendarFeedTitle, 'Type' => 'calendar')
		);

		return $data;
	}

	function index($month=NULL,$year=NULL) {
  		$data = CalendarView::_init($month,$year);
		$this->load->view('calendar/CalendarView',$data);
	}

	function getCalendars($userid,$accountUserid,$renderType='array',$return=1) {
        $permArray = $this->session->userdata('permissions');
        if ($permArray['calendarViewAll'] == 1) {
            $viewAll = TRUE;
        } else {
            $viewAll = FALSE;
        }
		$calendarArray = $this->Calendar_view->getCalendars($userid,$accountUserid,$viewAll);
        $newCalendarArray = array();
		
		$calID = '';
		$a=0;
		$b=0;
		$memberString = '';
		foreach($calendarArray as $calendar) {
            if ($calendar['MemberID']>0) {
                $memberString .= $calendar['MemberID'].'|'.$calendar['NameFirst'].' '.$calendar['NameLast'].'|'.$calendar['Language'].'||';
            }
			if ($calendar['CalendarID'] != $calID) {
				$newCalendarArray[$a]['MemberInfo'] = $memberString;
				$newCalendarArray[$a]['CalendarID'] = $calendar['CalendarID'];
				$newCalendarArray[$a]['Color']      = $calendar['Color'];
				$newCalendarArray[$a]['Title']      = $calendar['Title'];
                $newCalendarArray[$a]['Deletable']  = 1;
                if ($calendar['DefaultCalendar'] == 1) {
                    $newCalendarArray[$a]['Deletable'] = 0;
                }
				$calID = $calendar['CalendarID'];
				$memberString = '';
				$a++;
			}
            /*
            if ($calendar['MemberID']>0) {
                $memberString .= $calendar['MemberID'].'|'.$calendar['NameFirst'].' '.$calendar['NameLast'].'|'.$calendar['Language'].'||';
            }
            */
        }

		if ($renderType == 'json') {
			$calendars = json_encode($newCalendarArray);
			if ($return == 1) {
				return $calendars;
			} else {
				echo $calendars;
			}
		} elseif ($renderType == 'array') {
			return $newCalendarArray;
		}
	}

    /**
	 * getEvents : retrieve events for calendars
	 *
	 * @access	public
	 * @param   string  $dateStart   Start date of date range for which to find events.
     *                               Will also serve as the single date for finding events for 1 day.
     * @param   string  $dateEnd     End date of date range for which to find events
     * @param   string  $range       For which format are we finding events (month,week,day)
     * @param   string  $renderType  Return data as PHP array or JSON
     * @param   bool    $return      1 = return results, 0 = echo results
	 * @return	mixed  PHP array of task data or JSON sting
	 */
    function getEvents($dateStart=NULL,$dateEnd=NULL,$range='month') {
        $eventCollection = $this->commoncalendar->getEvents($dateStart,$dateEnd,$range,$this->session->userdata('accountUserid'),'json',1);
        echo $eventCollection;
    }

    function getNumberOfEventsForCalendar($calendarID) {
        $eventCount = $this->Calendar_view->getNumberOfEventsForCalendar($calendarID);
        echo $eventCount['EventCount'];
    }

    function renderDashboardCalendar($month=NULL,$year=NULL,$userType=NULL) {
        if (empty($userType)) {
            $userType = $this->session->userdata('userType');
        }

        if (empty($month)) {
            $month = date('n');
        }
        if (empty($year)) {
            $year = date('Y');
        }

		/*
		 * Determing prev and next months
		 */
		if ($month == 1) {
			$prevMonth = 12;
			$prevYear = $year-1;
		} else {
			$prevMonth = $month-1;
			$prevYear = $year;
		}

		if ($month == 12) {
			$nextMonth = 1;
			$nextYear = $year+1;
		} else {
			$nextMonth = $month+1;
			$nextYear = $year;
		}

		$linkMonth = $month-1;
        /*
         * Set calendar preferences
         */
        $calPrefs['day_type'] = 'abr';
 		$calPrefs['template'] = '
			{table_open}<table class="calendar" style="width: 100%;">{/table_open}
            {heading_title_cell}<th colspan="{colspan}" class="boxBlue" style="padding: 2px;">
				<div style="float: left; width: 10%;"><a class="link-prev changeMonth" href="#" id="widgetCalendarMonthPrev" dateCode="'.$prevMonth.'_'.$prevYear.'"></a></div>
				<div style="float: left; width: 80%; text-align: center;"><a href="'.site_url('calendar/CalendarView').'/index/'.$linkMonth.'/'.$year.'"><strong>{heading}</strong></a></div>
				<div style="float: right;"><a class="link-next changeMonth" href="#" id="widgetCalendarMonthNext" dateCode="'.$nextMonth.'_'.$nextYear.'"></a></div></th>{/heading_title_cell}
			{week_day_cell}<td class="widgetCalendarDayHead" style="width: 14%;">{week_day}</td>{/week_day_cell}
			{cal_cell_start}<td class="DateBox widget dayCell clickable">{/cal_cell_start}
            {cal_cell_content}<span class="dayNum" id="dayNum_{day}">{day}</span><br />{content}{/cal_cell_content}
            {cal_cell_no_content}<span class="dayNum" id="dayNum_{day}">{day}</span>{/cal_cell_no_content}
			{table_close}</table>{/table_close}
		';

		/*
         * Get calendar and event data
         */
        if (strlen($month)<2) {
            $month = '0'.$month;
        }
        $dateStart = $year.'-'.$month.'-01';
        $dateEnd   = $year.'-'.$month.'-'.days_in_month($month,$year);

        $eventCollection = $this->commoncalendar->getEvents($dateStart,$dateEnd,'month',$this->session->userdata('accountUserid'),'array',1);
		//print_r($eventCollection);
		
        $printEventArray = array();
        $a=0;
        foreach($eventCollection as $event) {
            $startDateArray = explode(' ',$event['StartDateTime']);
            $startDateArray = explode('-',$startDateArray[0]);

			$startDay = ltrim($startDateArray[2],0);
			$printEventArray[$a]['startDay']      = $startDay;
			$printEventArray[$a]['eventTitle']    = $event['Title'];
			$printEventArray[$a]['cssClass']      = $event['CssClass'];
			$printEventArray[$a]['Title']         = $event['Title'];
			$printEventArray[$a]['CalendarTitle'] = $event['CalendarTitle'];
			$printEventArray[$a]['StartDateTime'] = $event['StartDateTime'];
			if (isset($event['EndDateTime'])) {
				$printEventArray[$a]['EndDateTime']   = $event['EndDateTime'];
			} else {
				$printEventArray[$a]['EndDateTime'] = '0000-00-00 00:00:00';
			}
			$a++;
        }
        /*
         * Sort printEventArray by day
         */
        function cmp($a, $b) {
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        }
        usort($printEventArray, "cmp");

        $currentDay = 0;
        $currentDayCount = 0;
        $a=0;
        $data = array();
        $eventString = '';
        foreach($printEventArray as $event) {
            $newDay = $event['startDay'];
            if ($newDay != $currentDay) {
                if ($a>0) {
                    $data[$currentDay] = $eventString;
                }
                $eventString = '';
                $currentDayCount = 0;
                $currentDay = $newDay;
            } else {
                $currentDayCount++;
            }
			$dateStartArray = explode(' ',$event['StartDateTime']);
			$unixTimeStart  = strtotime($event['StartDateTime']);
			
			$dateEndArray   = explode(' ',$event['EndDateTime']);
			$unixTimeEnd    = strtotime($event['EndDateTime']);
			if (isset($dateStartArray[1]) && $dateStartArray[1] != '00:00:00') {
				$dateStart = date('M j, g:i A',$unixTimeStart);
			} else {
				$dateStart = date('M j',$unixTimeStart);
			}
			
			if ($dateStartArray[0] != $dateEndArray[0] && $dateEndArray[0] != '0000-00-00') {
				if ($dateEndArray[1] != '00:00:00') {
					$dateEnd = date('M j, g:i A',$unixTimeEnd);
				} else {
					$dateEnd = date('M j',$unixTimeEnd);
				}
				$dateString = $dateStart.' to '.$dateEnd;
			} else {
				$dateString = $dateStart;
			}
			$eventTitle = stripslashes($event['Title']);
			$calTitle   = stripslashes($event['CalendarTitle']);

			$eventDetailString = '<p><strong>'.$eventTitle.'</strong> in '.$calTitle.'<br />'.$dateString.'</p>';
            $eventString .= '<div class="EventWidget colorBlock '.$event['cssClass'].'" style="margin: 1px ;">'.$eventDetailString.'</div>';
            $a++;
        }
        /*
         * Let's pick up the last event because we didn't
         * get it in the loop above
         */
        $data[$currentDay] = $eventString;
        
        $this->load->library('calendar',$calPrefs);
		$calendarArray = CalendarView::getCalendars($this->session->userdata('userid'),$this->session->userdata('accountUserid'),'array',1);

		$options = '';
		foreach($calendarArray as $calendar) {
			$options .= '<option value="'.$calendar['CalendarID'].'">'.$calendar['Title'].'</option>';
		}
		$htmlContent = $this->calendar->generate($year,$month,$data);
		$htmlContent .= '<a id="linkEventAdd" class="icon_calendar_add" style="margin: 6px 0 0 0;" href="#">'.lang('calendar_new').'</a>';
		$htmlContent .= '<input type="hidden" id="widgetCalendarCurrentDate" value="'.$year.'-'.$month.'" />';
		$htmlContent .= '<div style="display: none; margin-top: 6px;" id="formCalendarWidget">
						<input type="hidden" class="eventWhenHiddenWidget" />
						<strong>'.lang('common_when').':</strong><br />
						<input type="text" id="eventDateStartWidget" style="width: 100px;" />
						<strong>&nbsp;at&nbsp;</strong>
						<input type="text" class="clearOnFocus saverField" id="eventTimeStartWidget" tabindex=99" style="width: 50px;" /><br />
						
						<strong>'.lang('common_title').':</strong><br />
						<input type="text" class="saverField" id="eventTitleWidget" tabindex="100" style="width: 287px;" /><br />

						<strong>'.lang('task_description').':</strong><br />
						<textarea class="expanding" id="eventDescriptionWidget" tabindex="101" style="width: 287px; height: 40px;"></textarea><br />

						<strong>'.lang('widget_calendar').':</strong><br />
						<select class="eventCalendarPopup saverField" id="eventCalendarWidget" style="width: 294px;" tabindex="101">'.$options.'</select><br />
						<button id="buttonSaveEvent" class="smallButton" tabindex="300" style="margin: 6px 6px 0 0; float: left;"><span class="save">'.lang('button_save').'</span></button>
						<button id="buttonCancelEvent" class="smallButton" tabindex="301" style="margin-top: 6px; float: left;"><span class="cancel">'.lang('button_cancel').'</span></button>
						<div style="clear: left;"></div>
					</div>';
		echo $htmlContent;
    }

	function makeIcalendar($authKey,$calendarID=NULL) {
        $userid = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF']);
		$error = FALSE;
		if ($calendarID>0) {
			/*
			 * Then just get events for 1 calendar
			 */
			/*
			 * Only get dates that are 30 days in the future.
			 */
			$filename = lang('calendar').'.ics';
			$endDate = new_date_from_days(date('Y-m-d'),365);
			$eventArray = $this->Calendar_view->getCalendarEntryList($calendarID,20,$endDate,$this->session->userdata('accountUserid'));
		} else {
			/*
			 * Get all calendars for this user
			 */
			$filename = lang('calendar').'.ics';

			/*
			 * Only get dates that are 30 days in the future.
			 */
			$endDate = new_date_from_days(date('Y-m-d'),365);
			$eventArray = $this->Calendar_view->getCalendarEntryList(NULL,20,$endDate,$this->session->userdata('accountUserid'));
		}

		if ($error == FALSE) {
            $data  = "BEGIN:VCALENDAR";
            $data .= "\r\nVERSION:2.0";
            $data .= "\r\nCALSCALE:GREGORIAN";
			//$data .= "\r\nPRODID:-//CreativePro Office//Calendar 2.0//".strtoupper($this->session->userdata('language'));
            $data .= "\r\nPRODID:-//Google Inc//Google Calendar 70.9054//EN";
			$data .= "\r\nX-WR-CALNAME: My calendar";
			$data .= "\r\nX-WR-TIMEZONE: US/Eastern";
			$data .= "\r\nX-ORIGINAL-URL: http://www.creativeprooffice.com";
			$data .= "\r\nX-WR-CALDESC: Calendar entries from Jeff";
            $data .= "\r\nMETHOD:PUBLISH";
            if ($calendarID>0) {
                $data .= "\r\nX-WR-CALNAME:Tech Stuff";
            }

			foreach($eventArray as $event) {
                $data .= "\r\nBEGIN:VEVENT";
                $data .= "\r\n".formatIcalData("SUMMARY:".$event['Title']);
                $data .= "\r\n" . formatIcalData("DESCRIPTION:".$event['Description']);

				if ($event['DateEnd'] != '0000-00-00' && $event['DateEnd'] != '0000-00-00 00:00:00') {
					$dateEnd = "\r\nDTEND:".convertMySQLToGMT($event['DateEnd'],'pull','Ymd\THis\Z');
				} else {
					$dateEnd = '';
				}

				$dateStart  = convertMySQLToGMT($event['DateStart'],'pull','Ymd\THis\Z');

                $data .= "\r\nDTSTART:".$dateStart;
                $data .= $dateEnd;
                $data .= "\r\nDTSTAMP:".date('Ymd\THis\Z');
                $data .= "\r\nCREATED:19000101T120000Z";
                $data .= "\r\nUID:".generate_guid()."@creativeprooffice.com";
                //$data .= 'SUMMARY;ENCODING=QUOTED-PRINTABLE:'.$eventTitle.Chr(10);
                //$data .= 'DESCRIPTION;ENCODING=QUOTED-PRINTABLE:'.$eventDescription.Chr(10);
                $data .= "\r\nTRANSP:OPAQUE";
                $data .= "\r\nEND:VEVENT";
			}

            $data .= "\r\nEND:VCALENDAR";
            //iCalHeader($filename);
            echo $data;
			//force_download($filename, $data);
		}
	}

    function getEventGuests($eventID,$renderType='json',$return=0) {
        $guestArray = $this->Calendar_view->getEventGuests($eventID);
        if ($renderType == 'json') {
            $jsonString = json_encode($guestArray);
            if ($return == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        } elseif ($renderType == 'array') {
            return $guestArray;
        }
    }

    function iCalParse() {
        ini_set('error_reporting',E_ALL);
        ini_set('display_errors',1);
        $v = new vcalendar();
          // create a new calendar instance
        $v->setConfig( 'unique_id', 'http://www.mycpohq.com' );
          // set Your unique id, required if any component UID is missing

        $v->setProperty( 'method', 'PUBLISH' );
          // required of some calendar software
        $v->setProperty( "x-wr-calname", "Calendar Sample" );
          // required of some calendar software
        $v->setProperty( "X-WR-CALDESC", "Calendar Description" );
        // required of some calendar software
        $v->setProperty( "X-WR-TIMEZONE", "Europe/Stockholm" );
          // required of some calendar software

        /* start parse of local file */
        //$v->setConfig( 'directory', 'calendar' );
          // set directory
        //$v->setConfig( 'filename', 'file.ics' );
          // set file name
        //$v->parse();

        /* start parse of remote file */
        $v->setConfig( 'url', 'http://www.google.com/calendar/ical/chiefupstart%40gmail.com/public/basic.ics' );
          // iCalcreator also support remote files
        $v->parse();
        $v->sort();

        /*
        while( $comp = $v->getComponent()) {
            echo $comp->summary['value'].' ||| ';
            print_r($comp);
        }
        */

        $events_arr = $v->selectComponents( 2010,10,1,2010,11,30,'vevent');
        foreach( $events_arr as $year => $year_arr ) {
            foreach( $year_arr as $month => $month_arr ) {
                foreach( $month_arr as $day => $day_arr ) {
                    foreach( $day_arr as $event ) {
                        $currddate = $event->getProperty( 'x-current-dtstart' );
                        $startdate = $event->getProperty( 'dtstart' );
                        $summary = $event->getProperty( 'summary' );
                        $description = $event->getProperty( 'description' );
                        print_r($startdate);
                        print_r($summary);
                        echo $currddate.' '.$startdate.' '.$summary.' '.$description.' ||| ';
                    }
                }
            }
        }
    }
}
?>