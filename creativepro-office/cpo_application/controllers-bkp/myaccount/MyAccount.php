<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MyAccount extends Controller {
	function __construct()
	{
		parent::Controller();
        checkPermissionsForController($this->router->fetch_class());
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
		
		$this->load->model('accounts/accounts','',true);

		$this->load->library('application/CommonAppData');
        $this->load->helper('currency');        
    }
	
	function _init() {
		$data['page']                  = 'account';
		$data['pageTitle']             = lang('common_your_account');
		$data['pageSubTitle']          = lang('common_your_account');
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsSettings.js','jsAccount.js');
		$data['pageIconClass']         = 'iconPageClients';
		$data['pageLayoutClass']       = 'withRightColumn';
		$data['rightColumnComponents'] = array();
        
        /*
         * Account plan information
         */
        global $accountLevelArray;
        $accountDesc = array();
        foreach ($accountLevelArray as $account) {
            if ($account['fileSpace'] == 0) {
                $storageSpace = 'None';
            } else {
                $storageSpace = byte_format($account['fileSpace']);
            }

            if ($account['cost'] == 0) {
                $costPerMonth = 'Free!';
            } else {
                //$costPerMonth = '$'.$account['cost'].'.00';
                $costPerMonth = '$'.$data['accountInformation']['AccountPrice'];
            }

            if ($account['projects'] == 100000) {
                $account['projects'] = 'Unlimited';
            }
            if ($account['teamMembers'] == 100000) {
                $account['teamMembers'] = 'Unlimited';
            }

            $accountDescTxt  = '<ul class="check">';
            $accountDescTxt .= '<li>'.lang('plan_cost').': <strong>'.$costPerMonth.'</strong></li>';
            $accountDescTxt .= '<li>'.lang('plan_active_projects').': <strong>'.$account['projects'].'</strong></li>';
            $accountDescTxt .= '<li>'.lang('plan_team').': <strong>'.$account['teamMembers'].'</strong></li>';
            $accountDescTxt .= '<li>'.lang('plan_storage').': <strong>'.$storageSpace.'</strong></li>';
            $accountDescTxt .= '</ul>';
            $accountDesc[$account['name']] = $accountDescTxt;
        }
        $data['accountDescriptions'] = $accountDesc;

        switch ($this->session->userdata('accountLevel')) {
            case 0:
                $data['accountDesc'] = $data['accountDescriptions']['free'];
                $data['accountName'] = 'Free';
                break;
            case 1:
                $data['accountDesc'] = $data['accountDescriptions']['solo'];
                $data['accountName'] = 'Solo';
                break;
            case 2:
                $data['accountDesc'] = $data['accountDescriptions']['shop'];
                $data['accountName'] = 'Shop';
                break;
            case 3:
                $data['accountDesc'] = $data['accountDescriptions']['team'];
                $data['accountName'] = 'Team';
                break;
        }

        return $data;
	}
	
	function index() {
        
		$data = MyAccount::_init();
		$this->load->view('myaccount/MyAccount',$data);
	}
    
    function getAccountPayments($renderType='json',$return=0) {
        $paymentArray = $this->accounts->getAccountPayments($this->session->userdata('accountUserid'));
        
        $a=0;
        foreach($paymentArray as $payment) {
            $paymentArray[$a]['CurrencyMark'] = getCurrencyMark($payment['Currency']);
            $paymentArray[$a]['DatePaymentHuman'] = date('M j, Y',strtotime($payment['DatePayment']));
            $a++;
        }
        
        if ($renderType == 'json') {
            if ($return == 1) {
                return json_encode($paymentArray);
            } else {
                echo json_encode($paymentArray);
            }
        } else {
            return $paymentArray;
        }
    }
}	
?>