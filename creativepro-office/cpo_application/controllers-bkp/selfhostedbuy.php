<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Selfhostedbuy extends Controller {

    function __construct(){
		parent::Controller();

		$this->load->model('common/public_site','',true);
        $this->load->model('accounts/accounts','',true);

        $this->load->library('application/CommonEmail');

        $this->load->helper('account');
        $this->load->helper('date');
        $this->load->helper('payment');
        $this->load->helper('templates');
	}

    function _init($isPaymentSuccess=FALSE) {
        global $monthNameArray;

        $data['child']                     = TRUE;
        $data['page']                      = 'selfhostedbuy';
        if ($isPaymentSuccess == TRUE) {
            $data['headerTextImage']       = 'txtThanksHeader_public.png';
        } else {
            $data['headerTextImage']       = 'txtPaymentHeaderSelfHosted_public.png';
        }
        $data['rightColumnComponents'] = array('paymentInfo');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Payment';

        $data['currentMonth']  = date('m');
        $data['currentYear']   = date('Y');
        $data['months'] = $monthNameArray;

        $whyPayArray = determineWhyPay('selfHosted');
        $data['submitButtonClass'] = $whyPayArray['buttonClass'];

        return $data;
    }

    function index($product=null,$accountID=null,$errorCode=NULL) {
        global $productArray;
        $data = Selfhostedbuy::_init();
        if (!empty($product)) {
            $data['product'] = $productArray[$product];
            $data['productType'] = $product;
        }
        $data['errorMessage'] = '';
        if (!empty($errorCode)) {
            $data['errorMessage'] = getHumanTransactionErrorMessage($errorCode);
        }
        $data['accountID'] = $accountID;
        $this->load->view('public_site/Selfhostedbuy',$data);
    }
}