<?
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProjectTemplates extends Controller {

	function __construct()
	{
		parent::Controller();
        checkPermissionsForController($this->router->fetch_class());
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('projects/project_update','ProjectUpdate',TRUE);
		$this->load->model('projects/project_view','ProjectView',TRUE);
        $this->load->model('projects/project_template','ProjectTemplates',TRUE);
	}

	function _init() {
        $data['page']                  = 'projectTemplates';
		$data['pageTitle']             = lang('menu_project_templates');
        $data['wysiwyg']               = 0;
        $data['charts']                = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsProjects.js');
		$data['pageIconClass']         = 'iconPageProjectTemplates';
		$data['pageLayoutClass']       = 'withRightColumn';
        $data['rightColumnComponents'] = array();
		$data['pageButtons']           = array();

        noCache();
        return $data;
    }

    function index() {
        $data = ProjectTemplates::_init();
		$this->load->view('projects/ProjectTemplates',$data);
    }

    function getProjectTemplates($renderType='json',$return=0) {
        $templateArray = $this->ProjectTemplates->getProjectTemplates($this->session->userdata('accountUserid'));
        if ($renderType == 'json') {
            if ($return == 1) {
                return json_encode($templateArray);
            } else {
                echo json_encode($templateArray);
            }
        } else if ($renderType == 'array') {
            return $templateArray;
        }
    }

    function saveProjectTemplate() {
        /*
         * Save template information
         */
        $templateArray['action']        = $_POST['action'];
        $templateArray['templateID']    = $_POST['templateID'];
        $templateArray['projectID']     = $_POST['projectID'];
        $templateArray['accountUserid'] = $this->session->userdata('accountUserid');
        $templateArray['userid']        = $this->session->userdata('userid');
        $templateArray['Name']          = fieldToDB($this->input->post('templateName', TRUE));
        $templateArray['Desc']          = fieldToDB(strip_tags($this->input->post('templateDesc', TRUE)), TRUE,TRUE,FALSE,TRUE);
        $templateArray['Milestones']    = $_POST['chkMilestones'];
        $templateArray['Tasks']         = $_POST['chkTasks'];
        $templateArray['Contacts']      = $_POST['chkContacts'];
        $templateArray['TeamMembers']   = $_POST['chkTeamMembers'];
        $templateArray['Files']         = $_POST['chkFiles'];
        $templateArray['Expenses']      = $_POST['chkExpenses'];
        $templateArray['Notes']         = $_POST['chkNotes'];


        $templateID = $this->ProjectTemplates->saveProjectTemplate($templateArray);
        echo '{"TemplateID":"'.$templateID.'","Status":"success","Message":"'.lang('project_template_saved').'"}';
    }

    function getProjectTemplate($templateID) {
        $templateArray = $this->ProjectTemplates->getProjectTemplate($templateID);
        echo json_encode($templateArray);
    }

    function deleteTemplate($templateID) {
        $this->ProjectTemplates->deleteTemplate($templateID);
    }
}
?>