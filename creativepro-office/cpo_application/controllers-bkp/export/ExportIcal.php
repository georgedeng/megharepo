<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ExportIcal extends Controller {
	function __construct()
	{
		parent::Controller();
        loadLanguageFiles();

		$this->load->model('calendar/Calendar_view','',true);
        $this->load->model('clients/Client_view','',true);
        $this->load->model('contacts/Contacts','',true);
        $this->load->model('common/app_data','AppData',TRUE);

		$this->load->library('application/CommonAppData');
        $this->load->library('application/CommonCalendar');
        $this->load->helper('download');
	}

    function makeIcalendar($authKey,$calendarID=NULL) {
        $loginArray = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF']);
        $userid        = $loginArray['UID'];
        $accountUserid = $loginArray['UseridAccount'];
        $permissions   = $loginArray['RolePermissions'];
        
		$error = FALSE;
		if ($calendarID>0) {
			/*
			 * Then just get events for 1 calendar
			 */
			/*
			 * Only get dates that are 30 days in the future.
			 */
			$filename = lang('calendar').'.ics';
			$endDate = new_date_from_days(date('Y-m-d'),30);
			$eventArray = $this->Calendar_view->getCalendarEntryList($calendarID,NULL,$endDate,$accountUserid);
		} else {
			/*
			 * Get all calendars for this user
			 */
			$filename = lang('calendar').'.ics';

			/*
			 * Only get dates that are 30 days in the future.
			 */
			$endDate = new_date_from_days(date('Y-m-d'),30);
			$eventArray = $this->Calendar_view->getCalendarEntryList(NULL,NULL,$endDate,$accountUserid);
		}

		if ($error == FALSE) {
            $data  = 'BEGIN:VCALENDAR\r\n';
            $data .= 'PRODID:-//CreativePro Office//Calendar 2.0//'.strtoupper($this->session->userdata('language')).'\r\n';
            $data .= 'VERSION:2.0'.Chr(10);
            $data .= 'CALSCALE:GREGORIAN\r\n';
            $data .= 'METHOD:PUBLISH\r\n';
            if ($calendarID>0) {
                $data .= 'X-WR-CALNAME:Tech Stuff\r\n';
            }

			foreach($eventArray as $event) {
                $data .= 'BEGIN:VEVENT\r\n';

				$eventTitle       = str_replace(',','\,',$event['Title']);
				$eventDescription = str_replace(',','\,',$event['Description']);
                $eventDescription = str_replace('<br />','\n',$eventDescription);

                $eventDescription = DBToField($eventDescription);
                $eventTitle       = DBToField($eventTitle);

                $dateEnd = '';
				if ($event['DateEnd'] != '0000-00-00' && $event['DateEnd'] != '0000-00-00 00:00:00') {
                    $dateEndDateTime = convertMySQLToGMT($event['DateEnd'],'pull','Ymd\THis\Z');
                    if (!empty($dateEndDateTime)) {
                        $dateEnd = 'DTEND:'.$dateEndDateTime.'\r\n';
                    }
                }

				$dateStart  = convertMySQLToGMT($event['DateStart'],'pull','Ymd\THis\Z');

                $data .= 'DTSTART:'.$dateStart.'\r\n';
                $data .= $dateEnd;
                $data .= 'UID:'.random_string('alnum',20).'@creativeprooffice.com\r\n';
                $data .= 'SUMMARY:'.$eventTitle.'\r\n';
                $data .= 'DESCRIPTION:'.$eventDescription.'\r\n';
                $data .= 'END:VEVENT\r\n';
			}

            $data .= 'END:VCALENDAR\r\n';
			force_download($filename, $data);
		}
	}
}
?>
