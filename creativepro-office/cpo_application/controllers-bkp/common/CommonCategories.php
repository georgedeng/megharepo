<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonCategories extends Controller {
	function CommonCategories()
	{
		parent::Controller();
		$this->config->set_item('language',$this->session->userdata('language'));
		if($this->session->userdata('logged') != TRUE) {
			header('Location: '.site_url('Login'));
		}
		$this->load->model('common/app_data','AppData',TRUE);
		$this->load->helper('security');
	}

	function newCategory($itemValue,$itemType) {
		$itemValue = $this->input->xss_clean($itemValue);

		if ($itemType == 'category') {
			$this->AppData->saveCategory(null,$itemValue,'add');
		} elseif ($itemType == 'tag') {
			$this->AppData->saveTag(null,$itemValue,'add');
		}
	}

	function updateCategory($itemID,$itemValue,$itemType) {
		$itemValue = $this->input->xss_clean($itemValue);

		if ($itemType == 'category') {
			$this->AppData->saveCategory($itemID,$itemValue,'edit');
		} elseif ($itemType == 'tag') {
			$this->AppData->saveTag($itemID,$itemValue,'edit');
		}
	}

	function deleteCategories($itemID,$itemType) {
		if ($itemType == 'category') {
			$this->AppData->deleteCategory($itemID);
		} elseif ($itemType == 'tag') {
			$this->AppData->deleteTag($itemID);
		}
	}
}
?>
