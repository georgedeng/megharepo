<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Desktop extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
        $this->load->library('twitter');
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtAboutHeader_public.png';
        $data['tweets']                = $this->twitter->call('statuses/user_timeline', array('id' => 'cpohq'));
        $data['rightColumnComponents'] = array('loginBox','whosUsingCPO','newsFeeds');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'CPO Desktop';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
		$data = Desktop::_init();
        $this->load->view('public_site/Desktop',$data);
    }
}
?>