<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notes extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('notes/Site_notes','',true);
	}

    function saveNote() {
        $noteArray['onWidget'] = $_POST['onWidget'];
        $noteArray['noteID']   = $_POST['noteID'];
		$noteArray['userid']   = $this->session->userdata('userid');
        $noteArray['accountUserid'] = $this->session->userdata('accountUserid');
        $noteArray['itemID']   = $_POST['itemID'];
        $noteArray['itemType'] = $_POST['itemType'];
        $noteArray['note']     = fieldToDB($this->input->post('note', TRUE), TRUE,FALSE,FALSE,FALSE,TRUE,FALSE,TRUE);
        $noteArray['action']   = $_POST['action'];
        $noteArray['public']   = $_POST['makePublic'];
        $this->Site_notes->saveNote($noteArray);
		echo $noteArray['note'];
    }

    function getNotes($onHome=0,$itemID=NULL,$itemType=NULL,$start=0,$limit=5,$renderType='json',$return=0) {
		$noteArray = array();

        $viewPublic = TRUE;
        $client = FALSE;
        if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
            $viewPublic = FALSE;
            $client = TRUE;
        }

        if ($start<0) {
            $start=0;
        }

        $noteArray['Notes'] = $this->Site_notes->getNotes($onHome,$itemID,$itemType,$client,$start,$limit,$this->session->userdata('accountUserid'),$this->session->userdata('userid'),$viewPublic);
		for($a=0;$a<=sizeof($noteArray['Notes'])-1;$a++) {
			$noteArray['Notes'][$a]['DateNote'] = convertMySQLToGMT($noteArray['Notes'][$a]['DateNote'],'pull','M j, g:i a');

            $noteArray['Notes'][$a]['IsEditable'] = '1';
            if ($noteArray['Notes'][$a]['Public'] == 1 && $noteArray['Notes'][$a]['UseridEntry'] != $this->session->userdata('userid')) {
                $noteArray['Notes'][$a]['IsEditable'] = '0';
            }
		}
		$noteArray['MoveBack'] = '';
		$noteArray['MoveForward'] = '';
		if ($start == 0) {
			$noteArray['MoveBack'] = 'off';
		}
		if (count($noteArray['Notes'])<$limit) {
			$noteArray['MoveForward'] = 'off';
		}
        $noteArray['NextStart'] = $start+$limit;
        $noteArray['PrevStart'] = $start-$limit;
        if ($renderType == 'json') {
            $jsonString = json_encode($noteArray);
            if ($return == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        } else {
            return $noteArray;
        }
    }

	function deleteNote($noteID,$tagForDelete=TRUE) {
		$this->Site_notes->deleteNote($noteID,$tagForDelete,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
	}

    function restoreNote($noteID) {
		$this->Site_notes->restoreNote($noteID,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
	}
}
?>