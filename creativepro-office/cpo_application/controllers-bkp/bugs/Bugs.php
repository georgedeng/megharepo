<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bugs extends Controller {
	function __construct()
	{
		parent::Controller();
		loadLanguageFiles();
		//checkAuthentication($_POST);

		$this->load->model('bugs/bug_model','BugModel',true);
        $this->load->model('contacts/contacts','Contacts',true);

        $this->load->library('application/CommonEmail');
        $this->load->library('encrypt');
	}

    function _init() {
        $data['page']                  = 'adminUsers';
		$data['pageTitle']             = 'Administration: Bugs';
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsBugs.js');
		$data['pageIconClass']         = 'iconPageAdministration';
		$data['pageLayoutClass']       = 'withRightColumn';
        $data['rightColumnComponents'] = array('menuAdministration','bugSidebar');

        return $data;
    }

	function index() {
        $data = Bugs::_init();
		$this->load->view('administration/AdminBugs',$data);
	}

    function saveBugReport() {
        $bugContentEmail = $_POST['bugContent'];
        $bugContent = fieldToDB($this->input->post('bugContent', TRUE), TRUE,TRUE,FALSE,TRUE);
        $bugArray['bugFeature'] = $_POST['type'];
        $bugArray['content']    = $bugContent;
        $bugArray['page']       = $_POST['thisPage'];
        $bugArray['browser']    = $this->agent->browser();
        $bugArray['version']    = $this->agent->version();
        $bugArray['platform']   = $this->agent->platform();
        $bugArray['userid']     = $this->session->userdata('userid');
        $bugArray['pid']        = $this->session->userdata('pid');

        if (!empty($bugContent)) {
            $bugID = $this->BugModel->saveBugReport($bugArray);

            $subject = '#'.$bugID.' : CreativePro Office Bug Report';
            $message  = $bugContentEmail.Chr(10).Chr(10);
            $message .= 'On Page: '.$bugArray['page'].Chr(10).Chr(10);
            $message .= 'Submitted on: '.date('M j, Y').Chr(10);
            $message .= 'Submitted by: '.$this->session->userdata('fullName').' from '.$this->session->userdata('userCompany').' <'.$this->session->userdata('userEmail').'>'.Chr(10);
            $message .= 'Browser: '.$this->agent->browser().' '.$this->agent->version().Chr(10);
            $message .= 'Operating System: '.$this->agent->platform().Chr(10);
            $message .= 'Account Level: '.$this->session->userdata('accountLevel').' - '.$this->session->userdata('isPaid').Chr(10).Chr(10);

            $message .= 'To close this bug: '.BASE_URL.'bugs/Bugs/closeBugReport/oiSIoe1ygbT45Ls0AMEO/'.$bugID;

            $this->commonemail->sendEmailToAdmin($message,$subject,'CreativePro Office Bug',$this->session->userdata('userEmail'));
        }
    }

    function closeBugReport() {
        $data['bugID'] = $_POST['bugID'];
        $data['textReworded'] = fieldToDB($this->input->post('rewordedText', TRUE), TRUE,TRUE,FALSE,TRUE);
        $data['bugFeature']   = $_POST['type'];

        /*
         * Check for authenticated user or user with correct permissions.
         */
        if ($this->session->userdata('logged') == 1) {
            $userid = $this->session->userdata('userid');
            $auth = true;
        } else {
            $auth = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF']);
            $userid = $auth;
        }
        if ($auth != false) {
            $data['bugID'];
            $this->BugModel->closeBugReport($data,$userid);
        }

        /*
         * Send email to bug submitter.
         */
        $bugArray = $this->BugModel->getBug($bugID);
        $pID = $bugArray['PID'];
        $subject = 'Closed: CreativePro Office Bug #'.$bugID;
        $message  = 'Hello,'.Chr(10).Chr(10);
        $message .= 'The following bug report has been closed and fixed. If you find that this bug has not been fixed correctly, please submit another bug report.'.Chr(10).Chr(10);
        $message .= '::--------------------------::'.Chr(10);
        $message .= $bugArray['BugText'].Chr(10);
        $message .= '::--------------------------::'.Chr(10);
        $message .= 'Thank you for helping to make CreativePro Office an excellent product!';
        $this->commonemail->sendEmail($message,$pID,'team',$subject,NULL,TRUE);
    }

    function deleteBugReport($bugID) {
        $this->BugModel->deleteBug($bugID);
    }

    function getBugs($open=NULL,$closed=NULL,$renderType='json',$return=0) {
        if ($open == '1') {
            $open = TRUE;
        } else {
            $open = FALSE;
        }
        if ($closed == '1') {
            $closed = TRUE;
        } else {
            $closed = FALSE;
        }
        $bugArray = $this->BugModel->getBugs($open,$closed);

        $a=0;
        foreach($bugArray as $bug) {
            $bugArray[$a]['BugDateEntry']     = date('M j, Y', strtotime($bug['BugDateEntry']));
            $bugArray[$a]['BugDateEntrySort'] = date('m/d/Y', strtotime($bug['BugDateEntry']));
            $bugArray[$a]['BugDateClose']     = date('M j, Y', strtotime($bug['BugDateClose']));
            $bugArray[$a]['BugDateCloseSort'] = date('m/d/Y', strtotime($bug['BugDateClose']));

            if ($closed == 1) {
                $bugCloserArray = $this->Contacts->getContactPerson(NULL,$bug['CloseUserid']);
                if (isset($bugCloserArray['NameFirst'])) {
                    $bugArray[$a]['Closer'] = $bugCloserArray['NameFirst'].' '.$bugCloserArray['NameLast'];
                }
            }
            $a++;
        }

        if ($renderType == 'json') {
            $jsonString = json_encode($bugArray);
            if ($return == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        } elseif ($renderType == 'array') {
            return $bugArray;
        }
    }
}
?>