<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class InvoiceViewExternal extends Controller {
	function __construct()
	{
		parent::Controller();
        
		$this->load->model('common/app_data','AppData',TRUE);
        $this->load->model('clients/Client_view','',TRUE);
		$this->load->model('accounts/Accounts','',TRUE);
		$this->load->model('projects/Project_view','',TRUE);
		$this->load->model('finances/Finance_view','',TRUE);
        $this->load->model('settings/Settings_update','',TRUE);
        $this->load->model('accounts/Accounts','',TRUE);
        $this->load->model('finances/Invoice_update','InvoiceUpdate',TRUE);

        $this->load->library('application/CommonFinance');
        $this->load->library('application/CommonMessages');
        $this->load->library('application/CommonLogin');
	}

    function viewInvoiceExternal($authKey,$invoiceID,$payPalResponse=NULL,$paymentID=NULL) {
        $loginArray = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF']);
		$userid        = $loginArray['UID'];
        $accountUserid = $loginArray['UseridAccount'];
		$langCode      = $loginArray['Language'];
        $userType      = $loginArray['User_Type'];
		loadLanguageFiles($langCode);
        
        /*
         * Get currency mark.
         */
        $accountArray = $this->Accounts->getAccountInformation($accountUserid);
        $this->session->set_userdata('currency', $accountArray['Currency']);

        /*
		 * Do we have rights to view this invoice?
		 */
        $viewAll = TRUE;
        if ($userType == USER_TYPE_TEAM) {
            $permissions   = $loginArray['RolePermissions'];
            $permArray = createPermissionsArray($permissions);
            $clientUserid = 0;
            if ($permArray['invoiceViewOwn'] == 1) {
                $viewAll = FALSE;
            }
        } elseif ($userType == USER_TYPE_CLIENT) {
            $clientUserid = $userid;
        }
        $invoiceRights = $this->Finance_view->checkForValidInvoice($invoiceID,$userid,$accountUserid,$clientUserid,$viewAll);

		$data['errorMessage']    = '';
		$data['page']            = 'external';
		$data['pageTitle']       = lang('finance_view_invoices');
		$data['pageLayoutClass'] = 'withRightColumn';
		$data['pageIconClass']   = 'iconinvoices';
		$data['jsFileArray']     = array('jsInvoiceViewExternal.js');

        $data['pageButtons'] = array();
        array_push($data['pageButtons'], '<button class="buttonExpand green buttonPrint" id="buttonprint" itemID="'.$invoiceID.'" title="'.lang('button_print').'"><span class="print">'.lang('button_print').'</span></button>');
        
        
        if ($this->session->userdata('logged') != TRUE) {
            array_push($data['pageButtons'], '<button class="buttonExpand action buttonLogin" id="buttonlogin" title="'.lang('login_button').'"><span class="login">'.lang('login_button').'</span></button>');
        
        }

		if ($invoiceRights == TRUE) {
            /*
             * Check for PayPal ability
             */
            $settingsArray = $this->Settings_update->getSettings($accountUserid,'emailPayPal');

            if (isset($settingsArray['SettingValue']) && !empty($settingsArray['SettingValue'])) {
                $acctOwnerArray = $this->Accounts->getAccountInformation($accountUserid,1);
                $data['emailPayPal'] = $settingsArray['SettingValue'];
                $data['currencyCode'] = $acctOwnerArray['Currency'];
            } else {
                $data['emailPayPal'] = '';
            }
			$data['invoiceHTML'] = $this->commonfinance->getInvoiceDetails($invoiceID,'html',1,$langCode,null,$accountUserid);
            $data['invoiceID']   = $invoiceID;
 		} else {
			$data['errorMessage'] = lang('error_cant_view_this');
		}

        /*
         * Get any payments for this invoice
         */
        $paymentsArray = $this->commonfinance->getInvoicePayments($invoiceID,'array');
        $data['totalDue'] = $paymentsArray['TotalDue'];
        $data['totalDueWithCurrency'] = $paymentsArray['TotalDueWithCurrency'];
        $data['payments'] = $paymentsArray['Payments'];

        /*
         * Did we make a PayPal payment?
         */
        $data['payPalResponse'] = '';
        if ($payPalResponse == 'cancel') {
            /*
             * Delete payment entry
             */
            $this->InvoiceUpdate->deleteInvoicePayment($paymentID);
            $data['payPalResponse'] = lang('finance_pay_pay_payment_canceled');
        } else {
            /*
             * Try parsing on ? because PayPal might return success?.....
             */
            $successArray = explode('?',$payPalResponse);
            if ((isset($successArray[0]) && $successArray[0] == 'success') || $payPalResponse == 'success') {
                $data['payPalResponse'] = lang('finance_pay_pay_payment_completed');
            }
        }

		$this->load->view('finances/InvoiceViewExternal',$data);
    }

    function makePayPalPayment() {
        $amount        = $_POST['amount'];
        $business      = $_POST['business'];
        $item_name     = $_POST['item_name'];
        $return        = $_POST['payPalReturn'];
        $cancel_return = $_POST['cancel_return'];
        $currency_code = $_POST['currency_code'];
        $invoiceID     = $_POST['invoiceID'];

        /*
         * Enter payment for this invoice
         */
        $payArray['payInvoiceAccountNumber'] = '';
        $payArray['payInvoiceAmount']    = $amount;
        $payArray['payInvoiceDate']      = convertMySQLToGMT(date('Y-m-d'),$direction='push',$format='Y-m-d');
        $payArray['payInvoiceStatus']    = '';
        $payArray['payComments']         = lang('finance_pay_pal_payment_received');
        $payArray['invoiceID']           = $invoiceID;
        $payArray['userid']              = '';
        $payArray['payInvoicePaymentType'] = 'paypal';
        $payArray['payStatus']             = 0;
        $payArray['invoiceStatus']         = 0;
        $paymentID = $this->InvoiceUpdate->saveInvoicePayment($payArray);

        $payPalParams  = 'cmd=_xclick&';
        $payPalParams .= 'currency_code=USD&';
        $payPalParams .= 'tax=0&';

        $payPalParams .= 'business='.$business.'&';
        $payPalParams .= 'item_name='.rawurlencode($item_name).'&';
        $payPalParams .= 'amount='.$amount.'&';        
        $payPalParams .= 'return='.rawurlencode($return.'/'.$paymentID).'&';
        $payPalParams .= 'currency_code='.rawurlencode($currency_code).'&';
        $payPalParams .= 'cancel_return='.rawurlencode($cancel_return.'/'.$paymentID);

        $payPalURL = PAY_PAL_URL.'?'.$payPalParams;
        echo $payPalURL;
    }

    function sendRecurringInvoices() {
        $invoiceArray = $this->Finance_view->getRecurringInvoices();
        
        foreach ($invoiceArray as $invoice) {
            $invoiceArrayNew = array();
            $daysFromSignup = days_between_dates(strtotime($invoice['DateSignup']),time());
            $dayText  = date('D');
            $dayNum   = date('j');
            $monthNum = date('m');
            $monthDay = $monthNum.'-'.$dayNum;
            
            if ($daysFromSignup <= 31 || $invoice['IsPaid'] == 1) {
                /*
                 * Figure out what kind of recurring invoice we got here.
                 * Weekly, Monthly, Yearly
                 */
                $recurringArray = explode('|',$invoice['Recurring']);
                $recurringDateArray = explode(',',$recurringArray[1]);
                if ($recurringArray[0] == 'week') {
                    foreach ($recurringDateArray as $recurringDate) {
                        if (!empty($recurringDate) && $recurringDate == $dayText) {
                            $newInvoiceID = $this->commonfinance->copyInvoice($invoice['InvoiceID']);
                        }
                    }
                } else if ($recurringArray[0] == 'month') {
                    foreach ($recurringDateArray as $recurringDate) {
                        if (!empty($recurringDate) && $recurringDate == $dayNum) {
                            $newInvoiceID = $this->commonfinance->copyInvoice($invoice['InvoiceID']);
                        }
                    }
                } else if ($recurringArray[0] == 'year') {
                    foreach ($recurringDateArray as $recurringDate) {
                        if (!empty($recurringDate) && $recurringDate == $monthDay) {
                            $newInvoiceID = $this->commonfinance->copyInvoice($invoice['InvoiceID']);
                        }
                    }
                }
            }
        }
    }
}
?>