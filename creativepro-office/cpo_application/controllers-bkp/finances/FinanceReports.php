<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FinanceReports extends Controller {
    function __construct() {
        parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('finances/Finance_reports','',true);
        include(APPPATH.'libraries/OpenFlashCharts/open-flash-chart.php');

        $this->load->helper('chart');
    }

    function reportInvoicedReceived($reportYear,$renderType='json',$return=0,$chartDataOnly=0) {
        $colorChartArray = colorChartArray();
        $currencyMark = getCurrencyMark($this->session->userdata('currency'));

        $reportArray = $this->Finance_reports->getReportDataInvoicedReceived($this->session->userdata('accountUserid'),$reportYear);
        $invoicedArray = $reportArray['Invoiced'];
        $receivedArray = $reportArray['Received'];

        $invoicedOFCData = array();
        $receivedOFCData = array();
        $xLabelOFCData   = array();
        
        $resultsArray = array();
        $b=0;

        $totalInvoiced = 0;
        $totalReceived = 0;
        for($a=1;$a<=12;$a++) {
            $gotMonth = FALSE;
            $resultsArray['Invoiced'][$b]['MonthNum'] = $a;
            foreach($invoicedArray as $invoiced) {
                if ($invoiced['MonthNum'] == $a) {
                    $totalInvoicedNum = $invoiced['TotalInvoiced'];
                    $resultsArray['Invoiced'][$b]['Total'] = $currencyMark.$invoiced['TotalInvoiced'];
                    $invoicedOFCData[$b] = floatval($invoiced['TotalInvoiced']);
                    $gotMonth = TRUE;
                }
            }

            if ($gotMonth == FALSE) {
                $resultsArray['Invoiced'][$b]['Total'] = "0";
                $invoicedOFCData[$b] = 0;
            }
            $totalInvoiced += $invoicedOFCData[$b];
            $resultsArray['Invoiced'][$b]['Month'] = date('M',mktime(0, 0, 0, $a, 1, date('Y')));
            $resultsArray['Invoiced'][$b]['Month'] = lang('cal_'.strtolower($resultsArray['Invoiced'][$b]['Month']));
            $xLabelOFCData[$b] = lang('cal_'.strtolower($resultsArray['Invoiced'][$b]['Month']));
            $b++;
        }

        $b=0;
        for($a=1;$a<=12;$a++) {
            $gotMonth = FALSE;
            $resultsArray['Received'][$b]['MonthNum'] = $a;
            foreach($receivedArray as $received) {
                if ($received['MonthNum'] == $a) {
                    $totalReceivedNum = $received['TotalReceived'];
                    $resultsArray['Received'][$b]['Total'] = $currencyMark.$received['TotalReceived'];
                    $receivedOFCData[$b] = floatval($received['TotalReceived']);
                    $gotMonth = TRUE;
                }
            }

            if ($gotMonth == FALSE) {
                $resultsArray['Received'][$b]['Total'] = "0";
                $receivedOFCData[$b] = 0;
            }
            $totalReceived += $receivedOFCData[$b];
            $resultsArray['Received'][$b]['Month'] = date('M',mktime(0, 0, 0, $a, 1, date('Y')));
            $resultsArray['Received'][$b]['Month'] = lang('cal_'.strtolower($resultsArray['Received'][$b]['Month']));
            $b++;
        }
        $resultsArray['Title'] = lang('reports_invoiced_received').' ('.$reportYear.')';
        $resultsArray['Legend'] = array(lang('reports_invoiced'),lang('reports_received'));
        $resultsArray['TotalInvoiced'] = $currencyMark.number_format($totalInvoiced,2);
        $resultsArray['TotalReceived'] = $currencyMark.number_format($totalReceived,2);

        /*
         * Open Flash Charts
         */
        $chart = new open_flash_chart();
        $title = new title(lang('reports_invoiced_received').' ('.$reportYear.')');
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );
        $chart->set_title( $title );
        $chart->set_bg_colour('#FFFFFF');        

        $bar = new bar_glass();
        $bar->colour($colorChartArray[0]);
        $bar->key(lang('reports_invoiced'), 12);
        $bar->set_values($invoicedOFCData);
        $bar->set_tooltip($currencyMark.'#val# '.lang('reports_invoiced'));
        $bar->set_on_show(new bar_on_show('fade-in',0,0));

        $bar2 = new bar_glass();
        $bar2->colour($colorChartArray[1]);
        $bar2->key(lang('reports_received'), 12);
        $bar2->set_values($receivedOFCData);
        $bar2->set_tooltip($currencyMark.'#val# '.lang('reports_received'));
        $bar2->set_on_show(new bar_on_show('fade-in',0,0));

        $x_labels = new x_axis_labels();
        $x_labels->set_colour( '#333' );

        $x = new x_axis();
        $x->set_labels_from_array($xLabelOFCData);
        $x->set_grid_colour('#eeeeee');
        $chart->set_x_axis( $x );
        
        $chart->add_element( $bar );
        $chart->add_element( $bar2 );        

        $x_labels = new x_axis_labels();

        $y = new y_axis();
        $yMaxInvoiced = max($invoicedOFCData);
        $yMaxReceived = max($receivedOFCData);

        if ($yMaxInvoiced>$yMaxReceived) {
            $yMax = $yMaxInvoiced+20;
        } else {
            $yMax = $yMaxReceived+20;
        }
        $yInc = ceil($yMax/10);

        $y->set_range(0,$yMax,$yInc);
        $y->set_grid_colour('#eeeeee');
        //$y->set_label_text( $currencyMark."#val#" );
        $y->set_label_text( "#val#" );

        $t = setChartTooltipStyle();        

        $chart->set_y_axis($y);
        $chart->set_tooltip($t);
        $resultsArray['HeaderLabels'] = lang('common_month').'|'.lang('reports_invoiced').'|'.lang('reports_received');
        if ($renderType == 'json') {
            if ($return == 0) {
                if ($chartDataOnly == 1) {
                    $outToClient = $chart->toString();
                } else {
                    $outToClient  = '{"ChartData":'.$chart->toString();
                    $outToClient .= ',"GridData":'.json_encode($resultsArray).'}';
                }
                echo utf8_encode($outToClient);
            } else {
                return json_encode($resultsArray);
            }
        } elseif ($renderType == 'array') {
            return $resultsArray;
        }
    }

    function reportInvoicedByProjectClient($byWhat,$renderType='json',$return=0) {
        global $colorCSSArray, $colorChartArray;
        $currencyMark = getCurrencyMark($this->session->userdata('currency'));

        $reportDateStart = jsDateToMySQL($this->input->post('reportDateStart', TRUE));
        $reportDateEnd   = jsDateToMySQL($this->input->post('reportDateEnd', TRUE));

        $reportArray = $this->Finance_reports->getReportDataInvoicedByProjectClient($this->session->userdata('accountUserid'),$reportDateStart,$reportDateEnd,$byWhat);

        $chart = new open_flash_chart();
        if ($byWhat == 'project') {
            $titleText = lang('reports_invoiced_by_project');
        } elseif ($byWhat == 'client') {
            $titleText = lang('reports_invoiced_by_client');
        }
        $title = new title($titleText.' ('.MySQLDateToJS($reportDateStart).' to '.MySQLDateToJS($reportDateEnd).')');
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );        

        $pieData = array();
        $newColorArray = array();
        $a=0;
        $b=0;
        $colorIndex=0;

        $grandTotal = 0;
        foreach($reportArray as $reportItem) {
            $grandTotal += $reportItem['Total'];            
        }

        $otherTotal = null;
        $otherLabel = '<br>';
        foreach($reportArray as $reportItem) {
            $total = floatval($reportItem['Total']);
            $itemPercentage = number_format(($reportItem['Total']/$grandTotal)*100,1);
            $reportArray[$b]['Percentage'] = $itemPercentage.'%';
            $reportArray[$b]['Total'] = $currencyMark.number_format($total,2);

            if ($total>0) {
                if (strlen($reportItem['Label'])>20) {
                    $label = substr($reportItem['Label'],0,17).'...';
                } else {
                    $label = $reportItem['Label'];
                }
                //$label = stripslashes(entities_to_quotes($label)).' ('.$currencyMark.number_format($total,2).')';
                $label = stripslashes(entities_to_quotes($label)).' ('.number_format($total,2).')';
                if (sizeof($reportArray)>5 && $itemPercentage<5) {
                    $otherTotal += $total;
                    $otherLabel .= $label.'<br>';
                } else {
                    $tmp = new pie_value($total,"");
                    $tmp->set_label($label,'#333333',11);
                    $tmp->set_tooltip('#label#<br>'.$currencyMark.'#val# of '.$currencyMark.$grandTotal.' ('.$itemPercentage.'%)');
                    if (!isset($colorChartArray[$colorIndex])) {
                        $colorIndex=0;
                    }
                    $newColorArray[$a] = $colorChartArray[$colorIndex];
                    $pieData[] = $tmp;
                    $a++;
                    $colorIndex++;
                }
            }
            $b++;
        }

        $pie = new pie();
        if ($otherTotal != null) {
            $tmp = new pie_value($otherTotal,"");
            $tmp->set_label(lang('common_other'),'#333333',11);
            $tmp->set_tooltip($otherLabel);
            $newColorArray[$a] = $colorChartArray[$colorIndex];
            $pieData[] = $tmp;
        }
        $pie->set_start_angle(60);
        $pie->gradient_fill();
        $pie->add_animation( new pie_fade() );
        $pie->colours($newColorArray);
        $pie->set_values($pieData);

        $t = setChartTooltipStyle();

        $chart = new open_flash_chart();
        $chart->set_title( $title );
        $chart->add_element( $pie );
        $chart->set_tooltip($t);
        $chart->set_bg_colour('#FFFFFF');

        if ($renderType == 'json') {
            if ($return == 0) {
                $outToClient  = '{"ChartData":'.$chart->toString();
                $outToClient .= ',"GridData":'.json_encode($reportArray);
                $outToClient .= ',"GrandTotal":"'.$currencyMark.number_format($grandTotal,2).'"}';
                echo $outToClient;
            } else {
                return json_encode($reportArray);
            }
        } elseif ($renderType == 'array') {
            return $reportArray;
        }
    }

    function reportIncomeExpenses($reportYear,$renderType='json',$return=0) {
        $colorChartArray = colorChartArray();
        $currencyMark = getCurrencyMark($this->session->userdata('currency'));

        $reportArray = $this->Finance_reports->getReportDataIncomeExpenses($this->session->userdata('accountUserid'),$reportYear);
        $expensesArray = $reportArray['Expenses'];
        $incomeArray   = $reportArray['Income'];

        $expensesOFCData = array();
        $incomeOFCData = array();
        $xLabelOFCData   = array();

        $resultsArray = array();
        $b=0;
        $totalIncome = 0;
        $totalExpenses = 0;
        for($a=1;$a<=12;$a++) {
            $gotMonth = FALSE;
            $resultsArray['Expenses'][$b]['MonthNum'] = $a;
            foreach($expensesArray as $expense) {
                if ($expense['MonthNum'] == $a) {
                    $totalExpensesNum = $expense['TotalExpenses'];
                    $resultsArray['Expenses'][$b]['Total'] = $currencyMark.$expense['TotalExpenses'];
                    $expensesOFCData[$b] = floatval($expense['TotalExpenses']);
                    $gotMonth = TRUE;
                }
            }

            if ($gotMonth == FALSE) {
                $totalExpensesNum = 0;
                $resultsArray['Expenses'][$b]['Total'] = "0";
                $expensesOFCData[$b] = 0;
            }
            $totalExpenses += $totalExpensesNum;
            $resultsArray['Expenses'][$b]['Month'] = date('M',mktime(0, 0, 0, $a, 1, date('Y')));
            $resultsArray['Expenses'][$b]['Month'] = lang('cal_'.strtolower($resultsArray['Expenses'][$b]['Month']));
            $xLabelOFCData[$b] = lang('cal_'.strtolower($resultsArray['Expenses'][$b]['Month']));
            $b++;
        }

        $b=0;
        for($a=1;$a<=12;$a++) {
            $gotMonth = FALSE;
            $resultsArray['Income'][$b]['MonthNum'] = $a;
            foreach($incomeArray as $income) {
                if ($income['MonthNum'] == $a) {
                    $totalIncomeNum = $income['TotalIncome'];
                    $resultsArray['Income'][$b]['Total'] = $currencyMark.$income['TotalIncome'];
                    $incomeOFCData[$b] = floatval($income['TotalIncome']);
                    $gotMonth = TRUE;
                }
            }

            if ($gotMonth == FALSE) {
                $totalIncomeNum = 0;
                $resultsArray['Income'][$b]['Total'] = "0";
                $incomeOFCData[$b] = 0;
            }
            $totalIncome += $totalIncomeNum;
            $resultsArray['Income'][$b]['Month'] = date('M',mktime(0, 0, 0, $a, 1, date('Y')));
            $resultsArray['Income'][$b]['Month'] = lang('cal_'.strtolower($resultsArray['Income'][$b]['Month']));
            $b++;
        }
        $resultsArray['Title'] = lang('reports_income_expenses').' ('.$reportYear.')';
        $resultsArray['Legend'] = array(lang('finance_expenses'),lang('reports_income'));
        $resultsArray['TotalExpenses'] = $currencyMark.number_format($totalExpenses,2);
        $resultsArray['TotalIncome'] = $currencyMark.number_format($totalIncome,2);

        /*
         * Open Flash Charts
         */
        $chart = new open_flash_chart();
        $title = new title(lang('reports_income_expenses').' ('.$reportYear.')');
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );
        $chart->set_title( $title );
        $chart->set_bg_colour('#FFFFFF');

        $bar = new bar_glass();
        $bar->colour($colorChartArray[0]);
        $bar->key(lang('finance_expenses'), 12);
        $bar->set_values($expensesOFCData);
        $bar->set_tooltip($currencyMark.'#val# '.lang('finance_expenses'));
        $bar->set_on_show(new bar_on_show('fade-in',0,0));

        $bar2 = new bar_glass();
        $bar2->colour($colorChartArray[1]);
        $bar2->key(lang('reports_income'), 12);
        $bar2->set_values($incomeOFCData);
        $bar2->set_tooltip($currencyMark.'#val# '.lang('reports_income'));
        $bar2->set_on_show(new bar_on_show('fade-in',0,0));

        $x_labels = new x_axis_labels();
        $x_labels->set_colour( '#333' );

        $x = new x_axis();
        $x->set_labels_from_array($xLabelOFCData);
        $x->set_grid_colour('#eeeeee');
        $chart->set_x_axis( $x );

        $chart->add_element( $bar );
        $chart->add_element( $bar2 );

        $x_labels = new x_axis_labels();

        $y = new y_axis();
        $yMaxExpenses = max($expensesOFCData);
        $yMaxIncome = max($incomeOFCData);

        if ($yMaxExpenses>$yMaxIncome) {
            $yMax = $yMaxExpenses+20;
        } else {
            $yMax = $yMaxIncome+20;
        }
        $yInc = ceil($yMax/10);

        $y->set_range(0,$yMax,$yInc);
        $y->set_grid_colour('#eeeeee');
        //$y->set_label_text( $currencyMark."#val#" );
        $y->set_label_text( "#val#" );

        $t = setChartTooltipStyle();

        $chart->set_y_axis($y);
        $chart->set_tooltip($t);

        $resultsArray['HeaderLabels'] = lang('common_month').'|'.lang('reports_income').'|'.lang('finance_expenses');
        if ($renderType == 'json') {
            if ($return == 0) {
                if ($chartDataOnly == 1) {
                    $outToClient = $chart->toString();
                } else {
                    $outToClient  = '{"ChartData":'.$chart->toString();
                    $outToClient .= ',"GridData":'.json_encode($resultsArray).'}';
                }
                echo $outToClient;
            } else {
                return json_encode($resultsArray);
            }
        } elseif ($renderType == 'array') {
            return $resultsArray;
        }
    }

    function reportExpensesBySegment($byWhat,$renderType='json',$return=0) {
        global $colorCSSArray, $colorChartArray;
        $currencyMark = getCurrencyMark($this->session->userdata('currency'));

        $reportDateStart = jsDateToMySQL($this->input->post('reportDateStart', TRUE));
        $reportDateEnd   = jsDateToMySQL($this->input->post('reportDateEnd', TRUE));

        $reportArray = $this->Finance_reports->getReportDataExpensesBySegment($this->session->userdata('accountUserid'),$reportDateStart,$reportDateEnd,$byWhat);

        $chart = new open_flash_chart();
        if ($byWhat == 'project') {
            $titleText = lang('reports_expenses_by_project');
        } elseif ($byWhat == 'client') {
            $titleText = lang('reports_expenses_by_client');
        } elseif ($byWhat == 'vendor') {
            $titleText = lang('reports_expenses_by_vendor');
        } elseif ($byWhat == 'category') {
            $titleText = lang('reports_expenses_by_category');
        }
        $title = new title($titleText.' ('.MySQLDateToJS($reportDateStart).' to '.MySQLDateToJS($reportDateEnd).')');
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );

        $pieData = array();
        $newColorArray = array();
        $a=0;
        $b=0;
        $colorIndex=0;

        $grandTotal = 0;
        foreach($reportArray as $reportItem) {
            $grandTotal += $reportItem['Total'];
        }

        $otherTotal = null;
        $otherLabel = '<br>';
        foreach($reportArray as $reportItem) {
            $total = floatval($reportItem['Total']);
            $itemPercentage = number_format(($reportItem['Total']/$grandTotal)*100,1);
            $reportArray[$b]['Percentage'] = $itemPercentage.'%';
            $reportArray[$b]['Total'] = $currencyMark.number_format($total,2);

            if ($total>0) {
                if (strlen($reportItem['Label'])>20) {
                    $label = substr($reportItem['Label'],0,17).'...';
                } else {
                    $label = $reportItem['Label'];
                }
                //$label = stripslashes(entities_to_quotes($label)).' ('.$currencyMark.number_format($total,2).')';
                $label = stripslashes(entities_to_quotes($label)).' ('.number_format($total,2).')';

                if (sizeof($reportArray)>5 && $itemPercentage<5) {
                    $otherTotal += $total;
                    $otherLabel .= $label.'<br>';
                } else {
                    $tmp = new pie_value($total,"");
                    $tmp->set_label($label,'#333333',11);
                    $tmp->set_tooltip('#label#<br>'.$currencyMark.'#val# of '.$currencyMark.$grandTotal.' ('.$itemPercentage.'%)');
                    if (!isset($colorChartArray[$colorIndex])) {
                        $colorIndex=0;
                    }
                    $newColorArray[$a] = $colorChartArray[$colorIndex];
                    $pieData[] = $tmp;
                    $a++;
                    $colorIndex++;
                }
            }
            $b++;
        }

        $pie = new pie();
        if ($otherTotal != null) {
            $tmp = new pie_value($otherTotal,"");
            $tmp->set_label(lang('common_other'),'#333333',11);
            $tmp->set_tooltip($otherLabel);
            $newColorArray[$a] = $colorChartArray[$colorIndex];
            $pieData[] = $tmp;
        }
        
        $pie->set_start_angle(60);
        $pie->gradient_fill();
        $pie->add_animation( new pie_fade() );
        $pie->colours($newColorArray);
        $pie->set_values($pieData);

        $t = setChartTooltipStyle();

        $chart = new open_flash_chart();
        $chart->set_title( $title );
        $chart->add_element( $pie );
        $chart->set_tooltip($t);
        $chart->set_bg_colour('#FFFFFF');

        if ($renderType == 'json') {
            if ($return == 0) {
                $outToClient  = '{"ChartData":'.$chart->toString();
                $outToClient .= ',"GridData":'.json_encode($reportArray);
                $outToClient .= ',"GrandTotal":"'.$currencyMark.number_format($grandTotal,2).'"}';
                echo $outToClient;
            } else {
                return json_encode($reportArray);
            }
        } elseif ($renderType == 'array') {
            return $reportArray;
        }
    }
}
?>