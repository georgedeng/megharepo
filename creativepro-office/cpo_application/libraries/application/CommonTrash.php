<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonTrash {
    function countItemsInTrash($return=FALSE) {
        $CI =& get_instance();
        $permissions = $CI->session->userdata('permissions');
        
        if ($permissions['trashViewAll'] == 1) {
            $trashTotal = $CI->Trash->getNumberOfItemsInTrash(NULL,$CI->session->userdata('accountUserid'));
        } else {
            $trashTotal = $CI->Trash->getNumberOfItemsInTrash($CI->session->userdata('userid'),NULL);
        }
        $CI->session->set_userdata('trashTotal',$trashTotal);
        if ($return == FALSE) {
            echo $trashTotal;
        } else {
            return $trashTotal;
        }
    }
}
?>