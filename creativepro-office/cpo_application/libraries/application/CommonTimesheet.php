<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonTimesheet {	
	function getTimesheetRecords($dateStart=NULL,$dateEnd=NULL,$userid=NULL,$accountUserid=NULL,$clientID=NULL,$projectID=NULL,$taskID=NULL,$renderType='json',$return=1,$report=0,$orderFor='entry') {
		$CI =& get_instance();
        $CI->load->model('timesheets/Timesheet_view','',true);

		$CI->lang->load('app',$CI->session->userdata('language'));

        /*
         * Do we have permission to view all timesheet records?
         * If so, a null userid means we want all timesheet records for
         * all team members.
         * Else, a null userid means we just want the records of the user
         * who's logged in.
         */
        $permissionArray = $CI->session->userdata('permissions');
		$viewAll = TRUE;

		if ($permissionArray['timesheetViewOwn'] == 1) {
            $viewAll = FALSE;
		}
        if (empty($userid) && $viewAll == FALSE) {
			$userid = $CI->session->userdata('userid');
		}

		/*
		 * Check for dates in the POST collection
		 */
		if (!empty($_POST['dateStart'])) {
			$dateStart = jsDateToMySQL($_POST['dateStart']);
		}
		if (!empty($_POST['dateEnd'])) {
			$dateEnd = jsDateToMySQL($_POST['dateEnd']);
		}
		$timeSheetEntryArray = $CI->Timesheet_view->getTimesheetRecords($dateStart,$dateEnd,$userid,$accountUserid,$clientID,$projectID,$taskID,$report,$orderFor,$viewAll);
        if ($renderType == 'array') {
			if ($report == 1) {
				return CommonTimesheet::createTimesheetReport($timeSheetEntryArray);
			} else {
				return $timeSheetEntryArray;			
			}
		} elseif ($renderType == 'json' && $return == 1) {
			if ($timeSheetEntryArray == false) {
				return false;
			} else {
				if ($report == 1) {
					return decode_utf8(json_encode(CommonTimesheet::createTimesheetReport($timeSheetEntryArray)));
				} else {
					return decode_utf8(json_encode($timeSheetEntryArray));
				}
			}
		} elseif ($renderType == 'json') {
			if ($timeSheetEntryArray == false) {
				echo '{}';
			} else {
				if ($report == 1) {
					echo decode_utf8(json_encode(CommonTimesheet::createTimesheetReport($timeSheetEntryArray)));
				} else {
					echo decode_utf8(json_encode($timeSheetEntryArray));
				}
			}
		}
	}

    function getTimesheetRecord($timesheetID,$renderType='json',$return=1) {
        $CI =& get_instance();
        $CI->load->model('timesheets/Timesheet_view','',true);

		$CI->lang->load('app',$CI->session->userdata('language'));

		if (empty($userid)) {
			$userid = $CI->session->userdata('userid');
		}
        $timeSheetEntryArray = $CI->Timesheet_view->getTimesheetRecord($timesheetID);
        if ($renderType == 'json') {
            $timesheetString = json_encode($timeSheetEntryArray);

            if ($return == 1) {
                return $timesheetString;
            } else {
                echo $timesheetString;
            }
        } else {
            return $timeSheetEntryArray;
        }
    }

	function createTimesheetReport($timeSheetEntryArray) {
		$CI =& get_instance();
		$CI->load->model('timesheets/Timesheet_view','',true);
		$CI->load->model('tasks/Task_view','TaskView',true);

		$CI->lang->load('app',$CI->session->userdata('language'));
		
		
		$clientID  = '';
		$projectID = '';

		$reportArray = array();
		$j=-1;
		$totalReportHours = 0;
		$totalProjectReportHours = 0;

        if (is_array($timeSheetEntryArray) && count($timeSheetEntryArray)>0) {
			$reportArray['Headers']['Task']       = lang('task_comments');
			$reportArray['Headers']['TeamMember'] = lang('team_member');
			$reportArray['Headers']['Billable']   = lang('common_billable');
			$reportArray['Headers']['Hours']      = lang('common_hours');
			$reportArray['Headers']['Date']       = lang('common_date');
			foreach($timeSheetEntryArray as $timeSheetEntry) {
                if (!empty($timeSheetEntry['ProjectTitle'])) {
                    if ($timeSheetEntry['ClientID'] != $clientID) {
                        $j++;
                        $clientID = $timeSheetEntry['ClientID'];
                        $reportArray['Clients'][$j]['ClientID'] = $clientID;
                        $reportArray['Clients'][$j]['Company']  = $timeSheetEntry['Company'];
                        $i=0;
                    }
                    if ($timeSheetEntry['ProjectID'] != $projectID) {
                        $projectID = $timeSheetEntry['ProjectID'];
                        $reportArray['Clients'][$j]['Projects'][$i]['ProjectID']    = $timeSheetEntry['ProjectID'];
                        $reportArray['Clients'][$j]['Projects'][$i]['ProjectTitle'] = $timeSheetEntry['ProjectTitle'];
                        $reportArray['Clients'][$j]['Projects'][$i]['ProjectTotalHours'] = 0;

                        /*
                         * Get total hours and estimated hours for this project
                         */
                        $totalHours = $CI->Timesheet_view->getTotalHours('project',$projectID);
                        $totalEstimatedHours = $CI->TaskView->getEstimatedTimeForProjectTasks($projectID);

                        if ($totalHours>0) {
                            $reportArray['Clients'][$j]['Projects'][$i]['TotalHours'] = $totalHours;
                        } else {
                            $reportArray['Clients'][$j]['Projects'][$i]['TotalHours'] = 0;
                        }
                        if ($totalEstimatedHours>0) {
                            $reportArray['Clients'][$j]['Projects'][$i]['EstimatedHours'] = $totalEstimatedHours;
                        } else {
                            $reportArray['Clients'][$j]['Projects'][$i]['EstimatedHours'] = 0;
                        }
                        $g=$i;
                        $h=0;
                        $i++;
                    }
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['TimeSheetID']    = $timeSheetEntry['TimeSheetID'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['Userid']         = $timeSheetEntry['Userid'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['TaskID']         = $timeSheetEntry['TaskID'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['TaskTitle']      = $timeSheetEntry['TaskTitle'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['ElapsedTime']    = $timeSheetEntry['ElapsedTime'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['Comments']       = $timeSheetEntry['Comments'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['Billable']       = $timeSheetEntry['Billable'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['DateClockStart'] = $timeSheetEntry['DateClockStart'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['TeamMember']     = $timeSheetEntry['NameFirst'].' '.$timeSheetEntry['NameLast'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['TeamMemberID']   = $timeSheetEntry['PID'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['TeamMemberRate'] = $timeSheetEntry['Rate'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['InvoiceID']      = $timeSheetEntry['InvoiceID'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['InvoiceNo']      = $timeSheetEntry['InvNo'];
                    $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['InvoiceDate']    = date('M j, Y', strtotime($timeSheetEntry['DateEntry']));
                    if (empty($timeSheetEntry['DateClockEnd']) || $timeSheetEntry['DateClockEnd'] == '0000-00-00 00:00:00') {
                        $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['DateClockEnd']   = date('M j, Y', strtotime($timeSheetEntry['DateClockStart']));
                    } else {
                        $reportArray['Clients'][$j]['Projects'][$g]['Entries'][$h]['DateClockEnd']   = date('M j, Y', strtotime($timeSheetEntry['DateClockEnd']));
                    }
                    $totalReportHours = $totalReportHours+$timeSheetEntry['ElapsedTime'];
                    $reportArray['Clients'][$j]['Projects'][$g]['ProjectTotalHours'] = $reportArray['Clients'][$j]['Projects'][$g]['ProjectTotalHours']+$timeSheetEntry['ElapsedTime'];
                    $h++;
                }
			}
			$reportArray['Totals']['TotalHours'] = $totalReportHours;
		}
		return $reportArray;
	}

}
?>