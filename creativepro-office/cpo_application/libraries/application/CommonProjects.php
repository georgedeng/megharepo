<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonProjects {
     /**
	 * getTask : retrieve task information for view or edit
	 *
	 * @access	public
	 * @param	int  $taskID ID of the task for which we want data
	 * @return	mixed  PHP array of task data or JSON sting
	 */
     function renderProject($projectArray,$renderType=NULL) {
        $projectArray['ProjectTime'] = number_format($project['ProjectTime'],2);

		/*
		 * Project status
		 */
		$statusArray = getStatus($project['Status'],$project['Date_Start'],$project['Date_End']);
		$projectArray['StatusHuman']   = $statusArray[0];
		$projectArray['StatusMachine'] = $statusArray[1];
		$projectArray['StatusText']    = $statusArray[2];

		/*
		 * Project date range
		 */
		if ($projectArray['Date_Start'] != '0000-00-00') {
			$projectArray['Date_Start'] = date('M j, Y', strtotime($projectArray['Date_Start']));
			$projectArray['Date_Start_Sort'] = date('m/d/Y', strtotime($projectArray['Date_Start']));
		} else {
			$projectArray['Date_Start'] = '';
			$projectArray['Date_Start_Sort'] = '';
		}

		if ($projectArray['Date_End'] != '0000-00-00') {
			$projectArray['Date_End'] = date('M j, Y', strtotime($projectArray['Date_End']));
			$projectArray['Date_End_Sort'] = date('m/d/Y', strtotime($projectArray['Date_End']));
		} else {
			$projectArray['Date_End'] = '';
			$projectArray['Date_End_Sort'] = '';
		}
		if (!empty($projectArray['Date_Start']) && !empty($projectArray['Date_End']) ) {
			$projectArray['Date_Range'] = $projectArray['Date_Start'].' to '.$projectArray['Date_End'];
		} elseif (!empty($projectArray['Date_Start'])) {
			$projectArray['Date_Range'] = $projectArray['Date_Start'];
		} elseif (!empty($projectArray['Date_End'])) {
			$projectArray['Date_Range'] = $projectArray['Date_End'];
		} else {
			$projectArray['Date_Range'] = lang('common_not_specified');
		}

		/*
		 *
		 */
		$projectArray['Description'] = addslashes($projectArray['Description']);

		/*
		 * Project hours
		 */
		if (empty($projectArray['ProjectTime'])) {
			$projectArray['ProjectTime'] = 0;
		}

		/*
		 * Number of invoices and messages for this project
		 */
		if (($invoiceTotalArray = $this->Finance_view->getInvoiceTotals($projectArray['ProjectID'],'project'))>0) {
			$invoiceCountArray = $this->Finance_view->getInvoiceCount($projectArray['ProjectID'],'project');

			$projectArray['InvoiceTotal']      = $invoiceTotalArray['Total'];
			$projectArray['InvoiceTotalHuman'] = getCurrencyMark($this->session->userdata('currency')).number_format($invoiceTotalArray['Total'],2);
			$projectArray['InvoiceCount']      = $invoiceCountArray['InvoiceCount'];
		} else {
			$projectArray['InvoiceTotal']      = 0;
			$projectArray['InvoiceTotalHuman'] = 0;
			$projectArray['InvoiceCount']      = 0;
		}

		/*
		 * Number of tasks for this project
		 */
		$taskTotalArray = $this->Task_view->getNumberOfTasks($projectArray['ProjectID'],'project','incomplete');
		if ($taskTotalArray['Tasks']>0) {
			$projectArray['TaskTotal']          = $taskTotalArray['Tasks'];
			$projectArray['TaskEstimatedHours'] = $taskTotalArray['HoursEstimated'];
		} else {
			$projectArray['TaskTotal']          = '0';
			$projectArray['TaskEstimatedHours'] = '0';
		}
		$jsonString .= json_encode($project).',';
		$totalHours = $totalHours+$project['ProjectTime'];
     }
}
?>