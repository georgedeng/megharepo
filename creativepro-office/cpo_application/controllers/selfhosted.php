<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Selfhosted extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
        $this->load->model('common/self_hosted','',true);
        $this->load->library('twitter');
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtSelfHostedHeader_public.png';
        $data['tweets']                = $this->twitter->call('statuses/user_timeline', array('id' => 'cpohq'));
        $data['rightColumnComponents'] = array('selfHostedInfo_nobg','whosUsingCPO_nobg');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Self-hosted, source code option';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(6);
        return $data;
    }

    function index() {
		$data = Selfhosted::_init();
        $this->load->view('public_site/Selfhosted',$data);
    }

    function faq() {
        $data = Selfhosted::_init();
        $this->load->view('public_site/Selfhostedfaq',$data);
    }

    function paymentCancel() {
        $data = Selfhosted::_init();
        $this->load->view('public_site/Selfhostedpaymentcancel',$data);
    }

    function paymentSuccess() {
        $data = Selfhosted::_init();
        $data['firstPayment'] = true;
        $this->load->view('public_site/Selfhosteddownloads',$data);
    }

    function checkVersion() {
        $versionArray = $this->self_hosted->checkVersion();
        $versionNumber = str_replace('.','',$versionArray['VersionNumber']);
        echo $versionNumber;
    }

    function checkInstall($orderNumber,$installKey) {
        if (empty($orderNumber) || empty($installKey)) {
            /*
             * Check POST collection.
             */
            $orderNumber = $_POST['orderNumber'];
            $installKey  = $_POST['installKey'];
        }
        $checkArray = $this->self_hosted->checkInstall($orderNumber,$installKey);

        if (!$checkArray) {
            echo '{"message":"Could not locate Order Number or Installation Key.","status":"error"}';
        } else {
            if ($checkArray['NumberOfLicenses'] == $checkArray['NumberOfLicensesInstalled']) {
                echo '{"message":"Maximum number of licenses installed.","status":"error"}';
            } else {
                echo '{"message":"Good to go!","status":"success"}';
            }
        }
    }

    function updateInstallRecord($orderNumber,$installKey) {
        if (empty($orderNumber) || empty($installKey)) {
            /*
             * Check POST collection.
             */
            $orderNumber = $_POST['orderNumber'];
            $installKey  = $_POST['installKey'];
        }
        $installArray = $this->self_hosted->checkInstall($orderNumber,$installKey);

        $ipAddresses =  $installArray['InstallationIPAddress'].$_SERVER['REMOTE_ADDR'].'|';
        $installedLicenses = $installArray['NumberOfLicensesInstalled']+1;

        $data['OrderNumber'] = $orderNumber;
        $data['InstallKey']  = $installKey;
        $data['IPAddresses'] = $ipAddresses;
        $data['orderNumber'] = $orderNumber;
        $data['installKey']  = $installKey;
        $data['NumberOfLicensesInstalled'] = $installedLicenses;

        $this->self_hosted->updateInstallRecord($data);
    }

    function checkForUpdates() {
        $orderNumber = $_POST['orderNumber'];
        $installKey  = $_POST['installKey'];
        $versionNumber = $_POST['versionNumber'];

        $versionNumber = str_replace('.','',$versionNumber);
        $updateArray = $this->self_hosted->checkForUpdates($versionNumber);

        $newVersionNumber = $updateArray['VersionNumber'];
        $newVersionNumberHuman = $updateArray['VersionNumberHuman'];

        if ($updateArray != FALSE) {
            $updateFilename = $updateArray['UpdateFilename'];
            echo '{"message":"There are updates available.","filename":"'.$updateFilename.'","versionNumber":"'.$newVersionNumber.'","versionNumberHuman":"'.$newVersionNumberHuman.'","status":"success"}';
        } else {
            echo '{"message":"There are no updates available right now.","status":"error"}';
        }
    }

    function buyoptions() {
        $data = Selfhosted::_init();
        $this->load->view('public_site/SelfHosted_buyoptions',$data);
    }
}
?>