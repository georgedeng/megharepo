<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class InvoiceUpdate extends Controller {
	function __construct()
	{
		parent::Controller();
        checkPermissionsForController($this->router->fetch_class());
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('common/app_data','AppData',TRUE);
		$this->load->model('finances/invoice_update','InvoiceUpdate',TRUE);
		$this->load->model('finances/finance_view','FinanceView',TRUE);
        $this->load->model('finances/expense_update','ExpenseUpdate',TRUE);
		$this->load->model('clients/client_view','Clients',TRUE);
        $this->load->model('clients/client_update','ClientUpdate',TRUE);
        $this->load->model('projects/project_view','Projects',TRUE);
        $this->load->model('tasks/task_view','Tasks',TRUE);
		$this->load->model('timesheets/timesheet_view','TimesheetView',TRUE);
        $this->load->model('timesheets/timesheet_update','TimesheetUpdate',TRUE);
        $this->load->model('settings/settings_update','SettingsUpdate',TRUE);
        $this->load->model('users/user_login','UserLogin',TRUE);

		$this->load->library('application/CommonFinance');
        $this->load->library('application/CommonAppData');
	}

	function _init($invoiceID=NULL,$clientID=NULL,$projectID=NULL) {
		$data['page']              = 'finances';
		$data['pageTitle']         = $this->lang->line('finance_page_title');
		$data['pageSubTitle']      = $this->lang->line('finance_invoice_new');
        $data['pageTitleLink']     = site_url('finances/FinanceView');
		$data['wysiwyg']           = 0;
		$data['map']               = 0;
		$data['jsFileArray']       = array('jsFinances.js','jsInvoiceUpdate.js','jsExpenses.js');
		$data['pageIconClass']     = 'iconinvoices';
		$data['pageLayoutClass']   = 'withRightColumn';
		$data['rightColumnComponents'] = array('financeUpdateInvoice');
		$data['currencyMark']         = getCurrencyMark($this->session->userdata('currency'));
        $data['teamMembers']          = $this->Contacts->getContacts($this->session->userdata('accountUserid'),NULL,NULL,TRUE);

		/*
		 * Create validation messages
		 */
		$messageArray = array(
						'clientID'     => lang('form_required_client'),
						'projectTitle' => lang('form_required_project_title')
						);
		$data['validationJSON'] = json_encode($messageArray);

		/*
		 * Create tag string for invoices
		 */
		$tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'invoice');
		$data['tags'] = makeTagJSON($tagArray);

		/*
		 * Get client list for select box
		 */
		$data['clients'] = $this->Clients->getClientsForSelect();
		
        /*
		 * Get invoice category list
         */
        $data['categories'] = $this->AppData->getCategories($this->session->userdata('accountUserid'),'invoice');
		$data['catJSON']    = makeCategoryJSON($data['categories']);
		$data['lastInvoiceNumber'] = '';

		/*
		 * Set some default field values to be overwritten if we're editing
		 */
        $today = convertMySQLToGMT(date('Y-m-d H:i:s'),$direction='pull',$format='Y-m-d');
		$data['invoiceDate'] = MySQLDateToJS($today);
		$data['invoiceSubTotal'] = '0.00';
		$data['shippingAmount']  = '0.00';
		$data['discountAmount']  = '0.00';
		$data['taxAmount']       = '0.00';
		$data['invoiceTotal']    = '0.00';
        $data['invoiceItemCount']= 0;
        $data['nextInvoiceNumber'] = '';
        $data['recurringRange']    = '';
        $data['recurringDays']     = array();

        if ($invoiceID>0) {
            /*
             * We're editing, retrieve and format invoice data
             */

            /*
             * See if we are supposed to see this invoice
             */
            $viewAll = FALSE;
            $permArray = $this->session->userdata('permissions');
            if ($permArray['invoiceViewAll'] == 1) {
                $viewAll = TRUE;
            }

            if (!$this->FinanceView->checkForValidInvoice($invoiceID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'),NULL,$viewAll)) {
                header('Location: '.site_url('finances/FinanceView'));
            } else {
                $data['action'] = 'edit';
                $data['invoiceInformation'] = $this->commonfinance->getInvoiceDetails($invoiceID,'array');
                $invoiceDate = $data['invoiceInformation'][0]['DateInvoiceOriginal'];

                if ($data['invoiceInformation'][0]['Tags'] == '0') {
                    $data['invoiceInformation'][0]['Tags'] = '';
                }

                $data['invoiceDate'] = convertMySQLToGMT($invoiceDate,$direction='pull',$format='m/d/Y');

				$data['paymentDueDate'] = MySQLDateToJS($data['invoiceInformation'][0]['PaymentDueDate']);

				$data['invoiceSubTotal'] = $data['invoiceInformation'][0]['InvSubTotal'];
				$data['shippingAmount']  = $data['invoiceInformation'][0]['ShippingAmount'];
				$data['discountAmount']  = $data['invoiceInformation'][0]['DiscountAmount'];
				$data['taxAmount']       = $data['invoiceInformation'][0]['TaxAmount'];
				$data['invoiceTotal']    = $data['invoiceInformation'][0]['InvTotal'];
                $data['invMessage']      = DBToField($data['invoiceInformation'][0]['InvComments']);
                $data['lastInvoiceNumberString'] = '';
                $data['clientEmail']     = $data['invoiceInformation'][0]['ClientEmail'];

                $data['invoiceItemCount']= count($data['invoiceInformation'][0]['InvoiceItems']);
                if (!empty($data['invoiceInformation'][0]['Recurring'])) {
                    $recurringArray = explode('|',$data['invoiceInformation'][0]['Recurring']);
                    $data['recurringRange'] = $recurringArray[0];
                    $recurringDayArray = explode(',',$recurringArray[1]);
                    $a=0;
                    foreach($recurringDayArray as $day) {
                        if (!empty($day)) {
                            $data['recurringDays'][$a] = $day;
                            $a++;
                        }
                    }
                }
                /*
                 * Parse recurring invoice information
                 */
            }
            $data['pageSubTitle'] = lang('finance_invoice_edit');

        } else {
			/*
			 * Get last used invoice number
			 */
			$lastInvoiceNumber = $this->FinanceView->getLastInvoiceNumber($this->session->userdata('accountUserid'));
			$data['lastInvoiceNumberString'] = '('.lang('finance_last_invoice_number').' '.$lastInvoiceNumber.')';
            $data['lastInvoiceNumber'] = $lastInvoiceNumber;
            if (is_numeric($data['lastInvoiceNumber'])) {
                $data['nextInvoiceNumber'] = $lastInvoiceNumber+1;
            }    

            /*
             * Get any account-wide settings for invoices
             */
            $settingsArray = $this->SettingsUpdate->getSettings($this->session->userdata('accountUserid'),'','account','invoice');
            if ($settingsArray != FALSE) {
                foreach($settingsArray as &$setting) {
                    if ($setting['Setting'] == 'emailPayPal') {
                        $data['emailPayPal'] = $setting['SettingValue'];
                    }
                    $setting['SettingValue'] = DBToField($setting['SettingValue']);
                }
            }

            $data['settingsJSON'] = json_encode($settingsArray);

            $data['invoiceInformation'] = null;
            $data['invMessage']         = null;
            $data['action']             = 'add';
            $data['pageSubTitle']       = lang('finance_invoice_new');
			$data['paymentDueDate']     = null;
			$data['invoiceSubTotal']    = null;
			$data['shippingAmount']     = null;
			$data['discountAmount']     = null;
			$data['taxAmount']          = null;
			$data['invoiceTotal']       = null;
            $data['clientEmail']        = null;
        }

        if ($clientID>0) {
            $clientArray = $this->Clients->getClientInformation($clientID);
            $data['invoiceInformation'][0]['ClientID'] = $clientArray['ClientID'];
            $data['invoiceInformation'][0]['ClientCompany'] = $clientArray['Company'];
        }

        noCache();
		return $data;
	}

	function index($invoiceID=NULL,$clientID=NULL,$projectID=NULL) {
        $data = InvoiceUpdate::_init($invoiceID,$clientID,$projectID);
        $data['projectID'] = $projectID;
        $data['financeType'] = 'invoice';
        $data['estimate'] = false;
        $data['emailButtonText'] = $this->lang->line('finance_invoice_email');
		$this->load->view('finances/InvoiceUpdate',$data);
	}
    
    function estimate() {
        $data = InvoiceUpdate::_init($invoiceID,$clientID,$projectID);
        $data['pageSubTitle'] = $this->lang->line('menu_new_estimate');
        $data['projectID'] = $projectID;
        $data['estimate'] = true;
        $data['financeType'] = 'estimate';
        $data['emailButtonText'] = $this->lang->line('finance_estimate_email');
		$this->load->view('finances/InvoiceUpdate',$data);
    }

	function saveInvoice() {
        if (isset($_POST['save']) && $_POST['save'] == 1) {
			if ($this->form_validation->run() == FALSE)	{
				$data = InvoiceUpdate::_init();
				$this->load->view('finances/InvoiceUpdate',$data);
			} else {
                /*
				 * Save data
				 */
                $invoiceArray['clientID'] = 0;
                $invoiceArray['ToUID']    = '';
                $invoiceArray['ToType']   = '';
                                
                /*
                 * Are we saving a new client by chance?
                 */
                if (empty($_POST['clientIDHolder'])) {
                    /*
                     * Then save the new client name
                     */
                    //$clientCompany = $this->input->post('clientID', TRUE);
                    $clientCompany = fieldToDB($this->input->post('clientID', TRUE));
                    $clientArray = array(
                        'userid'        => $this->session->userdata('accountUserid'),
                        'clientCompany' => $clientCompany,
                        'action'        => 'add',
                        'clientCategory'=> '',
                        'clientAddress' => '',
                        'clientCity'    => '',
                        'clientState'   => '',
                        'clientZip'     => '',
                        'clientCountry' => '',
                        'clientPhone1'  => '',
                        'clientPhone2'  => '',
                        'clientPhone3'  => '',
                        'clientEmail'   => '',
                        'clientURL'     => '',
                        'clientNotes'   => '',
                        'clientArchive' => '',
                        'clientStatus'  => '',
                        'clientIndustry'=> '',
                        'clientSince'   => '',
                        'clientTimezone'=> '',
                        'clientLanguage'=> '',
                        'clientTags'    => ''
                    );
                    $invoiceArray['clientID'] = $this->ClientUpdate->saveClient($clientArray);

                    /*
                     * Save client login information
                     */
                    $loginArray['action']        = 'add';
                    $loginArray['Userid']        = '';
                    $loginArray['Password']      = '';
                    $loginArray['RoleID']        = '';
                    $loginArray['UserType']      = USER_TYPE_CLIENT;
                    $loginArray['UseridAccount'] = $this->session->userdata('accountUserid');

                    $loginUID = $this->UserLogin->saveLoginInformation($loginArray);

                    /*
                     * Update client table with ClientUserid
                     */
                    $clientUseridArray['action']       = 'update';
                    $clientUseridArray['clientUserid'] = $loginUID;
                    $clientUseridArray['clientID']     = $invoiceArray['clientID'];
                    $newClientID = $this->ClientUpdate->saveClient($clientUseridArray);

                    $_POST['projectID'] = NULL;
                } else {
                    $invoiceArray['clientID'] = $_POST['clientIDHolder'];
                }
                /*
                 * Get client userid
                 */
                if ($invoiceArray['clientID']>0) {
                    $clientArray = $this->Clients->getClientInformation($invoiceArray['clientID']);
                    $invoiceArray['ToUID']  = $clientArray['ClientUserid'];
                    $invoiceArray['ToType'] = 'client';
                }

                $invoiceArray['invoiceID']      = $_POST['invoiceID'];
				$invoiceArray['action']         = $_POST['action'];
                $invoiceArray['accountUserid']  = $this->session->userdata('accountUserid');
                $invoiceArray['userid']         = $this->session->userdata('userid');
				$invoiceArray['invoiceEmail']   = $_POST['invoiceEmail'];
				$invoiceArray['invoicePrint']   = $_POST['invoicePrint'];				
				$invoiceArray['projectID']      = $_POST['projectID'];
				$invoiceArray['invoiceNumber']  = $this->input->post('invoiceNumber', TRUE);
                
                $invoiceArray['estimate'] = 0;
                if ($_POST['financeType'] == 'estimate') {
                    $invoiceArray['estimate'] = 1;
                }
                
				$invoiceArray['invoiceDate']    = null;
                $invoiceArray['estimateDate']   = null;
				if (!empty($_POST['invoiceDate'])) {
					$invoiceArray['invoiceDate'] = jsDateToMySQL($this->input->post('invoiceDate', TRUE));
                    $invoiceArray['estimateDate'] = jsDateToMySQL($this->input->post('invoiceDate', TRUE));
                } else {
                    $invoiceArray['invoiceDate'] = date('Y-m-d');
                    $invoiceArray['estimateDate'] = date('Y-m-d');
                }

				$invoiceArray['invoicePONumber']= $this->input->post('invoicePONumber', TRUE);
				$invoiceArray['invoiceTitle']   = fieldToDB($this->input->post('invoiceTitle', TRUE));
                $invoiceArray['invoiceMessage'] = fieldToDB($this->input->post('invoiceMessage', TRUE), TRUE,TRUE,FALSE,TRUE);
                
                if (isset($_POST['printMessage']) && $_POST['printMessage'] == 1) {
                    $invoiceArray['printMessage'] = 1;
                } else {
                    $invoiceArray['printMessage'] = 0;
                }
				$invoiceArray['invoiceTags'] = fieldToDB($this->input->post('invoiceTagsHolder', TRUE));
                $invoiceArray['invoiceTags'] = str_replace(',',' ',$invoiceArray['invoiceTags']);                

                /*
                 * Invoice category
                 */
                $invoiceArray['invoiceCategory'] = $this->commonappdata->saveCategory(fieldToDB($this->input->post('invoiceCategoryCombo', TRUE)),'invoice');
                
                /*
                 * Recurring invoice stuff
                 */
                $invoiceArray['invoiceRecurring'] = '';
                if ($_POST['invoiceRecurringWhen'] != '0') {
                    if ($_POST['invoiceRecurringWhen'] == 'week') {
                        $invoiceArray['invoiceRecurring'] = 'week|';
                        if ($_POST['recurringWeekSU'] == 'Sun') { $invoiceArray['invoiceRecurring'] .= $_POST['recurringWeekSU'].','; }
                        if ($_POST['recurringWeekMO'] == 'Mon') { $invoiceArray['invoiceRecurring'] .= $_POST['recurringWeekMO'].','; }
                        if ($_POST['recurringWeekTU'] == 'Tue') { $invoiceArray['invoiceRecurring'] .= $_POST['recurringWeekTU'].','; }
                        if ($_POST['recurringWeekWE'] == 'Wed') { $invoiceArray['invoiceRecurring'] .= $_POST['recurringWeekWE'].','; }
                        if ($_POST['recurringWeekTH'] == 'Thu') { $invoiceArray['invoiceRecurring'] .= $_POST['recurringWeekTH'].','; }
                        if ($_POST['recurringWeekFR'] == 'Fri') { $invoiceArray['invoiceRecurring'] .= $_POST['recurringWeekFR'].','; }
                        if ($_POST['recurringWeekSA'] == 'Sat') { $invoiceArray['invoiceRecurring'] .= $_POST['recurringWeekSA'].','; }
                    } elseif ($_POST['invoiceRecurringWhen'] == 'month') {
                        $invoiceArray['invoiceRecurring'] = 'month|';
                        for($a=0;$a<=5;$a++) {
                            if ($_POST['recurringMonthDay'.$a] > 0) {
                                $invoiceArray['invoiceRecurring'] .= $_POST['recurringMonthDay'.$a].',';
                            }
                        }
                    } elseif ($_POST['invoiceRecurringWhen'] == 'year') {
                        $invoiceArray['invoiceRecurring'] = 'year|';
                        for($a=0;$a<=5;$a++) {
                            if ($_POST['recurringYearMonth'.$a] > 0 && $_POST['recurringYearDay'.$a] > 0) {
                                $invoiceArray['invoiceRecurring'] .= $_POST['recurringYearMonth'.$a].'-'.$_POST['recurringYearDay'.$a].',';
                            }
                        }
                    }
                }
				/*
				 * Invoice details
				 */
				$invoiceArray['invoiceTaxRate']       = $this->input->post('invoiceTaxRate', TRUE);
				$invoiceArray['invoiceTaxName']       = $this->input->post('invoiceTaxName', TRUE);
				$invoiceArray['invoiceTaxID']         = $this->input->post('invoiceTaxID', TRUE);
				$invoiceArray['invoiceShipping']      = $this->input->post('invoiceShipping', TRUE);
				$invoiceArray['invoicePaymentDue']    = $_POST['invoicePaymentDue'];

                if ($invoiceArray['invoicePaymentDue']>0 && $invoiceArray['invoicePaymentDue'] != 'other') {
                    $invoiceArray['invoicePaymentDueDate'] = new_date_from_days($invoiceArray['invoiceDate'],$invoiceArray['invoicePaymentDue']);
                } elseif ($invoiceArray['invoicePaymentDue'] == '0') {
                    $invoiceArray['invoicePaymentDueDate'] = $invoiceArray['invoiceDate'];
                } else {
                    $invoiceArray['invoicePaymentDueDate'] = jsDateToMySQL($this->input->post('invoicePaymentDueDate', TRUE));
                }

				$invoiceArray['invoiceLateFee']       = $_POST['invoiceLateFee'];
				$invoiceArray['invoiceLateFeeOther']  = $this->input->post('invoiceLateFeeOther', TRUE);
				$invoiceArray['invoiceDiscount']      = $this->input->post('invoiceDiscount', TRUE);

				/*
				 * Invoice bottom line totals
				 */
				$invoiceArray['amountSubTotal'] = $this->input->post('amountSubTotal', TRUE);
				$invoiceArray['amountDiscount'] = $this->input->post('amountDiscount', TRUE);
				$invoiceArray['amountShipping'] = $this->input->post('amountShipping', TRUE);
				$invoiceArray['amountTax']      = $this->input->post('amountTax', TRUE);
				$invoiceArray['invoiceTotal']   = $this->input->post('invoiceTotal', TRUE);

				/*
				 * Invoice items
				 */
				$invoiceItemCount = $_POST['invoiceItemCount'];
				$invoiceArray['items'] = array();
				
				for($a=0;$a<=$invoiceItemCount;$a++) {
					if ((isset($_POST['total_'.$a]) && $_POST['total_'.$a] > 0) || $_POST['qty_'.$a]>0 || !empty($_POST['desc_'.$a])) {
						$invoiceArray['items'][$a]['desc'] = fieldToDB($this->input->post('desc_'.$a, TRUE), TRUE,TRUE,FALSE,TRUE);
						$invoiceArray['items'][$a]['type'] = $_POST['type_'.$a];
						$invoiceArray['items'][$a]['qty']  = $this->input->post('qty_'.$a, TRUE);
						$invoiceArray['items'][$a]['rate'] = $this->input->post('rate_'.$a, TRUE);
                        if (isset($_POST['tax_'.$a])) {
                            $invoiceArray['items'][$a]['tax']  = $_POST['tax_'.$a];
                        } else {
                            $invoiceArray['items'][$a]['tax'] = '';
                        }

                        /*
                         * Are we entering an expense or timesheet entry?
                         */
                        $invoiceArray['items'][$a]['timesheetID'] = 0;
                        $invoiceArray['items'][$a]['expenseID']   = 0;
                        if (!empty($_POST['extra_'.$a])) {
                            $extraArray = explode('_',$_POST['extra_'.$a]);
                            if ($extraArray[0] == 'timesheet') {
                                $invoiceArray['items'][$a]['timesheetID'] = $extraArray[1];
                            } elseif ($extraArray[0] == 'expense') {
                                $invoiceArray['items'][$a]['expenseID'] = $extraArray[1];
                            }
                        }

                        $itemTotal = $this->input->post('total_'.$a, TRUE);
                        /*
                         * Not sure why number_format($itemTotal,2,'.','') doesn't work here...but it doesn't
                         */
						$invoiceArray['items'][$a]['total'] = str_replace(',','',$itemTotal);
					}
				}
                $invoiceID = $this->InvoiceUpdate->saveInvoice($invoiceArray);
                
                /*
                 * Update timesheet and expense entries as billed. 
                 * Clear any existing billed items first.
                 */
                $this->TimesheetUpdate->markTimesheetsUnbilled($invoiceID);
                $this->ExpenseUpdate->markExpensesUnbilled($invoiceID);
                
                foreach($invoiceArray['items'] as $item) {
                    if ( $item['timesheetID'] > 0 ) {
                        $this->TimesheetUpdate->markTimesheetBilled($item['timesheetID'], $invoiceID);
                    }
                    if ( $item['expenseID'] > 0 ) {
                        $this->ExpenseUpdate->markExpenseBilled($item['expenseID'], $invoiceID);
                    }
                }

				if ($_POST['invoiceEmail'] == 1) {
					/*
                     * Email this invoice
                     */
                    $emailArray['emailAddress']   = $this->input->post('emailInvoiceTo', TRUE);
                    $emailArray['invoiceMessage'] = $this->input->post('emailInvoiceMessage', TRUE);
                    $emailArray['language']       = $this->input->post('clientLanguage', TRUE);
                    $emailArray['invoiceID']      = $invoiceID;
                    $emailArray['invoiceNumber']  = $invoiceArray['invoiceNumber'];
                    
                    $emailArray['sendCopyToUser'] = $_POST['emailInvoiceCopyToUser'];
                    $emailArray['invoicePayPalLink'] = $_POST['emailInvoicePayPalLink'];
                    $emailInvoiceDetails = $this->commonfinance->sendInvoiceEmail($emailArray);
				}
				if ($_POST['invoicePrint'] == 1) {
                    $printInvoice = 1;
				} else {
                    $printInvoice = 0;
                }

                /*
                 * Save tags
                 */
                if (!empty($invoiceArray['invoiceTags'])) {
                    $this->commonappdata->saveTags($invoiceArray['invoiceTags'],'invoice');
                }
                
                /*
                 * Log activity
                 */
                $activity = 'create';
                if ($projectArray['action'] != 'add') {
                    $activity = 'update';
                }
                $title = $invoiceArray['invoiceNumber'].' '.$invoiceArray['invoiceTitle'];
                $data = array(
                    'AccountUserid' => $this->session->userdata('accountUserid'),
                    'UserPID' => $this->session->userdata('pid'),
                    'ProjectID' => $invoiceArray['projectID'],
                    'UserType' => $this->session->userdata('userType'),
                    'Activity' => $activity,
                    'ItemID' => $invoiceID,
                    'ItemType' => 'invoice',
                    'ItemTitle' => $title,
                    'ReportToUser' => 1
                );
                $this->App_activity->logAppActivity($data);

				header('Location: '.BASE_URL.'finances/FinanceView/viewInvoice/'.$invoiceID.'/'.$printInvoice);
			}
		} else {
			header('Location: '.BASE_URL.'finances/FinanceView');
		}
	}

	function createInvoiceFromTimesheet($timeSheetID=NULL,$projectIDCollection=NULL,$taskIDCollection=NULL,$dateStart=NULL,$dateEnd=NULL,$userid=NULL) {
		if (isset($_POST['timeSheetID'])) {
			$timeSheetID = $_POST['timeSheetID'];
		}
		if (isset($_POST['projectIDCollection'])) {
			$projectIDCollection = $_POST['projectIDCollection'];
		}
		if (isset($_POST['taskIDCollection'])) {
			$taskIDCollection = $_POST['taskIDCollection'];
		}
		if (isset($_POST['dateStart'])) {
			$dateStart = $_POST['dateStart'];
		}
		if (isset($_POST['dateEnd'])) {
			$dateEnd = $_POST['dateEnd'];
		}
		if (isset($_POST['userid'])) {
			$userid = $_POST['userid'];
		}

        /*
         * Let's set some default stuff and get some invoice settings first.
         */
        $invoiceArray['action']        = 'add';
        $invoiceArray['accountUserid'] = $this->session->userdata('accountUserid');
        $invoiceArray['userid']        = $this->session->userdata('userid');
        $invoiceArray['ToType']        = 'client';
        $today = convertMySQLToGMT(date('Y-m-d'),$direction='pull',$format='Y-m-d');
        $invoiceArray['invoiceDate']   = $today;

        $settingsArray = array();
        $settingsArray = $this->SettingsUpdate->getSettings($this->session->userdata('accountUserid'),NULL,NULL,'invoice');

        $invoiceArray['invoiceTaxRate']        = '';
        $invoiceArray['invoiceTaxName']        = '';
        $invoiceArray['invoiceTaxID']          = '';
        $invoiceArray['invoicePaymentDue']     = '';
        $invoiceArray['invoiceShipping']       = '';
        $invoiceArray['invoiceLateFee']        = '';
        $invoiceArray['invoiceMessage']        = '';
        $invoiceArray['invoiceLateFeeOther']   = '';
        $invoiceArray['invoiceDiscount']       = '';
        $invoiceArray['invoicePaymentDueDate'] = '';
        $invoiceArray['printMessage']          = '';
        $invoiceArray['invoiceCategory']       = '';
        $invoiceArray['invoiceTags']           = '';
        $invoiceArray['invoicePONumber']       = '';
        $invoiceArray['amountSubTotal'] = '';
        $invoiceArray['amountDiscount'] = '';
        $invoiceArray['amountShipping'] = '';
        $invoiceArray['amountTax']      = '';
        $invoiceArray['invoiceTotal']   = '';

        foreach($settingsArray as $setting) {
            if ($setting['Setting'] == 'invoiceMessage') {
                $invoiceArray['invoiceMessage'] = $setting['SettingValue'];
            } elseif ($setting['Setting'] == 'invoiceTaxName') {
                $invoiceArray['invoiceTaxName'] = $setting['SettingValue'];
            } elseif ($setting['Setting'] == 'invoiceTaxID') {
                $invoiceArray['invoiceTaxID'] = $setting['SettingValue'];
            } elseif ($setting['Setting'] == 'invoiceTaxRate') {
                $invoiceArray['invoiceTaxRate'] = $setting['SettingValue'];
            } elseif ($setting['Setting'] == 'invoicePaymentDue') {
                $invoiceArray['invoicePaymentDue'] = $setting['SettingValue'];
            } elseif ($setting['Setting'] == 'invoiceLateFee') {
                $invoiceArray['invoiceLateFee'] = $setting['SettingValue'];
            }
        }

		if ($timeSheetID>0) {
			$timeSheetEntryArray = $this->TimesheetView->getTimesheetEntry($timeSheetID);

            if (!empty($timeSheetEntryArray)) {
                $invoiceArray['invoiceNumber'] = date('m-d-Y');
                $lastInvoiceNumber = $this->FinanceView->getLastInvoiceNumber($this->session->userdata('accountUserid'));
                if ($lastInvoiceNumber != false) {
                    /*
                     * Extract numeric portion of lastInvoiceumber
                     */
                    $lastInvoiceNumberNumeric = getNumericPortion($lastInvoiceNumber);
                    if ($lastInvoiceNumberNumeric != false) {
                        $invoiceArray['invoiceNumber'] = $lastInvoiceNumberNumeric+1;
                    }
                }
                $projectArray = $this->Projects->getProjectInformation($timeSheetEntryArray['ProjectID']);
                $invoiceArray['clientID']      = $projectArray['ClientID'];
                $clientArray = $this->Clients->getClientInformation($invoiceArray['clientID']);
                $invoiceArray['ToUID']         = $clientArray['ClientUserid'];
                $invoiceArray['projectID']     = $timeSheetEntryArray['ProjectID'];
                $invoiceArray['invoiceTitle']  = lang('finance_invoice_for').' '.$projectArray['Title'];

                /*
                 * Enter invoice item
                 */
                $taskTitle = '';
                if ($timeSheetEntryArray['TaskID']>0) {
                    $taskArray = $this->Tasks->getTask($timeSheetEntryArray['TaskID']);
                    $taskTitle = $taskArray['Title'];
                }
                $invoiceArray['items'][0]['desc'] = $taskTitle.'<br />'.$timeSheetEntryArray['Comments'];
                $invoiceArray['items'][0]['type'] = lang('finance_invoice_type_hours');
                $invoiceArray['items'][0]['qty']  = $timeSheetEntryArray['ElapsedTime'];

                /*
                 * Find rate for project,client,shop - in that order.
                 */
                $invoiceArray['items'][0]['rate'] = '';
                $invoiceArray['items'][0]['tax']  = '';
                $invoiceArray['items'][0]['total']= '';

                $invoiceID = $this->InvoiceUpdate->saveInvoice($invoiceArray);
                $invoiceNoArray['InvoiceNo'] = $invoiceArray['invoiceNumber'];
                $invoiceNoArray['InvoiceID'] = $invoiceID;
                $invoiceNoArray['Company']   = $clientArray['Company'];

                echo json_encode($invoiceNoArray);
            }
		} else {
			if (empty($userid)) {
				$userid = $this->session->userdata('userid');
			}
			if (is_numeric($projectIDCollection)) {
                /*
                 * We're creating an invoice for 1 timesheet entry
                 */
				$timeSheetEntryArray = $this->TimesheetView->getTimesheetRecords($dateStart,$dateEnd,$userid,NULL,$projectIDCollection,$taskIDCollection);
			} else {
                /*
                 * We're creating invoice(s) for potentially many projects and timesheet entries.
                 */
				$projectIDArray = explode('|',$projectIDCollection);
                $taskIDArray    = explode('|',$taskIDCollection);
				
				$j=0;
                /*
                 * Create timesheet entry array.
                 */
                $projectIDArrayFilter = array_unique($projectIDArray);
                $projectID = 0;
                foreach($projectIDArrayFilter as $projectID) {
                    if ($projectID>0 && !empty($projectID)) {
                        $timeSheetEntryArray[$projectID] = $this->TimesheetView->getTimesheetRecords($dateStart,$dateEnd,$userid,NULL,$projectID);
                        $j++;
                    }
				}
                /*
                 * Now, format the timesheet arrays so that we can send them
                 * to the saveInvoice method.
                 */
                //$projectIDArrayFilter = array_unique($projectIDArray);
                $j=0;
                $k=0;
                $invoiceNoArray = array();
                foreach($projectIDArrayFilter as $projectID) {
                    if ($projectID>0) {
                        /*
                         * Try to create an invoice number...default to today's date
                         */
                        $invoiceArray['invoiceNumber'] = date('m-d-Y');
                        $lastInvoiceNumber = $this->FinanceView->getLastInvoiceNumber($this->session->userdata('accountUserid'));
                        if ($lastInvoiceNumber != false) {
                            /*
                             * Extract numeric portion of lastInvoiceumber
                             */
                            $lastInvoiceNumberNumeric = getNumericPortion($lastInvoiceNumber);
                            if ($lastInvoiceNumberNumeric != false) {
                                $invoiceArray['invoiceNumber'] = $lastInvoiceNumberNumeric+1;
                            }
                        }

                        $invoiceArray['clientID']      = $timeSheetEntryArray[$projectID][0]['ClientID'];
                        $clientArray = $this->Clients->getClientInformation($invoiceArray['clientID']);
                        $invoiceArray['ToUID']         = $clientArray['ClientUserid'];
                        $invoiceArray['projectID']     = $timeSheetEntryArray[$projectID][0]['ProjectID'];
                        $invoiceArray['invoiceTitle']  = lang('finance_invoice_for').' '.$timeSheetEntryArray[$projectID][0]['ProjectTitle'];

                        /*
                         * Do invoice items
                         */
                        $j=0;
                        $invoiceArray['items'] = array();
                        foreach($timeSheetEntryArray[$projectID] as $item) {
                            $invoiceArray['items'][$j]['desc'] = $item['TaskTitle'].'<br />'.$item['Comments'];
                            $invoiceArray['items'][$j]['type'] = lang('finance_invoice_type_hours');
                            $invoiceArray['items'][$j]['qty']  = $item['ElapsedTime'];

                            /*
                             * Find rate for project,client,shop - in that order.
                             */
                            $invoiceArray['items'][$j]['rate'] = '';
                            $invoiceArray['items'][$j]['tax']  = '';
                            $invoiceArray['items'][$j]['total']= '';
                            $j++;
                        }

                        $invoiceID = $this->InvoiceUpdate->saveInvoice($invoiceArray);
                        $invoiceNoArray[$k]['InvoiceNo'] = $invoiceArray['invoiceNumber'];
                        $invoiceNoArray[$k]['InvoiceID'] = $invoiceID;
                        $invoiceNoArray[$k]['Company']   = $clientArray['Company'];
                    }
                    $j++;
                    $k++;
                }                
                echo json_encode($invoiceNoArray);
			}
		}	
	}

    function createInvoiceFromExpenseItem($expenseID,$userid=NULL) {

    }

    function addExpenseItemToInvoice($invoiceID,$expenseID,$userid=NUL) {

    }

    function makeInvoicePayment($payArray=NULL) {
        if (array_empty($payArray)) {
            $payArray = $_POST;
            $payArray['payInvoiceAccountNumber'] = fieldToDB($this->input->post('payInvoiceAccountNumber', TRUE));
            $payArray['payInvoiceAmount']        = $this->input->post('payInvoiceAmount', TRUE);
            $payArray['payInvoiceDate']          = $this->input->post('payInvoiceDate', TRUE);
            $payArray['payInvoiceStatus']        = $this->input->post('payInvoiceStatus', TRUE);
            $payArray['payComments']             = fieldToDB($this->input->post('payInvoiceComment', TRUE), TRUE,TRUE,FALSE,TRUE);
        }
        $payArray['userid'] = $this->session->userdata('userid');
        if (!empty($payArray['payInvoiceDate'])) {
            $payArray['payInvoiceDate'] = jsDateToMySQL($payArray['payInvoiceDate']);
        } else {
            $payArray['payInvoiceDate'] = convertMySQLToGMT(date('Y-m-d'),$direction='pull',$format='Y-m-d');
        }
        /*
         * Let's look at our status
         */
        $statusArray = getInvoiceStatusForUpdate($payArray['payInvoiceStatus']);
        $payArray['invoiceStatus'] = $statusArray['invoiceStatus'];
        $payArray['payStatus'] = $statusArray['payStatus'];

        $this->InvoiceUpdate->saveInvoicePayment($payArray);
        
        $finalStatusArray = getInvoiceStatus($payArray['invoiceStatus'],$payArray['payStatus'],null,null,$payArray['payInvoiceDate']);
        
        $invoiceID = $payArray['invoiceID'];
        $invoiceArray = $this->commonfinance->getInvoiceDetails($invoiceID,'array');
        $title = $invoiceArray[0]['InvNo'].' '.$invoiceArray[0]['Title'];
        $data = array(
            'AccountUserid' => $this->session->userdata('accountUserid'),
            'UserPID' => $this->session->userdata('pid'),
            'ProjectID' => $invoiceArray[0]['ProjectID'],
            'UserType' => $this->session->userdata('userType'),
            'Activity' => 'invoice payment',
            'ItemID' => $invoiceID,
            'ItemType' => 'invoice',
            'ItemTitle' => $title,
            'ReportToUser' => 1
        );
        $this->App_activity->logAppActivity($data);
        $paymentArray = $this->commonfinance->getInvoicePayments($payArray['invoiceID'],'array');
        $paymentArray['NewStatusHuman'] = $finalStatusArray[0];
        $paymentArray['NewStatusMachine'] = $finalStatusArray[1];
        echo json_encode($paymentArray);
    }
    
    function updateInvoicePayment() {
        $payArray['payInvoiceAmount'] = $this->input->post('payInvoiceAmount', TRUE);
        $payArray['paymentID'] = $this->input->post('paymentID', TRUE);
        $payArray['userid'] = $this->session->userdata('userid');
        $this->InvoiceUpdate->updateInvoicePayment($payArray);
    }

    function deleteInvoicePayment($paymentID) {
        $this->InvoiceUpdate->deleteInvoicePayment($paymentID);
    }

	function deleteInvoice($invoiceID,$tagForDelete=TRUE) {
        $invoiceArray = $this->commonfinance->getInvoiceDetails($invoiceID,'array');
        $title = $invoiceArray[0]['InvNo'].' '.$invoiceArray[0]['Title'];
        $data = array(
            'AccountUserid' => $this->session->userdata('accountUserid'),
            'UserPID' => $this->session->userdata('pid'),
            'ProjectID' => $invoiceArray[0]['ProjectID'],
            'UserType' => $this->session->userdata('userType'),
            'Activity' => 'delete',
            'ItemID' => $invoiceID,
            'ItemType' => 'invoice',
            'ItemTitle' => $title,
            'ReportToUser' => 1
        );
        $this->App_activity->logAppActivity($data);
        
        $this->InvoiceUpdate->deleteInvoice($invoiceID,$tagForDelete,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }

    function restoreInvoice($invoiceID) {
        $this->InvoiceUpdate->restoreInvoice($invoiceID,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }

    function updateInvoiceStatus($invoiceID=NULL,$newStatus=NULL) {
        if (empty($invoiceID)) {
            $invoiceID = $_POST['invoiceID'];
            $newStatus = $_POST['newStatus'];
        }
        $statusArray   = getInvoiceStatusForUpdate($newStatus);
        $invoiceStatus = $statusArray['invoiceStatus'];
        $payStatus     = $statusArray['payStatus'];
        $this->InvoiceUpdate->updateInvoiceStatus($invoiceID,$invoiceStatus,$payStatus);
        $statusArray = $this->FinanceView->getInvoiceStatus($invoiceID);
        echo '{"InvoiceID":"'.$invoiceID.'","NewStatusMachine":"'.$statusArray[1].'","NewStatusHuman":"'.$statusArray[0].'"}';
    }
}
?>