<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payment extends Controller {

    function __construct(){
		parent::Controller();
                
		$this->load->model('common/public_site','',true);
        $this->load->model('accounts/accounts','',true);

        $this->load->library('application/CommonEmail');

        $this->load->helper('account');
        $this->load->helper('date');
        $this->load->helper('payment');
        $this->load->helper('templates');
	}

    function _init($plan=NULL,$isPaymentSuccess=FALSE) {
        global $accountLevelArray, $monthNameArray, $accountPricePrev, $accountPriceNext, $datePriceChange;

        $data['child']                 = TRUE;
        if ($isPaymentSuccess == TRUE) {
            $data['headerTextImage']       = 'txtThanksHeader_public.png';
        } else {
            $data['headerTextImage']       = 'txtPaymentHeader_public.png';
        }
        $data['rightColumnComponents'] = array('paymentInfo');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Payment';

        $planCost = '';
        $a=0;
        foreach ($accountLevelArray as $accountLevel) {
            if ($accountLevel['name'] == $plan) {
                //$planCost = $accountLevel['cost'];
                $planCost = $accountPricePrev[$a];
                if (days_between_dates(strtotime($datePriceChange),time())>30) {
                    $planCost = $accountPriceNext[$a];
                }
                $planNumber = $a;
            }
            $a++;
        }
        $data['rate_plan']  = $plan;
        $data['planNumber'] = $planNumber;
        $data['rate_price'] = $planCost;
        $data['planDataArray'] = createPlanData($plan);

        if ($this->session->userdata('logged') != TRUE) {
            header('Location: '.site_url('error'));
        }

        $data['currentMonth']  = date('m');
        $data['currentYear']   = date('Y');

        $data['months'] = $monthNameArray;

        return $data;
    }

    function paymentInfo($accountUserid,$plan=NULL) {
        global $accountLevelArray;

        /*
         * Retrieve account data and put it into session.
         */        
        $accountArray = spinUpAccountSession($accountUserid);
        $paymentProfileID = $this->session->userdata('paymentProfileID');
        $isPaid           = $this->session->userdata('isPaid');
        $accountLevel     = $this->session->userdata('accountLevel');

        /*
         * Are we paying for the first time?
         */        
        $linkSelectNewPlan = '/account/changeAccount/'.$accountUserid;
        if (empty($paymentProfileID)) {
            $whyPay = 'newAccount';
        } elseif (!empty($paymentProfileID) && $isPaid == TRUE) {
            $whyPay = 'changeAccount';
        } elseif (!empty($paymentProfileID) && $isPaid == FALSE) {
            $whyPay = 'paymentError';
        }
        $this->session->set_userdata('whyPay',$whyPay);

        if (empty($plan)) {
            foreach ($accountLevelArray as $key => $value) {
                if ($key == $accountLevel) {
                    $plan = $value['name'];
                }
            }
        }
        
        $data = Payment::_init($plan);
        $data['linkSelectNewPlan'] = $linkSelectNewPlan;
        
        /*
         * Load up our fields
         */
        $data['firstName'] = $accountArray['BillingNameFirst'];
        $data['lastName']  = $accountArray['BillingNameLast'];
        $data['address']   = $accountArray['BillingAddress'];
        $data['city']      = $accountArray['BillingCity'];
        $data['state']     = $accountArray['BillingStateRegion'];
        $data['zip']       = $accountArray['BillingZipPostalCode'];
        $data['country']   = $accountArray['BillingCountry'];
        $data['currency']  = $accountArray['BillingCurrency'];
        $data['cardtype']  = NULL;
        $data['expMonth']  = date('m');
        $data['expYear']   = date('Y');
        $data['description'] = 'CreativePro Office '.$plan.' subscription for $'.$data['rate_price'].'.00 per month.';
        $data['whyPay']    = $whyPay;

        $whyPayArray = determineWhyPay();
        $data['submitButtonClass'] = $whyPayArray['buttonClass'];

        $this->load->view('public_site/Payment',$data);
    }

    function sendPayment() {
        global $monthNameArray;
        $validPayment = TRUE;

        /*
         * Package up some POST data into our own array.
         */
        $plan = $_POST['plan'];
        $data = Payment::_init($plan);
        $data['linkSelectNewPlan'] = '/account/changeAccount/'.$this->session->userdata('accountUserid');

        $data['rate_price']  = $_POST['amount'];
        $data['description'] = $_POST['description'];
        $data['rate_plan']   = $plan;
        $data['currentMonth']= $_POST['currentMonth'];
        $data['currentYear'] = $_POST['currentYear'];

        $data['firstName']   = $_POST['firstName'];
        $data['lastName']    = $_POST['lastName'];
        $data['address']     = $_POST['address'];
        $data['city']        = $_POST['city'];

        if (!empty($_POST['stateRegion'])) {
            $data['state'] = fieldToDB($this->input->post('stateRegion', TRUE));
        } else {
            $data['state'] = $_POST['stateUS'];
        }

        $data['zip']         = $_POST['zip'];
        $data['country']     = $_POST['country'];
        $data['currency']    = $_POST['currency'];
        $data['cardtype']    = $_POST['cardtype'];
        $data['expMonth']    = $_POST['expmonth'];
        $data['expYear']     = $_POST['expyear'];

        /*
         * Determine why this user is submitting a transaction. Could be
         * 1. New account payment
         * 2. Upgrading/downgrading to another account level
         * 3. Updating credit card info because a transaction failed.
         * determineWhyPay() is in payment_helper.php
         */
        $whyPayArray = determineWhyPay();
        $data['submitButtonClass'] = $whyPayArray['buttonClass'];

        /*
         * Some CodeIgniter (CI) form validation stuff.
         */
        if ($this->form_validation->run() == FALSE)	{ 
            $this->load->view('public_site/Payment',$data);
        } else {
            /*
             * If form validation passes, set request-specific fields for PayPal.
             */
            $paymentType      = urlencode('Sale');
            $description      = urlencode($_POST['description']); // Description of product or service.

            $firstName        = urlencode(fieldToDB($this->input->post('firstName', TRUE)));
            $lastName         = urlencode(fieldToDB($this->input->post('lastName', TRUE)));
            $address          = urlencode(fieldToDB($this->input->post('address', TRUE)));
            $city             = urlencode(fieldToDB($this->input->post('city', TRUE)));
            $state            = urlencode($data['state']);
            $creditCardType   = urlencode($_POST['cardtype']);

            /*
             * Pull numberic portion of credit card number out in case
             * user puts in - or x or any other non-numeric character.
             * NOTE: fieldToDB is my little data cleaning function - you likely have your own.
             */
            $creditCardInput = fieldToDB($this->input->post('cardnumber', TRUE));
            $creditCardNumeric = '';
            for($a=0;$a<=strlen($creditCardInput)-1;$a++) {
                if (is_numeric($creditCardInput[$a])) {
                    $creditCardNumeric .= $creditCardInput[$a];
                }
            }
            $creditCardNumber = urlencode($creditCardNumeric);
            /*
             * CC number rendered back to the user with only last 4 digits.
             */
            $creditCardNoSecure = 'XXXX-XXXX-XXXX-'.substr($creditCardNumeric,-4,4);

            $expDateMonth     = urlencode($_POST['expmonth']);
            $expDateYear      = urlencode($_POST['expyear']);
            $cvv2Number       = urlencode(fieldToDB($this->input->post('cvv2number', TRUE))); // Credit card security code.
            $zip              = urlencode(fieldToDB($this->input->post('zip', TRUE)));
            $country          = urlencode($_POST['country']);
            $amount           = urlencode($_POST['amount']);
            $currencyID       = urlencode($_POST['currency']);	// Or other PayPal supported currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
            $now = time();
            $profileStartDate = standard_date('DATE_PAYPAL',$now);

            /*
             * Start building the request string to send via CURL to PayPal API.
             * NOTE: BILLINGPERIOD=Month&BILLINGFREQUENCY=1 for recurring payments once a month.
             */
            $nvpStr  = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                        "&EXPDATE=$expDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                        "&COUNTRYCODE=$country&STATE=$state&STREET=$address&CITY=$city&ZIP=$zip&CURRENCYCODE=$currencyID".
                        "&PROFILESTARTDATE=$profileStartDate&BILLINGPERIOD=Month&BILLINGFREQUENCY=1&DESC=$description".
                        "&MAXFAILEDPAYMENTS=3";

            /*
             * THIS IS IMPORTANT!!!!
             */
            $method =   "CreateRecurringPaymentsProfile";
            
            if ($validPayment == TRUE) {
                if ($this->session->userdata('whyPay') != 'newAccount') {
                    /*
                     * If this user is NOT paying for a new account, but instead
                     * switching accounts, then cancel existing PayPal recurring billing
                     * profile first, then create a new profile. PayPal doesn't allow you
                     * to just update an existing recurring payment profile. It's a hassle
                     * and your users WILL BE BILLED AGAIN WHEN THEY CHANGE ACCOUNTS. So you
                     * need some way to refund a previous payment.  For example, if your user signs
                     * up for a $30/month plan and has already been billed for this month, then
                     * they upgrade to a $70/month plan in the middle of the month, they will be
                     * billed $30 + $70 for the month.  You'll need to refund one payment.
                     */

                    /*
                     * paymentProfileID is stored in the CPO database and is returned by
                     * PayPal during the creation of a new recurring account.
                     */
                    $profileID = urlencode($this->session->userdata('paymentProfileID'));
                    $note = urlencode('CreativePro Office account updated. Need new profile ID.');
                    $nvpStrCancel =	"&PROFILEID=$profileID&ACTION=Cancel&NOTE=$note";

                    /*
                     * PPHttpPost contains the CURL stuff.  It's located in payment_helper.php
                     */
                    $httpParsedResponseArCancel = PPHttpPost('ManageRecurringPaymentsProfileStatus', $nvpStrCancel);
                }
                
                /*
                 * Now we create the new PayPal profile.
                 */
                $accountArray['profileID'] = NULL;
                if ($plan != 'free' && $data['rate_price']>0) {
                    /*
                     * FINALLY!!! We send the request to PayPal.  Look in payment_helper.php
                     * for the PPHttpPost function.
                     */
                    $httpParsedResponseAr = PPHttpPost($method, $nvpStr);
                    $data['payPalResponseArray'] = $httpParsedResponseAr;

                    /*
                     * If PayPal sends us some kind of success message, then we're ok.
                     */
                    if(strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESS" || strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESSWITHWARNING") {
                        $validPayment = TRUE;
                    } else {
                        $validPayment = FALSE;
                    }
                    $accountArray['profileID'] = urldecode($data['payPalResponseArray']['PROFILEID']);
                }
            }

            /*
             * The following is just rendering views to the user and putting payment data into
             * user account table.
             */

            /*
             * Load the required view based on payment success or failure.
             */
            if ($validPayment == TRUE) {
                /*
                 * Update account table for this account with billing address and
                 * PayPal Profile ID.
                 */
                $accountArray['accountID'] = $this->session->userdata('accountUserid');
                $accountArray['isPaid']    = 1;
                $accountArray['billingNameFirst'] = urldecode($firstName);
                $accountArray['billingNameLast']  = urldecode($lastName);
                $accountArray['billingAddress']   = urldecode($address);
                $accountArray['billingCity']      = urldecode($city);
                $accountArray['billingState']     = urldecode($state);
                $accountArray['billingZip']       = urldecode($zip);
                $accountArray['billingCountry']   = $_POST['country'];
                $accountArray['billingCurrency']  = $_POST['currency'];
                $accountArray['accountPrice']     = $_POST['amount'];
                $accountArray['accountLevel']     = $data['planNumber'];

                $this->accounts->updateAccountWithPaymentInformation($accountArray);

                /*
                 * Send thank you confirmation email to account owner.
                 */
                $whyPayArray = determineWhyPay();
                $messageSubject = $whyPayArray['emailSubject'];

                $templateData['whyPay']      = $this->session->userdata('whyPay');
                $templateData['plan']        = $plan;
                $templateData['cost']        = $data['planDataArray']['acctVars']['cost'];
                $templateData['projects']    = $data['planDataArray']['acctVars']['projects'];
                $templateData['teamMembers'] = $data['planDataArray']['acctVars']['teamMembers'];
                $templateData['storage']     = $data['planDataArray']['acctVars']['storage'];
                $templateData['billingInfo']  = $accountArray['billingNameFirst'].' '.$accountArray['billingNameLast'].'<br>'.Chr(10);
                $templateData['billingInfo'] .= $accountArray['billingAddress'].'<br>'.Chr(10);
                $templateData['billingInfo'] .= $accountArray['billingCity'].' '.$accountArray['billingState'].' '.$accountArray['billingZip'].'<br>'.Chr(10);
                $templateData['billingInfo'] .= 'Card No: '.$creditCardNoSecure.'<br>'.Chr(10);
                $templateData['billingInfo'] .= 'Expires on: '.$_POST['expmonth'].'/'.$_POST['expyear'];


                $messageHTML = templatePaymentEmail($templateData);

                $messageSent = $this->commonemail->sendPaymentEmail($messageSubject,$messageHTML,$this->session->userdata('email'),NULL);
               
                /*
                 * Redirect to Thank You page in insecure area.
                 */
                header('Location: '.SECURE_URL.'payment/paymentSuccess/'.$plan.'/'.$this->session->userdata('whyPay'));
            } else {
                /*
                 * If PayPal transaction failed.....
                 * Let's format some error messages
                 */
                $errorCode = urldecode($data['payPalResponseArray']['L_ERRORCODE0']);
                $errorMessage = getHumanTransactionErrorMessage($errorCode); // in payment_helper.php

                /*
                 * Log error to database
                 */
                $errorArray['errorType']     = 'PayPal';
                $errorArray['errorCode']     = $errorCode;
                $errorArray['accountUserid'] = $this->session->userdata('accountUserid');
                $errorMessage = urldecode($data['payPalResponseArray']['L_LONGMESSAGE0']);
                $errorArray['errorMessage']  = fieldToDB($errorMessage);
                $errorArray['errorPage']     = NULL;
                $errorLogID = $this->accounts->enterPaymentErrorLog($errorArray);

                /*
                 * Send email notification to CPO support
                 */
                $messageAdmin  = 'Someone encountered an error while paying.'.Chr(10);
                $messageAdmin .= 'Account ID: '.$this->session->userdata('accountUserid').Chr(10);
                $messageAdmin .= 'Error Code: '.$errorCode.Chr(10);
                $messageAdmin .= 'Error Message: '.$errorArray['errorMessage'];
                $subjectAdmin = "CreativePro Office: Account payment problem";
                $this->commonemail->sendEmailToAdmin($messageAdmin,$subjectAdmin);

                $data['errorMessage'] = $errorMessage;
                $this->load->view('public_site/Payment',$data);
            }
        }
    }    

    function paymentSuccess($plan,$whyPay=NULL) {
        $data = Payment::_init($plan,TRUE);
        $accountLevelStatusArray = $this->accounts->checkAccountLevelStatus($this->session->userdata('accountUserid'));
        if ($accountLevelStatusArray['IsPaid'] == TRUE) {
            $this->session->set_userdata('isPaid',TRUE);
        } else {
            $this->session->set_userdata('isPaid',FALSE);
        }
        $this->session->set_userdata('accountLevel',$accountLevelStatusArray['AccountLevel']);
        $this->session->set_userdata('paymentProfileID',$accountLevelStatusArray['PaymentProfileID']);

        $data['whyPay'] = $whyPay;
        $data['linkSelectNewPlan'] = NULL;

        $this->load->view('public_site/Payment_success',$data);
    }

    function selfHostedSendPayment($approved=null) {
        global $monthNameArray;
        if ($approved == 'approved') {
            $approved = TRUE;
        } else {
            $approved = FALSE;
        }
        $validPayment = TRUE;
        $this->session->set_userdata('logged',true);
        $data = Payment::_init();
        
        $data['price']       = $_POST['amount'];
        $data['description'] = $_POST['description'];
        $data['currentMonth']= $_POST['currentMonth'];
        $data['currentYear'] = $_POST['currentYear'];

        $data['firstName']   = $_POST['firstName'];
        $data['lastName']    = $_POST['lastName'];
        $data['address']     = $_POST['address'];
        $data['city']        = $_POST['city'];

        if (!empty($_POST['stateRegion'])) {
            $data['state'] = fieldToDB($this->input->post('stateRegion', TRUE));
        } else {
            $data['state'] = $_POST['stateUS'];
        }

        $data['zip']         = $_POST['zip'];
        $data['country']     = $_POST['country'];
        $data['currency']    = $_POST['currency'];
        $data['cardtype']    = $_POST['cardtype'];
        $data['expMonth']    = $_POST['expmonth'];
        $data['expYear']     = $_POST['expyear'];

        $whyPayArray = determineWhyPay();
        $data['submitButtonClass'] = $whyPayArray['buttonClass'];

        if ($this->form_validation->run() == FALSE && $approved == FALSE)	{
            $this->load->view('public_site/Selfhostedbuy',$data);
        } else {
            /*
             * Set request-specific fields.
             */
            $paymentType      = urlencode('Sale');				// or 'Sale'
            $description      = urlencode($_POST['description']);

            $firstName        = urlencode(fieldToDB($this->input->post('firstName', TRUE)));
            $lastName         = urlencode(fieldToDB($this->input->post('lastName', TRUE)));
            $address          = urlencode(fieldToDB($this->input->post('address', TRUE)));
            $city             = urlencode(fieldToDB($this->input->post('city', TRUE)));
            $email            = urlencode(fieldToDB($this->input->post('email', TRUE)));
            $state            = urlencode($data['state']);
            $creditCardType   = urlencode($_POST['cardtype']);
            $ipAddress        = urlencode($_SERVER['REMOTE_ADDR']);
            $productType      = $_POST['productType'];

            /*
             * Pull numberic portion of credit card number out
             */
            $creditCardInput = fieldToDB($this->input->post('cardnumber', TRUE));
            $creditCardNumeric = '';
            for($a=0;$a<=strlen($creditCardInput)-1;$a++) {
                if (is_numeric($creditCardInput[$a])) {
                    $creditCardNumeric .= $creditCardInput[$a];
                }
            }
            $creditCardNumber = urlencode($creditCardNumeric);
            $creditCardNoSecure = 'XXXX-XXXX-XXXX-'.substr($creditCardNumeric,-4,4);

            $expDateMonth     = urlencode($_POST['expmonth']);
            $expDateYear      = urlencode($_POST['expyear']);
            $cvv2Number       = urlencode(fieldToDB($this->input->post('cvv2number', TRUE)));
            $zip              = urlencode(fieldToDB($this->input->post('zip', TRUE)));
            $country          = urlencode($_POST['country']);
            $amount           = urlencode($_POST['amount']);
            $currencyID       = urlencode($_POST['currency']);	// Or other PayPal supported currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
            $now = time();
            $profileStartDate = standard_date('DATE_PAYPAL',$now);

            if ($approved == TRUE) {
                $accountArray['orderNumber']      = $this->accounts->getNewSelfHostedOrderNumber();
                $accountArray['installationKey']  = random_string('alnum',20);
                $accountArray['isPaid']           = 1;
                $accountArray['billingNameFirst'] = urldecode($firstName);
                $accountArray['billingNameLast']  = urldecode($lastName);
                $accountArray['billingAddress']   = urldecode($address);
                $accountArray['billingCity']      = urldecode($city);
                $accountArray['billingState']     = urldecode($state);
                $accountArray['billingZip']       = urldecode($zip);
                $accountArray['billingCountry']   = $_POST['country'];
                $accountArray['billingCurrency']  = $_POST['currency'];
                $accountArray['accountPrice']     = $_POST['amount'];
                $accountArray['email']            = urldecode($email);
                $accountArray['company']          = $this->input->post('company', TRUE);
                $accountArray['url']              = $this->input->post('url', TRUE);
                $accountArray['amount']           = $amount;

                /*
                 * Add request-specific fields to the request string.
                 */
                $nvpStr  = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                        "&EXPDATE=$expDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                        "&COUNTRYCODE=$country&STATE=$state&STREET=$address&CITY=$city&ZIP=$zip&CURRENCYCODE=$currencyID".
                        "&PROFILESTARTDATE=$profileStartDate&BILLINGPERIOD=Month&BILLINGFREQUENCY=1&DESC=$description".
                        "&MAXFAILEDPAYMENTS=3";

                $nvpStr  = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                            "&EXPDATE=$expDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                            "&EMAIL=$email&COUNTRYCODE=$country&STATE=$state&STREET=$address&CITY=$city&ZIP=$zip".
                            "&CURRENCYCODE=$currencyID&DESC=$description&IPADDRESS=$ipAddress";
                $method =   "DoDirectPayment";
                if ($validPayment == TRUE) {
                    $httpParsedResponseAr = PPHttpPost($method, $nvpStr);
                    $data['payPalResponseArray'] = $httpParsedResponseAr;

                    if(strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESS" || strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESSWITHWARNING") {
                        $validPayment = TRUE;
                    } else {
                        $validPayment = FALSE;
                    }
                }

                /*
                 * Load the required view based on payment success or failure.
                 */
                if ($validPayment == TRUE) {
                    if ($productType == 'updates') {
                        $paymentArray['accountID']    = $_POST['accountID'];
                        $paymentArray['productTitle'] = 'Self-hosted updates';
                        $paymentArray['amount']       = $accountArray['amount'];
                        $paymentArray['isUpdate']     = '1';
                        $this->accounts->saveSelfHostedPaymentInformation($paymentArray);

                        $accountInfo = $this->accounts->getProductAccountInformation($_POST['accountID']);
                        $accountArray['orderNumber']     = $accountInfo['OrderNumber'];
                        $accountArray['installationKey'] = $accountInfo['InstallationKey'];

                        /*
                         * Send thank you confirmation email to buyer.
                         */
                        $messageSubject = 'CreativePro Office : Thank you for your purchase';
                        $templateData['billingInfo']  = $accountArray['billingNameFirst'].' '.$accountArray['billingNameLast'].'<br>'.Chr(10);
                        $templateData['billingInfo'] .= $accountArray['billingAddress'].'<br>'.Chr(10);
                        $templateData['billingInfo'] .= $accountArray['billingCity'].' '.$accountArray['billingState'].' '.$accountArray['billingZip'].'<br>'.Chr(10);
                        $templateData['billingInfo'] .= 'Card No: '.$creditCardNoSecure.'<br>'.Chr(10);
                        $templateData['billingInfo'] .= 'Expires on: '.$_POST['expmonth'].'/'.$_POST['expyear'].Chr(10);
                        $templateData['amount']       = 'Payment Amount: $'.$amount.' USD';
                        $templateData['orderNumber']  = $accountArray['orderNumber'];
                        $templateData['installationKey'] = $accountArray['installationKey'];


                        $messageHTML = templateSelfHostedUpdatePaymentEmail($templateData);
                        $messageSent = $this->commonemail->sendPaymentEmail($messageSubject,$messageHTML,$accountArray['email'],NULL);
                        /*
                         * Redirect to download page in insecure area.
                         */
                        $sessionArray['isPaid'] = true;
                        $this->session->set_userdata($sessionArray);
                        header('Location: '.INSECURE_URL.'selfhosteddownload/index/updateRenew');
                    } elseif ($productType == 'selfhosted') {
                        /*
                         * Enter data into cpo_account_profile_product
                         */ 
                        $accountID = $this->accounts->saveSelfHostedAccountInformation($accountArray);
                    
                        /*
                         * Save payment information - 1 record for the purchase,
                         * 1 record for the free year of updates.
                         */
                        $paymentArray1['accountID']    = $accountID;
                        $paymentArray1['productTitle'] = 'Self-hosted download';
                        $paymentArray1['amount']       = $amount;
                        $paymentArray1['isUpdate']     = '0';
                        $this->accounts->saveSelfHostedPaymentInformation($paymentArray1);

                        $paymentArray2['accountID']    = $accountID;
                        $paymentArray2['productTitle'] = 'Self-hosted updates, free first year';
                        $paymentArray2['amount']       = '0';
                        $paymentArray2['isUpdate']     = '1';
                        $this->accounts->saveSelfHostedPaymentInformation($paymentArray2);

                        /*
                         * Send thank you confirmation email to buyer.
                         */
                        $messageSubject = 'CreativePro Office : Thank you for your purchase';
                        $templateData['billingInfo']  = $accountArray['billingNameFirst'].' '.$accountArray['billingNameLast'].'<br>'.Chr(10);
                        $templateData['billingInfo'] .= $accountArray['billingAddress'].'<br>'.Chr(10);
                        $templateData['billingInfo'] .= $accountArray['billingCity'].' '.$accountArray['billingState'].' '.$accountArray['billingZip'].'<br>'.Chr(10);
                        $templateData['billingInfo'] .= 'Card No: '.$creditCardNoSecure.'<br>'.Chr(10);
                        $templateData['billingInfo'] .= 'Expires on: '.$_POST['expmonth'].'/'.$_POST['expyear'].Chr(10);
                        $templateData['amount']       = 'Payment Amount: $'.$amount.' USD';
                        $templateData['orderNumber']  = $accountArray['orderNumber'];
                        $templateData['installationKey'] = $accountArray['installationKey'];


                        $messageHTML = templateSelfHostedPaymentEmail($templateData);

                        $messageSent = $this->commonemail->sendPaymentEmail($messageSubject,$messageHTML,$accountArray['email'],NULL);
                        /*
                         *
                         * Redirect to download page in insecure area.
                         */
                        $sessionArray['orderNumber']      = $accountArray['orderNumber'];
                        $sessionArray['installationKey']  = $accountArray['installationKey'];
                        $sessionArray['selfHostedLogged'] = true;
                        $sessionArray['company']          = $accountArray['company'];
                        $sessionArray['email']            = $accountArray['email'];
                        $sessionArray['isPaid']           = true;
                        $sessionArray['accountID']        = $accountID;
                        $this->session->set_userdata($sessionArray);
                        header('Location: '.INSECURE_URL.'selfhosteddownload/index/firstAccess');
                    }
                } else {
                    /*
                     * Let's format some error messages
                     */
                    $errorCode = urldecode($data['payPalResponseArray']['L_ERRORCODE0']);
                    $errorMessage = getHumanTransactionErrorMessage($errorCode);

                    /*
                     * Log error to database
                     */
                    $errorArray['errorType']     = 'PayPal';
                    $errorArray['errorCode']     = $errorCode;
                    $errorArray['accountUserid'] = $this->session->userdata('accountUserid');
                    $errorMessage = urldecode($data['payPalResponseArray']['L_LONGMESSAGE0']);
                    $errorArray['errorMessage']  = fieldToDB($errorMessage);
                    $errorArray['errorPage']     = NULL;
                    $errorLogID = $this->accounts->enterPaymentErrorLog($errorArray);

                    /*
                     * Send email notification to CPO support
                     */
                    $messageAdmin  = 'Someone encountered an error while paying.'.Chr(10);
                    $messageAdmin .= 'Account ID: '.$this->session->userdata('accountUserid').Chr(10);
                    $messageAdmin .= 'Error Code: '.$errorCode.Chr(10);
                    $messageAdmin .= 'Error Message: '.$errorArray['errorMessage'];
                    $subjectAdmin = "CreativePro Office: Account payment problem";
                    $this->commonemail->sendEmailToAdmin($messageAdmin,$subjectAdmin);

                    if (empty($_POST['accountID'])) {
                        $accountID = '0';
                    } else {
                        $accountID = $_POST['accountID'];
                    }
                    $this->session->set_userdata($_POST);
                    header('Location: '.SECURE_URL.'selfhostedbuy/index/'.$productType.'/'.$accountID.'/'.$errorCode);
                    //$this->load->view('public_site/Selfhostedbuy',$data);
                }
            } else {
                /*
                 * If $approve != TRUE then show approval page.
                 */
                $data['formData'] = $_POST;
                $data['formData']['ccNoSecure'] = $creditCardNoSecure;
                $this->load->view('public_site/Selfhostedapprove',$data);               
            }
        }
    }

    function selfHostedPaymentSuccess($plan,$whyPay=NULL) {
        $data = Payment::_init($plan,TRUE);
        $accountLevelStatusArray = $this->accounts->checkAccountLevelStatus($this->session->userdata('accountUserid'));
        if ($accountLevelStatusArray['IsPaid'] == TRUE) {
            $this->session->set_userdata('isPaid',TRUE);
        } else {
            $this->session->set_userdata('isPaid',FALSE);
        }
        $this->session->set_userdata('accountLevel',$accountLevelStatusArray['AccountLevel']);
        $this->session->set_userdata('paymentProfileID',$accountLevelStatusArray['PaymentProfileID']);

        $data['whyPay'] = $whyPay;
        $data['linkSelectNewPlan'] = NULL;

        $this->load->view('public_site/Payment_success',$data);
    }
}
?>
