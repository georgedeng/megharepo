<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class SiteSearch extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();

        $this->load->model('common/site_search','',true);
        $this->load->library('application/CommonMessages');
	}

    function _init($searchTerm=NULL,$clientID=0) {
        $data['page']            = 'finances';
		$data['pageTitle']       = lang('common_search_office');
		$data['pageSubTitle']    = '';
		$data['wysiwyg']         = 0;
		$data['map']             = 0;
		$data['jsFileArray']     = array('jsSiteSearch.js');
		$data['pageIconClass']   = 'iconPageSearch';
		$data['pageLayoutClass'] = 'withRightColumn';
        $data['rightColumnComponents'] = array();

        $data['searchTerm'] = $searchTerm;

        $accountUserid = $this->session->userdata('accountUserid');
        $userid = $this->session->userdata('userid');
        if (!empty($searchTerm) && $searchTerm != NULL) {
            /*
             * Conduct site search
             */
            $clientID = NULL;
            if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
                $clientID = $this->session->userdata('clientID');
            }
            $q = cleanStringTag(strtolower($searchTerm));
            $data['searchResults'] = $this->site_search->performSiteSearch($q,$accountUserid,$userid,$clientID);
        } else {
            $data['searchResults'] = NULL;
        }

        return $data;
    }

    function index() {
        $searchTerm = $this->input->post('appSearch', TRUE);
        $data = SiteSearch::_init($searchTerm);
		$this->load->view('search/SiteSearch',$data);
    }

    function siteSearchAutocomplete($clientID=0) {
        if (!empty($_POST['term'])) {
            $q = cleanStringTag(strtolower($this->input->post('term', TRUE)));
        } else {
            $q = cleanStringTag(strtolower($this->input->get('term', TRUE)));
        }    
		if (!$q) return;
        $accountUserid = $this->session->userdata('accountUserid');
        $userid        = $this->session->userdata('userid');
		$searchArray = $this->site_search->getSearchResultsForAutoCompleter($q,$accountUserid,$userid,$clientID);
		echo $searchArray;
    }
}
?>