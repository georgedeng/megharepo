<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Appinit extends Controller {
     function __construct()
	{
          parent::Controller();
          $this->load->model('common/public_site','',true);
          $this->load->model('blog/Blog_model','',true);
	}

	function _init() {
        $data['child'] = FALSE;
        $data['jsFileArray'] = array();
        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(6);

        /*
         * Get latest blog posts
         */
        $data['blogRecentEntries']    = $this->Blog_model->getLatestEntries(7);

        $data['rightColumnComponents'] = array('whosUsingCPO_nobg');
        $data['headerTextImage']       = '';
        $data['showBlogLatest']        = TRUE;
        $data['jsFileArray']           = array('jquery/plugins/jquery.tweet.js','jquery/plugins/jquery.colorbox.js','jquery/plugins/jquery.fixfloat.js');

        noCache();
        return $data;
	}

	function index() {
        $data = Appinit::_init();

        global $accountPricePrev,$accountPriceNext,$datePriceChange;

        $data['currentPrices'] = $accountPricePrev;
        if (days_between_dates(strtotime($datePriceChange),time())>0) {
            $data['currentPrices'] = $accountPriceNext;
        }

        $this->load->view('AppInit',$data);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
