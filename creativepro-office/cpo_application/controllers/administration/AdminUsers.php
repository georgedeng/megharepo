<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AdminUsers extends Controller {
	function __construct()
	{
		parent::Controller();	
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('administration/Admin_users','',true);	
	}

    function _init() {
        $data['page']                  = 'adminUsers';
		$data['pageTitle']             = 'Administration: User Manager';
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsAdminUsers.js');
		$data['pageIconClass']         = 'iconPageAdministration';
		$data['pageLayoutClass']       = 'withRightColumn';
        $data['rightColumnComponents'] = array('menuAdministration');

        return $data;
    }

	function index() {
        $data = AdminUsers::_init();
		$this->load->view('administration/AdminUsers',$data);
	}
	
	function searchUsers() {
		$q = cleanStringTag(strtolower($this->input->get('term', TRUE)));
		if (!$q) return;
		$names = $this->Admin_users->getUsersForAutoCompleter($q);
		echo $names;
	}
	
	function getUserInformation() {
        global $accountLevelArray;

		$userInfoArray = $this->Admin_users->getUserInformation($_POST['accountUserid']);
		$accountUserid = $_POST['accountUserid'];
		
		$projectArray = $this->Admin_users->getNumberUserProjects($accountUserid);
		$invoiceArray = $this->Admin_users->getNumberUserInvoices($accountUserid);
		$clientArray  = $this->Admin_users->getNumberUserClients($accountUserid);
		$teamArray    = $this->Admin_users->getNumberUserTeam($accountUserid);
		$loginArray   = $this->Admin_users->getUserLoginInfo($accountUserid);
		
		$userInfoArray['Signup_Date'] = date('M j, Y', strtotime($userInfoArray['DateSignup']));
		$userInfoArray['Projects'] = $projectArray['Projects'];
		$userInfoArray['Invoices'] = $invoiceArray['Invoices'];
		$userInfoArray['Clients']  = $clientArray['Clients'];
		$userInfoArray['Team']     = $teamArray['TeamMembers'];
		$userInfoArray['UserName'] = $loginArray['Userid'];
        $userInfoArray['LastLogin']= $loginArray['LastLogin'];
        $userInfoArray['LinkUsername'] = urlencode($userInfoArray['UserName']);
		if ( !empty($userInfoArray['LastLogin']) && ($userInfoArray['LastLogin'] != '0000-00-00') ) {
			$userInfoArray['LastLogin']= date('M j, Y', strtotime($userInfoArray['LastLogin']));
		} else  {
			$userInfoArray['LastLogin'] = '';	
		}
        if ( !empty($userInfoArray['DatePaid']) && ($userInfoArray['DatePaid'] != '0000-00-00') ) {
			$userInfoArray['DatePaid']= date('M j, Y', strtotime($userInfoArray['DatePaid']));
		} else  {
			$userInfoArray['DatePaid'] = '';
		}
		$userInfoArray['Logins']   = $loginArray['Logins'];
        $userInfoArray['PlanName'] = strtoupper($accountLevelArray[$userInfoArray['AccountLevel']]['name']);
		$userInfo = json_encode($userInfoArray);
		
		echo $userInfo;
	}
	
	function toggleUserStatus() {
		$this->Admin_users->toggleUserStatus($_POST['accountUserid']);
	}
}