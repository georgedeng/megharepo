<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MaintenanceTasks extends Controller {

	function __construct() {
		parent::Controller();
		$this->load->helper('auth');
		checkAuthentication($_POST);
	}
}
?>