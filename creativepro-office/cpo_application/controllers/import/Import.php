<?php
//ini_set('display_errors',1);
//ini_set('error_reporting', E_ALL);

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Import extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();

        $this->load->model('common/app_data','AppData',true);
		$this->load->model('clients/client_update','ClientUpdate',true);
		$this->load->model('clients/client_view','ClientView',true);
        $this->load->model('settings/settings_update','SettingsUpdate',TRUE);
        $this->load->model('contacts/contacts','Contacts',true);
        $this->load->model('users/user_login','UserLogin',true);

		$this->load->helper('dataImport');

        $this->load->library('application/CommonFileManager');
        $this->load->library('application/CommonAppData');
        $this->load->library('application/CommonEmail');

        //ini_set('display_errors',1);
        //ini_set('error_reporting', E_ALL);
	}

	function _init() {
        global $subMenuArray;
		$data['page']                  = 'import';
		$data['pageTitle']             = lang('import_page_title');
		$data['pageSubTitle']          = '';
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsImport.js');
		$data['pageIconClass']         = 'iconPageImport';
		$data['pageLayoutClass']       = 'noRightColumn';
		$data['rightColumnComponents'] = array();

        $permissionArray = $this->session->userdata('permissions');
		$viewAll = TRUE;

        noCache();
        return $data;
	}

    function index($status=NULL) {
        $data = Import::_init();
        switch ($status) {
            case 'success':
                $data['message'] = lang('import_success_message');
                break;
            case 'rollBack':
                $data['message'] = lang('import_rollBack_message');
                break;
            default:
                $data['message'] = '';
        }
        $data['pageLayoutClass'] = 'withRightColumn';
		$this->load->view('import/ImportDefault',$data);
    }

    function importClients($fileType=NULL) {
        $data = Import::_init();
        ini_set('auto_detect_line_endings', true);
        
        $newGUID = random_string('alnum',50);
        $this->session->set_userdata('importGUID',$newGUID);

        $importFilename = $this->session->userdata('importFilename');
        $csvFilePath = USER_DIR.$importFilename;
        $importType = $_POST['importType'];
        
        if (file_exists($csvFilePath)) {
            $file = fopen($csvFilePath, 'r');

            /*
             * Check POST collection for empty's.
             */            
            foreach($_POST as $key => $value) {
                if ($value == 'empty') {
                    $_POST[$key] = '';
                }
            }

            $rowCount = 0;
            while(!feof($file)) {
                $row = fgetcsv($file,0,',','"');

                if ($_POST['firstRowHeaders'] == 1 && $rowCount == 0) {
                    $rowCount++;
                    continue;
                }

                $clientArray['action']        = 'add';
                $clientArray['userid']        = $this->session->userdata('accountUserid');
                $clientArray['clientCompany'] = fieldToDB($row[$_POST['clientCompany']]);
                $clientArray['clientPhone1']  = fieldToDB($row[$_POST['clientPhone1']]);
                $clientArray['clientPhone2']  = fieldToDB($row[$_POST['clientPhone2']]);
                $clientArray['clientPhone3']  = fieldToDB($row[$_POST['clientPhone3']]);
                $clientArray['clientEmail']   = fieldToDB($row[$_POST['clientEmail']]);
                $clientArray['clientURL']     = fieldToDB($row[$_POST['clientURL']], TRUE,FALSE,TRUE);
                $clientArray['clientAddress'] = fieldToDB($row[$_POST['clientAddress']], TRUE,TRUE);
                $clientArray['clientCity']    = fieldToDB($row[$_POST['clientCity']]);
                $clientArray['clientState']   = fieldToDB($row[$_POST['clientState']]);
                $clientArray['clientZip']     = fieldToDB($row[$_POST['clientZip']]);
                $clientArray['clientCountry'] = $_POST['clientCountry'];
                $clientArray['clientTimezone']= $_POST['clientTimezone'];
                $clientArray['clientArchive'] = 0;
                $clientArray['clientStatus']  = 0;

                $clientArray['clientNotes']   = fieldToDB($row[$_POST['clientNotes']], TRUE,TRUE,FALSE,TRUE);
                $clientArray['clientSince']   = '';
                $clientArray['clientTags']    = '';

                $clientArray['newClientID'] = $this->ClientUpdate->saveClient($clientArray);

                /*
                 * Save client login information
                 */
                if (!empty($clientArray['clientEmail'])) {
                    $password = Import::saveClientLogin($clientArray);
                }

                /*
                 * Get some email settings
                 */
                $settingsEmailClientNew = $this->SettingsUpdate->getSettings($this->session->userdata('accountUserid'),'emailSendClientNew');
                if ($_POST['sendEmailToClients'] == 1 && $settingsEmailClientNew['SettingValue'] != 0 && !empty($clientArray['clientEmail'])) {
                    /*
                     * Send welcome email to client
                     */
                    $messageArray = $this->commonemail->createSystemMessage('clientNew',$clientArray['clientLanguage']);
                    $message = $messageArray['message'];
                    $subject = $messageArray['subject'];
                    /*
                     * Company, Company, User Name, Password
                     */
                    $messageIntro = sprintf($message,$this->session->userdata('userCompany'),$clientArray['clientCompany'],$this->session->userdata('userCompany'),$clientArray['clientEmail'],$password);
                    $footer = $this->commonemail->createSystemMessage('messageFooter',$clientArray['clientLanguage']);

                    $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$footer['message'],TRUE);
                    $this->commonemail->sendEmail($finalMessage,$loginUID,'client',$subject,'');
                }
                $rowCount++;
            }
            fclose($file);
            unlink($csvFilePath);
        } else {
            header('Location: '.site_url('import/Import'));
        }

        /*
         * Get data confirmation
         */
        $dataArray = $this->ClientView->getClientsForImportConfirm();
        $dataHeader = array(lang('client_form_company'),lang('client_form_address'),lang('client_form_phone1'),lang('client_form_email'),lang('client_form_url'));
        $dataContent = array();
        $a = 0;
        foreach($dataArray as $dataItem) {
            $dataContent[$a][0] = $dataItem['Company'];
            $dataContent[$a][1] = $dataItem['Address'];
            $dataContent[$a][2] = $dataItem['Phone1'];
            $dataContent[$a][4] = $dataItem['Email'];
            $dataContent[$a][5] = $dataItem['URL'];
            $a++;
        }
        $data['confirmHeader']  = $dataHeader;
        $data['confirmContent'] = $dataContent;
        $data['importType']     = $importType;
        $data['rowCount']       = $rowCount;
        $this->load->view('import/ImportConfirm',$data);
    }

    function importContacts($fileType=NULL) {
        $data = Import::_init();
        ini_set('auto_detect_line_endings', true);

        $newGUID = random_string('alnum',50);
        $this->session->set_userdata('importGUID',$newGUID);

        $importFilename = $this->session->userdata('importFilename');
        $csvFilePath = USER_DIR.$importFilename;
        $importType = $_POST['importType'];

        if (file_exists($csvFilePath)) {
            $file = fopen($csvFilePath, 'r');

            /*
             * Check POST collection for empty's.
             */
            foreach($_POST as $key => $value) {
                if ($value == 'empty') {
                    $_POST[$key] = '';
                }
            }

            $contactsFlag = $_POST['contactsContacts'];
            $vendorsFlag  = $_POST['contactsVendors'];
            $teamFlag     = $_POST['contactsTeam'];

            $rowCount = 0;
            while(!feof($file)) {
                $row = fgetcsv($file,0,',','"');

                if ($_POST['firstRowHeaders'] == 1 && $rowCount == 0) {
                    $rowCount++;
                    continue;
                }

                $contactArray['action']        = 'add';
                $contactArray['accountUserid'] = $this->session->userdata('accountUserid');
                $contactArray['teamMember']    = $teamFlag;
                $contactArray['vendor']        = $vendorsFlag;
                $contactArray['contact']       = $contactsFlag;

                $contactArray['nameFirst'] = fieldToDB($row[$_POST['nameFirst']]);
                $contactArray['nameLast']  = fieldToDB($row[$_POST['nameLast']]);
                $contactArray['title']     = fieldToDB($row[$_POST['title']]);
                $contactArray['company']   = fieldToDB($row[$_POST['company']]);
                $contactArray['address']   = fieldToDB($row[$_POST['address']]);
                $contactArray['city']      = fieldToDB($row[$_POST['city']]);
                $contactArray['state']     = fieldToDB($row[$_POST['state']]);
                $contactArray['zip']       = fieldToDB($row[$_POST['zip']]);
                $contactArray['country']   = fieldToDB($row[$_POST['country']]);
                $contactArray['timezone']  = fieldToDB($row[$_POST['timezone']]);

                $contactArray['phone']['data'][0] = fieldToDB($row[$_POST['phone_work']]);
                $contactArray['phone']['type'][0] = 'work';
                $contactArray['phone']['data'][1] = fieldToDB($row[$_POST['phone_mobile']]);
                $contactArray['phone']['type'][1] = 'mobile';
                $contactArray['phone']['data'][2] = fieldToDB($row[$_POST['phone_fax']]);
                $contactArray['phone']['type'][2] = 'fax';
                $contactArray['phone']['data'][3] = fieldToDB($row[$_POST['phone_home']]);
                $contactArray['phone']['type'][3] = 'home';
                $contactArray['phone']['data'][4] = fieldToDB($row[$_POST['phone_skype']]);
                $contactArray['phone']['type'][4] = 'skype';
                $contactArray['phone']['data'][5] = fieldToDB($row[$_POST['phone_pager']]);
                $contactArray['phone']['type'][5] = 'pager';
                $contactArray['phone']['data'][6] = fieldToDB($row[$_POST['phone_other']]);
                $contactArray['phone']['type'][6] = 'other';

                $contactArray['email']['data'][0] = fieldToDB($row[$_POST['email_work']]);
                $contactArray['email']['type'][0] = 'work';
                $contactArray['email']['data'][1] = fieldToDB($row[$_POST['email_personal']]);
                $contactArray['email']['type'][1] = 'personal';
                $contactArray['email']['data'][2] = fieldToDB($row[$_POST['email_other']]);
                $contactArray['email']['type'][2] = 'other';

                $contactArray['url']['data'][0] = fieldToDB($row[$_POST['url_work']]);
                $contactArray['url']['type'][0] = 'work';
                $contactArray['url']['data'][1] = fieldToDB($row[$_POST['url_personal']]);
                $contactArray['url']['type'][1] = 'personal';
                $contactArray['url']['data'][2] = fieldToDB($row[$_POST['url_other']]);
                $contactArray['url']['type'][2] = 'other';

                /*
                 * Filter bad im types
                 */
                $im_type_work     = strtolower(fieldToDB($row[$_POST['im_type_work']]));
                $im_type_personal = strtolower(fieldToDB($row[$_POST['im_type_personal']]));
                $im_type_other    = strtolower(fieldToDB($row[$_POST['im_type_other']]));

                $contactArray['im']['data'][0] = fieldToDB($row[$_POST['im_work']]).'|'.$im_type_work;
                $contactArray['im']['type'][0] = 'work';
                $contactArray['im']['data'][1] = fieldToDB($row[$_POST['im_personal']]).'|'.$im_type_personal;
                $contactArray['im']['type'][1] = 'personal';
                $contactArray['im']['data'][2] = fieldToDB($row[$_POST['im_other']]).'|'.$im_type_other;
                $contactArray['im']['type'][2] = 'other';

                if (
                    empty($contactArray['nameFirst']) &&
                    empty($contactArray['nameLast'])  &&
                    empty($contactArray['title'])     &&
                    empty($contactArray['company'])
                ) {
                    // Skip this record
                } else {
                    $contactArray['newClientID'] = $this->Contacts->saveContactPerson($contactArray);
                }
                $rowCount++;
            }
            fclose($file);
            unlink($csvFilePath);
        } else {
            header('Location: '.site_url('import/Import'));
        }

        /*
         * Get data confirmation
         */
        $dataArray = $this->Contacts->getContactsForImportConfirm();
        $dataHeader = array(lang('common_first_name'),lang('common_last_name'),lang('client_form_company'),lang('client_form_address'),lang('client_form_city'),lang('client_form_state'),lang('client_form_zip'));
        $dataContent = array();
        $a = 0;
        foreach($dataArray as $dataItem) {
            $dataContent[$a][0] = $dataItem['NameFirst'];
            $dataContent[$a][1] = $dataItem['NameLast'];
            $dataContent[$a][2] = $dataItem['Company'];
            $dataContent[$a][4] = $dataItem['Address'];
            $dataContent[$a][5] = $dataItem['City'];
            $dataContent[$a][6] = $dataItem['State'];
            $dataContent[$a][7] = $dataItem['Zip'];
            $a++;
        }
        $data['confirmHeader']  = $dataHeader;
        $data['confirmContent'] = $dataContent;
        $data['importType']     = $importType;
        $data['rowCount']       = $rowCount;
        $this->load->view('import/ImportConfirm',$data);
    }

    function mapFieldsCSV($importType) {
        //ini_set('display_errors',1);
        //ini_set('error_reporting',E_ALL);
        $data = Import::_init();        

        $importFilename = $this->session->userdata('importFilename');
        $filePath = USER_DIR.$importFilename;
        if (file_exists($filePath)) {
            /*
             * Try to parse the CSV file.
             */
             $fieldsArray = getCSVFirstRow($filePath);
             if ($fieldsArray != FALSE) {
                 
                 $data['mappedFieldsArray'] = makeMappedFieldsArray($importType);

                 switch ($importType) {
                    case 'clients':
                        $data['postURL'] = site_url('import/Import/importClients');
                        $data['importColText'] = lang('import_client_data_column');
                        break;
                    case 'contacts':
                        $data['postURL'] = site_url('import/Import/importContacts');
                        $data['importColText'] = lang('import_contact_data_column');
                        break;
                }
                $data['importType'] = $importType;
                $data['userFieldsArray'] = $fieldsArray;
                $data['importType'] = $importType;
                $this->load->view('import/ImportMapFields',$data);
             }
        }  else {
            header('Location: '.site_url('import/Import'));
        }
    }

    function saveClientLogin($clientArray) {
        include(SITE_CODE_PATH);
        $loginArray['action']        = 'add';
        $loginArray['Userid']        = $clientArray['clientEmail'];
        $password = random_string();
        $loginArray['Password']      = sha1($password.$siteCode);
        $loginArray['RoleID']        = '';
        $loginArray['UserType']      = USER_TYPE_CLIENT;
        $loginArray['UseridAccount'] = $this->session->userdata('accountUserid');

        $loginUID = $this->User_login->saveLoginInformation($loginArray);
        /*
         * Create authString and update
         */
        $authString              = sha1($loginUID.$siteCode);
        $authArray['UID']        = $loginUID;
        $authArray['action']     = 'updateAuthString';
        $authArray['authString'] = $authString;
        $this->User_login->saveLoginInformation($authArray);

        /*
         * Update client table with ClientUserid
         */
        $clientUseridArray['action']       = 'update';
        $clientUseridArray['clientUserid'] = $loginUID;
        $clientUseridArray['clientID']     = $clientArray['newClientID'];
        $newClientID = $this->ClientUpdate->saveClient($clientUseridArray);

        return $password;
    }

    function uploadImportFile() {
        $response = $this->commonfilemanager->uploadLocalFile(USER_DIR,$_FILES['Filedata'],MAX_FILE_UPLOAD_SIZE,'csv|xls|vcd');
        /*
         * Do we have an error?
         */
        $responseArray = explode('|',$response);
        if (is_array($responseArray) && $responseArray[0] == 'error') {
            echo '{"Result":"error","Message":"'.$responseArray[1].'"}';
        } else {
            $fileName = $response;
            $this->session->set_userdata('importFilename',$fileName);
            echo '{"Result":"success","Filename":"'.$fileName.'"}';
        }
    }

    function rollBackImport($importType) {
        switch ($importType) {
            case 'clients':
                $this->ClientUpdate->rollBackImport();
                Import::index('rollBack');
            case 'contacts':
                break;
        }
    }
}
?>