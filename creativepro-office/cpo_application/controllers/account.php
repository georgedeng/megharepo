<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
        $this->load->model('accounts/accounts','',true);

        $this->load->library('application/CommonEmail');

        $this->load->helper('account');
        $this->load->helper('payment');
        $this->load->helper('currency');
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtAccountHeader_public.png';
        $data['rightColumnComponents'] = array('loginBox','whosUsingCPO');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Change your plan';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        $data['currentAccountLevel'] = $this->session->userdata('accountLevel');

        switch ($data['currentAccountLevel']) {
            case 0:
                $class1 = 'green1';
                $class2 = 'blue2';
                $class3 = 'blue3';
                $class4 = 'blue4';
                $buttonState1 = 'none';
                $buttonState2 = '';
                $buttonState3 = '';
                $buttonState4 = '';
                break;
            case 1:
                $class1 = 'blue1';
                $class2 = 'green1';
                $class3 = 'blue3';
                $class4 = 'blue4';
                $buttonState1 = '';
                $buttonState2 = 'none';
                $buttonState3 = '';
                $buttonState4 = '';
                break;
            case 2:
                $class1 = 'blue1';
                $class2 = 'blue2';
                $class3 = 'green1';
                $class4 = 'blue4';
                $buttonState1 = '';
                $buttonState2 = '';
                $buttonState3 = 'none';
                $buttonState4 = '';
                break;
            case 3:
                $class1 = 'blue1';
                $class2 = 'blue2';
                $class3 = 'blue3';
                $class4 = 'green1';
                $buttonState1 = '';
                $buttonState2 = '';
                $buttonState3 = '';
                $buttonState4 = 'none';
                break;
        }
        $data['class1'] = $class1;
        $data['class2'] = $class2;
        $data['class3'] = $class3;
        $data['class4'] = $class4;
        $data['buttonState1'] = $buttonState1;
        $data['buttonState2'] = $buttonState2;
        $data['buttonState3'] = $buttonState3;
        $data['buttonState4'] = $buttonState4;
        return $data;
    }

    function changeAccount($accountUserid) {
        global $accountPricePrev,$accountPriceNext,$datePriceChange;
        $data = Account::_init();

        /*
         * We are only supposed to be here if we have an accountUserid.
         */
        if (empty($accountUserid)) {
            header('Location: '.site_url('pricing'));
        }
        $data['currentPrices'] = $accountPricePrev;
        if (days_between_dates(strtotime($datePriceChange),time())>0) {
            $data['currentPrices'] = $accountPriceNext;
        }

        /*
         * Retrieve account data and put it into session.
         */
        $accountArray = spinUpAccountSession($accountUserid);
        $this->load->view('public_site/Account',$data);
    }

    function checkPayPalProfiles() {
        $profileArray = $this->accounts->getAllAccountProfiles();
        foreach($profileArray as $profile) {
            /*
             * Go check PayPal status
             */
            $profileID = urlencode($profile['PaymentProfileID']);
            $httpParsedResponseAr = PPHttpPost('GetRecurringPaymentsProfileDetails', '&PROFILEID='.$profileID);

            if(strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESS" || strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESSWITHWARNING") {
                               
                $status = urldecode($httpParsedResponseAr['STATUS']);            
                if ($status == 'Suspended') {
                    /*
                     * Log error to database
                     */
                    $errorArray['errorType']     = 'PayPal';
                    $errorArray['errorCode']     = 'SUSPENDED ACCOUNT';
                    $errorArray['accountUserid'] = $profile['AccountID'];
                    $errorArray['errorMessage']  = 'Account suspended by PayPal: Max failed billing attempts reached.';
                    $errorArray['errorPage']     = NULL;
                    $errorLogID = $this->accounts->enterPaymentErrorLog($errorArray);

                    /*
                     * Send email notification to CPO support
                     */
                    $messageAdmin  = 'Someone encountered an error while paying.'.Chr(10);
                    $messageAdmin .= 'Account ID: '.$profile['AccountID'].Chr(10);
                    $messageAdmin .= 'Profile ID: '.$profile['PaymentProfileID'].Chr(10);
                    $messageAdmin .= 'Error Code: '.$errorArray['errorCode'].Chr(10);
                    $messageAdmin .= 'Error Message: '.$errorArray['errorMessage'];
                    $subjectAdmin = "CreativePro Office: Account payment problem";
                    $this->commonemail->sendEmailToAdmin($messageAdmin,$subjectAdmin);

                    /*
                     * Update paid status in account table
                     */
                    $this->accounts->changeAccountPaidStatus($profile['AccountID'],'0');

                    /*
                     * Send email to account owner with subject line of
                     * "CreativePro Office Payment Error"
                     */
                }
            }
        }
    }
    
    function createPayPalReceipts() {
        $profileArray = $this->accounts->getAllAccountProfiles();
        foreach($profileArray as $profile) {
            /*
             * Go get PayPal shit
             */
            $profileID = urlencode($profile['PaymentProfileID']);
            $httpParsedResponseAr = PPHttpPost('GetRecurringPaymentsProfileDetails', '&PROFILEID='.$profileID);
            
            if(strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESS" || strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESSWITHWARNING") {
                if ($httpParsedResponseAr['STATUS'] == 'Active') {
                    $data['payAddress']   = '';
                    $data['payCity']      = '';
                    $data['payState']     = '';
                    $data['payZip']       = '';
                    $data['payCountry']   = '';
                    
                    if (!isset($httpParsedResponseAr['LASTPAYMENTDATE']) || empty($httpParsedResponseAr['LASTPAYMENTDATE'])) {
                        $payDate = date('Y').'-'.date('m').'-'.'01';
                    } else {
                        $payDate = urldecode($httpParsedResponseAr['LASTPAYMENTDATE']);
                        $payDateArray = explode('T',$payDate);
                        $payDate = $payDateArray[0];
                    }
                    
                    $payAmt  = urldecode($httpParsedResponseAr['AMT']);
                    $payCurrency  = $httpParsedResponseAr['CURRENCYCODE'];
                    $payProfileID = urldecode($httpParsedResponseAr['PROFILEID']);
                    //$payName      = urldecode($httpParsedResponseAr['SUBSCRIBERNAME']);
                    $payName      = $profile['Company'];
                    $payStreet    = urldecode($httpParsedResponseAr['STREET']);
                    $payCity      = urldecode($httpParsedResponseAr['CITY']);
                    $payState     = urldecode($httpParsedResponseAr['STATE']);
                    $payZip       = urldecode($httpParsedResponseAr['ZIP']);
                    $payCountry   = urldecode($httpParsedResponseAr['COUNTRY']);
                    $payCardType  = urldecode($httpParsedResponseAr['CREDITCARDTYPE']);
                    $payCardNumber= urldecode($httpParsedResponseAr['ACCT']);
                    $payDesc      = urldecode($httpParsedResponseAr['DESC']);
                    
                    $accountID    = $profile['AccountID'];
                    $accountEmail = $profile['Email'];
                    
                    $nextPaymentDate = urldecode($httpParsedResponseAr['NEXTBILLINGDATE']);
                    $outstandingBalance = urldecode($httpParsedResponseAr['OUTSTANDINGBALANCE']);
                  
                    $currencyMark = getCurrencyMark($payCurrency);
                    
                    /*
                     * See if we already have an entry in the database.
                     */
                    $nextPaymentDateArray = explode('T',$nextPaymentDate);
                    $nextPaymentDate = $nextPaymentDateArray[0];
                    
                    $isPayment = true;
                    $isPayment = $this->accounts->checkPaymentEntry($accountID,$payProfileID,$payDate);
                    
                    /*
                     * If not, create a new entry.
                     */
                    if ($isPayment == false) {
                        $data['action']       = 'add';
                        $data['accountID']    = $accountID;
                        $data['payDate']      = $payDate;
                        $data['payAmt']       = $payAmt;
                        $data['payCurrency']  = $payCurrency;
                        $data['payProfileID'] = $payProfileID;
                        $data['payName']      = $payName;
                        $data['payAddress']   = $payStreet;
                        $data['payCity']      = $payCity;
                        $data['payState']     = $payState;
                        $data['payZip']       = $payZip;
                        $data['payCountry']   = $payCountry;
                        $data['payCardType']  = $payCardType;
                        $data['payCardNumber']= $payCardNumber;
                        
                        $paymentID = $this->accounts->savePaymentEntry($data);
                        $invoiceNo = 1000+$paymentID;
                        
                        /*
                         * And email a receipt to the payer.
                         */
                        $subject = 'CreativePro Office Receipt - Invoice #'.$invoiceNo;
                        $message  = 'Your CreativePro Office account was charged '.$currencyMark.$payAmt.' to the credit card ending in '.$payCardNumber.Chr(10).Chr(10);
                        $message .= '========================================================'.Chr(10);
                        $message .= 'INVOICE #'.$invoiceNo.'     '.$payDate.Chr(10);
                        $message .= '........................................................'.Chr(10);
                        $message .= 'GC Productions, Fairfax VA'.Chr(10).Chr(10);
                        $message .= 'BILL TO: '.$payName.Chr(10);
                        $message .= 'ACOUNT #'.$accountID.Chr(10).Chr(10);
                        $message .= 'BILLING SUMMARY'.Chr(10);
                        $message .= 'Monthly charge: '.$currencyMark.$payAmt.' '.$payCurrency.': '.$payDesc.Chr(10).Chr(10);
                        $message .= 'CREDIT CARD'.Chr(10);
                        $message .= $payCardType.' ending in '.$payCardNumber.Chr(10);  
                        $message .= '========================================================'.Chr(10);
                        $message .= '* Your next payment will be on '.$nextPaymentDate.Chr(10).Chr(10);
                        
                        $message .= 'Thank you for using CreativePro Office!'.Chr(10);
                        $message .= 'Contact us at: http://www.mycpohq.com/contact or support@mycpohq.com'.Chr(10);
                        $message .= 'Visit our blog for more information at blog.mycpohq.com';
                        
                        $this->commonemail->sendPaymentReceiptEmail($message,$accountEmail,$subject);
                    }
                }    
            }
        }
    }
}
?>
