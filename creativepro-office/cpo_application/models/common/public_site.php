<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Public_site extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getCPOCustomerInfo($howMany=NULL) {
        $sql = "SELECT * FROM cpo_customer_logos WHERE Active = 1 ORDER BY RAND() LIMIT $howMany";
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array();
        } else {
            return FALSE;
        }
    }

    function checkWebAddress($webAddress,$accountUserid=NULL) {
        if ($accountUserid>0) {
            $sql = "SELECT AccountID FROM cpo_account_profile WHERE SubDomain = '$webAddress' AND AccountID <> '$accountUserid'";
        } else {
            $sql = "SELECT AccountID FROM cpo_account_profile WHERE SubDomain = '$webAddress'";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function checkEmailPassword($email,$hashPassword) {
        $sql = "SELECT UID FROM cpo_login
                WHERE
                    Userid   = '$email' AND
                    Password = '$hashPassword' AND
                    Active = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function saveNewAccount($data) {
        /*
         * Create account entry
         */
        $sql = "INSERT into cpo_account_profile (
                Company,
                Email,
                Currency,
                SubDomain,
                AccountLevel,
                AccountPrice,
                Account_No,
                AuthString,
                DateSignup,
                DateExpire,
                AcceptTerms,
                GetCPOEmail,
                UserDir,
                AdLink,
                IsActive
                ) values (
                '".$data['companyName']."',
                '".$data['email']."',
                '".$data['currency']."',
                '".$data['webAddress']."',
                '".$data['selectedPlanNumber']."',
                '".$data['planCost']."',
                '".$data['newAccountNumber']."',
                '".$data['authStringAccount']."',
                '".date('Y-m-d')."',
                '".$data['dateExp']."',
                '1',
                '1',
                '".$data['authStringAccount']."',
                '".$data['adLink']."',    
                '1'
                )";
        $query = $this->db->query($sql);
        $accountID = $this->db->insert_id();

        /*
         * Create login entry
         */
        $sql = "INSERT into cpo_login (
                Userid,
                UseridAccount,
                Password,
                User_Type,
                AuthString,
                Active
                ) values (
                '".$data['email']."',
                '$accountID',
                '".$data['hashPassword']."',
                '".USER_TYPE_OWNER."',
                '".$data['authStringPersonal']."',
                '1'
                )";
        $query = $this->db->query($sql);
        $UID = $this->db->insert_id();
        
        /*
         * Create role entry
         */
        $sql = "INSERT into cpo_user_roles (
                Userid,
                RoleName,
                RolePermissions,
                UseridUpdate,
                DateUpdate
                ) values (
                '".$accountID."',
                '".$data['lang']['roleName']."',
                '".PERM_ADMIN."',
                '".$UID."',
                '".date('Y-m-d')."'
                )";
        $query = $this->db->query($sql);
        $roleID = $this->db->insert_id();

        /*
         * Update login table with new roleID
         */
        $sql = "UPDATE cpo_login SET RoleID = '$roleID' WHERE UID = '$UID'";
        $query = $this->db->query($sql);

        /*
         * Create people entry
         */
        $sql = "INSERT into cpo_people (
                Userid,
                LoginUserid,
                NameFirst,
                NameLast,
                Company,
                Timezone,
                Language,
                AccountCreator,
                UseridUpdate,
                DateEntry
                ) values (
                '".$accountID."',
                '".$UID."',
                '".$data['firstName']."',
                '".$data['lastName']."',
                '".$data['companyName']."',
                '".$data['timezone']."',
                '".$data['language']."',
                '1',
                '".$UID."',
                '".date('Y-m-d')."'
                )";
        $query = $this->db->query($sql);
        $personID = $this->db->insert_id();

        /*
         * Enter contact info for this person
         */
        $sql = "INSERT into cpo_people_contact_info (
                PID,
                ContactType,
                ContactData,
                ContactLocation,
                UseridUpdate,
                DateUpdate
                ) values (
                '$personID',
                'email',
                '".$data['email']."',
                'work',
                '".$UID."',
                '".date('Y-m-d')."'
                )";
        $query = $this->db->query($sql);

        /*
         * Enter default calendars
         */       
        $sql = "INSERT into cpo_calendar (
                Userid,
                Title,
                Color,
                DefaultCalendar,
                CalendarType,
                UseridUpdate,
                DateCreated
                ) values (
                '".$accountID."',
                '".$data['lang']['projectCalendar']."',
                'dark-blue',
                '1',
                'project',
                '".$UID."',
                '".date('Y-m-d')."'
                )";
        $query = $this->db->query($sql);

        $sql = "INSERT into cpo_calendar (
                Userid,
                Title,
                Color,
                DefaultCalendar,
                CalendarType,
                UseridUpdate,
                DateCreated
                ) values (
                '".$accountID."',
                '".$data['lang']['taskCalendar']."',
                'brown',
                '1',
                'task',
                '".$UID."',
                '".date('Y-m-d')."'
                )";
        $query = $this->db->query($sql);

        /*
         * Enter some default dashboard widgets
         */
        $sql = "INSERT into cpo_widgetsActive (UserID,WID,WOrder,WColumn) values ('".$UID."',7,0,0)"; // Projects
        $query = $this->db->query($sql);
        $sql = "INSERT into cpo_widgetsActive (UserID,WID,WOrder,WColumn) values ('".$UID."',1,0,1)"; // Invoices
        $query = $this->db->query($sql);
        $sql = "INSERT into cpo_widgetsActive (UserID,WID,WOrder,WColumn) values ('".$UID."',3,0,2)"; // Calendar
        $query = $this->db->query($sql);

        $sql = "INSERT into cpo_widgetsActive (UserID,WID,WOrder,WColumn) values ('".$UID."',5,1,0)"; // Notes
        $query = $this->db->query($sql);
        $sql = "INSERT into cpo_widgetsActive (UserID,WID,WOrder,WColumn) values ('".$UID."',14,1,1)"; // Timeclock
        $query = $this->db->query($sql);

        /*
         * Enter some default settings
         */

        /*
         * Header style
         */
        $sql = "INSERT into cpo_settings
                       (Userid,Setting,SettingValue,SettingGroup,SettingSection,UseridUpdate,DateUpdate)
                values ('".$UID."','headerStyle','default','member','user','".$UID."','".date('Y-m-d')."')";
        $query = $this->db->query($sql);

        /*
         * Email settings
         */
        $emailSettingArray = array(
            'emailReceiveProject',
            'emailReceiveProjectUpdate',
            'emailReceiveTask',
            'emailReceiveTaskUpdate',
            'emailReceiveTaskOverdue',
            'emailReceiveMessage',
            'emailReceiveCalendar',
            'emailReceiveEvent',
            'emailReceiveTimeClock'
        );

        foreach($emailSettingArray as $setting) {
            $sql = "INSERT into cpo_settings
                       (Userid,Setting,SettingValue,SettingGroup,SettingSection,UseridUpdate,DateUpdate)
                values ('".$UID."','".$setting."','1','member','email','".$UID."','".date('Y-m-d')."')";
            $query = $this->db->query($sql);
        }

        $sql = "INSERT into cpo_settings
                       (Userid,Setting,SettingValue,SettingGroup,SettingSection,UseridUpdate,DateUpdate)
                values ('".$accountID."','emailSendClientNew','1','account','email','".$UID."','".date('Y-m-d')."')";
        $query = $this->db->query($sql);
    }

    function saveNewAccountLevel($accountUserid,$newAccountLevel,$newAccountPrice) {
        $sql = "UPDATE cpo_account_profile SET 
                AccountLevel = '$newAccountLevel',
                AccountPrice = '$newAccountPrice'
                WHERE AccountID = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function getLastAccountNumber() {
        $sql = "SELECT MAX(Account_No) AS MaxAccountNo FROM cpo_account_profile";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
}
?>