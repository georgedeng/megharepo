<?
class Site_messages extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
        $this->load->model('settings/settings_update');
        $this->load->model('contacts/contacts');
    }    
    
    /**
	 * saveMessage : saves application messages.
	 *
	 * @access	public
	 * @param	string $to list of | delimited userid's for whom this message is intended
	 * @param   int    $from ID of person sending the message. If it's a client, it will be clientID.
	 *                       If it's a team member or account owner, it will be thier UID. 
	 * @param   string $fromType client,team,owner
	 * @param   string $message the text of the message.
	 * @param   string $itemType if this message is in reference to something: invoice,project,etc.
	 * @param   int    $itemID the ID for the item mentioned in $itemType
	 * @param   int    $replyTo the MID of the message we're replying to if this is a reply.
	 * @return	int    $messageID ID of recently inserted message
	 */	
    function saveMessage($to,$toType,$fromType,$message,$itemType=NULL,$itemID=NULL,$replyTo=NULL,$userid,$accountUserid) {
    	/*
    	 * Save message first
    	 */
		$sql = "INSERT INTO cpo_messages (
                Userid,
				UIDFrom,
				TypeFrom,
				ItemID,
				ItemType,
				Message,
				DateMessage,
				ReplyToMID
				) values (
                '".$accountUserid."',
				'".$userid."',
				'$fromType',
				'$itemID',
				'$itemType',
				'$message',
				'".date('Y-m-d G:i:s')."',
				'$replyTo'
				)";
		$query = $this->db->query($sql); 
		$messageID = $this->db->insert_id();

		/*
		 * Now save message to
		 */
		$toArray = explode('|',$to);
        $toTypeArray = explode('|',$toType);
        $a=0;
		foreach($toArray as $to) {
            $toType = $toTypeArray[$a];
			if ($to>0) {
                //if ($toType == 'client' || $toType == 'team' || $toType == 'owner') {
                if ($toType == 'client') {
                    /*
                     * If we're sending a message to a client, enter the client user ID
                     * because we don't have a people ID.
                     */
                    $sql = "INSERT INTO cpo_linkMemberMessage (
                        Userid,
                        UIDTo,
                        MID
                    ) values (
                        '$accountUserid',
                        '$to',
                        '$messageID'
                    )";
                    Site_messages::updateMessageNotification($messageID,$to,'',TRUE);
                } else {
                    /*
                     * If we're sending message to a team member, vendor, contact, or owner,
                     * then enter the people ID.
                     */
                    $sql = "INSERT INTO cpo_linkMemberMessage (
                        Userid,
                        PIDTo,
                        MID
                    ) values (
                        '$accountUserid',
                        '$to',
                        '$messageID'
                    )";
                    Site_messages::updateMessageNotification($messageID,$accountUserid,$to,TRUE);
                }
				$query = $this->db->query($sql);
			}
            $a++;
		}
		return $messageID;
    }
    
    function getMessage($messageID) {
        $sql = "SELECT * from cpo_messages WHERE MID = $messageID";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    
    function getMessages(
        $itemID=NULL,
        $itemType=NULL,
        $viewAll=FALSE,
        $toUserOnly=FALSE,
        $unread=NULL,
        $userid=NULL,
        $accountUserid=NULL,
        $pID=NULL,
        $start=NULL,
        $limit=NULL,
        $notNotified=NULL
    ) {
		/*
         * So we have 3 basic scenarios here:
         * 1.  View only those messages sent to me (viewAll == FALSE)
         * 2.  View all messages sent among users in my account. (viewAll == TRUE)
         * 3.  View all messages sent to me or to all AND all message sent BY me. (viewAll == ? AND toUserOnly == TRUE)
         */

        $where = '';
        $joinLM = '';
        $readWhere = '';

		if ($itemID>0) {
			$where .= " M.ItemID = '$itemID' AND ";
		}
        if (!empty($itemType)) {
            $where .= " M.ItemType = '$itemType' AND ";
        }

        if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
            /*
             * If we're a client.
             */
            $where .= " (LM.UIDTo = '".$userid."' OR M.UIDFrom = '".$userid."') ";
            $joinLM = " LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID ";
            $readWhere = " AND LMR.UID = '$userid' ";

        } elseif ($viewAll == FALSE) {
            /*
             * If we can only view our owm messages (to or from us)
             * 8/6/2013 added the "OR M.UIDFrom = $userid" to ensure
             * that we retrieve all messages that we write.
             */
            $where .= " (LM.PIDTo = '".$pID."' OR M.UIDFrom = $userid) ";
            $joinLM = " LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID ";
            $readWhere = " AND LMR.PID = '$pID' ";

        } else {
            /*
             * If we're account owner or super user with view all priveledges.
             */
            $where .= " M.Userid = '".$accountUserid."'";
            $readWhere = " AND LMR.PID = '$pID' ";
        }

        if ($toUserOnly == TRUE) {
            $where .= " AND M.UIDFrom <> '".$userid."' ";
        }

        $sql = "SELECT
				M.MID,M.UIDFrom,M.TypeFrom,M.ItemID,M.ItemType,M.Subject,M.Message,
				M.DateMessage,M.DateRead,M.ReplyToMID
				FROM cpo_messages M
				$joinLM
                LEFT JOIN cpo_linkMemberMessageRead LMR ON LMR.MID = M.MID
				WHERE
					$where";
    	if ($unread == 1) {
    		$sql .= " AND LMR.DateRead IS NULL";
    	}
		if ($notNotified == 1) {
    		$sql .= " AND (LMR.Notified = '0' OR LMR.Notified IS NULL) ";
    	}

        if ($unread == 1 || $notNotified == 1) {
            $sql .= $readWhere;
        }
		$sql .= " ORDER BY M.DateMessage DESC ";
        $query = $this->db->query($sql);
    	$j=0;
    	$messageArray = array();
		$mID = '';
    	foreach ($query->result_array() as $row) {
			if ($row['MID'] != $mID) {
				if($row['TypeFrom'] == USER_TYPE_CLIENT) {
					/*
					 * We have a client
					 */
					$sql2 = "SELECT Company
								FROM cpo_clients
								WHERE ClientUserid = '".$row['UIDFrom']."'";
					$query2 = $this->db->query($sql2);
					$row2 = $query2->row_array();
					$row['FromName'] = '';
					$row['Company']  = $row2['Company'];
                    $row['PIDFrom']  = '';
					$avatarURL = '';
				} else {
					/*
					 * We have a team member
					 */
                    $contactArray = Contacts::getContactPerson(NULL,$row['UIDFrom']);
					$row['FromName'] = $contactArray['NameFirst'].' '.$contactArray['NameLast'];
					$row['Company']  = $contactArray['Company'];
                    $row['PIDFrom']  = $contactArray['PID'];
					$avatarURL       = $contactArray['AvatarURL'];
				}

				$messageArray[$j]['MID']             = $row['MID'];
				$messageArray[$j]['ItemType']        = $row['ItemType'];
				$messageArray[$j]['ItemID']          = $row['ItemID'];
				$messageArray[$j]['UIDFrom']         = $row['UIDFrom'];
                $messageArray[$j]['PIDFrom']         = $row['PIDFrom'];
				$messageArray[$j]['TypeFrom']        = $row['TypeFrom'];
				$messageArray[$j]['FromName']        = $row['FromName'];
				$messageArray[$j]['Company']         = $row['Company'];
				$messageArray[$j]['Message']         = $row['Message'];
                $messageArray[$j]['DateMessage']     = $row['DateMessage'];
                $messageArray[$j]['DateRead']        = $row['DateRead'];
				$messageArray[$j]['ReplyTo']         = $row['ReplyToMID'];
				$messageArray[$j]['Avatar']          = $avatarURL;
				$j++;

				$mID = $row['MID'];
			}
    	}
    	return $messageArray;        
    }

	/**
      * getNumberOfMessagesForSomething
	  *
      * @access	public
	  * @param  int    $itemID ID of the item for which we want the file count
	  * @param  string $itemType the type of item for which we want the file count
	  *                This could be project, task, message, etc.
      * @return array  [The number of total messages],[The number of unread messages]
      */
	function getNumberOfMessagesForSomething($itemID=NULL,$itemType=NULL,$userid=NULL,$accountUserid=NULL,$pID=NULL,$viewAll=FALSE) {
		/*
		 * Get number of messages for an item
         * (project, client, invoice, expense, etc.)
		 */

        if ($itemID>0) {
            if ($viewAll == TRUE) {
                $sql = "SELECT COUNT(M.MID) AS NoOfMessagesTotal
                        FROM cpo_messages M
                        WHERE
                            M.ItemID   = $itemID AND
                            M.ItemType = '$itemType'";
            } else {
                /*
                 * 8/6/2013 added the "OR M.UIDFrom = $userid" to ensure
                 * that we retrieve all messages that we write.
                 */
                $sql = "SELECT COUNT(M.MID) AS NoOfMessagesTotal
                        FROM cpo_messages M
                        LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                        WHERE
                            (LM.PIDTo = '".$pID."' OR M.UIDFrom = $userid) AND
                            M.ItemID   = $itemID AND
                            M.ItemType = '$itemType'";
            }
            $query = $this->db->query($sql);
            $row1 = $query->row_array();

            if (!empty($userid)) {
                if ($viewAll == TRUE) {
                    $sql = "SELECT COUNT(M.MID) AS NoOfMessagesUnread
                            FROM cpo_messages M
                            LEFT JOIN cpo_linkMemberMessageRead LMR ON LMR.MID = M.MID
                            WHERE
                                LMR.DateRead IS NULL AND
                                M.ItemID   = $itemID AND
                                M.ItemType = '$itemType' AND
                                M.UIDFrom <> ".$userid;
                } else {
                    $sql = "SELECT COUNT(M.MID) AS NoOfMessagesUnread
                            FROM cpo_messages M
                            LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                            LEFT JOIN cpo_linkMemberMessageRead LMR ON LMR.MID = M.MID
                            WHERE
                                LMR.DateRead IS NULL AND
                                LM.PIDTo = '".$pID."' AND
                                M.ItemID   = $itemID AND
                                M.ItemType = '$itemType' AND
                                M.UIDFrom <> ".$userid;
                }
                $query = $this->db->query($sql);
                $row2 = $query->row_array();
            } else {
                $row2['NoOfMessagesUnread'] = 0;
            }
        } else {

            /*
             * Just get the number of messages for section
             * (projects, invoices, expenses, clients, etc.)
             * Or, get the total number of messages for a client
             * in the client view.
             */

            if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
                /*
                 * If we're a client.
                 */
                $sql = "SELECT COUNT(M.MID) AS NoOfMessagesTotal
                        FROM cpo_messages M
                        LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                        WHERE
                            LM.UIDTo = '$userid' OR
                            M.UIDFrom = '$userid'";
                $query = $this->db->query($sql);
                $row1 = $query->row_array();

                $sql = "SELECT COUNT(M.MID) AS NoOfMessagesUnread
                        FROM cpo_messages M
                        LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                        LEFT JOIN cpo_linkMemberMessageRead LMR ON LMR.MID = M.MID
                        WHERE
                            (LM.UIDTo = '$userid' OR M.UIDFrom = '$userid') AND
                            LMR.DateRead IS NULL";
                $query = $this->db->query($sql);
                $row2 = $query->row_array();

            } else {

                /*
                 * Get messages for an area like projects,
                 * invoices, expenses, clients, etc.
                 */

                if ($viewAll == TRUE) {
                    $sql = "SELECT COUNT(M.MID) AS NoOfMessagesTotal
                            FROM cpo_messages M
                            WHERE
                                M.Userid   = '$accountUserid' AND
                                M.ItemType = '$itemType'";
                } else {
                    $sql = "SELECT COUNT(M.MID) AS NoOfMessagesTotal
                            FROM cpo_messages M
                            LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                            WHERE
                                LM.PIDTo   = '$pID' AND
                                M.ItemType = '$itemType'";
                }
                $query = $this->db->query($sql);
                $row1 = $query->row_array();

                if ($viewAll == TRUE) {
                    $sql = "SELECT COUNT(M.MID) AS NoOfMessagesUnread
                            FROM cpo_messages M
                            LEFT JOIN cpo_linkMemberMessageRead LMR ON LMR.MID = M.MID
                            WHERE
                                M.Userid = '$accountUserid' AND
                                M.ItemType = '$itemType' AND
                                LMR.DateRead IS NULL AND
                                M.UIDFrom <> ".$userid;
                } else {
                    $sql = "SELECT COUNT(M.MID) AS NoOfMessagesUnread
                            FROM cpo_messages M
                            LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                            LEFT JOIN cpo_linkMemberMessageRead LMR ON LMR.MID = M.MID
                            WHERE
                                LM.PIDTo = '$pID' AND
                                M.ItemType = '$itemType' AND
                                LMR.DateRead IS NULL";
                }
                $query = $this->db->query($sql);
                $row2 = $query->row_array();
            }
        }

		return array(
						'NoOfMessagesTotal'  => $row1['NoOfMessagesTotal'],
						'NoOfMessagesUnread' => $row2['NoOfMessagesUnread']
					);
	}	

    /*
     * getMessagesForUpdateWindow: This retrieves messages for the growl-like popup
     * message notifier - not the little message box in the lower-right. That's
     * handled by getMessages method.
     */
	function getMessagesForUpdateWindow($accountUserid=NULL,$userid=NULL,$pID=NULL,$viewAll=FALSE) {
        /*
        if ($viewAll == FALSE) {
            $whereUser = " (LM.UIDTo = '$userid' OR LM.PIDTo = '$pID') ";
        } else {
            //$whereUser = " (LM.UIDTo = '$userid' OR LM.UIDTo = '$accountUserid' OR LM.PIDTo = '$pID') ";
            $whereUser = " (LM.UIDTo = '$userid' OR LM.PIDTo = '$pID') ";
        }
        */
        $sql = "SELECT
				M.MID,M.UIDFrom,M.TypeFrom,M.ItemID,M.ItemType,M.Subject,M.Message,
				M.DateMessage,M.DateRead,M.ReplyToMID
				FROM cpo_messages M
				LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                LEFT JOIN cpo_linkMemberMessageRead LMR ON LMR.MID = M.MID
				WHERE
					LMR.PID = '$pID'
					AND LMR.Notified = 0
					AND M.Deleted = 0";
		$query = $this->db->query($sql);
    	$j=0;
    	$messageArray = array();
		$mID = '';
    	foreach ($query->result_array() as $row) {
			if ($row['MID'] != $mID) {
				if($row['TypeFrom'] == USER_TYPE_CLIENT) {
					/*
					 * We have a client
					 */
					$sql2 = "SELECT Company
								FROM cpo_clients
								WHERE ClientUserid = '".$row['UIDFrom']."'";
					$query2 = $this->db->query($sql2);
					$row2 = $query2->row_array();
					$row['FromName'] = '';
					$row['Company']  = $row2['Company'];
                    $row['PIDFrom']  = '';
					$avatarURL = '';
				} else {
					/*
					 * We have a team member
					 */
                    $contactArray = Contacts::getContactPerson(NULL,$row['UIDFrom']);
					$row['FromName'] = $contactArray['NameFirst'].' '.$contactArray['NameLast'];
					$row['Company']  = $contactArray['Company'];
                    $row['PIDFrom']  = $contactArray['PID'];
					$avatarURL       = $contactArray['AvatarURL'];
				}

				$messageArray[$j]['MID']             = $row['MID'];
				$messageArray[$j]['ItemType']        = $row['ItemType'];
				$messageArray[$j]['ItemID']          = $row['ItemID'];
				$messageArray[$j]['UIDFrom']         = $row['UIDFrom'];
                $messageArray[$j]['PIDFrom']         = $row['PIDFrom'];
				$messageArray[$j]['TypeFrom']        = $row['TypeFrom'];
				$messageArray[$j]['FromName']        = $row['FromName'];
				$messageArray[$j]['Company']         = $row['Company'];
				$messageArray[$j]['Message']         = $row['Message'];
                $messageArray[$j]['DateMessage']     = $row['DateMessage'];
                $messageArray[$j]['DateRead']        = $row['DateRead'];
				$messageArray[$j]['ReplyTo']         = $row['ReplyToMID'];
				$messageArray[$j]['Avatar']          = $avatarURL;
				$j++;

				$mID = $row['MID'];
			}
    	}
		/*
		 * Update notification status for these messages
		 */
		Site_messages::updateMessageNotification(NULL,$userid,$pID);
    	return $messageArray;
	}

    function updateMessageNotification($messageID=NULL,$userid=NULL,$pID=NULL,$newMessage=FALSE) {
		if ($userid != NULL) {
            $where = " UID = '$userid' OR PID = '$pID' ";
			$sql = "UPDATE cpo_linkMemberMessageRead SET Notified = 1 WHERE UID = '$userid' OR PID = '$pID'";
		} elseif ($messageID>0) {
            $where = " MID = '$messageID' ";
			$sql = "UPDATE cpo_linkMemberMessageRead SET Notified = 1 WHERE MID = '$messageID'";
		}
        if ($newMessage == TRUE && $messageID>0) {
                $sql = "INSERT into cpo_linkMemberMessageRead (
                        MID,
                        UID,
                        PID,
                        Notified
                    ) values (
                        '$messageID',
                        '$userid',
                        '$pID',
                        0
                    )";
        } elseif ($this->messageReadRecord($messageID, $userid, $pID) == FALSE) {
            $sql = "INSERT into cpo_linkMemberMessageRead (
                        MID,
                        UID,
                        PID,
                        Notified
                    ) values (
                        '$messageID',
                        '$userid',
                        '$pID',
                        1
                    )";
        } else {
            $sql = "UPDATE cpo_linkMemberMessageRead SET
                        Notified = 1
                    WHERE $where";
        }
        //echo $sql;
        $query = $this->db->query($sql);
	}

    function messageReadRecord($messageID,$userid=NULL,$pID=NULL) {
        $whereTo = '';
        if ($pID>0) {
            $whereTo = " AND PID = '$pID' ";
        } elseif ($userid>0) {
            $whereTo = " AND UID = '$userid' ";
        }
        $sql = "SELECT LinkID FROM cpo_linkMemberMessageRead
                WHERE MID = '$messageID' $whereTo ";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function markMessageRead($messageID,$userid=NULL,$pID=NULL) {
        $whereTo = '';
        if ($pID>0) {
            $whereTo = " AND PID = '$pID' ";
        } elseif ($userid>0) {
            $whereTo = " AND UID = '$userid' ";
        }

        if ($this->messageReadRecord($messageID, $userid, $pID) == FALSE) {
            $sql = "INSERT into cpo_linkMemberMessageRead (
                        MID,
                        UID,
                        PID,
                        DateRead
                    ) values (
                        '$messageID',
                        '$userid',
                        '$pID',
                        '".date('Y-m-d H:i:s')."'
                    )";
        } else {
            $sql = "UPDATE cpo_linkMemberMessageRead SET
                        DateRead = '".date('Y-m-d H:i:s')."'
                    WHERE MID = '$messageID' $whereTo ";
        }
        $query = $this->db->query($sql);
    }

    function getMessagesForExport($accountUserid,$messageType) {
        if ($messageType == 'project') {
            $sql = "SELECT
                        M.Message,M.DateMessage,M.DateRead,P.Title,PP.NameFirst,PP.NameLast
                    FROM
                        cpo_messages M
                    LEFT JOIN
                        cpo_project P ON P.ProjectID = M.ItemID
                    LEFT JOIN
                        cpo_people PP ON PP.LoginUserid = M.UIDFrom
                    WHERE
                        M.Userid = '$accountUserid' AND
                        M.ItemType = 'project' AND
                        M.TypeFrom <> ".USER_TYPE_CLIENT." AND
                        M.Deleted = 0
                    ORDER BY
                        M.DateMessage DESC";
        } elseif ($messageType == 'client') {
            $sql = "SELECT
                        M.Message,M.DateMessage,M.DateRead,C.Company AS Title,PP.NameFirst,PP.NameLast
                    FROM
                        cpo_messages M
                    LEFT JOIN
                        cpo_clients C ON C.ClientID = M.ItemID
                    LEFT JOIN
                        cpo_people PP ON PP.LoginUserid = M.UIDFrom
                    WHERE
                        M.Userid = '$accountUserid' AND
                        M.ItemType = 'client' AND
                        M.TypeFrom <> ".USER_TYPE_CLIENT." AND
                        M.Deleted = 0
                    ORDER BY
                        M.DateMessage DESC";
        } elseif ($messageType == 'invoice') {
            $sql = "SELECT
                        M.Message,M.DateMessage,M.DateRead,I.InvNo,I.Title AS Title,PP.NameFirst,PP.NameLast
                    FROM
                        cpo_messages M
                    LEFT JOIN
                        cpo_invoice I ON I.InvoiceID = M.ItemID
                    LEFT JOIN
                        cpo_people PP ON PP.LoginUserid = M.UIDFrom
                    WHERE
                        M.Userid = '$accountUserid' AND
                        M.ItemType = 'invoice' AND
                        M.TypeFrom <> ".USER_TYPE_CLIENT." AND
                        M.Deleted = 0
                    ORDER BY
                        M.DateMessage DESC";
        } elseif ($messageType == 'expense') {
            $sql = "SELECT
                        M.Message,M.DateMessage,M.DateRead,E.Title AS Title,PP.NameFirst,PP.NameLast
                    FROM
                        cpo_messages M
                    LEFT JOIN
                        cpo_expense E ON E.ExpenseID = M.ItemID
                    LEFT JOIN
                        cpo_people PP ON PP.LoginUserid = M.UIDFrom
                    WHERE
                        M.Userid = '$accountUserid' AND
                        M.ItemType = 'expense' AND
                        M.TypeFrom <> ".USER_TYPE_CLIENT." AND
                        M.Deleted = 0
                    ORDER BY
                        M.DateMessage DESC";
        } elseif ($messageType == 'task') {
            $sql = "SELECT
                        M.Message,M.DateMessage,M.DateRead,T.Title AS Title,PP.NameFirst,PP.NameLast
                    FROM
                        cpo_messages M
                    LEFT JOIN
                        cpo_project_tasks T ON T.TaskID = M.ItemID
                    LEFT JOIN
                        cpo_people PP ON PP.LoginUserid = M.UIDFrom
                    WHERE
                        M.Userid = '$accountUserid' AND
                        M.ItemType = 'task' AND
                        M.TypeFrom <> ".USER_TYPE_CLIENT." AND
                        M.Deleted = 0
                    ORDER BY
                        M.DateMessage DESC";
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function checkForValidMessage($messageID,$pID,$accountUserid,$viewAll=FALSE) {
		$valid = FALSE;
		if ($viewAll == FALSE) {
			/*
			 * Make sure this message is sent to userid
			 */
            $sql = "SELECT *
                    FROM cpo_messages M
                    LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                    WHERE
                        LM.PIDTo = '".$pID."' AND
                        M.MID = $messageID";
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				$valid = TRUE;
			}
		} else {
			/*
			 * Make sure this message belongs to accountUserid
			 */
			$sql = "SELECT * 
                    FROM cpo_messages 
                    WHERE MID = $messageID AND 
                    Userid = '$accountUserid'";
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				$valid = TRUE;
			}
		}
		return $valid;
	}
}    
?>