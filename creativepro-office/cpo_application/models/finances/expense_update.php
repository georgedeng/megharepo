<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Expense_update extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    /**
	 * saveExpense : inserts new expense entries or updates existing entries
	 *
	 * @access	public
	 * @param	array $data PHP array of data to save
	 * @return	int   $expenseID the record id of the inserted or updated entry
	 */
    function saveExpense($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT INTO cpo_expense (
                        Userid,
                        ProjectID,
                        ClientID,
                        CatID,
                        VendorID,
                        Title,
                        DateExpense,
                        PaymentMethod,
                        AccountNumber,
                        Reimbursement,
                        Amount,
                        Markup,
                        MarkupType,
                        Comments,
                        Tags,
                        Recurring,
                        UseridEntry,
                        UseridUpdate,
                        DateEntry,
                        DateUpdate,
                        ImportGUID
                    ) values (
                        '".$data['accountUserid']."',
                        '".$data['projectID']."',
                        '".$data['clientID']."',
                        '".$data['expenseCategory']."',
                        '".$data['expenseVendor']."',
                        '".$data['expenseTitle']."',
                        '".$data['expenseDate']."',
                        '".$data['expensePaymentMethod']."',
                        '".$data['expenseAcctNo']."',
                        '".$data['expenseReimburse']."',
                        '".$data['expenseAmount']."',
                        '".$data['expenseMarkup']."',
                        '".$data['markupType']."',
                        '".$data['expenseNotes']."',
                        '".$data['expenseTags']."',
                        '".$data['expenseRepeats']."',
                        '".$data['userid']."',
                        '".$data['userid']."',
                        '".date('Y-m-d G:i:s')."',
                        '".date('Y-m-d G:i:s')."',
                        '".$this->session->userdata('importGUID')."'
                    )";
            $query = $this->db->query($sql);
    		$expenseID = $this->db->insert_id();
		} elseif ($data['action'] == 'edit') {
            $sql = "UPDATE cpo_expense SET
                        ProjectID     = '".$data['projectID']."',
                        ClientID      = '".$data['clientID']."',
                        CatID         = '".$data['expenseCategory']."',
                        VendorID      = '".$data['expenseVendor']."',
                        Title         = '".$data['expenseTitle']."',
                        DateExpense   = '".$data['expenseDate']."',
                        PaymentMethod = '".$data['expensePaymentMethod']."',
                        AccountNumber = '".$data['expenseAcctNo']."',
                        Reimbursement = '".$data['expenseReimburse']."',
                        Amount        = '".$data['expenseAmount']."',
                        Markup        = '".$data['expenseMarkup']."',
                        MarkupType    = '".$data['markupType']."',
                        Comments      = '".$data['expenseNotes']."',
                        Tags          = '".$data['expenseTags']."',
                        Recurring     = '".$data['expenseRepeats']."',
                        UseridUpdate  = '".$this->session->userdata('userid')."',
                        DateUpdate    = '".date('Y-m-d G:i:s')."'
                    WHERE
                        ExpenseID = '".$data['expenseID']."'";
            $query = $this->db->query($sql);
            $expenseID = $data['expenseID'];
		}       

		return $expenseID;
	}

    function deleteExpense($expenseID,$tagForDelete=TRUE,$userid) {
         if ($tagForDelete == TRUE) {
             $sql = "UPDATE cpo_expense SET UseridUpdate = '$userid', Deleted = 1 WHERE ExpenseID = '$expenseID'";
             $query = $this->db->query($sql);
         } else {
             $sql = "DELETE from cpo_expense WHERE ExpenseID = '$expenseID'";
             $query = $this->db->query($sql);
         }
    }

    function restoreExpense($expenseID,$userid) {
        $sql = "UPDATE cpo_expense SET UseridUpdate = '$userid', Deleted = 0 WHERE ExpenseID = '$expenseID'";
        $query = $this->db->query($sql);
    }

    function makeTemplateRecords($templateProjectID,$newProjectID,$userid) {
        $sql = "SELECT * from cpo_expense WHERE ProjectID = $templateProjectID AND Deleted = 0";
        $query = $this->db->query($sql);

        if ($query->num_rows()>0) {
            foreach ($query->result_array() as $row) {
                $sql = "INSERT into cpo_expense (
                        Userid,
                        ProjectID,
                        ClientID,
                        CatID,
                        VendorID,
                        Title,
                        DateExpense,
                        PaymentMethod,
                        AccountNumber,
                        Reimbursement,
                        Amount,
                        Markup,
                        MarkupType,
                        Comments,
                        Tags,
                        Recurring,
                        IsTemplate,
                        UseridEntry,
                        DateEntry
                        ) values (
                        '".$row['Userid']."',
                        '".$newProjectID."',
                        '".$row['ClientID']."',
                        '".$row['CatID']."',
                        '".$row['VendorID']."',
                        '".$row['Title']."',
                        '".date('Y-m-d')."',
                        '".$row['PaymentMethod']."',
                        '".$row['AccountNumber']."',
                        '".$row['Reimbursement']."',
                        '".$row['Amount']."',
                        '".$row['Markup']."',
                        '".$row['MarkupType']."',
                        '".$row['Comments']."',
                        '".$row['Tags']."',
                        '".$row['Recurring']."',
                        '1',
                        '".$userid."',
                        '".date('Y-m-d')."'
                        )";
                $query = $this->db->query($sql);
            }
        }
    }
    
    function saveExpenseNotes($expenseID,$expenseNotes) {
        $sql = "UPDATE cpo_expense SET
                Comments = '".$expenseNotes."'
                WHERE ExpenseID = $expenseID";
        $query = $this->db->query($sql);
    }
    
    function markExpenseBilled($expenseID, $invoiceID) {
        $sql = "UPDATE cpo_expense SET BilledInvoiceID = $invoiceID WHERE ExpenseID = $expenseID";
        $query = $this->db->query($sql);
    }
    
    function markExpensesUnbilled($invoiceID) {
        $sql = "UPDATE cpo_expense SET BilledInvoiceID = '' WHERE BilledInvoiceID = $invoiceID";
        $query = $this->db->query($sql);
    }
}
?>