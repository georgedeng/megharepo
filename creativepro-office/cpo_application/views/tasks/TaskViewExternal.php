<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="viewTasks">
	<div style="padding: 6px; width: 100%;">
	<?
	if (!empty($errorMessage)) {
		echo '<div class="errorMessageBig">'.$errorMessage.'</div>';
	} else {
		?>
		<h1 style="margin-bottom: 9px;"><?=$taskInfo['Title']; ?></h1>
		<div style="float: left; width: 50%;">
            <? if (isset($taskInfo['milestoneInfo']['Title'])) { ?>
			<p><strong><span title="<?=lang('task_milestone'); ?>: <?=$taskInfo['milestoneInfo']['Title']; ?>" class="icon_milestone_small stuff" style="display: inline-block; margin-right: 12px;"><?=$taskInfo['milestoneInfo']['Title']; ?></span>
			<? } ?>

            <? if (isset($taskInfo['projectInfo']['Title'])) { ?>
            <span class="icon_project_small" style="display: inline-block;" title="<?=lang('project_project'); ?>: <?=$taskInfo['projectInfo']['Title']; ?>">
            <?
            if ($this->session->userdata('logged') == 1) {
                echo '<a href="'.BASE_URL.'projects/ProjectDetail/index/'.$taskInfo['projectInfo']['ProjectID'].'">'.$taskInfo['projectInfo']['Title'].'</a>';
            } else {
                echo $taskInfo['projectInfo']['Title'];
            }
            ?>
            </span></strong></p>
			<? } ?>

            <span>
                <?
                if (!empty($taskInfo['DateString'])) {
                    echo $taskInfo['DateString'].' | ';
                }
                ?>
                <?=lang('common_created_by'); ?> <a href="#" class="personLink" personid="<?=$taskInfo['CreatorPID'] ;?>"><?=$taskInfo['CreatorName']; ?></a>
            </span>
		</div>
		<div style="float: left; width: 20%;">
			<?
            echo '<p>'.lang('common_assigned_to').'</p>';
			foreach($taskInfo['TeamMembers'] as $teamMember) {
				echo '<a href="#" class="personLink" personid="'.$teamMember['PID'].'">'.$teamMember['NameFirst'].' '.$teamMember['NameLast'].'</a><br />';
			}
			?>
		</div>
		<div style="float: left; width: 15%;">
			<span class="<?=$taskInfo['StatusMachine']; ?>" style="display: inline-block; margin-right: 12px;"></span> 
			<span class="icon_priority <?=$taskInfo['PriorityClass']; ?>" style="display: inline-block;"></span>
		</div>
		<div style="float: left; width: 15%;">
			<span>
			<?
			$redClass = '';
			if ($taskInfo['HoursActual'] > $taskInfo['HoursEstimated']) {
				$redClass = 'class="redText"';
			}
			?>
			<strong <?=$redClass; ?>>
				<?=$taskInfo['HoursActual']; ?> / <?=$taskInfo['HoursEstimated']; ?> <?=lang('date_hrs'); ?>
			</strong></span>
		</div>
		<div style="clear: left;"></div>
		<div style="margin: 6px 0 6px 0; width: 65%; float: left;"><?=$taskInfo['Description']; ?></div>
		<div style="margin: 6px 0 6px 0; width: 35%; float: left;">
			<?
			if (sizeof($taskInfo['Files']>0)) {
				echo '<ul class="fileList noBullet">';
				foreach($taskInfo['Files'] as $file) {
					echo '<li class="'.$file['FileClassName'].'_list fileLinkContainer"><a href="'.$file['path'].'" title="'.$file['FileNameActual'].'">'.$file['FileNameActual'].'</a></li>';
				}
				echo '</ul>';
			}
			?>
		</div>
		<div style="clear: left;"></div>
		<div>
			<?
			if (sizeof($taskInfo['messages'])>0) {
				foreach($taskInfo['messages'] as $message) {
					$avatarLink = '<img src="'.$message['Avatar'].'" class="avatar33 left noPrint" />';
					echo '<div class="messageContainer" id="'.$message['MID'].'">';
					echo '<div class="oddRow" style="padding: 2px;">';
					echo $avatarLink.'<div style="float: left;"><a href="#" class="personLink" pid="'.$message['UIDFrom'].'">'.$message['FromName'].'</a> <span class="subText">'.$message['DateSent'].'</span></div>';
					echo '<div style="clear: both;"></div></div>';
					echo '<p style="margin: 6px;">'.$message['Message'].'</p></div>';
				}
			}
			?>
		</div>
		<? } ?>
	</div>
</div>
<?
$this->load->view('includes/rightColumn');
$this->load->view('includes/footer');
?>
