<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Finance_reports extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getReportDataInvoicedReceived($userid,$year) {
        $sqlInvoiced = "SELECT
                            SUM(InvTotal) AS TotalInvoiced,
                            MONTHNAME(DateInvoice) AS TotalMonthInvoiced,
                            MONTH(DateInvoice) AS MonthNum
                        FROM cpo_invoice WHERE
                            YEAR(DateInvoice) = '$year' AND
                            Userid = '$userid' AND
                            Deleted = 0 AND
                            Estimate = 0
                        GROUP BY Monthname(DateInvoice)
                        ORDER BY DateInvoice";
        $sqlReceived = "SELECT
                            SUM(InvTotal) AS TotalReceived,
                            MONTHNAME(DatePaid) AS TotalMonthReceived,
                            MONTH(DatePaid) AS MonthNum
                        FROM cpo_invoice WHERE
                            YEAR(DatePaid) = '$year' AND
                            Userid = '$userid' AND
                            Estimate = 0 AND
                            Deleted = 0
                        GROUP BY Monthname(DatePaid)
                        ORDER BY DatePaid";
        $queryInvoiced = $this->db->query($sqlInvoiced);
        $queryReceived = $this->db->query($sqlReceived);

        $invoicedArray = $queryInvoiced->result_array();
        $receivedArray = $queryReceived->result_array();

        $resultArray = array(
                                'Invoiced'=>$invoicedArray,
                                'Received'=>$receivedArray
                            );
        return $resultArray;
    }

    function getReportDataInvoicedByProjectClient($userid,$reportDateStart,$reportDateEnd,$byWhat) {
        if ($byWhat == 'project') {
            $sql = "SELECT SUM(I.InvTotal) AS Total,P.Title AS Label
                    FROM cpo_invoice I
                    LEFT JOIN cpo_project P ON P.ProjectID = I.ProjectID
                    WHERE 
                        I.DateInvoice >= '$reportDateStart' AND
                        I.DateInvoice <= '$reportDateEnd' AND
                        I.Userid = '$userid' AND
                        I.Deleted = 0 AND
                        I.Estimate = 0
                    GROUP BY I.ProjectID
                    ORDER BY Total DESC";
        } elseif ($byWhat == 'client') {
            $sql = "SELECT SUM(I.InvTotal) AS Total,C.Company AS Label
                    FROM cpo_invoice I
                    LEFT JOIN cpo_clients C ON C.ClientID = I.ClientID
                    WHERE
                        I.DateInvoice >= '$reportDateStart' AND
                        I.DateInvoice <= '$reportDateEnd' AND
                        I.Userid = '$userid' AND
                        I.Deleted = 0 AND
                        I.Estimate = 0
                    GROUP BY I.ClientID
                    ORDER BY Total DESC";
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getInvoiceReportYearRange($userid) {
        $sql = "SELECT MIN(YEAR(DateInvoice)) AS MinYear FROM cpo_invoice WHERE Userid = '$userid'";
        $query = $this->db->query($sql);
    	if ($query->num_rows()>0) {
			 $row = $query->row_array();
			 return $row['MinYear'];
		 } else {
			 return false;
		 }
    }

    function getExpenseReportYearRange($userid) {
        $sql = "SELECT MIN(YEAR(DateExpense)) AS MinYear FROM cpo_expense WHERE Userid = '$userid'";
        $query = $this->db->query($sql);
    	if ($query->num_rows()>0) {
			 $row = $query->row_array();
			 return $row['MinYear'];
		 } else {
			 return false;
		 }
    }

    function getReportDataIncomeExpenses($userid,$year) {
        $sqlExpenses = "SELECT
                            SUM(Amount) AS TotalExpenses,
                            MONTHNAME(DateExpense) AS TotalMonthExpenses,
                            MONTH(DateExpense) AS MonthNum
                        FROM cpo_expense WHERE
                            YEAR(DateExpense) = '$year' AND
                            Userid = '$userid' AND
                            Deleted = 0
                        GROUP BY Monthname(DateExpense)
                        ORDER BY DateExpense";
        $sqlIncome = "SELECT
                            SUM(InvTotal) AS TotalIncome,
                            MONTHNAME(DatePaid) AS TotalMonthReceived,
                            MONTH(DatePaid) AS MonthNum
                        FROM cpo_invoice WHERE
                            YEAR(DatePaid) = '$year' AND
                            Userid = '$userid' AND
                            Deleted = 0
                        GROUP BY Monthname(DatePaid)
                        ORDER BY DatePaid";
        $queryExpenses = $this->db->query($sqlExpenses);
        $queryIncome   = $this->db->query($sqlIncome);

        $expensesArray = $queryExpenses->result_array();
        $incomeArray   = $queryIncome->result_array();

        $resultArray = array(
                                'Expenses' => $expensesArray,
                                'Income'   => $incomeArray
                            );
        return $resultArray;
    }

    function getReportDataExpensesBySegment($userid,$reportDateStart,$reportDateEnd,$byWhat) {
        if ($byWhat == 'project') {
            $sql = "SELECT SUM(E.Amount) AS Total,P.Title AS Label
                    FROM cpo_expense E
                    LEFT JOIN cpo_project P ON P.ProjectID = E.ProjectID
                    WHERE
                        E.DateExpense >= '$reportDateStart' AND
                        E.DateExpense <= '$reportDateEnd' AND
                        E.ProjectID > 0 AND
                        E.Userid = '$userid' AND
                        E.Deleted = 0
                    GROUP BY E.ProjectID
                    ORDER BY Total DESC";
        } elseif ($byWhat == 'client') {
            $sql = "SELECT SUM(E.Amount) AS Total,C.Company AS Label
                    FROM cpo_expense E
                    LEFT JOIN cpo_clients C ON C.ClientID = E.ClientID
                    WHERE
                        E.DateExpense >= '$reportDateStart' AND
                        E.DateExpense <= '$reportDateEnd' AND
                        E.ClientID > 0 AND
                        E.Userid = '$userid' AND
                        E.Deleted = 0
                    GROUP BY E.ClientID
                    ORDER BY Total DESC";
        } elseif ($byWhat == 'vendor') {
            $sql = "SELECT SUM(E.Amount) AS Total,P.Company AS Label
                    FROM cpo_expense E
                    LEFT JOIN cpo_people P ON P.PID = E.VendorID
                    WHERE
                        E.DateExpense >= '$reportDateStart' AND
                        E.DateExpense <= '$reportDateEnd' AND
                        E.VendorID > 0 AND
                        E.Userid = '$userid' AND
                        E.Deleted = 0
                    GROUP BY E.VendorID
                    ORDER BY Total DESC";
        } elseif ($byWhat == 'category') {
            $sql = "SELECT SUM(E.Amount) AS Total,C.MainCat AS Label
                    FROM cpo_expense E
                    LEFT JOIN cpo_main_cat C ON C.CatID = E.CatID
                    WHERE
                        E.DateExpense >= '$reportDateStart' AND
                        E.DateExpense <= '$reportDateEnd' AND
                        E.CatID > 0 AND
                        E.Userid = '$userid' AND
                        E.Deleted = 0
                    GROUP BY E.CatID
                    ORDER BY Total DESC";
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
?>