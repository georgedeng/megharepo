<?
class Bug_model extends Model {

    function Bug_model() {
        // Call the Model constructor
        parent::Model();
    }

 	/**
	 * saveBugs : inserts new client entries or updates existing entries.
	 *
	 * @access	public
	 * @param	array $data PHP array of data to save
	 * @return	int   $clientID the record id of the inserted or updated entry
	 */
    function saveBugReport($bugArray) {
        $sql = "INSERT into cpo_sysBugs (
                Userid,
                PID,
                BugText,
                UserAgent,
                UserAgentVersion,
                Platform,
                BugDateEntry,
                BugPage
                ) values (
                '".$bugArray['userid']."',
                '".$bugArray['pid']."',
                '".$bugArray['content']."',
                '".$bugArray['browser']."',
                '".$bugArray['version']."',
                '".$bugArray['platform']."',
                '".date('Y-m-d')."',
                '".$bugArray['page']."'
                )";
        $query = $this->db->query($sql);
        $bugID = $this->db->insert_id();
        return $bugID;
    }

    function closeBugReport($data,$userid) {
        $sql = "UPDATE cpo_sysBugs SET
                BugTextReworded = '".$data['textReworded']."',
                ItemType        = '".$data['bugFeature']."',
                BugDateClose    = '".date('Y-m-d')."',
                CloseUserid     = '$userid',
                Status          = 1
                WHERE BID = ".$data['bugID'];
        $query = $this->db->query($sql);
    }

    function getBug($bugID) {
        $sql = "SELECT * from cpo_sysBugs WHERE BID = '$bugID'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getBugs($open=NULL,$closed=NULL) {
        if ($open == TRUE) {
            $where = " Status = 0 ";
        } elseif ($closed == TRUE) {
            $where = " Status = 1 ";
        } elseif ($open == TRUE && $closed == TRUE) {
            $where = " Status = 0 OR Status = 1 ";
        }

        $sql = "SELECT *
                from cpo_sysBugs B
                LEFT JOIN cpo_login L ON L.UID = B.Userid
                LEFT JOIN cpo_people PE ON PE.PID = B.PID
                LEFT JOIN cpo_account_profile P ON P.AccountID = L.UseridAccount
                WHERE $where ORDER BY BID DESC";
        $query = $this->db->query($sql);
		return $query->result_array();
    }

    function getItemsForUpdates($limit=NULL) {
        $sql = "SELECT * from cpo_sysBugs
                WHERE
                Status = 1
                ORDER BY BugDateClose DESC";
        if ($limit>0) {
            $sql .= " LIMIT $limit ";
        }
        $query = $this->db->query($sql);
		return $query->result_array();
    }

    function toggleBugStatus($bugID) {
        $sql = "UPDATE cpo_sysBugs
                SET Status = IF(Status = 1, 0, 1)
                WHERE
                BID = '$bugID'";
        $query = $this->db->query($sql);
    }

    function deleteBug($bugID) {
        $sql = "DELETE from cpo_sysBugs WHERE BID = '$bugID'";
        $query = $this->db->query($sql);
    }
}
?>