<?php
class Admin_alerts extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getAlerts($offDate=NULL) {
        if (!empty($offDate)) {
            $sql = "SELECT * from cpo_sysAlerts
                    WHERE DateEntered > '$offDate'";
        } else {
            $sql = "SELECT * from cpo_sysAlerts
                    WHERE DateExpire >= '".date('Y-m-d')."'";
        }
		$query = $this->db->query($sql);

        if ($query->num_rows()>0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
}
?>