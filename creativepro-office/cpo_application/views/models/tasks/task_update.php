<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Task_update extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();

        $this->load->model('tasks/Task_view','',true);
    }

     /**
	 * saveTask : inserts new task entries or updates existing entries
	 *
	 * @access	 public
	 * @param	array $data PHP array of data to save
	 * @return	 array $taskID,$milestoneID the record id of the inserted or updated entry and it's
     *              associated milestoneID
	 */
    function saveTask($data) {
        if ($data['action'] == 'add') {
            $prevTaskID = '';
            $milestone = 0;
            if (isset($data['taskPrevID'])) {
                $prevTaskID = $data['taskPrevID'];
            }
            if (isset($data['milestone'])) {
                $milestone = $data['milestone'];
            }
    		$sql = "INSERT into cpo_project_tasks (
                         PrevTaskID,
                         Userid,
                         ParentTaskID,
                         ProjectID,
                         Title,
                         Description,
                         Date_Start,
                         Date_End,
                         HoursEstimated,
                         Status,
                         Priority,
                         TaskNo,
                         Milestone,
                         Tags,
                         CalendarID,
                         UseridEntry,
                         UseridUpdate,
                         DateEntry,
						 DateUpdate,
                         ImportGUID
					) values (
                        '$prevTaskID',
                        '".$data['accountUserid']."',
                        '".$data['milestoneID']."',
                        '".$data['projectID']."',
                        '".$data['taskTitle']."',
                        '".$data['description']."',
                        '".$data['taskDateStart']."',
                        '".$data['taskDateEnd']."',
                        '".$data['estimatedTime']."',
                        '".$data['status']."',
                        '".$data['priority']."',
                        '".$data['taskNumber']."',
                        '$milestone',
                        '".$data['taskTags']."',
                        '".$data['calendarID']."',
                        '".$data['userid']."',
			'".$data['userid']."',
                        '".date('Y-m-d G:i:s')."',
                        '".date('Y-m-d G:i:s')."',
                        '".$this->session->userdata('importGUID')."'
					)";
    		$query = $this->db->query($sql);
    		$taskID = $this->db->insert_id();

		} elseif ($data['action'] == 'edit') {
               $sql = "UPDATE cpo_project_tasks SET
						 Title          = '".$data['taskTitle']."',
                         Description    = '".$data['description']."',
                         Date_Start     = '".$data['taskDateStart']."',
                         Date_End       = '".$data['taskDateEnd']."',
                         HoursEstimated = '".$data['estimatedTime']."',
                         Priority       = '".$data['priority']."',
                         Tags           = '".$data['taskTags']."',
                         UseridUpdate   = '".$data['userid']."',
						 DateUpdate     = '".date('Y-m-d G:i:s')."'
					WHERE
						TaskID = '".$data['taskID']."'";
               $query = $this->db->query($sql);
               $taskID = $data['taskID'];
		}
        $returnArray = array($taskID,$data['milestoneID']);
		return $returnArray;
    }

    function clearUsersFromTask($taskID) {
        $sql = "DELETE FROM cpo_linkUIDTask
                WHERE
                TaskID = '$taskID'";
        $query = $this->db->query($sql);
    }

    function assignUserToTask($userid,$taskID) {
        $sql = "INSERT into cpo_linkUIDTask (
                MemberID,
                TaskID
                ) values (
                '$userid',
                '$taskID'
                )";
        $query = $this->db->query($sql);
    }    

	/**
	 * saveMilestone : inserts new milestone entries or updates existing entries
	 *
	 * @access	 public
	 * @param	 array $data PHP array of data to save
	 * @return	 int   $milestoneID the record id of the inserted or updated entry
	 */
    function saveMilestone($data) {
        if ($data['action'] == 'add') {
    		$sql = "INSERT into cpo_project_tasks (
                         Userid,
                         ProjectID,
                         Title,
                         Date_Start,
                         Date_End,
						 Milestone,
                         UseridEntry,
                         UseridUpdate,
                         DateEntry,
						 DateUpdate
					) values (
                        '".$this->session->userdata('accountUserid')."',
                        '".$data['projectID']."',
						'".$data['milestoneTitle']."',
						'".$data['milestoneDateStart']."',
						'".$data['milestoneDateEnd']."',
						'1',
                        '".$this->session->userdata('userid')."',
						'".$this->session->userdata('userid')."',
                        '".date('Y-m-d G:i:s')."',
                        '".date('Y-m-d G:i:s')."'
					)";
    		$query = $this->db->query($sql);
    		$milestoneID = $this->db->insert_id();
		} elseif ($data['action'] == 'edit') {
               $sql = "UPDATE cpo_project_tasks SET
						 Title          = '".$data['milestoneTitle']."',
                         Date_Start     = '".$data['milestoneDateStart']."',
                         Date_End       = '".$data['milestoneDateEnd']."',
                         UseridUpdate   = '".$this->session->userdata('userid')."',
						 DateUpdate     = '".date('Y-m-d G:i:s')."'
					WHERE
						TaskID = '".$data['milestoneID']."'";
               $query = $this->db->query($sql);
               $milestoneID = $data['milestoneID'];
		}
        
		return $milestoneID;
    }

    function updateTaskActualHours($taskID,$totalHours) {
        $sql = "UPDATE cpo_project_tasks SET
                HoursActual = '$totalHours'
                WHERE TaskID = '$taskID'";
        $query = $this->db->query($sql);

        if ($this->Task_view->getTaskStatus($taskID) == 0) {
            Task_update::updateTaskStatus($taskID,1);
        }
    }

    function updateTaskStatus($taskID,$newStatus) {
        /*
         * If we're marking this task complete, enter the completed data too.
         */
        if ($newStatus == 2) {
            $dateCompleted = date('Y-m-d');
        } else {
            $dateCompleted = '0000-00-00';
        }
        $sql = "UPDATE cpo_project_tasks SET 
                Status = '$newStatus',
                Date_Completed = '$dateCompleted'
                WHERE TaskID = '$taskID'";

        $query = $this->db->query($sql);
    }

    function updateTaskDatesAfterMove($taskID,$newDateStart,$newDateEnd) {
        $sql = "SELECT Date_Start,Date_End FROM cpo_project_tasks WHERE TaskID = '$taskID'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $oldDateStart = $row['Date_Start'];
        $oldDateEnd   = $row['Date_End'];

        $oldDateStartArray = explode(' ',$oldDateStart);
        $oldDateEndArray   = explode(' ',$oldDateEnd);

        $newDateStart = $newDateStart;
        $newDateEnd   = $newDateEnd;

		$newDateStart = convertMySQLToGMT($newDateStart,'push','Y-m-d H:i:s');
		$newDateEnd   = convertMySQLToGMT($newDateEnd,'push','Y-m-d H:i:s');

        $sql = "UPDATE cpo_project_tasks SET
                Date_Start = '$newDateStart',
                Date_End   = '$newDateEnd'
                WHERE
                TaskID = '$taskID'";
        $query = $this->db->query($sql);
    }

    function getNewTaskNumber($projectID) {
        $sql = "SELECT MAX(TaskNo) AS LastTaskNumber FROM cpo_project_tasks
                WHERE ProjectID = '$projectID'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $newTaskNumber = $row['LastTaskNumber']+1;
        return $newTaskNumber;
    }

    function moveTasksToMilestone($milestoneFrom,$milestoneTo) {
        $sql = "UPDATE cpo_project_tasks SET ParentTaskID = '$milestoneTo' WHERE ParentTaskID = '$milestoneFrom'";
        $query = $this->db->query($sql);
    }

    /**
	* deleteTask : delete a task
	*
	* @access	public
	* @param	int    $taskID ID of task we wish to delete.
	* @param    string $itemType milestone or task
    * @param    bool   $tagForDelete If TRUE, just tag this contact for deletion.
    *                  If FALSE, completely delete the contact.
	* @return	void
	*/
    function deleteTask($taskID,$itemType=NULL,$tagForDelete=TRUE,$userid=NULL,$accountUserid=NULL) {
		if ($tagForDelete == TRUE) {
			if ($itemType == 'task') {
				$sql = "UPDATE cpo_project_tasks SET UseridUpdate = '$userid', Deleted = 1 
                        WHERE TaskID = '$taskID' AND Userid = '$accountUserid'";
				$query = $this->db->query($sql);
			} else {
				$sql = "UPDATE cpo_project_tasks SET UseridUpdate = '$userid', Deleted = 1 
                        WHERE (TaskID = '$taskID' OR ParentTaskID = '$taskID') AND Userid = '$accountUserid'";
				$query = $this->db->query($sql);
			}
		} else {
			if ($itemType == 'task') {
				$sql = "DELETE from cpo_project_tasks WHERE TaskID = '$taskID'";
				$query = $this->db->query($sql);

				$sql = "DELETE from cpo_linkUIDTask WHERE TaskID = '$taskID'";
				$query = $this->db->query($sql);
			} else {
				$sql = "DELETE from cpo_project_tasks WHERE TaskID = '$taskID' OR ParentTaskID = '$taskID'";
				$query = $this->db->query($sql);
			}
		}
    }

    function restoreTask($taskID,$userid,$accountUserid=NULL) {
        $sql = "UPDATE cpo_project_tasks SET UseridUpdate = '$userid', Deleted = 0 WHERE TaskID = '$taskID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function deleteMilestone($milestoneID,$withTasks,$tagForDelete=TRUE,$userid=NULL,$accountUserid=NULL) {
        if ($tagForDelete == TRUE) {
            $sql = "UPDATE cpo_project_tasks SET
                        UseridUpdate = '$userid',
                        Deleted = 1
                    WHERE
                        TaskID = '$milestoneID' AND
                        Userid = '$accountUserid'";
            $query = $this->db->query($sql);

            if ($withTasks == 1) {
                $sql = "UPDATE cpo_project_tasks SET
                            UseridUpdate = '$userid',
                            Deleted = 1
                        WHERE
                            ParentTaskID = '$milestoneID' AND
                            Userid = '$accountUserid'";
                $query = $this->db->query($sql);
            }
        } else {
            $sql = "DELETE FROM cpo_project_tasks WHERE TaskID = '$milestoneID' AND Userid = '$accountUserid'";
            $query = $this->db->query($sql);

            if ($withTasks == 1) {
                $sql = "DELETE from cpo_project_tasks WHERE ParentTaskID = '$milestoneID' AND Userid = '$accountUserid'";
                $query = $this->db->query($sql);
            }
        }
    }

    function restoreMilestone($milestoneID,$userid,$accountUserid=NULL) {
        $sql = "UPDATE cpo_project_tasks SET UseridUpdate = '$userid', Deleted = 0 WHERE TaskID = '$milestoneID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);

        $sql = "UPDATE cpo_project_tasks SET UseridUpdate = '$userid', Deleted = 0 WHERE ParentTaskID = '$milestoneID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function setDateOverdueNotify($taskID,$date=NULL) {
        if ($date == NULL) {
            $date = date('Y-m-d');
        }

        $sql = "UPDATE cpo_project_tasks SET DateOverdueNotify = '$date' WHERE TaskID = '$taskID'";
        $query = $this->db->query($sql);
    }

    function updateTaskOrder($taskID,$order,$milestoneID=0) {
        $sql = "UPDATE cpo_project_tasks SET ParentTaskID = $milestoneID, DisplayOrder = $order WHERE TaskID = $taskID";
        $query = $this->db->query($sql);
    }

    function makeTemplateRecords($templateProjectID,$newProjectID,$userid,$milestones,$tasks) {
        if ($milestones == true && $tasks == true) {
            $sql = "SELECT * from cpo_project_tasks WHERE ProjectID = $templateProjectID AND Milestone = 1 AND Deleted = 0";
        } elseif ($milestones == true) {
            $sql = "SELECT * from cpo_project_tasks WHERE ProjectID = $templateProjectID AND Milestone = 1 AND Deleted = 0";
        } elseif ($tasks == true) {
             $sql = "SELECT * from cpo_project_tasks WHERE ProjectID = $templateProjectID AND Milestone = 0 AND Deleted = 0";
        }
        $query = $this->db->query($sql);

        if ($query->num_rows()>0) {
            if ($milestones == true && $tasks == true) {
                foreach ($query->result_array() as $row) {
                    /*
                     * Enter milestones first.
                     */
                    $origMilestoneID = $row['TaskID'];
                    $sql = "INSERT into cpo_project_tasks (
                            Userid,
                            ProjectID,
                            Title,
                            Description,
                            HoursEstimated,
                            Priority,
                            TaskNo,
                            Milestone,
                            DisplayOrder,
                            Tags,
                            CalendarID,
                            IsTemplate,
                            UseridEntry,
                            DateEntry
                            ) values (
                            '".$row['Userid']."',
                            '".$newProjectID."',
                            '".$row['Title']."',
                            '".$row['Description']."',
                            '".$row['HoursEstimated']."',
                            '".$row['Priority']."',
                            '".$row['TaskNo']."',
                            '".$row['Milestone']."',
                            '".$row['DisplayOrder']."',
                            '".$row['Tags']."',
                            '".$row['CalendarID']."',
                            '1',
                            '".$userid."',
                            '".date('Y-m-d')."'
                            )";
                    $query = $this->db->query($sql);
                    $milestoneID = $this->db->insert_id();

                    /*
                     * Then retrieve and enter tasks for the milestone.
                     */
                    $sql2 = "SELECT * from cpo_project_tasks WHERE ParentTaskID = $origMilestoneID AND Deleted = 0";
                    $query2 = $this->db->query($sql2);
                    foreach ($query2->result_array() as $row2) {
                        $sql = "INSERT into cpo_project_tasks (
                                Userid,
                                ParentTaskID,
                                ProjectID,
                                Title,
                                Description,
                                HoursEstimated,
                                Priority,
                                TaskNo,
                                Milestone,
                                DisplayOrder,
                                Tags,
                                CalendarID,
                                IsTemplate,
                                UseridEntry,
                                DateEntry
                                ) values (
                                '".$row2['Userid']."',
                                '".$milestoneID."',
                                '".$newProjectID."',
                                '".$row2['Title']."',
                                '".$row2['Description']."',
                                '".$row2['HoursEstimated']."',
                                '".$row2['Priority']."',
                                '".$row2['TaskNo']."',
                                '".$row2['Milestone']."',
                                '".$row2['DisplayOrder']."',
                                '".$row2['Tags']."',
                                '".$row2['CalendarID']."',
                                '1',
                                '".$userid."',

                                '".date('Y-m-d')."'
                                )";
                        $query = $this->db->query($sql);
                        $newTaskID = $this->db->insert_id();

                        /*
                         * Enter assigned users for this task.
                         */
                        $sqlT = "SELECT * from cpo_linkUIDTask WHERE TaskID = ".$row2['TaskID'];
                        $queryT = $this->db->query($sqlT);

                        foreach ($queryT->result_array() as $rowT) {
                            $sqlT2 = "INSERT into cpo_linkUIDTask (
                                        MemberID,
                                        TaskID
                                        ) values (
                                        '".$rowT['MemberID']."',
                                        '".$newTaskID."'
                                        )";
                            $queryT2 = $this->db->query($sqlT2);
                        }
                    }
                }

                /*
                 * Now get misc tasks.
                 */
                $sql = "SELECT * from cpo_project_tasks 
                        WHERE ProjectID = $templateProjectID AND ParentTaskID = 0 AND Milestone = 0 AND Deleted = 0";
                $query = $this->db->query($sql);
                foreach ($query->result_array() as $row) {
                    $sql = "INSERT into cpo_project_tasks (
                            Userid,
                            ParentTaskID,
                            ProjectID,
                            Title,
                            Description,
                            HoursEstimated,
                            Priority,
                            TaskNo,
                            Milestone,
                            DisplayOrder,
                            Tags,
                            CalendarID,
                            IsTemplate,
                            UseridEntry,
                            DateEntry
                            ) values (
                            '".$row['Userid']."',
                            '".$row['ParentTaskID']."',
                            '".$newProjectID."',
                            '".$row['Title']."',
                            '".$row['Description']."',
                            '".$row['HoursEstimated']."',
                            '".$row['Priority']."',
                            '".$row['TaskNo']."',
                            '".$row['Milestone']."',
                            '".$row['DisplayOrder']."',
                            '".$row['Tags']."',
                            '".$row['CalendarID']."',
                            '1',
                            '".$userid."',

                            '".date('Y-m-d')."'
                            )";
                    $query = $this->db->query($sql);
                    $newTaskID = $this->db->insert_id();

                    /*
                     * Enter assigned users for this task.
                     */
                    $sqlT = "SELECT * from cpo_linkUIDTask WHERE TaskID = ".$row['TaskID'];
                    $queryT = $this->db->query($sqlT);

                    foreach ($queryT->result_array() as $rowT) {
                        $sqlT2 = "INSERT into cpo_linkUIDTask (
                                    MemberID,
                                    TaskID
                                    ) values (
                                    '".$rowT['MemberID']."',
                                    '".$newTaskID."'
                                    )";
                        $queryT2 = $this->db->query($sqlT2);
                    }
                }
            } else {
                foreach ($query->result_array() as $row) {
                    $sql = "INSERT into cpo_project_tasks (
                            Userid,
                            ProjectID,
                            Title,
                            Description,
                            HoursEstimated,
                            Priority,
                            TaskNo,
                            Milestone,
                            DisplayOrder,
                            Tags,
                            CalendarID,
                            IsTemplate,
                            UseridEntry,
                            DateEntry
                            ) values (
                            '".$row['Userid']."',
                            '".$newProjectID."',
                            '".$row['Title']."',
                            '".$row['Description']."',
                            '".$row['HoursEstimated']."',
                            '".$row['Priority']."',
                            '".$row['TaskNo']."',
                            '".$row['Milestone']."',
                            '".$row['DisplayOrder']."',
                            '".$row['Tags']."',
                            '".$row['CalendarID']."',
                            '1',
                            '".$userid."',
                            '".date('Y-m-d')."'
                            )";
                    $query = $this->db->query($sql);
                    $newTaskID = $this->db->insert_id();
                    if ($tasks == true) {
                        /*
                         * Enter assigned users for this task.
                         */
                        $sqlT = "SELECT * from cpo_linkUIDTask WHERE TaskID = ".$row['TaskID'];
                        $queryT = $this->db->query($sqlT);

                        foreach ($queryT->result_array() as $rowT) {
                            $sqlT2 = "INSERT into cpo_linkUIDTask (
                                        MemberID,
                                        TaskID
                                        ) values (
                                        '".$rowT['MemberID']."',
                                        '".$newTaskID."'
                                        )";
                            $queryT2 = $this->db->query($sqlT2);
                        }
                    }
                }
            }
        }
    }
}
?>