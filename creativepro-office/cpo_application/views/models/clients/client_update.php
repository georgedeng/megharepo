<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Client_update extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }
    
 	/**
	 * saveClient : inserts new client entries or updates existing entries.
	 *
	 * @access	public
	 * @param	array $data PHP array of data to save
	 * @return	int   $clientID the record id of the inserted or updated entry
	 */
    function saveClient($data) {
        if ($data['action'] == 'add') {
            $prevClientID = '';
            if (isset($data['clientPrevID'])) {
                $prevClientID = $data['clientPrevID'];
            }
    		$sql = "INSERT into cpo_clients (
                        PrevClientID,
						Userid,
						CatID,
						Company,
						Address,
						City,
						State,
						Zip,
						Country,
                        Phone1,
                        Phone2,
                        Phone3,
                        Email,
                        URL,
                        Comments,
                        Archive,
                        Status,
                        Industry,
                        Client_Since,
                        Timezone,
						Language,
                        Tags,
						UseridUpdate,
                        DateEntry,
						DateUpdate,
                        ImportGUID
					) values (
                        '$prevClientID',
                        '".$data['userid']."',
						'".$data['clientCategory']."',
						'".$data['clientCompany']."',
						'".$data['clientAddress']."',
						'".$data['clientCity']."',
						'".$data['clientState']."',
						'".$data['clientZip']."',
						'".$data['clientCountry']."',
						'".$data['clientPhone1']."',
						'".$data['clientPhone2']."',
						'".$data['clientPhone3']."',
						'".$data['clientEmail']."',
						'".$data['clientURL']."',
						'".$data['clientNotes']."',
                        '".$data['clientArchive']."',
                        '".$data['clientStatus']."',
						'".$data['clientIndustry']."',
						'".$data['clientSince']."',
						'".$data['clientTimezone']."',
						'".$data['clientLanguage']."',
						'".$data['clientTags']."',
						'".$this->session->userdata('userid')."',
                        '".date('Y-m-d')."',
                        '".date('Y-m-d')."',
                        '".$this->session->userdata('importGUID')."'
					)";
    		$query = $this->db->query($sql);
    		$clientID = $this->db->insert_id();

		} elseif ($data['action'] == 'edit') {
    		$sql = "UPDATE cpo_clients SET
						CatID         = '".$data['clientCategory']."',
						Company       = '".$data['clientCompany']."',
						Address       = '".$data['clientAddress']."',
						City          = '".$data['clientCity']."',
						State         = '".$data['clientState']."',
						Zip           = '".$data['clientZip']."',
						Country       = '".$data['clientCountry']."',
                        Phone1        = '".$data['clientPhone1']."',
                        Phone2        = '".$data['clientPhone2']."',
                        Phone3        = '".$data['clientPhone3']."',
                        Email         = '".$data['clientEmail']."',
                        URL           = '".$data['clientURL']."',
                        Comments      = '".$data['clientNotes']."',
                        Industry      = '".$data['clientIndustry']."',
                        Client_Since  = '".$data['clientSince']."',
                        Timezone      = '".$data['clientTimezone']."',
						Language      = '".$data['clientLanguage']."',
                        Tags          = '".$data['clientTags']."',
                        UseridUpdate  = '".$this->session->userdata('userid')."',
                        DateUpdate    = '".date('Y-m-d')."'
					WHERE
						ClientID = '".$data['clientID']."'";
    		$query = $this->db->query($sql);
    		$clientID = $data['clientID'];
		} elseif ($data['action'] == 'update') {
            /*
             * Update the ClientUserid
             */
            $sql = "UPDATE cpo_clients SET
                    ClientUserid = '".$data['clientUserid']."'
                    WHERE
                    ClientID = '".$data['clientID']."'";
            $query = $this->db->query($sql);
            $clientID = $data['clientID'];
        }
		return $clientID;
    }

    function deleteClient($clientID,$tagForDelete=FALSE,$userid) {
        if ($tagForDelete == TRUE) {
             $sql = "UPDATE cpo_clients SET UseridUpdate = '$userid', Deleted = 1 WHERE ClientID = '$clientID'";
             $query = $this->db->query($sql);
         } else {
             $sql = "DELETE from cpo_clients WHERE ClientID = '$clientID'";
             $query = $this->db->query($sql);

             /*
              * TODO: What else do we want to delete here?
              */
         }
    }

    function rollBackImport() {
        $sql = "DELETE from cpo_clients WHERE ImportGUID = '".$this->session->userdata('importGUID')."'";
        $query = $this->db->query($sql);

        /*
         * Remove login information too.
         */
        $sql = "DELETE from cpo_login WHERE ImportGUID = '".$this->session->userdata('importGUID')."'";
        $query = $this->db->query($sql);
    }

    function restoreClient($clientID,$userid) {
        $sql = "UPDATE cpo_clients SET UseridUpdate = '$userid', Deleted = 0 WHERE ClientID = '$clientID'";
        $query = $this->db->query($sql);
    }
}   
?>