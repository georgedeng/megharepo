<?php
class Project_template extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
        $this->load->model('projects/Project_view','ProjectView',true);
    }

    function saveProjectTemplate($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT into cpo_project_template (
                    Userid,
                    ProjectID,
                    CategoryID,
                    Title,
                    Description,
                    IncludeMilestones,
                    IncludeTasks,
                    IncludeContacts,
                    IncludeTeam,
                    IncludeExpenses,
                    IncludeNotes,
                    IncludeFiles,
                    DateUpdate,
                    UseridUpdate
                    ) values (
                    '".$data['accountUserid']."',
                    '".$data['projectID']."',
                    '',
                    '".$data['Name']."',
                    '".$data['Desc']."',
                    '".$data['Milestones']."',
                    '".$data['Tasks']."',
                    '".$data['Contacts']."',
                    '".$data['TeamMembers']."',
                    '".$data['Files']."',
                    '".$data['Expenses']."',
                    '".$data['Notes']."',
                    '".date('Y-m-d')."',
                    '".$data['userid']."'
                    )";
            $query = $this->db->query($sql);
            $templateID = $this->db->insert_id();
        } elseif ($data['action'] == 'edit') {
            $sql = "UPDATE cpo_project_template SET
                    ProjectID         = '".$data['projectID']."',
                    CategoryID        = '',
                    Title             = '".$data['Name']."',
                    Description       = '".$data['Desc']."',
                    IncludeMilestones = '".$data['Milestones']."',
                    IncludeTasks      = '".$data['Tasks']."',
                    IncludeContacts   = '".$data['Contacts']."',
                    IncludeTeam       = '".$data['TeamMembers']."',
                    IncludeExpenses   = '".$data['Files']."',
                    IncludeNotes      = '".$data['Expenses']."',
                    IncludeFiles      = '".$data['Notes']."',
                    DateUpdate        = '".date('Y-m-d')."',
                    UseridUpdate      = '".$data['userid']."'
                    WHERE
                    TemplateID = '".$data['templateID']."'";
            $query = $this->db->query($sql);
            $templateID = $data['templateID'];
        }
        return $templateID;
    }

    function getProjectTemplate($templateID) {
        $sql = "SELECT * FROM cpo_project_template
                WHERE
                TemplateID = '".$templateID."'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getProjectTemplates($accountUserid) {
        $sql = "SELECT 
                    T.TemplateID,T.ProjectID,T.CategoryID,T.Title,P.Title AS ProjectTitle,T.Description,
                    T.IncludeMilestones,T.IncludeTasks,T.IncludeContacts,T.IncludeTeam,T.IncludeExpenses,
                    T.IncludeNotes,T.IncludeFiles,T.DateUpdate,
                    PP.PID,PP.NameFirst,PP.NameLast
                FROM cpo_project_template T
                LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                LEFT JOIN cpo_people PP ON PP.LoginUserid = T.UseridUpdate
                WHERE
                T.Userid = $accountUserid AND
                T.Deleted = 0
                ORDER BY T.Title";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function deleteTemplate($templateID) {
        $sql = "DELETE from cpo_project_template WHERE TemplateID = $templateID";
        $query = $this->db->query($sql);
    }

    function removeTemplateItemsForProject($projectID) {
        $sql = "DELETE from cpo_project_tasks WHERE IsTemplate = 1 AND ProjectID = $projectID";
        $query = $this->db->query($sql);

        $sql = "DELETE from cpo_expense WHERE IsTemplate = 1 AND ProjectID = $projectID";
        $query = $this->db->query($sql);

        $sql = "DELETE from cpo_notes WHERE IsTemplate = 1 AND ItemID = $projectID AND ItemType = 'P'";
        $query = $this->db->query($sql);

        $sql = "DELETE from cpo_linkFileItemType WHERE IsTemplate = 1 AND ItemID = $projectID AND ItemType = 'project'";
        $query = $this->db->query($sql);

        $sql = "DELETE from cpo_linkPeopleItemType WHERE IsTemplate = 1 AND ItemID = $projectID AND ItemType = 'P'";
        $query = $this->db->query($sql);
    }
}
