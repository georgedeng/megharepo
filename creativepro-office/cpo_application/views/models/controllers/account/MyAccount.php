<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MyAccount extends Controller {
	function __construct()
	{
		parent::Controller();
        checkPermissionsForController($this->router->fetch_class());
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
		
		$this->load->model('accounts/accounts','',true);

		$this->load->library('application/CommonAppData');
        $this->load->helper('account');
        $this->load->helper('payment');
	}
	
	function _init() {
		$data['page']                  = 'account';
		$data['pageTitle']             = lang('common_your_account');
		$data['pageSubTitle']          = lang('common_your_account');
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('');
		$data['pageIconClass']         = 'iconPageClients';
		$data['pageLayoutClass']       = 'withRightColumn';
		$data['rightColumnComponents'] = array();

        return $data;
	}
	
	function index() {
        ini_set('display_errors',1);
        ini_set('error_reporting',E_ALL);
		$data = MyAccount::_init();
		$this->load->view('account/MyAccount',$data);
	}
}	
?>