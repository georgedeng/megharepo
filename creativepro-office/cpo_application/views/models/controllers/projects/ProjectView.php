<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProjectView extends Controller {
	function __construct()
	{
		parent::Controller();
        checkPermissionsForController($this->router->fetch_class());
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('projects/Project_view','',true);
		$this->load->model('finances/Finance_view','',true);
		$this->load->model('tasks/Task_view','',true);
        $this->load->model('messaging/Site_messages','',true);

		$this->load->library('application/CommonProject');
		$this->load->library('application/CommonAppData');
	}
	
	function _init($tag=NULL) {
		global $subMenuArray;
		$data['page']                  = 'projects';
		$data['pageTitle']             = lang('project_page_title');
		$data['pageSubTitle']          = lang('project_view_page_title');
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsProjects.js','jsDashboardOwners.js');
		$data['pageIconClass']         = 'iconPageProjects';
		$data['pageLayoutClass']       = 'withRightColumn';
		$data['rightColumnComponents'] = array('tagRender');
		$data['tagRenderArray']        = $this->commonappdata->renderTags('project','array');
		$data['tag']                   = $tag;        
        $data['pageButtons'] = array();
        array_push($data['pageButtons'], '<button class="buttonExpand projectCreate action hide" id="buttonadd" title="'.lang('menu_new_project').'"><span class="add">'.lang('menu_new_project').'</span></button>');
        
        $permissionArray = $this->session->userdata('permissions');
		$viewAll = TRUE;

		if ($permissionArray['messageViewOwn'] == 1) {
            $viewAll = FALSE;
		}
        $numberMessagesArray = $this->Site_messages->getNumberOfMessagesForSomething(NULL,'project',$this->session->userdata('userid'),$this->session->userdata('accountUserid'),$this->session->userdata('pid'),$viewAll);
		$data['numberMessages'] = $numberMessagesArray['NoOfMessagesTotal'];
        $data['archive'] = 0;
        $data['maxProjects'] = 0;

        noCache();
        return $data;
	}
	
	function index($tag=NULL) {
  		$data = ProjectView::_init($tag);
		$this->load->view('projects/ProjectView',$data);
	}

	function tag($tag=NULL) {
  		$data = ProjectView::_init($tag);
		$this->load->view('projects/ProjectView',$data);
	}

    function deleted() {
        $data = ProjectView::_init();
        $data['deleteMessage'] = lang('help_project_deleted');
		$this->load->view('projects/ProjectView',$data);
    }

    function maxProjects() {
        $data = ProjectView::_init();
        $data['maxProjects'] = 1;
		$this->load->view('projects/ProjectView',$data);
    }
		
	function getProjectList($itemID=NULL,$group=NULL,$tag=NULL,$renderType=NULL,$return=1,$widget=0,$status=NULL,$archive=NULL) {
        if ($group == 'user') {
			$itemID = $this->session->userdata('accountUserid');
            /*
             * Can we view all or just our assigned projects?
             */
            $permArray = $this->session->userdata('permissions');
            $viewAll = TRUE;
            if ($permArray['projectViewOwn'] == 1) {
                $viewAll = FALSE;
                $itemID = $this->session->userdata('pid');
            }
		}
        if ($status == NULL || $status == '0') {
            /*
             * PROJECT STATUS: 0=Not Started,1=In Progress,2=Completed,3=On Hold,4=Overdue
             */
            $status = '0|1|2|3|4';
        }
		
		$acdc   = 'ASC';
		if ($widget == 1 && $status != 'all') {
			$acdc   = 'ASC';
            $status = '0|1|3|4';
		}
        if ($group == 'client') {
            $acdc = 'DESC';
            $viewAll = TRUE;
        }       

		$projectArray = $this->Project_view->getProjectList($itemID,$group,$status,$acdc,$tag,$archive,$viewAll);
        if ($renderType == 'json') {
            if (count($projectArray)<1) {
                $noDataMessage = lang('help_project_no_projects');
                if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
                    $noDataMessage = lang('help_project_no_projects_client');
                }
                $jsonString = '{"Projects":[],"NoDataMessage":"'.$noDataMessage.'"}';
            } else {
                $headerString = lang('common_title').'|'.lang('project_completion_date').'|'.lang('common_status').'|'.lang('common_hours');
                $footerString = lang('common_totals');

                $jsonString = '{"HeaderLabels":"'.$headerString.'","FooterLabels":"'.$footerString.'","Projects":[';
                $totalHours = 0;
                foreach($projectArray as $project) {
                    $newProjectArray = $this->commonproject->renderProject($project,'array',TRUE,$group);
                    $jsonString .= json_encode($newProjectArray).',';
                    $totalHours = $totalHours+$newProjectArray['ProjectTime'];
                }

                $jsonString = substr($jsonString,0,-1).'],"TotalHours":"'.$totalHours.'"}';
            }
			if ($return == 1) {
				return utf8_encode(stripslashes($jsonString));
			} else {
				echo decode_utf8($jsonString);
			}		
		}	
	}	
	
	function searchProjects() {
        $q = cleanStringTag(strtolower($this->input->get('term', TRUE)));
		if (!$q) return;
		$projects = $this->Project_view->getProjectsForAutoCompleter($q);
		echo $projects;
	}

	function getProjectsForSelect($clientID=NULL) {
        /*
         * Can we view all or just our assigned projects?
         */
        $permArray = $this->session->userdata('permissions');
        $viewAll = TRUE;
        if ($permArray['projectViewOwn'] == 1) {
            $viewAll = FALSE;
        }
        $projectArray = $this->Project_view->getProjectsForSelect($clientID,$viewAll,$this->session->userdata('pid'));
            $jsonString = json_encode($projectArray);
            echo decode_utf8($jsonString);
	}
}		
?>