<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set("memory_limit","32M");
ini_set("max_input_time","28800"); // <-- 8 hours should be enough

class FileManager_S3 extends Controller {

	function FileManager_S3()
	{
		parent::Controller();
        $this->load->model('files/File_maintenance');
        include(APPPATH.'libraries/s3/S3.php');
		$this->load->helper('file');
		$this->load->helper('download');
	}

	/**
      * uploadFileHandler : upload files to Amazon S3 service
	  *
      * @access	public
	  * @param  array  $fileArray Array of files to be deleted
      * @return void
      */
	function uploadFileHandler() {
          // Check the upload
          $fileNameTmp  = $_FILES['Filedata']['tmp_name'];
          $fileName     = $_FILES['Filedata']['name'];
          $someFileType = $_FILES['Filedata']['type'];
          $someFileSize = $_FILES['Filedata']['size'];
          $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);

		  /*
		   * Create hash filename for S3
		   */
		   $extension = substr(strrchr($fileName, '.'), 1);
		   $hashFileName = random_string('alnum',30).'_'.time().'.'.$extension;
          
		  /*
		   * Try creating a 'folder' and then putting the file into the folder
		   */
		  $folderName = $this->session->userdata('userDir');
		  $s3->putObject($folderName.'_$folder$', AWS_BUCKET_NAME, $folderName.'_$folder$', S3::ACL_PRIVATE);
		  
		  //move the file
		  if ($s3->putObjectFile($fileNameTmp, AWS_BUCKET_NAME, $folderName.'/'.$hashFileName, S3::ACL_PRIVATE)) {
			$status = 'Success';
			/*
			 * Enter file information into database
			 */
			$fileArray['FileNameActual'] = $fileName;
			$fileArray['FileNameHash']   = $hashFileName;
			$fileArray['FileSize']       = $someFileSize;
			$fileArray['FileType']       = $someFileType;
			$fileArray['action']         = 'add';
			$fileArray['ParentID']       = $_POST['selectedFolderID'];

			$this->File_maintenance->insertFileData($fileArray);
		  } else {
			$status = 'Error';
		  }
          return $status;    
     }
	 
	 /**
      * deleteItemFromS3 : delete files from S3 bucket
	  *
      * @access	public
	  * @param  array  $fileArray Array of files to be deleted
      * @return void
      */
	 function deleteItemFromS3($fileArray) {
		 if (is_array($fileArray)) {
			 $folderName = $this->session->userdata('userDir');
			 $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
			 $a=0;
			 foreach($fileArray as $file) {
				 $hashFileName = $file;
				 echo $hashFileName;
				 $s3->deleteObject(AWS_BUCKET_NAME, $folderName.'/'.$hashFileName);
				 $a++;
			 }
		 }
	 }

	 /**
      * deleteItem : delete folder or file. If file, then delete from S3
	  *
      * @access	public
	  * @param  int    $itemNodeID Database ID of item (folder or file)
	  * @param  string $itemType   Folder or File
      * @return float  $spaceLeft  Amount of file space left for this account.
      */
	 function deleteItem($itemNodeID,$itemType=NULL) {
		 $fileArray = $this->File_maintenance->deleteItem($itemNodeID,$itemType);
		 /*
		  * Now remove any files from S3
		  */
		 FileManager_S3::deleteItemFromS3($fileArray);
	 }	 
}
?>