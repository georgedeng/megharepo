<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CalendarUpdate extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('calendar/Calendar_update','',true);
		$this->load->model('calendar/Calendar_view','',true);
        $this->load->model('trash/Trash','',true);
        $this->load->model('contacts/contacts','',TRUE);

		$this->load->library('application/CommonAppData');
		$this->load->library('application/CommonEmail');
	}

	function saveCalendar() {
		$calendarArray['calendarID']   = $_POST['calendarID'];
		$calendarArray['calendarName'] = fieldToDB($this->input->post('calendarName', TRUE));
		$calendarArray['color']        = fieldToDB($this->input->post('color', TRUE));
		$calendarArray['accountUserid']= $this->session->userdata('accountUserid');
		$calendarArray['userid']       = $this->session->userdata('userid');
        $memberString                  = $_POST['memberString'];

		$calendarID = $this->Calendar_update->saveCalendar($calendarArray);

        $outArray['CalendarID'] = $calendarID;
        $outArray['Title']      = $calendarArray['calendarName'];
        $outArray['Color']      = $calendarArray['color'];
        $outArray['Deletable']  = 1;
        $outArray['MemberUIDs'] = $memberString;
        echo json_encode($outArray);
        
		/*
		 * Save members
		 */
        $this->Calendar_update->clearMembersFromCalendar($calendarID);
        
        $teamMembersArray = makeUserArrayFromString($memberString);
        if (sizeof($teamMembersArray)>0 && $teamMembersArray != FALSE) {
            foreach($teamMembersArray as $teamMember) {
                $teamMemberPID      = $teamMember['PID'];
                $teamMemberLanguage = $teamMember['Language'];

                if ($teamMemberPID>0) {
                    $this->Calendar_update->saveMemberToCalendar($calendarID,$teamMemberPID);
                    $userArray = $this->Contacts->getContactPerson($teamMemberPID);
                    $teamMemberUID = $userArray['UID'];

                    if (empty($_POST['calendarID'])) {
                        $messageType = 'calendarNew';
                        $messageArray = $this->commonemail->createSystemMessage($messageType,$teamMemberLanguage);
                        $subject = sprintf($messageArray['subject'],$calendarArray['calendarName']);

                        $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$calendarArray['calendarName']);
                        $message = '';
                        $footer = $this->commonemail->createSystemMessage('messageFooter',$teamMemberLanguage);

                        $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$message.Chr(10).Chr(10).$footer['message'],TRUE);
                        $this->commonemail->sendEmail($finalMessage,$teamMemberPID,'team',$subject,$messageType);
                    }
                }
            }
        }
	}

	function saveEvent() {
		if ($_POST['eventForm'] == 'popup') {
			$eventArray['eventID']         = '';
			$eventArray['eventDateStart']  = $_POST['eventDateStart'];
			$eventArray['eventDateEnd']    = $_POST['eventDateEnd'];
			$eventArray['eventTitle']      = fieldToDB($this->input->post('eventTitle', TRUE));
			$eventArray['eventCalendarID'] = $_POST['eventCalendar'];
			$eventArray['eventDescription']= fieldToDB($this->input->post('eventDescription', TRUE), TRUE,TRUE,TRUE,TRUE);
			$eventArray['accountUserid']   = $this->session->userdata('accountUserid');
			$eventArray['userid']          = $this->session->userdata('userid');
            $eventArray['eventTags']       = '';
            $eventArray['eventRepeats']    = '';
            $eventArray['eventReminder']   = '';
            $eventArray['eventLocation']   = '';

		} else {
            $eventArray['action']          = $_POST['action'];
            $eventArray['eventID']         = $_POST['eventID'];
            $eventArray['eventTitle']      = fieldToDB($this->input->post('eventTitle', TRUE));
			$eventArray['eventDateStart']  = trim($_POST['eventDateStart']);
            $eventArray['eventDateEnd']    = trim($_POST['eventDateEnd']);
			$eventArray['eventTimeStart']  = trim($_POST['eventTimeStart']);
			$eventArray['eventTimeEnd']    = trim($_POST['eventTimeEnd']);
            $eventArray['eventCalendarID'] = $_POST['eventCalendar'];
            $eventArray['eventRepeats']    = $_POST['eventRepeats'];
            $eventArray['eventReminder']   = 0;
            if ($_POST['eventReminder'] == 'on') {
                $eventArray['eventReminder'] = 1;
            }
            $eventArray['eventLocation']   = fieldToDB($this->input->post('eventLocation', TRUE), TRUE,TRUE,FALSE,TRUE);
            $eventArray['eventDescription']= fieldToDB($this->input->post('eventDescription', TRUE),TRUE,FALSE,FALSE,FALSE,TRUE,FALSE);
            $eventArray['accountUserid']   = $this->session->userdata('accountUserid');
			$eventArray['userid']          = $this->session->userdata('userid');
            
            $eventArray['eventTags'] = fieldToDB($this->input->post('tagHolder', TRUE));
            $eventArray['eventTags'] = str_replace(',',' ',$eventArray['eventTags']);                

            
            /*
             * Format times
             */
			if (!empty($eventArray['eventDateStart'])) {
				if (!empty($eventArray['eventTimeStart'])) {
					$eventTimeStart = convertTimeToMySQL($eventArray['eventTimeStart']);
				} else {
					$eventTimeStart = '00:00:00';
				}
                $eventArray['eventDateStart'] = $eventArray['eventDateStart'].' '.$eventTimeStart;
			} else {
				$eventArray['eventDateStart'] = '';
			}
			if (!empty($eventArray['eventDateEnd'])) {
				if (!empty($eventArray['eventTimeEnd'])) {
					$eventTimeEnd = convertTimeToMySQL($eventArray['eventTimeEnd']);
				} else {
					$eventTimeEnd = '00:00:00';
				}
                $eventArray['eventDateEnd']   = $eventArray['eventDateEnd'].' '.$eventTimeEnd;
			} else {
				$eventArray['eventDateEnd'] = '';
			}			
		}

		if (!empty($eventArray['eventDateStart'])) {
			$eventArray['DBEventDateStart'] = convertMySQLToGMT($eventArray['eventDateStart'],'push','Y-m-d H:i:s');
		} else {
			$eventArray['DBEventDateStart'] = '';
		}
		if (!empty($eventArray['eventDateEnd'])) {
			$eventArray['DBEventDateEnd'] = convertMySQLToGMT($eventArray['eventDateEnd'],'push','Y-m-d H:i:s');
		} else {
			$eventArray['DBEventDateEnd'] = '';
		}

		$calendarArray = $this->Calendar_view->getCalendarDetail($eventArray['eventCalendarID']);
		$calendarColor = $calendarArray[0]['Color'];
		$calendarTitle = $calendarArray[0]['Title'];
		$eventID = $this->Calendar_update->saveEvent($eventArray);

        /*
         * Save tags
         */
        if (!empty($eventArray['eventTags'])) {
            $this->commonappdata->saveTags($eventArray['eventTags'],'calendar');
        }

        if ($_POST['eventForm'] != 'popup') {
            /*
             * Clear any event guests in the database
             */
            $this->Calendar_update->deleteEventGuests($eventID);
            /*
             * Now save any guests for this event
             */
            $guestArray  = explode('|',$_POST['guests']);
            $guestArray = makeUserArrayFromString($_POST['guests']);

            if (count($guestArray)>0) {
                foreach($guestArray as $contact) {
                    if (!empty($contact)) {
                        $contactID = $contact['PID'];
                        $type      = $contact['SystemType'];
                        $langCode  = $contact['Language'];
                        
                        if ($type == 'client') {
                            $recipType = 'C';
                        } else {
                            $recipType = 'P';
                        }

                        /*
						 * Create email message to send to guests
						 */
                        if ($eventArray['action'] == 'edit') {
                            $messageArray = $this->commonemail->createSystemMessage('eventUpdate',$langCode);
                        } elseif ($eventArray['action'] == 'add') {
                            $messageArray = $this->commonemail->createSystemMessage('eventNew',$langCode);
                        }
                        $subject = sprintf($messageArray['subject'],$eventArray['eventTitle']);

                        $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'));
                        $footer = $this->commonemail->createSystemMessage('messageFooter',$langCode);

                        $dateString = '';
                        if (!empty($eventArray['eventDateStart'])) {
                            $startDateTime = date('M j, Y g:m a',strtotime($eventArray['eventDateStart'])).' '.$this->session->userdata('gmtOffset');
                            $dateString = $startDateTime;
                        }
                        if (!empty($eventArray['eventDateEnd'])) {
                            $endDateTime = date('M j, Y g:m a',strtotime($eventArray['eventDateEnd'])).' '.$this->session->userdata('gmtOffset');
                            $dateString .= ' to '.$endDateTime;
                        }
                        $message  = $eventArray['eventTitle'];
                        $message .= Chr(10).$dateString;
                        if (!empty($eventArray['eventDescription'])) {
                            $message .= Chr(10).Chr(10).$eventArray['eventDescription'];
                        }
                        $message .= Chr(10).Chr(10).lang('email_event_link').Chr(10);
                        $message .= BASE_URL.'calendar/CalendarViewExternal/viewEvent/'.$this->session->userdata('persAuthString').'/'.$eventID;

                        $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$message.Chr(10).Chr(10).$footer['message'],TRUE);
                        $this->commonemail->sendEmail($finalMessage,$contactID,$type,$subject,'eventShare');

                        $this->Calendar_update->saveEventGuest($contactID,$eventID,$recipType);
                    }
                }
            }
        }
        $outArray['EventID']         = $eventID;
        $outArray['StartDateTime']   = $eventArray['eventDateStart'];
        $outArray['EndDateTime']     = $eventArray['eventDateEnd'];

        if (determineAllDay($outArray['StartDateTime'], $outArray['EndDateTime']) == true) {
            $outArray['AllDay'] = '1';
        } else {
            $outArray['AllDay'] = '0';
        }

        $outArray['StartDateTimeTS'] = standard_date('DATE_ISO8601FC',strtotime($eventArray['eventDateStart']));
        $outArray['EndDateTimeTS']   = standard_date('DATE_ISO8601FC',strtotime($eventArray['eventDateEnd']));
        $outArray['Title']           = $eventArray['eventTitle'];
        $outArray['URL']             = '#';
        $outArray['Description']     = $eventArray['eventDescription'];
        $outArray['CssClass']        = $calendarColor;
        $outArray['CalendarID']      = $eventArray['eventCalendarID'];
		$outArray['CalendarTitle']   = $calendarTitle;
        $outArray['IconClass']       = 'icon_calendar_small';
        $outArray['Dragable']        = '1';
        $outArray['Deletable']       = '1';
        $outArray['Editable']        = '1';
        $outArray['Type']            = 'Event';
        echo json_encode($outArray);
    }   

    function moveEventsToCalendar($calendarFrom,$calendarTo) {
        $this->Calendar_update->moveEventsToCalendar($calendarFrom,$calendarTo);
        $calendarArrayFrom = $this->Calendar_view->getCalendarDetail($calendarFrom);
        $calendarArrayTo   = $this->Calendar_view->getCalendarDetail($calendarTo);
        echo '{"ColorFrom":"'.$calendarArrayFrom[0]['Color'].'","ColorTo":"'.$calendarArrayTo[0]['Color'].'"}';
    }

    function updateEventDatesAfterMove() {
        $eventID      = $_POST['eventID'];
        $newDateStart = $_POST['newStartDate'];
        $newDateEnd   = $_POST['newEndDate'];
        if (empty($newDateEnd)) {
            $newDateEnd = '0000-00-00 00:00:00';
        } else {
            $DBEventDateEnd = convertMySQLToGMT($newDateEnd,'push','Y-m-d H:i:s');
        }
        $DBEventDateStart = convertMySQLToGMT($newDateStart,'push','Y-m-d H:i:s');

        $newDateArray = $this->Calendar_update->updateEventDatesAfterMove($eventID,$DBEventDateStart,$DBEventDateEnd);
    }

    function deleteEvent($eventID,$tagForDelete=TRUE) {
        $eventID = $this->Calendar_update->deleteEvent($eventID,$tagForDelete,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }

    function deleteCalendar($calendarID,$withEvents=0,$tagForDelete=TRUE) {
        /*
         * If withEvents = 1 then we delete all events under this
         * calendar as well.
         */
        $this->Calendar_update->deleteCalendar($calendarID,$withEvents,$tagForDelete,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }

    function restoreCalendar($calendarID) {
        $this->Calendar_update->restoreCalendar($calendarID,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }

    function restoreEvent($eventID) {
        $this->Calendar_update->restoreEvent($eventID,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }
}
?>