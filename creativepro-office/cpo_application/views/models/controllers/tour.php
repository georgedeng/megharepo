<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tour extends Controller {
        function __construct()
        {
                parent::Controller();

        }

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = 'txtTourHeader_public.png';
        $data['pageTitle']       = 'Product Tour';
        $data['jsFileArray']     = array('jquery/plugins/jquery.easing-1.3.js','jquery/plugins/jquery.colorbox.js','jquery/plugins/jquery.fixfloat.js');

        return $data;
    }

    function index() {
                $data = Tour::_init();
        $this->load->view('public_site/Tour',$data);
    }

    function video($videoName=NULL,$videoTitle=NULL) {
        if ($videoName == NULL) {
            //Tour::index();
            $data = Tour::_init();
            $this->load->view('public_site/TourVideos',$data);
        } else {
            $data = Tour::_init();
            $data['videoName']  = $videoName;
            $data['videoTitle'] = $videoTitle;
            $this->load->view('public_site/TourVideo',$data);
        }
    }
}
?>