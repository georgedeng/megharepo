<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Refund extends Controller {
	function __construct()
	{
		parent::Controller();
        $this->load->model('common/public_site','',true);
	}

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = '';
        $data['pageTitle']       = 'Refund Policy';
        $data['jsFileArray']     = array();

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
		$data = Refund::_init();
        $this->load->view('public_site/Refund',$data);
    }

    function popup() {
        $data = Refund::_init();
        $this->load->view('public_site/RefundPopup',$data);
    }
}
?>