<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rss extends Controller {
	function __construct()
	{
		parent::Controller();
        $this->load->model('common/app_data','AppData',TRUE);
		$this->load->model('accounts/Accounts','',TRUE);
		$this->load->model('projects/Project_view','',TRUE);
		$this->load->model('tasks/Task_view','',TRUE);
		$this->load->model('calendar/Calendar_view','',TRUE);
        $this->load->model('blog/Blog_model','',TRUE);

		$this->load->helper('security');
	}

    function createRSSFeed($authKey,$feedType,$feedItemID=NULL,$feedItemType=NULL) {
		$loginArray = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF']);
        
        switch ($feedType) {
			case "task":
				$rss = Rss::createTaskFeed($feedItemID,$loginArray);
				break;
			case "calendar":
				$rss = Rss::createCalendarFeed($feedItemID,$feedItemType,$loginArray);
				break;
			case "project":
				$rss = Rss::createProjectFeed($feedItemID,$accountUserid);
				break;
			case "message":
				$rss = Rss::createMessageFeed($feedItemID,$feedItemType,$accountUserid);
				break;
			case "finance":
				$rss = Rss::createFinanceFeed($feedItemID,$accountUserid);
				break;
			case "files":
				$rss = Rss::createFileFeed($feedItemID,$feedItemType,$accountUserid);
				break;
		}
		$rssBody = $rss[0];
		$headerArray = $rss[1];
		
		$rssContent  = Rss::createRssHeader($headerArray);
		$rssContent .= $rssBody;
		$rssContent .= Rss::createRssFooter();
		echo $rssContent;
    }

    function productBlogFeed() {
        $headerArray['Title'] = CPO_BLOG_TITLE;
        $headerArray['Link']  = CPO_BLOG_URL;
        $blogPostArray = $this->Blog_model->getBlogPosts(20,NULL,NULL,NULL,1);

        $rssContent  = Rss::createRssHeader($headerArray);
		
        $rssBody = '';
        foreach($blogPostArray as $post) {
            $rssBody .= '<item>'.Chr(10);
            $rssBody .= '<title>'.stripslashes($post['Title']).'</title>'.Chr(10);
            $rssBody .= '<link>'.site_url('blog/entry'.$post['EntryURL']).'</link>'.Chr(10);
            $rssBody .= '<description></description>'.Chr(10);
            $rssBody .= '<guid isPermaLink="true">'.site_url('blog/entry/'.$post['EntryURL']).'</guid>'.Chr(10);
            $rssBody .= '<pubDate>'.standard_date('DATE_RFC822',strtotime($post['DateEntry'])).'</pubDate>'.Chr(10);
            $rssBody .= '</item>'.Chr(10);
        }
        
        $rssContent .= $rssBody;
		$rssContent .= Rss::createRssFooter();
		echo $rssContent;
    }

	function createRssHeader($headerArray) {
		$rssHeader  = '<?xml version="1.0" encoding="UTF-8"?>'.Chr(10);
		$rssHeader .= '<?xml-stylesheet type="text/xsl" media="screen" href="/~d/styles/rss2full.xsl"?>'.Chr(10);
		$rssHeader .= '<?xml-stylesheet type="text/css" media="screen" href="http://feeds.feedburner.com/~d/styles/itemcontent.css"?>'.Chr(10);
		$rssHeader .= '<rss version="2.0">'.Chr(10);
		$rssHeader .= '<channel>'.Chr(10);
        $rssHeader .= '<language>en-us</language>';
		$rssHeader .= '<title>'.$headerArray['Title'].'</title>'.Chr(10);
		$rssHeader .= '<link>'.$headerArray['Link'].'</link>'.Chr(10);
		$rssHeader .= '<description>'.$headerArray['Title'].'</description>'.Chr(10);
		$rssHeader .= '<generator uri="'.SITE_URL.'">'.SITE_TITLE.' ('.MAIL_SUPPORT.')</generator>'.Chr(10);
		return $rssHeader;
	}

	function createRssFooter() {
		$rssFooter = '</channel></rss>';
		return $rssFooter;
	}

	function createTaskFeed($projectID,$loginArray) {
		$userid        = $loginArray['UID'];
        $accountUserid = $loginArray['UseridAccount'];
        $permissions   = $loginArray['RolePermissions'];
		$langCode      = $loginArray['Language'];
		$authString    = $loginArray['AuthString'];
		loadLanguageFiles($langCode);

		$rssBody = '';
		$accountArray = $this->Accounts->getAccountInformation($accountUserid);
		$permArray = createPermissionsArray($permissions);

		if ($projectID>0) {
			$projectArray = $this->Project_view->getProjectInformation($projectID);
			$taskArray    = $this->Task_view->getMilestonesForProject($projectID,1);
			$title = lang('widget_tasks').': '.$projectArray['Title'];

			$headerArray['Title'] = $title;
			$headerArray['Link']  = '';
			$headerArray['Description'] = $title;
		} else {
			$title = lang('widget_tasks').': '.$accountArray['Company'];

			$headerArray['Title'] = $title;
			$headerArray['Link']  = '';
			$headerArray['Description'] = $title;
		}

		foreach($taskArray as $milestone) {
			$milestoneTitle = $milestone['Title'];
			foreach($milestone['Tasks'] as $task) {
				$taskID      = $task['TaskID'];
				$taskTitle   = $task['Title'];
				$creator     = $task['CreatorName'];
				$itemTitle   = $taskTitle.' : '.$milestoneTitle;
				if (!empty($task['Description'])) {
					$description = '<![CDATA['.$task['Description'].']]>';
				} else {
					$description = '';
				}

				$statusArray = getProjectStatus($task['Status'],$task['Date_Start'],$task['Date_End']);
				if ($statusArray[1] == 'status_in_progress') {
					$taskTitle = '[>] '.$taskTitle;
				} elseif ($statusArray[1] == 'status_past_due') {
					$taskTitle = '[!] '.$taskTitle;
				} elseif ($statusArray[1] == 'status_completed') {
					$taskTitle = '[*] '.$taskTitle;
				} elseif ($statusArray[1] == 'status_on_hold') {
					$taskTitle = '[?] '.$taskTitle;
				}

				/*
				 * Should this user be able to view this task?
				 */
				$shouldView = TRUE;
				if ($permArray['taskViewOwn'] == 1) {
					$shouldView = FALSE;
					foreach($task['TeamMembers'] as $teamMember) {
						if ($teamMember['UID'] == $userid) {
							$shouldView = TRUE;
						}
					}
				}

				if ($shouldView == TRUE) {
					$rssBody .= '<item>'.Chr(10);
					$rssBody .= '<title>'.$taskTitle.'</title>'.Chr(10);
					$rssBody .= '<link>'.site_url('tasks/TaskViewExternal').'/viewTask/'.$authString.'/'.$taskID.'</link>'.Chr(10);
					$rssBody .= '<description>'.$description.'</description>'.Chr(10);
					$rssBody .= '<guid>'.site_url('tasks/TaskViewExternal').'/viewTask/'.$authString.'/'.$taskID.'</guid>'.Chr(10);
					$rssBody .= '</item>'.Chr(10);
				}
			}
		}
		return array($rssBody,$headerArray);
	}

	function createCalendarFeed($feedItemID=NULL,$feedItemType=NULL,$loginArray) {
		$userid        = $loginArray['UID'];
        $accountUserid = $loginArray['UseridAccount'];
        $permissions   = $loginArray['RolePermissions'];
		$langCode      = $loginArray['Language'];
		$authString    = $loginArray['AuthString'];
		loadLanguageFiles($langCode);

		if ($this->session->userdata('logged') != TRUE) {
            /*
			 * Set timezone and DST in session for convertMySQLToGMT method.
			 */
			$this->session->set_userdata('gmtOffset',$loginArray['Timezone']);
			$this->session->set_userdata('daylightSavingsTime',$loginArray['DaylightSavingsTime']);
        }

		$rssBody = '';
		$accountArray = $this->Accounts->getAccountInformation($accountUserid);

		if (empty($feedItemID) && empty($feedItemType)) {
			/*
			 * Get all calendars for this user
			 */
			$headerArray['Title'] = lang('calendar_your_calendars');
			$headerArray['Link']  = '';
			$headerArray['Description'] = lang('calendar_your_calendars');
			
			/*
			 * Only get dates that are 90 days in the future.
			 */
			$endDate = new_date_from_days(date('Y-m-d'),90);
			$eventArray = $this->Calendar_view->getCalendarEntryList(NULL,20,$endDate,$accountUserid);
		} elseif ($feedItemType == 'calendar' && $feedItemID>0) {
			/*
			 * Then just get events for 1 calendar
			 */
			/*
			 * Only get dates that are 90 days in the future.
			 */
			$endDate = new_date_from_days(date('Y-m-d'),90);
			$eventArray = $this->Calendar_view->getCalendarEntryList($feedItemID,20,$endDate,$accountUserid);
		}

		foreach($eventArray as $event) {
			$eventID       = $event['EventID'];
			$eventTitle    = $event['Title'];
			$creator       = $event['NameFirst'].' '.$event['NameLast'];
			$eventCalendar = $event['CalendarTitle'];

			if ($event['DateEnd'] != '0000-00-00' && $event['DateEnd'] != '0000-00-00 00:00:00') {
				$dateEnd = ' to '.convertMySQLToGMT($event['DateEnd'],'pull','M j, g:i a');
			} else {
				$dateEnd = '';
			}

			$dateStart  = convertMySQLToGMT($event['DateStart'],'pull','M j, g:i a');
			$dateString = $dateStart.$dateEnd;
			
			$itemTitle   = $eventTitle.' : '.$dateString;
			$description  = '<![CDATA[';
			$description .= '<p>'.$eventTitle.' '.$dateString.'</p>';
			$description .= '<p>'.sprintf(lang('calendar_post_info'),$creator,$eventCalendar).'</p>';
			$description .= '<p>'.$event['Description'].'</p>';
			$description .= '<p>'.$event['Location'].'</p>';
			$description .= ']]>';

			$rssBody .= '<item>'.Chr(10);
			$rssBody .= '<title>'.$itemTitle.'</title>'.Chr(10);
			$rssBody .= '<link>'.site_url('calendar/CalendarViewExternal').'/viewEvent/'.$authString.'/'.$eventID.'</link>'.Chr(10);
			$rssBody .= '<description>'.$description.'</description>'.Chr(10);
			$rssBody .= '<guid>'.site_url('calendar/CalendarViewExternal').'/viewEvent/'.$authString.'/'.$eventID.'</guid>'.Chr(10);
			$rssBody .= '</item>'.Chr(10);
		}
		return array($rssBody,$headerArray);
	}

    function createFinanceFeed($feedType,$loginArray) {
		$userid        = $loginArray['UID'];
        $accountUserid = $loginArray['UseridAccount'];
        $permissions   = $loginArray['RolePermissions'];
		$langCode      = $loginArray['Language'];
		loadLanguageFiles($langCode);

		$rssBody = '';
		$accountArray = $this->Accounts->getAccountInformation($accountUserid);

		if ($feedType == 'invoice') {
			/*
			 * Get all invoices for this user
			 */
			$headerArray['Title'] = lang('calendar_your_calendars');
			$headerArray['Link']  = '';
			$headerArray['Description'] = lang('calendar_your_calendars');

			/*
			 * Only get dates that are 30 days in the future.
			 */
			$endDate = new_date_from_days(date('Y-m-d'),30);
		} elseif ($feedItemType == 'expense') {
			/*
             * Get all expenses for this user
             */
			$endDate = new_date_from_days(date('Y-m-d'),30);
		}

		foreach($eventArray as $event) {
			$eventTitle    = $event['Title'];
			$creator       = $event['NameFirst'].' '.$event['NameLast'];
			$eventCalendar = $event['CalendarTitle'];

			if ($event['DateEnd'] != '0000-00-00' && $event['DateEnd'] != '0000-00-00 00:00:00') {
				$dateEnd = ' to '.convertMySQLToGMT($event['DateEnd'],'pull','M j, g:i a');
			} else {
				$dateEnd = '';
			}

			$dateStart  = convertMySQLToGMT($event['DateStart'],'pull','M j, g:i a');
			$dateString = $dateStart.$dateEnd;

			$itemTitle   = $eventTitle.' : '.$dateString;
			$description  = '<![CDATA[';
			$description .= '<p>'.$eventTitle.' '.$dateString.'</p>';
			$description .= '<p>'.sprintf(lang('calendar_post_info'),$creator,$eventCalendar).'</p>';
			$description .= '<p>'.$event['Description'].'</p>';
			$description .= '<p>'.$event['Location'].'</p>';
			$description .= ']]>';

			$rssBody .= '<item>'.Chr(10);
			$rssBody .= '<title>'.$itemTitle.'</title>'.Chr(10);
			$rssBody .= '<link>'.site_url('calendar/CalendarView').'</link>'.Chr(10);
			$rssBody .= '<description>'.$description.'</description>'.Chr(10);
			$rssBody .= '<guid>'.site_url('calendar/CalendarView').'</guid>'.Chr(10);
			$rssBody .= '</item>'.Chr(10);
		}
		return array($rssBody,$headerArray);
	}
}
?>
