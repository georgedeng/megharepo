<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Updates extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
        $this->load->model('bugs/bug_model','BugModel',true);
        $this->load->model('contacts/contacts','Contacts',true);
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtUpdatesHeader_public.png';
        $data['rightColumnComponents'] = array('loginBox','whosUsingCPO_nobg');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Updates : Bug fixes and feature additions';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        $data['currentAccountLevel'] = $this->session->userdata('accountLevel');
        $data['itemArray'] = $this->BugModel->getItemsForUpdates(100);        
        return $data;
    }

    function index() {
        $data = Updates::_init();
        $this->load->view('public_site/Updates',$data);
    }
}
?>