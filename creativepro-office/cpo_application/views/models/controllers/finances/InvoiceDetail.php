<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class InvoiceDetail extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('common/app_data','AppData',TRUE);
		$this->load->model('finances/finance_view','FinanceView',TRUE);
		$this->load->library('application/CommonFinance');
	}

	function _init($invoiceID=NULL) {
		$data['page']              = 'finances';
		$data['pageTitle']         = '<a href="'.site_url('finances/FinanceView').'">'.lang('finance_invoices').'</a>';
		$data['wysiwyg']           = 0;
		$data['map']               = 0;
        $data['invoiceID']         = $invoiceID;
		$data['jsFileArray']       = array('jsFinances.js');
		$data['pageIconClass']     = 'iconinvoices';
		$data['pageLayoutClass']   = 'withRightColumn';
		$data['rightColumnComponents'] = array('financeInvoiceDetail');
		$data['currencyMark']         = getCurrencyMark($this->session->userdata('currency'));
        
        $data['invoiceInformation'] = $this->commonfinance->getInvoiceDetails($invoiceID);
		$jsonArray = json_decode($data['invoiceInformation'], true);

		$data['pageSubTitle']      = $jsonArray['Title'];

		return $data;
	}

	function index($invoiceID=NULL) {
        if (!is_numeric($invoiceID)) {
			header('Location: '.BASE_URL.'finances/FinanceView');
		}
		$data = InvoiceDetail::_init($invoiceID);
		$this->load->view('finances/InvoiceDetail',$data);
	}
}
?>
