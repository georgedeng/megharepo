<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FinanceView extends Controller {
	function __construct()
	{
		parent::Controller();	
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
		$this->load->model('finances/Finance_view','',true);
        $this->load->model('finances/Finance_reports','',true);
        $this->load->model('finances/Finance_view_expenses','',true);
        $this->load->model('finances/Invoice_update','',true);
		$this->load->model('users/User_information','',TRUE);
		$this->load->model('settings/Settings_update','',TRUE);
        $this->load->model('contacts/Contacts','',true);
        $this->load->model('messaging/Site_messages','',true);
        
		$this->load->library('application/CommonFinance');
		$this->load->library('application/CommonAppData');
        $this->load->library('application/CommonEmail');
	}
	
	function _init($tag=NULL,$invoiceID=NULL,$printInvoice=NULL,$expenseID=NULL,$tagExpense=NULL) {
		global $subMenuArray;
		
		$data['page']                  = 'finances';
		$data['pageTitle']             = lang('finance_page_title');
		$data['pageSubTitle']          = '';
		$data['wysiwyg']               = 0;
        $data['charts']                = 1;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsFinances.js','jsExpenses.js','jsFinanceReports.js','jsContacts.js','jsSettings.js');
		$data['pageIconClass']         = 'iconinvoices';
		$data['pageLayoutClass']       = 'withRightColumn';
		$data['tag']                   = $tag;
        $data['tagExpense']            = $tagExpense;
		$data['rightColumnComponents'] = array('financeSidebar','tagRender','feedExportSidebar');

		$invoiceTagArray = $this->commonappdata->renderTags('invoice','array');
        $expenseTagArray = $this->commonappdata->renderTags('expense','array');
        
        $ledgerTagArray = array_merge($invoiceTagArray,$expenseTagArray);
        sort($ledgerTagArray);
        $newLedgerTagArray['tagURL'] = $ledgerTagArray[0];
        $newLedgerTagArray['Tags'] = $ledgerTagArray[1];
        
        $data['tagRenderArray'] = $newLedgerTagArray;
        $data['tagRenderArray2'] = $invoiceTagArray;

        $permArray = $this->session->userdata('permissions');

        $data['pageButtons'] = array();
        if ($permArray['invoiceCreateSend'] == 1 || $permArray['invoiceCreateSendToAccountOwner'] == 1) {
            array_push($data['pageButtons'], '<button class="buttonExpand buttonAddInvoice hide action" id="buttonAddInvoice" title="'.lang('finance_invoice').'"><span class="add">'.lang('finance_invoice').'</span></button>');
            array_push($data['pageButtons'], '<button class="buttonExpand buttonAddEstimate hide action" id="buttonAddEstimate" title="'.lang('finance_estimate').'"><span class="add">'.lang('finance_estimate').'</span></button>');
        }
        if ($permArray['expenseCreate'] == 1) {
            array_push($data['pageButtons'], '<button class="buttonExpand buttonAddExpense hide action" id="buttonAddExpense" title="'.lang('finance_expense').'"><span class="add">'.lang('finance_expense').'</span></button>');
        }
        if ($permArray['invoiceUpdate'] == 1) {
            array_push($data['pageButtons'], '<button class="buttonExpand buttonEdit hide action" id="buttonEdit" title="'.lang('button_edit').'"><span class="edit">'.lang('button_edit').'</span></button>');
        }
        if ($permArray['invoiceDelete'] == 1) {
            array_push($data['pageButtons'], '<button class="buttonExpand buttonDelete hide action" id="buttonDelete" title="'.lang('button_delete').'"><span class="delete">'.lang('button_delete').'</span></button>');
        }
        array_push($data['pageButtons'], '<button class="buttonExpand buttonPrint hide action" id="buttonPrint" title="'.lang('button_print').'"><span class="print">'.lang('button_print').'</span></button>');
        
        $data['invoiceID']             = $invoiceID;
        $data['expenseID']             = $expenseID;

        $data['rptMinYearInvoice'] = date('Y');
        $data['rptMinYearExpense'] = date('Y');
        $rptMinYearInvoice = $this->Finance_reports->getInvoiceReportYearRange($this->session->userdata('accountUserid'));
        if ($rptMinYearInvoice != FALSE) {
            $data['rptMinYearInvoice'] = $rptMinYearInvoice;
        }

        $today = convertMySQLToGMT(date('Y-m-d'),$direction='pull',$format='Y-m-d');
		$data['payDateDefault'] = MySQLDateToJS($today);

        /*
         * Should we print this invoice?
         */
        if ($printInvoice != NULL && $printInvoice != 0) {
            $printInvoice = 1;
        } else {
            $printInvoice = 0;
        }
        $data['printInvoice'] = $printInvoice;

        $permissionArray = $this->session->userdata('permissions');
		$viewAll = TRUE;

		if ($permissionArray['messageViewOwn'] == 1) {
            $viewAll = FALSE;
		}

        $numberMessagesInvoiceArray = $this->Site_messages->getNumberOfMessagesForSomething(NULL,'invoice',$this->session->userdata('userid'),$this->session->userdata('accountUserid'),$this->session->userdata('pid'),$viewAll);
		$data['numberMessagesInvoice'] = $numberMessagesInvoiceArray['NoOfMessagesTotal'];
        
        /*
		 * Create RSS Feed Links
		 */
        $invoiceFeedLink  = site_url('rss/Rss').'/createRSSFeed/'.$this->session->userdata('persAuthString').'/finance/invoice';
		$invoiceFeedTitle = lang('finance_invoices');

		$data['rssFeeds'] = array(
			//array('Link' => $invoiceFeedLink, 'Title' => $invoiceFeedTitle, 'Type' => 'invoice')
		);
        
        /*
         * Get any vendors
         */
        $vendorArray = $this->Contacts->getContacts($this->session->userdata('accountUserid'),NULL,NULL,0,1);
        $data['vendors'] = json_encode($vendorArray);

        $financeReportExcelLink  = '';
        $financeReportExcelTitle = lang('common_export_excel');
        $data['exports'] = array(
            array('Link' => $financeReportExcelLink, 'Title' => $financeReportExcelTitle, 'Type' => 'excel', 'ID' => 'excelFinanceReport')
        );
        
        $data['ledgerDateStart'] = date('m/01/Y',strtotime('-3 months'));
        $data['ledgerDateEnd']   = date('m/t/Y',strtotime('this month'));
        
        /*
         * See if we have a PayPal email address in settings.
         * If so, give user the option to add PayPal email link
         * when emailing invoice.
         */
        $data['payPalEmail'] = null;
        $payPalEmailArray = $this->Settings_update->getSettings($this->session->userdata('accountUserid'),'emailPayPal');
        if ($payPalEmailArray != false) {
            $data['emailPayPal'] = $payPalEmailArray['SettingValue'];
        }       

		return $data;
	}

	function index($tag=NULL) {
		$data = FinanceView::_init();
		$this->load->view('finances/FinanceView',$data);
	}

	function tag($tag=NULL) {
		$data = FinanceView::_init($tag);
		$this->load->view('finances/FinanceView',$data);
	}

    function viewInvoice($invoiceID=NULL,$printInvoice=NULL) {
        $viewAll = TRUE;
        $permArray = $this->session->userdata('permissions');
        if ($permArray['invoiceViewOwn'] == 1 && $permArray['invoiceViewAll'] != 1) {
            $viewAll = FALSE;
        }
        $viewThisInvoice = $this->Finance_view->checkForValidInvoice($invoiceID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'),NULL,$viewAll);
        if ($viewThisInvoice == FALSE || empty($invoiceID)) {
			/*
			 * Send them back to this users' invoice view page.
			 */
			header('Location: '.site_url('finances/FinanceView'));
		}
        $data = FinanceView::_init(NULL,$invoiceID,$printInvoice);
		$this->load->view('finances/FinanceView',$data);
    }

    function viewExpense($expenseID=NULL) {
        $viewAll = TRUE;
        $permArray = $this->session->userdata('permissions');
        if ($permArray['expenseViewOwn'] == 1 && $permArray['expenseViewAll'] != 1) {
            $viewAll = FALSE;
        }
        $viewThisExpense = $this->Finance_view->checkForValidExpense($expenseID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'),NULL,$viewAll);
        if ($viewThisExpense == FALSE || empty($expenseID)) {
			/*
			 * Send them back to this users' invoice view page.
			 */
			header('Location: '.site_url('finances/FinanceView'));
		}
        
        $data = FinanceView::_init(NULL,NULL,NULL,$expenseID);
		$this->load->view('finances/FinanceView',$data);
    }
    
	/**
	 * getInvoiceList
	 *
	 * @access	public
	 * @param	int    $itemID Userid, ClientID, ProjID depending on if we want all invoices
	 *                         for account owner, all invoices for a client or all invoices for a project.
	 * @param   string $group  user, client, project
	 * @param   string $renderType
	 * @param   bool   $return
	 * @param   int    $limit
	 * @return	array  Array of invoice data
	 */
	function getInvoiceList($itemID,$group=null,$tag=NULL,$status=NULL,$renderType=null,$return=1) {
		$permArray = $this->session->userdata('permissions');
        if ($permArray['invoiceViewOwn'] == 1 && $permArray['invoiceViewAll'] != 1) {
            /*
             * We're only getting invoices that this user has sent.
             */
            $itemID = $this->session->userdata('userid');
        } elseif ($group == 'user') {
			$itemID = $this->session->userdata('accountUserid');
		}
        
        /*
         * Check for a search term
         */
        $searchTerm = null;
        if (isset($_POST['term'])) {
            $searchTerm = $this->input->post('term', TRUE);
        }

        if ($status == 'null') {
            $status = NULL;
        }

        if (isset($_POST['filter'])) {
            $filterArray = $_POST;
            $invoiceArray = $this->Finance_view->getInvoiceList($itemID,$group,$tag,$status,$filterArray,$this->session->userdata('accountUserid'),$searchTerm);
        } else {
            $invoiceArray = $this->Finance_view->getInvoiceList($itemID,$group,$tag,$status,null,$this->session->userdata('accountUserid'),$searchTerm);
        }

		if ($renderType == 'json') {
            if (count($invoiceArray)<1) {
                $noDataMessage = lang('help_invoice_no_invoices');
                if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
                    $noDataMessage = lang('help_invoice_no_invoices_client');
                }
                $jsonString = '{"Invoices":[],"NoDataMessage":"'.$noDataMessage.'"}';
            } else {
                $headerString = lang('common_number').'|'.lang('common_title').'|'.lang('client_client').'|'.lang('finance_date_created').'|'.lang('common_status').'|'.lang('common_total').'|'.lang('common_amount_due');
                $footerString = lang('finance_total_invoiced').'|'.lang('finance_total_due');
			
                $currencyMark = getCurrencyMark($this->session->userdata('currency'));
                $jsonString = '{"HeaderLabels":"'.$headerString.'","FooterLabels":"'.$footerString.'","Invoices":[';
                $totalInvoiced = 0;
                $totalDue = 0;
                foreach($invoiceArray as $invoice) {
                    $totalInvoiced = $totalInvoiced+$invoice['InvTotal'];
                    $invoice['InvoiceTotal'] = $invoice['InvTotal'];
                    $invoice['InvTotal'] = $currencyMark.number_format($invoice['InvTotal'],2);

                    $amountDue = $invoice['InvoiceTotal'] - $invoice['TotalPayments'];
                    $totalDue = $totalDue+$amountDue;
                    $invoice['AmountDue']= $amountDue;
                    $invoice['AmtDue']   = $currencyMark.number_format($amountDue,2);
				
                    /*
                     * Invoice status
                     */
                    $statusArray = getInvoiceStatus($invoice['Status'],$invoice['Paid'],$invoice['Sent'],$invoice['DateInvoice'],$invoice['DatePaid'],$invoice['DateSent'],$invoice['PaymentDueDate'],$invoice['PaymentDueDays']);
                    $invoice['StatusHuman']   = $statusArray[0];
                    $invoice['StatusMachine'] = $statusArray[1];

                    if ($invoice['DateEntry'] != '0000-00-00') {
                        $invoice['DateEntry'] = date('M j, Y', strtotime($invoice['DateInvoice']));
                        $invoice['DateEntrySort'] = date('m/d/Y', strtotime($invoice['DateInvoice']));
                    } else {
                        $invoice['DateEntry'] = '';
                        $invoice['DateEntrySort'] = '';
                    }
                    
                    $invoice['Recurring'] = formatRecurring($invoice['Recurring']);
				
                    $jsonString .= json_encode($invoice).',';
                }
                $jsonString = substr($jsonString,0,-1).'],"TotalInvoiced":"'.$currencyMark.number_format($totalInvoiced,2).'","TotalDue":"'.$currencyMark.number_format($totalDue,2).'"}';
            }
			if ($return == 1) {
				return $jsonString;
			} else {
				echo $jsonString;
			}		
		}	
	}
    
    function getLedger($dateStart=null,$dateEnd=null,$itemID=null,$group=null,$tag=null,$renderType=null,$return=0) {
        /*
         * Check for valid date range
         */
        if (strtotime($dateStart) > strtotime($dateEnd)) {
            return false;
        }
        
        /*
         * Check for a search term
         */
        $searchTerm = null;
        if (isset($_POST['term'])) {
            $searchTerm = $this->input->post('term', TRUE);
        }
        
        $permArray = $this->session->userdata('permissions');
        $getPayments = true;
        if ($permArray['invoiceViewOwn'] == 1 && $permArray['invoiceViewAll'] != 1) {
            /*
             * We're only getting payments for invoices that this user has sent.
             */
            $itemID = $this->session->userdata('userid');
        } elseif ($permArray['invoiceViewOwn'] != 1 && $permArray['invoiceViewAll'] != 1) {
            $getPayments = false;
		}
        
        if ($getPayments == true) {
            $ledgerIncomeArray = $this->Finance_view->getIncomeForLedger($dateStart,$dateEnd,$itemID,$tag,$searchTerm,$group,$this->session->userdata('accountUserid'));
        }
        
        if ($permArray['expenseViewOwn'] == 1 && $permArray['expenseViewAll'] != 1) {
            /*
             * We're only getting expenses that this user has entered.
             */
            $itemID = $this->session->userdata('userid');
        } elseif ($group == 'user') {
            /*
             * Commented out because it was screwing up the items
             * we can view based on permissions.
             */
			//$itemID = $this->session->userdata('accountUserid');
		}
        $ledgerExpenseArray = $this->Finance_view_expenses->getExpensesForLedger($dateStart,$dateEnd,$itemID,$group,$tag,$searchTerm,$this->session->userdata('accountUserid'));
        
        /*
         * Put everything into one array
         */
        $incomeTotal = 0;
        $expenseTotal = 0;
        $ledgerArray = array();
        foreach($ledgerIncomeArray as $income) {
            $dateRender = date('M j, Y', strtotime($income['DatePayment']));
            $dateSort   = $income['DatePayment'];
            
            $ledgerArray[$a]['ItemID']       = $income['PaymentID'];
            $ledgerArray[$a]['MetaItemID']   = $income['InvoiceID'];
            $ledgerArray[$a]['ItemType']     = 'Income';
            $ledgerArray[$a]['ProjectID']    = $income['ProjectID'];
            $ledgerArray[$a]['ClientID']     = $income['ClientID'];
            $ledgerArray[$a]['VendorID']     = '';
            $ledgerArray[$a]['Vendor']       = '';
            $ledgerArray[$a]['Title']        = $income['InvNo'].' '.$income['Title'];
            $ledgerArray[$a]['ProjectTitle'] = $income['Project'];
            $ledgerArray[$a]['Company']      = $income['Company'];
            $ledgerArray[$a]['DateEntry']    = date('M j, Y', strtotime($income['DateEntry']));
            $ledgerArray[$a]['Total']        = $income['Total'];
            $ledgerArray[$a]['IsPaid']       = $income['Paid'];
            $ledgerArray[$a]['DateRender']   = $dateRender;
            $ledgerArray[$a]['Recurring']    = formatRecurring($income['Recurring']);
            $ledgerArray[$a]['DateSort']     = strtotime($dateSort);            
            
            $statusArray = getInvoiceStatus($income['Status'],$income['Paid'],$income['Sent'],$income['DateInvoice'],$income['DatePaid'],$income['DateSent'],$income['PaymentDueDate'],$income['PaymentDueDays']);
            $ledgerArray[$a]['StatusHuman']   = $statusArray[0];
            $ledgerArray[$a]['StatusMachine'] = $statusArray[1];
            $ledgerArray[$a]['Comments']      = $income['InvComments'];
            $ledgerArray[$a]['Tags']          = $income['Tags'];
            
            $incomeTotal = $incomeTotal + $income['Total'];
            $a++;
        }
        
        foreach($ledgerExpenseArray as $expense) {
            $vendor = $expense['VendorCompany'];
            if (!empty($expense['VendorNameFirst']) || !empty($expense['VendorNameLast'])) {
                $vendor = $expense['VendorNameFirst'].' '.$expense['VendorNameLast'].', '.$expense['VendorCompany'];
            }
            
            $ledgerArray[$a]['ItemID']       = $expense['ExpenseID'];
            $ledgerArray[$a]['MetaItemID']   = $expense['ExpenseID'];
            $ledgerArray[$a]['ItemType']     = 'Expense';
            $ledgerArray[$a]['ProjectID']    = $expense['ProjectID'];
            $ledgerArray[$a]['ClientID']     = $expense['ClientID'];
            $ledgerArray[$a]['VendorID']     = $expense['Vendor'];
            $ledgerArray[$a]['Vendor']       = $expense['VendorCompany'];
            $ledgerArray[$a]['Title']        = $expense['Title'];
            $ledgerArray[$a]['ProjectTitle'] = $expense['Project'];
            $ledgerArray[$a]['Company']      = $expense['Company'];
            $ledgerArray[$a]['DateEntry']    = date('M j, Y', strtotime($expense['DateEntry']));
            $ledgerArray[$a]['Total']        = $expense['Amount'];
            $ledgerArray[$a]['IsPaid']          = '';
            $ledgerArray[$a]['DateRender']   = date('M j, Y', strtotime($expense['DateExpense']));
            $ledgerArray[$a]['Recurring']    = formatRecurring($expense['Recurring']);
            $ledgerArray[$a]['DateSort']     = strtotime($expense['DateExpense']);
            $ledgerArray[$a]['StatusHuman']  = '';
            $ledgerArray[$a]['StatusMachine']= '';
            $ledgerArray[$a]['Comments']     = $expense['Comments'];
            $ledgerArray[$a]['Tags']         = $expense['Tags'];
            
            $expenseTotal = $expenseTotal + $expense['Amount'];
            $a++;
        }
        
        $incomeTotal = number_format($incomeTotal,2);
        $expenseTotal = number_format($expenseTotal,2);
        
        function date_compare($a,$b) {
            $t1 = $a['DateSort'];
            $t2 = $b['DateSort'];
            return $t2 - $t1;
        }
        usort($ledgerArray, 'date_compare');
        
        $dataArray = array(
            'Ledger' => $ledgerArray,
            'TotalIncome'  => $incomeTotal,
            'TotalExpense' => $expenseTotal
        );
        if ($renderType == 'json') {
            $jsonString = json_encode($dataArray);
            if ($renturn == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        } else {
            return $dataArray;
        }
        
    }

	function getInvoiceDetails($invoiceID,$renderType=NULL,$return=1,$language=NULL) {
		$invoiceDetails = $this->commonfinance->getInvoiceDetails($invoiceID,$renderType,$return,$language);

		if ($return == 1) {
			return $invoiceDetails;
		} else {
			echo $invoiceDetails;
		}
	}

    function getInvoicePayments($invoiceID,$renderType=NULL,$return=1) {
        $paymentDetails = $this->commonfinance->getInvoicePayments($invoiceID,$renderType);

        if ($renderType == 'json') {
            if ($return == 1) {
                return $paymentDetails;
            } else {
                echo $paymentDetails;
            }
        } else {
            return $paymentDetails;
        }
    }

    function getInvoiceListForSelect($clientID=NULL,$projectID=NULL,$status=NULL,$renderType='json',$return=0) {
        $invoiceArray = $this->Finance_view->getInvoiceListForSelect($clientID,$projectID,$status,$this->session->userdata('accountUserid'));
        if ($return == 1) {
            if ($renderType == 'json') {
                return json_encode($invoiceArray);
            } else {
                return $invoiceArray;
            }
        } else {
            if ($renderType == 'json') {
                echo decode_utf8(json_encode($invoiceArray));
            }
        }
    }

    function convertEstimateToInvoice($invoiceID) {
        $this->Invoice_update->convertEstimateToInvoice($invoiceID);
        $statusArray = $this->Finance_view->getInvoiceStatus($invoiceID);
        echo json_encode($statusArray);
    }
    
	function searchInvoices() {
		$q = cleanStringTag(strtolower($this->input->get('term', TRUE)));
        if (!$q) return;
		$invoiceArray = $this->Finance_view->getInvoicesForAutoCompleter($q,$this->session->userdata('accountUserid'));

		echo $invoiceArray;
	}
    
    function searchLedger() {
		$q = cleanStringTag(strtolower($this->input->get('term', TRUE)));
		if (!$q) return;
		$ledgerArray = $this->Finance_view->getLedgerForAutoCompleter($q,$this->session->userdata('accountUserid'));
		echo $ledgerArray;
	}

    function sendInvoiceEmail($emailArray=NULL) {
        if (array_empty($emailArray)) {
            $emailArray = $_POST;
        }
        
        $emailInvoiceDetails = $this->commonfinance->sendInvoiceEmail($emailArray);

        /*
         * Update invoice status
         */
        $this->Invoice_update->updateInvoiceSentStatus($emailArray['invoiceID'],1);
    }

    function stopRecurringInvoice($invoiceID) {
        $this->Invoice_update->stopRecurringInvoice($invoiceID);
    }
}	
?>