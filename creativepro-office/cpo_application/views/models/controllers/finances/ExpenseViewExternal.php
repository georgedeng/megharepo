<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ExpenseViewExternal extends Controller {
	function __construct()
	{
		parent::Controller();

		$this->load->model('common/app_data','AppData',TRUE);
        $this->load->model('clients/Client_view','',TRUE);
		$this->load->model('accounts/Accounts','',TRUE);
		$this->load->model('finances/Finance_view_expenses','',TRUE);

        $this->load->library('application/CommonFinance');
	}

    function logRecurringExpenses() {
        $expenseArray = $this->Finance_view_expenses->getRecurringExpenses();

        foreach ($expenseArray as $expense) {
            $expenseArrayNew = array();
            $daysFromSignup = days_between_dates(strtotime($expense['DateSignup']),time());
            echo $daysFromSignup.' ';
            $dayText  = date('D');
            $dayNum   = date('j');
            $monthNum = date('m');
            $monthDay = $monthNum.'-'.$dayNum;

            if ($daysFromSignup <= 31 || $expense['IsPaid'] == 1) {
                /*
                 * Figure out what kind of recurring expense we got here.
                 */
                $recurringArray = explode('|',$expense['Recurring']);
                $recurringDateArray = explode(',',$recurringArray[1]);
                if ($recurringArray[0] == 'week') {
                    foreach ($recurringDateArray as $recurringDate) {
                        if (!empty($recurringDate) && $recurringDate == $dayText) {
                            $newExpenseID = $this->commonfinance->copyExpense($expense['ExpenseID']);
                        }
                    }
                } else if ($recurringArray[0] == 'month') {
                    foreach ($recurringDateArray as $recurringDate) {
                        if (!empty($recurringDate) && $recurringDate == $dayNum) {
                            $newExpenseID = $this->commonfinance->copyExpense($expense['ExpenseID']);
                        }
                    }
                } else if ($recurringArray[0] == 'year') {
                    foreach ($recurringDateArray as $recurringDate) {
                        if (!empty($recurringDate) && $recurringDate == $monthDay) {
                            $newExpenseID = $this->commonfinance->copyExpense($expense['ExpenseID']);
                        }
                    }
                }
                echo 'Expense ID: '.$newExpenseID;
            }
        }
    }
}
?>