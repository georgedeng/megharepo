<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="settingsOwner" class="helptag" data-help="settings">
	<div id="tabsSettings" class="ui-tabs">
		<ul class="ui-tabs-nav">
	        <li class="settingsViewAll <? if ($permissions['settingsViewAll'] == 1) { echo 'ui-tabs-selected'; } ?> hide" id="tabSettingsCompany"><a href="#windowSettingsCompany"><span><?=lang('common_company_info'); ?></span></a></li>
	        <li class="settingsViewAll hide"><a href="#windowSettingsLogos" id="tabSettingsLogos"><span><?=lang('settings_logos'); ?></span></a></li>
	        <li class="settingsViewAll hide"><a href="#windowSettingsInvoice" id="tabSettingsInvoice"><span><?=lang('finance_invoice'); ?></span></a></li>
	        <!--<li><a href="#windowSettingsCalendar" id="tabSettingsCalendar"><span><?=lang('widget_calendar'); ?></span></a></li>-->
			<li class="<? if ($permissions['settingsViewOwn'] == 1 && $permissions['settingsViewAll'] != 1) { echo 'ui-tabs-selected'; } ?>"><a href="#windowSettingsEmail" id="tabSettingsEmail"><span><?=lang('common_email'); ?></span></a></li>
			<li><a href="#windowSettingsAccount" id="tabSettingsAccount"><span><?=lang('common_account'); ?></span></a></li>
            <!--<li><a href="#windowSettingsTheme" id="tabSettingsTheme"><span><?=lang('common_theme'); ?></span></a></li>-->
            <li class="settingsViewAll hide"><a href="#windowSettingsData" id="tabSettingsData"><span><?=lang('common_data'); ?></span></a></li>
        </ul>
	    <div style="min-height: 200px;" class="windowFrame">
	    	<div id="windowSettingsCompany">
	    		<div style="padding: 6px;" id="companyInformationContainer">
                    <h3 class="icon_vcard" style="margin: 6px; float: left;"><?=lang('settings_update_company_information'); ?></h3>
					<? if ($success == '1') { ?>
					<div style="float: right;" class="successMessageBig"><?=$statusMessage; ?></div>
					<? } elseif ($success == '0') { ?>
                    <div style="float: right;" class="errorMessageBig"><?=$statusMessage; ?></div>
                    <? } ?>
					<div style="clear: both;"></div>
                    <form class="greyForm" id="formCompanyInformation" name="formCompanyInformation" action="<?=BASE_URL; ?>settings/SettingsOwners/saveCompanyInformation" method="POST">
                    <input type="hidden" name="save" id="save" value="1" />
                    <input type="hidden" name="accountID" id="accountID" value="<?=$accountInformation['AccountID']; ?>" />
                        <p>
                            <label class="required"><?=lang('common_company_name'); ?></label>
                            <input tabindex="1" type="text" style="width: 400px;" name="accountCompanyName" id="accountCompanyName" value="<?=$accountInformation['Company']; ?><?=set_value('accountCompanyName'); ?>" />
                            <?=form_error('accountCompanyName','<span class="formError">','</span>'); ?>
                        </p>
                        <p>
                            <label><?=lang('common_address_1'); ?></label>
                            <input tabindex="2" type="text" style="width: 400px;" name="accountAddress1" id="accountAddress1" value="<?=$accountInformation['Address1']; ?><?=set_value('accountAddress1'); ?>" />
                        </p>
                        <p>
                            <label><?=lang('common_address_2'); ?></label>
                            <input tabindex="3" type="text" style="width: 400px;" name="accountAddress2" id="accountAddress2" value="<?=$accountInformation['Address2']; ?><?=set_value('accountAddress2'); ?>" />
                        </p>
                        <p>
                            <label><?=lang('client_form_city'); ?></label>
                            <input tabindex="4" type="text" style="width: 400px;" name="accountCity" id="accountCity" value="<?=$accountInformation['City']; ?><?=set_value('accountCity'); ?>" />
                        </p>
                        <p>
                            <label><?=lang('client_form_state'); ?></label>
                            <input type="text" name="accountState" id="accountState" value="<?=$accountInformation['State']; ?><?=set_value('accountState'); ?>" tabindex="5" style="width: 175px; margin-right: 10px;" />
                            <span class="inlineLabel"><?=lang('client_form_zip'); ?></span>
                            <input type="text" value="<?=$accountInformation['Zip']; ?><?=set_value('accountZip'); ?>" name="accountZip" id="accountZip" tabindex="6" style="width: 87px;" />
                        </p>
                        <p>
                            <label><?=lang('client_form_country'); ?></label>
                            <select name="accountCountry" id="accountCountry" selected="<?=set_value('accountCountry'); ?>" tabindex="7" style="width: 183px; margin-right: 10px;">
                            <option><?=lang('form_field_select'); ?></option>
                            <? $this->load->view('includes/country.html'); ?>
                            </select>
                            <input type="hidden" id="countryHolder" value="<?=$accountInformation['Country']; ?>" />

                            <span class="inlineLabel"><?=lang('common_currency'); ?></span>
                            <select name="accountCurrency" id="accountCurrency" tabindex="8" class="accountCurrency" style="width: 137px;">
                                <?
                                $currencies = currencies();
                                foreach($currencies as $key => $value) {
                                    echo '<option value="'.$key.'"';
                                    if ($key == $accountInformation['Currency']) {
                                        echo ' selected="selected" ';
                                    }
                                    echo '>'.$value[0].'</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label><?=lang('client_form_phone1'); ?></label>
                            <input tabindex="9" type="text" name="accountPhoneOffice" id="accountPhoneOffice" value="<?=$accountInformation['PhoneOffice']; ?><?=set_value('accountPhoneOffice'); ?>" style="width: 400px;" />
                        </p>
                        <p>
                            <label><?=lang('common_mobile'); ?></label>
                            <input tabindex="10" type="text" name="accountMobile" id="accountMobile" value="<?=$accountInformation['PhoneCell']; ?><?=set_value('accountMobile'); ?>" style="width: 400px;" />
                        </p>
                        <p>
                            <label><?=lang('common_fax'); ?></label>
                            <input tabindex="11" type="text" name="accountFax" id="accountFax" value="<?=$accountInformation['PhoneFax']; ?><?=set_value('accountFax'); ?>" style="width: 400px;" />
                        </p>
                        <p>
                            <label><?=lang('settings_web_address'); ?></label>
                            <input tabindex="12" type="text" name="accountWebAddress" id="accountWebAddress" value="<?=$accountInformation['SubDomain']; ?><?=set_value('accountWebAddress'); ?>" style="width: 400px;" />
                        </p>
                        <p>
                            <label class="required"><?=lang('client_form_email'); ?></label>
                            <input tabindex="13" type="text" name="accountEmail" id="accountEmail" value="<?=$accountInformation['Email']; ?><?=set_value('accountEmail'); ?>" style="width: 400px;" />
                            <?=form_error('accountEmail','<span class="formError">','</span>'); ?>
                        </p>
                        <p>
                            <label><?=lang('client_form_url'); ?></label>
                            <input tabindex="14" type="text" name="accountURL" id="accountURL" value="<?=$accountInformation['URL']; ?><?=set_value('accountURL'); ?>" style="width: 400px;" />
                        </p>
                        <p>
                            <label><?=lang('client_form_industry'); ?></label>
                            <select name="accountIndustry" id="accountIndustry" tabindex="15" style="width: 408px;">
                                <?
                                $industries = industries();
                                foreach($industries as $key => $value) {
                                    echo '<option value="'.$key.'"';
                                    if ($key == $accountInformation['Company_Industry']) {
                                        echo ' selected="selected" ';
                                    }
                                    echo '>'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </p>

                        <div class="buttonContainer">
                            <button class="buttonExpand action primary" id="buttonSave" tabindex="20"><span class="save"><?=lang('button_save'); ?></span></button>
                            <div style="clear: left;"></div>
                        </div>
                    </form>
                </div>
			</div>
			<div id="windowSettingsLogos" class="ui-tabs-hide">
	    		<div style="padding: 6px;" id="">
                    <h3 class="icon_target" style="float: left; margin: 6px;"><?=lang('settings_upload_logo'); ?></h3>
                    <div style="float: left;" id="logoErrorMessage"></div>
                    <div style="clear: left;"></div>

                    <div style="width: 45%; float: left; margin-right: 12px;">
                        <div style="min-height: 110px;"><?=lang('help_logo_lo_res'); ?>
                        <ul class="plain" style="margin-top: 6px;">
                            <?=lang('help_logo_lo_res_instructions'); ?>
                        </ul>
                        </div>
                        
                        <button class="buttonExpand action mTop12 left" id="buttonUploadLogoLoRes"><span class="upload"><?=lang('files_upload_logo_small'); ?></span></button>
                        <div id="containerLogoLowRes" style="clear: both; margin: 12px 0 0 10px;">
                            <? if (!empty($logoThumbFileLoRes)) {
                                echo '<img src="'.$logoThumbFileLoRes.'" />';
                            } ?>
                        </div>
                    </div>
                    <div style="width: 45%; float: left;">
                        <div style="min-height: 110px;"><?=lang('help_logo_hi_res'); ?>
                        <ul class="plain" style="margin-top: 6px;">
                            <?=lang('help_logo_hi_res_instructions'); ?>
                        </ul>
                        </div>
                        <button class="buttonExpand action mTop12 left" id="buttonUploadLogoHiRes"><span class="upload"><?=lang('files_upload_logo_hires'); ?></span></button>
                        
                        <div id="containerLogoHiRes" style="clear: both; margin: 12px 0 0 10px;">
                            <? if (!empty($logoThumbFileHiRes)) {
                                echo '<img src="'.$logoThumbFileHiRes.'" />';
                            } ?>
                        </div>
                    </div>
                    <div style="clear: left;"></div>
                 </div>                 
			</div>
			<div id="windowSettingsInvoice" class="ui-tabs-hide">
	    		<div style="padding: 6px;" id="">
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_invoices_small" style="margin: 6px;"><?=lang('settings_update_invoice'); ?></h3>
                    </div>
                    <div class="hide">
                        <form class="noBorder" id="invoiceForm">
                            <p>
                                <label><?=lang('finance_invoice_message_form'); ?></label>
                                <textarea tabindex="100" class="settingInvoice" style="width: 400px; height: 50px;" name="invoiceMessage" id="invoiceMessage"><? if (isset($invoiceArray['invoiceMessage'])) { echo $invoiceArray['invoiceMessage']; } ?></textarea>
                            </p>
                            <p>
                                <label><?=lang('finance_tax_rate'); ?> (%)</label>
                                <input type="text" tabindex="101" class="settingInvoice" style="width: 150px;" name="invoiceTaxRate" id="invoiceTaxRate" value="<? if (isset($invoiceArray['invoiceTaxRate'])) { echo $invoiceArray['invoiceTaxRate']; } ?>" />
                            </p>
                            <p>
                                <label><?=lang('finance_tax_name'); ?></label>
                                <input type="text" tabindex="102" class="settingInvoice" style="width: 150px;" name="invoiceTaxName" id="invoiceTaxName" value="<? if (isset($invoiceArray['invoiceTaxName'])) { echo $invoiceArray['invoiceTaxName']; } ?>" />
                            </p>
                            <p>
                                <label><?=lang('finance_tax_id'); ?></label>
                                <input type="text" tabindex="103" class="settingInvoice" style="width: 150px;" name="invoiceTaxID" id="invoiceTaxID" value="<? if (isset($invoiceArray['invoiceTaxID'])) { echo $invoiceArray['invoiceTaxID']; } ?>" />
                            </p>
                            <p>
                                <label><?=lang('finance_invoice_payment_due'); ?></label>
                                <select name="invoicePaymentDue" class="settingInvoice" id="invoicePaymentDue" tabindex="104" style="width: 150px;">
                                    <option value="0"
                                    <? if (isset($invoiceArray['invoicePaymentDue']) && $invoiceArray['invoicePaymentDue'] == '0') { echo ' selected '; } ?>
                                    ><?=lang('common_immediately'); ?></option>
                                    <option value="15"
                                    <? if (isset($invoiceArray['invoicePaymentDue']) && $invoiceArray['invoicePaymentDue'] == '15') { echo ' selected '; } ?>
                                    >15 <?=lang('date_days'); ?></option>
                                    <option value="30"
                                    <? if (isset($invoiceArray['invoicePaymentDue']) && $invoiceArray['invoicePaymentDue'] == '30') { echo ' selected '; } ?>
                                    >30 <?=lang('date_days'); ?></option>
                                    <option value="45"
                                    <? if (isset($invoiceArray['invoicePaymentDue']) && $invoiceArray['invoicePaymentDue'] == '45') { echo ' selected '; } ?>
                                    >45 <?=lang('date_days'); ?></option>
                                    <option value="60"
                                    <? if (isset($invoiceArray['invoicePaymentDue']) && $invoiceArray['invoicePaymentDue'] == '60') { echo ' selected '; } ?>
                                    >60 <?=lang('date_days'); ?></option>
                                    <option value="90"
                                    <? if (isset($invoiceArray['invoicePaymentDue']) && $invoiceArray['invoicePaymentDue'] == '90') { echo ' selected '; } ?>
                                    >90 <?=lang('date_days'); ?></option>
                                </select>
                            </p>
                            <p>
                                <label><?=lang('finance_invoice_late_fee'); ?></label>
                                <select name="invoiceLateFee" id="invoiceLateFee" class="settingInvoice" tabindex="105" style="width: 150px;">
                                    <option value="0"></option>
                                    <option value="1"
                                    <? if (isset($invoiceArray['invoiceLateFee']) && $invoiceArray['invoiceLateFee'] == '1') { echo ' selected '; } ?>
                                    >1% <?=lang('common_per_month'); ?></option>
                                    <option value="1.5"
                                    <? if (isset($invoiceArray['invoiceLateFee']) && $invoiceArray['invoiceLateFee'] == '1.5') { echo ' selected '; } ?>
                                    >1.5% <?=lang('common_per_month'); ?></option>
                                    <option value="2"
                                    <? if (isset($invoiceArray['invoiceLateFee']) && $invoiceArray['invoiceLateFee'] == '2') { echo ' selected '; } ?>
                                    >2% <?=lang('common_per_month'); ?></option>
                                    <option value="amount"
                                    <? if (isset($invoiceArray['invoiceLateFeeAmount']) && $invoiceArray['invoiceLateFeeAmount'] > 0) { echo ' selected '; } ?>
                                    ><?=lang('common_specific_amount'); ?></option>
                                </select>
                            </p>
                            <div class="buttonContainer" style="margin-bottom: 12px;">
                                <button class="buttonExpand blueGray primary action" id="buttonSaveInvoiceSettings" tabindex="106"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageInvoiceSettings"></div>
                                <div style="clear: left;"></div>
                            </div>
                        </form>
                    </div>

                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_paypal_small" style="margin: 6px;"><?=lang('finance_invoice_paypal_title'); ?></h3>
                    </div>
                    <div class="hide">
                        <p style="margin-left: 30px;"><?=lang('help_paypal_payments'); ?></p>
                        <form class="noBorder" id="invoicePayPalForm">
                            <p>
                                <label for="emailPayPal"><?=lang('finance_invoice_paypal_email'); ?></label>
                                <input type="text" style="width: 250px;" id="emailPayPal" name="emailPayPal" value="<? if (isset($invoiceArray['emailPayPal'])) { echo $invoiceArray['emailPayPal']; } ?>" />
                            </p>
                            <div class="buttonContainer">
                                <button class="buttonExpand blueGray action" id="buttonSaveInvoiceSettingsPayPal"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageInvoiceSettingsPayPal"></div>
                                <div style="clear: left;"></div>
                            </div>
                        </form>    
                    </div>
                </div>
			</div>
			<div id="windowSettingsCalendar" class="ui-tabs-hide">
	    		<div style="padding: 6px;" id="">
                    <h3 class="icon_calendar_small" style="margin: 6px;"><?=lang('settings_update_calendar'); ?></h3>
                </div>
			</div>
			<div id="windowSettingsEmail" class="ui-tabs-hide">
	    		<div style="padding: 6px;" id="">
                    <h3 class="icon_email_receive" style="margin: 6px;"><?=lang('settings_email_notification'); ?></h3>
                    <form class="noBorder" id="emailForm">
                    <table class="dataTable" style="background: none;">
                        <tr>
                            <td style="width: 50px;"><input type="checkbox" class="settingEmail" value="emailReceiveProject" name="emailReceiveProject" id="emailReceiveProject" <? if (isset($emailArray['emailReceiveProject']) && $emailArray['emailReceiveProject'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveProject"><?=lang('settings_email_project'); ?></label></td>
                        </tr>
                        <tr class="rowLines">
                            <td><input type="checkbox" class="settingEmail" value="emailReceiveProjectUpdate" name="chkEmailProjectUpdate" id="emailReceiveProjectUpdate" <? if (isset($emailArray['emailReceiveProjectUpdate']) && $emailArray['emailReceiveProjectUpdate'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveProjectUpdate"><?=lang('settings_email_project_updated'); ?></label></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" class="settingEmail" value="emailReceiveTask" name="emailReceiveTask" id="emailReceiveTask" <? if (isset($emailArray['emailReceiveTask']) && $emailArray['emailReceiveTask'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveTask"><?=lang('settings_email_task'); ?></label></td>
                        </tr>
                        <tr class="rowLines">
                            <td><input type="checkbox" class="settingEmail" value="emailReceiveTaskUpdate" name="emailReceiveTaskUpdate" id="emailReceiveTaskUpdate" <? if (isset($emailArray['emailReceiveTaskUpdate']) && $emailArray['emailReceiveTaskUpdate'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveTaskUpdate"><?=lang('settings_email_task_update'); ?></label></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" class="settingEmail" value="emailReceiveTaskOverdue" name="emailReceiveTaskOverdue" id="emailReceiveTaskOverdue" <? if (isset($emailArray['emailReceiveTaskOverdue']) && $emailArray['emailReceiveTaskOverdue'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveTaskOverdue"><?=lang('settings_email_task_overdue'); ?></label></td>
                        </tr>
                        <tr class="rowLines">
                            <td><input type="checkbox" class="settingEmail" value="emailReceiveMessage" name="emailReceiveMessage" id="emailReceiveMessage" <? if (isset($emailArray['emailReceiveMessage']) && $emailArray['emailReceiveMessage'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveMessage"><?=lang('settings_email_message'); ?></label></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" class="settingEmail" value="emailReceiveCalendar" name="emailReceiveCalendar" id="emailReceiveCalendar" <? if (isset($emailArray['emailReceiveCalendar']) && $emailArray['emailReceiveCalendar'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveCalendar"><?=lang('settings_email_calendar'); ?></label></td>
                        </tr>
                        <tr class="rowLines">
                            <td><input type="checkbox" class="settingEmail" value="emailReceiveEvent" name="emailReceiveEvent" id="emailReceiveEvent" <? if (isset($emailArray['emailReceiveEvent']) && $emailArray['emailReceiveEvent'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveEvent"><?=lang('settings_email_event'); ?></label></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" class="settingEmail" value="emailReceiveTimeClock" name="emailReceiveTimeClock" id="emailReceiveTimeClock" <? if (isset($emailArray['emailReceiveTimeClock']) && $emailArray['emailReceiveTimeClock'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailReceiveTimeClock"><?=lang('settings_email_timer'); ?></label></td>
                        </tr>
                    </table>

                    <h3 class="icon_email_send" style="margin: 6px;"><?=lang('settings_email_send_notification'); ?></h3>
                    <table class="dataTable" style="background: none;">
                        <tr>
                            <td style="width: 50px;"><input type="checkbox" class="settingEmail" value="emailSendClientNew" name="emailSendClientNew" id="emailSendClientNew" <? if (isset($emailArray['emailSendClientNew']) && $emailArray['emailSendClientNew'] == 1) { echo 'checked'; } ?> /></td>
                            <td><label class="normal" for="emailSendClientNew"><?=lang('settings_email_client_create'); ?></label></td>
                        </tr>
                    </table>

                    <div style="margin: 6px 0 0 55px;">
                        <button class="buttonExpand action" id="buttonSaveEmailSettings"><span class="save"><?=lang('button_save'); ?></span></button>
                        
						<div style="float: left; margin-left: 12px;" id="messageSettingsEmail"></div>
                        <div style="clear: left;"></div>
					</div>
                    </form>
                </div>
			</div>
			<div id="windowSettingsAccount" class="ui-tabs-hide">
	    		<div style="padding: 6px;" id="">
                    <div class="settingsViewAll hide boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_lock" style="margin: 6px;"><?=lang('settings_account_security'); ?></h3>
                    </div>
                    <div class="hide" style="margin-bottom: 12px;">
                        <form class="noBorder" id="accountFormSecurity">

                            <div class="warningDiv" style="width: 643px; margin: 12px 0 12px 0;"><?=lang('help_account_security_settings'); ?></div>
                            <table class="dataTable" style="background: none;">
                                <tr>
                                    <td style="width: 42px;"><input type="checkbox" class="settingAccount" value="accountLoginFromItemView" name="chkAccountLoginItemView" id="chkAccountLoginItemView" <? if (isset($accountArray['accountLoginFromItemView']) && $accountArray['accountLoginFromItemView'] == 1) { echo 'checked'; } ?> /></td>
                                    <td><label for="chkAccountLoginItemView" class="normal"><?=lang('settings_account_login_on_item_view'); ?></label></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="settingAccount" value="accountLoginFromEmail" name="chkAccountLoginEmail" id="chkAccountLoginEmail" <? if (isset($accountArray['accountLoginFromEmail']) && $accountArray['accountLoginFromEmail'] == 1) { echo 'checked'; } ?> /></td>
                                    <td><label for="chkAccountLoginEmail" class="normal"><?=lang('settings_account_login_from_email'); ?></label></td>
                                </tr>
                            </table>

                            <div style="margin: 6px 0 24px 47px;">
                                <button class="buttonExpand action" id="buttonSaveAccountSecurity"><span class="save"><?=lang('button_save'); ?></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageSettingsAccountSecurity"></div>
                                <div style="clear: left;"></div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
            <div id="windowSettingsTheme" class="ui-tabs-hide">
                <div style="padding: 6px;" id="">
                </div>
            </div>
            <div id="windowSettingsData" class="ui-tabs-hide helptag" data-help="import">
                <div style="padding: 6px;" id="">
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_export2" style="margin: 6px;"><?=lang('export_cpo_data'); ?></h3>
                    </div>
                    <div class="hide padBottom12">
                        <p class="mBottom18"><?=lang('help_cpo_export_overview'); ?></p>
                        <button class="buttonExpand action left" id="buttonDataExport"><span class="export"><?=lang('common_data_export'); ?></span></button>
                        <div class="right" id="messageSettingsDataExport"></div>
                        <div style="clear: both;"></div>
                    </div>

                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_database_import" style="margin: 6px;"><?=lang('import_from_other'); ?></h3>
                    </div>
                    <div class="hide">
                        <p class="padBottom12"><?=lang('help_import_overview'); ?></p>
                        <button class="buttonExpand action left" id="buttonDataImportOther"><span class="import"><?=lang('import_begin_import'); ?></span></button>
                    </div>
                    
                    <!--
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_database_import" style="margin: 6px;"><?=lang('import_from_cpo'); ?></h3>
                    </div>
                    <div class="hide">
                        <form class="noBorder" id="accountFormDataImport">
                            <p style="margin-left: 40px;"><?=lang('help_account_import_v10'); ?></p>
                            <p class="padBottom12">
                                <label><?=lang('login_user_name'); ?></label>
                                <input type="text" style="width: 150px;" id="version10UserName" name="version10UserName" />
                            </p>
                            <p>
                                <label><?=lang('login_password'); ?></label>
                                <input type="password" style="width: 150px;" id="version10Password" name="version10Password" />
                            </p>
                            <div class="buttonContainer">
                                <button class="buttonExpand blueGray" style="float: left;" id="buttonDataImport"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator import"><?=lang('common_data_import'); ?></span></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageDataImport"></div>
                                <div style="clear: left;"></div>
                            </div>
                            <div style="margin: 6px 0 6px 266px;" id="importSummary"></div>

                        </form>
                    </div>
                    -->
                    <!--
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_database_import" style="margin: 6px;"><?=lang('import_from_cpo_self'); ?></h3>
                    </div>
                    <div class="hide padBottom12">
                        <p class="padBottom12"><?=lang('help_account_import_v10_self'); ?></p>
                        <form class="noBorder" id="accountFormDataImportSelfHosted">
                            <div class="padBottom12" style="margin-left: 50px; float: left;">
                                <label class="labelVert"><?=lang('settings_dbase_host'); ?></label>
                                <input type="text" style="width: 150px;" id="version10DbaseHost" name="version10DbaseHost" />
                            </div>
                            <div class="padBottom12" style="float: left;">
                                <label class="labelVert"><?=lang('settings_dbase_name'); ?></label>
                                <input type="text" style="width: 150px;" id="version10DbaseName" name="version10DbaseName" />
                            </div>
                            <?=help('help_import_database_fields','installKey','iconHelpFormOutside'); ?>

                            <div style="clear: left;"></div>
                            <div class="padBottom12" style="margin-left: 50px; float: left;">
                                <label class="labelVert"><?=lang('settings_dbase_username'); ?></label>
                                <input type="text" style="width: 150px;" id="version10DbaseUsername" name="version10DbaseUsername" />
                            </div>
                            <div class="padBottom12" style="float: left;">
                                <label class="labelVert"><?=lang('settings_dbase_password'); ?></label>
                                <input type="text" style="width: 150px;" id="version10DbasePassword" name="version10DbasePassword" />
                            </div>
                            <div style="clear: left;"></div>
                            <div style="margin-left: 50px;">
                                <label class="labelVert" style="width: 400px;"><?=lang('settings_file_path'); ?></label>
                                <input type="text" style="width: 415px;" id="filePath" name="filePath" />
                                <?=help('help_import_file_path','filePath','iconHelpFormOutside'); ?>
                                <br style="clear: both;" />
                                <div class="mTop12"><input type="checkbox" value="1" id="noFileImport" /> <?=lang('settings_no_file_import'); ?></div>
                            </div>

                            <div style="margin: 12px 0 0 48px;">
                                <button class="buttonExpand blueGray" style="float: left;" id="buttonDataImportSelfHosted"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator import"><?=lang('common_data_import'); ?></span></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageDataImportSelfHosted"></div>
                                <div style="clear: left;"></div>
                            </div>
                            <div style="margin: 6px 0 6px 266px;" id="importSummarySelfHosted"></div>
                        </form>
                    </div>
                    -->                    

                    <!--<h3 class="icon_database_export padBottom12" style="margin-top: 6px; margin-left: 6px;"><?=lang('common_data_export'); ?></h3>-->

                </div>
            </div>
		</div>
	</div>
    <div id="dialogCancelAccount" class="noPrint" style="background: #fff;" title="<?=lang('settings_cancel_account'); ?>">
        <div style="padding: 6px;" id="deleteContentsContainer">
            <div class="warningDiv" style="width: 418px; margin: 12px 0 12px 0;"><?=lang('help_account_cancel'); ?></div>
        </div>
        <button id="buttonCancelAccount" class="smallButton" style="margin: 0 6px 0 6px; float: left;"><span class="delete"><?=lang('settings_cancel_account'); ?></span></button>
        <button id="buttonCancel" class="smallButton" style="margin-right: 6px; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button
        <div style="clear: left;"></div>
    </div>   
</div>
<? $this->load->view('includes/footer'); ?>
