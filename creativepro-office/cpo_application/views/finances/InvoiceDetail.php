<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="viewInvoiceDetails">
	<input type="hidden" id="invoiceID" value="<?=$invoiceID; ?>" />
	<script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var invoiceData = '<?=$invoiceInformation; ?>';
	/* ]]> */
	</script>
	<div id="tabsInvoice">
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-selected" id="tabInvoiceDetails"><a href="#windowProjectDetails"><span><?=lang('project_details'); ?></span></a></li>
	        <li><a href="#windowMessages" id="tabInvoiceMessages"><span><?=lang('common_messages'); ?></span> <span id="messageCount" class="tabCount lightGray">(0)</span></a></li>
	    </ul>
	    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">
	    	<div id="windowInvoiceDetails">
	    		<div style="padding: 6px;" id="invoiceInformationContainer"></div>
			</div>
            <div id="windowMessages" class="ui-tabs-hide">
				<div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessage" itemID="<?=$invoiceID; ?>" itemType="invoice" class="messageBoxContainerLink"></div>
					</div>
				</div>
				<div id="messageListProject" style="margin-top: 6px;"></div>
	   		</div> 
		</div>
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
