<? $this->load->view('includes/headerSecureSite'); ?>
<div class="leftPane">
    <div style="width: 615px; float: left;">
        <input type="hidden" id="accountUserid" value="<?=$accountUserid; ?>" />
        <h1 class="mBottom" id="pageTitle">Confirm your plan change</h1>

        <p class="mBottom" id="introTextConfirm">Please make sure the plan selected below is the plan you want to keep, then click the
        green button at the bottom.

        <? if ($plan != 'free' && !empty($dateTrialExpires)) { ?>
        If you have selected a paid plan, you will be asked to enter your payment information when your free
        trial ends on <strong><?=$dateTrialExpires; ?></strong>.
        </p>
        <? } ?>

        <p class="mBottom" id="introTextConfirmed" style="display: none;">

        <? if ($plan != 'free') { ?>
        If you have selected a paid plan, you will be asked to enter your payment information when your free
        trial ends on <strong><?=$dateTrialExpires; ?></strong>.
        <? } ?>
        </p>

        <? $this->load->view('includes/planDetailBox'); ?>

        <? if ($daysLeftInTrial>0) { ?>
        <a href="#" plan="<?=$plan; ?>" id="buttonChangeAccountConfirm" class="greenButton" style="margin-right: 18px;"><span>Ok, change my plan.</span></a>
        <? } else { ?>
        <a href="<?=SECURE_URL; ?>payment/paymentInfo/<?=$accountUserid; ?>/<?=$plan; ?>" class="greenButton" style="margin-right: 18px;"><span>Ok, change my plan.</span></a>
        <? } ?>
        <a href="<?=INSECURE_URL; ?>dashboard/Dashboard/changeAccount/<?=$accountUserid; ?>" id="buttonGoToAccount" class="greenButton" style="display: none; margin-right: 18px;"><span>Go to your dashboard.</span></a>

        <a href="<?=INSECURE_URL; ?>dashboard/Dashboard" id="buttonCancel" class="darkOrangeButton"><span>Nope! Forget it.</span></a>
    </div>
</div>
<div class="rightPane"></div>
<? $this->load->view('includes/footerSecureSite'); ?>
