<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
<?
$monthYear = '';
foreach($itemArray as $item) {
    $newMonthYear = date('F Y',strtotime($item['BugDateClose']));

    if ($newMonthYear != $monthYear) {
        if (!empty($monthYear)) {
            echo '</ul>';
        }

        echo '<h2 class="mBottom12">'.$newMonthYear.'</h2>';
        echo '<ul class="mBottom12">';
        $monthYear = $newMonthYear;
    }

    $itemDate = date('M j',strtotime($item['BugDateClose']));
    $itemType = strtoupper($item['ItemType']);
    if (!empty($item['BugTextReworded'])) {
        echo '<li><strong>'.$itemDate.' '.$itemType.'</strong> '.$item['BugTextReworded'].'</li>';
    }
}
?>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>
