<? $this->load->view('includes/headerLight'); ?>
<div pageID="accountLogin">
    <div class="cpoLogo"></div>
	<div style="padding-top: 6px;">
        <? if (!empty($message)) { ?>
		<div id="messageContainer" style="width: 486px; margin-left: 15px;" class="boxYellow">
			<?=$message; ?>
		</div>
        <? } ?>
        <div>
			<form id="formLogin" class="noBorder" action="<?=site_url('login/tryLogin'); ?>" method="POST">
                <input type="hidden" name="gadget" value="<?=$gadgetType; ?>" />
                <h2>Email / User Name</h2>
                <input class="bigText" type="text" style="width: 90%;" id="userid" name="userid" />

                <h2 style="margin-top: 12px;">Password</h2>
                <input class="bigText" type="password" style="width: 90%;" id="password" name="password" />

                <p style="margin: 6px 0 6px 0;">
                    <input type="checkbox" id="remember" name="remember" value="1" /> Remember me
                    
                </p>
				<button class="buttonExpand action" id="buttonLogin" tabindex="9"><span class="buttonDecorator login">Login</span></button>
                <div style="clear: both;"></div>
			</form>

			<form id="formFindPassword" action="<?=site_url('login/findPassword'); ?>" method="POST" style="display: none;">
                <input type="hidden" name="subdomain" value="<?=$subdomain; ?>" />
                <div>
                    <h2>Email address</h2>
                    <input class="bigText" type="text" style="width: 100px;" id="email" name="email" />
                </div>
				<div style="margin-top: 12px;">
                    <button class="buttonExpand action" id="buttonFindPassword" style="margin-right: 24px;"><span class="buttonDecorator search">Find my password</span></button>
                    <a href="#" id="buttonCancelFindPassword" style="float: left;">Cancel</a>
				</div>
			</form>
		</div>
        <div style="margin-top: 20px;">
            <strong><a href="<?=INSECURE_URL.'pricing'; ?>" target="_blank">Create an account</a></strong>
        </div>
        <div style="clear: left;"></div>
	</div>
</div>
<? $this->load->view('includes/footerLight'); ?>