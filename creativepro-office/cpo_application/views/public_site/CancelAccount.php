<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="mBottom12">Cancel your account</h1>

    <p class="mBottom12"><strong>Your CreativePro Office account has been canceled and you will no longer be billed
    for our services.</strong></p>

    <p class="mBottom12">We are truly grateful that your company chose to work with CreativePro Office and we
    hope to see you again in the furture. Before you leave for good, would you mind
    <strong><a href="contact">letting us know</a></strong> why CreativePro Office didn't work out for you?</p>

    <p>Sincerely,<br />
    Jeff Denton and the CPO Team</p>
</div>
<div class="rightPane">
</div>
<? $this->load->view('includes/footerPublic'); ?>