<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="mBottom12">Self-hosted FAQ's</h1>

    <h3 class="mBottom12">What do I need to run CreativePro Office on my own server?</h3>
    <p>You will need a server environment with the following:
    <ul class="check" style="margin: 6px 0 0 36px;">
        <li>A Linux operating system (we currently do not support Windows installations).</li>
        <li>The Apache web server or something similar.</li>
        <li>PHP 5.2 or greater.</li>
        <li>MySQL 4 database or greater.</li>
        <li>At least 30MB of disk space.</li>
        <li>The ability to modify your .htaccess files.</li>
        <li>The ability to execute CRON jobs on your server if you wish to take advantage of things
            like recurring invoices, auto-task reminders, etc.</li>
        <li>The ability to set up a temporary FTP account should you require our assistance in
            getting your installation running.</li>
    </ul>
    </p>

    <h3 class="mTop12 mBottom12">How many users, projects, or files can I have?</h3>
    <p>Unlimited! The self-hosted option allows you to have as many users, clients, projects, and files
    as you need. You're only limited by your server storage capacity.</p>

    <h3 class="mTop12 mBottom12">Can I import my hosted account data into my self-hosted copy?</h3>
    <p class="errorMessageSmall" style="margin-top: 6px;">No, not yet!</p>
    <p>We are working on an import feature that will allow you to import data from a CreativePro Office
    hosted account into your self-hosted installation.  This should be available as an update in late June, 2011.</p>

    <h3 class="mTop12 mBottom12">What kind of support can I expect to get?</h3>
    <p>We offer 1 free hour of installation support, 1 year of priority email support, and
    1 year of free updates to the code. Additional years of support and updates can be purchased
    for $199.00 per year. We have found that this level of support is usually enough to get customers
    up and running and to take care of quirks and bugs down the road.</p>

    <h3 class="mTop12 mBottom12">Is the self-hosted version the same as the hosted version at MyCPOHq.com?</h3>
    <p class="mBottom12">For the most part, yes.  We release updates to the self-hosted version once per quarter while we update
        the hosted application weekly.  So your self-hosted version will be at most 3 months behind the hosted version
        regarding features.</p>
    <p>However, you can rest assured that your installation will be up to date by the time you install the first
        update.</p>

    <h3 class="mTop12 mBottom12">Can I modify the source code to add features or change things?</h3>
    <p>Yes you can! Your license gives you a complete copy of the source code. You can update,
    modify, and add feature however you wish. You can change colors or layout...you can alter email
    messages that get sent to your clients.</p>

    <p>CreativePro Office is build on the <a href="http://codeigniter.com/">Codeigniter PHP framework</a> and should be
    fairly simple for an experienced PHP developer to modify. Down the road, we hope to have some tutorials
    and other help for modifying the code.</p>

    <p class="alertMessageSmall" style="margin-top: 6px;">Modifying the source code may complicate or prevent future code updates
    from working correctly. Please be sure you know what you're doing and feel free to <a href="contact">ask for help</a>.</p>

    <h3 class="mTop12 mBottom12">What exactly can I do with my license?</h3>
    <p class="successMessageSmall" style="margin-top: 6px;">You can ...</p>
        <ul style="margin-left: 36px;">
            <li>Install your source code on 1 production server and 1 development or test server</li>
            <li>Have 1 company account on your installation</li>
            <li>Modify the source code any way you choose</li>
            <li>Sell updates, plugins, or apps that you create so long as they do not contain
            any original source code</li>
            <li>Allow your clients and team members to use your copy for free.</li>
        </ul>
    <p class="errorMessageSmall" style="margin-top: 6px;">You cannot ever, ever, ever ...</p>
        <ul class="mBottom12" style="margin-left: 36px;">
            <li>Sell or give away any portion of the original source code</li>
            <li>Host the application in such a way that multiple companies can sign up and
            use the application for free or for profit</li>
            <li>Sell access to your copy of the application to your clients or team members</li>
        </ul>

    <p><a href="<?=BASE_URL; ?>eula" target="_blank">Read the full self-hosted end user license agreement here.</a></p>

    <h3 class="mTop12 mBottom12">Can I get a refund if things don't work out?</h3>
    <p class="mBottom12"><strong>We currently do not offer refunds on our self-hosted licenses.</strong> You are
    welcome to sign up for a <a href="pricing">free 30 day trial</a> to see if CreativePro Office
    will be a good fit for your business.</p>

    <p>We will always do everything we can to help you install your copy and get it working
    on your server.</p>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>
