<? $this->load->view('includes/headerSC'); ?>

<div class="heading_area">

    <div class="container">

        <h1><span>Login</span> to CreativePro Office account here.</h1>

    </div>

</div>

<div class="login_area">

    <div class="container">

        <div class="left_area">

            <div class="login_info">

                <h4>CreativePro Office works best with</h4>

                <ul class="browser_compotablity">

                    <li><img src="<?php echo base_url(); ?>images/sc-image/Firefox.png" alt=""></li>

                    <li><img src="<?php echo base_url(); ?>images/sc-image/Enternet.png" alt=""></li>

                    <li><img src="<?php echo base_url(); ?>images/sc-image/Google.png" alt=""></li>

                    <li><img src="<?php echo base_url(); ?>images/sc-image/Safari.png" alt=""></li>

                    <li><img src="<?php echo base_url(); ?>images/sc-image/Opera.png" alt=""></li>

                </ul>

                <ul class="details">

                    <li>

                        <i class="comman_image"><img src="<?php echo base_url(); ?>images/sc-image/Icon-1.png"></i>

                        <p>Signup takes less than 1 minute</p>

                    </li>

                    <li>

                        <i class="comman_image"><img src="<?php echo base_url(); ?>images/sc-image/Icon-2.png"></i>

                        <p>Free 30 day trial on all packages - on credit card upfront</p>

                    </li>

                    <li>

                        <i class="comman_image"><img src="<?php echo base_url(); ?>images/sc-image/Icon-3.png"></i>

                        <p>Low priced packges loaded with features</p>

                    </li>

                </ul>

                <a class="create_new_account" href="<?php echo base_url();?>pricing">Create a new account &rsaquo;&rsaquo;</a>

            </div>

        </div>

        <div class="right_area">

            <div class="login_form">

                <?= $message; ?>

                <form name="loginForm" id="formLogin" class="noBorder" action="<?=site_url('login/tryLogin'); ?>" method="POST">

                    <div class="full_field">

                        <label>Email / Username</label>

                        <input type="text" id="userid" name="userid" required>

                    </div>

                    <div class="full_field">

                        <label>Password</label>

                        <input type="password" name="password" id="password" name="password" required>

                    </div>

                    <div class="another_full_area">

                        <p class="check_box"><input type="checkbox" id="remember" name="remember" value="1"><label for="remember">Remmeber me for 30 days</label></p>

                        <a class="forgot_password" id="linkFindPassword" href="#">I forgot my password</a>

                    </div>

                    <div class="submit_area">

                        <input type="submit" value="Login" name="">

                    </div>

                </form>

                

                <div class="forgot_form">

                    <div class="resetpass-info">
                    <p><strong>Reset your password.</strong> Please enter your account email address, we will reset your password and email it to you.</p>
                    <p><span style="color: red; font-weight: bold;">If you have multiple accounts under 1 email address</span> the passwords for all accounts will be changed. Each account password will be unique and instructions will be sent to you telling you how to access each account. Please <a href="<?php echo base_url();?>contact">Contact Us</a> with any questions.</p>
                    </div>

                    <form id="formFindPassword" action="<?=site_url('login/findPassword'); ?>" method="POST" style="display: block;">

                        <div class="full_field">

                            <label>Enter email here</label>

                            <input type="text" id="email" name="email" required>

                        </div>

                        <div class="another_full_area">

                            <a class="forgot_password" id="linkbacklogin" href="#">Back to login</a>

                        </div>

                        <div class="submit_area">

                            <input type="submit" value="Reset Password" name="">

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>





<div class="pricing_and_signup">

    <div class="container">

        <span>Free 30 days trial. Sign up now!</span><a href="<?php echo base_url();?>pricing">Pricing & Signup</a>

    </div>

</div>

<? $this->load->view('includes/footerSC'); ?>