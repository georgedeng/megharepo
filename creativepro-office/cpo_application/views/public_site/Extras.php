<? $this->load->view('includes/headerPublic'); ?>

<div>

    <div class="tourLeft" style="width: 550px; padding: 0 24px 0 24px;">

        <h2 class="mBottom12">iPhone App</h2>

        <p>Now you can use CreativePro Office on your iPhone or iPod Touch! Track your

        time and manage your tasks from wherever you are right on your mobile iOS device. 

        Our app currently supports time tracking, task management, and task creation. We will

        soon offer more features like expenses, messaging, and file viewing.</p>

        <p class="mTop24"><a href="http://itunes.apple.com/us/app/creativepro-office/id508303362?mt=8" class="appStoreBadge"></a></p>

    </div>

    <div class="tourRight" style="padding-right: 24px;">

            <img src="<?php echo base_url();?>images/tourImages/touriPhone.png" alt="" />

    </div>

    <div class="tourClear"></div>

    

    <div class="tourLeft" style="width: 550px; padding: 0 24px 0 24px;">

        <h2 class="mBottom12">Desktop Timer for Mac and Windows</h2>

        <p>You no longer need your browser to log timesheet entries. The CreativePro Office 

            desktop timer for Mac and Windows allows you to quickly enter time for your

            projects and tasks throughout the day without ever logging in to the web app. The desktop

            timer is FREE for all CreativePro Office customers.</p>

        <a href="<?php echo base_url();?>downloads/CreativeProOffice.dmg" class="iconApple" title="Download for Mac">Download for Mac</a>

        <a href="<?php echo base_url();?>downloads/CreativeProOffice.msi" class="iconWindows" title="Download for Windows">Download for Windows</a>

    </div>

    <div class="tourRight" style="padding-right: 24px;">

        <img src="<?php echo base_url();?>images/tourImages/tourDesktop.png" alt="" />

    </div>

    <div class="tourClear"></div>

    

    <div class="tourLeft" style="width: 550px; padding: 0 24px 0 24px;">

        <h2 class="mBottom12">Self-Hosted Version</h2>

        <p>Now you can use CreativePro Office in the security of your own intranet or 

            the hosting platform of your choice. Our self-hosted option gives you the 

            complete open source code, a perpetual license, 1 year of support and updates, 

            and complete peace of mind for one low price.</p>

    </div>

    <div class="tourRight" style="padding-right: 24px;">

        <a href="<?php echo base_url();?>selfhosted" title="" class="thumbs">

            <img src="<?php echo base_url();?>images/tourImages/tourSelfHosted.png" alt="" />

        </a>

    </div>

    <div class="tourClear"></div>

</div>

<? $this->load->view('includes/footerPublic'); ?>

