<? $this->load->view('includes/headerPublic'); ?>
<div>
    <div id="thumbnailContainerHolder">
        <div id="thumb_dashboard" class="thumbnailContainer rightPad">
            <img src="images/screenshots/thumb_Dashboard.png" title="Office Dashboard" border="0" />
            <div><h3>Office Dashboard</h3>
            </div>
        </div>
        <div id="thumb_project" class="thumbnailContainer rightPad">
            <img src="images/screenshots/thumb_Project.png" title="The Project View" border="0" />
            <div><h3>Project View</h3>
            </div>
        </div>
        <div id="thumb_client" class="thumbnailContainer">
            <img src="images/screenshots/thumb_Client.png" title="" border="0" />
            <div><h3>Client View</h3></div>
        </div>
        <div id="thumb_tasklist" class="thumbnailContainer rightPad">
            <img src="images/screenshots/thumb_TaskList.png" title="" border="0" />
            <div><h3>Task Lists</h3></div>
        </div>
        <div id="thumb_invoicelist" class="thumbnailContainer rightPad">
            <img src="images/screenshots/thumb_InvoiceList.png" title="" border="0" />
            <div><h3>Invoice List View</h3></div>
        </div>
        <div id="thumb_timesheet" class="thumbnailContainer">
            <img src="images/screenshots/thumb_Timesheet.png" title="" border="0" />
            <div><h3>Timesheets</h3></div>
        </div>
        <div id="thumb_reports" class="thumbnailContainer rightPad">
            <img src="images/screenshots/thumb_Reports.png" title="" border="0" />
            <div><h3>Reports</h3></div>
        </div>
        <div id="thumb_calendar" class="thumbnailContainer rightPad">
            <img src="images/screenshots/thumb_Calendar.png" title="" border="0" />
            <div><h3>Shared Calendar</h3></div>
        </div>
        <div id="thumb_permissions" class="thumbnailContainer">
            <img src="images/screenshots/thumb_Permissions.png" title="" border="0" />
            <div><h3>Team Permissions</h3></div>
        </div>
    </div>
    <div class="bigShotContainer" id="dashboard">
        <div class="bigImageContainer"><img src="images/screenshots/big_Dashboard.png" /></div>
        <div class="bigTextContainer">
            <h2>Office Dashboard</h2>
            <p>Stay informed at a glance with dashboard widgets that show your calendar, open invoices,
                tasks, current projects and much more.</p>
            <div class="iconFilmBig">
                <a href="<?=BASE_URL; ?>tour/video/DashboardUIFeatures">Watch a screencast of the office dashboard</a>
            </div>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div class="bigShotContainer" id="invoicelist">
        <div class="bigImageContainer"><img src="images/screenshots/big_InvoiceList.png" /></div>
        <div class="bigTextContainer">
            <h2>Invoice List View</h2>
            <p></p>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div class="bigShotContainer" id="timesheet">
        <div class="bigImageContainer"><img src="images/screenshots/big_Timesheet.png" /></div>
        <div class="bigTextContainer">
            <h2>Timesheets</h2>
            <p>Never lose another hour of billable work! The time clock widget is always close by while
                timesheets give you a daily view of time spent on specific projects and tasks.</p>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div class="bigShotContainer" id="project">
        <div class="bigImageContainer"><img src="images/screenshots/big_Project.png" /></div>
        <div class="bigTextContainer">
            <h2>Project View</h2>
            <p>Critical project data like tasks, hours spent, amounts invoiced, project contacts,
                notes and messages are all presented in one concise view.</p>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div class="bigShotContainer" id="client">
        <div class="bigImageContainer"><img src="images/screenshots/big_Client.png" /></div>
        <div class="bigTextContainer">
            <h2>Client View</h2>
            <p></p>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div class="bigShotContainer" id="tasklist">
        <div class="bigImageContainer"><img src="images/screenshots/big_TaskList.png" /></div>
        <div class="bigTextContainer">
            <h2>Task Lists</h2>
            <p>Tasks, milestones, simple to-do lists...CreativePro Office supports them all. Assign tasks
                to multiple team members, attach files, create comment threads, and much more.</p>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div class="bigShotContainer" id="reports">
        <div class="bigImageContainer"><img src="images/screenshots/big_Reports.png" /></div>
        <div class="bigTextContainer">
            <h1>Reports and Charts</h1>
            <p>See what's happening over time with finance, task, time-sheet, and team member productivity reports.</p>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div class="bigShotContainer" id="calendar">
        <div class="bigImageContainer"><img src="images/screenshots/big_Calendar.png" /></div>
        <div class="bigTextContainer">
            <h1>Shared Calendar</h1>
            <p>Share multiple calendars with your team or clients. Export calendars in iCal
                format, RSS, or pull them into your Google calendar account.</p>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div class="bigShotContainer" id="permissions">
        <div class="bigImageContainer"><img src="images/screenshots/big_Permissions.png" /></div>
        <div class="bigTextContainer">
            <h1>Team Permissions</h1>
            <p>Team members should only see what they need to see! Administrators have very
                fine control over team member permissions and access.</p>
        </div>
        <div style="clear: left;"></div>
    </div>
</div>
<? $this->load->view('includes/footerPublic'); ?>