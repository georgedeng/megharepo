<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="clientDashboard">
    <script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var accountData = '[<?=$accountData; ?>]';
	/* ]]> */
	</script>
	<input type="hidden" id="accountUserid" value="<?=$accountUserid; ?>" />
	<input type="hidden" id="invoiceCountHolder" value="<?=$invoiceCount['InvoiceCount']; ?>" />
	<input type="hidden" id="projectCountHolder" value="<?=$projectCount['Projects']; ?>" />
    <input type="hidden" id="messageCountHolder" value="<?=$messageCount['NoOfMessagesTotal']; ?>" />
    <input type="hidden" id="notesCountHolder" value="<?=$notesCount; ?>" />
	
	<div id="tabsClientDashboard" class="ui-tabs">
		<ul class="ui-tabs-nav"> 
	        <li class="ui-tabs-selected" id="tabClientDetails"><a href="#windowProjects"><span><?=lang('project_page_title'); ?></span> <span id="projectCount" class="lightGray">(0)</span></a></li> 
	        <li><a href="#windowInvoices" id="tabClientInvoices"><span><?=lang('finance_invoices'); ?></span> <span id="invoiceCount" class="lightGray">(0)</span></a></li> 
	        <li><a href="#windowMessages" id="tabClientMessages"><span><?=lang('common_messages'); ?></span> <span id="messageCount" class="lightGray">(0)</span></a></li> 
	        <li><a href="#windowFiles" id="tabClientFiles"><span><?=lang('files_files'); ?></span> <span id="fileCount" class="lightGray">(0)</span></a></li>
            <li><a href="#windowNotes" id="tabClientNotes"><span><?=lang('notes_project_notes'); ?></span> <span id="noteCount" class="lightGray">(0)</span></a></li>
	        <!--<li class="alert"><a href="#windowAlerts" id="tabClientAlerts"><span><?=lang('common_alerts'); ?></span> <span id="alertCount" class="red">(2)</span></a></li>-->
	    </ul> 
	    <div style="min-height: 200px;" class="windowFrame"> 
	    	<div id="windowProjects" class="ui-tabs-hide">
                <div id="projectInformationContainer" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
		    </div> 
		    <div id="windowInvoices" class="ui-tabs-hide"> 
		    	<div>
		    		<div id="invoiceInformationContainer"></div>
		    		<div style="padding: 6px; display: none;" id="invoiceDetailsContainer"></div>
		    		<div style="padding: 6px; display: none;" id="invoiceMessageContainer">
		    			<div id="newMessageInvoice" itemType="invoice" sendTo="<?=$this->session->userdata('accountUserid'); ?>" class="messageBoxContainer"></div>
		    		</div>
		    	</div>
		    </div>
            <div id="windowFiles" class="ui-tabs-hide">
                <div class="barYellow bottom" style="padding: 6px 0 6px;">
                    <select class="selectProject" id="selectProjectFiles" size="1" style="margin-left: 6px; width: 300px;"></select>
				</div>
                <div style="padding: 6px;" id="filesInformationContainer">
                    <div id="client_fileContainer" class="fileAttachProject">
                        <div class="infoMessageBig"><?=lang('common_select_project_files'); ?></div>
                    </div>
                </div>
		    </div>
            <div id="windowMessages" class="ui-tabs-hide">
				<div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessage" itemID="" itemType="" class="messageContainer"></div>
					</div>
				</div>
				<div id="messageList" style="margin-top: 6px;"></div>
                <div id="messageMoreContainer" class="boxBlue" style="display: none; margin-top: 6px;"><a href="#" class="linkMoreMessages"><?=lang('common_more'); ?></a></div>
	   		</div>

            <div id="windowMessages" class="ui-tabs-hide">
				<div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessage" itemID="" itemType="" class="messageBoxContainerLink"></div>
					</div>
				</div>
				<div id="messageListProject" style="margin-top: 6px;"></div>
	   		</div>
            <div id="windowNotes" class="ui-tabs-hide">
		    	<div id="noteListContainer"></div>
		    </div>
		    <div id="windowAlerts" class="ui-tabs-hide"> 
		    	<div style="padding: 6px;">
		    	</div>
		    </div> 
		</div>	    
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>