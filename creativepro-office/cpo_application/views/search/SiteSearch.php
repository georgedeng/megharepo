<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="siteSearch">
<?
$searchResultsNumber = 0;
    
$moduleArray = array('Clients','Projects','Invoices','Expenses','Events','Messages','Tags');
foreach($moduleArray as $module) {
    switch ($module) {
        case 'Clients':
            $langCode = 'menu_clients';
            break;
        case 'Projects':
            $langCode = 'menu_projects';
            break;
        case 'Invoices':
            $langCode = 'finance_invoices';
            break;
        case 'Expenses':
            $langCode = 'finance_expenses';
            break;
        case 'Events':
            $langCode = 'calendar_events';
            break;
        case 'Messages':
            $langCode = 'widget_messages';
            break;
        case 'Tags':
            $langCode = 'client_form_tags';
            break;
    }
    if (count($searchResults[$module])>0) {
        echo '<div class="barBlue bottom clickable searchModuleHeader"><h2 style="padding: 9px;">'.lang($langCode).' <span class="lightBlue">('.count($searchResults[$module]).')</span></h2></div>';
        echo '<div class="searchModuleContainer" style="margin-left: 12px;">';
        echo '<table class="dataTable" style="background: none;">';
        foreach($searchResults[$module] as $item) {
            echo '<tr class="rowLines" style="border-top: none;">';
            if ($module == 'Clients') {
                echo '<td><a href="'.site_url('clients/ClientDetail/index/'.$item['ClientID']).'">'.$item['Company'].'</a></td>';
            } elseif ($module == 'Projects') {
                echo '<td><a href="'.site_url('projects/ProjectDetail/index/'.$item['ProjectID']).'">'.$item['Title'].'</a>';
                if (!empty($item['ClientID'])) {
                    echo ' for <a href="'.site_url('clients/ClientDetail/index/'.$item['ClientID']).'">'.$item['Company'].'</a>';
                }
                echo '</td>';
            } elseif ($module == 'Invoices') {
                echo '<td style="width: 70%"><a href="'.site_url('finances/FinanceView/viewInvoice/'.$item['InvoiceID']).'">'.$item['Title'].'</a>';
                if (!empty($item['ClientID'])) {
                    echo ' for <a href="'.site_url('clients/ClientDetail/index/'.$item['ClientID']).'">'.$item['Company'].'</a>';
                }
                echo '</td>';
                echo '<td style="width: 15%;">'.date('M j, Y', strtotime($item['DateInvoice'])).'</td>';
                echo '<td style="width: 15%; text-align: right;">'.getCurrencyMark($this->session->userdata('currency')).$item['InvTotal'].'</td>';
            } elseif ($module == 'Expenses') {
                echo '<td style="width: 70%;"><a href="'.site_url('finances/FinanceView/viewExpense/'.$item['ExpenseID']).'">'.$item['Title'].'</a></td>';
                echo '<td style="width: 15%;">'.date('M j, Y', strtotime($item['DateEntry'])).'</td>';
                echo '<td style="width: 15%; text-align: right;">'.getCurrencyMark($this->session->userdata('currency')).$item['Amount'].'</td>';
            } elseif ($module == 'Events') {
                if ($item['DateStart'] == '0000-00-00 00:00:00' || $item['DateStart'] == '0000-00-00') {
                    $dateStart = '';
                } else {
                    $dateStart = convertMySQLToGMT($item['DateStart'],'pull','M j, g:i a');
                }
                if ($item['DateEnd'] == '0000-00-00 00:00:00' || $item['DateEnd'] == '0000-00-00') {
                    $dateEnd = '';
                } else {
                    $dateEnd   = convertMySQLToGMT($item['DateEnd'],'pull','M j, g:i a');
                }

                $dateString = '';
                if (!empty($dateStart)) {
                    $dateString .= $dateStart;
                }
                if (!empty($dateEnd)) {
                    $dateString .= ' - '.$dateEnd;
                }

                echo '<td><a href="'.site_url('calendar/CalendarViewExternal/viewEvent/'.$this->session->userdata('persAuthString').'/'.$item['EventID']).'">'.$item['Title'].'</a></td>';
                echo '<td>'.$dateString.'</td>';
            } elseif ($module == 'Messages') {
                $item['ItemIcon'] = $this->commonmessages->getMessageIcon($item['ItemType'],$item['ItemID']);
                if (!empty($item['FromName'])) {
					$from = $item['FromName'];
				} else {
					$from = $item['Company'];
				}
                $item['DateMessage'] = convertMySQLToGMT($item['DateMessage'],'pull','M j, g:i a');
                echo '<td><div class="messageContainer" id="'.$item['MID'].'">';
                echo '<div class="oddRow" style="padding: 2px;">';
                echo '<img src="'.$item['Avatar'].'" class="avatar33 left" /><div style="float: left;"><a href="#" class="personLink" personID="'.$item['PIDFrom'].'">'.$from.'</a> <span class="subText">'.$item['DateMessage'].'</span> '.$item['ItemIcon'].'</div>';
                echo '<div style="clear: both;"></div></div>';
                echo '<p style="margin: 6px;">'.$item['Message'].'</p>';
                echo '</td>';
            }
            echo '</tr>';
            $searchResultsNumber++;
        }
        echo '</table>';
        echo '</div>';
    }
}
if ($searchResultsNumber == 0) {
    echo '<div class="infoMessageBig">'.sprintf(lang('help_no_search_results'),$searchTerm).'</div>';
}
?>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>