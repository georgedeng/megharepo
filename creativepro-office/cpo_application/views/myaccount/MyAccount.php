<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="settingsOwner" class="helptag" data-help="settings">
	<div id="tabsSettings" class="ui-tabs">
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-selected" id="tabAccountPassword"><a href="#windowAccountPassword"><span><?=lang('settings_change_password'); ?></span></a></li>
            <? if ($this->session->userdata('userType') == USER_TYPE_OWNER || $this->session->userdata('userType') == USER_TYPE_GOD) { ?>
            <li id="tabAccountChangePlan"><a href="#windowAccountChangePlan"><span><?=lang('plan_update_plan_cc'); ?></span></a></li>
            <li id="tabAccountViewInvoices"><a href="#windowAccountViewInvoices"><span><?=lang('settings_account_view_payments'); ?></span></a></li>
            <li id="tabAccountCancel"><a href="#windowAccountCancel"><span><?=lang('settings_cancel_account'); ?></span></a></li>
            <? } ?>
        </ul>
	    <div style="min-height: 200px;" class="windowFrame">
            <div id="windowAccountPassword">
                <div style="padding: 12px;">
                    <h3 class="icon_password" style="margin: 6px;"><?=lang('settings_change_password'); ?></h3>

                    <form class="noBorder" id="accountFormPassword">
                        <p class="padBottom12">
                        <label><?=lang('common_current_password'); ?></label>
                        <input type="password" style="width: 150px;" id="currentPassword" name="currentPassword" />
                        </p>
                        <p>
                        <label for="password"><?=lang('common_new_password'); ?></label>
                        <input type="password" style="width: 150px;" id="newPassword" name="newPassword" />
                        </p>
                        <div style="margin: 6px 0 24px 262px;">
                            <button class="buttonExpand action" id="buttonSaveAccountPassword"><span class="save"><?=lang('button_save'); ?></span></button>
                            <div style="float: left; margin-left: 12px;" id="messageSettingsAccountPassword"></div>
                            <div style="clear: left;"></div>
                        </div>
                    </form>
                </div>    
            </div>
            
            <? if ($this->session->userdata('userType') == USER_TYPE_OWNER || $this->session->userdata('userType') == USER_TYPE_GOD) { ?>
                    
            <div id="windowAccountChangePlan" class="ui-tabs-hide">
                <div style="padding: 12px;">
                    <h3 class="icon_briefcase" style="margin: 6px;"><?=lang('plan_update_plan_or_cc'); ?></h3>
                    
                    <div style="margin: 12px 0 0 6px;">
                        <h4 style="margin-bottom: 12px;">Your account plan: <?=$accountName; ?></h4>
                        <?=$accountDesc; ?>

                        <button class="buttonExpand action" id="buttonUpdateAccount" style="margin: 12px 12px 0 0;"><span class="accountUpdate"><?=lang('plan_update_plan'); ?></span></button>
                        
                        <div id="changeCCContainer">
                            <button class="buttonExpand green mTop12" id="buttonUpdateCCInfo"><span class="creditCardEdit"><?=lang('plan_update_cc'); ?></span></button>
                        </div>
                    </div>    
                </div>    
            </div>
            
            <div id="windowAccountViewInvoices" class="ui-tabs-hide">
                <div>
                    <h3 class="icon_payments" style="margin: 12px;"><?=lang('settings_account_payments'); ?></h3>
                    <div id="paymentGridContainer"></div>
                </div>
            </div>    
            
            <div id="windowAccountCancel" class="ui-tabs-hide">
                <div style="padding: 12px;">
                    <h3 class="icon_delete" style="margin: 6px;"><?=lang('settings_cancel_account'); ?></h3>
                    <form class="noBorder" id="accountFormCancel">
                        <div class="warningDiv" style="width: 643px; margin: 12px 0 12px 0;"><?=lang('help_account_cancel'); ?></div>

                        <div style="margin-left: 50px;"><?=lang('help_account_cancel_message'); ?></div>

                        <div style="margin: 6px 0 24px 47px;">
                            <button class="buttonExpand cancel" id="buttonAccountCancel"><span class="delete"><?=lang('settings_cancel_account'); ?></span></button>
                            <div style="clear: left;"></div>
                        </div>
                    </form>
                </div>
            </div>  
			<? } ?>
		</div>
	</div>
    <div id="dialogCancelAccount" class="noPrint" style="background: #fff;" title="<?=lang('settings_cancel_account'); ?>">
        <div style="padding: 6px;" id="deleteContentsContainer">
            <div class="warningDiv" style="width: 418px; margin: 12px 0 12px 0;"><?=lang('help_account_cancel'); ?></div>
        </div>
        <button id="buttonCancelAccount" class="smallButton" style="margin: 0 6px 0 6px; float: left;"><span class="delete"><?=lang('settings_cancel_account'); ?></span></button>
        <button id="buttonCancel" class="smallButton" style="margin-right: 6px; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button
        <div style="clear: left;"></div>
    </div>   
</div>
<? $this->load->view('includes/footer'); ?>
