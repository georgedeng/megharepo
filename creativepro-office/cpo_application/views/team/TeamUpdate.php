<? $this->load->view('includes/header'); ?>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
	var roleData = '<?=$rolesPermissions; ?>';
	var contactData = '<?=$teamMemberInfo; ?>';
/* ]]> */
</script>
<div id="pageLeftColumn" pageID="teamUpdate">
	<input type="hidden" id="contactType" value="T" />
	<div id="tabsTeam" class="ui-tabs" style="width: 964px;">
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-selected"><a href="#windowViewTeamMembers"><span><?=lang('team_members'); ?></span></a></li>
	        <li class="teamCreate hide"><a href="#windowRoles"><span><?=lang('team_set_roles'); ?></span></a></li>
	    </ul>
		<div style="min-height: 200px;" class="windowFrame">
	    	<div id="windowViewTeamMembers">
				<div class="barYellow bottom" id="teamMemberControl">
                    <div style="margin: 6px; float: left; width: 650px;" class="infoMessageSmall lightText"><?=lang('help_team_add_team_member'); ?></div>
					<div style="padding: 6px; float: right;">
						<button class="buttonExpand blueGray action teamCreate hide" id="buttonNewContact"><span class="add"><?=lang('team_new_team_member'); ?></span></button>
						<div style="clear: left;"></div>
					</div>
					<div style="clear: both;"></div>
				</div>
                <p id="noContactLinkContainer" style="display: none;"></p>
				<div id="contactsContainer" class="padBottom12" style="padding-left: 9px;"><div class="activityMessageBig" style="margin-top: 9px;"><?=lang('common_loading'); ?></div></div>
				<div id="formContactContainer" style="display: none; padding: 6px;">
					<? $this->load->view('includes/contactForm'); ?>
				</div>
			</div>
			<div id="windowRoles" class="ui-tabs-hide">
				<div style="padding: 12px;">					
                    <form class="noBorder" id="rolesForm">
                    <input type="hidden" id="roleID" value="0" />
                    <input type="hidden" id="action" value="add" />
					<div style="width: 50%; float: left;">
						<div class="icon_add" id="addText"><strong><?=lang('team_role_create'); ?></strong></div>
						<div class="iconEditSmall" id="updateText" style="display: none; float: left; margin-right: 12px;"><strong><?=lang('team_role_edit_this'); ?></strong></div>
                        <div class="iconDeleteSmall" id="deleteText" style="display: none; float: left;"><a href="#" id="linkDeleteRole"><?=lang('team_role_delete_this'); ?></a></div>
                        <div style="clear: left;"></div>
                        <input type="text" id="roleName" style="margin-top: 3px; width: 250px;" />
					</div>
					<div style="width: 50%; float: right;">
						<div style="display: none;" id="selectRoleEditContainer">
							<strong><?=lang('team_role_edit'); ?></strong><br />
							<select id="selectRoleEdit" style="margin-top: 3px; width: 250px;"></select>
						</div>	
					</div>
					<div style="clear: both; height: 12px;"></div>
					<table class="dataTable" style="background: none;">
						<tr>
							<td style="width: 18%;"><h4 class="icon_client_small"><?=lang('menu_clients'); ?></h4></td>
                            <td style="width: 12%;"><input type="checkbox" class="selectAll" id="client_select_all" /> <label for="client_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td style="width: 25%;"><input type="checkbox" class="clientCheck view" id="client_view" /> <label for="client_view" class="normal clickable"><?=lang('common_view'); ?></label></td>
							<td style="width: 25%;"><input type="checkbox" class="clientCheck add" id="client_add" /> <label for="client_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td style="width: 10%;"><input type="checkbox" class="clientCheck update" id="client_update" /> <label for="client_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td style="width: 10%;"><input type="checkbox" class="clientCheck delete" id="client_delete" /> <label for="client_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
						<tr class="rowLines">
                            <td><h4 class="icon_project_small"><?=lang('menu_projects'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="project_select_all" /> <label for="project_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td>
                                <input type="checkbox" class="projectCheck all view" id="project_view" /> <label for="project_view" class="normal clickable"><?=lang('common_view_all'); ?></label><br />
                                <input type="checkbox" class="projectCheck own" id="project_view_assigned" /> <label for="project_view_assigned" class="normal clickable"><?=lang('project_view_assigned'); ?></label>
                            </td>
							<td><input type="checkbox" class="projectCheck add" id="project_add" /> <label for="project_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td><input type="checkbox" class="projectCheck update" id="project_update" /> <label for="project_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="projectCheck delete" id="project_delete" /> <label for="project_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
                        <tr>
                            <td><h4 class="icon_tasks_small"><?=lang('widget_tasks'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="task_select_all" /> <label for="task_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td>
                                <input type="checkbox" class="taskCheck all view" id="task_view" /> <label for="task_view" class="normal clickable"><?=lang('common_view_all'); ?></label><br />
                                <input type="checkbox" class="taskCheck own" id="task_view_assigned" /> <label for="task_view_assigned" class="normal clickable"><?=lang('project_view_assigned'); ?></label>
                            </td>
							<td><input type="checkbox" class="taskCheck add" id="task_add" /> <label for="task_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td><input type="checkbox" class="taskCheck update" id="task_update" /> <label for="task_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="taskCheck delete" id="task_delete" /> <label for="task_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
						<tr class="rowLines">
                            <td><h4 class="icon_invoice_small"><?=lang('finance_invoices'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="invoice_select_all" /> <label for="invoice_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td>
                                <input type="checkbox" class="invoiceCheck all viewAll view" id="invoice_view" /> <label for="invoice_view" class="normal clickable"><?=lang('common_view_all'); ?></label><br />
                                <input type="checkbox" class="invoiceCheck own viewOwn" id="invoice_view_own" /> <label for="invoice_view_own" class="normal clickable"><?=lang('common_view_personal'); ?></label>
                            </td>
							<td>
                                <input type="checkbox" class="invoiceCheck all addAll" id="invoice_add" /> <label for="invoice_add" class="normal clickable"><?=lang('finance_invoice_create_send'); ?></label><br />
                                <input type="checkbox" class="invoiceCheck own addOwn" id="invoice_add_own" /> <label for="invoice_add_own" class="normal clickable"><?=lang('finance_invoice_create_send_own'); ?></label>
                            </td>
							<td><input type="checkbox" class="invoiceCheck update" id="invoice_update" /> <label for="invoice_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="invoiceCheck delete" id="invoice_delete" /> <label for="invoice_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
						<tr>
                            <td><h4 class="icon_expense_small"><?=lang('finance_expenses'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="expense_select_all" /> <label for="expense_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td>
								<input type="checkbox" class="expenseCheck all view" id="expense_view" /> <label for="expense_view" class="normal clickable"><?=lang('common_view_all'); ?></label><br />
								<input type="checkbox" class="expenseCheck own" id="expense_view_own" /> <label for="expense_view_own" class="normal clickable"><?=lang('common_view_personal'); ?></label>
							</td>
							<td><input type="checkbox" class="expenseCheck add" id="expense_add" /> <label for="expense_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td><input type="checkbox" class="expenseCheck update" id="expense_update" /> <label for="expense_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="expenseCheck delete" id="expense_delete" /> <label for="expense_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
						<tr class="rowLines">
							<td><h4 class="icon_timer_small"><?=lang('menu_timesheets'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="timesheet_select_all" /> <label for="timesheet_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td>
								<input type="checkbox" class="timesheetCheck all view" id="timesheet_view" /> <label for="timesheet_view" class="normal clickable"><?=lang('common_view_all'); ?></label><br />
								<input type="checkbox" class="timesheetCheck own" id="timesheet_view_own" /> <label for="timesheet_view_own" class="normal clickable"><?=lang('common_view_personal'); ?></label><br />
							</td>
							<td><input type="checkbox" class="timesheetCheck add" id="timesheet_add" /> <label for="timesheet_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td><input type="checkbox" class="timesheetCheck update" id="timesheet_update" /> <label for="timesheet_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="timesheetCheck delete" id="timesheet_delete" /> <label for="timesheet_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
						<tr>
							<td><h4 class="icon_file_small"><?=lang('files_files'); ?></h4></td>
                            <td><input type="checkbox" class="selectAll" id="file_select_all" /> <label for="file_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td><input type="checkbox" class="fileCheck view" id="file_view" /> <label for="file_view" class="normal clickable"><?=lang('common_view'); ?></label></td>
							<td><input type="checkbox" class="fileCheck add" id="file_add" /> <label for="file_add" class="normal clickable"><?=lang('common_upload'); ?></label></td>
							<td><input type="checkbox" class="fileCheck update" id="file_update" /> <label for="file_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="fileCheck delete" id="file_delete" /> <label for="file_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
						<tr class="rowLines">
							<td><h4 class="icon_team_small"><?=lang('team_members'); ?></h4></td>
                            <td><input type="checkbox" class="selectAll" id="team_select_all" /> <label for="team_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td><input type="checkbox" class="teamCheck view" id="team_view" /> <label for="team_view" class="normal clickable"><?=lang('common_view'); ?></label></td>
							<td><input type="checkbox" class="teamCheck add" id="team_add" /> <label for="team_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td><input type="checkbox" class="teamCheck update" id="team_update" /> <label for="team_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="teamCheck delete" id="team_delete" /> <label for="team_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
						<tr>
							<td><h4 class="icon_comment_small"><?=lang('widget_messages'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="message_select_all" /> <label for="message_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td>
								<input type="checkbox" class="messageCheck view" id="message_view" /> <label for="message_view" class="normal clickable"><?=lang('common_view_all'); ?></label><br />
								<input type="checkbox" class="messageCheck" id="message_view_own" /> <label for="message_view_own" class="normal clickable"><?=lang('common_view_personal_messages'); ?></label>
							</td>
							<td><input type="checkbox" class="messageCheck add" id="message_add" /> <label for="message_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td><input type="checkbox" class="messageCheck update" id="message_update" /> <label for="message_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="messageCheck delete" id="message_delete" /> <label for="message_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
						<tr class="rowLines">
							<td><h4 class="icon_calendar_small"><?=lang('widget_calendar'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="calendar_select_all" /> <label for="calendar_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td>
								<input type="checkbox" class="calendarCheck all view" id="calendar_view" /> <label for="calendar_view" class="normal clickable"><?=lang('common_view_all'); ?></label><br />
								<input type="checkbox" class="calendarCheck own" id="calendar_view_own" /> <label for="calendar_view_own" class="normal clickable"><?=lang('common_view_personal'); ?></label>
							</td>
							<td><input type="checkbox" class="calendarCheck add" id="calendar_add" /> <label for="calendar_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td><input type="checkbox" class="calendarCheck update" id="calendar_update" /> <label for="calendar_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="calendarCheck delete" id="calendar_delete" /> <label for="calendar_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
                        <tr>
                            <td><h4 class="icon_vcard"><?=lang('common_contacts'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="contacts_select_all" /> <label for="contacts_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td><input type="checkbox" class="contactsCheck view" id="contacts_view" /> <label for="contacts_view" class="normal clickable"><?=lang('common_view'); ?></label></td>
							<td><input type="checkbox" class="contactsCheck add" id="contacts_add" /> <label for="contacts_add" class="normal clickable"><?=lang('common_create'); ?></label></td>
							<td><input type="checkbox" class="contactsCheck update" id="contacts_update" /> <label for="contacts_update" class="normal clickable"><?=lang('common_update'); ?></label></td>
							<td><input type="checkbox" class="contactsCheck delete" id="contacts_delete" /> <label for="contacts_delete" class="normal clickable"><?=lang('common_delete'); ?></label></td>
						</tr>
                        <tr class="rowLines">
                            <td><h4 class="icon_trash_small"><?=lang('common_trashcan'); ?></h4></td>
							<td><input type="checkbox" class="selectAll" id="trash_select_all" /> <label for="trash_select_all" class="normal clickable"><?=lang('common_select_all'); ?></label></td>
							<td><input type="checkbox" class="trashCheck" id="trash_view" /> <label for="trash_view" class="normal clickable"><?=lang('common_view_all'); ?></label></td>
							<td><input type="checkbox" class="trashCheck" id="trash_view_own" /> <label for="trash_view_own" class="normal clickable"><?=lang('common_view_personal_trash'); ?></label></td>
							<td></td>
							<td></td>
						</tr>
                        <tr class="rowLines">
                            <td><h4 class="icon_settings_small"><?=lang('settings_page_title'); ?></h4></td>
							<td></td>
							<td><input type="checkbox" class="settingsCheck" id="settings_view" /> <label for="settings_view" class="normal clickable"><?=lang('settings_update_all'); ?></label></td>
							<td><input type="checkbox" class="settingsCheck" id="settings_view_own" /> <label for="settings_view_own" class="normal clickable"><?=lang('settings_update_own'); ?></label></td>
							<td></td>
							<td></td>
						</tr>
					</table>
					<div style="margin: 6px 0 0 173px;">
						<button class="buttonExpand blueGray action" id="buttonSave"><span class="save"><?=lang('button_save'); ?></span></button>
						<button class="buttonExpand yellow cancel" id="buttonCancel" style="margin-left: 200px;"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
                        <div style="float: left; margin-left: 12px;" id="messageRoles"></div>
                        <div style="clear: left;"></div>
					</div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="clear: left;"></div>
<? $this->load->view('includes/footer'); ?>