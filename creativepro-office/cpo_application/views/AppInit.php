<? $this->load->view('includes/headerSC'); ?>
<div class="home_banner_slider">
    <div class="item">
        <div class="each_banner" style="background-image: url(images/sc-image/homepageBackground-1.jpg);">
            <div class="display_table_cell">
                <div class="banner_text">
                    <h1>Projects, Invoices, Tasks, Time Tracking, Calendar</h1>
                    <p>CreativePro Office does more for less!</p>
                    <div class="button_area">
                        <a href="<?php echo base_url();?>tour">Product Tour</a><a href="<?php echo base_url();?>pricing">Pricing & Signup</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services_sec">
    <div class="container">
        <div class="all_lists">
            <div class="esch_list">
                <div class="each_service">
                    <img src="images/sc-image/new-icon1.png" alt="">
                    <h4>Office Dashboard</h4>
                    <p>Stay informed at a glance with dashboard widgets that show your calendar, open invoices, tasks, current projects and much more.</p>
                </div>
            </div>
            <div class="esch_list">
                <div class="each_service">
                    <img src="images/sc-image/new-icon2.png" alt="">
                    <h4>Project Manager</h4>
                    <p>Critical project data like tasks, hours spent, amounts invoiced, project contacts, notes and messages are all presented in one concise view.</p>
                </div>
            </div>
            <div class="esch_list">
                <div class="each_service">
                    <img src="images/sc-image/new-icon3.png" alt="">
                    <h4>Invoices & Expenses</h4>
                    <p>Quickly create professional looking invoices for your clients and send them via email. Quickly add timesheet entries and expense items to your invoices.</p>
                </div>
            </div>
            <div class="esch_list">
                <div class="each_service">
                    <img src="images/sc-image/new-icon4.png" alt="">
                    <h4>Group Calendars</h4>
                    <p>Share multiple calendars with your team or clients. Export calendars in iCal format, RSS, or pull them into your Google calendar account.</p>
                </div>
            </div>
            <div class="esch_list">
                <div class="each_service">
                    <img src="images/sc-image/new-icon5.png" alt="">
                    <h4>Timesheets & Time Clock</h4>
                    <p>Stay informed at a glance with dashboard widgets that show your calendar, open invoices, tasks, current projects and much more.</p>
                </div>
            </div>
            <div class="esch_list">
                <div class="each_service">
                    <img src="images/sc-image/new-icon6.png" alt="">
                    <h4>Team Permissions</h4>
                    <p>Team members should only see what they need to see! Administrators have very fine control over team member permissions and access.</p>
                </div>
            </div>
            <div class="esch_list">
                <div class="each_service">
                    <img src="images/sc-image/new-icon7.png" alt="">
                    <h4>Task Management</h4>
                    <p>Tasks, milestones, simple to-do lists...CreativePro Office supports them all. Assign tasks to multiple team members, attach files, create comment threads, and much more.</p>
                </div>
            </div>
            <div class="esch_list">
                <div class="each_service">
                    <img src="images/sc-image/new-icon8.png" alt="">
                    <h4>International</h4>
                    <p>CreativePro Office currently supports almost 10 languages and 20 currencies with more being added soon.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pricing_sec">
    <div class="container">
        <h2>PRICING</h2>
        <div class="table_area">
            <table>
                <thead>
                    <tr align="center">
                        <td width="24%">&nbsp;</td>
                        <td width="24%">
                            <h4>SOLO</h4>
                            <p><span><sup>$</sup><?php echo $currentPrices[1]; ?></span>monthly</p>
                        </td>
                        <td width="24%">
                            <h4>SHOP</h4>
                            <p><span><sup>$</sup><?php echo $currentPrices[2]; ?></span>monthly</p>
                        </td>
                        <td width="24%">
                            <h4>TEAM</h4>
                            <p><span><sup>$</sup><?php echo $currentPrices[3]; ?></span>monthly</p>
                        </td>
                        <td width="4%">&nbsp;</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Active Storage</td>
                        <td>10</td>
                        <td>50</td>
                        <td>Unlimited</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Storage</td>
                        <td>10GB</td>
                        <td>50GB</td>
                        <td>100GB</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Team Member</td>
                        <td>3</td>
                        <td>10</td>
                        <td>Unlimited</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Support</td>
                        <td>Priority email</td>
                        <td>Priority email</td>
                        <td>Priority email</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="cradit_card">
                        <td><h4>No credit card required!</h4></td>
                        <td><a href="<?php echo base_url();?>signup/solo">Sign Up</a></td>
                        <td><a href="<?php echo base_url();?>signup/shop">Sign Up</a></td>
                        <td><a href="<?php echo base_url();?>signup/team">Sign Up</a></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="cradit_card">
                        <td colspan="5"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="testimonial_sec">
    <div class="container">
        <h2>Testimonials</h2>
        <div class="testi_slider_sec">
            <div class="slider-for">
                <div class="item">
                    <div class="textimonial_centent">
                        <p>We are wanting to switch all our book keeping to CPO from Freshbooks, so we can use CPO for everything. Thanks for a great product.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="textimonial_centent">
                        <p>Hi! I've been trying out your software and have to congratulate you. I've tried some of your competitors and CPO is by far the most intuitive and easy to use. The price is hard to beat as well.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="textimonial_centent">
                        <p>Hi! Awesome job with this app! Beats all the others I´ve been looking at!</p>
                    </div>
                </div>
            </div>
            <div class="slider-nav">
                <div class="item">
                    <figure><img src="images/sc-image/new-image1.jpg" alt=""></figure>
                    <h4>John Pack</h4>
                    <p>Baja Good Life Club</p>
                </div>
                <div class="item">
                    <figure><img src="images/sc-image/new-image1.jpg" alt=""></figure>
                    <h4>Helgi Mar Hallgrímsson</h4>
                    <!-- <p>Baja Good Life Club</p> -->
                </div>
                <div class="item">
                    <figure><img src="images/sc-image/new-image1.jpg" alt=""></figure>
                    <h4>Ignasi Perez</h4>
                    <!-- <p>Baja Good Life Club</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="partners_logo_sec">
    <div class="container">
        <div class="partners_slider">
            <!-- <div class="item">
                <figure><img src="images/sc-image/Client_1.jpg"></figure>
            </div>
            <div class="item">
                <figure><img src="images/sc-image/Client_2.jpg"></figure>
            </div>
            <div class="item">
                <figure><img src="images/sc-image/Client_3.jpg"></figure>
            </div>
            <div class="item">
                <figure><img src="images/sc-image/Client_4.jpg"></figure>
            </div> -->
            <?php //print_r($logoArray);
            foreach($logoArray as $logo) 
            { ?>
                <div class="item">
                    <figure><a href="<?php echo $logo['CompanyURL']; ?>" title="<?php echo $logo['CompanyName']; ?>" target="_blank"><img src="<?php echo base_url();?>images/customerLogos/<?php echo $logo['LogoFilename']; ?>" alt="<?php echo $logo['CompanyName'];?>"></a></figure>
                </div> <?php
            } ?>
        </div>
    </div>
</div>
<div class="pricing_and_signup">
    <div class="container">
        <span>Free 30 days trial. Sign up now!</span><a href="<?php echo base_url();?>pricing">Pricing & Signup</a>
    </div>
</div>
<? $this->load->view('includes/footerSC'); ?>
