<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="iconHorror"><?=$errorTitle; ?></h1>

    <p style="margin: 6px 0 0 45px;"><?=$errorMessage; ?></p>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>