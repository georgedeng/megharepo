<?

$randomString = random_string();



$headerClass = 'home';

if ($child == TRUE) {

    $headerClass = 'child';

}

if (!isset($pageTitle)) {

    $pageTitle = 'Project Management Dashboard Software to Organize Your Work';

} else {

    $pageTitle .= ' :: Project Management Dashboard Software to Organize Your Work';

}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">

<head>

    <META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?=$pageTitle; ?></title>

    <meta name="Description" content="<?=$pageDescription; ?>">

    <meta name="Keywords" content="project management dashboard software,task management software,web based office applications,online project management tools ,online timesheet,online invoice, project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist">

    <meta name="Author" content="Jeff Denton @ GC LLC Productions :: Fairfax VA :: admin@mycpohq.com">

    <META NAME="Rating" CONTENT="General">

    <META NAME="Distribution" CONTENT="global">

    <!-- Favicon -->

    <link rel="icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />



    <!-- Style Sheets -->

    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/browserReset.css?<?=$randomString; ?>" type="text/css" />

    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/stylesPublic.css?<?=$randomString; ?>" type="text/css" />

    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/buttons.css?<?=$randomString; ?>" type="text/css" />

    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/alerts.css?<?=$randomString; ?>" type="text/css" />



    

    <!-- RSS Feeds -->

    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?=BASE_URL; ?>rss/Rss/productBlogFeed" />



    <!-- Print Style Sheet -->

    <link rel="STYLESHEET" media="print" href="<?=BASE_URL; ?>css/print.css?<?=$randomString; ?>" type="text/css" />



    <link href="<?php echo base_url(); ?>css/sc-style.css" rel="stylesheet" type="text/css">

    
    <!-- sc added below part for inner header banner ==========================-->
    <style type="text/css">

        #header .headerTextContainer {

            background: url('<?php echo base_url(); ?>images/<?php print $headerTextImage; ?>') 0px 27px no-repeat;

        }
    </style>



</head>

<body>

    <header id="main_header" class="main_header">

        <div class="logo_navigation">

            <div class="container">

                <div class="logo">

                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/sc-image/logo.png" alt=""></a>

                </div>

                <!-- <span class="toggle-menu"><img src="<?php echo base_url(); ?>images/sc-image/menu-icon.png" alt=""></span>

                <nav class="nav_sec">

                    <ul>

                        <li><a href="<?php echo base_url(); ?>">Home</a></li>

                        <li>

                            <a href="<?php echo base_url(); ?>tour">Tour</a>

                            <ul>

                                <li><a href="#">Submenu 1</a></li>

                                <li><a href="#">Submenu 2</a></li>

                                <li><a href="#">Submenu 3</a></li>

                                <li><a href="#">Submenu 4</a></li>

                            </ul>

                        </li>

                        <li><a href="<?php echo base_url(); ?>extras">Extras</a></li>

                        <li><a href="<?php echo base_url(); ?>pricing">Pricing</a></li>

                        <li><a href="<?php echo base_url(); ?>blog">Blog</a></li>

                        <li><a href="<?php echo base_url(); ?>contact">Contact</a></li>



                    </ul>

                </nav> -->

                <div class="header_user">
                    <div class="headerAvatar default">

                        <a href="<?=INSECURE_URL; ?>dashboard/Dashboard">Go to your account</a> or <a href="<?=INSECURE_URL; ?>login/logout">Log out</a></div>
                </div>

            </div>
        </div>

    </header><!-- /header -->

        <!-- sc added below part for inner header banner ==========================-->
        <div id="header" class="<?=$headerClass; ?>">
             <div class="homeFieldBluePublic">

                <div class="content">

                    <div class="headerTextContainer"></div>

                </div>            

            </div>  

        </div>
        <!--===================================================================-->

    <div id="pageBody">
<div class="securePadlockHeader"></div>