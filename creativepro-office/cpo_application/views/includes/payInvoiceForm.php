<div id="payInvoiceFormContainer">
    <div style="float: left; margin-right: 6px;">
        <strong><?=lang('finance_expense_amount'); ?></strong><br />
        <input type="text" class="payInvoiceAmount" id="payInvoiceAmount" style="width: 70px;" />
    </div>
    <div style="float: left; margin-right: 6px;">
        <strong><?=lang('common_status'); ?></strong><br />
        <select id="payInvoiceStatus" class="payInvoiceStatus" size="1" style="width: 120px;">
            <option value="open"><?=lang('common_open'); ?></option>
            <option value="closed_paid"><?=lang('status_closed_paid'); ?></option>
            <option value="closed_not_paid"><?=lang('status_closed_not_paid'); ?></option>
        </select>
    </div>
    <div style="float: left;">
        <strong><?=lang('finance_date_paid'); ?></strong><br />
        <input type="text" class="payInvoiceDate dateField" id="payInvoiceDate" style="width: 100px;" value="<?=$payDateDefault; ?>" />
    </div>
    <div style="clear: left; height: 12px;"></div>
    
    <div style="float: left; margin-right: 6px;">
        <strong><?=lang('finance_payment_type'); ?></strong><br />
        <select id="payInvoicePaymentType" class="payInvoicePaymentType" size="1" style="width: 204px;">
            <option value="cash"><?=lang('finance_expense_pay_method_cash'); ?></option>
            <option value="check"><?=lang('finance_expense_pay_method_check'); ?></option>
            <option value="cc_company"><?=lang('finance_expense_pay_method_cc_company'); ?></option>
            <option value="cc_personal"><?=lang('finance_expense_pay_method_cc_personal'); ?></option>
            <option value="paypal"><?=lang('finance_expense_pay_method_paypal'); ?></option>
            <option value="money_order"><?=lang('finance_expense_pay_method_money_order'); ?></option>
            <option value="bank_transfer"><?=lang('finance_expense_pay_method_bank_transfer'); ?></option>

        </select>
    </div>
    <div style="float: left;">
        <strong><?=lang('finance_expense_pay_method_acct_no'); ?></strong><br />
        <input type="text" style="width: 100px;" id="payInvoiceAccountNumber" class="payInvoiceAccountNumber" />
    </div>
    <div style="clear: left; height: 12px;"></div>

    <div style="margin-bottom: 3px;">
        <strong><?=lang('common_comments'); ?></strong><br />
        <textarea id="payInvoiceComment" style="width: 310px; height: 30px;" class="payInvoiceComment expanding"></textarea>
    </div>
    <button class="smallButton payInvoiceSave" id="payInvoiceSave" style="float: left; margin-right: 12px;"><span class="moneyAdd"><?=lang('finance_save_payment'); ?></span></button>
    <button class="smallButton" id="payInvoiceCancel" style="display: none; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
    <div style="clear: left;"></div>
</div>
