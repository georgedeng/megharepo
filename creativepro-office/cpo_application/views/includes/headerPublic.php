<?

$randomString = random_string();



$headerClass = 'home';

if ($child == TRUE) {

    $headerClass = 'child';

}

if (!isset($pageTitle)) {

    $pageTitle = 'Project Management Dashboard Software to Organize Your Work';

} else {

    $pageTitle .= ' :: Project Management Dashboard Software to Organize Your Work';

}



if (!isset($pageDescription)) {

    $pageDescription = PUBLIC_META_DESCRIPTION;

}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">

<head>

    <META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?=$pageTitle; ?></title>

    <meta name="Description" content="<?=$pageDescription; ?>">

    <meta name="Keywords" content="project management dashboard software,task management software,web based office applications,online project management tools ,online timesheet,online invoice, project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist">

    <meta name="Author" content="Jeff Denton @ GC LLC Productions :: Fairfax VA :: admin@mycpohq.com">

    <META NAME="Rating" CONTENT="General">

    <META NAME="Distribution" CONTENT="global">

    <!-- Favicon -->

    <link rel="icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />



    <!-- Style Sheets -->

    <?=cachedFile('browserReset.css','media="screen"'); ?>

    <?=cachedFile('stylesPublic.css','media="screen"'); ?>

    <?=cachedFile('buttons.css','media="screen"'); ?>

    <?=cachedFile('alerts.css','media="screen"'); ?>

    <?=cachedFile('colorbox.css','media="screen"'); ?>

    <?=cachedFile('flexslider.css','media="screen"'); ?>



    

    <!--[if IE 8]>

        <?=cachedFile('stylesIE8.css','media="screen"'); ?>

    <![endif]-->



    <!-- RSS Feeds -->

    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?=BASE_URL; ?>rss/Rss/productBlogFeed" />



    <!-- Print Style Sheet -->

    <?=cachedFile('print.css','media="print"'); ?>

    <?//=cachedFile('sc-style.css','media="screen'); ?>

    <link href="<?php echo base_url(); ?>css/sc-style.css" rel="stylesheet" type="text/css">

    
    <!-- sc added below part for inner header banner ==========================-->
    <style type="text/css">

        #header .headerTextContainer {

            background: url('<?php echo base_url(); ?>images/<?php print $headerTextImage; ?>') 0px 27px no-repeat;

        }
    </style>
    <!--===========================================================================-->

    <?=cachedFile('swfobject.js'); ?>

   <!-- start Mixpanel --><script type="text/javascript">var mpq=[];mpq.push(["init","e7a2e26e7548ee0e68788dbb3fc0d72b"]);(function(){var b,a,e,d,c;b=document.createElement("script");b.type="text/javascript";b.async=true;b.src=(document.location.protocol==="https:"?"https:":"http:")+"//api.mixpanel.com/site_media/js/api/mixpanel.js";a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(b,a);e=function(f){return function(){mpq.push([f].concat(Array.prototype.slice.call(arguments,0)))}};d=["init","track","track_links","track_forms","register","register_once","identify","name_tag","set_config"];for(c=0;c<d.length;c++){mpq[d[c]]=e(d[c])}})();</script><!-- end Mixpanel -->



</head>

<body>

    <header id="main_header" class="main_header">

        <div class="logo_navigation">

            <div class="container">

                <div class="logo">

                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/sc-image/logo.png" alt=""></a>

                </div>

                <span class="toggle-menu"><img src="<?php echo base_url(); ?>images/sc-image/menu-icon.png" alt=""></span>

                <nav class="nav_sec">

                    <ul>

                        <li><a href="<?php echo base_url(); ?>">Home</a></li>

                        <li>

                            <a href="<?php echo base_url(); ?>tour">Tour</a>

                            <!-- <ul>

                                <li><a href="#">Submenu 1</a></li>

                                <li><a href="#">Submenu 2</a></li>

                                <li><a href="#">Submenu 3</a></li>

                                <li><a href="#">Submenu 4</a></li>

                            </ul> -->

                        </li>

                        <li><a href="<?php echo base_url(); ?>extras">Extras</a></li>

                        <li><a href="<?php echo base_url(); ?>pricing">Pricing</a></li>

                        <li><a href="<?php echo base_url(); ?>blog">Blog</a></li>

                        <li><a href="<?php echo base_url(); ?>contact">Contact</a></li>



                    </ul>

                </nav>

                <div class="header_user">
                    <p>
                        <?php if ($this->session->userdata('logged') == 1) { ?>
                            <a href="<?php echo base_url(); ?>dashboard/Dashboard">My Account</a>
                            <a href="<?php echo base_url(); ?>login/logout">Logout</a>
                        <?php }
                        else { ?>
                            <a href="<?php echo base_url(); ?>login">Login</a>
                            <a href="<?php echo base_url(); ?>pricing">Sign up</a>
                        <?php } ?>
                    </p>
                </div>

            </div>
        </div>

    </header><!-- /header -->

        <!-- sc added below part for inner header banner ==========================-->
        <div id="header" class="<?=$headerClass; ?>">
             <?php if ($child == TRUE) { ?>

                <!-- Child page header field -->

                <div class="homeFieldBluePublic">

                    <div class="content">

                        <div class="headerTextContainer"></div>

                    </div>

                </div>

                <?php } 
                else
                { ?>

                <!-- Home page header field -->

                <div class="homeFieldBlue">

                    <div class="content">

                        <div id="homeFieldButtonContainer">

                            <a href="<?php echo base_url(); ?>tour" class="buttonProductTour" title="Product Tour"><span>Product Tour</span></a>

                            <a href="<?php echo base_url(); ?>pricing" class="buttonSignup" title="Pricing and Signup"><span>Pricing and Signup</span></a>

                            <div class="homeClouds"></div>

                        </div>



                        <div class="flexslider">

                        <ul class="slides">

                            <li>

                                <div class="homeBG homeBG1"></div>

                            </li>

                        </ul>

                        </div>

                    </div>

                </div>

            <?php } ?>

        </div>
        <!--===================================================================-->

    <div id="pageBody">
