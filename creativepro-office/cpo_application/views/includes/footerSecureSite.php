

</div>

<div class="pricing_and_signup" style="margin-top:30px;">
    <div class="container">
        <span>Free 30 days trial. Sign up now!</span><a href="<?php echo base_url();?>pricing">Pricing & Signup</a>
    </div>
</div>

<div id="main_footer" class="main_footer">

    <div class="footer_info">

        <div class="container">

            <div class="all_section">

                <div class="each_section">

                    <div class="footer_nav">

                        <h4>Navigation</h4>

                        <ul>

                            <li><a href="<?php echo base_url(); ?>">Home</a></li>

                            <li><a href="<?php echo base_url(); ?>tour">Tour</a></li>

                            <li><a href="<?php echo base_url(); ?>extras">Extras</a></li>

                            <li><a href="<?php echo base_url(); ?>pricing">Pricing & Signup</a></li>

                            <li><a href="<?php echo base_url(); ?>selfhosted">Self-hosted version</a></li>

                            <li class="active"><a href="<?php echo base_url(); ?>login">Login</a></li>

                            <li><a href="<?php echo base_url(); ?>contact">Contact Us</a></li>

                        </ul>

                    </div>

                </div>

                <div class="each_section">

                    <div class="footer_nav">

                        <h4>Quostions?</h4>

                        <ul>

                            <li><a href="<?php echo base_url(); ?>blog">Product Blog</a></li>

                            <li><a href="<?php echo base_url(); ?>selfhosted/faq">Self-hosted FAQ</a></li>

                            <li><a href="<?php echo base_url(); ?>contact">Contact Us</a></li>

                            <li><a href="<?php echo base_url(); ?>refund">Refund Policy</a></li>

                            <li><a href="<?php echo base_url(); ?>terms">Terms of Use</a></li>

                            <li><a href="<?php echo base_url(); ?>privacy">Privacy Policy</a></li>

                        </ul>

                    </div>

                </div>

                <div class="each_section">

                    <div class="social_list">

                        <h4>Product Media</h4>

                        <ul>

                            <li><a target="_blank" href="https://www.facebook.com/pages/CreativePro-Office/114793700316"><img src="<?php echo base_url(); ?>images/sc-image/Facebook.png"><span>Facebook</span></a></li>

                            <li><a target="_blank" href="https://twitter.com/cpohq"><img src="<?php echo base_url(); ?>images/sc-image/Twitter.png"><span>Twitter</span></a></li>

                            <li><a target="_blank" href="http://mycpohq.com/rss/Rss/productBlogFeed"><img src="<?php echo base_url(); ?>images/sc-image/Feed.png"><span>RSS Feeds</span></a></li>

                            <li><a target="_blank" href="http://mycpohq.com/tour"><img src="<?php echo base_url(); ?>images/sc-image/Screen.png"><span>Screencasts</span></a></li>

                        </ul>

                    </div>

                </div>

                <div class="each_section">

                    <div class="footer_nav">

                        <h4>Special Thanks</h4>

                        <ul>

                            <li><a target="_blank" href="http://www.smashingmagazine.com/2009/06/18/freebie-release-quartz-icon-set/">Quartz Icon Set</a></li>

                            <li><a target="_blank" href="https://visualpharm.com/">Visual Pharm Icons</a></li>

                            <li><a target="_blank" href="http://wefunction.com/cgi-sys/suspendedpage.cgi">Function Icon Set</a></li>

                            <li><a target="_blank" href="http://www.pinvoke.com/">Fugue Icon Set</a></li>

                            <li><a target="_blank" href="http://www.woothemes.com/2009/09/woofunction-178-amazing-web-design-icons/">Woo Function Icon Set</a></li>

                            <li><a target="_blank" href="http://mixpanel.com/f/partner"><img src="<?php echo base_url(); ?>images/sc-image/Logo-2.jpg" alt=""></a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="copyright_section">

        <div class="container">

            <img src="<?php echo base_url(); ?>images/sc-image/footer-logo.jpg">

            <span>CreativePro Office is an <a href="http://www.generalcomputingsystem.com/">GC LLC Productions</a> service. © 2003-2019</span>

        </div>

    </div>     

</div>



    <!-- JQuery -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jquery/plugins/jquery.fancybox-1.2.6.packed.js"></script>

    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jquery/plugins/jquery.animateToClass.js"></script>

    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jquery/plugins/jquery.commonAppMethods.js"></script>



    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jsPublicSite.js"></script>

    <?

    if (is_array($jsFileArray)) {

        foreach ($jsFileArray as $jsFile) {

            echo '<script src="'.BASE_URL.'js/'.$jsFile.'" language="JavaScript" type=text/javascript></script>'."\n";

        }

    }

    ?>

    <script language="JavaScript" type="text/javascript">

    /* <![CDATA[ */

        var siteURL = '<?=BASE_URL; ?>';

        var insecureURL = '<?=INSECURE_URL; ?>';

        var thisPage  = '<?=$_SERVER['PHP_SELF']; ?>';



        /*

         * Google Analytics

         */

        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-1427307-6']);

        _gaq.push(['_setDomainName', '.mycpohq.com']);

        _gaq.push(['_trackPageview']);



        (function() {

          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

          var s = document.getElementsByTagName('script')[0];

          s.parentNode.insertBefore(ga, s);

        })();

    /* ]]> */

    </script>

    <?=cachedFile('sc-jquery.js'); ?>

    </body>

    </html>

