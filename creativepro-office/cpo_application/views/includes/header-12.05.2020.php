<?
$randomString = random_string();
if (!isset($pageTitle)) {
    $pageMetaTitle = 'Project Management Dashboard Software to Organize Your Work';
} else {
    $pageMetaTitle = $pageTitle.' : Project Management Dashboard Software to Organize Your Work';
}
?>
<!DOCTYPE html>
<html lang="<?=$this->session->userdata('language'); ?>">
<head>
	<meta charset="utf-8">
	
	<title><?=$pageMetaTitle; ?></title>
	<meta name="Description" content="At Mycpohq, you can easily manage your team, clients, projects, invoices, time sheet from one web-based app with the help of project and task management dashboard software.">
	<meta name="Keywords" content="project management dashboard software,task management software,web based office applications,online project management tools ,online timesheet,online invoice, project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist">
	<meta name="Author" content="Jeff Denton @ GC LLC Productions :: Fairfax VA :: admin@mycpohq.com">
	<META NAME="Rating" CONTENT="General">
	<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
	<META NAME="Distribution" CONTENT="global">
    <META http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon">
	
	<!-- Style Sheets -->
    <?=cachedFile('browserReset.css','media="screen"'); ?>
    <?=cachedFile('alerts.css','media="screen"'); ?>
    <?=cachedFile('buttons.css','media="screen"'); ?>
    <?=cachedFile('colors.css','media="screen"'); ?>
    <?=cachedFile('dashboardWidgets.css','media="screen"'); ?>
    <?=cachedFile('forms.css','media="screen"'); ?>
    <?=cachedFile('icons.css','media="screen"'); ?>
    <?=cachedFile('icons_mime.css','media="screen"'); ?>
    <?=cachedFile('jquery-ui-1.8.23.custom.css','media="screen"'); ?>
    <?=cachedFile('jquery.ui.custom.css','media="screen"'); ?>
    <?=cachedFile('layout.css','media="screen"'); ?>
    <?=cachedFile('menus.css','media="screen"'); ?>
    <?=cachedFile('fullcalendar.css','media="screen"'); ?>
    <?=cachedFile('stylesMaster.css','media="screen"'); ?>
    <?=cachedFile('plupload.queue.css','media="screen"'); ?>
    <?=cachedFile('jquery.wysiwyg.css','media="screen"'); ?>
    <?=cachedFile('autoSuggest.css','media="screen"'); ?>
    
    <?
    $cssAssets = array(
        'css/stylesMaster.css',
        'css/plupload.queue.css',
        'css/jquery.wysiwyg.css'
    );
    //$this->assets->add($cssAssets);
    //echo $this->assets->get_styles();
    ?>

    <? if (isset($charts) && $charts == 1) { ?>
    <?=cachedFile('jquery.jqplot.css','media="screen"'); ?>
    <? } ?>
	
	<!-- Print Style Sheet -->
    <?=cachedFile('print.css','media="print"'); ?>
	
	<!--[if IE]>
        <?=cachedFile('excanvas/excanvas.compiled.js'); ?>
	<![endif]-->
    <!--[if IE 8]>
        <?=cachedFile('stylesIE8.css','media="screen"'); ?>
    <![endif]-->

    <!-- RSS Feeds -->
    <?
    if (isset($rssFeeds) && is_array($rssFeeds)) {
        foreach ($rssFeeds as $feed) {
            echo '<link rel="alternate" type="application/rss+xml" title="'.$feed['Title'].'" href="'.$feed['Link'].'">'.Chr(10);
        }
    }
    ?>
    <!-- End RSS Feeds -->

<!-- SC style -->
<style type="text/css" media="screen">
	*{
	margin: 0;
	padding: 0;
	}
	@font-face {
	font-family: 'taile';
	src: url('<?=BASE_URL; ?>fonts/taile.eot');
	src: url('<?=BASE_URL; ?>fonts/taile.eot') format('embedded-opentype'),
	     url('<?=BASE_URL; ?>fonts/taile.woff2') format('woff2'),
	     url('<?=BASE_URL; ?>fonts/taile.woff') format('woff'),
	     url('<?=BASE_URL; ?>fonts/taile.ttf') format('truetype'),
	     url('<?=BASE_URL; ?>fonts/taile.svg#taile') format('svg');
	}
	@font-face {
	font-family: 'taileb';
	src: url('<?=BASE_URL; ?>fonts/taileb.eot');
	src: url('<?=BASE_URL; ?>fonts/taileb.eot') format('embedded-opentype'),
	     url('<?=BASE_URL; ?>fonts/taileb.woff2') format('woff2'),
	     url('<?=BASE_URL; ?>fonts/taileb.woff') format('woff'),
	     url('<?=BASE_URL; ?>fonts/taileb.ttf') format('truetype'),
	     url('<?=BASE_URL; ?>fonts/taileb.svg#taileb') format('svg');
	}
	img {
	margin: 0;
	padding: 0;
	border: none;
	max-width: 100%;
	height: auto !important;
	display: block;
	}
	.sc_container{
	width: 100%;
	max-width: 1315px;
	margin: 0 auto;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.custom_row{
	margin: 0 -15px;
	box-sizing: border-box;
	}
	.grid_1{
	width: 8.33333333%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_2{
	width: 16.66666667%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_3{
	width: 25%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_4{
	width: 33.33333333%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_5{
	width: 41.66666667%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_6{
	width: 50%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_7{
	width: 58.33333333%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_8{
	width: 66.66666667%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_9{
	width: 75%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_10{
	width: 83.33333333%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_11{
	width: 91.66666667%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.grid_11{
	width: 100%;
	float: left;
	padding: 0 15px;
	box-sizing: border-box;
	}
	.sc_after_login_header{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	}
	.sc_after_login_header .header_top {
	    width: 100%;
	    float: left;
	    margin: 0;
	    padding: 10px 0;
	    box-sizing: border-box;
	    background: #fff;
	}
	.sc_after_login_header .header_top .left_nemu{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	}
	.sc_after_login_header .header_top .left_nemu #menuMain{
		width: 100%;
		float: left;
		margin: 0;
		padding: 0;
	}
	.header_top .left_nemu ul#navmenu-h{
	margin: 0;
	padding: 0;
	text-align: left;
	box-sizing: border-box;
	}
	.header_top .left_nemu ul#navmenu-h li {
	    display: inline-block;
	    vertical-align: middle;
	    margin: 0;
	    padding: 3px 0;
	    box-sizing: border-box;
	    list-style: none;
	}
	.header_top .left_nemu ul#navmenu-h  > li + li{
	margin-left: 20px;
	}
	.header_top .left_nemu ul#navmenu-h li a{
	font-family: 'taileb';
	font-size: 17px;
	color: #101010;
	margin: 0;
	padding: 0;
	text-decoration: none;
	box-sizing: border-box;
	}
	.header_top .left_nemu ul#navmenu-h > li > a:hover,
	.header_top .left_nemu ul#navmenu-h > li:hover > a{
		color: #054b6e;
		background: none;
		border-radius: initial;
	}
	.header_top .left_nemu ul#navmenu-h > li > a:hover span,
	.header_top .left_nemu ul#navmenu-h > li:hover > a span{
		color: #054b6e;
		background: none;
		border-radius: initial;
	}
	.header_top .left_nemu ul#navmenu-h li a span{
		font-family: 'taileb';
		margin: 0;
		padding: 0;
		font-size: 17px;
		line-height: initial;
		color: initial;
	}

	.header_top .left_nemu ul#navmenu-h li ul {
		width: 186px;
		background: #ffeb5e;
		border-radius: 0;
		-moz-box-shadow: 0px 4px 6px #666;
		-webkit-box-shadow: 0px 4px 6px #666;
		box-shadow: 0px 4px 6px #666;
		margin: 0;
		list-style: none;
		display: none;
		position: absolute;
		top: 100%;
		left: 0;
		z-index: 1000;
		padding: 0;
	}

	.header_top .left_nemu ul#navmenu-h > li:hover ul{
		display: block;
	}

	.header_top .left_nemu ul#navmenu-h li ul li{
		width: 100%;
		float: left;
		margin: 0;
		padding: 0;
	}
	.header_top .left_nemu ul#navmenu-h li ul li a{
		font-family: 'taileb';
		font-size: 14px;
		color: #101010;
		display: block;
		margin: 0;
		padding: 5px 8px;
		width: 100%;
		border-radius: 0;
	} 

	.header_top .quick_access_icon {
	    width: 100%;
	    float: left;
	    margin: 1px 0 0;
	    padding: 0;
	    box-sizing: border-box;
	}
	.header_top .quick_access_icon ul{
	margin: 0;
	padding: 0;
	text-align: right;
	box-sizing: border-box;
	width: 100%;
	float: left;
	}
	.header_top .quick_access_icon ul li{
	margin: 0;
	padding: 0;
	display: inline-block;
	vertical-align: middle;
	list-style: none;
	box-sizing: border-box;
	float: none;
	}
	.header_top .quick_access_icon ul li + li{
	margin-left: 10px;
	}
	.sc_after_login_header .login_user_detail{
	width: 100%;
	float: left;
	margin: 0;
	padding: 45px 0;
	background: #f4f4f4;
	box-sizing: border-box;
	}   
	.login_user_detail .info{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	}
	.login_user_detail .info h1{
	font-family: 'taileb';
	font-size: 52px;
	color: #101010;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	}
	.login_user_detail .user_info{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	text-align: right;
	}
	.login_user_detail .user_info p{
	font-family: 'taileb';
	font-size: 21px;
	color: #101010;
	margin: 0;
	padding: 0;
	display: inline-block;
	vertical-align: middle;
	box-sizing: border-box;
	}
	.login_user_detail .user_info img{
	margin: 0 0 0 20px;
	padding: 0;
	display: inline-block;
	vertical-align: middle;
	border-radius: 100%;
	}
	.srarch_and_info_bar{
	width: 100%;
	float: left;
	margin: 0;
	padding: 15px 0;
	box-sizing: border-box;
	/* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#013b6c+0,127972+100 */
	background: #013b6c; /* Old browsers */
	background: -moz-linear-gradient(left,  #013b6c 0%, #127972 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  #013b6c 0%,#127972 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  #013b6c 0%,#127972 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#013b6c', endColorstr='#127972',GradientType=1 ); /* IE6-9 */
	}
	.srarch_and_info_bar .office_dashbord{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	}
	.srarch_and_info_bar .office_dashbord h1.pageTitle {
	    font-family: 'taileb';
	    position: relative;
	    margin: 0;
	    padding: 12px 0 12px 50px;
	    font-size: 17px;
	    color: #fff !important;
	    text-decoration: none;
	    display: table;
	    box-sizing: border-box;
	}
	.srarch_and_info_bar .office_dashbord h1.pageTitle.iconDashboard{
		background: url(<?=BASE_URL; ?>images/sc-image/after-login/icon7.png) no-repeat left;
	}
	.srarch_and_info_bar .office_dashbord a {
	font-family: 'taileb';
	position: relative;
	margin: 0;
	padding: 9px 0 9px 60px;
	font-size: 20px;
	color: #101010;
	color: #fff;
	text-decoration: none;
	display: table;
	box-sizing: border-box;
	}
	.srarch_and_info_bar .office_dashbord img{
	position: absolute;
	top: 0;
	left: 0;
	}
	.srarch_and_info_bar .add_widget{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	}
	.srarch_and_info_bar .add_widget > a, 
	.srarch_and_info_bar .add_widget > button.buttonExpand.action, 
	.srarch_and_info_bar .add_widget > button.buttonExpand span {
	    font-family: 'taileb' !important;
	    position: relative;
	    margin: 0;
	    padding: 13px 0px 13px 0px;
	    font-size: 17px !important;
	    color: #fff !important;
	    text-decoration: none;
	    display: table;
	    box-sizing: border-box;
	    border: 0;
	    float: none;
	    height: auto;
	    border-radius: initial;
	    display: inline-block;
	    outline: none;
	    background: transparent;
	    transition: all 0.3s ease-in-out;
	}
	.srarch_and_info_bar .add_widget > a:first-child, 
	.srarch_and_info_bar .add_widget > button.buttonExpand.action:first-child{
		background: transparent url(<?=BASE_URL; ?>images/sc-image/after-login/icon8.png) no-repeat left;
	    padding: 13px 0 13px 60px;
	}
	.srarch_and_info_bar .add_widget button.buttonExpand:first-child span{
		padding: 0;
		background: none;
	}
	.srarch_and_info_bar .add_widget button.buttonExpand span{
		padding: 0 0 0 12px;
		background: none;
	}
	.srarch_and_info_bar .add_widget > a:hover, 
	.srarch_and_info_bar .add_widget > button.buttonExpand.action:hover, 
	.srarch_and_info_bar .add_widget > button.buttonExpand:hover span {
		color: #F5C322 !important;
	}
	.srarch_and_info_bar .add_widget img{
	position: absolute;
	top: 0;
	left: 0;
	box-sizing: border-box;
	}
	.srarch_and_info_bar .search_area{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	}
	.srarch_and_info_bar .search_area .search_box {
	    width: 100%;
	    float: left;
	    margin: 3px 0 0;
	    padding: 0;
	    position: relative;
	    box-sizing: border-box;
	}
	.srarch_and_info_bar .search_area .search_box input {
	    font-family: 'taile' !important;
	    width: 100%;
	    height: 41px;
	    margin: 0;
	    padding: 0 48px 0 12px !important;
	    font-size: 16px;
	    color: #4b4a4b;
	    background: rgba(255,255,255,0.8);
	    border: 0;
	    box-sizing: border-box;
	    float: none;
	    border-radius: 0 !important;
	}
	.srarch_and_info_bar .search_area .search_box input[type="submit"] {
	background: url(<?=BASE_URL; ?>images/sc-image/after-login/icon9.png)no-repeat center;
	position: absolute;
	top: 0;
	right: 0;
	width: auto;
	color: transparent;
	padding: 0 !important;
	cursor: pointer;
	transition: all 0.3s ease-in-out;
	}
	.iconPageSearch {
    	background: url(<?=BASE_URL; ?>images/icons/iconsearch.png) no-repeat left;
	}
	.iconPageClients {
    	background: url(<?=BASE_URL; ?>images/icons/iconclients.png) no-repeat left;
	}
	.iconPageClients {
    	background: url(<?=BASE_URL; ?>images/icons/iconclients.png) no-repeat left;
	}
	.iconPageProjects, .iconprojects {
    	background: url(<?=BASE_URL; ?>images/icons/iconprojects.png) no-repeat left;
	}
	.iconinvoices {
    	background: url(<?=BASE_URL; ?>images/icons/iconinvoices.png) no-repeat left;
	}
	.iconPageTimesheets {
    	background: url(<?=BASE_URL; ?>images/icons/icontimesheets.png) no-repeat left;
	}
	.iconPageCalendar, .iconcalendar {
    	background: url(<?=BASE_URL; ?>images/icons/iconcalendar.png) no-repeat left;
	}
	.iconPageFile {
    	background: url(<?=BASE_URL; ?>images/icons/iconFileManager.png) no-repeat left;
	}
	.iconPageTeam {
    	background: url(<?=BASE_URL; ?>images/icons/iconteam.png) no-repeat left;
	}
	.srarch_and_info_bar .office_dashbord .pageTitle .pageSubTitle {
	    font: 14pt taileb !important;
	    color: #ffcc00;
	    height: 30px;
	}
	.srarch_and_info_bar .office_dashbord h1 > a {
	    display: inline-block;
	    vertical-align: middle;
	    padding: 0;
	    color: #fff !important;
	}
	.srarch_and_info_bar .office_dashbord h1 > a:hover a{
		color: #ffcc00 !important;
	}
	.sc_after_login_footer{
	width: 100%;
	float: left;
	margin: 0;
	padding: 75px 0 75px;
	background: #f5faf4;
	}
	.sc_after_login_footer .footer_top{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	}
	.sc_after_login_footer .footer_top .common_area{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	}
	.sc_after_login_footer .footer_top h4 {
	font-family: 'taileb' !important;
	font-size: 22px;
	margin: 0 0 16px;
	padding: 0;
	color: #000;
	}
	.sc_after_login_footer .footer_top .common_area ul{
	margin: 0;
	padding: 0;
	}
	.sc_after_login_footer .footer_top .common_area ul li{
	margin: 0 0 5px;
	padding: 0;
	list-style: none;
	}
	.sc_after_login_footer .footer_top .common_area ul li a{
	font-family: 'taile' !important;
	text-decoration: none;
	margin: 0;
	padding: 0;
	font-size: 16px;
	color: #000 !important;
	}
	.sc_after_login_footer .footer_top .common_area ul li a:hover{
	color: #fd9802 !important;
	}
	.sc_after_login_footer .footer_top .social_info{
	width: 100%;
	float: left;
	margin: 0;
	padding: 0;
	}
	.sc_after_login_footer .footer_top .social_info ul{
	margin: 0;
	padding: 0;
	}
	.sc_after_login_footer .footer_top .social_info ul li{
	margin: 0 0 10px;
	padding: 0;
	list-style: none;
	}
	.sc_after_login_footer .footer_top .social_info ul li img{
	display: inline-block;
	vertical-align: middle;
	margin: 0;
	}
	.sc_after_login_footer .footer_top .social_info ul li span{
	font-family: 'taile' !important;
	display: inline-block;
	vertical-align: middle;
	margin: 0 0 0 10px;
	font-size: 16px;
	color: #000;
	}

	.sc_after_login_footer .footer_bottom {
	    width: 100%;
	    margin: 0;
	    padding: 0;
	    text-align: center;
	    position: fixed;
	    bottom: 0;
	    left: 0;
	    background: #f5faf4;
	}
	.sc_after_login_footer .footer_bottom img {
	    display: inline-block;
	    vertical-align: middle;
	    margin: 0;
	    padding: 0;
	    width: 130px;
	}
	.sc_after_login_footer .footer_bottom p{
	font-family: 'taile' !important;
	display: inline-block;
	vertical-align: middle;
	margin: 0;
	font-size: 14px;
	}
	.sc_after_login_footer .footer_bottom a{
	text-decoration: none;
	color: #2ba4cf;
	}
	.srarch_and_info_bar .add_widget .buttonExpand.green {
		background: url(<?=BASE_URL; ?>images/bgButtonGreen.png) repeat-x;
		border: 1px solid #90c223;
		display: inline-block;
		float: none;
		margin: 0 0 0 20px;
	}

	@media only screen and (max-width: 1199px){
	.header_top .quick_access_icon ul li + li {
	    margin-left: 10px;
	}
	}

	@media only screen and (max-width: 991px){
	.header_top .custom_row .grid_8,
	.header_top .custom_row .grid_4{
	    width: 100%;
	}
	.header_top .left_nemu ul,
	.header_top .quick_access_icon ul{
	    text-align: center;
	}
	.header_top .quick_access_icon {
	    margin: 10px 0 0;
	}
	.srarch_and_info_bar .custom_row .grid_4{
	    width: 50%;
	}
	.srarch_and_info_bar .custom_row .grid_4:last-child{
	    width: 100%;
	}
	.srarch_and_info_bar .search_area {
	    float: none;
	    margin: 20px auto 0;
	    max-width: 520px;
	}
	.login_user_detail .custom_row  .grid_6{
	    width: 100%;
	}
	.login_user_detail .info {
	    text-align: center;
	}
	.login_user_detail .user_info {
	    text-align: center;
	}
	.sc_after_login_header .login_user_detail {
	    padding: 25px 0;
	}
	}
	@media only screen and (max-width: 768px){
	.sc_after_login_footer .footer_top .custom_row .grid_3{
	    width: 50%;
	}
	.sc_after_login_footer {
	    padding: 42px 0;
	}
	.sc_after_login_footer .footer_top .common_area {
	    min-height: 255px;
	}
	}
	@media only screen and (max-width: 640px){
	.header_top .left_nemu ul li a {
	    font-size: 14px;
	}
	.header_top .left_nemu ul li + li {
	    margin-left: 10px;
	}
	.login_user_detail .info h1 {
	    font-size: 35px;
	}
	.login_user_detail .user_info p {
	    font-size: 16px;
	}
	.srarch_and_info_bar .add_widget a {
	    font-size: 16px;
	}
	.srarch_and_info_bar .office_dashbord a{
	    font-size: 16px;
	}
	.srarch_and_info_bar .custom_row .grid_4{
	    width: 100%;
	}
	.srarch_and_info_bar .office_dashbord {
	    margin: 0 0 15px;
	}
	}
	@media only screen and (max-width: 520px){
	.header_top .left_nemu ul li{
	    padding: 0;
	}
	.sc_after_login_footer .footer_top .custom_row .grid_3{
	    width: 100%;
	}
	.sc_after_login_footer .footer_top .common_area,
	.sc_after_login_footer .footer_top .social_info{
	    min-height: initial;
	    margin: 0 0 20px;
	}
	.sc_after_login_footer .footer_bottom {
	    margin: 0;
	}
	.sc_after_login_footer .footer_top h4 {
	    font-size: 20px;
	    margin: 0 0 8px;
	}
	}
</style>

</head>

<?
if ($this->session->userdata('logged') == TRUE) {
    $loggedClass = 'logged';
} else {
    $loggedClass = '';
}

if ($this->session->userdata('headerStyle')) {
    $headerStyle = $this->session->userdata('headerStyle');
} else {
    $headerStyle = 'default';
}
?>

<body id="<?=$pageLayoutClass; ?>" class="page<?=$page; ?><?=$loggedClass; ?>">
    <? if ($this->session->userdata('userType') == USER_TYPE_CLIENT) { ?>
    <input type="hidden" name="clientArea" id="clientArea" value="1" />
    <input type="hidden" name="clientID" id="clientID" value="<?=$this->session->userdata('clientID'); ?>" />
    <input type="hidden" name="clientUserid" id="clientUserid" value="<?=$this->session->userdata('userid'); ?>" />
    <? } ?>
	


<? /*
    <div id="header" class="noPrint">
        <div class="blackBar">
            <div class="content">
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                    <?
                    if ($this->session->userdata('userType') != USER_TYPE_CLIENT) {
                        $this->load->view('includes/menuMain');
                    }
                    ?>
                    <div id="headerControls">
                        <div style="float: right;">
                            <ul class="smallButtonCluster">
                                <? if ($this->session->userdata('userType') == USER_TYPE_CLIENT) { ?>
                                <li><a href="<?=site_url('dashboard/DashboardClients'); ?>" title="<?=lang('dashboard_page_title'); ?>" class="headerButton"><span class="dashboard"></span></a></li>
                                <? } else { ?>
                                <li><a href="<?=site_url('dashboard/DashboardOwners'); ?>" title="<?=lang('dashboard_page_title'); ?>" class="headerButton"><span class="dashboard"></span></a></li>
                                <? } ?>
                                
                                <li><a href="<?=site_url('settings/Settings'); ?>" title="<?=lang('settings_page_title'); ?>" class="headerButton"><span class="settings"></span></a></li>
                                
                                <li><a href="<?=site_url('myaccount/MyAccount'); ?>" class="headerButton" title="<?=lang('common_your_account'); ?>"><span class="account"></span></a></li>
                                
                                <li><a href="#" title="<?=lang('common_language'); ?>" class="headerButton popUpMenuClick" content="contentLanguage" id="headerButtonLanguage"><span class="language"></span></a></li>
                                
                                <? if ($this->session->userdata('userType') == USER_TYPE_GOD) { ?>
                                <li><a href="#" title="<?=lang('common_help'); ?>" class="headerButton" content="contentHelp" id="headerButtonHelp"><span class="helpbutton"></span></a></li>
                                <? } ?>
                                
                                <li><a href="#" title="<?=lang('bugreport_page_title'); ?>" class="headerButton popUpMenuClick" content="contentBugs" id="headerButtonBugs"><span class="bugs"></span></a></li>
                                
                                <? if ($this->session->userdata('userType') == USER_TYPE_GOD) { ?>
                                <li><a href="<?=site_url('administration/AdminUsers'); ?>" title="Office Administration" class="headerButton"><span class="admin"></span></a></li>
                                <? } ?>
                                
                                <li><a href="<?=site_url('login/logout'); ?>" title="<?=lang('login_logout'); ?>" class="headerButton"><span class="logout"></span></a></li>
                            </ul>
                            
                            <div style="clear: left;"></div>

                            <div id="contentBugs" class="tooltip">
                                <?=lang('bugreport_instructions'); ?>
                                <textarea class="bugReportContent" style="width: 270px; height: 75px;"></textarea>
                                <button class="smallButton buttonSubmitBugReport" style="float: left;"><span class="decorator bug"><?=lang('bugreport_submit'); ?></span></button>
                                <p style="float: left; margin-left: 12px; width: 0px; display: none;" class="successMessageSmall"><?=lang('bugreport_success'); ?></p>
                            </div>
                            <div id="contentHelp" class="tooltip">Help</div>
                            <div id="contentLanguage" class="tooltip">
                            <?
                            $languages = languages();
                            echo '<ul class="language">';

                            foreach($languages as $key => $value) {
                                $activeState = '';
                                if ($key == $this->session->userdata('language')) {
                                    $activeState = 'active';
                                }
                                echo '<li class="'.$activeState.'"><a href="#" id="lang_'.$key.'" class="languageSelect">'.$value.'</a></li>';
                            }
                            echo '</ul>';
                            ?>
                            </div>
                        <div id="headerControlsMessage"></div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                <? } else { ?>
                    <p style="color: #fff; font-size: 16px; font-weight: bold; padding-top: 6px; text-align: center;">CreativePro Office is a complete set of online office management tools for creative professionals.</p>
                <? } ?>
            </div>
        </div>
        

        <div class="field <?=$headerStyle; ?>">
            <div class="content <?=$headerStyle; ?>">
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                <div class="accountIdentityContainer">
                    <?
                    $accountLogo = @fopen($this->session->userdata('logoLowRes'),'r');

                    if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
                        $logoURL = site_url('dashboard/DashboardClients');
                    } else {
                        $logoURL = site_url('dashboard/DashboardOwners');
                    }
                    if ($accountLogo == TRUE) {
                        $logoSizeArray = imageResize($this->session->userdata('logoLowRes'),NULL,92,FALSE);
                        $marginTop = floor((104-$logoSizeArray[1])/2);
                        echo '<div style="margin-top: '.$marginTop.'px;"><a href="'.$logoURL.'" title="'.lang('dashboard_page_title').': '.$this->session->userdata('userCompany').'"><img src="'.$this->session->userdata('logoLowRes').'" width="'.$logoSizeArray[0].'" height="'.$logoSizeArray[1].'" border="0" /></a></div>';
                    } else {
                        echo '<a href="'.$logoURL.'" title="'.lang('dashboard_page_title').'"><span>'.$this->session->userdata('userCompany').'</span></a>';
                    }
                    ?>
                </div>
                <div class="userAvatarContainer">
                    <p><? echo $this->session->userdata('fullName').', '.$this->session->userdata('roleText'); ?></p>
                    <?
                    $avatarMember = $this->session->userdata('avatarMember');
                    if (!empty($avatarMember)) { ?>
                        <img class="avatar22 default" src="<?=$this->session->userdata('avatarMember'); ?>" />
                    <? } else { ?>
                        <p class="avatar22 default"></p>
                    <? } ?>
                    <div style="float: left;"></div>
                </div>
                <? }  else { ?>
                    <center><div id="logoCPOTop"></div></center>                    
                <? } ?>
            </div>
        </div>


        <div class="greenBar">
            <div class="content">
                <div id="pageTitleContainer">
                    <h1 class="pageTitle <?=$pageIconClass; ?>">
                    <?
                    if (isset($pageTitleLink)) {
                        echo '<a href="'.$pageTitleLink.'" title="'.$pageTitle.'">'.$pageTitle.'</a>';
                    } else {
                        echo $pageTitle;
                    }
                    if (!empty($pageSubTitle)) {
                        echo ' / <span class="pageSubTitle" id="pageSubTitle">'.$pageSubTitle.'</span>';
                    }
                    ?>
                    </h1>
                </div>
                <div id="pageControls">
                    <div id="pageControlsContainer" style="float: right; margin-right: 6px;">
                        <?
                        if (isset($pageButtons)) {
                            foreach($pageButtons as $button) {
                                echo $button;
                            }
                        }
                        ?>
                    </div>&nbsp;
                </div>
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                <div id="siteSearchContainer">
                    <form action="<?=site_url('search/SiteSearch'); ?>" method="POST" id="formSiteSearch">
                    <input type="text" class="appSearchBox clearOnFocus" id="appSearch" name="appSearch" value="<?=lang('common_search_office'); ?>" />
                    <button class="buttonAppSearch" id="buttonAppSearch"></button>
                    </form>
                </div>
                <? } ?>
            </div>
        </div>
	</div>	
*/ ?>


    <div class="sc_after_login_header noPrint">
        <div class="header_top">
            <div class="sc_container">
                <div class="custom_row">
                    <? if ($this->session->userdata('logged') == TRUE) { ?>
                        <div class="grid_8">
                            <div class="left_nemu">
                                <?php if ($this->session->userdata('userType') != USER_TYPE_CLIENT) 
                                { $this->load->view('includes/menuMain'); } ?>
                            </div>
                        </div>
                        <div class="grid_4">
                            <div class="quick_access_icon">
                                <ul class="smallButtonCluster">
                                    <? if ($this->session->userdata('userType') == USER_TYPE_CLIENT) { ?>
                                    <li><a href="<?=site_url('dashboard/DashboardClients'); ?>"><span class="dashboard"></span></a></li>
                                    <? } else { ?>
                                    <li><a href="<?=site_url('dashboard/DashboardOwners'); ?>"><span class="dashboard"></span></a></li>
                                    <? } ?>

                                    <li><a href="<?=site_url('settings/Settings'); ?>"><span class="settings"></a></li>

                                    <li><a href="<?=site_url('myaccount/MyAccount'); ?>"><span class="account"></span></a></li>

                                    <li><a href="#" title="<?=lang('common_language'); ?>" class="popUpMenuClick" content="contentLanguage" id="headerButtonLanguage"><span class="language"></span></a></li>

                                    <? if ($this->session->userdata('userType') == USER_TYPE_GOD) { ?>
	                                <li><a href="#" title="<?=lang('common_help'); ?>" content="contentHelp" id="headerButtonHelp"><span class="helpbutton"></span></a></li>
	                                <? } ?>

                                    <li><a href="#" title="<?=lang('bugreport_page_title'); ?>" class="popUpMenuClick" content="contentBugs" id="headerButtonBugs"><span class="bugs"></span></a></li>

                                    <? if ($this->session->userdata('userType') == USER_TYPE_GOD) { ?>
	                                <li><a href="<?=site_url('administration/AdminUsers'); ?>" title="Office Administration"><span class="admin"></span></a></a></li>
	                                <? } ?>

                                    <li><a href="<?=site_url('login/logout'); ?>"><span class="logout"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    <? } else { ?>
                        <div class="grid_12">
                            <p>CreativePro Office is a complete set of online office management tools for creative professionals.</p>
                        </div>
                    <? } ?>
                </div>
                <div id="contentBugs" class="tooltip">
                    <?=lang('bugreport_instructions'); ?>
                    <textarea class="bugReportContent" style="width: 270px; height: 75px;"></textarea>
                    <button class="smallButton buttonSubmitBugReport" style="float: left;"><span class="decorator bug"><?=lang('bugreport_submit'); ?></span></button>
                    <p style="float: left; margin-left: 12px; width: 0px; display: none;" class="successMessageSmall"><?=lang('bugreport_success'); ?></p>
                </div>
                <div id="contentHelp" class="tooltip">Help</div>
                <div id="contentLanguage" class="tooltip">
                    <? $languages = languages();
                    echo '<ul class="language">';

                    foreach($languages as $key => $value) {
                        $activeState = '';
                        if ($key == $this->session->userdata('language')) {
                            $activeState = 'active';
                        }
                        echo '<li class="'.$activeState.'"><a href="#" id="lang_'.$key.'" class="languageSelect">'.$value.'</a></li>';
                    }
                    echo '</ul>'; ?>
                </div>
                <div id="headerControlsMessage"></div>
            </div>
        </div>
        <div class="login_user_detail">
            <div class="sc_container">
                <div class="custom_row">
                    <div class="grid_6">
                        
                        <? $accountLogo = @fopen($this->session->userdata('logoLowRes'),'r');

	                    if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
	                        $logoURL = site_url('dashboard/DashboardClients');
	                    } else {
	                        $logoURL = site_url('dashboard/DashboardOwners');
	                    }

	                    if ($accountLogo == TRUE) {
                        $logoSizeArray = imageResize($this->session->userdata('logoLowRes'),NULL,92,FALSE); ?>
                        	<div class="info"><a href="<? echo $logoURL; ?>"><img src="<? echo $this->session->userdata('logoLowRes'); ?>" width="<? echo $logoSizeArray[0]; ?>" height="<? echo $logoSizeArray[1]; ?>" border="0" /></a></div> <?
                        } else { ?>
                        	<div class="info"><a href="<? echo $logoURL; ?>"><h1><? echo $this->session->userdata('userCompany'); ?></h1></a></div> <?
                        } ?>

                    </div>
                    <div class="grid_6">
                        <div class="user_info">
                            <p><? echo $this->session->userdata('fullName').', '.$this->session->userdata('roleText'); ?></p>
                            <? $avatarMember = $this->session->userdata('avatarMember');
                    		if (!empty($avatarMember)) { ?><img src="<?=$this->session->userdata('avatarMember'); ?>" alt=""><? }
                    		else { ?><p class="avatar22 default"></p><? } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="srarch_and_info_bar">
            <div class="sc_container">
                <div class="custom_row">
                    <div class="grid_4">
                        <div class="office_dashbord">
                            <h1 class="pageTitle <?=$pageIconClass; ?>">
		                    <?
		                    if (isset($pageTitleLink)) {
		                        echo '<a href="'.$pageTitleLink.'" title="'.$pageTitle.'">'.$pageTitle.'</a>';
		                    } else {
		                        echo $pageTitle;
		                    }
		                    if (!empty($pageSubTitle)) {
		                        echo ' / <span class="pageSubTitle" id="pageSubTitle">'.$pageSubTitle.'</span>';
		                    }
		                    ?>
		                    </h1>
                        </div>
                    </div>
                    <div class="grid_4">
                        <div class="add_widget">
                            <?
	                        if (isset($pageButtons)) {
	                            foreach($pageButtons as $button) {
	                                echo $button;
	                            }
	                        }
	                        ?>
                        </div>
                    </div>
                    <div class="grid_4">
                        <? if ($this->session->userdata('logged') == TRUE) { ?>
                        <div class="search_area">
                            <form action="<?=site_url('search/SiteSearch'); ?>" method="POST" id="formSiteSearch">
                                <div class="search_box">
                                    <input type="text" class="appSearchBox clearOnFocus" id="appSearch" name="appSearch" value="<?=lang('common_search_office'); ?>" placeholder="Serach Your Office" />
                                    <input type="submit" value="search" name="buttonAppSearch">
                                </div>
                            </form>
                        </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    



	<div id="mainPageArea">
		<div id="contentArea" class="contentArea">
            <div id="fileViewContainer">
                <div class="barYellow bottom"><div style="padding: 6px;">
                    <button class="smallButton" id="closeFileView" style="float: left;" title="<?=lang('common_back'); ?>"><span class="back"><?=lang('common_back'); ?></span></button>
                    <button class="smallButton" id="downloadFileView" style="float: right;" title="<?=lang('common_download_file'); ?>"><span class="download"><?=lang('common_download_file'); ?></span></button>
                    <div style="clear: both;"></div>
                </div></div>
                <div id="fileViewInfomationContainer"></div>
                <div id="newMessageFile" itemID="" itemType="attachedFile" class="messageContainer"></div>
                <div id="messageListFile" style="margin-top: 6px;"></div>
            </div>
