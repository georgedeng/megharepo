<div class="whiteBoxContent_300">
    <form action="http://www.mycpohq.com/helpdesk/search" method="POST" class="searchHelpdeskForm">
        <input type="text" class="searchBlog" name="searchTerm" id="helpdeskSearch" value="Search..." />
        <button class="buttonAppSearch"></button>
    </form>
    <div style="clear: both; height: 9px;"></div>
    <h2 class="brown mBottom">Topics</h2>
    <ul class="arrow1 mBottom" style="margin-left: 25px;">
    <?
    foreach($helpdeskNavTopics as $navTopic) {
        if ($navTopic['tagCount']>0) {
            echo '<li><a href="'.BASE_URL.'helpdesk/tag/'.$navTopic['TagURL'].'">'.$navTopic['TagName'].'</a> <span class="gray2">[ '.$navTopic['tagCount'].' ]</span></li>';
        }
    }
    ?>
    </ul>

    <h2 class="brown mBottom">Recent Entries</h2>
    <ul class="arrow1 mBottom" style="margin-left: 25px;">
    <?
    foreach($helpdeskRecentEntries as $navEntry) {
        echo '<li><a href="'.BASE_URL.'helpdesk/entry/'.$navEntry['EntryURL'].'">'.$navEntry['Title'].'</a></li>';
    }
    ?>
    </ul>
</div>
