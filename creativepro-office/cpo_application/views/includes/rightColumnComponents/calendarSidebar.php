<h4 class="icon_calendar_agenda" id="labelAgenda"></h4>
<div id="containerAgenda" style="margin-left: 17px;"></div>

<h4 class="icon_calendar_small" style="margin-top: 12px;"><?=lang('calendar_your_calendars_short'); ?></h4>
<div id="containerYourCalendars" style="margin-left: 17px;"></div>
<button id="linkCalendarAdd" class="smallButton calendarCreate hide" style="margin: 6px 0 0 16px;"><span class="calendarAdd"><?=lang('calendar_new_calendar'); ?></span></button>

<div id="panelCalendarEdit" style="display: none;">
	<input type="hidden" id="calendarEditColor" />
	<input type="hidden" id="calendarEditID" />
    <input type="hidden" id="calendarOriginalColor" />

	<p style="margin: 3px 0 3px 0;"><strong><?=lang('calendar_name'); ?></strong><br />
	<input type="text" style="width: 200px;" tabindex="200" id="calendarEditTitle" /></p>

	<p style="margin: 3px 0 3px 0;"><strong><?=lang('calendar_select_color'); ?></strong><br />
	<?
	$tabindex = 201;
	foreach($colorCSSArray as $key => $value) {
		echo '<p class="colorBlock '.$key.' big calendarColorSwatch" tabindex="'.$tabindex.'" color="'.$key.'" style="cursor: pointer;"></p>';
		$tabindex++;
	}
	?>
	</p>
	<p class="mBottom12" id="containerCalendarTeamMembers"><strong><?=lang('team_select_team_members'); ?></strong><br />
        <input type="text" style="width: 200px;" class="autocompleteContactsCalendar" id="sTeamAutocompleter" />
    </p>
    <button id="buttonSaveCalendar" class="smallButton" tabindex="300" style="margin-right: 6px; float: left;"><span class="save"><?=lang('button_save'); ?></span></button>
    <button id="buttonCancelCalendar" class="smallButton" tabindex="301" style="margin-right: 6px; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
    <button id="buttonDeleteCalendar" class="smallButton" tabindex="302" style="display: none; margin-right: 6px; float: left;"><span class="single delete"></span></button>
    <div style="clear: left;"></div>
</div>

<div style="height: 12px;"></div>