<div id="filterContainer">
    <h4 class="icon_filter" id="filterHeader"><?=lang('common_filter_by'); ?></h4>
    <div id="invoiceFilterContainer" style="margin: 0 0 12px 12px; display: none;">
        <p><input type="checkbox" class="filterInvoiceCheck" id="filterInvoiceAll" value="all" /> <?=lang('common_all'); ?></p>
        <p><input type="checkbox" class="filterInvoiceCheck" id="filterInvoiceOpen" value="open" /> <?=lang('common_open'); ?></p>
        <p><input type="checkbox" class="filterInvoiceCheck" id="filterInvoiceClosed" value="closed" /> <?=lang('common_closed'); ?></p>
        <p><input type="checkbox" class="filterInvoiceCheck" id="filterInvoicePastDue" value="past due" /> <?=lang('common_past_due'); ?></p>
        <input type="text" id="invoiceSearchDateStart" name="invoiceSearchDateStart" class="dateField" value="01/01/<?=date('Y'); ?>" style="width: 75px; margin-right: 6px;" />
        <input type="text" id="invoiceSearchDateEnd" name="invoiceSearchDateEnd" class="dateField" value="12/31/<?=date('Y'); ?>" style="width: 75px;" />
        <button class="smallButton" id="buttonInvoiceFilter" style="width: 30px; margin-top: 6px;" title="<?=lang('common_filter_by'); ?>"><span class="filter"></span></button>
    </div>    
</div>

