<div style="display: none;" id="sidebarTasks">
	<div id="taskStats" style="float: left; margin-left: -6px; width: 100px;"></div>
	<div id="taskStatsPie" style="margin-left: 6px; float: left;"></div>
	<div style="clear: left;"></div>
	<div style="margin: -10px 0 12px -10px;">
		<p><input type="checkbox" id="chkViewMyTasks" value="1" /> <label for="chkViewMyTasks" class="normal clickable"><?=lang('task_view_my_tasks'); ?></label></p>
		<p><input type="checkbox" id="chkViewCompletedTasks" value="1" /> <label for="chkViewCompletedTasks" class="normal clickable"><?=lang('task_view_complete_tasks'); ?></label></p>
		<p><input type="checkbox" id="chkViewIncompleteTasks" value="1" /> <label for="chkViewIncompleteTasks" class="normal clickable"><?=lang('task_view_incomplete_tasks'); ?></label></p>
	</div>
</div>