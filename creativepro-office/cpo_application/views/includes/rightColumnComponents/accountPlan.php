<div id="accountPlanContainer" class="hide">
    <h4 class="icon_briefcase" style="margin-bottom: 12px;">Your account plan: <?=$accountName; ?></h4>
    <?=$accountDesc; ?>

    <button class="buttonExpand blueGray" id="buttonUpdateAccount" style="margin-top: 12px;"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator accountUpdate"><?=lang('plan_update_plan'); ?></span></span></button>
    <div style="clear: left;"></div>
    <div id="changeCCContainer" style="margin-left: -6px;">
        <hr />
        <button class="buttonExpand green" id="buttonUpdateCCInfo" style="margin-top: 12px;"><span class="buttonOuterSpan green"><span class="buttonDecorator creditCardEdit"><?=lang('plan_update_cc'); ?></span></span></button>
    </div>
</div>
