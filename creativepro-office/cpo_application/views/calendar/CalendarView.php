<? $this->load->view('includes/header'); ?>

<div id="pageLeftColumn" pageID="viewCalendar">

	<script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var calendars = <?=$calendars; ?>;
	/* ]]> */
	</script>

    <input type="hidden" id="masterMonth" value="<?=$defaultMonth; ?>" />
    <input type="hidden" id="masterYear" value="<?=$defaultYear; ?>" />

	<div id="formEventContainer" style="display: none;">
		<form class="greyForm" id="formEvent" name="formEvent">
			<input type="hidden" name="eventID" id="eventID" />
			<input type="hidden" name="action" id="action" value="add" />
            <input type="hidden" name="tagHolder" id="tagHolder" />
			<p>
				<label class="required" for="eventTitle"><?=lang('common_title'); ?></label>
				<input type="text" id="eventTitle" name="eventTitle" class="saverField" tabindex="1" style="width: 400px;" />
				<?=form_error('eventTitle','<span class="formError">','</span>'); ?>
			</p>
			<p>
				<label class="required" for="eventDateStart"><?=lang('common_when'); ?></label>
				<input type="text" id="eventDateStart" class="eventDates saverField" tabindex="2" style="width: 80px;" />
				<input type="text" id="eventTimeStart" class="clearOnFocus saverField" tabindex="3" style="width: 60px; display: none;" value="8:00 AM" />
				to
				<input type="text" id="eventDateEnd" class="eventDates saverField" tabindex="4" style="width: 80px;" />
				<input type="text" id="eventTimeEnd" class="clearOnFocus saverField" tabindex="5" style="width: 60px; display: none;" value="5:00 PM" />
				<input type="checkbox" id="eventAllDay" checked="checked" tabindex="6" /> <?=lang('calendar_all_day'); ?>
 				<?=form_error('projectTitle','<span class="formError">','</span>'); ?>
			</p>
			<p>
				<label><?=lang('widget_calendar'); ?></label>
				<select id="eventCalendar" style="width: 200px;" tabindex="7"></select>
			</p>
            <!--
			<p>
				<label><?=lang('calendar_repeats'); ?></label>
				<select id="eventRepeats" name="eventRepeats" style="width: 200px;" tabindex="8">
					<option value="0"><?=lang('calendar_repeats_none'); ?></option>
					<option value="day"><?=lang('calendar_repeats_day'); ?></option>
					<option value="week"><?=lang('calendar_repeats_week'); ?></option>
					<option value="month"><?=lang('calendar_repeats_month'); ?></option>
					<option value="year"><?=lang('calendar_repeats_year'); ?></option>
				</select><br />
				<input type="checkbox" id="eventReminder" style="margin: 6px 0 0 265px;" tabindex="9" /> <label class="normal" for="eventReminder"><?=lang('calendar_reminder'); ?></label>
			</p>
            -->
			<p>
				<label><?=lang('common_location'); ?></label>
				<textarea id="eventLocation" name="eventLocation" tabindex="10" style="width: 400px; height: 20px;" class="expanding"></textarea>
			</p>
			<p>
				<label><?=lang('task_description'); ?></label>
				<textarea tabindex="11" name="eventDescription" id="eventDescription" style="width: 425px; height: 100px;"></textarea>
			</p>
            
            <div style="float: left;">
                <div class="noBorder" style="margin: 6px 19px 0 0;">
                    <label><?=lang('client_form_tags'); ?></label>
                    <div style="float: right;">
                        <input type="text" name="calendarTags" id="calendarTags" tabindex="12" />
                    </div>
                </div>
            </div>
            <div style="float: left;">
                <?=help('help_formfield_tags','clientTags','iconHelpFormOutside'); ?>
            </div>
            <div style="clear: left;"></div>
            
            <div style="float: left;">
				<div class="noBorder" style="margin: 6px 19px 0 0;">
					<label><?=lang('common_add_guests'); ?></label>
                    <div style="float: right;">
                        <input type="text" style="width: 400px;" class="autocompleteContactsEvent" id="sContactsAutocompleter" />
                    </div>
                </div>
			</div>
			<div style="float: left;">
				<?=help('help_add_guests_calendar','inviteGuests','iconHelpFormOutside'); ?>
			</div>
			<div style="clear: left;"></div>

			<div class="buttonContainer">
				<button class="buttonExpand blueGray action primary" id="buttonSave" tabindex="20"><span class="save"><?=lang('button_save'); ?></span></button>
				<button class="buttonExpand yellow cancel secondary" id="buttonCancel" tabindex="21"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
				<div style="clear: left;"></div>
			</div>
		</form>
	</div>

    <div style="display: none;" id="formCalendarPopup">
		<input type="hidden" class="eventWhenPopupHidden" />
        <input type="hidden" id="dateStartPopup" />
        <input type="hidden" id="dateEndPopup" />
        <input type="hidden" id="allDay" />
		<table class="layout" style="margin: 12px;">
			<tr>
				<td style="text-align: right;"><strong><?=lang('common_when'); ?>:</strong></td>
				<td>
					<strong><span id="dateStartPopupText"></span></strong>&nbsp;at&nbsp;
					<input type="text" id="eventTimeStartPopup" class="eventTimeStartPopup clearOnFocus saverField" tabindex=99" style="width: 60px;" />
                    &nbsp;to&nbsp;
                    <strong><span id="dateEndPopupText"></span></strong>&nbsp;at&nbsp;
					<input type="text" id="eventTimeEndPopup" class="eventTimeStartPopup clearOnFocus saverField" tabindex=99" style="width: 60px;" />

                </td>
			</tr>
			<tr>
				<td style="text-align: right;"><strong><?=lang('common_title'); ?>:</strong></td>
				<td><input type="text" id="eventTitlePopup" class="eventTitlePopup saverField" tabindex="100" style="width: 296px;" /></td>
			</tr>
			<tr>
				<td style="text-align: right;"><strong><?=lang('widget_calendar'); ?>:</strong></td>
				<td>
					<select id="eventCalendarPopup" class="eventCalendarPopup saverField" style="width: 304px;" tabindex="101"></select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td style="padding-left: 8px;">
                    <button class="buttonExpand buttonSavePopup action" id="buttonSavePopup" tabindex="102" style="margin: 6px 12px 0 -8px;"><span class="save"><?=lang('button_save'); ?></span></button>
                    <a href="#" id="linkMoreDetails" class="linkMoreDetails icon_calendar_edit" tabindex="103" style="display: inline-block; margin: 11px 0 0 0;"><?=lang('calendar_more_details'); ?></a>
				</td>
			</tr>
		</table>
	</div>

    <div id="calendarFullContainer">
        <div class="barYellow top">
            <div style="padding: 6px;">
                <h1 style="float: left;" id="calTitle"></h1>
                <div id="test"></div>
                <div style="float: right;">
                    <ul class="smallButtonCluster">
                        <li><a href="#" id="viewMonth" title="<?=lang('calendar_month_view'); ?>"><span class="monthView"></span></a></li>
                        <li><a href="#" id="viewWeek" title="<?=lang('calendar_week_view'); ?>"><span class="weekView"></span></a></li>
                        <li><a href="#" id="viewDay" title="<?=lang('calendar_daily_view'); ?>"><span class="dayView"></span></a></li>
                        <li style="margin-right: 12px;"><a href="#" id="viewToday" title="<?=lang('calendar_today_view'); ?>"><span class="todayView"></span></a></li>
                        <li><a href="#" id="viewPrev" title="<?=lang('common_previous'); ?>"><span class="prev"></span></a></li>
                        <li><a href="#" id="viewNext" title="<?=lang('common_next'); ?>"><span class="next"></span></a></li>
                    </ul>
                    <select id="monthSelect" style="width: 70px; margin: 0 2px 0 6px;">
                        <option value="0"><?=lang('cal_jan'); ?></option>
                        <option value="1"><?=lang('cal_feb'); ?></option>
                        <option value="2"><?=lang('cal_mar'); ?></option>
                        <option value="3"><?=lang('cal_apr'); ?></option>
                        <option value="4"><?=lang('cal_may'); ?></option>
                        <option value="5"><?=lang('cal_jun'); ?></option>
                        <option value="6"><?=lang('cal_jul'); ?></option>
                        <option value="7"><?=lang('cal_aug'); ?></option>
                        <option value="8"><?=lang('cal_sep'); ?></option>
                        <option value="9"><?=lang('cal_oct'); ?></option>
                        <option value="10"><?=lang('cal_nov'); ?></option>
                        <option value="11"><?=lang('cal_dec'); ?></option>
                    </select>
                    <select id="yearSelect" style="width: 70px;">
                        <?
                        $year      = date('Y');
                        $yearStart = $year-10;
                        $yearEnd   = $year+10;
                        for($a=$yearEnd;$a>=$yearStart;$a--) {
                            echo '<option value="'.$a.'"';
                            if ($a == $year) {
                                echo ' selected="selected" ';
                            }
                            echo '>'.$a.'</options>';
                        }
                        ?>
                    </select>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div id="calendarContainer">&nbsp;</div>
    </div>

	<div style="display: none;" id="eventPopupContainer"></div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
