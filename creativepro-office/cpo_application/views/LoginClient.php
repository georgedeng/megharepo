<? $this->load->view('includes/header'); ?>
<div id="loginBox" pageID="clientLogin">
	<div class="windowFrame" style="margin-bottom: 10px;">
		<div id="messageContainer" class="boxYellowInner" style="margin: 6px 6px 18px 6px;">
			<?=$message; ?>
		</div>
		
		<div style="float: left; width: 378px;">
			<form id="formLoginClient" class="noBorder" action="<?=site_url('Login/tryLoginClient'); ?>" method="POST">
				<p>
				<label style="width: 120px;">Client user name</label>	
				<input type="text" style="width: 200px;" id="userid" name="userid" />
				</p>
				<div style="margin: 4px 0 10px 126px;">
                         <button class="buttonExpand blueGray" id="buttonLoginClient"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator login"><?=lang('login_button'); ?></span></span></button>
                    </div>
			</form>
		</div>
		<div style="float: left; margin-top: 5px;">
			<ul class="ULStandard">
				<li><a href="<?=site_url('Login/index'); ?>">I'm an account owner.</a></li>
			</ul>	
		</div>	
		<div style="clear: left; height: 20px;"></div>
	</div>
</div>
<? $this->load->view('includes/footer'); ?>