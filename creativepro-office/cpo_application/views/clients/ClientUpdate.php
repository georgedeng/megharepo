<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="updateClients" class="helptag" data-help="clients">
	<form class="greyForm" id="formClient" name="formClient" action="<?=BASE_URL; ?>clients/ClientUpdate/saveClient" method="POST">
	<input type="hidden" name="clientID" id="clientID" value="<?=$clientInformation['ClientID']; ?><?=set_value('clientID'); ?>" />
	<input type="hidden" name="action" id="action" value="<?=$action; ?><?=set_value('action'); ?>" />
    <input type="hidden" name="clientUserid" value="<?=$clientInformation['ClientUserid']; ?>" />
    <input type="hidden" name="origClientEmail" value="<?=$clientInformation['Email']; ?>" />
	<input type="hidden" name="tagHolder" id="tagHolder" value="<? echo str_replace(' ',',',$clientInformation['Tags']); ?><?=set_value('tagHolder'); ?>" />
    <input type="hidden" name="save" id="save" value="1" />

		<p>
			<label class="required" for="clientCompany"><?=lang('client_form_company'); ?></label>
			<input type="text" value="<?=$clientInformation['Company']; ?><?=set_value('clientCompany'); ?>" name="clientCompany" id="clientCompany" tabindex="1" style="width: 400px;" />
			<?=form_error('clientCompany','<span class="formError">','</span>'); ?>
		</p>
		<p>
			<label><?=lang('client_form_phone1'); ?></label>
			<input type="text" value="<?=$clientInformation['Phone1']; ?><?=set_value('clientPhone1'); ?>" name="clientPhone1" id="clientPhone1" tabindex="2" style="width: 400px;" />
		</p>
		<p>
			<label><?=lang('client_form_email'); ?></label>
			<input type="text" value="<?=$clientInformation['Email']; ?><?=set_value('clientEmail'); ?>" name="clientEmail" id="clientEmail" tabindex="3" style="width: 400px;" />
			<?=form_error('clientEmail','<span class="formError">','</span>'); ?>
		</p>
        
		<p>
        <div style="float: left; margin-right: 18px;">
            <label><?=lang('client_form_tags'); ?></label>
            <div style="float: right;">
            <input type="text" name="clientTags" id="clientTags" tabindex="4" value="<?=$clientInformation['Tags']; ?><?=set_value('clientTags'); ?>" />
            </div>
        </div>
        <div style="float: left;">
            <?=help('help_formfield_tags','clientTags','iconHelpFormOutside'); ?>
        </div>
        </p>
        
		<p class="noBorder"></p>

        <? //if ($action == 'add') { ?>
            <div style="float: left;">
            <p>
                <label><?=lang('client_send_login_email'); ?></label>
                <input type="checkbox" value="1" name="clientSendEmail" id="clientSendEmail" tabindex="28" <? if ($settingEmailClientNew == 1) { echo 'checked'; } ?> />

            </p>
            </div>
            <div style="float: left;">
                <?=help('help_formfield_client_send_email','clientID','iconHelpFormOutside'); ?>
            </div>
            <div style="clear: left;"></div>
            <p class="noBorder"></p>
        <? //} ?>
		
		<a href="#" id="moreLink" tabindex="5" class="moreLink iconArrowDown"><?=lang('common_form_more_link'); ?></a>
		
		<div id="moreContent" style="display: none;">
			<p>
				<label><?=lang('client_form_phone2'); ?></label>
				<input type="text" value="<?=$clientInformation['Phone2']; ?><?=set_value('clientPhone2'); ?>" name="clientPhone2" id="clientPhone2" tabindex="9" style="width: 400px;" />
			</p>
			<p>
				<label><?=lang('client_form_phone3'); ?></label>
				<input type="text" value="<?=$clientInformation['Phone3']; ?><?=set_value('clientPhone3'); ?>" name="clientPhone3" id="clientPhone3" tabindex="10" style="width: 400px;" />
			</p>
			<p>
				<label><?=lang('client_form_url'); ?></label>
				<input type="text" value="<?=$clientInformation['URL']; ?><?=set_value('clientURL'); ?>" name="clientURL" id="clientURL" tabindex="11" style="width: 400px;" />
			</p>
			<p>
				<label><?=lang('client_form_address'); ?></label>
				<textarea name="clientAddress" id="clientAddress" class="expanding" tabindex="12" style="width: 400px; height: 40px;"><?=$clientInformation['Address']; ?><?=set_value('clientAddress'); ?></textarea>
			</p>
			<p>
				<label><?=lang('client_form_city'); ?></label>
				<input type="text" value="<?=$clientInformation['City']; ?><?=set_value('clientCity'); ?>" name="clientCity" id="clientCity" tabindex="14" style="width: 400px;" />
			</p>
			<p>
				<label><?=lang('client_form_state'); ?></label>
				<input type="text" name="clientState" id="clientState" value="<?=$clientInformation['State']; ?><?=set_value('clientState'); ?>" tabindex="15" style="width: 175px; margin-right: 10px;" />
				<span class="inlineLabel"><?=lang('client_form_zip'); ?></span>
				<input type="text" value="<?=$clientInformation['Zip']; ?><?=set_value('clientZip'); ?>" name="clientZip" id="clientZip" tabindex="16" style="width: 87px;" />
            </p>
			<p>
				<label><?=lang('client_form_country'); ?></label>
				<select name="clientCountry" id="clientCountry" selected="<?=set_value('clientCountry'); ?>" tabindex="17" style="width: 183px; margin-right: 10px;">
				<option><?=lang('form_field_select'); ?></option>
				<? $this->load->view('includes/country.html'); ?>
				</select>
				<input type="hidden" id="countryHolder" value="<?=$clientInformation['Country']; ?>" />

				<span class="inlineLabel"><?=lang('common_language'); ?></span>
				<select name="clientLanguage" id="clientLanguage" tabindex="18" class="clientLanguage" style="width: 133px;">
					<?
					$languages = languages();
					foreach($languages as $key => $value) {
						echo '<option value="'.$key.'"';
						if ($key == $this->session->userdata('language')) {
							echo ' selected="selected" ';
						}
						echo '>'.$value.'</option>';
					}
					?>
				</select>
            </p>
			<p>
				<label><?=lang('client_form_timezone'); ?></label>
                <?=timezone_menu($clientInformation['Timezone'],'tzStyle','clientTimezone',407,19); ?>
            </p>
            <p>
				<label><?=lang('client_form_industry'); ?></label>
                <?=industry_menu($clientInformation['Industry'],'tzStyle','clientIndustry',407,20); ?>
            </p>
			<p class="noBorder">
				<label for="invoiceCategory"><?=lang('client_form_category'); ?></label>
                <input type="text" name="clientCategorySelect" id="clientCategorySelect" value="<?=$clientInformation['Category']; ?>" tabindex="22" style="width: 400px;" />
                <input type="hidden" name="clientCategory" id="clientCategory" value="<?=$clientInformation['CatID']; ?>" />
            </p>

            <!--
            <div style="float: left;">
            <p class="noBorder">
                <label><?=lang('client_rate'); ?></label>
                <input type="text" name="clientRateAmount" id="clientRateAmount" tabindex="23" value="<?=lang('finance_expense_amount'); ?><?=$clientInformation['RateAmount']; ?><?=set_value('clientRateAmount'); ?>" style="float: left; margin-right: 6px; width: 100px;" />
                <select id="clientRatePer" name="clientRatePer" size="1" tabindex="24" style="float: left; width: 100px; margin-right: 6px;">
                    <option value="hour"><?=lang('common_per_hour'); ?></option>
                    <option value="hour"><?=lang('common_per_day'); ?></option>
                </select>
                <textarea name="clientRateDesc" id="clientRateDesc" tabindex="25" style="float: left; width: 180px; height: 50px;"><?=lang('finance_invoice_item_description'); ?><?=$clientInformation['RateDescription']; ?><?=set_value('clientRateDesc'); ?></textarea>
            </p>
            </div>
            <div style="float: left;">
                <?=help('help_rates','projectRateAmount','iconHelpFormOutside'); ?>
            </div>
            <div style="clear: left;"></div>
            <p></p>
            -->
			<p>
				<label><?=lang('client_form_notes'); ?></label>
				<textarea name="clientNotes" id="clientNotes" class="expanding" tabindex="26" style="width: 400px; height: 40px;"><?=$clientInformation['Comments']; ?><?=set_value('clientNotes'); ?></textarea>
			</p>
			<p>
				<label><?=lang('client_form_client_since'); ?></label>
				<input type="text" value="<?=$clientInformation['Client_Since']; ?><?=set_value('clientSince'); ?>" name="clientSince" id="clientSince" tabindex="27" style="width: 200px;" />
			</p>						
		</div>        
		<div class="buttonContainer">
            <button class="buttonExpand blueGray action primary" id="buttonSave" tabindex="100"><span class="save"><?=lang('button_save'); ?></span></button>
			<button class="buttonExpand yellow cancel secondary" id="buttonCancel" tabindex="101"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
            <div style="clear: left;"></div>
		</div>	
	</form>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>