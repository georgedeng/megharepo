<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>Maintenance :: CreativePro Office</title>
	<meta name="Description" content="At Mycpohq, you can easily manage your team, clients, projects, invoices, time sheet from one web-based app with the help of project and task management dashboard software.">
	<meta name="Keywords" content="project management dashboard software,task management software,web based office applications,online project management tools ,online timesheet,online invoice, project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist">
	<meta name="Author" content="Jeff Denton @ UpStart Productions :: Portland OR :: support@mycpohq.com">
	<META NAME="Rating" CONTENT="General">
	<META NAME="Distribution" CONTENT="global">
	<!-- Favicon -->
    <link rel="icon" href="http://mycpohq.com/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="http://mycpohq.com/images/favicon.ico" type="image/x-icon" />

    <!-- Style Sheets -->
    <link rel="stylesheet" type="text/css" href="http://mycpohq.com/css/browserReset.css?1401084564" media="screen" />
    <link rel="stylesheet" type="text/css" href="http://mycpohq.com/css/stylesPublic.css?1401084572" media="screen" />
    <link rel="stylesheet" type="text/css" href="http://mycpohq.com/css/buttons.css?1401084564" media="screen" />
    <link rel="stylesheet" type="text/css" href="http://mycpohq.com/css/alerts.css?1401084563" media="screen" />
    <link rel="stylesheet" type="text/css" href="http://mycpohq.com/css/colorbox.css?1401084564" media="screen" />
    <link rel="stylesheet" type="text/css" href="http://mycpohq.com/css/flexslider.css?1401084565" media="screen" />
    
    <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="http://mycpohq.com/css/stylesIE8.css?1401084572" media="screen" />
    <![endif]-->

    <!-- RSS Feeds -->
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://mycpohq.com/rss/Rss/productBlogFeed" />

    <!-- Print Style Sheet -->
    <link rel="stylesheet" type="text/css" href="http://mycpohq.com/css/print.css?1401084571" media="print" />

    <style type="text/css">
        #header .headerTextContainer {
            background: url('http://mycpohq.com/images/txtLoginHeader_public.png') 0px 27px no-repeat;
        }
    </style>
 
</head>
<body>
    <div id="header" class="child">
        <div class="blackBar">
            <div class="content">
                <a href="http://mycpohq.com/" class="logoCPO" title="CreativePro Office : HOME"></a>
            </div>
        </div>
                <!-- Child page header field -->
        <div class="homeFieldBluePublic">
            <div class="content">
             
            </div>
        </div>
            </div>
    <div id="pageBody">
        <div style="text-align:center;font-size:19px !important;padding-top:100px;">System will be back soon after maintenance</div>
 
<div style="clear: left;"></div> <!-- Clear page left, right panel floats -->    
</div> <!-- End #pageBody -->
    </div> 
    <div id="footer">
        <div class="content 
            <div class="mBottom" style="clear: left;"></div>
            <div class="copyright">CreativePro Office is an <a href="http://www.UpStart-Productions.com" title="UpStart Productions">UpStart Productions</a> service. &copy; 2003-2015</div>
        </div>
    
    </body>
    </html>
