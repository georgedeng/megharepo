<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<?
ini_set('max_execution_time',7200);
set_time_limit(0);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>

    <title>Translation</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">      
  </head>
  <body>
  <h1>Performing Translations</h1>
  <?
  class Google_API_translator {
    public $opts = array("text" => "", "language_pair" => "en|it");
    public $out = "";

    function __construct() {
        echo "Google Translator API\n(c) 2007 Involutive snc http://www.involutive.com\n";
        echo "Author: Paolo Ardoino < paolo@involutive.com >";
    }

    function setOpts($opts) {
        if($opts["text"] != "") $this->opts["text"] = $opts["text"];
        if($opts["language_pair"] != "") $this->opts["language_pair"] = $opts["language_pair"];
    }

    function translate() {
        $this->out = "";
        $google_translator_url = "http://translate.google.com/translate_t?langpair=".urlencode($this->opts["language_pair"])."&amp;";
        $google_translator_data .= "text=".urlencode($this->opts["text"]);
        $gphtml = $this->postPage(array("url" => $google_translator_url, "data" => $google_translator_data));
        $out = substr($gphtml, strpos($gphtml, "<div id=result_box dir=\"ltr\">"));
        $out = substr($out, 29);
        $out = substr($out, 0, strpos($out, "</div>"));
        $this->out = utf8_encode($out);
        return $this->out;
    }

    // post form data to a given url using curl libs
    function postPage($opts) {
        $html = "";
        if($opts["url"] != "" && $opts["data"] != "") {
            $ch = curl_init($opts["url"]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $opts["data"]);
            $html = curl_exec($ch);
            if(curl_errno($ch)) $html = "";
            curl_close ($ch);
        }
        return $html;
    }
}
$g = new Google_API_translator();
$rootDir = '../cpo_system/language/';
$translateFromDir = 'en';
$languageArray = array('es');
/*
 * Read the translate from directory and find all language files
 */
if ($handle = opendir($rootDir.$translateFromDir)) {
    while ( false !== ($file = readdir($handle)) ) {
         if ( ($file != 'index.html') && ($file != '.') && ($file != '..') ) {
               include($rootDir.$translateFromDir.'/'.$file);
               echo '<p style="margin: 15px;">Translating file '.$file.'...<br>';
               foreach($languageArray as $language) {
                    echo '...into '.$language.'<br>';
                    $fileHandle = fopen($rootDir.$language.'/'.$file, "w");
                    $fileContent = "<?\n";
                    foreach($lang as $key => $word) {
                         $g->setOpts(array("text" => $word, "language_pair" => "en|".$language));
                         $g->translate();
                         $translation = $g->out;
                         //echo $word.'|'.$g->out.'<br>';
                         $fileContent .= '$lang[\''.$key.'\'] = \''.$translation.'\';'."\n";
                    }
                    $fileContent .= "?>";
                    echo 'DONE!</p>';
                    fwrite($fileHandle, pack("CCC",0xef,0xbb,0xbf));
                    fwrite($fileHandle, $fileContent);
                    fclose($fileHandle);
               }
         }
    }
}
closedir($handle);
?>
  <div id="translation"></div>
  </body>
</html>
