{
  "title":{
    "text":  "Chart",
    "style": "{font-size: 20px; color:#0000ff; font-family: Verdana; text-align: center;}"
  },

  "y_legend":{
    "text": "Chart",
    "style": "{color: #736AFF; font-size: 12px;}"
  },

  "elements":[
    {
      "type":      "bar",
      "alpha":     0.5,
      "colour":    "#9933CC",
      "text":      "Chart",
      "font-size": 10,
      "values" :   [0]
    },
    {
      "type":      "bar",
      "alpha":     0.5,
      "colour":    "#CC9933",
      "text":      "Chart",
      "font-size": 10,
      "values" :   [0]
    }
  ],

  "x_axis":{
    "stroke":1,
    "tick_height":10,
    "colour":"#d000d0",
    "grid_colour":"#00ff00",
    "labels": ["Chart"]
   },

  "y_axis":{
    "stroke":      4,
    "tick_length": 3,
    "colour":      "#d000d0",
    "grid_colour": "#00ff00",
    "offset":      0,
    "max":         20
  }
}

