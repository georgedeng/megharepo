<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonWidgets {
	/**
	 * getTask : retrieve task information for view or edit
	 *
	 * @access	public
	 * @param	int  $taskID ID of the task for which we want data
	 * @return	mixed  PHP array of task data or JSON sting
	 */
     function getActiveFooterWidgets($userid=NULL) {
          $CI =& get_instance();
          $CI->load->model('dashboard/Dashboard_owners','DashboardOwners',TRUE);
          $footerWidgetArray = $CI->DashboardOwners->getActiveWidgets('footer',$userid);
		  $availableWidgets = availableWidgets();
		  foreach($availableWidgets as $widget) {
			  $a=0;
			  foreach($footerWidgetArray as $footerWidget) {
				if ($widget['WID'] == $footerWidget['WID']) {
					$footerWidgetArray[$a]['WidgetName'] = $widget['WidgetName'];
					$footerWidgetArray[$a]['WName']      = $widget['WName'];
					break;
				}
				$a++;
			  }
		  }
		  return $footerWidgetArray;		  
     }

	 function saveFooterWidgets() {
		 
	 }
}
?>