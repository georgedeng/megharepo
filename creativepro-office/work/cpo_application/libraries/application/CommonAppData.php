<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonAppData {
    /**
	 * renderTags : renderTag list for various site modules
	 *
	 * @access	public
	 * @param	string  $tagType client,project,invoice,etc.
	 * @param   string  $renderType :  JSON, HTML, PHP array
	 * @return	mixed   PHP array, JSON string, HTML string
	 */
    function renderTags($siteArea,$renderType='json') {
        $CI =& get_instance();
		$CI->load->model('common/app_data','',true);
		$CI->lang->load('app',$CI->session->userdata('language'));

		$tagArray = $CI->app_data->getTags($CI->session->userdata('accountUserid'),$siteArea);
		$tagCounter = 0;
		$alphaCounter = 0;
		$alphaChar = '';
		$newTagArray['Tags']   = array();

		switch ($siteArea) {
			case 'project':
				$viewURL = site_url('projects/ProjectView/tag');
				break;
			case 'invoice':
				$viewURL = site_url('finances/FinanceView/tag');
				break;
			case 'calendar':
				$viewURL = site_url('calendar/CalendarView/tag');
				break;
			case 'client':
				$viewURL = site_url('clients/ClientView/tag');
				break;
            case 'task':
				$viewURL = site_url('projects/ProjectView/tag');
				break;
            case 'expense':
				$viewURL = site_url('finances/FinanceView/tagExpense');
				break;
		}
		$newTagArray['tagURL'] = $viewURL;

		foreach($tagArray as $tag) {
			/*
			 * Get first letter of tag
			 */
			$tagID     = $tag['TID'];
			$tagString = $tag['Tag'];
			if (!empty($tagString)) {
				$firstChar = substr($tagString,0,1);
				if ($firstChar != $alphaChar && $firstChar != strtoupper($alphaChar)) {
					$newAlphaChar = strtoupper($firstChar);
					$newTagArray['Tags'][$newAlphaChar] = array();
					$alphaChar = $firstChar;
					$tagCounter = 0;
				}
				$newTagArray['Tags'][$newAlphaChar][$tagCounter]['TagID'] = $tagID;
				$newTagArray['Tags'][$newAlphaChar][$tagCounter]['Tag']   = $tagString;
				$tagCounter++;
			}
		}

		if ($renderType == 'json') {
			return json_encode($newTagArray);
		} elseif ($renderType == 'array') {
			return $newTagArray;
		}
    }

    function saveTags($tagString,$siteArea) {
        ini_set('display_errors',1);
        $CI =& get_instance();
		$CI->load->model('common/app_data','',true);
		$CI->lang->load('app',$CI->session->userdata('language'));
        $CI->load->helper('string');

        $tagArray = explode(' ',$tagString);
        foreach($tagArray as $tag) {
            //$tag = cleanStringTag($tag);
            if (!empty($tag)) {
                if (!$CI->app_data->getTag($tag,$CI->session->userdata('accountUserid'),$siteArea)) {
                    /*
                     * Insert new tag
                     */
                    $CI->app_data->saveTag($CI->session->userdata('accountUserid'),null,$tag,'add',$siteArea);
                }
            }
        }
    }

    function saveHelpdeskTags($tagString) {
        $CI =& get_instance();
		$CI->load->model('common/app_data','',true);
        $CI->load->helper('string');

        $tagArray = explode(' ',$tagString);
        foreach($tagArray as $tag) {
            $tag = cleanStringTag($tag);
            if (!empty($tag)) {
                if (!$CI->Helpdesk_model->getHelpdeskTag($tag)) {
                    /*
                     * Insert new tag
                     */
                    $CI->Helpdesk_model->saveHelpdeskTag(null,$tag,'add');
                }
            }
        }
    }

    function saveBlogTags($tagString) {
        $CI =& get_instance();
		$CI->load->model('common/app_data','',true);
        $CI->load->helper('string');

        $tagArray = explode(' ',$tagString);
        foreach($tagArray as $tag) {
            $tag = cleanStringTag($tag);
            if (!empty($tag)) {
                if (!$CI->app_data->getTag($tag,$CI->session->userdata('accountUserid'),$siteArea)) {
                    /*
                     * Insert new tag
                     */
                    $CI->app_data->saveTag($CI->session->userdata('accountUserid'),null,$tag,'add',$siteArea);
                }
            }
        }
    }

	function modifyTrashCount($delta,$direction) {
        $CI =& get_instance();
		$trashTotal = $CI->session->userdata('trashTotal');
		if ($direction == 'down') {
			if ($trashTotal>0) {
				$trashTotal = $trashTotal-$delta;
			}
		} else {
			$trashTotal = $trashTotal+$delta;
		}
		$CI->session->set_userdata('trashTotal',$trashTotal);
	}
    
    function saveCategory($category,$siteArea=null) {
        $CI =& get_instance();
		$CI->load->model('common/app_data','',true);
        $CI->load->helper('string');
        $CI->load->helper('security');
        
        if ($category == '0') {
            $category = fieldToDB($CI->input->post('category', TRUE));
        } else {
            $category = $CI->input->xss_clean($category);
        }
        
        if (!empty($category)) {
            $catID = $CI->app_data->getCategoryID($category,$CI->session->userdata('accountUserid'),$siteArea);
            if ($catID === false) {
                /*
                 * Insert new category
                 */
                $catID = $CI->app_data->saveCategory($CI->session->userdata('accountUserid'),null,$category,'add',$siteArea);
            }
            return $catID;
        }
	}
}
?>