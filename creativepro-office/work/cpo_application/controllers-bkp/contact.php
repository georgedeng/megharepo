<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
        $this->load->library('application/CommonEmail');
	}

    function _init($mailSent) {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtContactHeader_public.png';
        $data['jsFileArray']     = array();
        $data['pageTitle']       = 'Contact Us';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        $data['mailSent'] = $mailSent;
        $data['rightColumnComponents'] = array('loginBox','whosUsingCPO_nobg');
        return $data;
    }

    function index($mailSent = false) {
		$data = Contact::_init($mailSent);
        $this->load->view('public_site/Contact',$data);
    }

    function sendMessage() {
        $this->commonemail->sendContactEmail($_POST);
        Contact::index(true);
    }
}
?>