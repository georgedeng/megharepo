<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Forum extends Controller {
	function __construct()
	{
		parent::Controller();

	}

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = 'txtForumHeader_public.png';

        return $data;
    }

    function index() {
        header('Location: http://forum.creativeprooffice.com');
		$data = Forum::_init();
        $this->load->view('forum/Forum',$data);
    }
}
?>
