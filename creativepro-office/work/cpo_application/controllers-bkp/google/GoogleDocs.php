<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class GoogleDocs extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('messaging/Site_messages','SiteMessages',true);
        $this->load->model('tasks/Task_view','',true);
        $this->load->library('application/CommonMessages');
		$this->load->library('application/CommonEmail');

		$this->load->helper('zend');
        
        Zend_Loader::loadClass('Zend_Gdata_AuthSub');
        Zend_Loader::loadClass('Zend_Gdata_Docs');
	}

	function _init() {
		$data['page']                  = 'finances';
		$data['pageTitle']             = 'Google Doc Test';
		$data['pageSubTitle']          = '';
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsGoogle.js');
		$data['pageIconClass']         = '';
		$data['pageLayoutClass']       = 'withRightColumn';

        /*
         * Try force download of zip file
         */
        
        $file = '/var/www/dev.mycpohq.com/downloads/CreativeProOffice-2.0.0.2.zip';
        header("Content-type: application/force-download");
        header("Content-Transfer-Encoding: Binary");
        header("Content-length: ".filesize($file));
        header("Content-disposition: attachment; filename=\"".basename($file)."\"");
        readfile($file);
        

        
		return $data;
	}   

	function index() {        
        $gdataDocs = new Zend_Gdata_Docs();
		$data = GoogleDocs::_init();
        $data['authLink'] = GoogleDocs::getAuthSubUrl();
		$this->load->view('google/GoogleDocTest',$data);
	}

    function getAuthSubUrl($return=0) {
        $googleToken = $this->session->userdata('googleToken');
        if (!empty($googleToken)) {
            GoogleDocs::getGoogleDocs();
        } else {
            $thisPage = $this->uri->uri_string();
            if (isset($_POST['thisPage']) && !empty($_POST['thisPage'])) {
                $thisPage = $_POST['thisPage'];
            }
            $next = SITE_URL.$thisPage;
            $scope = 'http://docs.google.com/feeds/';
            $secure = false;
            $session = true;
            $authLink = Zend_Gdata_AuthSub::getAuthSubTokenUri($next, $scope, $secure, $session);
            if ($return == 1) {
                return $authLink;
            } else {
                echo '{"AuthLink":"'.$authLink.'"}';
            }
        }
    }

    function getGoogleDocs() {
        $httpClient = Zend_Gdata_AuthSub::getHttpClient($this->session->userdata('googleToken'));
		$gdClient = new Zend_Gdata_Docs($httpClient);

		$docFeed = $gdClient->getDocumentListFeed();
		$a=0;
		foreach ($docFeed->entries as $entry) {
			/*
			 *  Find the URL of the HTML view of the document.
			 */
			$alternateLink = '';
			$docExtension = '';
			foreach($entry->link as $link) {
                if ($link->getRel() === 'alternate') {
					$alternateLink = $link->getHref();
					if (preg_match('/spreadsheet/',$alternateLink)) {
						$docExtension = 'xls';
					} elseif (preg_match('/present/',$alternateLink)) {
						$docExtension = 'ppt';
					} elseif (preg_match('/docs/',$alternateLink)) {
						$docExtension = 'doc';
					}
				}
			}
            $author = $entry->getAuthor();            

            $docArray[$a]['FileID']         = 0;
            $docArray[$a]['ParentID']       = 'fileservice';
			$docArray[$a]['FileTitle']      = $entry->title->text;
            $docArray[$a]['FileNameActual'] = $entry->title->text;
            $docArray[$a]['FileSize']       = '';
            $docArray[$a]['FileType']       = $docExtension;
            $docArray[$a]['FileDesc']       = '';
            $docArray[$a]['Tags']           = '';
            $docArray[$a]['Public']         = 0;
            $docArray[$a]['Folder']         = 0;
            $docArray[$a]['FolderState']    = 0;
            $docArray[$a]['ImportFolder']   = 0;
            $docArray[$a]['DateEntry']      = convertIcalToHuman($entry->published->text,'M j, Y g:i A');
            $docArray[$a]['DateUpdate']     = convertIcalToHuman($entry->updated->text,'M j, Y g:i A');
            $docArray[$a]['PID']            = '';
            $docArray[$a]['UploaderName']   = $author[0]->getName()->getText();
            $docArray[$a]['iconSmall']      = '';
            $docArray[$a]['iconBig']        = '';
			$docArray[$a]['fileLink']       = $alternateLink;
			$docArray[$a]['fileExt']        = $docExtension;
            $docArray[$a]['FileService']    = 1;
			$a++;
		}
		
        $jsonString = '{"Path":[{"FolderName":"All files","NodeID":0,"ParentID":0},{"FolderName":"Google Docs","NodeID":"0","ParentID":"google"}],"Files":';
        $jsonString .= json_encode($docArray);
        $jsonString .= '}';
		echo $jsonString;
   }

	function testGoogleEmail() {
		$data = GoogleDocs::_init();
		$this->load->view('google/GoogleEmailTest',$data);
	}

	function getGmail() {
        $mailArrayMyCPOHQ = array(
			'host'     => 'mail.mycpohq.com:110',
			'user'     => 'jeff@mycpohq.com',
			'password' => 'addyG00SE',
			'port'     => 110
		);

		$mailArrayGmail = array(
			'host'     => 'pop.gmail.com:995',
			'user'     => 'chiefupstart@gmail.com',
			'password' => 'laser10',
			'ssl'      => 'SSL',
			'port'     => 995
		);

		Zend_Loader::loadClass('Zend_Mail_Storage_Pop3');
		$gmail = new Zend_Mail_Storage_Pop3($mailArrayMyCPOHQ);
		$numberOfMessages = $gmail->countMessages();

		$messageID = $numberOfMessages;
		for($i=0; $i<10; $i++) {
			$message = $gmail->getMessage($messageID);
			echo $message->getContent();
			$messageID = $messageID-1;
		}
	}
}
?>