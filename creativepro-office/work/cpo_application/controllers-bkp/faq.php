<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Faq extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtFAQHeader_public.png';
        $data['jsFileArray']     = array();
        $data['pageTitle']       = 'Product FAQ';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
		$data = Faq::_init();
        $this->load->view('public_site/Faq',$data);
    }
}
?>