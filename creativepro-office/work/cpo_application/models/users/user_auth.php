<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_auth extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
        $this->load->library('application/CommonLogin');
        $this->_assign_libraries();
        $this->_init();
    }

    function _init() {
        /*
         * If we're not logged in, let's see if user
         * has a rememberMe cookie.  And then let's
         * see if the cookie is expired. If not, let's
         * log this user in.
         */        
        if ($this->session->userdata('logged') != TRUE && $this->uri->segment(3) != 'logout') {
            $rememberMeGUID = get_cookie('rememberMe');
            if (!empty($rememberMeGUID)) {
                $sql = "SELECT * from cpo_login WHERE KeepActiveGUID = '$rememberMeGUID'";
                $query = $this->db->query($sql);
                if ($query->num_rows()>0) {
                    $row = $query->row_array();

                    $this->commonlogin->tryLogin($row['Userid'],$row['Password'],NULL,NULL,NULL,TRUE,NULL,FALSE);
                }
            }
        }
    }
}
?>