<?php
class Calendar_update extends Model {

    function Calendar_update() {
        // Call the Model constructor
        parent::Model();
    }

	function saveCalendar($data) {
		if ($data['calendarID']>0) {
			/*
			 * Editing a calendar
			 */
			$sql = "UPDATE cpo_calendar SET
					Userid       = '".$data['accountUserid']."',
					Title        = '".$data['calendarName']."',
					Color        = '".$data['color']."',
					UseridUpdate = '".$data['userid']."'
					WHERE CalendarID = '".$data['calendarID']."'";
			$query = $this->db->query($sql);
    		$calendarID = $data['calendarID'];
		} else {
			/*
			 * Inserting new calendar
			 */
			$sql = "INSERT into cpo_calendar (
					Userid,
					Title,
					Color,
					DateCreated,
                    UseridEntry,
					UseridUpdate
					) values (
					'".$data['accountUserid']."',
					'".$data['calendarName']."',
					'".$data['color']."',
					'".date('Y-m-d')."',
                    '".$data['userid']."',
					'".$data['userid']."'
					)";
			$query = $this->db->query($sql);
    		$calendarID = $this->db->insert_id();

		}
		return $calendarID;
	}

	function saveMemberToCalendar($calendarID,$memberID) {
		$sql = "INSERT into cpo_linkMemberCalendar (
				CalendarID,
				MemberID,
				MemberType
				) values (
				'$calendarID',
				'$memberID',
				'T'
				)";
		$query = $this->db->query($sql);
	}

    function clearMembersFromCalendar($calendarID) {
        $sql = "DELETE FROM cpo_linkMemberCalendar WHERE CalendarID = '$calendarID'";
        $query = $this->db->query($sql);
    }

	function saveEvent($data) {
		if ($data['eventID']>0) {
            $sql = "UPDATE cpo_calendar_events SET
                        CalendarID    = '".$data['eventCalendarID']."',
                        DateStart     = '".$data['DBEventDateStart']."',
                        DateEnd       = '".$data['DBEventDateEnd']."',
                        Title         = '".$data['eventTitle']."',
                        Description   = '".$data['eventDescription']."',
                        Location      = '".$data['eventLocation']."',
                        Tags          = '".$data['eventTags']."',
                        Recurring     = '".$data['eventRepeats']."',
                        EmailReminder = '".$data['eventReminder']."',
                        UseridUpdate  = '".$data['userid']."',
                        DateUpdate    = '".date('Y-m-d H:i:s')."'
                    WHERE
                        EventID = '".$data['eventID']."'";
            $query = $this->db->query($sql);
            $eventID = $data['eventID'];
		} else {
			$sql = "INSERT into cpo_calendar_events (
					CalendarID,
					Userid,
					DateStart,
					DateEnd,
					Title,
					Description,
                    Location,
                    Tags,
                    Recurring,
                    EmailReminder,
                    UseridEntry,
					UseridUpdate,
					DateEntry,
					DateUpdate,
                    ImportGUID
					) values (
					'".$data['eventCalendarID']."',
					'".$data['accountUserid']."',
					'".$data['DBEventDateStart']."',
					'".$data['DBEventDateEnd']."',
					'".$data['eventTitle']."',
					'".$data['eventDescription']."',
                    '".$data['eventLocation']."',
                    '".$data['eventTags']."',
                    '".$data['eventRepeats']."',
                    '".$data['eventReminder']."',
                    '".$data['userid']."',
					'".$data['userid']."',
					'".date('Y-m-d')."',
					'".date('Y-m-d')."',
                    '".$this->session->userdata('importGUID')."'
					)";
			$query = $this->db->query($sql);
			$eventID = $this->db->insert_id();
		}
		return $eventID;
	}

    function deleteEvent($eventID,$tagForDelete=TRUE) {
        if ($tagForDelete == TRUE) {
            $sql = "UPDATE cpo_calendar_events SET Deleted = 1 WHERE EventID = '$eventID'";
            $query = $this->db->query($sql);
        } else {
            $sql = "DELETE FROM cpo_calendar_events WHERE EventID = '$eventID'";
            $query = $this->db->query($sql);
        }
        return $eventID;
    }

    function deleteCalendar($calendarID,$withEvents,$tagForDelete=TRUE,$userid) {
        if ($tagForDelete == TRUE) {
            $sql = "UPDATE cpo_calendar SET UseridUpdate = '$userid', Deleted = 1 WHERE CalendarID = '$calendarID'";
            $query = $this->db->query($sql);

            if ($withEvents == 1) {
                $sql = "UPDATE cpo_calendar_events SET UseridUpdate = '$userid', Deleted = 1 WHERE CalendarID = '$calendarID'";
                $query = $this->db->query($sql);
            }
        } else {
            $sql = "DELETE FROM cpo_calendar WHERE CalendarID = '$calendarID'";
            $query = $this->db->query($sql);

            $sql = "DELETE FROM cpo_linkMemberCalendar WHERE CalendarID = '$calendarID'";
            $query = $this->db->query($sql);

            if ($withEvents == 1) {
                $sql = "DELETE from cpo_calendar_events WHERE CalendarID = '$calendarID'";
                $query = $this->db->query($sql);
            }
        }
    }

    function restoreEvent($eventID,$userid) {
        $sql = "UPDATE cpo_calendar_events SET UseridUpdate = '$userid', Deleted = 0 WHERE EventID = '$eventID'";
        $query = $this->db->query($sql);
    }

    function restoreCalendar($calendarID,$userid) {
        $sql = "UPDATE cpo_calendar SET UseridUpdate = '$userid', Deleted = 0 WHERE CalendarID = '$calendarID'";
        $query = $this->db->query($sql);
    }

    function saveEventGuest($guestID,$eventID,$guestType) {
        $sql = "INSERT into cpo_linkMemberEvent (
                EventID,
                ItemID,
                ItemType
                ) values (
                '$eventID',
                '$guestID',
                '$guestType'
                )";
        $query = $this->db->query($sql);
    }

    function deleteEventGuests($eventID) {
        $sql = "DELETE from cpo_linkMemberEvent WHERE EventID = '$eventID'";
        $query = $this->db->query($sql);
    }

    function moveEventsToCalendar($calendarFrom,$calendarTo) {
        $sql = "UPDATE cpo_calendar_events SET CalendarID = '$calendarTo' WHERE CalendarID = '$calendarFrom'";
        $query = $this->db->query($sql);
    }

    function updateEventDatesAfterMove($eventID,$newDateStart,$newDateEnd) {
        /*
        $sql = "SELECT DateStart,DateEnd FROM cpo_calendar_events WHERE EventID = '$eventID'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $oldDateStart = convertMySQLToGMT($row['DateStart'],'pull','Y-m-d H:i:s');
        $oldDateEnd   = convertMySQLToGMT($row['DateEnd'],'pull','Y-m-d H:i:s');
        
        $oldDateStartArray = explode(' ',$oldDateStart);
        $oldDateEndArray   = explode(' ',$oldDateEnd);

        $newDateStart = $newDateStart.' '.$oldDateStartArray[1];
        $newDateEnd   = $newDateEnd.' '.$oldDateEndArray[1];

		$newDateStartSave = convertMySQLToGMT($newDateStart,'push','Y-m-d H:i:s');
		$newDateEndSave   = convertMySQLToGMT($newDateEnd,'push','Y-m-d H:i:s');

        if ($oldDateEnd == '0000-00-00 00:00:00') {
            $newDateEndSave = $oldDateEnd;
            $newDateEnd = $oldDateEnd;
        }
        */

        $sql = "UPDATE cpo_calendar_events SET
                DateStart = '$newDateStart',
                DateEnd   = '$newDateEnd'
                WHERE
                EventID = '$eventID'";
        $query = $this->db->query($sql);
        $dateArray = array($newDateStart,$newDateEnd);
        return $dateArray;
    }
}
?>
