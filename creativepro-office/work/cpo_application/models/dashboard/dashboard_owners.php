<?php
class Dashboard_owners extends Model {

    function Dashboard_owners() {
        // Call the Model constructor
        parent::Model();
    }
        
    function getActiveWidgets($type=NULL,$userid=NULL) {
		if (empty($userid)) {
			$userid = $this->session->userdata('userid');
		}
		if ($type == 'dashboard') {
			$where = " WColumn < 3 ";
		} elseif ($type == 'footer') {
			$where = " WColumn = 3 ";
		}
    	$sql = "SELECT W.WID,W.WOrder,W.WColumn
				FROM cpo_widgetsActive W
				WHERE 
					W.UserID = '".$userid."' AND
					$where
				ORDER BY W.WColumn, W.WOrder";
		$query = $this->db->query($sql); 	
    	return $query->result_array(); 
    }

    function addWidget($wID,$userid,$type=NULL,$column=0) {
		if ($type == 'footer') {
			$column = 3;
			$sql = "SELECT * FROM cpo_widgetsActive
				WHERE Userid = '".$userid."' AND
				WID = '$wID' AND WColumn = 3";
		} else {
			$sql = "SELECT * FROM cpo_widgetsActive
				WHERE Userid = '".$userid."' AND
				WID = '$wID'";
		}
    	/*
    	 * Check for existing widget
    	 */
    	$query = $this->db->query($sql);
    	if($query->num_rows()<1) {
    		$sql = "INSERT INTO cpo_widgetsActive (
					UserID,
					WID,
					WOrder,
					WColumn
					) values (
					'".$userid."',
					'$wID',
					'0',
					'$column'
					)";
    		$query = $this->db->query($sql);
    	}
    }
    
    function removeWidget($wID,$type=NULL) {
		if ($type == 'dashboard') {
			$where = " WColumn < 3 ";
		} elseif ($type == 'footer') {
			$where = " WColumn = 3 ";
		}
    	$sql = "DELETE FROM cpo_widgetsActive
				WHERE Userid = '".$this->session->userdata('userid')."' AND 
				WID = '$wID' AND
				$where";
    	$query = $this->db->query($sql);
    }
    
    function saveDashboardOrder($wID,$column,$order) {
    	$sql = "UPDATE cpo_widgetsActive SET
				WOrder = '$order', 
				WColumn = '$column' 
				WHERE Userid = '".$this->session->userdata('userid')."' AND 
				WID = '$wID'";
    	$query = $this->db->query($sql);
    }
} 
?>