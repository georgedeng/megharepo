<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class File_maintenance extends Model {
    var $fileTreeArrayMaster = array();
    var $fileInfoArray = array();
    var $fileIDArray = array();

    function __construct() {
        parent::Model();

    }

	/**
      * insertFileData : insert file information into database
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
    function insertFileData($fileDataArray) {
        if (!isset($fileDataArray['FileServicePath'])) {
            $fileDataArray['FileServicePath'] = '';
        }
        if (!isset($fileDataArray['FileService'])) {
            $fileDataArray['FileService'] = 0;
        }
		if($fileDataArray['action'] == 'add') {
			$sql = "INSERT into cpo_files (
					ParentID,
					Userid,
                    FileNameActual,
                    FileServicePath,
					FileSize,
                    FileType,
                    FileService,
                    UseridUpdate,
					DateEntry,
                    DateUpdate,
                    ImportGUID
                    ) values (
					'".$fileDataArray['ParentID']."',
					'".$fileDataArray['accountUserid']."',
                    '".$fileDataArray['FileNameActual']."',
                    '".$fileDataArray['FileServicePath']."',
                    '".$fileDataArray['FileSize']."',
					'".$fileDataArray['FileType']."',
                    '".$fileDataArray['FileService']."',
					'".$fileDataArray['userid']."',
					'".date('Y-m-d G:i:s')."',
					'".date('Y-m-d G:i:s')."',
                    '".$this->session->userdata('importGUID')."'
                    )";
			$query = $this->db->query($sql);
			$fileID = $this->db->insert_id();
		} elseif ($fileDataArray['action'] == 'edit') {
			
		}
		return $fileID;
    }

	/**
      * createFolder : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	function getFileItems($nodeID,$accountUserid) {
        $getFileService = '';
        if ($nodeID == '0') {
            $getFileService = " OR (F.FileService = 1 AND F.Folder = 1) ";
        }
		$sql = "SELECT 
                    F.FileID,F.ParentID,F.FileTitle,F.FileNameActual,F.FileSize,F.FileType,F.FileDesc,F.Tags,
                    F.Public,F.Folder,F.FolderState,F.ImportFolder,F.DateEntry,F.DateUpdate,F.FileService,
                    P.PID,P.NameFirst,P.NameLast
                FROM cpo_files F
                LEFT JOIN cpo_people P ON P.LoginUserid = F.UseridUpdate
				WHERE
                    (
                        F.ParentID = $nodeID AND
                        F.Userid = '".$accountUserid."' AND
                        F.FileService = 0 AND
                        F.Deleted = 0
                    ) $getFileService
				ORDER BY F.Folder DESC, F.FileNameActual ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    /**
      * getFilesForSomething
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	function getFilesForSomething($itemID,$itemType,$shared=NULL,$guid=NULL,$accountUserid) {
        $sharedFilter = '';
        if ($shared == TRUE) {
            $sharedFilter = " AND LF.Shared = 1 ";
        }
        
        $guidFilter = '';
        if ($guid != NULL) {
            $itemID = 0;
            $guidFilter = " AND LF.ImportGUID = '$guid' ";
        }
        $sql = "SELECT
                    LF.LinkID,F.FileID,F.ParentID,F.FileTitle,F.FileNameActual,F.FileServicePath,F.FileSize,F.FileType,F.FileDesc,F.Tags,
                    F.Public,F.Folder,F.FolderState,F.ImportFolder,F.DateEntry,F.DateUpdate,F.FileService,
                    P.PID,P.NameFirst,P.NameLast,LF.Shared
                FROM cpo_files F
                LEFT JOIN cpo_people P ON P.LoginUserid = F.UseridUpdate
                LEFT JOIN cpo_linkFileItemType LF ON LF.FileID = F.FileID
				WHERE
                    F.Userid = '$accountUserid' AND
                    LF.ItemID = '$itemID' AND 
                    LF.ItemType = '$itemType'
                    $sharedFilter
                    $guidFilter
				ORDER BY F.FileNameActual";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function getFileLink($fileLinkID) {
        $sql = "SELECT * from cpo_linkFileItemType WHERE LinkID = '$fileLinkID'";
        $query = $this->db->query($sql);
		return $query->row_array();
    }
    
    function getFolderPath($parentID=NULL,$accountUserid=NULL) {
        if ($parentID != NULL) {
            $where = " AND ParentID <= '$parentID' ";
        }
        $sql = "SELECT FileID,ParentID,FileTitle,FileNameActual
                FROM cpo_files
                WHERE
                    Folder  = 1 AND
                    Deleted = 0 AND                    
                    Userid  = '".$accountUserid."'
                    $where
                    ORDER BY ParentID DESC";
        //echo $sql;
        $query = $this->db->query($sql);
		return $query->result_array();
    }

    function getNumberOfItemsInFolder($nodeID,$accountUserid) {
        $sql = "SELECT COUNT(FileID) AS Items from cpo_files
                WHERE
                    Userid = '$accountUserid' AND
                    ParentID = '$nodeID' AND
                    Deleted = 0";
        $query = $this->db->query($sql);
		$resultArray = $query->row_array();
        return $resultArray['Items'];
    }

	/**
      * createFolder : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	function saveFolder($parentNodeID,$folderName,$action,$nodeID=NULL) {
        if ($action == 'add') {
            $sql = "INSERT into cpo_files (
                    ParentID,
                    Userid,
                    FileTitle,
                    FileNameActual,
                    Folder,
                    UseridUpdate,
                    DateEntry,
                    DateUpdate
                    ) values (
                    '$parentNodeID',
                    '".$this->session->userdata('accountUserid')."',
                    '".$folderName."',
                    '".$folderName."',
                    '1',
                    '".$this->session->userdata('userid')."',
                    '".date('Y-m-d G:i:s')."',
                    '".date('Y-m-d G:i:s')."'
                    )";
        } else {
            $sql = "UPDATE cpo_files SET
                        FileTitle      = '".$folderName."',
                        FileNameActual = '".$folderName."'
                    WHERE
                        FileID = '".$nodeID."'";
        }
		$query = $this->db->query($sql);
		return $this->db->insert_id();
	}

	/**
      * createFolder : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	function renameFolder($folderName,$folderID) {
		$sql = "UPDATE cpo_files SET FileNameActual = '$folderName'
				WHERE FileID = '$folderID'";
		$query = $this->db->query($sql);
	}

	/**
      * createFolder : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	function dropFileItem($itemNodeID,$targetNodeID) {
		$sql = "UPDATE cpo_files SET ParentID = '$targetNodeID'
				WHERE FileID = '$itemNodeID'";
		$query = $this->db->query($sql);
	}

	function getFileItemInformation($fileID) {
        $sql = "SELECT
                    F.FileID,F.ParentID,F.FileTitle,F.FileNameActual,F.FileServicePath,F.FileSize,F.FileType,F.FileDesc,F.Tags,
                    F.Public,F.Folder,F.FolderState,F.ImportFolder,F.DateEntry,F.DateUpdate,F.FileService,
                    P.PID,P.NameFirst,P.NameLast
                FROM cpo_files F
                LEFT JOIN cpo_people P ON P.LoginUserid = F.UseridUpdate
				WHERE F.FileID = $fileID";
		$query = $this->db->query($sql);
        return $query->row_array();
    }

    function getFileItemInformationFromLinkID($linkID) {
        $sql = "SELECT
                    F.FileID,F.ParentID,F.FileTitle,F.FileNameActual,F.FileServicePath,F.FileSize,F.FileType,F.FileDesc,F.Tags,
                    F.Public,F.Folder,F.FolderState,F.ImportFolder,F.DateEntry,F.DateUpdate,F.FileService,
                    P.PID,P.NameFirst,P.NameLast
                FROM cpo_files F
                LEFT JOIN cpo_people P ON P.LoginUserid = F.UseridUpdate
                LEFT JOIN cpo_linkFileItemType FIT ON FIT.FileID = F.FileID
				WHERE FIT.LinkID = $linkID";
		$query = $this->db->query($sql);
        return $query->row_array();
    }

	/**
      * createFolder : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	function deleteItem($itemNodeID,$itemParentID=NULL,$itemType=NULL,$deleteContents=NULL) {
		$fileCount = 0;
        $fileInfoArray = array();
		if ($itemType == 'file') {
            $itemInformationArray = File_maintenance::getFileItemInformation($itemNodeID);

			$sql = "DELETE from cpo_files WHERE FileID = '$itemNodeID'";
			$query = $this->db->query($sql);
            $fileInfoArray[$fileCount]['FileID'] = $itemNodeID;
            $fileInfoArray[$fileCount]['FileNameActual'] = $itemInformationArray['FileNameActual'];
            /*
             * Remove file from link table
             */
            File_maintenance::removeAttachedFileFromItem(NULL,$itemNodeID);
		} else {
            /*
             * If we're deleting a folder...
             */
            if ($deleteContents == 1) {
                /*
                 * Create a file tree in fileTreeArrayMaster
                 */

                /*
                 * Capture the root node first
                 */
                $fileTreeArray['ItemID']   = $itemNodeID;
                $fileTreeArray['ParentID'] = $itemParentID;
                $fileTreeArray['ItemType'] = 1;
                $fileTreeArray['FileNameActual'] = 'Root Node';
                array_push($this->fileTreeArrayMaster,$fileTreeArray);

                File_maintenance::buildItemTree($itemNodeID,$itemParentID);
                $deleteOrString = '';
                foreach($this->fileTreeArrayMaster as $fileItem) {
                    if ($fileItem['ItemType'] == 0) {
                        /*
                         * Create array of file id's
                         */
                        $fileInfoArray[$fileCount]['FileID'] = $fileItem['ItemID'];
                        $fileInfoArray[$fileCount]['FileNameActual'] = $fileItem['FileNameActual'];
                        $fileCount++;
                    }
                    $deleteOrString .= " FileID = '".$fileItem['ItemID']."' OR";
                }
                /*
                 * Remove trailing OR
                 */
                $deleteOrString = substr($deleteOrString,0,-2);

                /*
                 * Delete from file link table
                 */
                $sql = "DELETE from cpo_linkFileItemType WHERE ".$deleteOrString;
                $query = $this->db->query($sql);

                $sql = "DELETE from cpo_files WHERE ".$deleteOrString;
                $query = $this->db->query($sql);
            } else {
                /*
                 * Just delete the folder and move its contents up one level.
                 */
                $sql = "UPDATE cpo_files SET ParentID = '$itemParentID' WHERE ParentID = '$itemNodeID'";
                $query = $this->db->query($sql);

                $sql = "DELETE from cpo_files WHERE FileID = '$itemNodeID'";
                $query = $this->db->query($sql);
            }
		}        
		return $fileInfoArray;
	}

    function buildItemTree($folderID,$parentID) {
        $count = 1;
        $sql = "SELECT FileID,ParentID,FileNameActual,Folder
                FROM cpo_files
                WHERE ParentID = '$folderID'
                ORDER BY Folder ASC";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $fileTreeArray['ItemID']   = $row['FileID'];
            $fileTreeArray['ParentID'] = $row['ParentID'];
            $fileTreeArray['ItemType'] = $row['Folder'];
            $fileTreeArray['FileNameActual'] = $row['FileNameActual'];
            array_push($this->fileTreeArrayMaster,$fileTreeArray);
            $count++;

            if ($row['Folder'] == 1) {
                File_maintenance::buildItemTree($row['FileID'],$row['ParentID']);
            }
        }        
    }

    function deleteFolderContents($folderID,$fileInfoArray) {
        $a = count($fileInfoArray);
        $sql = "SELECT FileID,FileNameActual
                FROM cpo_files
                WHERE
                    ParentID = '$folderID' AND
                    Folder = 0";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $fileInfoArray[$a]['FileID'] = $row['FileID'];
            $fileInfoArray[$a]['FileNameActual'] = $row['FileNameActual'];
            $a++;
        }

        $sql = "DELETE from cpo_files WHERE ParentID = '$folderID' AND Folder = 0";
        $query = $this->db->query($sql);

        return $fileInfoArray;
    }

	/**
      * createFolder : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	function checkForDuplicateFileName($fileName) {
		$sql = "SELECT COUNT(FileID) AS NoOfSameFiles FROM cpo_files
				WHERE
				FileNameActual = '$fileName' AND
				Userid = '".$this->session->userdata('accountUserid')."'";
		$query = $this->db->query($sql);
		$row = $query->row_array();
		if ($row['NoOfSameFiles']>0) {
			$fileNameArray = explode(strrchr($fileName, '.'),$fileName);
			$fileNamePre = $fileNameArray[0];
			$extension   = strrchr($fileName, '.');

			$nextRev = $row['NoOfSameFiles']+1;
			$newFileName = $fileNamePre.'_['.time().']'.$extension;
		} else {
			$newFileName = $fileName;
		}
		return $newFileName;
	}

    function checkForDuplicateFileServiceFile($fileName,$filePath,$accountUserid) {
        $sql = "SELECT FileID FROM cpo_files
                WHERE
                    FileNameActual = '$fileName' AND
                    FileServicePath = '$filePath' AND
                    Userid = '$accountUserid'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
            return FALSE;
        } else {
            $row = $query->row_array();
            return $row['FileID'];
        }
    }

    function checkForAttachedFile($fileID,$itemID,$itemType,$guid=null) {
        if ($guid != null) {
            $sql = "SELECT * FROM cpo_linkFileItemType
                    WHERE
                    FileID   = '$fileID' AND
                    ItemID   = '0' AND
                    ItemType = '$itemType' AND
                    ImportGUID = '$guid'";
        } else {
            $sql = "SELECT * FROM cpo_linkFileItemType
                    WHERE
                    FileID   = '$fileID' AND
                    ItemID   = '$itemID' AND
                    ItemType = '$itemType'";
        }    
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
            return true;
        } else {
            return false;
        }
    }

	/**
      * attachFilesToItem : attach files to an item like a project, task, message, etc.
	  *
      * @access	public
	  * @param  int    $itemID       ID of the item to which we are attaching files.
	  * @param  string $itemType     Type of item to which we are attaching files (project, task, message, etc.)
	  * @return void                 For now
      */
	function attachFilesToItem($fileID,$itemID,$itemType,$userid,$guid=null) {
		$sql = "INSERT into cpo_linkFileItemType (
				FileID,
				ItemID,
				ItemType,
                UseridUpdate,
                ImportGUID
				) values (
				'$fileID',
				'$itemID',
				'$itemType',
                '$userid',
                '$guid'
				)";
		$query = $this->db->query($sql);
        $fileLinkID = $this->db->insert_id();
        return $fileLinkID;
	}

	function removeAttachedFileFromItem($fileLinkID=NULL,$fileID=NULL,$itemID=NULL,$itemType=NULL) {
        $sql = "DELETE from cpo_linkFileItemType WHERE ";
        if ($fileLinkID>0) {
            $sql .= "LinkID = '$fileLinkID' AND ";
        } 
        if ($fileID>0) {
            $sql .= "FileID = '$fileID' AND ";
        }
        if (!empty($itemID)) {
            $sql .= "ItemID   = '$itemID' AND ";
        }
        if (!empty($itemType)) {
            $sql .= "ItemType = '$itemType' AND ";
        }

        /*
         * Remove trailing AND
         */
        if ($fileLinkID != NULL || $fileID != NULL || $itemID != NULL || $itemType != NULL) {
            $sql = substr($sql,0,-4);
            $query = $this->db->query($sql);
        }
	}

	/**
      * getNumberOfFilesForSomething
	  *
      * @access	public
	  * @param  int    $itemID ID of the item for which we want the file count
	  * @param  string $itemType the type of item for which we want the file count
	  *                This could be project, task, message, etc.
      * @return int    The number of files associated with this item.
      */
	function getNumberOfFilesForSomething($itemID,$itemType,$shared=NULL) {
        $sharedFilter = '';
        if ($shared == TRUE) {
            $sharedFilter = " AND Shared = 1 ";
        }
		$sql = "SELECT COUNT(FileID) AS NoOfFiles
				FROM cpo_linkFileItemType
				WHERE
					ItemID   = $itemID AND
					ItemType = '$itemType'
                    $sharedFilter";
		$query = $this->db->query($sql);
		$row = $query->row_array();
		return $row['NoOfFiles'];
	}

    function getFileSpaceUsage($accountUserid=NULL) {
        if (empty($accountUserid)) {
            $accountUserid = $this->session->userdata('accountUserid');
        }
        $sql = "SELECT SUM(FileSize) AS FileUse FROM cpo_files
                WHERE Userid = '$accountUserid'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $fileUseTotal = $row['FileUse'];
        return $fileUseTotal;
    }

    function updateFileLinkWithItemID($fileLinkID,$itemID) {
        $sql = "UPDATE cpo_linkFileItemType SET
                ItemID = '$itemID'
                WHERE
                LinkID = '$fileLinkID'";
        $query = $this->db->query($sql);
    }

    function getImportFolder($accountUserid) {
        $sql = "SELECT * from cpo_files WHERE ImportFolder = 1 AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
            return FALSE;
        } else {
            return $query->row_array();
        }
    }

    function createImportFolder($data) {
        $sql = "INSERT into cpo_files (
				Userid,
                FileNameActual,
				Folder,
                ImportFolder,
				UseridUpdate,
				DateEntry,
				DateUpdate,
                ImportGUID
				) values (
				'".$data['accountUserid']."',
                '".$data['folderName']."',
				'1',
                '1',
				'".$data['userid']."',
				'".date('Y-m-d G:i:s')."',
				'".date('Y-m-d G:i:s')."',
                '".$this->session->userdata('importGUID')."'
				)";
		$query = $this->db->query($sql);
		return $this->db->insert_id();
    }

    function updateFileShareStatus($fileLinkID) {
        /*
         * First check current status.
         */
        $sql = "SELECT Shared from cpo_linkFileItemType WHERE LinkID = '$fileLinkID'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        if ($row['Shared'] == 1) {
            $newShareStatus = 0;
        } else {
            $newShareStatus = 1;
        }

        $sql = "UPDATE cpo_linkFileItemType SET Shared = '$newShareStatus' WHERE LinkID = '$fileLinkID'";
        $query = $this->db->query($sql);
        return $newShareStatus;
    }

    function makeTemplateRecords($templateProjectID,$newProjectID,$userid) {
        $sql = "SELECT * from cpo_linkFileItemType WHERE ItemID = $templateProjectID AND ItemType = 'project'";
        $query = $this->db->query($sql);

        if ($query->num_rows()>0) {
            foreach ($query->result_array() as $row) {
                $sql = "INSERT into cpo_linkFileItemType (
                        FileID,
                        ItemID,
                        ItemType,
                        IsTemplate,
                        UseridUpdate
                        ) values (
                        '".$row['FileID']."',
                        '".$newProjectID."',
                        '".$row['ItemType']."',
                        '1',
                        '".$userid."'
                        )";
                $query = $this->db->query($sql);
            }
        }
    }
}
?>
