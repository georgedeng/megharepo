<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Timesheet_update extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function saveTimesheetRecordFromJobTimer($data) {
        /*
         * 12/07/2011: Saving DateClockEnd when 'start' because
         * sometimes DateClockEnd remains 0000-00-00 and screws up the
         * timesheet view.
         */
	$timeSheetID = $data['timeSheetID'];
        if ($data['action'] == 'start') {
                if ($data['timeSheetID'] == 0) {
                        $sql = "INSERT into cpo_project_time (
                UseridAccount,
                                        Userid,
                                        ProjectID,
                                        TaskID,
                                        Start,
                                        DateClockStart,
                DateClockEnd
                                        ) values (
                '".$data['accountUserid']."',
                                        '".$data['userid']."',
                                        '".$data['projectID']."',
                                        '".$data['taskID']."',
                                        '".time()."',
                                        '".mysqldate_to_localdate()."',
                '".mysqldate_to_localdate()."'
                                        )";
                        $query = $this->db->query($sql);
                        $timeSheetID = $this->db->insert_id();
                }
        } elseif ($data['action'] == 'save') {
                $sql = "UPDATE cpo_project_time SET
                                ProjectID    = '".$data['projectID']."',
                                TaskID       = '".$data['taskID']."',
                                ElapsedTime  = '".$data['elapsedTime']."',
                                Comments     = '".$data['comments']."',
                                Billable     = '".$data['billable']."'
                                WHERE
                                TimeSheetID = '".$data['timeSheetID']."'";
                $query = $this->db->query($sql);
        } elseif ($data['action'] == 'stop') {
            /*
             * 03/08/2011 Added updating of ElapsedTime and Billable because
             * little clock icons in task view were not saving elapsed time
             * when stopped. Elapsed time would only get saved with the
             * 'save' command above.
             */
            $sql = "UPDATE cpo_project_time SET
                        Stop         = '".time()."',
                        ElapsedTime  = '".$data['elapsedTime']."',
                        DateClockEnd = '".mysqldate_to_localdate()."',
                        Billable     = '".$data['billable']."'
                    WHERE
                        TimeSheetID = '".$data['timeSheetID']."'";
            $query = $this->db->query($sql);
	} elseif ($data['action'] == 'delete') {
            Timesheet_update::deleteTimesheetRecord($timeSheetID);
	} elseif ($data['action'] == 'saveFromDesktop') {
            if ($timeSheetID>0) {
                $sql = "UPDATE cpo_project_time SET
                            ProjectID    = '".$data['projectID']."',
                            TaskID       = '".$data['taskID']."',
                            ElapsedTime  = '".$data['elapsedTime']."',
                            Comments     = '".$data['comments']."',
                            Billable     = '".$data['billable']."'
                            WHERE
                            TimeSheetID = '".$data['timeSheetID']."'";
                $query = $this->db->query($sql);
                return $sql;
            } else {
                $sql = "INSERT into cpo_project_time (
                        UseridAccount,
                        Userid,
                        ProjectID,
                        TaskID,
                        ElapsedTime,
                        DateClockEnd,
                        Comments,
                        Billable
                    ) values (
                        '".$data['accountUserid']."',
                        '".$data['userid']."',
                        '".$data['projectID']."',
                        '".$data['taskID']."',
                        '".$data['elapsedTime']."',
                        '".$data['dateTime']."',
			'".$data['comments']."',
                        '".$data['billable']."'
                    )";
                $query = $this->db->query($sql);
                $timeSheetID = $this->db->insert_id();
            }    
        }
	return $timeSheetID;
    }

    function saveTimesheetRecordFromTimesheet($data) {
        if ($data['timeSheetID']>0) {
            $timeSheetID = $data['timeSheetID'];

            if ($data['elapsedTime'] > 0) {
                $sql = "UPDATE cpo_project_time SET
                        ProjectID      = '".$data['projectID']."',
                        TaskID         = '".$data['taskID']."',
                        ElapsedTime    = '".$data['elapsedTime']."',
                        DateClockStart = '0000-00-00',     
                        DateClockEnd   = '".$data['date']."',
                        Comments       = '".$data['comments']."',
                        Billable       = '".$data['billable']."'
                        WHERE
                        TimeSheetID = '".$timeSheetID."'";
                $query = $this->db->query($sql);
            } else {
                Timesheet_update::deleteTimesheetRecord($timeSheetID);
            }
        } else {
            $sql = "INSERT into cpo_project_time (
                    UseridAccount,
                    Userid,
                    ProjectID,
                    TaskID,
                    ElapsedTime,
                    DateClockEnd,
                    Comments,
                    Billable
                    ) values (
                    '".$data['accountUserid']."',
                    '".$data['userid']."',
                    '".$data['projectID']."',
                    '".$data['taskID']."',
                    '".$data['elapsedTime']."',
                    '".$data['date']."',
                    '".$data['comments']."',
                    '".$data['billable']."'
                    )";
            $query = $this->db->query($sql);
            $timeSheetID = $this->db->insert_id();
        }
        
        return $timeSheetID;
    }

    function saveTimesheetFromImport($data) {
        $prevTimesheetID = '';
        if (isset($data['prevTimesheetID'])) {
            $prevTimesheetID = $data['prevTimesheetID'];
        }
        $sql = "INSERT into cpo_project_time (
                PrevTimesheetID,
                Userid,
                ProjectID,
                TaskID,
                ElapsedTime,
                DateClockStart,
                DateClockEnd,
                Comments,
                ImportGUID
                ) values (
                '$prevTimesheetID',
                '".$data['userid']."',
                '".$data['projectID']."',
                '".$data['taskID']."',
                '".$data['elapsedTime']."',
                '".$data['dateClockStart']."',
                '".$data['dateClockEnd']."',
                '".$data['comments']."',
                '".$this->session->userdata('importGUID')."'
                )";
        $query = $this->db->query($sql);
        $timeSheetID = $this->db->insert_id();
    }

	function saveTimesheetComments($comments,$billable,$timeSheetID) {
		$sql = "UPDATE cpo_project_time 
				SET
					Comments = '$comments',
					Billable = '$billable'
				WHERE TimeSheetID = '$timeSheetID'";
		$query = $this->db->query($sql);
	}

	function deleteTimesheetRecord($timeSheetID=NULL,$tagForDelete=TRUE) {
		if ($tagForDelete == TRUE) {
			$sql = "UPDATE cpo_project_time SET Deleted = 1 WHERE TimeSheetID = '$timeSheetID'";
			$query = $this->db->query($sql);
        } else {
			$sql = "DELETE FROM cpo_project_time WHERE TimeSheetID = '$timeSheetID'";
			$query = $this->db->query($sql);
        }
	}
    
    function markTimesheetBilled($timesheetID, $invoiceID) {
        $sql = "UPDATE cpo_project_time SET BilledInvoiceID = $invoiceID WHERE TimesheetID = $timesheetID";
        $query = $this->db->query($sql);
    }
    
    function markTimesheetsUnbilled($invoiceID) {
        $sql = "UPDATE cpo_project_time SET BilledInvoiceID = '' WHERE BilledInvoiceID = $invoiceID";
        $query = $this->db->query($sql);
    }
}
?>
