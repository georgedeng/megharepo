<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_notes extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function saveNote($data) {
		if ($data['action'] == 'add') {
            if (!empty($data['dateNote'])) {
                $dateNote = $data['dateNote'];
            } else {
                $dateNote = date('Y-m-d G:i:s');
            }
			$sql = "INSERT into cpo_notes (
					Userid,
					ItemID,
					ItemType,
					Note,
					DateNote,
					OnWidget,
					Public,
                    UseridEntry,
					UseridUpdate,
					DateEntry,
                    ImportGUID
					) values (
					'".$data['accountUserid']."',
					'".$data['itemID']."',
					'".$data['itemType']."',
					'".$data['note']."',
					'".$dateNote."',
					'".$data['onWidget']."',
					'".$data['public']."',
                    '".$data['userid']."',
					'".$data['userid']."',
					'".date('Y-m-d G:i:s')."',
                    '".$this->session->userdata('importGUID')."'
					)";
			$query = $this->db->query($sql);
    		$noteID = $this->db->insert_id();
		} else {
			$sql = "UPDATE cpo_notes SET
						Note         = '".$data['note']."',
						Public       = '".$data['public']."',
						UseridUpdate = '".$data['userid']."',
						DateUpdate   = '".date('Y-m-d G:i:s')."'
					WHERE
						NID = '".$data['noteID']."'";
			$query = $this->db->query($sql);
    		$noteID = $data['noteID'];
		}
		return $noteID;
    }

    function getNotes($onHome=0,$itemID=NULL,$itemType=NULL,$client=FALSE,$start=NULL,$limit=5,$accountUserid,$userid,$viewPublic=FALSE) {
        $where = '';
        $whereClient = '';
        if ($onHome == 1) {
            $where = " N.OnWidget = 1 ";
        } elseif ($itemID>0) {
            $where = " (N.ItemID = '$itemID' AND N.ItemType = '$itemType') ";
        }

        if ($viewPublic == TRUE) {
            $wherePublic = " ((N.UseridEntry = '$userid' AND N.Userid = '$accountUserid') OR (N.Userid = '$accountUserid' AND N.Public = 1)) ";
        } else {
            $wherePublic = " N.UseridEntry = '$userid' AND N.Userid = '$accountUserid' ";
        }

        if ($client === TRUE) {
            $whereClient = " N.Public = 1 ";
            $wherePublic = " N.Userid = '$accountUserid' AND ";
        }

        if (!empty($where)) {
            $whereFinal = $where.' AND '.$wherePublic;
        } else {
            $whereFinal = $wherePublic;
        }
        $sql = "SELECT * from cpo_notes N
                LEFT JOIN cpo_people P ON P.LoginUserid = N.UseridEntry
                WHERE                    
                    $whereFinal $whereClient
				AND N.Deleted = 0
                ORDER BY N.DateNote DESC LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getNumberOfNotesForSomething($itemID=NULL,$itemType=NULL,$userid=NULL,$accountUserid=NULL) {
        if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
            $sql = "SELECT COUNT(NID) AS TotalNotes FROM cpo_notes
                    WHERE
                    Userid = '$accountUserid' AND
                    Public = 1 AND
                    OnWidget = 0 AND
                    Deleted = 0";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['TotalNotes'];
        } else {
            
        }
    }

	function deleteNote($noteID,$tagForDelete=TRUE,$userid) {
		if ($tagForDelete == TRUE) {
             $sql = "UPDATE cpo_notes SET UseridUpdate = '$userid', Deleted = 1 WHERE NID = '$noteID'";
             $query = $this->db->query($sql);
         } else {
             $sql = "DELETE from cpo_notes WHERE NID = '$noteID'";
             $query = $this->db->query($sql);
         }
	}

    function restoreNote($noteID,$userid) {
        $sql = "UPDATE cpo_notes SET UseridUpdate = '$userid', Deleted = 0 WHERE NID = '$noteID'";
        $query = $this->db->query($sql);
    }

    function makeTemplateRecords($templateProjectID,$newProjectID,$userid) {
        $sql = "SELECT * from cpo_notes WHERE ItemID = $templateProjectID AND ItemType = 'P' AND Deleted = 0";
        $query = $this->db->query($sql);

        if ($query->num_rows()>0) {
            foreach ($query->result_array() as $row) {
                $sql = "INSERT into cpo_notes (
                        Userid,
                        ItemID,
                        ItemType,
                        NoteTitle,
                        Note,
                        DateNote,
                        OnWidget,
                        Public,
                        IsTemplate,
                        UseridEntry,
                        DateEntry
                        ) values (
                        '".$row['Userid']."',
                        '".$newProjectID."',
                        '".$row['ItemType']."',
                        '".$row['NoteTitle']."',
                        '".$row['Note']."',
                        '".date('Y-m-d G:i:s')."',
                        '".$row['OnWidget']."',
                        '".$row['Public']."',
                        '1',
                        '".$userid."',
                        '".date('Y-m-d')."'
                        )";
                $query = $this->db->query($sql);
            }
        }
    }
}
?>