<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_user_metrics extends Model {
    function Admin_user_metrics() {
        // Call the Model constructor
        parent::Model();
    }

    function reportNewAccountsByDate($year) {
        $sql = "SELECT
                COUNT(AccountID) AS Accounts, AccountLevel, MONTHNAME(DateSignup) AS MonthName, MONTH(DateSignup) AS MonthNumber
                FROM cpo_account_profile
                WHERE YEAR(DateSignup) = '$year'
                GROUP BY AccountLevel,MONTHNAME(DateSignup)
                ORDER BY DateSignup";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function reportAccountsPieByDate($year,$active=FALSE) {
        $activeSQL = '';
        if ($active == TRUE) {
            $activeSQL = " AND IsActive = 1 ";
        }
        $sql = "SELECT
                COUNT(AccountID) Accounts,SUM(AccountPrice) AS TotalPrice,AccountLevel
                FROM cpo_account_profile
                WHERE 
                YEAR(DatePaid) = '$year' AND
                PaymentProfileID <> ''
                $activeSQL
                GROUP BY AccountLevel";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function getDataLoginChart($dtateStart=null,$dateEnd=null) {
    	/*
    	 * Data for no logins
    	 */
		$sql = "SELECT COUNT(UID) AS Logins from cpo_login WHERE Logins < 1 AND User_Type = 1";
		$query = $this->db->query($sql); 
		$row = $query->row_array();
		$returnArray['data'][0] = $row['Logins'];
		$returnArray['label'][0] = 'No logins ('.$row['Logins'].')';

		/*
    	 * Data for 1 login
    	 */
		$sql = "SELECT COUNT(UID) AS Logins from cpo_login WHERE Logins = 1 AND User_Type = 1";
		$query = $this->db->query($sql); 
		$row = $query->row_array();
		$returnArray['data'][1] = $row['Logins'];
		$returnArray['label'][1] = '1 login ('.$row['Logins'].')';
		
		/*
    	 * Data for 2-5 login
    	 */
		$sql = "SELECT COUNT(UID) AS Logins from cpo_login WHERE (Logins > 1 AND Logins < 6) AND User_Type = 1";
		$query = $this->db->query($sql); 
		$row = $query->row_array();
		$returnArray['data'][2] = $row['Logins'];
		$returnArray['label'][2] = '2-5 logins ('.$row['Logins'].')';
		
		/*
    	 * Data for 6-20 login
    	 */
		$sql = "SELECT COUNT(UID) AS Logins from cpo_login WHERE (Logins > 5 AND Logins < 21) AND User_Type = 1";
		$query = $this->db->query($sql); 
		$row = $query->row_array();
		$returnArray['data'][3] = $row['Logins'];
		$returnArray['label'][3] = '6-20 logins ('.$row['Logins'].')';
		
		/*
    	 * Data for 21-50 login
    	 */
		$sql = "SELECT COUNT(UID) AS Logins from cpo_login WHERE (Logins > 20 AND Logins < 51) AND User_Type = 1";
		$query = $this->db->query($sql); 
		$row = $query->row_array();
		$returnArray['data'][4] = $row['Logins'];
		$returnArray['label'][4] = '21-50 logins ('.$row['Logins'].')';
		
		/*
    	 * Data for > 50 login
    	 */
		$sql = "SELECT COUNT(UID) AS Logins from cpo_login WHERE Logins > 50 AND User_Type = 1";
		$query = $this->db->query($sql); 
		$row = $query->row_array();
		$returnArray['data'][5] = $row['Logins'];
		$returnArray['label'][5] = 'More than 50 logins ('.$row['Logins'].')';
		
		return $returnArray;
    }
    
    function getDataLoginSignup($year=null) {
    	if (empty($year)) {
    		/*
    		 * Then we're getting data for years
    		 */
			$sql = "SELECT MIN(Signup_Date) AS MinDate from cpo_user_profile";
			$query = $this->db->query($sql); 
			$row = $query->row_array();
			$dateArray = explode('-',$row['MinDate']);
			$startYear = $dateArray[0];
			$endYear = date('Y');
			$b=0;
			for($a=$startYear;$a<=$endYear;$a++) {
				$sql = "SELECT COUNT(UP.ID) AS Signups 
						from cpo_user_profile UP
						LEFT JOIN
							cpo_login L ON UP.Userid = L.UID
						WHERE 
						YEAR(UP.Signup_Date) = '$a' AND
						L.User_Type = 1";	
				$query = $this->db->query($sql); 
				$row = $query->row_array();
				$returnArray['data'][$b] = $row['Signups'];
				$returnArray['label'][$b] = $a;
				$b++;
			}
			return $returnArray;
    	}
    }

    function getCumulativeStats() {
        $sql = "SELECT COUNT(*) AS TotalUsers FROM cpo_account_profile WHERE IsActive = 1";
        $query = $this->db->query($sql);
        $row1 = $query->row_array();

        $sql = "SELECT SUM(AccountPrice) AS TotalAccountValue FROM cpo_account_profile WHERE IsActive = 1 AND PaymentProfileID <> ''";
        $query = $this->db->query($sql);
        $row2 = $query->row_array();

        $sql = "SELECT COUNT(*) AS TotalPayingUsers FROM cpo_account_profile WHERE IsActive = 1 AND AccountPrice > 0 AND PaymentProfileID <> ''";
        $query = $this->db->query($sql);
        $row3 = $query->row_array();

        return array(
                        'TotalUsers'        =>$row1['TotalUsers'],
                        'TotalAccountValue' =>$row2['TotalAccountValue'],
                        'TotalPayingUsers'  =>$row3['TotalPayingUsers']
                    );
    }
}    
?>