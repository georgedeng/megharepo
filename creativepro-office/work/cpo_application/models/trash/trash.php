<?
class Trash extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    /**
	 * getNumberOfItemsInTrash : gets number of items tagged for deletion.
	 *
	 * @access public
	 * @return int    $trashItems the number of items tagged for deletion.
	 */
    function getNumberOfItemsInTrash($userid=NULL,$accountUserid=NULL) {
         $trashItems = 0;

         $trashTableArray = array(
                    'cpo_calendar',
                    'cpo_calendar_events',
                    'cpo_clients',
                    'cpo_expense',
                    'cpo_invoice',
                    'cpo_messages',
                    'cpo_notes',
                    'cpo_people',
                    'cpo_project_tasks',
                    'cpo_project'
         );

         foreach($trashTableArray as $table) {
             if ($userid > 0) {
                 if ($table == 'cpo_project_tasks') {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from $table WHERE Milestone = 0 AND UseridUpdate = '$userid' AND Deleted = 1";
                 } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from $table WHERE UseridUpdate = '$userid' AND Deleted = 1";
                 }
             } else {
                 if ($table == 'cpo_messages') {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from $table WHERE UIDFrom = '$accountUserid' AND Deleted = 1";
                 } elseif ($table == 'cpo_project_tasks') {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from $table WHERE Milestone = 0 AND UseridUpdate = '$accountUserid' AND Deleted = 1";
                 } elseif ($table == 'cpo_notes') {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from $table WHERE UseridUpdate = '$userid' AND Deleted = 1";
                 } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from $table WHERE Userid = '$accountUserid' AND Deleted = 1";
                 }
             }
             $query = $this->db->query($sql);
             $row = $query->row_array();
             $trashItems = $trashItems+$row['Deleted'];
         }
         return $trashItems;
    }

    function getNumberOfItemsInTrashByType($itemType,$userid=NULL,$accountUserid=NULL) {
        $sql = '';
        switch ($itemType) {
            case 'calendar':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_calendar WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_calendar WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'event':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_calendar_events WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_calendar_events WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'client':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_clients WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_clients WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'expense':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_expense WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_expense WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'invoice':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_invoice WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_invoice WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }
                break;
            case 'message':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_messages WHERE UIDFrom = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_messages WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'note':
                $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_notes WHERE Userid = '$userid' AND Deleted = 1";
                break;
            case 'contact':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_people WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_people WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'task':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_project_tasks WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_project_tasks WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }
                break;
            case 'project':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_project WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_project WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }
                break;
        }
        $query = $this->db->query($sql);
        $row = $query->row_array();

        return $row['Deleted'];
    }

    function getItemsInTrashByType($itemType,$userid=NULL,$accountUserid=NULL) {
        $sql = '';
        switch ($itemType) {
            case 'calendar':
                if ($accountUserid > 0) {
                    $sql = "SELECT CalendarID AS ID,Title from cpo_calendar WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT CalendarID AS ID,Title from cpo_calendar WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'event':
                if ($accountUserid > 0) {
                    $sql = "SELECT EventID AS ID,Title from cpo_calendar_events WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT EventID AS ID,Title from cpo_calendar_events WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'client':
                if ($accountUserid > 0) {
                    $sql = "SELECT ClientID AS ID,Company AS Title from cpo_clients WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT ClientID AS ID,Company AS Title from cpo_clients WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'expense':
                if ($accountUserid > 0) {
                    $sql = "SELECT ExpenseID AS ID,Title from cpo_expense WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT ExpenseID AS ID,Title from cpo_expense WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'invoice':
                if ($accountUserid > 0) {
                    $sql = "SELECT InvoiceID AS ID,Title from cpo_invoice WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT InvoiceID AS ID,Title from cpo_invoice WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'message':
                if ($accountUserid > 0) {
                    $sql = "SELECT MID AS ID,Message AS Title from cpo_messages WHERE UIDFrom = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT MID AS ID,Message AS Title from cpo_messages WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'note':
                $sql = "SELECT NID AS ID,Note AS Title from cpo_notes WHERE Userid = '$userid' AND Deleted = 1 ORDER BY Title";
                break;
            case 'contact':
                if ($accountUserid > 0) {
                    $sql = "SELECT PID AS ID,CONCAT(NameFirst,' ',NameLast,' ',Company) AS Title from cpo_people WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY NameLast";
                } else {
                    $sql = "SELECT PID AS ID,CONCAT(NameFirst,' ',NameLast,' ',Company) AS Title from cpo_people WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY NameLast";
                }
                break;
            case 'task':
                if ($accountUserid > 0) {
                    $sql = "SELECT TaskID AS ID, Title from cpo_project_tasks WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT TaskID AS ID, Title from cpo_project_tasks WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'project':
                if ($accountUserid > 0) {
                    $sql = "SELECT ProjectID AS ID,Title from cpo_project WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT ProjectID AS ID,Title from cpo_project WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
?>