<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Accounts extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getAccountInformation($accountID,$isActive=null) {
        $sql = "SELECT * from cpo_account_profile 
                WHERE AccountID = '$accountID'";
        if ($isActive == 1) {
    		$sql .= " AND IsActive = '1'";
    	}
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getAllAccountProfiles() {
        $sql = "SELECT AccountID,Company,Email,AccountLevel,AccountPrice,Account_No,PaymentProfileID
                FROM cpo_account_profile WHERE 
                PaymentProfileID IS NOT NULL AND
                PaymentProfileID <> '000000' AND
                IsActive = 1";
        $query = $this->db->query($sql);
		return $query->result_array();
    }

    function updateAccountWithPaymentInformation($data) {
        $sql = "UPDATE cpo_account_profile SET
                    AccountLevel         = '".$data['accountLevel']."',
                    AccountPrice         = '".$data['accountPrice']."',
                    PaymentProfileID     = '".$data['profileID']."',
                    IsPaid               = '1',
                    BillingNameFirst     = '".$data['billingNameFirst']."',
                    BillingNameLast      = '".$data['billingNameLast']."',
                    BillingAddress       = '".$data['billingAddress']."',
                    BillingCity          = '".$data['billingCity']."',
                    BillingStateRegion   = '".$data['billingState']."',
                    BillingZipPostalCode = '".$data['billingZip']."',
                    BillingCountry       = '".$data['billingCountry']."',
                    BillingCurrency      = '".$data['billingCurrency']."',
                    DatePaid             = '".date('Y-m-d')."'
                WHERE
                AccountID = '".$data['accountID']."'";
        $query = $this->db->query($sql);
    }

    function enterPaymentErrorLog($errorData) {
        $sql = "INSERT into cpo_errors (
                DateError,
                AccountUserid,
                ErrorType,
                ErrorCode,
                ErrorMessage,
                ErrorPage
                ) values (
                '".date('Y-m-d G:i:s')."',
                '".$errorData['accountUserid']."',
                '".$errorData['errorType']."',
                '".$errorData['errorCode']."',
                '".$errorData['errorMessage']."',
                '".$errorData['errorPage']."'
                )";
        $query = $this->db->query($sql);
    	return $this->db->insert_id();
    }

    function checkAccountLevelStatus($accountUserid) {
        $sql = "SELECT AccountLevel,PaymentProfileID,IsPaid from cpo_account_profile WHERE AccountID = '$accountUserid'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function changeAccountPaidStatus($accountUserid,$newStatus) {
        $sql = "UPDATE cpo_account_profile SET IsPaid = '$newStatus' WHERE AccountID = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function cancelAccount($accountUserid) {
        /*
         * Deactivate account owner login
         */
        $sql = "UPDATE cpo_login SET Active = '0' WHERE UID = '$accountUserid'";
        $query = $this->db->query($sql);

        /*
         * Deactivate other account members
         */
        $sql = "UPDATE cpo_login SET Active = '0' WHERE UseridAccount = '$accountUserid'";
        $query = $this->db->query($sql);

        /*
         * Deactivate account profile
         */
        $sql = "UPDATE cpo_account_profile SET IsActive = '0' WHERE AccountID = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function getAccountInfoFromSubdomain($subdomain) {
        $sql = "SELECT A.AccountID,A.Company,A.Email,A.URL
                FROM cpo_account_profile A
                WHERE A.SubDomain = '".$subdomain."'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
        }
    }

    function getAccountInfoFromEmail($email=NULL) {
        $sql = "SELECT L.UID,L.UseridAccount,L.AuthString,L.RoleID,R.RolePermissions,P.PID,P.NameFirst,P.NameLast from cpo_login L
                LEFT JOIN cpo_user_roles R ON R.RoleID = L.RoleID
                LEFT JOIN cpo_people P ON P.LoginUserid = L.UID
                WHERE L.Userid = '$email' AND
                L.User_Type > 1 AND
                L.Active = 1
                LIMIT 1";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
        }
    }

    function getProductAccountInformation($accountID) {
        $sql = "SELECT * from cpo_account_profile_product WHERE AccountID = $accountID";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getNewSelfHostedOrderNumber() {
        $sql = "SELECT MAX(OrderNumber) AS MaxOrderNumber from cpo_account_profile_product";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['MaxOrderNumber']+1;
    }

    function saveSelfHostedAccountInformation($data) {
        $sql = "INSERT into cpo_account_profile_product (
                Company,
                Address1,
                City,
                State,
                Zip,
                Email,
                URL,
                Country,
                Currency,
                ProductLevel,
                ProductPrice,
                OrderNumber,
                InstallationKey,
                BillingNameFirst,
                BillingNameLast,
                BillingAddress,
                BillingCity,
                BillingStateRegion,
                BillingZipPostalCode,
                BillingCountry,
                BillingCurrency,
                DatePurchase,
                AcceptTerms,
                GetCPOEmail,
                NumberOfLicenses
                ) values (
                '".$data['company']."',
                '".$data['billingAddress']."',
                '".$data['billingCity']."',
                '".$data['billingState']."',
                '".$data['billingZip']."',
                '".$data['email']."',
                '".$data['url']."',
                '".$data['billingCountry']."',
                '".$data['billingCurrency']."',
                '0',
                '".$data['amount']."',
                '".$data['orderNumber']."',
                '".$data['installationKey']."',
                '".$data['billingNameFirst']."',
                '".$data['billingNameLast']."',
                '".$data['billingAddress']."',
                '".$data['billingCity']."',
                '".$data['billingState']."',
                '".$data['billingZip']."',
                '".$data['billingCountry']."',
                '".$data['billingCurrency']."',
                '".date('Y-m-d')."',
                '1',
                '1',
                '5'
                )";
        $query = $this->db->query($sql);
        return $this->db->insert_id();
    }

    function saveSelfHostedPaymentInformation($data) {
        $sql = "INSERT into cpo_product_payment (
                AccountID,
                ProductTitle,
                PaymentAmount,
                DatePaid,
                IsUpdate
                ) values (
                '".$data['accountID']."',
                '".$data['productTitle']."',
                '".$data['amount']."',
                '".date('Y-m-d')."',
                '".$data['isUpdate']."'
                )";
        $query = $this->db->query($sql);
        return $this->db->insert_id();
    }

    function trySelfHostedLogin($orderNo,$installationKey) {
        $sql = "SELECT * from cpo_account_profile_product WHERE
                OrderNumber = '$orderNo' AND
                InstallationKey = '$installationKey'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
        }
    }

    function checkSubscriptionStatus($accountID,$datePaid) {
        $sql = "SELECT * from cpo_product_payment
                WHERE
                    AccountID = '$accountID' AND
                    IsUpdate  = 1 AND
                    DatePaid >= '$datePaid'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();;
        }
    }
    
    function checkPaymentEntry($accountUserid,$profileID,$paymentDate) {
        $sql = "SELECT * from cpo_account_payments
                WHERE
                    AccountID = '$accountUserid' AND
                    ProfileID = '$profileID' AND
                    DatePayment = '$paymentDate'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return true;
        }
    }
    
    function savePaymentEntry($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT into cpo_account_payments (
                    AccountID,
                    ProfileID,
                    DatePayment,
                    PaymentAmount,
                    Name,
                    Address,
                    City,
                    State,
                    Zip,
                    Country,
                    CardType,
                    CardNumber,
                    Currency
                ) values (
                    '".$data['accountID']."',
                    '".$data['payProfileID']."',
                    '".$data['payDate']."',        
                    '".$data['payAmt']."', 
                    '".$data['payName']."', 
                    '".$data['payAddress']."',
                    '".$data['payCity']."', 
                    '".$data['payState']."', 
                    '".$data['payZip']."',  
                    '".$data['payCountry']."',
                    '".$data['payCardType']."',
                    '".$data['payCardNumber']."',
                    '".$data['payCurrency']."'
                )";
            $query = $this->db->query($sql);
            $paymentID = $this->db->insert_id();
            return $paymentID;
        }
    }
    
    function getAccountPayments($accountID) {
        $sql = "SELECT * from cpo_account_payments WHERE AccountID = $accountID ORDER BY DatePayment DESC";
        $query = $this->db->query($sql);			
		return $query->result_array();
    }
    
    function getAccountPayment($paymentID) {
        $sql = "SELECT * from cpo_account_payments WHERE PaymentID = $paymentID";
        $query = $this->db->query($sql);			
		return $query->row_array();
    }
}
?>
