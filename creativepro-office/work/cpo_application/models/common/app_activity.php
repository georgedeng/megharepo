<?php
class App_activity extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

	function logAppActivity($data) {
		$sql = "INSERT into cpo_activity (
                Userid,		
                UserPID,
                UserType,
				Activity,
				ItemID,
				ItemType,
                ItemTitle,
                DateActivity,
                ReportToUser
				) values (
                '".$data['AccountUserid']."',
                '".$data['UserPID']."',
                '".$data['UserType']."',
				'".$data['Activity']."',
				'".$data['ItemID']."',
				'".$data['ItemType']."',
				'".$data['ItemTitle']."',
				'".date('Y-m-d G:i:s')."',
                '".$data['ReportToUser']."'    
				)";
		$query = $this->db->query($sql);
	}

	/**
	* getTags : Retrieve tags for a given site area.
    *           Site areas: Projects,Library,Users,Expense,etc.
	*
	* @access public
	* @param  int     $userid Userid of account owner
    * @param  string  $siteArea Module for which we want tags
	* @return array   PHP Array of tags
	*/
	function getAppActivity($accountUserid,$start=null,$limit=null) {
        if ($start == null) {
            $start = 0;
        }
        if ($limit == null) {
            $limit = 30;
        }
        $sql = "SELECT 
                    A.ActivityID,A.Userid,A.UserPID,A.UserType,A.Activity,A.ItemID,A.ItemType,A.ItemTitle,A.DateActivity,
                    P.NameFirst,P.NameLast,P.AvatarURL
                    from cpo_activity A
                LEFT JOIN cpo_people P ON P.PID = A.UserPID
                WHERE 
                    A.Userid = ".$accountUserid." AND
                    A.ReportToUser = 1
                ORDER BY DateActivity DESC
                LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result_array();
	}
}
?>