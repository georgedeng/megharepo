<?
class Client_view extends Model {

    function Client_view() {
        // Call the Model constructor
        parent::Model();
    }

    /**
	 * checkForValidClient : Make sure that user has view rights for this client.
      *                        This prevents users from seeing other users clients by
      *                        simply typing a random clientID in the URL.
	 *
	 * @access public
	 * @param	 int     $clientID Client ID of client we're wanting to verify.
	 * @return boolean true if client can be viewed by this user
	 */
    function checkForValidClient($clientID,$accountUserid=NULL) {
    	$sql = "SELECT ClientID
				FROM cpo_clients 
				WHERE 
					ClientID = '$clientID' AND
					Userid = '".$accountUserid."'";
    	$query = $this->db->query($sql); 
    	if ($query->num_rows()<1) {
    		return false;
    	} else {
            return true;
        }
    }
    
    /**
	 * getClientsForAutocompleter : get clients for autocomplete searchbox
	 *
	 * @access	public
	 * @param   string $q search term
	 * @return	string pipe delimited string of clients
	 */	
    function getClientsForAutoCompleter($q) {
    	$sql = "SELECT ClientID,Company,Email
				FROM cpo_clients
				WHERE 
				(
					Company LIKE '$q%' OR Company LIKE '% $q%' OR
					Email LIKE '$q%' OR Email LIKE '% $q%'
				)
				AND Userid = '".$this->session->userdata('accountUserid')."'
                AND Deleted = 0
				ORDER BY Company";
		$query = $this->db->query($sql); 
        $clientArray = array();
        $a=0;
		foreach ($query->result_array() as $row) {
            $clientArray[$a]['id'] = $row['ClientID'];
            $clientArray[$a]['label'] = $row['Company'].' ['.$row['Email'].']';
            $a++;
		}
		return json_encode($clientArray);
        
    }  
    
    function getClientsForUser($userid,$start=null,$limit=null,$tag=null) {
    	$sql = "SELECT * FROM 
					cpo_clients 
				WHERE
                    Deleted = 0 AND
					Userid = '$userid'";
        if (!empty($tag)) {
			$sql .= " AND (Tags LIKE '$tag%' OR Tags LIKE '% $tag%') ";
		}
    	if (empty($order)) {
    		$order = 'Company';
    	}
    	$sql .= " ORDER BY $order";
    	if ($limit>0) {
    		if(empty($start)) {
    			$start = 0;
    		}
    		$sql .= " LIMIT $start,$limit";
    	}
    	$query = $this->db->query($sql); 
		return $query->result_array();			
    }
    
    function getClientInformation($clientID=NULL,$clientUserid=NULL) {
        $sql = "SELECT C.ClientID,C.Userid,C.ClientUserid,C.CatID,C.Company,
                C.Address,C.City,C.State,C.Zip,C.Country,C.Phone1,C.Phone2,
                C.Phone3,C.Email,C.URL,C.Comments,C.Industry,C.Client_Since,
                C.Timezone,C.Language,C.Tags,CAT.MainCat AS Category
                FROM cpo_clients C
                LEFT JOIN cpo_main_cat CAT ON CAT.CatID = C.CatID
                ";
        if ($clientUserid>0) {
            $sql .= " WHERE C.ClientUserid = '$clientUserid'";
        } else {
            $sql .= " WHERE C.ClientID = '$clientID'";
        }
    	$query = $this->db->query($sql); 
		return $query->row_array();	
    }
    
    function getClientInfoForLogin($userid,$accountUserid) {
    	$sql = "SELECT * from cpo_clients WHERE
					ClientUserid = '$userid' AND Userid = '$accountUserid'";
    	$query = $this->db->query($sql); 
    	if ($query->num_rows()<1) {
    		return false;
    	} else {
    		return $query->row_array();
    	}					
    }

    function getClientAuthKey($clientID) {
        $sql = "SELECT L.AuthString FROM cpo_login L
                LEFT JOIN cpo_clients C ON C.ClientUserid = L.UID
                WHERE
                C.ClientID = '$clientID'";
        $query = $this->db->query($sql);
    	if ($query->num_rows()<1) {
    		return false;
    	} else {
            $row = $query->row_array();
    		return $row['AuthString'];
    	}					
    }
    
    /**
	 * getClientsForSelect : get list of clients for select menu
	 *
	 * @access	public
	 * @return	array PHP array of client data
	 */
	function getClientsForSelect() {
		$sql = "SELECT ClientID,Company 
				FROM cpo_clients
				WHERE 
                Userid = '".$this->session->userdata('accountUserid')."' AND
                Deleted = 0
				ORDER BY Company";
		$query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            $clientArray = array();
            $a=0;
            foreach($query->result_array() as $row) {
                $clientArray[$a]['id'] = $row['ClientID'];
                $clientArray[$a]['ClientID'] = $row['ClientID'];
                $clientArray[$a]['Company']  = $row['Company'];
                $clientArray[$a]['value'] = DBToField($row['Company']);
                $a++;
            }
    		return $clientArray;
    	}
	}

    function getClientFromPrevClientID($prevClientID,$accountUserid) {
        $sql = "SELECT * from cpo_clients WHERE PrevClientID = '$prevClientID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
    	}
    }

    function getClientFromCompany($clientCompany,$accountUserid) {
        $sql = "SELECT * from cpo_clients
                WHERE
                (Company LIKE '$clientCompany%' OR
                Company LIKE '% $clientCompany%') AND
                Userid = '$accountUserid' AND
                Deleted = 0
                LIMIT 1";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
    	}
    }

    function getClientLoginInformationFromEmail($email,$accountUserid) {
        $sql = "SELECT * from cpo_login
                WHERE
                Userid        = '$email' AND
                User_Type     = '".USER_TYPE_CLIENT."' AND
                UseridAccount = '$accountUserid'
                LIMIT 1
                ";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
        }
    }

    function getClientsForImportConfirm() {
        $sql = "SELECT * from cpo_clients WHERE ImportGUID = '".$this->session->userdata('importGUID')."' LIMIT 8";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
        }
    }
}    
?>