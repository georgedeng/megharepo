<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="settingsOwner" class="helptag" data-help="settings">
	<div id="tabsSettings">
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-selected" id="tabAccountPassword"><a href="#windowAccountPassword"><span><?=lang('settings_change_password'); ?></span></a></li>
            <li id="tabAccountChangePlan"><a href="#windowAccountChangePlan"><span><?=lang('plan_update_plan_cc'); ?></span></a></li>
            <li id="tabAccountViewInvoices"><a href="#windowAccountViewInvoices"><span><?=lang('settings_account_view_payments'); ?></span></li>
            <li id="tabAccountCancel"><a href="#windowAccountCancel"><span><?=lang('settings_cancel_account'); ?></span></a></li>
        </ul>
	    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">
	    	
			
			
			<div id="windowSettingsAccount" class="ui-tabs-hide">
	    		<div style="padding: 6px;" id="">
                    <div class="settingsViewAll hide boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_briefcase" style="margin: 6px;"><?=lang('plan_update_plan_or_cc'); ?></h3>
                    </div>
                    <div class="hide" style="margin-bottom: 12px; padding-left: 24px;">
                        <h4 class="icon_briefcase" style="margin-bottom: 12px;">Your account plan: <?=$accountName; ?></h4>
                        <?=$accountDesc; ?>

                        <button class="buttonExpand blueGray" id="buttonUpdateAccount" style="margin: 12px 12px 0 0;"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator accountUpdate"><?=lang('plan_update_plan'); ?></span></span></button>
                        <div id="changeCCContainer">
                            <button class="buttonExpand green" id="buttonUpdateCCInfo" style="margin-top: 12px;"><span class="buttonOuterSpan green"><span class="buttonDecorator creditCardEdit"><?=lang('plan_update_cc'); ?></span></span></button>
                        </div>
                        <div style="clear: left;"></div>
                    </div>

                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_password" style="margin: 6px;"><?=lang('settings_change_password'); ?></h3>
                    </div>
                    <div class="hide">
                        <form class="noBorder" id="accountFormPassword">
                            <p class="padBottom12">
                            <label><?=lang('common_current_password'); ?></label>
                            <input type="password" style="width: 150px;" id="currentPassword" name="currentPassword" />
                            </p>
                            <p>
                            <label for="password"><?=lang('common_new_password'); ?></label>
                            <input type="password" style="width: 150px;" id="newPassword" name="newPassword" />
                            </p>
                            <div style="margin: 6px 0 24px 262px;">
                                <button class="buttonExpand blueGray" id="buttonSaveAccountPassword"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageSettingsAccountPassword"></div>
                                <div style="clear: left;"></div>
                            </div>
                        </form>
                    </div>

                    <div class="settingsViewAll hide boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_lock" style="margin: 6px;"><?=lang('settings_account_security'); ?></h3>
                    </div>
                    <div class="hide" style="margin-bottom: 12px;">
                        <form class="noBorder" id="accountFormSecurity">

                            <div class="warningDiv" style="width: 643px; margin: 12px 0 12px 0;"><?=lang('help_account_security_settings'); ?></div>
                            <table class="dataTable" style="background: none;">
                                <tr>
                                    <td style="width: 42px;"><input type="checkbox" class="settingAccount" value="accountLoginFromItemView" name="chkAccountLoginItemView" id="chkAccountLoginItemView" <? if (isset($accountArray['accountLoginFromItemView']) && $accountArray['accountLoginFromItemView'] == 1) { echo 'checked'; } ?> /></td>
                                    <td><label for="chkAccountLoginItemView" class="normal"><?=lang('settings_account_login_on_item_view'); ?></label></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="settingAccount" value="accountLoginFromEmail" name="chkAccountLoginEmail" id="chkAccountLoginEmail" <? if (isset($accountArray['accountLoginFromEmail']) && $accountArray['accountLoginFromEmail'] == 1) { echo 'checked'; } ?> /></td>
                                    <td><label for="chkAccountLoginEmail" class="normal"><?=lang('settings_account_login_from_email'); ?></label></td>
                                </tr>
                            </table>

                            <div style="margin: 6px 0 24px 47px;">
                                <button class="buttonExpand blueGray" id="buttonSaveAccountSecurity"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageSettingsAccountSecurity"></div>
                                <div style="clear: left;"></div>
                            </div>
                        </form>
                    </div>

                    <? if ($this->session->userdata('userType') == USER_TYPE_OWNER || $this->session->userdata('userType') == USER_TYPE_GOD) { ?>
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_delete" style="margin: 6px;"><?=lang('settings_cancel_account'); ?></h3>
                    </div>
                    <div class="hide" style="margin-bottom: 12px;">
                        <form class="noBorder" id="accountFormCancel">
                            <div class="warningDiv" style="width: 643px; margin: 12px 0 12px 0;"><?=lang('help_account_cancel'); ?></div>

                            <div style="margin-left: 50px;"><?=lang('help_account_cancel_message'); ?></div>

                            <div style="margin: 6px 0 24px 47px;">
                                <button class="buttonExpand yellow" id="buttonAccountCancel"><span class="buttonOuterSpan yellow"><span class="buttonDecorator delete"><?=lang('settings_cancel_account'); ?></span></span></button>
                                <div style="clear: left;"></div>
                            </div>
                        </form>
                    </div>
                    <? } ?>
                </div>
			</div>
		</div>
	</div>
    <div id="dialogCancelAccount" class="noPrint" style="background: #fff;" title="<?=lang('settings_cancel_account'); ?>">
        <div style="padding: 6px;" id="deleteContentsContainer">
            <div class="warningDiv" style="width: 418px; margin: 12px 0 12px 0;"><?=lang('help_account_cancel'); ?></div>
        </div>
        <button id="buttonCancelAccount" class="smallButton" style="margin: 0 6px 0 6px; float: left;"><span class="delete"><?=lang('settings_cancel_account'); ?></span></button>
        <button id="buttonCancel" class="smallButton" style="margin-right: 6px; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button
        <div style="clear: left;"></div>
    </div>   
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
