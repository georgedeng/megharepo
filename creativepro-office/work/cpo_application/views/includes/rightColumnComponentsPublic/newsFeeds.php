<div class="whiteBoxContent_300">
    <? if (!isset($showBlogLatest) || $showBlogLatest != FALSE) { ?>
    <a href="http://blog.<?=SITE_DOMAIN; ?>" class="h2link brown iconBlogBlock" style="margin: 0 0 12px 6px;">Recent blog entries</a>
    <ul class="arrow1 mBottom" style="margin-left: 25px;">
    <?
    foreach($blogRecentEntries as $navEntry) {
        echo '<li><a href="'.BASE_URL.'blog/entry/'.$navEntry['EntryURL'].'">'.$navEntry['Title'].'</a></li>';
    }
    ?>
    </ul>
    <? } ?>

    <? if (!isset($showTwitter) || $showTwitter != FALSE) { ?>
    <a href="http://twitter.com/cpohq" target="_blank" class="h2link brown iconTwitterBlock" style="margin: 0 0 12px 6px;">Follow us on Twitter!</a>
    <?
    $a = 0;
    if (is_array($tweets) && count($tweets)>0) {
        foreach($tweets as $tweet) {
            if ($a<5) {
                echo '<p style="margin: 0 0 12px 6px;"><img src="'.$tweet->user->profile_image_url.'" style="float: left; padding: 0 6px 0 0; width: 30px; height: 30px;" />';
                $tweet = twitterReplaceHashTags(twitterReplaceMention($tweet->text)).'</p>';
                echo auto_link($tweet,'url');
                $a++;
            }
        }
    }
    ?>
    <? } ?>
</div>
