<ul class="rightColMenu">
    <li>
        <a href="<?=site_url('blog/Blog/BlogAdmin'); ?>">CPO Blog</a>
        <ul>
            <li><a href="<?=BASE_URL; ?>blog/Blog/blogUpdate">New blog post</a></li>
        </ul>
    </li>
    <li>
        <a href="<?=site_url('helpdesk/Helpdesk/HelpdeskAdmin'); ?>">Help Desk</a>
        <ul>
            <li><a href="<?=BASE_URL; ?>helpdesk/Helpdesk/HelpdeskUpdate">New helpdesk entry</a></li>
        </ul>
    </li>
    <li><a href="<?=site_url('administration/AdminUsers'); ?>">User Manager</a></li>
    <li><a href="<?=site_url('administration/AdminUserMetrics'); ?>">User Metrics</a></li>
    <li><a href="<?=site_url('bugs/Bugs'); ?>">Bugs</a></li>
    <li><a href="<?=site_url('administration/AdminTranslation'); ?>">Translation</a></li>
</ul>
