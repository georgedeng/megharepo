<div style="text-align: center; margin-bottom: 6px;" id="totalBadgeWeekly" class="bigText colorBackground light-green">
    <p style="text-align: center; margin-bottom: 6px;"><?=lang('timesheets_total_weekly'); ?></p>
    <span id="totalHoursContainer"></span>
</div>
<div style="text-align: center; margin-bottom: 6px;" id="totalBadgeReports" class="hide bigText colorBackground light-green">
    <p style="text-align: center; margin-bottom: 6px;"><?=lang('timesheets_total_hours'); ?></p>
    <span id="totalHoursContainerReports" class="hugeTextEntry">0.00</span> hrs
</div>
<div id="sparkGraphContainer" style="text-align: center;"></div>
<div style="padding: 0; text-align: center;">
    <span class="chartAxis1 subText"><?=lang('cal_mo'); ?><br /><span id="dayHoursChart0"></span></span>
    <span class="chartAxis1 subText"><?=lang('cal_tu'); ?><br /><span id="dayHoursChart1"></span></span>
    <span class="chartAxis1 subText"><?=lang('cal_we'); ?><br /><span id="dayHoursChart2"></span></span>
    <span class="chartAxis1 subText"><?=lang('cal_th'); ?><br /><span id="dayHoursChart3"></span></span>
    <span class="chartAxis1 subText"><?=lang('cal_fr'); ?><br /><span id="dayHoursChart4"></span></span>
    <span class="chartAxis1 subText"><?=lang('cal_sa'); ?><br /><span id="dayHoursChart5"></span></span>
    <span class="chartAxis1 subText"><?=lang('cal_su'); ?><br /><span id="dayHoursChart6"></span></span>
</div>