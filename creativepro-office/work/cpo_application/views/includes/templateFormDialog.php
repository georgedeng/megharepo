<div style="display: none;" id="projectTemplateDialog" title="<?=lang('project_template_create'); ?>: <?=$pageSubTitle; ?>">
    <div style="padding: 12px;">
        <input type="hidden" id="templateAction" value="add" name="templateAction" />
        <input type="hidden" id="templateID" value="add" name="templateID" />
        <input type="hidden" id="templateProjectID" name="templateProjectID" />

        <p><strong><?=lang('project_template_name'); ?></strong><br />
        <input type="text" style="width: 400px;" name="templateName" id="templateName" /></p>

        <p><strong><?=lang('project_template_desc'); ?></strong><br />
        <textarea name="templateDesc" id="templateDesc" style="width: 400px; height: 40px;"></textarea></p>

        <p><strong><?=lang('project_template_items'); ?></strong>
        <table style="margin-bottom: 12px;">
            <tr>
                <td>
                    <input type="checkbox" value="1" id="chkMilestones" /> <?=lang('task_milestone'); ?><br />
                    <input type="checkbox" value="1" id="chkTasks" /> <?=lang('task_tasks'); ?><br />
                    <input type="checkbox" value="1" id="chkContacts" /> <?=lang('common_contacts'); ?><br />
                    <input type="checkbox" value="1" id="chkTeamMembers" /> <?=lang('team_members'); ?><br /></td>
                <td>
                    <input type="checkbox" value="1" id="chkFiles" /> <?=lang('files_files'); ?><br />
                    <input type="checkbox" value="1" id="chkExpenses" /> <?=lang('finance_expenses'); ?><br />
                    <input type="checkbox" value="1" id="chkNotes" /> <?=lang('widget_notes'); ?><br />
                </td>
            </tr>
        </table>
        <button id="buttonTemplateModalSave" class="smallButton" style="margin: 0 6px 0 6px; float: left;"><span class="save"><?=lang('button_save'); ?></span></button>
        <button id="buttonTemplateModalCancel" class="smallButton" style="margin-right: 6px; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
        <div style="float: left;" id="templateResponseContainer"></div>
        <div style="clear: left;"></div>
    </div>
</div>