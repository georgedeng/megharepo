<? $this->load->view('includes/header'); ?>

<div id="pageLeftColumn" pageID="" class="helptag" data-help="import">
    <input type="hidden" id="importType" value="<?=$importType; ?>" />
    <div style="margin: 6px 0 0 12px;">
        <h4 class="status_completed" style="margin-bottom: 12px;"><?=lang('import_confirm_data'); ?></h4>

        <div class="infoMessageSmall lightText" style="margin: 12px 0 12px 0;"><? printf(lang('help_import_step3'),$rowCount); ?></div>

        <table class="dataTable" id="confirmTable" style="width: 960px;">
        <thead>
        <tr>
        <?
        $cols = count($confirmHeader);
        $cellWidth = floor(920/$cols);
        
        foreach($confirmHeader as $header) {
            echo '<th style="width: '.$cellWidth.'px; overflow: hidden;">'.$header.'</th>';
        }
        echo '</tr></thead>';

        foreach($confirmContent as $content) {
            echo '<tr class="rowLines">';
            foreach($content as $element) {
                echo '<td><div style="width: '.$cellWidth.'px; overflow: hidden;">'.$element.'</div></td>';
            }
            echo '</tr>';
        }
        ?>
        </table>
        <div style="margin-top: 12px;">
            <button class="buttonExpand action" id="buttonConfirmImport"><span class="save"><?=lang('import_accept_import'); ?></span></button>
            <button class="buttonExpand alert" id="buttonDeleteImport"><span class="cancel"><?=lang('import_cancel_import'); ?></span></button>
            <div style="clear: left;"></div>
            <p style="margin: 3px 0 0 120px;" class="subText">(<?=lang('help_import_cancel_import'); ?>)</p>
            
        </div>
    </div>
</div>
<div style="clear: left;"></div>
<? $this->load->view('includes/footer'); ?>