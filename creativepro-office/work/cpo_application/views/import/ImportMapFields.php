<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="" class="helptag" data-help="import">
    <div style="margin: 6px 0 0 12px;">
        <h4 class="icon_map_fields" style="margin-bottom: 12px;"><?=lang('import_map_fields'); ?></h4>

        <div class="infoMessageSmall lightText" style="margin: 12px 0 12px 0;"><?=lang('help_import_step2'); ?></div>
    
        <form action="<?=$postURL; ?>" id="formMapFields" method="POST">
            <input type="hidden" name="importType" id="importType" value="<?=$importType; ?>" />

            <table class="dataTable" id="mapTable" style="width: 500px;">
            <thead>
                <tr>
                    <th style="width: 250px;"><?=$importColText; ?></th>
                    <th style="width: 250px;"><?=lang('import_your_data_column'); ?></th>
                </tr>
            </thead>
                
            <? foreach($mappedFieldsArray as $key => $value) { ?>
                <tr>
                    <td><strong><?=$value; ?></strong></td>
                    <td>
                        <select name="<?=$key; ?>" size="1" style="width: 250px;">
                            <option value="empty"></option>
                            <?
                            $a = 1;
                            foreach($userFieldsArray as $userKey => $userValue) {
                                if (empty($userValue)) {
                                    $userValue = 'Col: '.$a;
                                }
                                echo '<option value="'.$userKey.'">'.$userValue.'</option>';
                                $a++;
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            <? } ?>
            </table>
            <div class="boxYellow" style="padding: 6px; margin: 12px 0 12px 0;">
            <input type="checkbox" name="firstRowHeaders" value="1" /> <?=lang('import_first_row_headers'); ?><br />
            <? if ($importType == 'contacts') { ?>
            <?=lang('import_contacts_are'); ?>&nbsp;
            <input type="checkbox" name="contactsContacts" checked="checked" value="1" /> <?=lang('common_contacts'); ?>
            <input type="checkbox" name="contactsVendors" value="1" /> <?=lang('common_vendors'); ?>
            <? } ?>
            </div>

            <div>
                <button class="buttonExpand action" id="buttonImportMapping"><span class="save"><?=lang('import_import_data'); ?></span></button>
                <button class="buttonExpand alert" id="buttonCancelImportMapping"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
                <div style="clear: left;"></div>
            </div>
        </form>
    </div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
