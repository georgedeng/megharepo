<? $this->load->view('includes/header'); ?>

<div id="pageLeftColumn" pageID="importFileUpload" class="helptag" data-help="import">
    <? if (!empty($message)) { ?>
    <div class="boxYellow" style="padding: 6px;"><span style="display: block;" class="infoMessageBig"><?=$message; ?></span></div>
    <? } else { ?>
    <div class="infoMessageSmall lightText" style="margin: 12px 0 12px 0;"><?=lang('help_import_step1'); ?></div>
    <? } ?>

    <div style="width: 680px;">
        <div class="widgetContent windowFrame" style="padding: 12px; margin: 6px;">
            <h4 class="icon_client_small"><?=lang('import_import_clients'); ?></h4>
            <button class="buttonExpand action" id="buttonClientUpload"><span class="upload"><?=lang('import_upload_data_file'); ?></span></button>
            <div id="uploadErrorMessageClients"></div>
        </div>
        <div style="clear: both;"></div>

        <div class="widgetContent windowFrame" style="padding: 12px; margin: 6px;">
            <h4 class="icon_vcard"><?=lang('import_import_contacts'); ?></h4>
            <button class="buttonExpand action" id="buttonContactUpload"><span class="upload"><?=lang('import_upload_data_file'); ?></span></button>
            <div id="uploadErrorMessageContacts"></div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
