<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="settingsOwner">
	<div id="tabsSettings">
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-selected" id="tabSettingsAccount"><a href="#windowSettingsAccount"><span><?=lang('common_account'); ?></span></a></li>
	    </ul>
	    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">		
			<div id="windowSettingsAccount">
	    		<div style="padding: 6px;" id="">
                    <h3 class="icon_password padBottom12" style="margin-top: 6px; margin-left: 6px;"><?=lang('settings_change_password'); ?></h3>

                    <p class="padBottom12">
                    <label><?=lang('common_current_password'); ?></label>
                    <input type="password" style="width: 150px;" id="currentPassword" name="currentPassword" />
                    </p>
                    <p>
                    <label for="password"><?=lang('common_new_password'); ?></label>
                    <input type="textbox" style="width: 150px;" id="newPassword" name="newPassword" />
                    </p>
                    
                    <div style="margin: 6px 0 0 255px;">
						<button class="buttonExpand blueGray" id="buttonSaveAccountSettingsClient"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
                        <div style="clear: both;"></div>
                        <div style="margin-top: 12px;" id="messageSettingsAccountPassword"></div>
                    </div>
                    </form>
                </div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>