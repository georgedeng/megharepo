<? $this->load->view('includes/header'); ?>
<form class="greyForm" id="formInvoice" name="formInvoice" action="<?=BASE_URL; ?>finances/InvoiceUpdate/saveInvoice" method="POST">
  
<div id="pageLeftColumn" pageID="updateInvoice">
	<div id="pageMessage"></div>
	<input type="hidden" name="invoiceID" id="invoiceID" value="<?=$invoiceInformation[0]['InvoiceID']; ?><?=set_value('invoiceID'); ?>" />
	<input type="hidden" name="clientIDHolder" id="clientIDHolder" value="<?=$invoiceInformation[0]['ClientID']; ?><?=set_value('clientIDHolder'); ?>" />
	<input type="hidden" name="projectIDHolder" id="projectIDHolder" value="<?=$invoiceInformation[0]['ProjectID']; ?>" />
    <input type="hidden" name="clientLanguage" id="clientLanguage" />
    <input type="hidden" name="action" id="action" value="<?=$action; ?><?=set_value('action'); ?>" />
	<input type="hidden" name="save" id="save" value="1" />
	<input type="hidden" name="invoiceEmail" id="invoiceEmail" value="<?=$clientEmail; ?>" />
    <input type="hidden" name="financeType" id="financeType" value="<?=$financeType; ?>" />
	<input type="hidden" name="invoicePrint" id="invoicePrint" />
    <input type="hidden" name="tagHolder" id="tagHolder" value="<? echo str_replace(' ',',',$invoiceInformation[0]['Tags']); ?><?=set_value('tagHolder'); ?>" />
        
		<div style="float: left;">
		<p class="noBorder">
			<label class="required" for="clientID"><?=lang('project_form_select_client'); ?></label>
			<input type="text" class="vcardSearch" value="<?=$invoiceInformation[0]['ClientCompany']; ?>" id="clientID" name="clientID" tabindex="1" style="width: 383px;" />
            <div class="formHelpText" style="margin-top: -6px;"><span class="subText"><?=lang('client_search'); ?></span></div>
			<?=form_error('clientID','<span class="formError">','</span>'); ?>
		</p>
        </div>
        <div style="float: left;">
			<?=help('help_formfield_client_search','clientID','iconHelpFormOutside'); ?>
		</div>
		<div style="clear: left;"></div>
		<p style="display: none;" id="projectIDContainer">
			<label for="invoiceProjectID"><?=lang('project_select_project'); ?></label>
			<select id="projectID" name="projectID" tabindex="2" style="width: 400px;">
			</select>
		</p>
        <p>
			<label class="required" for="invoiceTitle">
            <?
                if ($estimate == true) {
                    echo lang('finance_estimate_title');
                } else {
                    echo lang('finance_invoice_title');
                }
            ?>
            </label>
			<input type="text" class="bigText" value="<?=$invoiceInformation[0]['Title']; ?><?=set_value('invoiceTitle'); ?>" id="invoiceTitle" name="invoiceTitle" tabindex="3" style="width: 400px;" />
			<?=form_error('invoiceTitle','<span class="formError">','</span>'); ?>
		</p>
		<p>
			<label class="required" for="invoiceNumber">
            <?
                if ($estimate == true) {
                    echo lang('finance_estimate_number');
                } else {
                    echo lang('finance_invoice_number');
                }
            ?>                    
            </label>
			<span style="float: right;" class="subText"><?=$lastInvoiceNumberString; ?></span>
			<input type="text" name="invoiceNumber" id="invoiceNumber" style="width: 100px;" tabindex="4" value="<?=$nextInvoiceNumber; ?><?=$invoiceInformation[0]['InvNo']; ?><?=set_value('invoiceNumber'); ?>">
			<?=form_error('invoiceNumber','<span class="formError">','</span>'); ?>
		</p>
		
		<p>
			<label for="invoiceDate">
            <?
                if ($estimate == true) {
                    echo lang('finance_estimate_date');
                } else {
                    echo lang('finance_invoice_date');
                }
            ?>      
            </label>
			<input type="text" id="invoiceDate" name="invoiceDate" class="dateField" tabindex="5" value="<?=$invoiceDate; ?><?=set_value('invoiceDate'); ?>" style="width: 100px; margin-right: 6px;" />
        </p>
		<p>
			<label for="invoicePONumber"><?=lang('finance_invoice_ponumber'); ?></label>
			<input type="text" name="invoicePONumber" id="invoicePONumber" tabindex="6" value="<?=$invoiceInformation[0]['PONumber']; ?><?=set_value('invoicePONumber'); ?>" style="width: 100px;">
		</p>
		<p class="noBorder">
			<label for="invoiceCategory">
            <?
                if ($estimate == true) {
                    echo lang('finance_estimate_category');
                } else {
                    echo lang('finance_invoice_category');
                }
            ?>      
            </label>
            <input type="text" name="invoiceCategorySelect" id="invoiceCategorySelect" tabindex="7" value="<?=$invoiceInformation[0]['Category']; ?>" style="width: 400px;" />
			<input type="hidden" name="invoiceCategory" id="invoiceCategory" value="<?=$invoiceInformation[0]['CatID']; ?>" />
        </p>
		<p class="noBorder">
			<label for="invoiceMessage"><?=lang('finance_invoice_message_form'); ?></label>
			<textarea name="invoiceMessage" id="invoiceMessage" class="expanding settingsField" settingType="invoiceMessage_account_invoice" position="top" tabindex="9" style="width: 400px;"><?=$invMessage; ?><?=set_value('invoiceMessage'); ?></textarea>
			<?
            $printMessage = '';
			if ($invoiceInformation[0]['IncComments'] == 1) {
				$printMessage = ' checked="checked" ';
			}
			?>
            <div class="formRightContent"><input type="checkbox" name="printMessage" id="printMessage" tabindex="10" <?=$printMessage; ?> value="1" /> 
                <label for="printMessage" class="normal clickable">
                <?
                    if ($estimate == true) {
                        echo lang('finance_estimate_message_print');
                    } else {
                        echo lang('finance_invoice_message_print');
                    }
                ?>        
                </label></div>
		</p>
        <? if ($estimate != true) { ?>
        <p class="noBorder">
			<label for="invoiceRecurring"><?=lang('finance_invoice_recurring'); ?></label>
            <select id="invoiceRecurringWhen" name="invoiceRecurringWhen" size="1" tabindex="11" style="width: 300px;">
                <option value="0"><?=lang('finance_invoice_repeats'); ?></option>
                <option value="week" <? if ($recurringRange == 'week') { echo 'selected="selected"'; } ?>><?=lang('calendar_repeats_week'); ?></option>
                <option value="month" <? if ($recurringRange == 'month') { echo 'selected="selected"'; } ?>><?=lang('calendar_repeats_month'); ?></option>
                <option value="year" <? if ($recurringRange == 'year') { echo 'selected="selected"'; } ?>><?=lang('calendar_repeats_year'); ?></option>
            </select>
            <div id="containerWeek" style="margin-left: 265px;" class="hide">
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Sun') { $sunChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekSU" name="recurringWeekSU" value="Sun" checked="<?=$sunChecked; ?>" /> <?=lang('cal_su') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Mon') { $monChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekMO" name="recurringWeekMO" value="Mon" <?=$monChecked; ?> /> <?=lang('cal_mo') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Tue') { $tueChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekTU" name="recurringWeekTU" value="Tue" <?=$tueChecked; ?> /> <?=lang('cal_tu') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Wed') { $wedChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekWE" name="recurringWeekWE" value="Wed" <?=$wedChecked; ?> /> <?=lang('cal_we') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Thu') { $thuChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekTH" name="recurringWeekTH" value="Thu" <?=$thuChecked; ?> /> <?=lang('cal_th') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Fri') { $friChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekFR" name="recurringWeekFR" value="Fri" <?=$friChecked; ?> /> <?=lang('cal_fr') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Sat') { $satChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekSA" name="recurringWeekSA" value="Sat" <?=$satChecked; ?> /> <?=lang('cal_sa') ;?>
            </div>
            <div id="containerMonth" style="margin-left: 265px;" class="hide">
                <? for($a=0;$a<=5;$a++) { ?>
                <select id="recurringMonthDay<?=$a; ?>" name="recurringMonthDay<?=$a; ?>" style="margin-right: 3px; width: 65px;">
                    <option value="0"><?=lang('date_day'); ?></option>
                    <?
                    for($b=1;$b<=31;$b++) {
                        $selected = '';
                        if (isset($recurringDays[$a]) && $recurringDays[$a] == $b) {
                            $selected = 'selected="selected"';                        
                        }
                        echo '<option value="'.$b.'" '.$selected.'>'.$b.'</option>';
                    }
                    ?>                    
                </select>    
                <? } ?>
            </div>
            <div id="containerYear" style="margin-left: 265px;" class="hide">
                <? for($a=0;$a<=5;$a++) { ?>
                <div style="margin-bottom: 3px;">
                    <select id="recurringYearMonth<?=$a; ?>" name="recurringYearMonth<?=$a; ?>" style="margin-right: 3px; width: 100px;">
                        <option value="0"><?=lang('date_month'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '01') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="01" <?=$selected; ?>><?=lang('cal_january'); ?></option>
                        <? 
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '02') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="02" <?=$selected; ?>><?=lang('cal_february'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '03') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="03" <?=$selected; ?>><?=lang('cal_march'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '04') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="04" <?=$selected; ?>><?=lang('cal_april'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '05') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="05" <?=$selected; ?>><?=lang('cal_may'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '06') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="06" <?=$selected; ?>><?=lang('cal_june'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '07') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="07" <?=$selected; ?>><?=lang('cal_july'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '08') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="08" <?=$selected; ?>><?=lang('cal_august'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '09') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="09" <?=$selected; ?>><?=lang('cal_september'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '10') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="10" <?=$selected; ?>><?=lang('cal_october'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '11') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="11" <?=$selected; ?>><?=lang('cal_november'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '12') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="12" <?=$selected; ?>><?=lang('cal_december'); ?></option>
                    </select>
                    <select id="recurringYearDay<?=$a; ?>" name="recurringYearDay<?=$a; ?>" style="width: 70px;">
                    <option value="0"><?=lang('date_day'); ?></option>
                    <?
                    for($b=1;$b<=31;$b++) {
                        $selected = '';
                        if (isset($recurringDays[$a]) && $recurringDays[$a] == $b) {
                            $selected = 'selected="selected"';
                        }
                        echo '<option value="'.$b.'" '.$selected.'>'.$b.'</option>';
                    }
                    ?>                    
                    </select>
                </div>    
                <? } ?>
            </div>
		</p>
		<? } // End if ($estimate != true) ?>   
        
		<p>
        <div style="float: left;">
            <label><?=lang('client_form_tags'); ?></label>
            <div style="float: right;">
            <input type="text" name="invoiceTags" id="invoiceTags" tabindex="22" value="<?=$expenseInformation['Tags']; ?><?=set_value('expenseTags'); ?>" />
            </div>
        </div>
        <div style="float: left;">
            <?=help('help_formfield_tags','clientTags','iconHelpFormOutside'); ?>
        </div>
        </p>
        <div style="clear: both;"></div>
		<!-- This is where the invoice items are rendered -->

        <? if ($estimate != true) { ?>    
        <div style="margin: 12px 0 0 0;">
            <button class="smallButton" id="buttonSelectExpenses" style="float: left; margin-right: 6px;" title="<?=lang('finance_select_expenses'); ?>"><span class="expense"><?=lang('finance_select_expenses'); ?></span></button>
            <button class="smallButton" id="buttonSelectTimesheetEntries" style="float: left;" title="<?=lang('finance_select_timesheet_entries'); ?>"><span class="timesheet"><?=lang('finance_select_timesheet_entries'); ?></span></button>
            <div style="clear: left;"></div>
        </div>
        <? } ?>
		<input type="hidden" name="invoiceItemCount" id="invoiceItemCount" value="<?=$invoiceItemCount; ?>" />
		<table style="margin-top: 12px;" id="invoiceItemTable" class="dataTable">
		<thead>
			<tr>
				<th style="width: 280px;"><?=lang('finance_invoice_item_description'); ?></th>
				<th style="width: 110px;"><?=lang('finance_invoice_item_type'); ?></th>
				<th style="width: 30px; text-align: right;"><?=lang('finance_invoice_item_qty'); ?></th>
				<th style="width: 60px; text-align: right;"><?=lang('finance_invoice_item_cost'); ?></th>
				<th style="width: 40px; text-align: center;"><?=lang('finance_invoice_item_tax'); ?></th>
				<th style="width: 60px; text-align: right;"><?=lang('common_total'); ?></th>
				<th style="width: 50px;"></th>
			</tr>
		</thead>
		<tbody id="invoiceItemTableBody">
			<?
			$itemCount = 0;
			$tabindex = 40;
			if (isset($invoiceInformation[0]['InvoiceItems'])) {
				foreach($invoiceInformation[0]['InvoiceItems'] as $invoiceItem) {
				?>
				<tr class="invoiceItemRowTemplate invoiceItemRow" id="itemRow_<?=$itemCount; ?>">
					<td>
                        <input type="hidden" class="invoiceItemExtra" name="extra_<?=$itemCount; ?>" id="extra_<?=$itemCount; ?>" />
                        <textarea name="desc_<?=$itemCount; ?>" id="desc_<?=$itemCount; ?>" style="width: 280px;" tabindex="<?=$tabindex++; ?>" class="invoiceItemDesc expanding"><?=DBToField($invoiceItem['ItemDesc']);?></textarea>
                    </td>
					<td style="vertical-align: top;">
						<select class="invoiceItemType" name="type_<?=$itemCount; ?>" id="type_<?=$itemCount; ?>" tabindex="<?=$tabindex++; ?>" style="width: 110px;">
						<option value="hours"
						<? if ($invoiceItem['Kind'] == 'hours') { echo  ' selected '; } ?>
						><?=lang('finance_invoice_type_hours'); ?></option>
						<option value="days"
						<? if ($invoiceItem['Kind'] == 'days') { echo  ' selected '; } ?>
						><?=lang('finance_invoice_type_days'); ?></option>
						<option value="product"
						<? if ($invoiceItem['Kind'] == 'product') { echo  ' selected '; } ?>
						><?=lang('finance_invoice_type_product'); ?></option>
						<option value="service"
						<? if ($invoiceItem['Kind'] == 'service') { echo  ' selected '; } ?>
						><?=lang('finance_invoice_type_service'); ?></option>
						</select>
					</td>
					<td style="vertical-align: top; text-align: right;"><input tabindex="<?=$tabindex++; ?>" class="invoiceItemQty numberField invoiceItemCalculate" type="text" style="width: 30px;" name="qty_<?=$itemCount; ?>" id="qty_<?=$itemCount; ?>" value="<?=$invoiceItem['Qty'] ;?>" /></td>
					<td style="vertical-align: top; text-align: right;"><input tabindex="<?=$tabindex++; ?>" class="invoiceItemRate numberField invoiceItemCalculate" type="text" style="width: 60px;" name="rate_<?=$itemCount; ?>" id="rate_<?=$itemCount; ?>" value="<?=$invoiceItem['CostPer'] ;?>" /></td>
					<td style="vertical-align: top; text-align: center;"><input tabindex="<?=$tabindex++; ?>" type="checkbox" class="invoiceItemTax" name="tax_<?=$itemCount; ?>" id="tax_<?=$itemCount; ?>" value="1"
					<?
					if ($invoiceItem['Taxable'] == 1) {
						echo ' checked ';
					} else {
						echo ' disabled ';
					}
					?>
					/></td>
					<td style="vertical-align: top; text-align: right;"><input tabindex="<?=$tabindex++; ?>" class="invoiceItemTotal numberField invoiceItemCalculate" type="text" style="width: 60px;" name="total_<?=$itemCount; ?>" id="total_<?=$itemCount; ?>" value="<?=$invoiceItem['ItemTotal'] ;?>" /></td>
					<td class="itemControl" style="vertical-align: top; text-align: right;"><button tabindex="<?=$tabindex++; ?>" id="control_<?=$itemCount; ?>" class="smallButton rowDelete invoiceItemControl"><span class="delete single"></span></button></td>
				</tr>
				<?
					$itemCount++;
				}
			}
			?>
			<tr class="invoiceItemRowTemplate invoiceItemRow" id="itemRow_<?=$itemCount; ?>">
				<td>
                    <input type="hidden" class="invoiceItemExtra" name="extra_<?=$itemCount; ?>" id="extra_<?=$itemCount; ?>" />
                    <textarea name="desc_<?=$itemCount; ?>" id="desc_<?=$itemCount; ?>" style="width: 280px;" tabindex="<?=$tabindex++; ?>" class="invoiceItemDesc expanding"></textarea>
                </td>
				<td style="vertical-align: top;">
					<select class="invoiceItemType" name="type_<?=$itemCount; ?>" id="type_<?=$itemCount; ?>" tabindex="<?=$tabindex++; ?>" style="width: 110px;">
					<option value="hours"><?=lang('finance_invoice_type_hours'); ?></option>
					<option value="days"><?=lang('finance_invoice_type_days'); ?></option>
					<option value="product"><?=lang('finance_invoice_type_product'); ?></option>
					<option value="service"><?=lang('finance_invoice_type_service'); ?></option>
					</select>
				</td>
				<td style="vertical-align: top; text-align: right;"><input tabindex="<?=$tabindex++; ?>" class="invoiceItemQty numberField invoiceItemCalculate" type="text" style="width: 30px;" name="qty_<?=$itemCount; ?>" id="qty_<?=$itemCount; ?>" value="" /></td>
				<td style="vertical-align: top; text-align: right;"><input tabindex="<?=$tabindex++; ?>" class="invoiceItemRate numberField invoiceItemCalculate" type="text" style="width: 60px;" name="rate_<?=$itemCount; ?>" id="rate_<?=$itemCount; ?>" value="" /></td>
				<td style="vertical-align: top; text-align: center;"><input tabindex="<?=$tabindex++; ?>" type="checkbox" class="invoiceItemTax" name="tax_<?=$itemCount; ?>" id="tax_<?=$itemCount; ?>" disabled value="1" /></td>
				<td style="vertical-align: top; text-align: right;"><input tabindex="<?=$tabindex++; ?>" class="invoiceItemTotal numberField invoiceItemCalculate" type="text" style="width: 60px;" name="total_<?=$itemCount; ?>" id="total_<?=$itemCount; ?>" value="" /></td>
				<td class="itemControl" style="vertical-align: top; text-align: right;"><button tabindex="<?=$tabindex++; ?>" id="control_<?=$itemCount; ?>" class="smallButton rowAdd invoiceItemControl"><span class="add single"></span></button></td>
			</tr>
		</tbody>
		<tfoot>
			<tr class="plain" id="rowSubTotal">
				<td style="text-align: right;" colspan="5"><strong><?=lang('common_subtotal'); ?>:</strong></td>
				<td style="text-align: right;"><strong><span id="amountSubTotal"><?=$invoiceSubTotal; ?></span></strong></td>
				<td></td>
			</tr>
			<tr class="plain" style="display: none;" id="rowDiscount">
				<td style="text-align: right;" colspan="5"><strong><?=lang('finance_invoice_discount'); ?>:</strong></td>
				<td style="text-align: right;" class="red">-<strong><span id="amountDiscount" class="redText"><?=$discountAmount; ?></span></strong></td>
				<td></td>
			</tr>
			<tr class="plain" style="display: none;" id="rowShipping">
				<td style="text-align: right;" colspan="5"><strong><?=lang('finance_invoice_freight'); ?>:</strong></td>
				<td style="text-align: right;"><strong><span id="amountShipping"><?=$shippingAmount; ?></span></strong></td>
				<td></td>
			</tr>
			<tr class="plain" style="display: none;" id="rowTax">
				<td style="text-align: right;" colspan="5"><strong><?=lang('finance_invoice_item_tax'); ?>:</strong></td>
				<td style="text-align: right;"><strong><span id="amountTax"><?=$taxAmount; ?></span></strong></td>
				<td></td>
			</tr>
			<tr>
				<td style="text-align: right;" colspan="5" class="bigText"><?=lang('common_total'); ?>:</td>
				<td style="text-align: right;" class="bigText" id="totalCell"><?=$currencyMark; ?><?=$invoiceTotal; ?></td>
				<td></td>
			</tr>
		</tfoot>
		</table>

		<input type="hidden" name="amountSubTotal" id="amountSubTotalHidden" value="0.00" />
		<input type="hidden" name="amountDiscount" id="amountDiscountHidden" value="0.00" />
		<input type="hidden" name="amountShipping" id="amountShippingHidden" value="0.00" />
		<input type="hidden" name="amountTax" id="amountTaxHidden" value="0.00" />
		<input type="hidden" name="invoiceTotal" id="invoiceTotalHidden" value="0.00" />

		<div class="buttonContainer">
            <button class="buttonExpand blueGray action primary" id="buttonSave" tabindex="309"><span class="save"><?=lang('button_save'); ?></span></button>
			<button class="buttonExpand blueGray action" id="buttonEmail" tabindex="310"><span class="email"><?=$emailButtonText; ?></span></button>
			<button class="buttonExpand green" id="buttonPrint" tabindex="311"><span class="print"><?=lang('button_print'); ?></span></button>
			<button class="buttonExpand yellow secondary cancel" id="buttonCancel" tabindex="312"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
            <div style="clear: left;"></div>
		</div>        
        <div style="float: right; width: 370px; padding: 6px; margin-top: 6px; display: none;" id="emailFormContainer" class="boxBlue">
            <? $this->load->view('includes/emailInvoiceForm.php'); ?>
        </div>
        <div style="height: 200px;"></div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
</form>

<? $this->load->view('includes/footer'); ?>
