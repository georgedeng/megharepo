<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="expenseView">
    <script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var vendorData = '<?=$vendors; ?>';
	/* ]]> */
	</script>

	<input type="hidden" id="tag" value="<?=$tag; ?>" />
    <input type="hidden" id="tagExpense" value="<?=$tagExpense; ?>" />
    <input type="hidden" id="expenseID" value="<?=$expenseID; ?>" />
    <input type="hidden" id="contactType" value="V" />
    <input type="hidden" id="numberOfMessagesExpense" value="<?=$numberMessagesExpense; ?>" />
	<div id="tabsExpenses">
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-selected"><a href="#windowViewExpenses"><span><?=lang('finance_view_expenses'); ?></span></a></li>
            <li><a href="#windowMessagesExpense"><span><?=lang('common_messages_expense'); ?></span> <span id="messageCountExpense" class="tabCount lightGray">(0)</span></a></li>
	        <li><a href="#windowManageVendors"><span><?=lang('common_vendors'); ?></span></a></li>
	        <li><a href="#windowViewFinanceReports"><span><?=lang('widget_reports'); ?></span></a></li>
			<!--<li class="alert"><a href="#windowAlerts"><span><?=lang('common_alerts'); ?></span> <span class="red">(2)</span></a></li> -->
	    </ul>
	    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">	    	
			<div id="windowViewExpenses" class="ui-tabs-hide">
		        <div class="barYellow bottom"><div style="padding: 6px;">
		        	<div style="float: left;">
			        	<input type="text" style="width: 300px;" class="search" id="sExpenseAutocompleter" />
			        	<div class="loadingInline"></div>
						<input type="hidden" id="sExpenseID" />
					</div>
					<div style="float: right">
                        <button class="smallButton" id="buttonCloseExpenseDetails" style="display: none; float: left;" title="<?=lang('common_list_view_return'); ?>"><span class="listView"><?=lang('common_list_view_return'); ?></span></button>
                    </div>
					<div style="clear: both;"></div>
		        </div></div>
		        <div id="expenseInformationContainer" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
                <div id="expenseDetailsContainer" style="padding: 6px; display: none;">
					<div id="expenseDetailsHTML"></div>

                    <!-- Expense Comments Area -->
                    <div class="barYellow bottom">
                        <div style="padding: 6px;">
                            <div id="newMessageExpense" itemID="" itemType="expense" class="messageContainer"></div>
                        </div>
                    </div>
                    <div id="messageListExpense" style="margin-top: 6px;"></div>
                    <div id="messageMoreContainer" class="boxBlue" style="display: none; margin-top: 6px;"><a href="#" class="linkMoreMessages"><?=lang('common_more'); ?></a></div>

				</div>
				<div id="expenseMessageContainer" style="display: none;"></div>
			</div>
            <div id="windowMessagesExpense" class="ui-tabs-hide">
                <div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessageExpenseAll" itemID="0" itemType="expense" class="messageContainer"></div>
					</div>
				</div>
				<div id="messageListExpenseAll" style="margin-top: 6px;"></div>
	   		</div>
            <div id="windowManageVendors" class="ui-tabs-hide">
		        <div class="barYellow bottom"><div style="padding: 6px;">
		        	<div style="float: left;">
			        	<input type="text" style="width: 300px;" class="search" id="sVendorAutocompleter" />
			        	<div class="loadingInline"></div>
						<input type="hidden" id="sVendorID" />
					</div>
					<div style="padding: 6px; float: right;">
						<button class="buttonExpand blueGray" id="buttonNewContact"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator add"><?=lang('finance_add_vendor'); ?></span></span></button>
						<div style="clear: left;"></div>
					</div>
					<div style="clear: both;"></div>
		        </div></div>
		        <div id="formContactContainer" style="display: none; padding: 6px;">
                    <? $this->load->view('includes/contactForm'); ?>
                </div>
                <div id="contactsContainer" style="padding: 0 0 6px 6px;"></div>
			</div>
	    	<div id="windowViewFinanceReports" class="ui-tabs-hide" style="background: #fff;">
	        	<div class="barYellow bottom"><div style="padding: 6px;">
                    <div style="float: left;">
                        <select id="selectReportType" style="width: 300px; margin-right: 6px;" class="chart">
                            <option value="repInvoicedReceived"><?=lang('reports_invoiced_received'); ?></option>
                            <option value="repIncomeExpenses"><?=lang('reports_income_expenses'); ?></option>
                            <option value="repInvoicedByProject"><?=lang('reports_invoiced_by_project'); ?></option>
                            <option value="repInvoicedByClient"><?=lang('reports_invoiced_by_client'); ?></option>
                            <option value="repExpensesByProject"><?=lang('reports_expenses_by_project'); ?></option>
                            <option value="repExpensesByClient"><?=lang('reports_expenses_by_client'); ?></option>
                            <option value="repExpensesByCategory"><?=lang('reports_expenses_by_category'); ?></option>
                            <option value="repExpensesByVendor"><?=lang('reports_expenses_by_vendor'); ?></option>
                        </select>
                        <select id="selectReportYear" style="width: 100px;">
                        <?
                        if ($rptMinYearInvoice<date('Y')) {
                            for ($a=date('Y');$a>=$rptMinYearInvoice;$a--) {
                                echo '<option value="'.$a.'">'.$a.'</option>';
                            }
                        } else {
                            echo '<option value="'.$rptMinYearInvoice.'">'.$rptMinYearInvoice.'</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <span class="hide" id="reportDateRangeContainer">
                        <div style="float: left;">
                            <input type="text" id="reportDateStart" name="reportDateStart" class="dateField" style="width: 80px; margin-right: 6px;" />
                            <input type="text" id="reportDateEnd" name="reportDateEnd" class="dateField" style="width: 80px; margin-right: 6px;" />
                        </div>
                        <button class="smallButton" id="submitFinanceReport" style="float: left;" title="<?=lang('common_create_report'); ?>"><span class="chart"><?=lang('common_create_report'); ?></span></button>
                    </span>
                    <div style="clear: left;"></div>
                </div></div>
                <div style="margin: 6px;"><div id="chartContainer"></div></div>
                <div style="margin: 18px 6px 6px 6px;"><div id="gridContainer"></div></div>
	   		</div>
			<div id="windowAlerts" class="ui-tabs-hide">
		        <div style="padding: 6px;">
		    		Alerts will include: 1. Invoice payment overdue, 2. Project marked complete but not invoices.
		    	</div>
		    </div>
		</div>
	</div>
	<? $this->load->view('includes/emailInvoiceForm.php'); ?>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>