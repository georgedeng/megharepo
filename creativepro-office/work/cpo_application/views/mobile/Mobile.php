<!DOCTYPE html>
 <html>
 <head>
     <meta charset="utf-8">
     <title>CreativePro Office</title>

	 <!-- Sencha Touch CSS -->
	 <link rel="stylesheet" href="<?=BASE_URL; ?>css/mobile/sencha-touch.css" type="text/css">
     <!-- Custom CSS -->
     <link rel="stylesheet" href="<?=BASE_URL; ?>css/mobile/mobile.css" type="text/css">
     <!--<link rel="stylesheet" href="<?=BASE_URL; ?>css/fullcalendar.css" type="text/css">-->

     <script type="text/javascript">
         var siteURL = '<?=BASE_JS_URL; ?>';
     </script>
	 <!-- Sencha Touch JS -->
	 <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/sencha-touch-debug.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/jquery/jquery-1.4.2.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/fullCalendar/fullcalendar_mobile.js"></script>

     <!-- Application JS -->
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/models.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/index.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/timesheets.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/tasks.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/messages.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/contacts.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/calendar.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/files.js"></script>
     <script type="text/javascript" src="<?=BASE_URL; ?>js/mobile/finances.js"></script>
 </head>
 <body>
     <input type="hidden" id="logged" value="<?=$isLogged; ?>" />
     <input type="hidden" id="authKey" value="<?=$authKey; ?>" />
     <input type="hidden" id="taskTimerStart" />

     <div id="taskMenu" class="x-form-fieldset x-hidden-display">
         <div class="whitePanel x-panel-body">
             <div id="overdueTaskMenuItem" class="iconListBig taskOverdue"><span>Overdue tasks</span></div>
             <div id="overdueTaskMenuItem" class="iconListBig taskOverdue"><span>Tasks due today</span></div>
             <div id="upcomingTaskMenuItem" class="iconListBig taskOverdue"><span>Upcoming tasks</span></div>
         </div>
     </div>

     <!-- Job Timer Template -->
     <div id="jobTimer" class="x-form-fieldset x-hidden-display">
         <div class="whitePanel x-panel-body">
             <div id="timerContainer">
                 <span id="timerDisplayControl" class="timerClockReadout">00:00:00</span>
             </div>
         </div>
     </div>

     <!-- Contact List Template -->
     <div id="contactList" class="x-hidden-display">
         <div class="contact" >
            <div class="avatar"><img src="{AvatarURL}" /></div>
                <p class="biggerText">{NameFirst} <strong>{NameLast}</strong></p>
                <p><span class="subText">{Company}</span></p>
         </div>
     </div>

     <!-- Contact Details Template -->
     <div id="contactDetails" style="width: 90%;" class="x-form-fieldset x-hidden-display">
         <div style="background: url({AvatarURLBig}) no-repeat; min-height: 80px; margin: 18px 0 18px 26px; padding-left: 92px;">
             <span class="biggerText"><strong>{NameFirst} {NameLast}</strong></span>
             <p><span class="subText">{Company}</span></p>
         </div>
             
         <div class="whitePanelRadius x-panel-body">
             <tpl for="ContactItems">
                <div class="contactItemContainer">
                    <div class="blue contactLeft">{ContactType}</div>
                    <div class="contactRight">
                        <tpl if="ContactType == 'email'">
                            <a href="mailto:{ContactData}" class="linkNoShow">
                        </tpl>
                        <tpl if="ContactData.length > 23">
                            {[values.ContactData.substr(0,23)]}...
                        </tpl>
                        <tpl if="ContactData.length < 23">
                            {ContactData}
                        </tpl>
                        <tpl if="ContactType == 'email'">
                            </a>
                        </tpl> 
                    </div>
                </div>
             </tpl>
         </div>
     </div>

 </body>
 </html>
