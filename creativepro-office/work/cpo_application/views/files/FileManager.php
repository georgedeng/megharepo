<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="fileView">
    <div id="filesContainer">
        <div class="barYellow bottom"><div style="padding: 6px;">
            <div id="panelDefault">
                <div style="float: left; margin-right: 12px;">
                    <div class="fileManagerMeter" style="margin-bottom: 12px;">
                        <div class="fileManagerMeterBar"></div>
                        <div class="fileManagerMeterText subText"></div>
                    </div>
                </div>
                <div style="float: right; margin-right: 12px;">
                    <ul class="smallButtonCluster">
                        <li><a href="#" id="thumbnailView"><span class="thumbnailView"></span></a></li>
                        <li><a href="#" id="detailView"><span class="listView"></span></a></li>
                    </ul>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div id="panelNewFolder" class="hide">
                <?=lang('files_new_folder_name'); ?><br />
                <input type="hidden" id="folderAction" value="add" />
                <input type="hidden" id="folderID" />

                <input type="text" style="width: 350px;" id="folderName" />
                <div style="margin: 6px 0 0 0;">
                    <button id="buttonNewFolderSave" style="float: left;" class="smallButton"><span class="save"><?=lang('button_save'); ?></span></button>
                    <button id="buttonNewFolderCancel" style="float: left; margin-left: 12px;" class="smallButton"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
                    <div style="clear: left;"></div>
                </div>
            </div>
            <div id="panelUpload" class="fileUploaderContainer hide"></div>
        </div></div>
        <div id="fileWindowNavContainer" style="margin: 6px 0 0 6px;" class="breadCrumb"></div>
        <h3 id="fileWindowFolderNameContainer" class="icon_folder_open_small" style="margin: 12px 0 6px 6px;"><?=lang('files_all_files'); ?></h3>
        <div id="fileWindowViewContainer" style="margin-top: 18px;"></div>
    </div>
    <div id="fileViewContainer" class="hide">
        <div class="barYellow bottom"><div style="padding: 6px;">
            <a href="#" id="closeFileView">Back to files</a>
        </div></div>
    </div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>