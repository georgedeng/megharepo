<? $this->load->view('includes/header'); ?>
<div id="loginBox" pageID="accountLogin">
	<div class="windowFrame" style="margin-bottom: 10px;">
		<div id="messageContainer" class="boxYellowInner" style="margin: 6px 6px 18px 6px;">
			<?=$message; ?>
		</div>
		
		<div style="float: left; width: 378px;">
			<form id="formLogin" class="noBorder" action="<?=site_url('Login/tryLogin'); ?>" method="POST">
				<p>
				<label style="width: 120px;">User Name</label>	
				<input type="text" style="width: 200px;" id="userid" name="userid" />
				</p>
				<p>
				<label style="width: 120px;">Password</label>	
				<input type="password" style="width: 200px;" id="password" name="password" />
				</p>
				<div style="margin: 4px 0 20px 126px;">
                         <button class="buttonExpand blueGray" id="buttonLogin" tabindex="9"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator login"><?=lang('login_button'); ?></span></span></button>
                    </div>
                    <p style="height: 20px;"></p>
			</form>
			<form id="formFindPassword" action="<?=site_url('Login/findPassword'); ?>" method="POST" style="display: none;">
				<p>
				<label style="width: 120px;">Email address</label>	
				<input type="text" style="width: 200px;" id="email" name="email" />
				</p>
				<div style="margin: 4px 0 10px 126px;">
                         <button class="buttonExpand blueGray" id="buttonFindPassword" style="margin-right: 12px;"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator search"><?=lang('login_find_password'); ?></span></span></button>
                         <a href="#" id="buttonCancelFindPassword" style="float: left;"><?=lang('button_cancel'); ?></a>
				</div>
                    <p style="height: 20px;"></p>
			</form>
		</div>
		<div style="float: left; margin-top: 5px;">
			<ul class="ULStandard">
				<li><a href="http://www.CreativeProOffice.com/createAccount">I need to create an account.</a></li>
				<li><a href="<?=site_url('Login/clientLogin'); ?>">I am a client.</a></li>
				<li><a href="" id="linkFindPassword" title="Find my password">I forgot my password.</a></li>
			</ul>	
		</div>	
		<div style="clear: left;"></div>
	</div>
</div>
<? $this->load->view('includes/footer'); ?>