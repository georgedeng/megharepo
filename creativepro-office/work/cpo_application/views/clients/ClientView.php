<? $this->load->view('includes/header'); ?>
<input type="hidden" id="tag" value="<?=$tag; ?>" />
<input type="hidden" id="numberOfMessages" value="<?=$numberMessages; ?>" />
<div id="pageLeftColumn" pageID="viewAllClients" class="helptag" data-help="clients">
	<div id="tabsClient" class="ui-tabs">
		<ul class="ui-tabs-nav"> 
	        <li class="ui-tabs-selected"><a href="#windowViewClients"><span><?=lang('client_page_title'); ?></span></a></li> 
	        <li><a href="#windowMessages"><span><?=lang('common_messages'); ?></span> <span id="messageCount" class="tabCount lightGray">(0)</span></a></li>
	    </ul> 
	    <div style="min-height: 200px;" class="windowFrame"> 
	    	<div id="windowViewClients">
		        <div class="barYellow bottom"><div style="padding: 6px;">
		        	<div style="float: left;">
			        	<input type="text" style="width: 300px;" class="search" id="sClientAutocompleter" />
			        	<div class="loadingInline"></div>
						<input type="hidden" id="sClientID" />
					</div>	
					<div style="float: right">
						<select id="selectClientSort" class="sort" style="width: 200px;">
							<option value="company"><?=lang('common_company_name'); ?></option>
							<option value="invoice"><?=lang('common_invoice_total'); ?></option>
							<option value="project"><?=lang('common_number_projects'); ?></option>
						</select>
					</div>
					<div style="clear: both;"></div>
		        </div></div>		        
		        <div id="clientListContainer" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
			</div>        
	    	<div id="windowMessages" class="ui-tabs-hide">
                <div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessage" itemID="0" itemType="client" class="messageContainer"></div>
					</div>
				</div>
				<div id="messageListClient" style="margin-top: 6px;"></div>
	   		</div>
		    <div id="windowAlerts" class="ui-tabs-hide"> 
		    	<div style="padding: 6px;">
		    		Alerts will include: 1. Project or tasks overdue, 2. Invoice payment overdue.
		    	</div>
		    </div> 
		</div>	    
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>