<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <script type="text/javascript">
		// <![CDATA[
		var flashPath = 'http://dev.mycpohq.com/flash/AIRInstallBadge.swf';
		var flashvars = {
			airversion:    '2.0',
            appurl:        '<?=ASSET_URL; ?>desktop/CreativeProOffice.air',
            appname:       'CreativePro Office Desktop',
            appid:         'CreativeProOffice',
            pubid:         '',
            appversion:    '1.0.0',
            image:         '<?=ASSET_URL; ?>desktop/CreativeProOfficeAIRBadge.png',
            appinstallarg: 'installed from web',
            applauncharg:  'launched from web',
            helpurl:       'http://dev.mycpohq.com',
            hidehelp:      'false',
            skiptransition:'false',
            titlecolor:    '#00AAFF',
            buttonlabelcolor: '#00AAFF',
            appnamecolor:     '#00AAFF'
		};
		var params = {
			WMode: 'window',
			Quality: 'high',
			Menu: -1,
			BGColor: '#ffffff',
			SeamlessTabbing: 1
		};
		var attributes = {};
		swfobject.embedSWF(flashPath, 'alternateVideoContent', "215", "180", "9.0.115",'http://dev.mycpohq.com/flash/expressInstall.swf', flashvars, params, attributes);

		// ]]>
	</script>
    <div id="alternateVideoContent">
		<a href="http://www.adobe.com/go/getflashplayer">
			<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
		</a>
	</div>
</div>
<div class="rightPane">
</div>
<? $this->load->view('includes/footerPublic'); ?>
