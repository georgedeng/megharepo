<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="mBottom12">Your purchase has been canceled.</h1>
    <p>We're sorry you changed your mind.  Please <a href="contact">let us know</a> if something
    went wrong or if you have any questions. <strong>Your credit card has not been billed.</strong></p>

</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>
