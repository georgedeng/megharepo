<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="mBottom12" style="float: left;">Self-hosted downloads</h1>
    <p style="float: right;">
        <strong>Welcome, <?=$this->session->userdata('company'); ?></strong> (<a href="<?=INSECURE_URL; ?>selfhosteddownload/logout">Logout</a>)<br />
        <?
        $expDate = $this->session->userdata('expDate');
        if (!empty($expDate)) {
            echo 'Your subscription expires on '.$expDate;
        }
        ?>
    </p>
    <div style="clear: both;"></div>
    <?
    if ($this->session->userdata('isPaid') == true) {

        if ($firstAccess == 'firstAccess') {
            echo '<div class="successMessageBig">Thank you for buying the CreativePro Office self-hosted version!</div>';
            echo '<p style="margin-bottom: 12px;">You will receive a confirmation email shortly with the details of your purchase.</p>';
        }
        if ($firstAccess == 'updateRenew') {
            echo '<div class="successMessageBig">Thank you for renewing your update and support subscription!</div>';
            echo '<p style="margin-bottom: 12px;>You will receive a confirmation email shortly with the details of your purchase.</p>';
        }
        ?>
        <div class="boxBlue" style="padding: 12px;">
            <div class="iconInstallerBig">
                <a class="bigText" href="<?=site_url('selfhosteddownload/getDownload/'.$fullVersion['UpdateFilename']); ?>">Full download: CreativePro Office version <?=$fullVersion['VersionNumberHuman']; ?></a>
                <p>Release date: <?=date('M j, Y',strtotime($fullVersion['ReleaseDate'])); ?></p>
            </div>
        </div>


        <div class="iconUpdatesBig bigText" style="margin: 18px 0 0 14px; padding-top: 12px;">Update packages</div>
        <p style="margin-left: 68px;">Note: you do not need to download and install updates that are versioned prior to your full download. For example,
        if you downloaded full version 2.0.0.1, you should install all updates from version 2.0.0.2 on.</p>

        <table class="dataTable" style="margin: 18px 0 0 68px; width: 560px;">
            <thead>
                <tr>
                    <th>Update filename</th>
                    <th>Version</th>
                    <th style="text-align: right;">Release date</th>
                </tr>
            </thead>

        <?
        foreach($updates as $update) {
            $updateFile    = $update['UpdateFilename'];
            $updateVersion = $update['VersionNumberHuman'];
            $updateDate    = date('M j, Y',strtotime($update['ReleaseDate']));
            echo '<tr><td><strong><a href="'.site_url('selfhosteddownload/getUpdate/'.$updateFile).'">'.$updateFile.'</a></strong></td><td>'.$updateVersion.'</td><td style="text-align: right;">'.$updateDate.'</td></tr>';
        }
        ?>
        </table>
    <? } else { ?>
        <div class="infoMessageBig">Your update subscription has expired.</div>
        <p>You can purchase another 1 year subscription for $<?=$products['updates']['price']; ?>.
        Please <a href="contact">contact support</a> if you have any questions.</p>

        <button class="buttonExpand blueGray" onclick="window.location='<?=SECURE_URL; ?>selfhostedbuy/index/updates/<?=$this->session->userdata('accountID'); ?>';" id="buttonRenew" style="margin-top: 12px;"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator renew">Renew subscription</span></span></button>

    <? } ?>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>
