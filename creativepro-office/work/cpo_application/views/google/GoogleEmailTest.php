<? $this->load->view('includes/header'); ?>

<style type="text/css" media="screen">
	 #scrollup {
	   position: relative;
	   overflow: hidden;
	   border: 1px solid #000;
	   height: 20px;
	   width: 200px
	 }
	 .headline {
	   position: absolute;
	   top: 210px;
     left: 5px;
	   height: 195px;
	   width:190px;
	 }
</style>

<div id="pageLeftColumn" pageID="viewProjectDetails">
	<h1>Google Doc API Test</h1>

	<a href="#" id="linkGmail">Get Gmail</a>
	<div id="emailListContainer"></div>

    <div id="scrollup">
        <div class="headline">Hi, this is headline 1</div>
        <div class="headline">Hi, another headline!</div>
        <div class="headline">Hey there - how are you?</div>
    </div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
