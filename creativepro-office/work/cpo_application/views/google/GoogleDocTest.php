<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="viewProjectDetails">
	<h1>Google Doc API Test</h1>
    <a href="<?=$authLink; ?>">Auth</a>

    <div class="noPrint" style="background: #fff; min-width: 600px;" title="<?=lang('files_file_manager'); ?>">
		<input type="hidden" id="fileManagerSelectedFolder" value="0" />
		
		
	</div>


	<a href="#" id="linkGoogleDocs">Get Google Docs</a>
	<div id="docListContainer"></div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>