<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class About extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
        $this->load->library('twitter/tweet');
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtAboutHeader_public.png';
        //$data['tweets']                = $this->twitter->call('statuses/user_timeline', array('id' => 'cpohq'));
        
        $timeline = $this->tweet->call('get', 'statuses/home_timeline');

			var_dump($timeline);
        
        $data['tweets'] = $this->tweet->call('get','statuses/user_timeline', array('id' => 'cpohq'));
        print_r($data['tweets']);
        
        $data['rightColumnComponents'] = array('loginBox','whosUsingCPO','newsFeeds');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'About Us';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
		$data = About::_init();
        $this->load->view('public_site/About',$data);
    }
}
?>