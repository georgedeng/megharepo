<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
class Error extends Controller {
    function __construct()
	{
		parent::Controller();
        loadLanguageFiles();

        $this->load->model('common/public_site','',true);
	}

    function _init($errorType=NULL) {
        $data['errorTitle']   = lang('error_title_'.$errorType);
        $data['errorMessage'] = lang('error_site_text');
        $data['jsFileArray']  = array();

        if ($this->session->userdata('logged') == 1) {
            $data['page']            = 'error';
            $data['siteName']        = 'CreativePro Office';
            $data['pageTitle']       = lang('error_page_title');
            $data['wysiwyg']         = 0;
            $data['map']             = 0;
            $data['pageIconClass']   = 'iconErrorPage';
            $data['pageLayoutClass'] = 'withRightColumn';
            $data['loadView']        = 'Error';
         } else {
            $data['child']           = TRUE;
            $data['headerTextImage'] = 'txtErrorHeader_public.png';
            $data['rightColumnComponents'] = array('loginBox','whosUsingCPO');
            $data['jsFileArray']           = array();
            $data['pageTitle']       = lang('error_page_title');

            /*
             * Get some customer logos
             */
            $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
            $data['loadView']  = 'ErrorPublic';
         }

         return $data;
    }

    function index() {
        /*
         * This method handles 404 errors!
         */
        $data = Error::_init('404');
        $data['errorType'] = '404';
        $this->load->view('error/'.$data['loadView'],$data);
    }

	function general() {
		/*
         * This method handles general errors!
         */
        $data = Error::_init();
        $data['errorType'] = '404';
        $this->load->view('error/'.$data['loadView'],$data);
	}

    function db() {
        /*
         * This method handles database errors!
         */
        $data = Error::_init('database');
        $data['errorType'] = 'database';
        $this->load->view('error/'.$data['loadView'],$data);
    }

    function php() {
        /*
         * This method handles PHP errors!
         */
        $data = Error::_init('PHP');
        $data['errorType'] = 'PHP';
        $this->load->view('error/'.$data['loadView'],$data);
    }

    function offline() {
        /*
         * This method handles the application being offline!
         */
        $data = Error::_init('offline');
        $data['errorType'] = 'offline';
        $this->load->view('error/'.$data['loadView'],$data);
    }
}
?>