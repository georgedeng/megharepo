<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ExpenseView extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('finances/Finance_view','',true);
        $this->load->model('finances/Finance_reports','',true);
        $this->load->model('finances/Finance_view_expenses','',true);
		$this->load->model('users/User_information','',TRUE);
		$this->load->model('settings/Settings_update','',TRUE);
        $this->load->model('contacts/Contacts','',true);
        $this->load->model('messaging/Site_messages','',true);

		$this->load->library('application/CommonFinance');
		$this->load->library('application/CommonAppData');
        $this->load->library('application/CommonEmail');
	}

	function _init($tag=NULL,$expenseID=NULL,$printExpense=NULL) {
		global $subMenuArray;

		$data['page']                  = 'finances';
		$data['pageTitle']             = lang('finance_page_title');
		$data['pageSubTitle']          = lang('finance_view_expenses');
		$data['wysiwyg']               = 0;
        $data['charts']                = 1;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsFinances.js','jsExpenses.js','jsFinanceReports.js','jsContacts.js','jsSettings.js');
		$data['pageIconClass']         = 'iconinvoices';
		$data['pageLayoutClass']       = 'withRightColumn';
		$data['tag']                   = $tag;
        $data['tagExpense']            = $tagExpense;
		$data['rightColumnComponents'] = array('expenseFilter','tagRender','feedExportSidebar');

		$data['tagRenderArray']       = $this->commonappdata->renderTags('expense','array');

        $permArray = $this->session->userdata('permissions');

        $data['pageButtons'] = array();
        if ($permArray['expenseCreate'] == 1) {
            $data['pageButtons']['add'] = array(lang('menu_new_expense'),lang('menu_new_expense'),'blueGray','buttonAddExpense hide','');
        }
        if ($permArray['expenseUpdate'] == 1) {
            $data['pageButtons']['edit'] = array(lang('button_edit'),lang('client_edit_client'),'blueGray','buttonEditExpense hide','');
        }
        if ($permArray['expenseDelete'] == 1) {
            $data['pageButtons']['delete'] = array(lang('button_delete'),lang('client_delete_client'),'blueGray','buttonDeleteExpense hide','');
        }
        $data['pageButtons']['print'] = array(lang('button_print'),lang('button_print'),'green','buttonPrint hide','');

        $data['invoiceID']             = $invoiceID;

        $data['rptMinYearInvoice'] = date('Y');
        $data['rptMinYearExpense'] = date('Y');
        $rptMinYearInvoice = $this->Finance_reports->getInvoiceReportYearRange($this->session->userdata('accountUserid'));
        if ($rptMinYearInvoice != FALSE) {
            $data['rptMinYearInvoice'] = $rptMinYearInvoice;
        }

        $rptMinYearExpense = $this->Finance_reports->getExpenseReportYearRange($this->session->userdata('accountUserid'));
        if ($rptMinYearExpense != FALSE) {
            $data['rptMinYearExpense'] = $rptMinYearExpense;
        }

        $today = convertMySQLToGMT(date('Y-m-d'),$direction='pull',$format='Y-m-d');
		$data['payDateDefault'] = MySQLDateToJS($today);

        /*
         * Get any vendors
         */
        $vendorArray = $this->Contacts->getContacts($this->session->userdata('accountUserid'),NULL,NULL,0,1);
        $data['vendors'] = json_encode($vendorArray);

        /*
         * Should we print this expense?
         */
        if ($printExpense != NULL && $printExpense != 0) {
            $printExpense = 1;
        } else {
            $printExpense = 0;
        }
        $data['printExpense'] = $printExpense;

        $permissionArray = $this->session->userdata('permissions');
		$viewAll = TRUE;

		if ($permissionArray['messageViewOwn'] == 1) {
            $viewAll = FALSE;
		}

        $numberMessagesExpenseArray = $this->Site_messages->getNumberOfMessagesForSomething(NULL,'expense',$this->session->userdata('userid'),$this->session->userdata('accountUserid'),$this->session->userdata('pid'),$viewAll);
		$data['numberMessagesExpense'] = $numberMessagesExpenseArray['NoOfMessagesTotal'];

        if ($expenseID == NULL || empty($expenseID)) {
            $data['expenseID'] = 0;
        } else {
            $data['expenseID'] = $expenseID;
        }

        /*
		 * Create RSS Feed Links
		 */
        $expenseFeedLink  = site_url('rss/Rss').'/createRSSFeed/'.$this->session->userdata('persAuthString').'/finance/expense';
		$expenseFeedTitle = lang('finance_expenses');

		$data['rssFeeds'] = array(
			array('Link' => $expenseFeedLink, 'Title' => $expenseFeedTitle, 'Type' => 'expense')
		);

        $financeReportExcelLink  = '';
        $financeReportExcelTitle = lang('common_export_excel');
        $data['exports'] = array(
            array('Link' => $financeReportExcelLink, 'Title' => $financeReportExcelTitle, 'Type' => 'excel', 'ID' => 'excelFinanceReport')
        );

		return $data;
	}

	function index($tag=NULL) {
		$data = ExpenseView::_init();
		$this->load->view('finances/ExpenseView',$data);
	}

	function tag($tag=NULL) {
		$data = ExpenseView::_init($tag);
		$this->load->view('finances/ExpenseView',$data);
	}

    function getInvoiceListForSelect($clientID=NULL,$projectID=NULL,$status=NULL,$renderType='json',$return=0) {
        $invoiceArray = $this->Finance_view->getInvoiceListForSelect($clientID,$projectID,$status,$this->session->userdata('accountUserid'));
        if ($return == 1) {
            if ($renderType == 'json') {
                return json_encode($invoiceArray);
            } else {
                return $invoiceArray;
            }
        } else {
            if ($renderType == 'json') {
                echo decode_utf8(json_encode($invoiceArray));
            }
        }
    }

    function searchExpenses() {
		$q = cleanStringTag(strtolower($this->input->get('term', TRUE)));
		if (!$q) return;
		$expenseArray = $this->Finance_view_expenses->getExpensesForAutoCompleter($q,$this->session->userdata('accountUserid'));
		echo $expenseArray;
	}

    /**
	 * getExpenseList
	 *
	 * @access	public
	 * @param	int    $itemID Userid, ClientID, ProjID depending on if we want all invoices
	 *                         for account owner, all invoices for a client or all invoices for a project.
	 * @param   string $group  user, client, project
	 * @param   string $renderType
	 * @param   bool   $return
	 * @param   int    $limit
	 * @return	array  Array of expense data
	 */
	function getExpenseList($itemID,$group=null,$tag=NULL,$status=NULL,$renderType=null,$return=1) {
		if ($group == 'user') {
			$itemID = $this->session->userdata('accountUserid');
		}

        if (isset($_POST['filter'])) {
            $filterArray = $_POST;
            $expenseArray = $this->Finance_view_expenses->getExpenseList($itemID,$group,$tag,$status,$filterArray);
        } else {
            $expenseArray = $this->Finance_view_expenses->getExpenseList($itemID,$group,$tag,$status);
        }

		if ($renderType == 'json') {
            if (count($expenseArray)<1) {
                $jsonString = '{"Expenses":[]}';
            } else {
                $headerString = lang('common_title').'|'.lang('common_category').'|'.lang('common_date').'|'.lang('finance_expense_amount');
                $footerString = lang('common_total');

                $currencyMark = getCurrencyMark($this->session->userdata('currency'));
                $jsonString = '{"HeaderLabels":"'.$headerString.'","FooterLabels":"'.$footerString.'","Expenses":[';
                $totalExpenses = 0;
                foreach($expenseArray as $expense) {
                    $totalExpenses = $totalExpenses+$expense['Amount'];
                    $expense['Total'] = $currencyMark.number_format($expense['Amount'],2);

                    if ($expense['DateExpense'] != '0000-00-00') {
                        $expense['DateExpense'] = date('M j, Y', strtotime($expense['DateExpense']));
                        $expense['DateExpenseSort'] = date('m/d/Y', strtotime($expense['DateExpense']));
                    } else {
                        $expense['DateExpense'] = '';
                        $expense['DateExpenseSort'] = '';
                    }
                    $expense['InvoiceDate'] = date('M j, Y', strtotime($expense['InvoiceDate']));

                    $jsonString .= json_encode($expense).',';
                }
                $jsonString = substr($jsonString,0,-1).'],"TotalExpenses":"'.$currencyMark.number_format($totalExpenses,2).'"}';
            }
			if ($return == 1) {
				return stripslashes($jsonString);
			} else {
				echo stripslashes($jsonString);
			}
		}
	}

    function viewExpense($expenseID=NULL) {
        $data = ExpenseView::_init(NULL,$expenseID);
		$this->load->view('finances/ExpenseView',$data);
    }

    function getExpenseDetails($expenseID,$renderType=NULL,$return=1,$language=NULL) {
		$expenseDetails = $this->commonfinance->getExpenseDetails($expenseID,$renderType,$return,$language);

		if ($return == 1) {
			return $expenseDetails;
		} else {
			echo $expenseDetails;
		}
	}
}
?>