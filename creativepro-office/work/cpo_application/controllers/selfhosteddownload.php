<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Selfhosteddownload extends Controller {

    function __construct(){
		parent::Controller();

		$this->load->model('common/public_site','',true);
        $this->load->model('common/self_hosted','',true);
        $this->load->model('accounts/accounts','',true);

        $this->load->library('application/CommonEmail');

        $this->load->helper('account');
        $this->load->helper('date');
        $this->load->helper('payment');
        $this->load->helper('templates');
	}

    function _init() {
        global $monthNameArray;

        $data['child']           = TRUE;
        $data['page']            = 'selfhosteddownload';
        $data['headerTextImage'] = 'txtSelfHostedHeader_public.png';
        $data['jsFileArray']     = array();
        $data['pageTitle']       = 'Self-hosted downloads';

        return $data;
    }

    function index($firstAccess=null) {
        global $productArray;
        $data = Selfhosteddownload::_init();
        $data['firstAccess'] = $firstAccess;
        $data['products']    = $productArray;

        /*
         * Checked for logged in user
         */
        if ($this->session->userdata('selfHostedLogged') != true) {
            $data['message'] = 'Your order number and installation key can be found in the confirmation email you received when you purchased your product. If you need help locating these numbers, please <a href="'.site_url('contact').'">contact support</a>.';
            $this->load->view('public_site/Selfhostedlogin',$data);
        } else {
            $data['fullVersion'] = $this->self_hosted->getUpdates(true);
            $data['updates']     = $this->self_hosted->getUpdates();
            $this->load->view('public_site/Selfhosteddownload',$data);
        }

    }

    function logout() {
        $this->session->sess_destroy();
		header('Location: '.site_url('selfhosteddownload'));
    }

    function tryLogin() {
        $orderNo         = $this->input->post('orderNo', TRUE);
		$installationKey = $this->input->post('installationKey', TRUE);

        if (empty($orderNo) || empty($installationKey)) {
            $data = Selfhosteddownload::_init();
            $data['message'] = '<div class="errorMessageSmall">Your order number or installation key is blank.</div>';
            $this->load->view('public_site/Selfhostedlogin',$data);
        } else {
            $loginArray = $this->accounts->trySelfHostedLogin($orderNo,$installationKey);
            if ($loginArray == false) {
                $data = Selfhosteddownload::_init();
                $data['message'] = '<div class="errorMessageSmall">Your order number or installation key cannot be found. If you feel this is an error, please <a href="'.site_url('contact').'">contact support</a>.</div>';
                $this->load->view('public_site/Selfhostedlogin',$data);
            } else {
                /*
                 * See if we have an active subscription.
                 */
                $accountID = $loginArray['AccountID'];
                $datePaid  = new_date_from_days(date('Y-m-d'),-365);
                $subscriptionStatus = $this->accounts->checkSubscriptionStatus($accountID,$datePaid);

                if ($subscriptionStatus == false) {
                    $isPaid = false;
                    $expDate = '';
                } else {
                    $isPaid = true;
                    $expDate = date('M j, Y',strtotime($subscriptionStatus['DatePaid'])+(60*60*24*365));
                }

                /*
                 * Log 'em in.
                 */
                $sessionArray['orderNumber']      = $orderNo;
                $sessionArray['installationKey']  = $installationKey;
                $sessionArray['selfHostedLogged'] = true;
                $sessionArray['company']          = $loginArray['Company'];
                $sessionArray['email']            = $loginArray['Email'];
                $sessionArray['isPaid']           = $isPaid;
                $sessionArray['accountID']        = $accountID;
                $sessionArray['expDate']          = $expDate;
                $this->session->set_userdata($sessionArray);
                header('Location: '.site_url('selfhosteddownload'));
            }
        }
    }

    function getDownload($file) {
        $accountID = $this->session->userdata('accountID');
        $file = '/var/www/mycpohq.com/downloads/'.$file;
        if (empty($accountID)) {
            header('Location: '.site_url('selfhosteddownload'));
        } else {
            header("Content-type: application/force-download");
            header("Content-Transfer-Encoding: Binary");
            header("Content-length: ".filesize($file));
            header("Content-disposition: attachment; filename=\"".basename($file)."\"");
            readfile($file);
        }
    }

    function getUpdate($file) {
        $accountID = $this->session->userdata('accountID');
        if (empty($accountID)) {
            header('Location: '.site_url('selfhosteddownload'));
        } else {
            $file = '/var/www/mycpohq.com/downloads/updates/'.$file;
            header("Content-type: application/force-download");
            header("Content-Transfer-Encoding: Binary");
            header("Content-length: ".filesize($file));
            header("Content-disposition: attachment; filename=\"".basename($file)."\"");
            readfile($file);
        }
    }
}