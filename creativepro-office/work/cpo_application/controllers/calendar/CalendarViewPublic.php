<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CalendarViewPublic extends Controller {
	function __construct()
	{
		parent::Controller();
        loadLanguageFiles();

		$this->load->model('calendar/Calendar_view','',true);
        $this->load->model('clients/Client_view','',true);
        $this->load->model('contacts/Contacts','',true);
        $this->load->model('common/app_data','AppData',TRUE);

		$this->load->library('application/CommonAppData');
        $this->load->library('application/CommonCalendar');
        $this->load->helper('download');
	}

    function makeIcalendar($authKey,$calendarID=NULL) {
        $userArray = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF']);
		$error = FALSE;
		if ($calendarID>0) {
			/*
			 * Then just get events for 1 calendar
			 */
			/*
			 * Only get dates that are 30 days in the future.
			 */
			$filename = lang('calendar').'.ics';
			$endDate = new_date_from_days(date('Y-m-d'),30);
			$eventArray = $this->Calendar_view->getCalendarEntryList($calendarID,20,$endDate,$userArray['UseridAccount']);
		} else {
			/*
			 * Get all calendars for this user
			 */
			$filename = lang('calendar').'.ics';

			/*
			 * Only get dates that are 30 days in the future.
			 */
			$endDate = new_date_from_days(date('Y-m-d'),30);
			$eventArray = $this->Calendar_view->getCalendarEntryList(NULL,20,$endDate,$userArray['UseridAccount']);
		}

		if ($error == FALSE) {
            $data  = "BEGIN:VCALENDAR";
            $data .= "\r\nVERSION:2.0";
            $data .= "\r\nCALSCALE:GREGORIAN";
			$data .= "\r\nPRODID:-//CreativePro Office//Calendar 2.0//".strtoupper($userArray['Language']);
            //$data .= "\r\nPRODID:-//Google Inc//Google Calendar 70.9054//EN";
			$data .= "\r\nX-WR-CALNAME: My calendar";
			$data .= "\r\nX-WR-TIMEZONE: US/Eastern";
			$data .= "\r\nX-ORIGINAL-URL: http://www.creativeprooffice.com";
			$data .= "\r\nX-WR-CALDESC: Calendar entries from Jeff";
            $data .= "\r\nMETHOD:PUBLISH";
            if ($calendarID>0) {
                $data .= "\r\nX-WR-CALNAME:Tech Stuff";
            }

			foreach($eventArray as $event) {
                $data .= "\r\nBEGIN:VEVENT";
                $data .= "\r\n".formatIcalData("SUMMARY:".$event['Title']);
                $data .= "\r\n" . formatIcalData("DESCRIPTION:".$event['Description']);

				if ($event['DateEnd'] != '0000-00-00' && $event['DateEnd'] != '0000-00-00 00:00:00') {
					$dateEnd = "\r\nDTEND:".convertMySQLToGMT($event['DateEnd'],'pull','Ymd\THis\Z');
				} else {
					$dateEnd = '';
				}

				$dateStart  = convertMySQLToGMT($event['DateStart'],'pull','Ymd\THis\Z');

                $data .= "\r\nDTSTART:".$dateStart;
                $data .= $dateEnd;
                $data .= "\r\nDTSTAMP:".date('Ymd\THis\Z');
                $data .= "\r\nCREATED:19000101T120000Z";
                $data .= "\r\nUID:".generate_guid()."@creativeprooffice.com";
                //$data .= 'SUMMARY;ENCODING=QUOTED-PRINTABLE:'.$eventTitle.Chr(10);
                //$data .= 'DESCRIPTION;ENCODING=QUOTED-PRINTABLE:'.$eventDescription.Chr(10);
                $data .= "\r\nTRANSP:OPAQUE";
                $data .= "\r\nEND:VEVENT";
			}

            $data .= "\r\nEND:VCALENDAR";
            iCalHeader($filename);
            echo $data;
			//force_download($filename, $data);
		}
	}
}
?>
