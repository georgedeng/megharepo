<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
	}

    function index() {
        if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
            header('Location: '.site_url('settings/SettingsOwners/client'));
        } else {
            header('Location: '.site_url('settings/SettingsOwners'));
        }
    }
}
?>