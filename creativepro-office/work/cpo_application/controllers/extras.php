<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Extras extends Controller {
    function __construct() {
        parent::Controller();
        $this->load->model('common/public_site','',true);
        $this->load->library('twitter');
    }

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = 'txtExtrasHeader_public.png';
        //$data['rightColumnComponents'] = array('loginBox','whosUsingCPO');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Extras and Add-ons for CreativePro Office';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
	$data = Extras::_init();
        $this->load->view('public_site/Extras',$data);
    }
}
?>
