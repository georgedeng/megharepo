<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Terms extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = '';
        $data['pageTitle']       = 'Terms of Use';
        $data['jsFileArray']     = array();

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index($popup=NULL) {
		$data = Terms::_init();
        $this->load->view('public_site/Terms',$data);        
    }

    function popup() {
        $data = Terms::_init();
        $this->load->view('public_site/TermsPopup',$data);
    }
}
?>