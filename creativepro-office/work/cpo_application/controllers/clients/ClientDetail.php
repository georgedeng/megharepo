<?php
class ClientDetail extends Controller {
	function __construct()
	{
		parent::Controller();	
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
		$this->load->model('contacts/Contacts','',TRUE);
		$this->load->model('clients/Client_view','',TRUE);
        $this->load->model('clients/client_update','ClientUpdate',TRUE);
		$this->load->model('finances/Finance_view','',TRUE);
		$this->load->model('projects/Project_view','',TRUE);
        $this->load->model('users/user_login','UserLogin',TRUE);
	}
	
	function _init($clientID=NULL) {
		$data['page']              = 'clients';
		$data['pageTitle']         = lang('client_page_title');
        $data['pageTitleLink']     = site_url('clients/ClientView');
		$data['wysiwyg']           = 0;
		$data['map']               = 0;
		$data['clientID']          = $clientID;
		$data['jsFileArray']       = array('jsClients.js','jsProjects.js','jsFinances.js','jsContacts.js','jsSettings.js');
		$data['pageIconClass']     = 'iconPageClients';
		$data['pageLayoutClass']   = 'withRightColumn';
        
        $data['pageButtons'] = array();
        array_push($data['pageButtons'], '<button class="buttonExpand buttonEditMain action" id="buttonedit" itemid="'.$projectID.'" title="'.lang('button_edit').'"><span class="edit">'.lang('button_edit').'</span></button>');
        array_push($data['pageButtons'], '<button class="buttonExpand buttonDeleteMain action" id="buttondelete" itemid="'.$projectID.'" title="'.lang('button_delete').'"><span class="delete">'.lang('button_delete').'</span></button>');
        		
		$data['clientInformation'] = ClientDetail::getClientDetail($clientID,'json',1);
		$jsonArray = json_decode($data['clientInformation'], true);		
		$data['pageSubTitle']      = $jsonArray['Company'];
        $data['clientUserid']      = $jsonArray['ClientUserid'];

        /*
         * Sometimes clientUserid is 0. This seems to happen sometimes when people import data from 1.0
         * Keep an eye on this but for now, let's just pull over the UID for the client if it's 0.
         */
        if ($data['clientUserid'] == 0 || empty($data['clientUserid'])) {
            $data['clientUserid'] = NULL;
            if (!empty($jsonArray['Email'])) {
                $clientLoginArray = $this->Client_view->getClientLoginInformationFromEmail($jsonArray['Email'],$this->session->userdata('accountUserid'));

                if ($clientLoginArray != FALSE) {
                    $data['clientUserid'] = $clientLoginArray['UID'];
                }
            } else {
                $loginArray['action']        = 'add';
                $loginArray['Userid']        = '';
                $loginArray['Password']      = '';
                $loginArray['RoleID']        = '';
                $loginArray['UserType']      = USER_TYPE_CLIENT;
                $loginArray['UseridAccount'] = $this->session->userdata('accountUserid');

                $data['clientUserid'] = $this->UserLogin->saveLoginInformation($loginArray);
            }

            if ($data['clientUserid'] != NULL) {
                $clientUseridArray['action']       = 'update';
                $clientUseridArray['clientUserid'] = $data['clientUserid'];
                $clientUseridArray['clientID']     = $clientID;
                $newClientID = $this->ClientUpdate->saveClient($clientUseridArray);
            }
        }

        $data['permissions'] = $this->session->userdata('permissions');

        noCache();
		return $data;
	}
	
	function index($clientID=NULL) {
		if (!is_numeric($clientID)) {
			header('Location: '.BASE_URL.'clients/ClientView');
		}
		$data = ClientDetail::_init($clientID);
		$this->load->view('clients/ClientDetail',$data);
	}
		
	function getClientDetail($clientID,$renderType=NULL,$return=1) {
		/*
		 * Check and make sure we can view this client
		 */
		if (!$this->Client_view->checkForValidClient($clientID,$this->session->userdata('accountUserid')) || empty($clientID)) {
			/*
			 * Send them back to this users' client view page.
			 */
			header('Location: '.site_url('clients/ClientView'));
		} else {
			$clientArray = $this->Client_view->getClientInformation($clientID);

            $clientUserid = $clientArray['ClientUserid'];
            $invoiceTotal = 0;
            $invoiceCount = 0;
            if (!empty($clientUserid) && $clientUserid != 0) {
                $invoiceTotal = $this->Finance_view->getInvoiceTotals($clientUserid,'client');
                $invoiceCount = $this->Finance_view->getInvoiceCount($clientUserid,'client',$this->session->userdata('accountUserid'));
            }
			$invoiceTotalString = number_format($invoiceTotal['Total'],2);
            $clientArray['InvoiceTotalString'] = getCurrencyMark($this->session->userdata('currency')).$invoiceTotalString;
			$clientArray['InvoiceTotal']       = $invoiceTotal['Total'];
			$clientArray['InvoiceCount']       = $invoiceCount['InvoiceCount'];
						
			$projectTotal = $this->Project_view->getNumberOfProjects($clientID,'client');
			$projectTotal = $projectTotal['Projects'];
			$clientArray['TotalProjectsString'] = lang('project_total').': '.$projectTotal;
			$clientArray['TotalProjects'] = $projectTotal;
            if ($clientArray['Country'] == 0) {
				$clientArray['Country'] = '';
			}
			if (	
				!empty($clientArray['Address'])  && 
				!empty($clientArray['City'])  && 
				!empty($clientArray['State']) && 
				!empty($clientArray['Zip'])) {
				
				$googleLocation = $clientArray['Address'].' '.$clientArray['City'].' '.$clientArray['State'].' '.$clientArray['Zip'];	
				if (!empty($clientArray['Country'])) {
					$googleLocation .= ' '.$clientArray['Country'];
				}
				$clientArray['googleLocation'] = $googleLocation;
			} else {
				$clientArray['googleLocation'] = 0;
			}
			
			$contactArray = $this->Contacts->getContacts($this->session->userdata('accountUserid'),'C',$clientID,0);
			$clientArray['Contacts'] = $contactArray;	
			if ($renderType == 'array') {
				return $clientArray;
			} elseif ($renderType == 'json') {
				if ($return == 1) {
					return json_encode($clientArray);
				} else {
					echo json_encode($clientArray);		
				}
			}
		}
	}
}		
?>