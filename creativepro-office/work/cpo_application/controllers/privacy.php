<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Privacy extends Controller {
	function __construct()
	{
		parent::Controller();
        $this->load->model('common/public_site','',true);
	}

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = '';
        $data['pageTitle']       = 'Privacy Policy';
        $data['jsFileArray']     = array();

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
		$data = Privacy::_init();
        $this->load->view('public_site/Privacy',$data);
    }

    function popup() {
        $data = Privacy::_init();
        $this->load->view('public_site/PrivacyPopup',$data);
    }
}
?>