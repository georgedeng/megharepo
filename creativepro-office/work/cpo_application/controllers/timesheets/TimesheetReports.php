<?php
class TimesheetReports extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
	}

	function _init() {
		$data['page']              = 'timesheets';
		$data['pageTitle']         = lang('menu_view_timesheet_reports');
		$data['wysiwyg']           = 0;
		$data['map']               = 0;
		$data['jsFileArray']       = array('jsTimesheets.js');
		$data['pageIconClass']     = 'iconPageTimesheets';
		$data['pageLayoutClass']   = 'withRightColumn';

        noCache();
		return $data;
	}

	function index() {
		$data = TimesheetReports::_init();
		$this->load->view('timesheets/TimesheetReports',$data);
	}
}
?>