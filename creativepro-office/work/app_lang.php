<?php
/*
 * ENGLISH TRANSLATION
 */

/*
 * ERROR MESSAGES
 */

/*
 * STATUS INDICATORS
 */
$lang['status_not_started']     = 'Not started';
$lang['status_in_progress']     = 'In progress';
$lang['status_due_in']          = 'Due in %s %s';
$lang['status_due_on']          = 'Due on %s';
$lang['status_completed']       = 'Completed';
$lang['status_overdue']         = 'Overdue';
$lang['status_on_hold']         = 'On hold';
$lang['status_not_sent']        = 'Not sent';
$lang['status_sent']            = 'Sent';
$lang['status_paid']            = 'Paid';
$lang['status_past_due']        = 'Past due';
$lang['status_past_due_text']    = '%s %s past due';
$lang['status_past_due_text_year'] = 'More than 1 year past due';
$lang['status_not_read']        = 'Not read';
$lang['status_read']            = 'Read';
$lang['status_urgent']          = 'Urgent';
$lang['status_needs_attention'] = 'Needs attention';
$lang['status_open_sent_on']    = 'Open: sent on';
$lang['status_open_created_on'] = 'Open: created on';
$lang['status_closed_paid_on']  = 'Closed: paid on';
$lang['status_closed_not_paid'] = 'Closed: not paid';

/*
 * MAIN, SUB MENU ITEMS
 */
$lang['menu_add_dashboard_widgets'] = 'Add dashboard widgets';
$lang['menu_clients']               = 'Clients';
$lang['menu_view_clients']          = 'View clients';
$lang['menu_new_client']            = 'New client';

$lang['menu_projects']              = 'Projects';
$lang['menu_view_projects']         = 'View projects';
$lang['menu_new_project']           = 'New project';

$lang['menu_timesheets']            = 'Timesheets';
$lang['menu_view_timesheets']       = 'View timesheets';
$lang['menu_view_timesheet_reports']= 'View timesheet reports';

$lang['menu_finances']      = 'Finances';
$lang['menu_view_finances'] = 'Finance overview';
$lang['menu_new_invoice']   = 'New invoice';
$lang['menu_new_expense']   = 'New expense';

/*
 * DASHBOARD WIDGET TITLES
 */
$lang['widget_invoices']  = 'Invoices';
$lang['widget_quotes']    = 'Quotes';
$lang['widget_calendar']  = 'Calendar';
$lang['widget_rss']       = 'RSS Feeds';
$lang['widget_notes']     = 'Notes';
$lang['widget_bugs']      = 'Bugs';
$lang['widget_projects']  = 'Projects';
$lang['widget_tasks']     = 'Tasks';
$lang['widget_messages']  = 'Messages';
$lang['widget_reports']   = 'Reports';
$lang['widget_delicious'] = 'Delicious.com';
$lang['widget_today']     = 'What\'s Happening?';
$lang['widget_timer']     = 'Job Timer';

/*
 * COMMON WORDS, PHRASES AND TERMS
 */
$lang['common_form_more_link'] = 'Show more fields';
$lang['common_field_select']   = 'Select...';
$lang['common_first_name']     = 'First Name';
$lang['common_last_name']      = 'Last Name';
$lang['common_messages']       = 'Messages';
$lang['common_alerts']         = 'Alerts';
$lang['common_search']         = 'Search';
$lang['common_company_name']   = 'Company Name';
$lang['common_invoice_total']  = 'Invoice Total';
$lang['common_no_projects']    = 'Number of Projects';
$lang['common_loading_clients']= 'Loading clients...';
$lang['common_loading_map']    = 'Loading map...';
$lang['common_hours']          = 'Hours';
$lang['common_status']         = 'Status';
$lang['common_totals']         = 'Totals';
$lang['common_total']          = 'Total';
$lang['common_title']          = 'Title';
$lang['common_number']         = 'Number';
$lang['common_help']           = 'Help';
$lang['common_working']        = 'Working...';
$lang['common_loading']        = 'Loading...';
$lang['common_title']          = 'Title';
$lang['common_company']        = 'Company';
$lang['common_phone_number']   = 'Phone number';
$lang['common_welcome']        = 'Welcome';
$lang['common_search_office']  = 'Search your office';
$lang['common_send_message']   = 'Send a message';

$lang['common_work']           = 'Work';
$lang['common_mobile']         = 'Mobile';
$lang['common_fax']            = 'Fax';
$lang['common_pager']          = 'Pager';
$lang['common_home']           = 'Home';
$lang['common_skype']          = 'Skype';
$lang['common_other']          = 'Other';
$lang['common_personal']       = 'Personal';

$lang['common_aim']            = 'AIM';
$lang['common_msn']            = 'MSN';
$lang['common_icq']            = 'ICQ';
$lang['common_jabber']         = 'Jabber';
$lang['common_yahoo']          = 'Yahoo';
$lang['common_googletalk']     = 'Google Talk';

/*
 * ROLE TEXT
 */
$lang['role_loggedinas']       = 'Logged in as ';
$lang['role_administrator']    = 'Administrator';
$lang['role_employee']         = 'Employee';
$lang['role_manager']          = 'Manager';
$lang['role_contractor']       = 'Contractor';

/*
 * BUTTON TEXT
 */
$lang['button_save']    = 'Save';
$lang['button_cancel']  = 'Cancel';
$lang['button_add']     = 'New';
$lang['button_edit']    = 'Edit';
$lang['button_delete']  = 'Delete';
$lang['button_options'] = 'Options';
$lang['button_print']   = 'Print';
$lang['button_pdf']     = 'Create PDF';

/*
 * LOGIN AREA MESSAGES
 */
$lang['login_page_title']               = 'Office Login';
$lang['login_user_name']                = 'User Name';
$lang['login_password']                 = 'Password';
$lang['login_button']                   = 'Login';
$lang['login_logout']                   = 'Logout';
$lang['login_forget_password']          = 'I forgot my password.'; 
$lang['login_message_logout']           = 'You are logged out.';
$lang['login_message_inactiveAccount']  = 'Your account is not active.';
$lang['login_message_missingField']     = 'You are missing some fields.';
$lang['login_message_noAccount']        = 'You do not have an account.';
$lang['login_client_page_title']        = 'Client Login';

/*
 * DASHBOARD AREA TERMS
 */
$lang['dashboard_page_title'] = 'Office Dashboard';

/*
 * CLIENT AREA MESSAGES
 */
$lang['client_page_title']          = 'Clients';
$lang['client_client']              = 'Client';
$lang['client_add_page_title']      = 'Add client';
$lang['client_view_page_title']     = 'View clients';
$lang['client_new_client']          = 'New client';
$lang['client_edit_client']         = 'Edit client';
$lang['client_delete_client']       = 'Delete client';
$lang['client_details']             = 'Client Details';
$lang['client_form_user_name']      = 'Client user name';
$lang['client_form_company']        = 'Company name';
$lang['client_form_industry']       = 'Industry or business type';
$lang['client_form_client_since']   = 'Client since';
$lang['client_form_address']        = 'Address';
$lang['client_form_city']           = 'City';
$lang['client_form_state']          = 'State or region';
$lang['client_form_zip']            = 'Zip/Postal code';
$lang['client_form_country']        = 'Country';
$lang['client_form_timezone']       = 'Time zone';
$lang['client_form_contact_person'] = 'Primary contact person';
$lang['client_form_phone1']         = 'Primary phone number';
$lang['client_form_phone2']         = 'Phone 2';
$lang['client_form_phone3']         = 'Phone 3';
$lang['client_form_email']          = 'Email address';
$lang['client_form_url']            = 'Website';
$lang['client_form_im']             = 'Instant messenger';
$lang['client_form_notes']          = 'Notes for this client';
$lang['client_form_tags']           = 'Tags';
$lang['client_client_contacts']     = 'Client contacts';

/*
 * FINANCE AREA MESSAGES
 */
$lang['finance_page_title']         = 'Finances';
$lang['finance_view_page_title']    = 'Finance overview';
$lang['finance_view_invoices']      = 'View invoices';
$lang['finance_view_expenses']      = 'View expenses';
$lang['finance_reports']            = 'Finance reports';
$lang['finance_total_invoiced']     = 'Total invoiced';
$lang['finance_invoices']           = 'Invoices';
$lang['finance_date_sent']          = 'Date sent';
$lang['finance_date_created']       = 'Date created';

/*
 * PROJECT AREA MESSAGES
 */
$lang['project_page_title']         = 'Projects';
$lang['project_total']              = 'Total projects';
$lang['project_completion_date']    = 'Completion date';

/*
 * ADMINISTRATION AREA MESSAGES
 */
$lang['admin_page_title'] = 'Administration';

/*
 * SETTINGS AREA TERMS
 */
$lang['settings_page_title'] = 'Office Settings';

/*
 * BUG REPORT AREA TERMS
 */
$lang['bugreport_page_title'] = 'Report Bugs';
?>