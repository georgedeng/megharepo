<? $this->load->view('includes/header'); ?>

<div id="pageLeftColumn" pageID="viewCalendar">

	<script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var calendars = <?=$calendars; ?>;
	/* ]]> */
	</script>

	<div id="formEventContainer" style="display: none;">
		<form class="greyForm" id="formEvent" name="formEvent">
			<input type="hidden" name="eventID" id="eventID" />
			<input type="hidden" name="action" id="action" value="add" />
			<p>
				<label class="required" for="eventTitle"><?=lang('common_title'); ?></label>
				<input type="text" id="eventTitle" name="eventTitle" class="saverField" tabindex="1" style="width: 400px;" />
				<?=form_error('eventTitle','<span class="formError">','</span>'); ?>
			</p>
			<p>
				<label class="required" for="eventDateStart"><?=lang('common_when'); ?></label>
				<input type="text" id="eventDateStart" class="eventDates saverField" tabindex="2" style="width: 80px;" />
				<input type="text" id="eventTimeStart" class="clearOnFocus saverField" tabindex="3" style="width: 60px; display: none;" value="8:00 AM" />
				to
				<input type="text" id="eventDateEnd" class="eventDates saverField" tabindex="4" style="width: 80px;" />
				<input type="text" id="eventTimeEnd" class="clearOnFocus saverField" tabindex="5" style="width: 60px; display: none;" value="5:00 PM" />
				<input type="checkbox" id="eventAllDay" checked="checked" tabindex="6" /> <?=lang('calendar_all_day'); ?>
 				<?=form_error('projectTitle','<span class="formError">','</span>'); ?>
			</p>
			<p>
				<label><?=lang('widget_calendar'); ?></label>
				<select id="eventCalendar" style="width: 200px;" tabindex="7"></select>
			</p>
			<p>
				<label><?=lang('calendar_repeats'); ?></label>
				<select id="eventRepeats" name="eventRepeats" style="width: 200px;" tabindex="8">
					<option value="0"><?=lang('calendar_repeats_none'); ?></option>
					<option value="day"><?=lang('calendar_repeats_day'); ?></option>
					<option value="week"><?=lang('calendar_repeats_week'); ?></option>
					<option value="month"><?=lang('calendar_repeats_month'); ?></option>
					<option value="year"><?=lang('calendar_repeats_year'); ?></option>
				</select><br />
				<input type="checkbox" id="eventReminder" style="margin: 6px 0 0 265px;" tabindex="9" /> <label class="normal" for="eventReminder"><?=lang('calendar_reminder'); ?></label>
			</p>
			<p>
				<label><?=lang('common_location'); ?></label>
				<textarea id="eventLocation" name="eventLocation" tabindex="10" style="width: 400px; height: 20px;" class="expanding"></textarea>
			</p>
			<p>
				<label><?=lang('task_description'); ?></label>
				<textarea tabindex="11" name="eventDescription" id="eventDescription" style="width: 350px; height: 100px;"></textarea>
				<span id="eventDescription_f"></span>
			</p>
			<div style="float: left;">
				<p class="noBorder">
					<label><?=lang('client_form_tags'); ?></label>
					<input type="text" name="calendarTags" id="calendarTags" tabindex="12" class="saverField" style="float: left; width: 361px;" />
					<div style="float: left; margin: -6px 0 0 7px;"><button class="smallButton popUpMenuClick" id="buttonTagEdit" tabindex="13" title="<?=lang('button_edit'); ?>"><span class="edit single"></span></button></div>
					<div class="formHelpText"><span class="subText"><?=lang('help_tag_format'); ?></span></div>
				</p>
			</div>
			<div style="float: left;">
				<?=help('help_formfield_tags','clientTags','iconHelpFormOutside'); ?>
			</div>
			<div style="clear: left;"></div>
            <p></p>
            <div style="float: left;">
				<p class="noBorder">
					<label><?=lang('common_add_guests'); ?></label>
                    <div style="margin: -7px 0 0 200px;">
                        <div style="float: left; width: 262px;">
                            <strong><?=lang('menu_clients'); ?></strong><br />
                            <select id="guestClients" multiple style="width: 250px; height: 80px;" tabindex="14">
                                <?
                                foreach($clients as $client) {
                                    echo '<option value="'.$client['ClientID'].'">'.stripslashes($client['Company']).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div style="float: left;">
                            <strong><?=lang('common_contacts'); ?></strong><br />
                            <select id="guestContacts" multiple style="width: 150px; height: 80px;" tabindex="15">
                                <?
                                foreach($contacts as $contact) {
                                    if (empty($contact['NameFirst'])) {
                                        $fullName = stripslashes($contact['NameLast']);
                                    } elseif (empty($contact['NameLast'])) {
                                        $fullName = stripslashes($contact['NameFirst']);
                                    } else {
                                        $fullName = stripslashes($contact['NameLast']).', '.stripslashes($contact['NameFirst']);
                                    }
                                    echo '<option value="'.$contact['PID'].'_'.$contact['Language'].'">'.$fullName.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div style="clear: left;"></div>

                        <div id="guestClientsCollection" style="float: left; width: 262px; margin: 3px 0 0 70px;"><br /></div>
                        <div id="guestContactsCollection" style="float: left; margin-top: 3px;"><br /></div>
                        <div style="clear: left;"></div>
                    </div>
				</p>
			</div>
			<div style="float: left;">
				<?=help('help_add_guests_calendar','inviteGuests','iconHelpFormOutside'); ?>
			</div>
			<div style="clear: left;"></div>

			<div class="buttonContainer">
				<button class="buttonExpand blueGray primary" id="buttonSave" tabindex="20"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
				<button class="buttonExpand yellow secondary" id="buttonCancel" tabindex="21"><span class="buttonOuterSpan yellow"><span class="buttonDecorator cancel"><?=lang('button_cancel'); ?></span></span></button>
				<div style="clear: left;"></div>
			</div>
		</form>
	</div>
	<input type="hidden" id="monthFromURL" value="<?=$defaultMonth; ?>" />
	<input type="hidden" id="yearFromURL" value="<?=$defaultYear; ?>" />
	<div id="jMonthCalendar">&nbsp;</div>

	<div style="display: none;" id="formCalendarPopup">
		<input type="hidden" class="eventWhenPopupHidden" />
		<table class="layout">
			<tr>
				<td style="text-align: right;"><strong><?=lang('common_when'); ?>:</strong></td>
				<td>
					<strong><span class="eventWhenPopup"></span>&nbsp;at&nbsp;</strong>
					<input type="text" class="eventTimeStartPopup clearOnFocus saverField" tabindex=99" style="width: 50px;" />
				</td>
			</tr>
			<tr>
				<td style="text-align: right;"><strong><?=lang('common_title'); ?>:</strong></td>
				<td><input type="text" class="eventTitlePopup saverField" tabindex="100" style="width: 250px;" /></td>
			</tr>			
			<tr>
				<td style="text-align: right;"><strong><?=lang('widget_calendar'); ?>:</strong></td>
				<td>
					<select class="eventCalendarPopup saverField" style="width: 200px;" tabindex="101"></select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<button class="buttonExpand blueGray buttonSavePopup" tabindex="102" style="margin: 6px 12px 0 -8px;"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
					<a href="#" class="linkMoreDetails icon_calendar_edit" tabindex="103" style="display: inline-block; margin: 11px 0 0 0;"><?=lang('calendar_more_details'); ?></a>
				</td>
			</tr>
		</table>
	</div>

	<div style="display: none;" id="eventPopupContainer"></div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
