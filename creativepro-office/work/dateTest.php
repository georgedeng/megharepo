<?php
function parse_to_date_and_string($parse) {
    $exclude = array('i', 'at');
    $parse_array = explode(" ", $parse);
    $date = array();
    $data['text'] = array();
    $word_count = count($parse_array);
    for($i = 0; $i < $word_count; $i++) {
        if (strtotime($parse_array[$i]) > 0 && !in_array($parse_array[$i], $exclude)) {
            $date[] = $parse_array[$i];
        } else {
            $data['text'][] = $parse_array[$i];
        }
    }
    $data['date'] = strtotime(implode(" ", $date));
    return $data;
}

if (!empty($_POST['input'])) {
    $output = parse_to_date_and_string($_POST['input']);
}

?>
<html>
    <title></title>
    <body>
        <?
        if (!empty($output)) {
            echo date('Y-m-d H:i:s',$output['date']);
        }
        ?>
        <form action="dateTest.php" method="POST">
            <input type="text" name="input" />
            <input type="submit" value="Do it!" />
        </form>
    </body>
</html>
