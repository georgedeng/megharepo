var eventEditor;
var eventPopup;
var eventArray;
var dateRange = { startDate: null, endDate: null };

var initCalendarObj = {
	initCalendarView : function() {
		var calOptions = {
				headerHeight: 25,
				firstDayOfWeek: 0,
				onMonthChanging: function(dateStart,dateEnd) {
					return true;
				},
                onMonthChanged: function() {
                    getCalendarDataObj.getEventsForCalendarMonth(dateRange.startDate,dateRange.endDate);
					initCalendarObj.attachDatePopups();
					return true;
				},
				onDayCellDblClick: function(date) {
				},
				onEventLinkClick: function(event) {
					return true;
				},
				onEventBlockClick: function(calEvent,eventBlockObj) {
                    supportCalendarObj.renderEventPopup(calEvent,eventBlockObj);
					return false;
				},
				onEventBlockOver: function(event) {
					return true;
				},
				onEventBlockOut: function(event) {
					return true;
				},
				onDayCellClick: function(date,events) {
					supportCalendarObj.renderAgendaForToday(date,events);
					return true;
				},				
				onEventDropped: function(event,newStartDate,newEndDate) {
                    updateCalendarObj.updateEventAfterMove(event, newStartDate, newEndDate);
                    initCalendarObj.attachEventPopups();
				},
				navLinks: {
						showMore: commonTerms['more']
				},
				locale: {
						days: daysArray,
						daysShort: daysShortArray,
						daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
						months: monthArray,
						monthsShort: monthShortArray,
						weekMin: 'wk'
				}
		};
        $.jMonthCalendar.Initialize(calOptions);

		/*
		 * Look for a default month/year
		 */
		if ($('#monthFromURL').val()>0) {
			var defaultMonth = $('#monthFromURL').val();
			var defaultYear = $('#yearFromURL').val();
			var date = new Date(defaultYear,defaultMonth, 1);
			$.jMonthCalendar.ChangeMonth(date);
		}

        initCalendarObj.attachDatePopups();
        getCalendarDataObj.getEventsForCalendarMonth(dateRange.startDate,dateRange.endDate);
        		
		supportCalendarObj.renderCalendarPanel(calendars);

		/*
		 * Event form event handlers
		 */
		$('.calendarColorSwatch').click(function() {
			var colorSelected = $(this).attr('color');
			var colorOrig     = $('#calendarEditColor').val();
			$('#calendarEditColor').val(colorSelected);
            $('#calendarEditTitle').removeClass().addClass(colorSelected);
		});

		$('#linkCalendarAdd').click(function() {
			$('#panelCalendarEdit').slideDown('fast');
			$('#linkCalendarAdd').hide();
            $('#buttonDeleteCalendar').hide();
            return false;
		});

		$('#buttonSaveCalendar').click(function() {
			updateCalendarObj.saveCalendar();
            return false;
		});

		$('#buttonCancelCalendar').click(function() {
			supportCalendarObj.clearCalendarForm();
            return false;
		});

        $('#buttonDeleteCalendar').click(function() {
            var calendarID = $('#calendarEditID').val();
            var calendarTitle = $('#calendarEditTitle').val();
            $.get(siteURL+'calendar/CalendarView/getNumberOfEventsForCalendar/'+calendarID, function(payload) {
                if (payload>0) {
                    updateCalendarObj.deleteCalendarModal(calendarID,calendarTitle);
                } else {
                    updateCalendarObj.deleteCalendar(calendarID,0,true);
                }
            });
            return false;
		});

		/*
		 * Render team members
		 */
        teamMemberObj.createTeamCheckboxes('v','memberCheckbox','containerCalendarTeamMembers');

		/*
		 * Init tag field
		 */
        $("#calendarTags").autocomplete(tagJSON, {
			width: 295,
			max: 4,
            highlight: false,
            multiple: true,
            multipleSeparator: " ",
            scroll: true,
            scrollHeight: 300,
			formatItem: function(row, i, max) {
				return row.ItemName;
		    },
		    formatMatch: function(row, i, max) {
				return row.ItemName;
		    },
		    formatResult: function(row) {
				return row.ItemName;
		    }
		});

		$('#labelAgenda').html(Date.today().toString('dddd MMMM d'));
	},
	attachCalendarControls : function() {
		var changeYear  = '<select id="yearSelect">';
		var d = new Date();
		var thisYear = d.getFullYear();
		var yearStart = thisYear-10;
		var yearEnd   = thisYear+10;
		for(a=yearStart;a<=yearEnd;a++) {
			changeYear += '<option value="'+a+'"';
			if (a == currentYear) {
					changeYear += ' selected ';
			}
			changeYear += '>'+a+'</option>';
		}
		changeYear += '</select>';
		$('.YearSelect').append(changeYear);

		var changeMonth  = '<select id="monthSelect">';
		for(var a=0;a<=11;a++) {
			changeMonth += '<option value="'+a+'"';
			if (currentMonthNum == a) {
					changeMonth += ' selected ';
			}
			changeMonth += '>'+monthArray[a]+'</option>';
		}
		changeMonth += '</select>';
		$('.MonthSelect').append(changeMonth);

		/*
		 * Attach event handlers to month/year selects
		 */
		$('#monthSelect').change(function() {
			var date = new Date(currentYear, $(this).val(), 1);
			$.jMonthCalendar.ChangeMonth(date);
		});
		$('#yearSelect').change(function() {
			var date = new Date($(this).val(),currentMonthNum, 1);
			$.jMonthCalendar.ChangeMonth(date);
		});
	},
	attachDatePopups : function() {
		// For a larger 'hit' area use .DateLabel
		$('.DateNumber').each(function() {
			//var dateMySQL = $(this).parent().attr('dateMySQL');
            var dateMySQL = $(this).parent().parent().attr('dateMySQL');
			var tipContent = $('#formCalendarPopup').html();		

			$(this).qtip({
				content:    {
					text:      tipContent
				},
				position: {
					corner: {
						   target:  'topRight',
						   tooltip: 'bottomLeft'
					   }
				},
				show: {
				   when: 'click',
				   delay: 0,
				   solo: true
				},
				hide: {
				   when: {
					   event: 'unfocus'
				   }
				},
				style: {
				   border: {
					   width: 5
				   },
				   tip: {
					   corner: 'bottomLeft'
				   },
				   name: 'cream',
				   width: 350,
				   padding: 6
				},
				api: {
					onRender: function() {
						initCalendarObj.initPopupFormControls(this,dateMySQL);
					},
                    onHide: function() {
                        supportCalendarObj.clearEventForm(true);
                    }
				}
		   });

		   $(this).click(function() {
				return false;
			});
		});
	},
	attachEventPopups : function() {
		$('.Event').each(function() {
			var dateMySQL = $(this).parent().attr('dateMySQL');
			var tipContent = $('#eventPopupContainer').html();
			var eventBlock = $(this);

			$(this).qtip({
				content:    {
					text:      tipContent
				},
				position: {
					corner: {
						   target:  'topMiddle',
						   tooltip: 'bottomMiddle'
					   }
				},
				show: {
				   when: { event: false },
				   delay: 0,
				   solo: true
				},
				hide: {
				   when: {  event: 'unfocus'  }
				},
				style: {
				   border: {
					   width: 5
				   },
				   tip: {
					   corner: 'bottomMiddle'
				   },
				   name: 'cream',
				   width: 350,
				   padding: 6
				},
				api: {
					onHide: function() {
						
					}
				}
		   });

		   $(this).click(function() {
				return false;
			});
		});
	},
	initPopupFormControls : function(popupObj,dateMySQL) {
		var dateParsed = Date.parse(dateMySQL);
		var dateHuman = dateParsed.toString('dddd MMMM d');
		var dateJS    = dateParsed.toString('MM/dd/yyyy');
		$('.eventWhenPopup').html(dateHuman);
		$('.eventWhenPopupHidden').val(dateMySQL);
		$('.eventTimeStartPopup').val('8:00 AM');

		$('.eventTimeStartPopup').focus(function() {
			$(this).val('');
		});
        
		$('.buttonSavePopup').unbind('click').click(function() {
			updateCalendarObj.saveEvent('popup',$(this));
			popupObj.hide();
			return false;
		});

		$('.saverField').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
				updateCalendarObj.saveEvent('popup',$(this));
				popupObj.hide();
                return false;
            }
        });

		supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('.eventCalendarPopup'),false);

		$('.linkMoreDetails').unbind('click').click(function() {
			$('#formEventContainer').slideDown('fast');
			$('#jMonthCalendar').hide();

			var eventTitle = $(this).parents('table').find('.eventTitlePopup').val();
			var eventCalendar = $(this).parents('table').find('.eventCalendarPopup').val();
			var eventTime     = $(this).parents('table').find('.eventTimeStartPopup').val();

			$('#eventTitle').val(eventTitle);
			$('#eventDateStart').val(dateJS);
			$('#eventCalendar').val(eventCalendar);
			$('#eventTimeStart').val(eventTime);

			eventEditor.resizeTo(405,100);
			popupObj.hide();
			return false;
		});
	},
	initFormControls : function() {
		/*
		 * Init date pickers
		 */
		$(".eventDates").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

		eventEditor = new punymce.Editor({
            id : 'eventDescription',
            toolbar : 'bold,italic,underline,strike,increasefontsize,decreasefontsize,ul,ol,indent,outdent,left,center,right',
            plugins : 'Paste'
        });

		$('textarea.expanding').elastic();

		$('#eventAllDay').click(function() {
			if ($(this).attr('checked') == true) {
				$('#eventTimeStart').hide();
				$('#eventTimeEnd').hide();
			} else {
				$('#eventTimeStart').show();
				$('#eventTimeEnd').show();
			}
		});

		$('#buttonSave').click(function() {
			updateCalendarObj.saveEvent();
			return false;
		});

		$('#buttonCancel').click(function() {
			supportCalendarObj.clearEventForm();
			return false;
		});

		$('#buttonadd').click(function() {
			$('#formEventContainer').slideDown('fast');
			$('#jMonthCalendar').hide();
			
			eventEditor.resizeTo(405,100);
			return false;
		});

		$('.saverField').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                updateCalendarObj.saveEvent();
                return false;
            }
        });

		supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('#eventCalendar'));

        $('#guestClients').change(function() {
            var text = $(this).find(':selected').text();
            var id   = $(this).val();

			/*
			 * Is this id already in the list?
			 */
			var exists = false;
			$('#guestClientsCollection span').each(function() {
				if ($(this).attr('id') == 'client_'+id) {
					exists = true;
				}
			});

			if (exists == false) {
                supportCalendarObj.renderGuestItem($('#guestClientsCollection'),'client_'+id,text);
			}

            $(this).val('');
        });

        $('#guestContacts').change(function() {
            var text = $(this).find(':selected').text();
            var id   = $(this).val();

			/*
			 * Is this id already in the list?
			 */
			var exists = false;
			$('#guestContactsCollection span').each(function() {
				if ($(this).attr('id') == 'contact_'+id) {
					exists = true;
				}
			});

			if (exists == false) {
                supportCalendarObj.renderGuestItem($('#guestContactsCollection'),'contact_'+id,text);
			}

            $(this).val('');
        });

        $('#buttonTagEdit').makeCategoryEditBox({
			listJSON: tagJSON,
			listType: 'tag',
			instanceName: 'tags',
			siteArea: 'calendar'
		});
	}
}

var getCalendarDataObj = {
	getEventsForCalendarMonth : function(dateStart,dateEnd) {
        $.ajax({
            type: "POST",
            url:  siteURL+'calendar/CalendarView/getEvents/'+dateStart+'/'+dateEnd+'/month',
            dataType: 'json',
            success: function(payload) {
                eventArray = payload;
                $.jMonthCalendar.ReplaceEventCollection(eventArray);
				initCalendarObj.attachEventPopups();
				var today = Date.today().toString('yyyy-MM-dd');
				var events = $.jMonthCalendar.GetEventsForDate(today);
				supportCalendarObj.renderAgendaForToday(today,events);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
}

var updateCalendarObj = {
	saveEvent : function(formInterface,popupObj) {
		if (formInterface == 'popup' || formInterface == 'widget') {
			if (formInterface == 'popup') {
				var eventWhen     = popupObj.parents('div').find('.eventWhenPopupHidden').val();
				var eventTime     = popupObj.parents('table').find('.eventTimeStartPopup').val()
                var eventTitle    = popupObj.parents('table').find('.eventTitlePopup').val();
				var eventCalendar = popupObj.parents('table').find('.eventCalendarPopup').val();
				var eventDescription = '';
			} else if (formInterface == 'widget') {
				var eventWhen        = $('#eventDateStartWidget').val();
				var eventTime        = $('#eventTimeStartWidget').val()
				var eventTitle       = $('#eventTitleWidget').val();
				var eventCalendar    = $('#eventCalendarWidget').val();
				var eventDescription = $('#eventDescriptionWidget').val();
			}
            eventTime = Date.parse(eventTime);

			if (eventTime != null) {
				eventTime = eventTime.toString('hh:mm tt');
			} else {
				eventTime = '';
			}			

			var formHash = {
					eventForm        : 'popup',
					eventDateStart   : eventWhen,
                    eventTimeStart   : eventTime,
                    eventTitle       : eventTitle,
					eventCalendar    : eventCalendar,
					eventDescription : eventDescription
			}
			$.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/saveEvent',
				data: formHash,
				dataType: 'json',
				success: function(payload) {
					if (formInterface == 'popup') {
						eventArray.push(payload);
						$.jMonthCalendar.ReplaceEventCollection(eventArray);
						initCalendarObj.attachEventPopups();
						supportCalendarObj.clearEventForm(true);
					} else if (formInterface == 'widget') {
                        var startDateTime = Date.parse(payload.StartDateTime);
                        var startDate = startDateTime.toString('MMM d');
                        var startTime = startDateTime.toString('h:mm');
                        var dateString = startDate;

                        var startDateHour = startDateTime.getHours();
                        var startDateMin  = startDateTime.getMinutes();

                        if (startTime != '0:00' && startDateHour != 0) {
                            dateString += ', '+startDateTime.toString('h:mm tt');
                        }

						var date = payload.StartDateTime;
						date=date.replace(/0(\d\/)/g,'$1');
						var dateArray = date.split('/');
						var day = dateArray[1];

						var eventDetailString  = '<p><strong>'+payload.Title+'</strong> in '+payload.CalendarTitle+'<br />'+dateString+'</p>';
						var eventHTML = '<div class="EventWidget colorBlock '+payload.CssClass+'" style="margin: 1px ;">'+eventDetailString+'</div>';

						$('#dayNum_'+day).parents('td').append(eventHTML);
						widgetToolsObj.attachEventPopups();
                        widgetToolsObj.clearEventForm();
					} else {
                        eventArray.push(payload);
						$.jMonthCalendar.ReplaceEventCollection(eventArray);
						initCalendarObj.attachEventPopups();
						supportCalendarObj.clearEventForm(false);
                    }
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
		} else {
			var eventAllDay = 0;
			var eventTimeStart = Date.parse($('#eventTimeStart').val());
			var eventTimeEnd   = Date.parse($('#eventTimeEnd').val());
			if ($('#eventAllDay').attr('checked') == true) {
				eventAllDay = 1;
				eventTimeStart = '';
				eventTimeEnd   = '';
			}

			if (eventTimeStart != '') {
                eventTimeStart = eventTimeStart.toString('hh:mm tt');
			}
			if (eventTimeEnd != '' && eventTimeEnd != null) {
				eventTimeEnd = eventTimeEnd.toString('hh:mm tt');
			}

			var clients = '';
			$('#guestClientsCollection span').each(function() {
				var id = $(this).attr('id');
				var idArray = id.split('_');
				clients += idArray[1]+'|';
			});
			var contacts = '';
			$('#guestContactsCollection span').each(function() {
				var id = $(this).attr('id');
				var idArray = id.split('_');
				contacts += idArray[1]+','+idArray[2]+'|';
			});

			var eventDescription = eventEditor.getContent();

            var eventDateStart = Date.parse($('#eventDateStart').val());
            eventDateStart = eventDateStart.toString('yyyy-MM-dd');
            var eventDateEnd = Date.parse($('#eventDateEnd').val());
            if (eventDateEnd != null) {
                eventDateEnd = eventDateEnd.toString('yyyy-MM-dd');
            } else {
                eventDateEnd = '';
            }
			var formHash = {
					action          : $('#action').val(),
					eventForm       : '',
					eventID         : $('#eventID').val(),
					eventTitle      : $('#eventTitle').val(),
					eventDateStart  : eventDateStart,
                    eventTimeStart  : eventTimeStart,
					eventDateEnd    : eventDateEnd,
                    eventTimeEnd    : eventTimeEnd,
					eventAllDay     : eventAllDay,
					eventCalendar   : $('#eventCalendar').val(),
					eventRepeats    : $('#eventRepeats').val(),
					eventReminder   : $('#eventReminder').val(),
					eventLocation   : $('#eventLocation').val(),
					eventDescription: eventDescription,
					eventTags       : $('#calendarTags').val(),
					guestClients    : clients,
					guestContacts   : contacts

			}
			$.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/saveEvent',
				data: formHash,
				dataType: 'json',
				success: function(payload) {
                    supportCalendarObj.clearEventForm();
                    eventArray.push(payload);
					$.jMonthCalendar.ReplaceEventCollection(eventArray);
                    initCalendarObj.attachEventPopups();                    
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
		}
	},
	saveCalendar : function(order) {
		var calendarID   = $('#calendarEditID').val();
		var calendarName = $('#calendarEditTitle').val();
		var color        = $('#calendarEditColor').val();
        var origColor    = $('#calendarOriginalColor').val();
		var memberString = '';

		$('.memberCheckbox').each(function() {
			if ($(this).attr('checked') == true) {
				memberString += $(this).val()+','+$(this).attr('language')+'|';
			}
		});

		if (calendarName != '') {
			var formHash = {
                    calendarID   : calendarID,
                    calendarName : calendarName,
					color        : color,
					memberString : memberString
			}
			$.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/saveCalendar',
				data: formHash,
				dataType: 'json',
				success: function(payload) {
                    var edit = 0;
                    for(var a=0;a<=(calendars.length-1);a++) {
                        if (calendars[a].CalendarID == payload.CalendarID) {
                            calendars[a] = payload;
                            edit = 1;
                        }
                    }
                    if (edit == 0) {
                        calendars.push(payload);
                    }
					supportCalendarObj.renderCalendarPanel(calendars);

                    /*
                     * Update event colors for this calendar
                     */
                    $('.Event').each(function() {
                       if ($(this).attr('calendarid') == calendarID) {
                           $(this).removeClass(origColor).addClass(color);
                           $(this).children('.eventContainer').removeClass(origColor).addClass(color);
                       }
                    });

                    $('#containerAgenda span').each(function() {
                       if ($(this).attr('calendarid') == calendarID) {
                           $(this).removeClass(origColor).addClass(color);
                       }
                    });
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
		}
        alert('hey');
		supportCalendarObj.clearCalendarForm();
	},
    deleteCalendar : function(deleteCalendarID,moveToCalendarID,deleteEvents) {
        if (deleteEvents == true) {
            updateTrashObj.sendToTrash('calendarWithEvents',deleteCalendarID);
            /*
             * Remove events from page
             */
            $('.Event').each(function(){
               if ($(this).attr('calendarid') == deleteCalendarID) {
                   $(this).fadeOut('fast');
                   var eventID = $(this).attr('eventid');
                   eventArray.splice(eventArray.indexOf(eventID),1);
               }
            });

            $('.agendaItem').each(function() {
                if ($(this).attr('calendarID') == deleteCalendarID) {
                   $(this).parent().fadeOut('fast');
                }
            });
        } else {
            $.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/moveEventsToCalendar/'+deleteCalendarID+'/'+moveToCalendarID,
				success: function(payload) {
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
            updateTrashObj.sendToTrash('calendar',deleteCalendarID);
        }
        /*
         * Remove calendar from calendars array
         */
        calendars.splice(calendars.indexOf(deleteCalendarID), 1);
        $('#modalDelete').dialog('close');

        $('.linkCalendarColorBlock').each(function() {
           if ($(this).attr('calid') == deleteCalendarID) {
               $(this).fadeOut('fast');
               $(this).next().fadeOut('fast');
           }
        });

        supportCalendarObj.clearCalendarForm();
    },
    deleteCalendarModal : function(calendarID,calendarTitle) {
        var modalContents = [];
        modalContents.push('<div class="errorMessageBig">'+commonTerms['deleteCalendar2']+': '+calendarTitle+'</div>');
        modalContents.push('<p>'+commonTerms['deleteCalendar']+'</p>');
        modalContents.push('<p><select id="selectMoveToCalendar" style="margin: 3px 0 3px 0; width: 300px;"></select></p>');
        var outString = modalContents.join('');
        $('#deleteContentsContainer').html(outString);
        supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('#selectMoveToCalendar'),true);

        $('#selectMoveToCalendar option').each(function() {
            if ($(this).val() == calendarID) {
                $(this).remove();
            }
        });

        $('#buttonDeleteModalDelete').click(function(){
            var moveToCalendarID = $('#selectMoveToCalendar').val();
            var deleteEvents = 0;
            if (moveToCalendarID == 0) {
                deleteEvents = 1;
            }
            updateCalendarObj.deleteCalendar(calendarID,moveToCalendarID,deleteEvents);
        });
        $('#modalDelete').dialog('open');
    },
    updateEventAfterMove : function(event,newStartDate,newEndDate) {
        var eventType = event.Type;
        var eventID   = event.EventID;
        if (eventType == 'Event') {
            $.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/updateEventDatesAfterMove/'+eventID+'/'+newStartDate+'/'+newEndDate,
				dataType: 'json',
				success: function(payload) {
                    for(var a in eventArray) {
                        if (eventArray[a].EventID == eventID) {
                            event.StartDateTime = $.jMonthCalendar.getJSONDate(payload[0].NewDateStart);
                            if (payload[0].NewDateEnd == '0000-00-00 00:00:00') {
                                event.EndDateTime = event.StartDateTime.clone();
                            } else {
                                event.EndDateTime   = $.jMonthCalendar.getJSONDate(payload[0].NewDateEnd);
                            }
                        }
                    }
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
        } else if (eventType == 'Task') {
            $.ajax({
				type: "POST",
				url:  siteURL+'tasks/TaskUpdate/updateTaskDatesAfterMove/'+eventID+'/'+newStartDate+'/'+newEndDate,
				dataType: 'json',
				success: function(payload) {
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
        } else if (eventType == 'Project') {

        }
    }
}

var supportCalendarObj = {
	clearEventForm : function(popup) {
        if (popup == true) {
            $('.eventTimeStartPopup').val('');
            $('.eventTitlePopup').val('');
            $('.eventCalendarPopup').val('');
        } else {
            $('#formEventContainer').slideUp('fast');
            $('#jMonthCalendar').fadeIn('fast');
            $('#eventTitle').val('');
            $('#eventDateStart').val('');
            $('#eventTimeStart').val('');
            $('#eventDateEnd').val('');
            $('#eventTimeEnd').val('');
            $('#eventAllDay').attr('checked',true);
            $('#eventCalendar').val('');
            $('#eventRepeats').val('');
            $('#eventReminder').attr('checked',false);
            $('#eventLocation').val('');
            $('#calendarTags').val('');
            eventEditor.setContent('');
            $('#guestClientsCollection').html('');
            $('#guestContactsCollection').html('');
        }
	},
	clearCalendarForm : function() {
		var color = $('#calendarEditColor').val();
		$('#panelCalendarEdit').slideUp('fast', function() {
			$('#linkCalendarAdd').show();
		});
		$('#calendarEditColor').val('');
        $('#calendarOriginalColor').val('');
		$('#calendarEditTitle').removeClass(color).val('');
        $('#calendarEditID').val('');
        alert('clear');
		$('#teamMemberContainer').html('');
	},
	populateCalendarSelectFromJSON : function(calendarArray,selectObj,deleteCalendar) {
        if (deleteCalendar == true) {
            //selectObj.html('<option value="0">'+commonTerms['deleteEventsToo']+'</option><option value="0"></option>');
        }
        for(var a=0;a<=(calendarArray.length-1);a++) {
			var jsonObject = calendarArray[a];
			var calendarID = jsonObject.CalendarID;
			var calendarTitle = jsonObject.Title;
			
			var option = '<option value="'+calendarID+'">'+calendarTitle+'</option>';
			selectObj.append(option);
		}
	},
	renderCalendarPanel : function(calendarArray) {
		$('#containerYourCalendars').html('');
		for(var a=0;a<=(calendarArray.length-1);a++) {
			var jsonObject = calendarArray[a];
			var calendarID = jsonObject.CalendarID;
			var calendarTitle = jsonObject.Title;
			var calendarColor = jsonObject.Color;
			var memberUIDs    = jsonObject.MemberUIDs;
            var deletable     = jsonObject.Deletable;
			var data = a+'_'+calendarID+'_'+calendarColor+'_'+memberUIDs+'_'+deletable;

			var calendar = '<p class="colorBlock '+calendarColor+' linkCalendarColorBlock" style="cursor: pointer;" calID="'+calendarID+'"></p><a href="#" class="linkCalendarText"  data="'+data+'" style="display: inline-block;">'+calendarTitle+'</a>';
			calendar += '<p></p>';
			$('#containerYourCalendars').append(calendar);
		}

		$('.linkCalendarText').click(function() {
            $('.memberCheckbox').each(function() {
                $(this).attr('checked',false);
            });

			var data = $(this).attr('data');
			var dataArray = data.split('_');
			var count     = dataArray[0];
			var calID     = dataArray[1];
			var color     = dataArray[2];
			var members   = dataArray[3];
            var deletable = dataArray[4];
			var memberArray = members.split('|');
			var title   = $(this).html();

			$('#panelCalendarEdit').slideDown('fast');
			$('#linkCalendarAdd').hide();

			$('#calendarEditID').val(calID);
			$('#calendarEditColor').val(color);
            $('#calendarOriginalColor').val(color);
			$('#calendarEditTitle')
                //.removeClass($('#calendarEditColor').val(color))
                .removeClass()
                .addClass(color)
                .val(title);

			for(var a=0;a<=(memberArray.length-1);a++) {
				var memberID = memberArray[a];

				$('.memberCheckbox').each(function() {
                    if ($(this).val() == memberID) {
						$(this).attr('checked',true);
					}
				});
			}

            /*
             * Show delete button if deletable = 1
             */
            if (deletable == 1) {
                $('#buttonDeleteCalendar').show();
            } else {
                $('#buttonDeleteCalendar').hide();
            }

			return false;
		});

		$('.linkCalendarColorBlock').click(function() {
			var calID = $(this).attr('calID');

			if ($(this).hasClass('off')) {
				$(this).removeClass('off');
				supportCalendarObj.toggleCalendar(calID,'on');
			} else {
				$(this).addClass('off');
				supportCalendarObj.toggleCalendar(calID,'off');
			}
			return false;
		});
	},
	toggleCalendar : function(calendarID,state) {
		if (state == 'off') {
			$('.Event').each(function() {
				if ($(this).attr('calendarID') == calendarID) {
					$(this).fadeOut('fast');
				}
			});
		} else {
			$('.Event').each(function() {
				if ($(this).attr('calendarID') == calendarID) {
					$(this).fadeIn('fast');
				}
			});
		}
	},
	renderAgendaForToday : function(agendaDate,events) {
        var humanDate = Date.parse(agendaDate);
		$('#labelAgenda').html(humanDate.toString('dddd MMMM d'));

        /*
         * Render event list
         */
        var outArray = [];
        if (events.length>0) {
			outArray.push('<ul class="listLined">');
            for(var a in events) {
                var startDateTime = events[a].StartDateTime;
                var startDateHour = startDateTime.getHours();
                var startDateMin  = startDateTime.getMinutes();

                var startTime = startDateTime.toString('h:mm tt');
                startTime = startTime.trim();

                if (startDateHour == 0 && startDateMin == 0) {
                    startTime = '';
                } else {
                    if (startTime == '0:00 AM' || startTime == '0:00 PM' || startTime == '0:00') {
                        startTime = '';
                    }
                }
                outArray.push('<li><span class="colorText '+events[a].CssClass+' agendaItem" eventID="'+events[a].EventID+'" calendarID="'+events[a].CalendarID+'" style="font-weight: bold;">'+events[a].Title+'</span> <span class="subText">'+startTime+'</span></li>');
            }
			outArray.push('</ul>');
            var outString = outArray.join('');
            $('#containerAgenda').html(outString);
        } else {
            $('#containerAgenda').html(commonTerms['no_events_today']);
        }
	},
    renderEventPopup : function(eventObj,eventBlockObj) {
        var outString = [];

		var startDate = eventObj.StartDateTime.toString('MMM d');
		var endDate   = eventObj.EndDateTime.toString('MMM d');
		var startTime = eventObj.StartDateTime.toString('h:mm');
		var dateString = startDate;

        var startDateHour = eventObj.StartDateTime.getHours();
        var startDateMin  = eventObj.StartDateTime.getMinutes();

        var endDateHour = eventObj.EndDateTime.getHours();
        var endDateMin  = eventObj.EndDateTime.getMinutes();

		if (startTime != '0:00' && startDateHour != 0) {
			dateString += ', '+eventObj.StartDateTime.toString('h:mm tt');
		}
        if (endDate != '' && endDate != startDate) {
			var endTime = eventObj.EndDateTime.toString('h:mm');
			if (endTime != '0:00' && endDateHour != 0 && endDateMin != 0) {
				endDate += ', '+eventObj.EndDateTime.toString('h:mm tt');
			}
			dateString += ' to '+endDate;
		}
		//outString.push('<p style="background: #ccc; width: 100%;">'+dateString+'</p>');
		var eventTitle = '';
		if (eventObj.Type == 'Task') {
			eventTitle = '<a href="'+siteURL+'task/TaskViewExternal">'+eventObj.Title+'</a>';
		} else {
			eventTitle = eventObj.Title;
		}
        outString.push('<p style="margin-bottom: 6px;"><strong>'+eventTitle+'</strong> ('+dateString+')</p>');
		outString.push('<p>'+eventObj.Description+'</p>');			
        
		if (eventObj.Editable == 1) {
			outString.push('<button class="smallButton buttonEditEvent" eventID="'+eventObj.EventID+'" style="margin: 6px 12px 0 0; float: left;"><span class="edit single"></span></button>');
		}
        if (eventObj.Deletable == 1) {
            outString.push('<button class="smallButton buttonDeleteEvent" eventID="'+eventObj.EventID+'" style="margin: 6px 12px 0 0; float: left;"><span class="delete single"></span></button>');
        }

        var renderString = outString.join('');

        var popupAPI = eventBlockObj.qtip('api');
        popupAPI.updateContent(renderString);
        popupAPI.show();

		$('.buttonEditEvent').click(function() {
			/*
			 * Populate event form from object
			 */
			$('#eventID').val(eventObj.EventID);
			$('#action').val('edit');
			$('#eventTitle').val(eventObj.Title);

			$('#eventTimeStart').show();
			$('#eventTimeEnd').show();
			$('#eventAllDay').attr('checked',false);
			
			var startDate = eventObj.StartDateTime.toString("M/d/yyyy");
			var startTime = eventObj.StartDateTime.toString("h:mm tt");
			var endDate   = eventObj.EndDateTime.toString("M/d/yyyy");
			var endTime   = eventObj.EndDateTime.toString("h:mm tt");

			$('#eventDateStart').val(startDate);
			$('#eventTimeStart').val(startTime);
			if (endDate != ''  && endDate != startDate) {
				/*
				 * This is a multi-day event, populate end date
				 */
				$('#eventDateEnd').val(endDate);
				$('#eventTimeEnd').val(endTime);
			} else {
				$('#eventTimeEnd').val('');
			}
			$('#eventCalendar').val(eventObj.CalendarID);
			$('#eventRepeats').val(eventObj.Recurring);
			$('#eventReminder').attr('checked',false);
			if (eventObj.EmailReminder == 1) {
				$('#eventReminder').attr('checked',true);
			}
			$('#eventLocation').val(eventObj.Location);
			$('#calendarTags').val(eventObj.Tags);
			eventEditor.setContent(eventObj.Description);

			popupAPI.hide();
			$('#formEventContainer').slideDown('fast');
			$('#jMonthCalendar').hide();
			eventEditor.resizeTo(405,100);

            /*
             * Get any guests for this event
             */
            $.getJSON(siteURL+'calendar/CalendarView/getEventGuests/'+eventObj.EventID+'/json/0', function(payload) {
                $.each(payload, function(i,item){
                    var itemID = payload[i].ItemID;
					var language = payload[i].Language;
                    var itemText,itemHTML;
                    if (payload[i].ItemType == 'C') {
                        itemText = payload[i].Company;
                        supportCalendarObj.renderGuestItem($('#guestClientsCollection'),'client_'+itemID,itemText);
                    } else if (payload[i].ItemType == 'P') {
                        itemText = payload[i].NameFirst+' '+payload[i].NameLast;
                        supportCalendarObj.renderGuestItem($('#guestContactsCollection'),'contact_'+itemID+'_'+language,itemText);
                    }

                });
            });
			return false;
		});

        $('.buttonDeleteEvent').click(function() {
			updateTrashObj.sendToTrash('event',eventObj.EventID);
            eventBlockObj.fadeOut('fast');
            popupAPI.hide();

            /*
             * Remove event from agenda
             */
            $('.agendaItem').each(function() {
               if ($(this).attr('eventID') == eventObj.EventID) {
                   $(this).parent().fadeOut('fast');
               }
            });
            /*
             * Remove event from eventArray
             */
            supportCalendarObj.removeEventFromArray(eventObj.EventID);
            $.jMonthCalendar.ReplaceEventCollection(eventArray);
            initCalendarObj.attachEventPopups();
        });

        $('#eventPopupContainer').html(outString);
    },
    removeEventFromArray : function(eventID) {
        for(var a in eventArray) {
            if (eventArray[a].EventID == eventID) {
                eventArray[a] = '';
            }
        }
    },
    renderGuestItem : function(containerObj,id,text) {
        var itemHTML = '<div><span class="buttonDeleteSmall removeClient" id="'+id+'"></span>'+text+'</div>';
		containerObj.append(itemHTML);

        $('#'+id).click(function() {
            $(this).parent().remove();
        });
    }
}

$(document).ready(function() {
	if ($('#pageLeftColumn').attr('pageID') == 'viewCalendar') {
		initCalendarObj.initCalendarView();
		initCalendarObj.initFormControls();
		
		$('#buttonprint').unbind('click').click(function() {
			var calendarIDString = '';
			$('#containerYourCalendars > p.colorBlock:not(.off)').each(function(){
				calendarIDString += $(this).attr('calid')+'-';
			});
            var yearPrint  = $('#yearSelect').val();
            var monthPrint = parseInt($('#monthSelect').val());
            monthPrint = monthPrint+1;
			var itemIDString = calendarIDString+'--'+yearPrint+'---'+monthPrint;
			printObj.goPrint(itemIDString,'Calendar');
			return false;
		});
	}
});
