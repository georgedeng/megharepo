<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function cpo_clients($clientArray) {
    $CI =& get_instance();
    include(SITE_CODE_PATH);
    $CI->load->model('clients/client_update','',true);
    $CI->load->model('common/app_data','',true);
    $CI->load->model('common/user_login','',true);
    $CI->load->model('contacts/contacts','Contacts',true);

    $countClients = 0;
    foreach($clientArray as $client) {
        /*
         * Get new categoryID for client
         */
        $newCatIDArray = $CI->app_data->getNewCatIDFromOld($client->Main_Cat,$CI->session->userdata('accountUserid'));
        $newCatID = $newCatIDArray['CatID'];

        if (!empty($client->Address2)) {
            $address = $client->Address1.'<br />'.$client->Address2;
        } else {
            $address = $client->Address1;
        }

        $data['userid']         = $CI->session->userdata('accountUserid');
        $data['action']         = 'add';
        $data['clientPrevID']   = $client->ID;
        $data['clientCategory'] = $newCatID;
        $data['clientCompany']  = fieldToDB($client->Company);
        $data['clientAddress']  = $address;
        $data['clientCity']     = $client->City;
        $data['clientState']    = $client->State;
        $data['clientZip']      = $client->Zip;
        $data['clientCountry']  = $client->Country;
        $data['clientPhone1']   = $client->Phone1;
        $data['clientPhone2']   = $client->Phone2;
        $data['clientPhone3']   = $client->Phone3;
        $data['clientEmail']    = $client->Email;
        $data['clientURL']      = $client->URL;
        $data['clientNotes']    = fieldToDB($client->Comments);
        $data['clientArchive']  = $client->Archive;
        $data['clientStatus']   = $client->Status;
        $data['clientIndustry'] = $client->Industry;
        $data['clientSince']    = $client->Client_Since;
        $data['clientTimezone'] = convertTimezoneFormat($client->GMTOffset);
        $data['clientLanguage'] = $CI->session->userdata('language');
        $data['clientTags']     = $client->Tags;

        $clientID = $CI->client_update->saveClient($data);

        /*
         * Get our ClientUserid from cpo_login
         * There are NOT as many client entries in the cpo_login table as there are clients (version 1)
         * Se we could end up with no results from the following query.
         * In which case we will need to create an entry in cpo_login for this client.
         */
        $loginArray = $CI->user_login->getLoginInformation(NULL,$CI->session->userdata('accountUserid'),$client->Client_Userid);
        if ($loginArray != FALSE) {
            $data['clientUserid'] = $loginArray[0]['UID'];
        } else {
            /*
             * Enter new entry for client in cpo_login here...
             */            
            $loginArray['action']        = 'add';
            $loginArray['Userid']        = $data['clientEmail'];
            if (empty($data['clientEmail'])) {
                $loginArray['Userid'] = 'client';
            }
            $password = random_string();
            $loginArray['Password']      = sha1($password.$siteCode);
            $loginArray['RoleID']        = '';
            $loginArray['UserType']      = USER_TYPE_CLIENT;
            $loginArray['UseridAccount'] = $CI->session->userdata('accountUserid');

            $loginUID = $CI->user_login->saveLoginInformation($loginArray);

            /*
             * Create authString and update
             */
            $authString              = sha1($loginUID.$siteCode);
            $authArray['UID']        = $loginUID;
            $authArray['action']     = 'updateAuthString';
            $authArray['authString'] = $authString;
            $CI->user_login->saveLoginInformation($authArray);
            
            $data['clientUserid'] = $loginUID;
        }
        $data['clientID']     = $clientID;
        $data['action']       = 'update';
        $CI->client_update->saveClient($data);
        
        /*
         * Prep client contact data for cpo_people
         */
         if (!empty($client->Contact_First_Name) || !empty($client->Contact_Last_Name)) {
             $data['action']        = 'add';
             $data['accountUserid'] = $CI->session->userdata('accountUserid');
             $data['loginUID']      = '';
             $data['itemID']        = $clientID;
             $data['itemType']      = 'C';
             $data['nameFirst']     = fieldToDB($client->Contact_First_Name);
             $data['nameLast']      = fieldToDB($client->Contact_Last_Name);
             $data['title']         = '';
             $data['company']       = fieldToDB($client->Company);
             $data['address']       = $address;
             $data['city']          = $client->City;
             $data['state']         = $client->State;
             $data['zip']           = $client->Zip;
             $data['country']       = $client->Country;
             $data['timezone']      = convertTimezoneFormat($client->GMTOffset);
             $data['language']      = $CI->session->userdata('language');
             $data['avatarFilename']= '';
             $data['teamMember']    = 0;
             $data['vendor']        = 0;
             $data['peopleTags']    = '';
             $data['phone']['data'][0] = $client->Phone1;
             $data['phone']['type'][0] = 'work';
             $data['email']['data'][0] = $client->Email;
             $data['email']['type'][0] = 'work';
             $data['url']['data'][0]   = $client->URL;
             $data['url']['type'][0]   = 'work';
             $data['im']['data'][0]    = '';
             $data['im']['type'][0]    = '';

             $pID = $CI->Contacts->saveContactPerson($data);
         }

         $countClients++;
    }

    return $countClients;
}

function cpo_login($loginArray) {
    $CI =& get_instance();
    $CI->load->model('users/user_login','',true);
    include(SITE_CODE_PATH);

    foreach($loginArray as $login) {
        $data['action']        = 'add';
        $data['PrevUID']       = $login->UID;
        $data['Userid']        = $login->Userid;
        $data['UseridAccount'] = $CI->session->userdata('accountUserid');
        $data['Password']      = $login->Password;
        $data['UserType']      = $login->User_Type;
        $data['RoleID']        = '';

        $loginUID = $CI->User_login->saveLoginInformation($data);

        /*
         * Create authString and update
         */
        $authString              = sha1($loginUID.$siteCode);
        $authArray['UID']        = $loginUID;
        $authArray['action']     = 'updateAuthString';
        $authArray['authString'] = $authString;
        $CI->User_login->saveLoginInformation($authArray);
    }
}

function cpo_main_cat($categoryArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);

    foreach($categoryArray as $category) {
        $accountUserid = $CI->session->userdata('accountUserid');

        switch ($category->Module) {
            case 'Projects':
                $siteArea = 'project';
                break;
            case 'Expense':
                $siteArea = 'expense';
                break;
            case 'Clients':
                $siteArea = 'client';
                break;
            case 'Invoices':
                $siteArea = 'invoice';
                break;
            default:
                $siteArea = '';
        }

        $prevCatID     = $category->ID;
        $category      = fieldToDB($category->Main_Cat);
        $action        = 'add';

        $CI->app_data->saveCategory($accountUserid,NULL,$category,$action,$siteArea,$prevCatID);
    }
}

function cpo_projects($projectArray) {
    $CI =& get_instance();
    $CI->load->model('projects/project_update','',true);
    $CI->load->model('clients/client_view','',true);
    $accountUserid = $CI->session->userdata('accountUserid');

    $countProjects = 0;
    foreach($projectArray as $project) {
        /*
         * Get new client ID
         */
        $newClientID = 0;
        $prevClientID = $project->ClientID;
        $clientArray = $CI->client_view->getClientFromPrevClientID($prevClientID,$accountUserid);
        if ($clientArray != FALSE && isset($clientArray[0]['ClientID'])) {
            $newClientID = $clientArray[0]['ClientID'];
        }

        /*
         * Get new categoryID for project
         */
        $newCatIDArray = $CI->app_data->getNewCatIDFromOld($project->Main_Cat,$CI->session->userdata('accountUserid'));
        $newCatID = $newCatIDArray['CatID'];

        $data['accountUserid']   = $accountUserid;
        $data['action']          = 'add';
        $data['clientID']        = $newClientID;
        $data['prevProjectID']   = $project->ID;
        $data['projectCategory'] = $newCatID;
        $data['projectTitle']    = $project->Title;
        $data['projectURL']      = $project->URL;
        $data['projectNotes']    = fieldToDB($project->Description);
        $data['projectStatus']   = $project->Status;
        $data['projectDateStart']= $project->Date_Start;
        $data['projectDateEnd']  = $project->Date_End;
        $data['projectTags']     = fieldToDB($project->Tags);

        $CI->project_update->saveProject($data);
        $countProjects++;
    }
    return $countProjects;
}

function cpo_project_contacts($projectContactArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('projects/project_view','',true);
    $CI->load->model('contacts/contacts','Contacts',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    foreach($projectContactArray as $projectContact) {
        /*
         * Get new project ID
         */
        $prevProjectID = $projectContact->Proj_ID;
        $projectArray = $CI->project_view->getProjectFromPrevProjectID($prevProjectID,$accountUserid);
        $newProjectID = $projectArray[0]['ProjectID'];

        $nameArray = explode(' ',$projectContact->Name);
        $nameFirst = '';
        $nameLast  = '';
        if (isset($nameArray[0])) {
            $nameFirst = $nameArray[0];
        }
        if (isset($nameArray[1])) {
            $nameLast = $nameArray[1];
        }

        $data['action']        = 'add';
        $data['accountUserid'] = $CI->session->userdata('accountUserid');
        $data['loginUID']      = '';
        $data['itemID']        = $newProjectID;
        $data['itemType']      = 'P';
        $data['nameFirst']     = fieldToDB($nameFirst);
        $data['nameLast']      = fieldToDB($nameLast);
        $data['title']         = '';
        $data['company']       = '';
        $data['address']       = '';
        $data['city']          = '';
        $data['state']         = '';
        $data['zip']           = '';
        $data['country']       = '';
        $data['timezone']      = $CI->session->userdata('gmtOffset');;
        $data['language']      = $CI->session->userdata('language');
        $data['avatarFilename']= '';
        $data['teamMember']    = 0;
        $data['vendor']        = 0;
        $data['peopleTags']    = '';
        $data['phone']['data'][0] = $projectContact->Phone;
        $data['phone']['type'][0] = 'work';
        $data['email']['data'][0] = $projectContact->Email;
        $data['email']['type'][0] = 'work';
        $data['url']['data'][0]   = '';
        $data['url']['type'][0]   = 'work';
        $data['im']['data'][0]    = '';
        $data['im']['type'][0]    = '';
        $pID = $CI->Contacts->saveContactPerson($data);
    }
}

function cpo_team($teamArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('contacts/contacts','Contacts',true);
    $CI->load->model('dashboard/dashboard_owners','Dashboard',true);
    $CI->load->model('users/user_login','',true);
    $accountUserid = $CI->session->userdata('accountUserid');

    $countTeam = 0;
    foreach($teamArray as $teamMember) {
        /*
         * Get new UID for this team member
         */
        $prevUID = $teamMember->UID;
        $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
        $newUID = $uidArray['UID'];

        $data['action']        = 'add';
        $data['accountUserid'] = $CI->session->userdata('accountUserid');
        $data['loginUID']      = $newUID;
        $data['itemID']        = '';
        $data['itemType']      = '';
        $data['nameFirst']     = fieldToDB($teamMember->First_Name);
        $data['nameLast']      = fieldToDB($teamMember->Last_Name);
        $data['title']         = '';
        $data['company']       = '';
        $data['address']       = '';
        $data['city']          = '';
        $data['state']         = '';
        $data['zip']           = '';
        $data['country']       = '';
        $data['timezone']      = convertTimezoneFormat($teamMember->GMTOffset);
        $data['language']      = $CI->session->userdata('language');
        $data['avatarFilename']= '';
        $data['teamMember']    = 1;
        $data['vendor']        = 0;
        $data['peopleTags']    = '';
        $data['phone']['data'][0] = $teamMember->PhoneOffice;
        $data['phone']['type'][0] = 'work';
        $data['email']['data'][0] = $teamMember->Email;
        $data['email']['type'][0] = 'work';
        $data['url']['data'][0]   = '';
        $data['url']['type'][0]   = 'work';
        $data['im']['data'][0]    = '';
        $data['im']['type'][0]    = '';
        $pID = $CI->Contacts->saveContactPerson($data);

        /*
         * Add a couple of dashboard widgets
         */
        $CI->Dashboard->addWidget('3',$newUID,NULL,$column=0);
        $CI->Dashboard->addWidget('5',$newUID,NULL,$column=1);
        $CI->Dashboard->addWidget('14',$newUID,NULL,$column=2);

        $countTeam++;
    }
    return $countTeam;
}

function cpo_project_tasks($taskArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('tasks/task_update','',true);
    $CI->load->model('tasks/task_view','',true);
    $CI->load->model('calendar/calendar_view','CalendarView',TRUE);

    $accountUserid = $CI->session->userdata('accountUserid');

    $countTasks = 0;
    foreach($taskArray as $task) {
        /*
         * Get new project ID
         */
        $prevProjectID = $task->ProjID;
        $projectArray  = $CI->project_view->getProjectFromPrevProjectID($prevProjectID,$accountUserid);
        $newProjectID  = $projectArray[0]['ProjectID'];

        /*
         * Get new UID for this team member
         */
        $prevUID = $task->AssignedTeamMember;
        $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
        $newUID = $uidArray['UID'];

        /*
         * Get CalendarID
         */
        $calendarArray = $CI->CalendarView->getCalendarDetailsFromType('task',$CI->session->userdata('accountUserid'));
        $data['calendarID'] = $calendarArray['CalendarID'];

        /*
         * Get parent task ID
         */
        $parentTaskID = $task->ParentID;
        $milestoneID = 0;
        if ($parentTaskID>0) {
            $taskArray = $CI->task_view->getTaskFromPrevTaskID($parentTaskID,$CI->session->userdata('accountUserid'));
            $milestoneID = $taskArray[0]['TaskID'];
        }

        /*
         * Get new task number
         */
        $taskNumber = $CI->task_update->getNewTaskNumber($newProjectID);
        
        /*
         * Let's see if there is a milestone with this
         */

        $data['accountUserid'] = $accountUserid;
        $data['action']        = 'add';
        $data['taskPrevID']    = $task->ID;
        $data['milestoneID']   = $milestoneID;
        $data['milestone']     = $task->Milestone;
        $data['projectID']     = $newProjectID;
        $data['taskTitle']     = $task->Title;
        $data['description']   = fieldToDB($task->Description);
        $data['taskDateStart'] = $task->Date_Start;
        $data['taskDateEnd']   = $task->Date_End;
        $data['estimatedTime'] = '';
        $data['status']        = $task->Status;
        $data['priority']      = $task->Priority;
        //$data['taskNumber']    = $task->TaskNo;
        $data['taskNumber']    = $taskNumber;
        $data['taskTags']      = $task->Tags;
        $data['userid']        = $CI->session->userdata('userid');

        $idArray = $CI->task_update->saveTask($data);
        $taskID = $idArray[0];

        $countTasks++;
    }
    return $countTasks;
}

function cpo_invoices($invoiceArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('projects/project_view','',true);
    $CI->load->model('clients/client_view','',true);
    $CI->load->model('finances/invoice_update','',true);
    $CI->load->model('finances/finance_view','',true);
    $CI->load->model('common/user_login','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    $countInvoices = 0;
    foreach($invoiceArray as $invoice) {
        /*
         * Get new client ID
         */
        $newClientID = 0;
        $clientArray = $CI->client_view->getClientFromPrevClientID($invoice->ClientID,$accountUserid);
        if ($clientArray != FALSE && isset($clientArray[0]['ClientUserid'])) {
            $newClientID = $clientArray[0]['ClientUserid'];
        }

        /*
         * Get new project ID
         */
        $prevProjectID = $invoice->Proj_ID;
        $projectArray  = $CI->project_view->getProjectFromPrevProjectID($prevProjectID,$accountUserid);
        $newProjectID  = $projectArray[0]['ProjectID'];

        /*
         * Get new category ID
         */
        $newCatIDArray = $CI->app_data->getNewCatIDFromOld($invoice->Main_Cat,$CI->session->userdata('accountUserid'));
        $newCatID = $newCatIDArray['CatID'];

        $invoiceNumber = $invoice->Inv_Prefix.$invoice->Inv_No;

        $data['accountUserid']         = $accountUserid;
        $data['userid']                = $CI->session->userdata('userid');
        $data['action']                = 'add';
        $data['prevInvoiceID']         = $invoice->ID;
        $data['ToUID']                 = $newClientID;
        $data['ToType']                = 'client';
        $data['invoiceNumber']         = $invoiceNumber;
        $data['clientID']              = $newClientID;
        $data['projectID']             = $newProjectID;
        $data['invoiceCategory']       = $newCatID;
        $data['invoiceTitle']          = fieldToDB($invoice->Inv_Name);
        $data['invoiceMessage']        = fieldToDB($invoice->Inv_Comments);
        $data['printMessage']          = $invoice->Inc_Comments;
        $data['amountSubTotal']        = $invoice->Inv_SubTotal;
        $data['amountShipping']        = $invoice->Shipping;
        $data['amountDiscount']        = '';
        $data['amountTax']             = $invoice->TaxAmt;
        $data['invoiceTotal']          = $invoice->Inv_Total;
        $data['invoicePONumber']       = '';
        $data['invoiceTaxRate']        = $invoice->TaxRate;
        $data['invoiceTaxName']        = '';
        $data['invoiceTaxID']          = '';
        $data['invoiceDiscount']       = '';
        $data['invoicePaymentDue']     = $invoice->PaymentDueDays;
        $data['invoicePaymentDueDate'] = $invoice->PaymentDueDate;
        $data['invoiceLateFee']        = $invoice->LateFeePercent;
        $data['invoiceLateFeeOther']   = $invoice->LateFeeAmount;
        $data['invoiceTags']           = $invoice->Tags;
        $data['invoiceDate']           = $invoice->Date_Entered;
        $data['items']                 = array();

        $invoiceID = $CI->invoice_update->saveInvoice($data);
        /*
         * Update invoice status
         */
        if ($invoice->Status == 1 || $invoice->Paid == 1) {
            $CI->invoice_update->updateInvoiceStatus($invoiceID,$invoice->Status,$invoice->Paid);
        }

        /*
         * Update sent status
         */
        if ($invoice->Sent == 1) {
            $CI->invoice_update->updateInvoiceSentStatus($invoiceID,$invoice->Sent,$invoice->Sent_Date);
        }

        /*
         * Enter payment information (Pay_No, Paid_Date,Pay_By)
         */
         switch ($invoice->Pay_By) {
             case "21":
                 $payBy = 'check';
                 break;
             case "Check":
                 $payBy = 'check';
                 break;
             case "Cash":
                 $payBy = 'cash';
                 break;
             case "Credit Card":
                 $payBy = 'cc_company';
                 break;
             case "Money Order":
                 $payBy = 'money_order';
                 break;
             default:
                 $payBy = '';
         }

         $data['invoiceID']               = $invoiceID;
         $data['payInvoicePaymentType']   = $payBy;
         $data['payInvoiceAccountNumber'] = fieldToDB($invoice->Pay_No);
         $data['payInvoiceAmount']        = '';
         $data['payInvoiceDate']          = $invoice->Paid_Date;
         $data['payComments']             = '';
         $data['userid']                  = $CI->session->userdata('userid');
         $data['invoiceStatus']           = $invoice->Status;
         $data['payStatus']               = $invoice->Paid;         

        $CI->invoice_update->saveInvoicePayment($data);
        $countInvoices++;
    }

    return $countInvoices;
}

function cpo_invoice_items($invoiceItemsArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('finances/invoice_update','',true);
    $CI->load->model('finances/finance_view','',true);
    $CI->load->model('timesheets/timesheet_view','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    foreach($invoiceItemsArray as $invoiceItem) {
        /*
         * Get new invoiceID
         */
        $newInvoiceID = 0;
        $invoiceArray = $CI->finance_view->getInvoiceFromPrevInvoiceID($invoiceItem->InvID,$accountUserid);
        if ($invoiceArray != FALSE && isset($invoiceArray[0]['InvoiceID'])) {
            $newInvoiceID = $invoiceArray[0]['InvoiceID'];
        }

        /*
         * Get new timeSheetID
         */
        $newTimeSheetID = 0;
        $timeSheetArray = $CI->timesheet_view->getTimeSheetEntryFromPrevTimeSheetID($invoiceItem->TimeSheetID,$accountUserid);
        if ($timeSheetArray != FALSE && isset($timeSheetArray[0]['TimeSheetID'])) {
            $newTimeSheetID = $timeSheetArray[0]['TimeSheetID'];
        }

        $data['invoiceID']   = $newInvoiceID;
        $data['desc']        = fieldToDB($invoiceItem->Item_Desc);
        $data['type']        = strtolower($invoiceItem->Kind);
        $data['qty']         = $invoiceItem->Qty;
        $data['tax']         = '';
        $data['rate']        = $invoiceItem->CostPer;
        $data['total']       = $invoiceItem->Item_Total;
        $data['timeSheetID'] = $newTimeSheetID;
        $CI->invoice_update->saveInvoiceItem($data);

    }
}

function cpo_project_time($timeArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);
    $CI->load->model('tasks/task_view','',true);
    $CI->load->model('projects/project_view','',true);
    $CI->load->model('timesheets/timesheet_update','',true);
    $CI->load->model('timesheets/timesheet_view','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    $countTimesheets = 0;
    foreach($timeArray as $time) {
        /*
         * Get new UID for this timesheet entry
         */
        $prevUID = $time->Userid;
        $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
        $newUID = $uidArray['UID'];

        if (empty($newUID)) {
            /*
             * Then we're importing time from the account owner and not a team member.
             */
            //$newUID = $CI->session->userdata('accountUserid');
            $uidArray = $CI->User_login->getLoginInformation(NULL,$CI->session->userdata('accountUserid'),NULL);
            $newUID = $uidArray[0]['UID'];
        }

        /*
         * Get new taskID
         */
        $prevTaskID = $time->Task_ID;
        $taskArray = $CI->task_view->getTaskFromPrevTaskID($prevTaskID,$CI->session->userdata('accountUserid'));
        $newTaskID = $taskArray[0]['TaskID'];

        /*
         * Get new projectID
         */
        $prevProjectID = $time->Proj_ID;
        $projectArray  = $CI->project_view->getProjectFromPrevProjectID($prevProjectID,$accountUserid);
        $newProjectID  = $projectArray[0]['ProjectID'];

        $timeEnd = $time->Stop;
        if (empty($time->Stop) || $time->Stop == 0) {
            $timeEnd = $time->Clock_Date;
        }

        $data['prevTimesheetID'] = $time->ID;
        $data['userid']          = $newUID;
        $data['projectID']       = $newProjectID;
        $data['taskID']          = $newTaskID;
        $data['elapsedTime']     = $time->E_Time;
        $data['dateClockStart']  = $time->Start;
        $data['dateClockEnd']    = $timeEnd;
        $data['comments']        = fieldToDB($time->Comments);

        $CI->timesheet_update->saveTimesheetFromImport($data);
        $countTimesheets++;
    }
    return $countTimesheets;
}

function cpo_notes($notesArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);
    $CI->load->model('notes/site_notes','',true);
    $CI->load->model('projects/project_view','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    $countNotes = 0;
    foreach($notesArray as $note) {
        /*
         * Get new projectID
         */
        $prevProjectID = $note->ProjID;
        $newProjectID = 0;
        $itemType = '';
        if ($prevProjectID>0) {
            $projectArray  = $CI->project_view->getProjectFromPrevProjectID($prevProjectID,$accountUserid);
            $newProjectID  = $projectArray[0]['ProjectID'];
            $itemType = 'P';
        }

        /*
         * Get new UID for this note
         */
        $prevUID = $note->Userid;
        $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
        $newUID = $uidArray['UID'];

        if (empty($newUID)) {
            $newUID = $CI->session->userdata('accountUserid');
        }

        $data['action']   = 'add';
        $data['userid']   = $newUID;
        $data['itemID']   = $newProjectID;
        $data['itemType'] = $itemType;
        $data['note']     = fieldToDB($note->Note);
        $data['dateNote'] = $note->DateNote;
        $data['onWidget'] = $note->OnHomeWidget;
        $data['public']   = $note->Public;
        
        $CI->site_notes->saveNote($data);
        $countNotes++;
    }
    return $countNotes;
}

function cpo_events($eventsArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);
    $CI->load->model('calendar/calendar_update','',true);
    $CI->load->model('calendar/calendar_view','',true);
    $CI->load->model('projects/project_view','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    /*
     * Get default calendarID for this account
     */
    $calendarArray = $CI->calendar_view->getDefaultCalendar($accountUserid,'project');
    $calendarID = $calendarArray[0]['CalendarID'];

    $countEvents = 0;
    foreach($eventsArray as $event) {
        /*
         * Don't enter events with ItemType = Project
         */
        if ($event->Item_Type != 'Project') {
            /*
             * Get new UID for this note
             */
            $prevUID = $event->Userid;
            $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
            $newUID = $uidArray['UID'];

            $data['action']           = 'add';
            $data['eventID']          = '';
            $data['accountUserid']    = $accountUserid;
            $data['eventCalendarID']  = $calendarID;
            $data['DBEventDateStart'] = $event->Start_Date;
            $data['DBEventDateEnd']   = $event->End_Date;
            $data['eventTitle']       = $event->Title;
            $data['eventDescription'] = fieldToDB($event->Description);
            $data['eventLocation']    = '';
            $data['eventTags']        = '';
            $data['eventRepeats']     = '';
            $data['eventReminder']    = '';
            $data['userid']           = $newUID;

            $CI->calendar_update->saveEvent($data);
            $countEvents++;
        }
    }
    return $countEvents;
}

function cpo_vendors($vendorsArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    $countVendors = 0;
    foreach($vendorsArray as $vendor) {     
        $data['action']        = 'add';
        $data['accountUserid'] = $CI->session->userdata('accountUserid');
        $data['prevPID']       = $vendor->VID;
        $data['loginUID']      = '';
        $data['itemID']        = '';
        $data['itemType']      = '';
        $data['nameFirst']     = fieldToDB($vendor->Name);
        $data['nameLast']      = '';
        $data['title']         = '';
        $data['company']       = $vendor->Name;
        $data['address']       = fieldToDB($vendor->Address,TRUE,TRUE,FALSE,TRUE);
        $data['city']          = '';
        $data['state']         = '';
        $data['zip']           = '';
        $data['country']       = '';
        $data['timezone']      = $CI->session->userdata('gmtOffset');;
        $data['language']      = $CI->session->userdata('language');
        $data['avatarFilename']= '';
        $data['teamMember']    = 0;
        $data['vendor']        = 1;
        $data['peopleTags']    = '';
        $data['phone']['data'][0] = $vendor->Phone;
        $data['phone']['type'][0] = 'work';
        $data['email']['data'][0] = $vendor->Email;
        $data['email']['type'][0] = 'work';
        $data['url']['data'][0]   = $vendor->URL;
        $data['url']['type'][0]   = 'work';
        $data['im']['data'][0]    = '';
        $data['im']['type'][0]    = '';
        $pID = $CI->Contacts->saveContactPerson($data);

        $countVendors++;
    }
    return $countVendors;
}

function cpo_tags($tagsArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    foreach($tagsArray as $tag) {
        /*
         * Get new UID for this tag
         */
        $prevUID = $tag->Userid;
        $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
        $newUID = $uidArray['UID'];

        $action   = 'add';
        $userid   = $newUID;
        $tag     = $tag->Tag;
        $siteArea = $tag->SiteArea;

        $CI->app_data->saveTag($userid,null,$tag,$action,$siteArea);
    }
}

function cpo_RSSRead($feedArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    foreach($feedArray as $feed) {
        /*
         * Get new UID for this feed
         */
        $prevUID = $feed->Userid;
        $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
        $newUID = $uidArray['UID'];

        $data['userid']  = $newUID;
        $data['feedURL'] = $feed->RSSURL;
    }
}

function cpo_APILogin($apiArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    foreach($apiArray as $api) {
        /*
         * Get new UID for this api
         */
        $prevUID = $api->Userid;
        $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
        $newUID = $uidArray['UID'];

        $data['apiUsername'] = $api->DUserName;
        $data['apiPassword'] = $api->DPassword;
        $data['api']         = $api->API;
    }
}

function cpo_expenses($expensesArray) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);
    $CI->load->model('projects/project_view','',true);
    $CI->load->model('contacts/contacts','Contacts',true);
    $CI->load->model('finances/expense_update','',true);

    $accountUserid = $CI->session->userdata('accountUserid');
    $countExpenses = 0;
    foreach($expensesArray as $expense) {
        /*
         * Get new UID for this api
         */
        $prevUID = $expense->Userid;
        $uidArray = $CI->User_login->getLoginFromPrevUID($prevUID,$accountUserid);
        $newUID = $uidArray['UID'];

        /*
         * Get new projectID
         */
        $prevProjectID = $expense->ProjID;
        $projectArray  = $CI->project_view->getProjectFromPrevProjectID($prevProjectID,$accountUserid);
        $newProjectID  = $projectArray[0]['ProjectID'];

        /*
         * Get new vendorID
         */
        $prevPID = $expense->VendorID;
        $personArray  = $CI->Contacts->getPersonFromPrevPersonID($prevPID,$accountUserid);
        $newPID  = $personArray[0]['PID'];

        /*
         * Get new categoryID
         */
        $newCatIDArray = $CI->app_data->getNewCatIDFromOld($expense->MainID,$CI->session->userdata('accountUserid'));
        $newCatID = $newCatIDArray['CatID'];
        
        if ($expense->Reimbursement == 'Reimbursable') {
            $reimburse = 'yes';
        } else {
            $reimburse = 'no';
        }

        switch ($expense->Payment_Method) {
             case "Check":
                 $payBy = 'check';
                 break;
             case "Cash":
                 $payBy = 'cash';
                 break;
             case "Corporate Credit Card":
                 $payBy = 'cc_company';
                 break;
             case "Personal Credit Card":
                 $payBy = 'cc_personal';
                 break;
             case "Money Order":
                 $payBy = 'money_order';
                 break;
             default:
                 $payBy = '';
         }
        $data['action']               = 'add';
        $data['accountUserid']        = $accountUserid;
        $data['userid']               = $newUID;
        $data['projectID']            = $newProjectID;
        $data['clientID']             = '';
        $data['expenseCategory']      = $newCatID;
        $data['expenseVendor']        = $newPID;
        $data['expenseTitle']         = fieldToDB($expense->Item_Name);
        $data['expenseDate']          = $expense->Expense_Date;
        $data['expensePaymentMethod'] = $payBy;
        $data['expenseReimburse']     = $reimburse;
        $data['expenseAmount']        = $expense->Amount;
        $data['expenseMarkup']        = '';
        $data['markupType']           = '';
        $data['expenseNotes']         = fieldToDB($expense->Comments);
        $data['expenseTags']          = '';
        $data['expenseRepeats']       = '';
        $data['expenseAcctNo']        = '';

        $CI->expense_update->saveExpense($data);
        $countExpenses++;
    }
    return $countExpenses;
}

function cpo_files($filesArray,$userDir) {
    $CI =& get_instance();
    $CI->load->model('common/app_data','',true);
    $CI->load->model('users/user_login','',true);
    $CI->load->model('files/file_maintenance','',true);
    $CI->load->model('projects/project_view','',true);

    $accountUserid = $CI->session->userdata('accountUserid');

    if (count($filesArray)>0) {
        $config['hostname'] = 'www.upstart-productions.com';
        $config['username'] = 'u39835398-cpo_dev';
        $config['password'] = 'addyG0@SE';
        $config['debug']    = FALSE;
        $config['port']     = 21;
        $config['passive']  = FALSE;
        $CI->ftp->connect($config);
    }

    $importFolder = $CI->file_maintenance->getImportFolder($accountUserid);
    if ($importFolder == FALSE) {
        /*
         * Create import folder
         */
        $data['accountUserid'] = $accountUserid;
        $data['userid']        = $CI->session->userdata('userid');
        $data['folderName']    = lang('file_import_folder');

        $importFolderID = $CI->file_maintenance->createImportFolder($data);
    } else {
        $importFolderID = $importFolder['FileID'];
    }

    $countFiles = 0;
    foreach($filesArray as $file) {
        $fileName = $file->FileName;
        $extension = substr(strrchr($fileName, '.'), 1);
        $newFileName = random_string('alnum',15).'.'.$extension;
        
        $remotePath = '/'.$userDir.'/projects/'.$file->ItemID.'/'.$fileName;
        $remoteRelPath = 'http://www.creativeprooffice.com/apps/user_files'.$remotePath;
        $localPath  = USER_DIR.$fileName;
        
        $ftpTransfer = $CI->ftp->download($remotePath,$localPath);

        if ($ftpTransfer != FALSE) {
            /*
             * Add file info to database and transfer to Amazon S3
             */
            $dataFile['action']         = 'add';
            $dataFile['accountUserid']  = $accountUserid;
            $dataFile['userid']         = $CI->session->userdata('userid');
            $dataFile['FileNameActual'] = cleanString($fileName);
            $dataFile['FileSize']       = $file->FileSize;
            $dataFile['FileType']       = $file->FileType;
            $dataFile['ParentID']       = $importFolderID;
            $fileID = $CI->file_maintenance->insertFileData($dataFile);

            /*
             * Connect file with project
             */
            $projectArray  = $CI->project_view->getProjectFromPrevProjectID($file->ItemID,$accountUserid);
            $newProjectID  = $projectArray[0]['ProjectID'];
            if ($newProjectID>0) {
                $CI->file_maintenance->attachFilesToItem($fileID,$newProjectID,'project',$CI->session->userdata('userid'));
            }

            $CI->filemanagers3->moveFileToS3($localPath,$fileName,$CI->session->userdata('userDir'),'private');
            unlink($localPath);
            $countFiles++;
        }
    }

    if (count($filesArray)>0) {
        $CI->ftp->close();
    }

    return $countFiles;
}

function array_transform($array){
	$output = '';
	foreach($array as $key => $value){
		if(!is_array($value)){
			$output .= "<$key>".htmlspecialchars($value)."</$key>\r\n";
		} else {
			$output .="<$key>";
			array_transform($value);
			$output .="</$key>\r\n";
		}
	}
	return $output;

}

function getXMLFromLocalDatabase($dbaseArray) {
    $dbaseHost     = $dbaseArray['dbaseHost'];
    $dbname        = $dbaseArray['dbaseName'];
    $dbaseUserName = $dbaseArray['dbaseUserName'];
    $dbasePassword = $dbaseArray['dbasePassword'];
    mysql_connect($dbaseHost,$dbaseUserName,$dbasePassword);

    $userid = '1';

	/*
	 * Start grabbing stuff for this user from the database
	 */
	$xmlString  = '<document>'."\r\n";
	$xmlString .= '<status>'."\r\n";
    $xmlString .= '<statusType>success</statusType>'."\r\n";
    $xmlString .= '<statusMessage>success</statusMessage>'."\r\n";
    $xmlString .= '</status>'."\r\n";

    /*
     * Get user file directory
     */
    $sql = "SELECT * from cpo_user_profile WHERE Userid = '$userid'";
    $result = mysql_db_query($dbname,$sql);
    $row = mysql_fetch_assoc($result);
    $userDir = $row['UserDir'];
    $xmlString .= '<userDir>'.$userDir.'</userDir>';

	/*
	 * Clients
	 */
	$sql = "SELECT * from cpo_clients WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<clients>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<client>';
		$xmlString .= array_transform($value);
		$xmlString .= '</client>';
	}
	$xmlString .= '</clients>'."\r\n";

	/*
	 * Projects
	 */
	$sql = "SELECT * from cpo_project WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
    $projectIDArray = array();

	$xmlString .= '<projects>';
    $pCount=0;
	while ($value = mysql_fetch_assoc($result)) {
        $projectIDArray[$pCount] = $value['ID'];
		$xmlString .= '<project>';
		$xmlString .= array_transform($value);
		$xmlString .= '</project>';
        $pCount++;
	}
	$xmlString .= '</projects>'."\r\n";

	/*
	 * Project Contacts
	 */
	$sql = "SELECT PC.ID,PC.Proj_ID,PC.Name,PC.Email,PC.Phone,PC.Temp from cpo_project_contacts PC
			LEFT JOIN cpo_project P ON P.ID = PC.Proj_ID
			WHERE P.Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<project_contacts>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<project_contact>';
		$xmlString .= array_transform($value);
		$xmlString .= '</project_contact>';
	}
	$xmlString .= '</project_contacts>'."\r\n";

	/*
	 * Project Tasks
	 */
	$sql = "SELECT * from cpo_project_tasks WHERE Userid = '$userid' ORDER BY Milestone DESC";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<tasks>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<task>';
		$xmlString .= array_transform($value);
		$xmlString .= '</task>';
	}
	$xmlString .= '</tasks>'."\r\n";

	/*
	 * Project Team Members
	 */
	$sql = "SELECT * from cpo_project_team_member WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<project_team_members>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<project_team_member>';
		$xmlString .= array_transform($value);
		$xmlString .= '</project_team_member>';
	}
	$xmlString .= '</project_team_members>'."\r\n";

	/*
	 * Invoices
	 */
	$sql = "SELECT * from cpo_invoice WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<invoices>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<invoice>';
		$xmlString .= array_transform($value);
		$xmlString .= '</invoice>';
	}
	$xmlString .= '</invoices>'."\r\n";

	/*
	 * Invoice Items
	 */
	$sql = "SELECT * from cpo_invoice_items WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<invoice_items>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<invoice_item>';
		$xmlString .= array_transform($value);
		$xmlString .= '</invoice_item>';
	}
	$xmlString .= '</invoice_items>'."\r\n";

	/*
	 * User Categories
	 */
	$sql = "SELECT * from cpo_main_cat WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<categories>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<category>';
		$xmlString .= array_transform($value);
		$xmlString .= '</category>';
	}
	$xmlString .= '</categories>'."\r\n";

	/*
	 * Notes
	 */
	$sql = "SELECT * from cpo_notes WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<notes>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<note>';
		$xmlString .= array_transform($value);
		$xmlString .= '</note>';
	}
	$xmlString .= '</notes>'."\r\n";

	/*
	 * Project Time
     * We need to pull timesheet entries by Proj_ID instead of Userid
     * in order to get timesheet entries from team members too.
	 */
    $xmlString .= '<times>';
    foreach($projectIDArray as $pID) {
        $sql = "SELECT * from cpo_project_time WHERE Proj_ID = '$pID'";
        $result = mysql_db_query($dbname,$sql);
        while ($value = mysql_fetch_assoc($result)) {
            $xmlString .= '<time>';
            $xmlString .= array_transform($value);
            $xmlString .= '</time>';
        }
    }
	$xmlString .= '</times>'."\r\n";

	/*
	 * Settings Invoice
	 */
	$sql = "SELECT * from cpo_settingsInvoice WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<settings>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<setting>';
		$xmlString .= array_transform($value);
		$xmlString .= '</setting>';
	}
	$xmlString .= '</settings>'."\r\n";

	/*
	 * Vendors
	 */
	$sql = "SELECT * from cpo_vendors WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<vendors>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<vendor>';
		$xmlString .= array_transform($value);
		$xmlString .= '</vendor>';
	}
	$xmlString .= '</vendors>'."\r\n";

	/*
	 * Tags
	 */
	$sql = "SELECT * from cpo_tags WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<tags>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<tag>';
		$xmlString .= array_transform($value);
		$xmlString .= '</tag>';
	}
	$xmlString .= '</tags>'."\r\n";

	/*
	 * RSS Feed
	 */
	$sql = "SELECT * from cpo_RSSRead WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<feeds>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<feed>';
		$xmlString .= array_transform($value);
		$xmlString .= '</feed>';
	}
	$xmlString .= '</feeds>'."\r\n";

	/*
	 * API's
	 */
	$sql = "SELECT * from cpo_APILogin WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<apis>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<api>';
		$xmlString .= array_transform($value);
		$xmlString .= '</api>';
	}
	$xmlString .= '</apis>'."\r\n";

	/*
	 * Calendar
	 */
	$sql = "SELECT * from cpo_calendar WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<events>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<event>';
		$xmlString .= array_transform($value);
		$xmlString .= '</event>';
	}
	$xmlString .= '</events>'."\r\n";

	/*
	 * Expenses
	 */
	$sql = "SELECT * from cpo_expense WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<expenses>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<expense>';
		$xmlString .= array_transform($value);
		$xmlString .= '</expense>';
	}
	$xmlString .= '</expenses>'."\r\n";

    /*
	 * Logins (just for clients and team members, not for account owners)
	 */
	$sql = "SELECT * from cpo_login WHERE Parent_Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<logins>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<login>';
		$xmlString .= array_transform($value);
		$xmlString .= '</login>';
	}
	$xmlString .= '</logins>'."\r\n";

    /*
	 * Files
	 */
	$sql = "SELECT * from cpo_files WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<files>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<file>';
		$xmlString .= array_transform($value);
		$xmlString .= '</file>';
	}
	$xmlString .= '</files>'."\r\n";

    /*
     * Team members
     */
    $sql = "SELECT
				First_Name,Last_Name,PhoneOffice,
				Email,Signup_Date,Comments,TimeZone,GMTOffset,
				L.Userid,L.User_Type,L.Role,L.UID
				FROM cpo_user_profile P
				LEFT JOIN cpo_login L ON P.Userid = L.UID
				WHERE L.User_Type = '".USER_TYPE_TEAM."' AND
				L.Parent_Userid = '$userid'";
    $result = mysql_db_query($dbname,$sql);

	$xmlString .= '<team_members>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<team_member>';
		$xmlString .= array_transform($value);
		$xmlString .= '</team_member>';
	}
	$xmlString .= '</team_members>'."\r\n";

	/*
	 * Frequent Tasks
	 */
	$sql = "SELECT * from cpo_freq_tasks WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<freq_tasks>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<freq_task>';
		$xmlString .= array_transform($value);
		$xmlString .= '</freq_task>';
	}
	$xmlString .= '</freq_tasks>'."\r\n";

	$xmlString .= '</document>';

	return $xmlString;
}

function getCSVFirstRow($csvFilePath) {
    $file = fopen($csvFilePath, 'r');
    $fieldsArray = fgetcsv($file, 0, ',', '"');
    if (is_array($fieldsArray)) {
        return $fieldsArray;
    } else {
        return FALSE;
    }
}

function makeMappedFieldsArray($importType) {
    switch ($importType) {
        case 'clients':
            $mappedFieldsArray = array(
                'clientCompany' => lang('client_form_company'),
                'clientAddress' => lang('client_form_address'),
                'clientCity'    => lang('client_form_city'),
                'clientState'   => lang('client_form_state'),
                'clientZip'     => lang('client_form_zip'),
                'clientCountry' => lang('client_form_country'),
                'clientPhone1'  => lang('client_form_phone1'),
                'clientPhone2'  => lang('client_form_phone2'),
                'clientPhone3'  => lang('client_form_phone3'),
                'clientEmail'   => lang('client_form_email'),
                'clientURL'     => lang('client_form_url'),
                'clientTimezone'=> lang('client_form_timezone'),
                'clientNotes'   => lang('client_form_notes')
            );
            break;
        case 'contacts':
            $mappedFieldsArray = array(
                'nameFirst'      => lang('common_first_name'),
                'nameLast'       => lang('common_last_name'),
                'title'          => lang('common_title'),
                'company'        => lang('common_company'),
                'address'        => lang('client_form_address'),
                'city'           => lang('client_form_city'),
                'state'          => lang('client_form_state'),
                'zip'            => lang('client_form_zip'),
                'country'        => lang('client_form_country'),
                'timezone'       => lang('client_form_timezone'),

                'phone_work'     => lang('common_phone_number').': '.lang('common_work'),
                'phone_mobile'   => lang('common_phone_number').': '.lang('common_mobile'),
                'phone_fax'      => lang('common_phone_number').': '.lang('common_fax'),
                'phone_home'     => lang('common_phone_number').': '.lang('common_home'),
                'phone_skype'    => lang('common_phone_number').': '.lang('common_skype'),
                'phone_pager'    => lang('common_phone_number').': '.lang('common_pager'),
                'phone_other'    => lang('common_phone_number').': '.lang('common_other'),

                'email_work'     => lang('client_form_email').': '.lang('common_work'),
                'email_personal' => lang('client_form_email').': '.lang('common_personal'),
                'email_other'    => lang('client_form_email').': '.lang('common_other'),

                'url_work'       => lang('client_form_url').': '.lang('common_work'),
                'url_personal'   => lang('client_form_url').': '.lang('common_personal'),
                'url_other'      => lang('client_form_url').': '.lang('common_other'),
                
                'im_type_work'     => lang('client_form_im_type').': '.lang('common_work'),
                'im_work'          => lang('client_form_im').': '.lang('common_work'),
                'im_type_personal' => lang('client_form_im_type').': '.lang('common_personal'),
                'im_personal'      => lang('client_form_im').': '.lang('common_personal'),
                'im_type_other'    => lang('client_form_im_type').': '.lang('common_other'),
                'im_other'         => lang('client_form_im_type').': '.lang('common_other')
            );
            break;
    }

    return $mappedFieldsArray;
}
?>