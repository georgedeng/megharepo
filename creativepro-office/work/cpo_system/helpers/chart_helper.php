<?php
function setChartTooltipStyle() {
    $t = new tooltip( 'Hello<br>val = #val#' );
    $t->set_shadow(true);
    $t->set_stroke(1);
    $t->set_colour('#e8e7d6');
    $t->set_background_colour('#fafaf5');
    $t->set_title_style( "{font-size: 12px; color: #333333;}" );
    $t->set_body_style( "{font-size: 10px; font-weight: bold; color: #333333;}" );
    $t->set_hover();

    return $t;
}

function colorChartArray() {
    $colorChartArray = array(
                    '#0662B0',
					'#E48701',
					'#A5bC4E',
					'#1B95D9',
					'#CACA9E',
					'#6693B0',
					'#F05E27',
					'#86D1E4',
					'#E4F9A0',
					'#FFD512',
					'#75B000',
					'#F984A1',
					'#FF66CC',
					'#FF0000',
					'#F6BD0F',
					'#FF9933',
					'#e0ad4d',
					'#BF9A55',
					'#ad7f2a',
					'#CCCC00',
					'#DEDDDD',
					'#999999',
					'#0099CC',
					'#99FFCC'
					);
    return $colorChartArray;
}
?>
