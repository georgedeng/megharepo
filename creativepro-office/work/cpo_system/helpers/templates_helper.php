<?php
function htmlEmailHeader($title) {
    $html = '
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>'.$title.'</title>
        </head>
        <body bgcolor="#FFFFFF" link="#003399" alink="#003399" vlink="#003399" leftmargin="0" topmargin="0" style="text-align: left;">
        <!-- This DIV wraps the entire invoice message. Style this tag instead of the BODY tag -->
        <div style="width: 100%;">';

    return $html;
}

function htmlEmailFooter() {
    $html = '</div></body>';

    return $html;
}

function htmlEmailHeaderCPO($title) {
    $html = '
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>'.$title.'</title>
        </head>
        <body bgcolor="#FFFFFF" link="#003399" alink="#003399" vlink="#003399" leftmargin="0" topmargin="0" style="text-align: left;">
        <!-- This DIV wraps the entire invoice message. Style this tag instead of the BODY tag -->
        <div style="width: 100%;"><center>
        <table style="background: #ffffff; border: 2px solid #e1e3e4;" align="center" cellpadding="0" cellspacing="0" border="0" width="700">
            <tr>
                <td>
                    <a href="'.INSECURE_URL.'"><img src="'.IMAGES_PATH_EMAIL.'emailHeader.gif" border="0" /></a>
                </td>
            </tr>
            <tr>
                <td>
                <table cellpadding="6">
                <tr>
                    <td>
        ';

    return $html;
}

function htmlEmailFooterCPO() {
    global $emailStyleArray;

    $h1 = $emailStyleArray['H1'];
    $h2 = $emailStyleArray['H2'];
    $h3 = $emailStyleArray['H3'];
    $p  = $emailStyleArray['P'];
    $smallGrayText = $emailStyleArray['SMALL_GRAY'];

    $html = '
        </td></tr></table>

        </td></tr>
        <tr><td>

        <table style="background: #2d3737; border-top: 2px solid #e1e3e4;" align="center" cellpadding="0" cellspacing="0" border="0" width="700">
            <tr>
                <td style="padding: 12px; width: 120px;">
                    <a href="http://www.upstart-productions.com" title="UpStart Productions"><img src="'.IMAGES_PATH_EMAIL.'logoUpStartProductions.gif" border="0" /></a>
                </td>
                <td>
                    <span '.$smallGrayText.'>CreativePro Office is a product of
                    <a style="color: #ffffff;" href="http://www.upstart-productions.com">UpStart Productions</a> Portland Oregon<br/ >
                    <a style="color: #ffffff;" href="mailto:'.MAIL_SUPPORT.'"><b>'.MAIL_SUPPORT.'</b></a> | <a style="color: #ffffff;" href="'.INSECURE_URL.'privacy">Privacy Policy</a> | <a style="color: #ffffff;" href="'.INSECURE_URL.'terms">Terms of Use</a> | <a style="color: #ffffff;" href="'.INSECURE_URL.'refund">Refund Policy</a></span>
                </td>
            </tr>
        </table>

        </td></tr></table>
        </center></div></body>';

    return $html;
}

function templatePaymentEmail($data) {
    global $emailStyleArray;

    $h1 = $emailStyleArray['H1'];
    $h2 = $emailStyleArray['H2'];
    $h3 = $emailStyleArray['H3'];
    $p  = $emailStyleArray['P'];
    $boxYellow = $emailStyleArray['BOX_YELLOW'];
    $boxBlue   = $emailStyleArray['BOX_BLUE'];

    $smallGrayText = $emailStyleArray['SMALL_GRAY'];

    if ($data['whyPay'] == 'newAccount') {
        $heading = 'Thanks for your payment!';
    } elseif ($data['whyPay'] == 'changeAccount') {
        $heading = 'You\'ve just updated your account!';
    } else {
        $heading = 'Thanks for updating your account.';
    }

    $templateHTML = '
        <h1 '.$h1.'>'.$heading.'</h1>
        
        <h1 '.$h2.'>You currently have the '.$data['plan'].' plan.</h1>
        <table cellpadding="0" cellspacing="6" border="0">
            <tr>
                <td style="border: 3px solid #fcfc80; background: #FFFFD2; padding: 6px; width: 320px;">
                    <p '.$h2.'>Cost per month: '.$data['cost'].'</p>
                    <p '.$h2.'>Active projects:  '.$data['projects'].'</p>
                    <p '.$h2.'>Team members:  '.$data['teamMembers'].'</p>
                    <p '.$h2.'>Storage Space:  '.$data['storage'].'</p>
                </td>
                <td style="border: 3px solid #bddbf9; background: #ebf4fd; padding: 6px; width: 320px;">
                    <h2 '.$h2.'>Your billing information</h2>
                    <p '.$p.'>'.$data['billingInfo'].'</p>
                </td>
            </tr>
        </table>
        <br>
        <p '.$p.'>Thanks for using CreativePro Office! We sincerely value and appreciate your business. If
        you have any questions or concerns about this transaction, please contact us right away at
        <b><a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a></b>.</p>
        <br>
        <p '.$p.'>[This transaction was processed securly using 256 bit encryption. Also, neither CreativePro
        Office nor UpStart Productions ever stores your credit card information on our servers.]</p>
        <br>
        ';

    return $templateHTML;
}

function templateSelfHostedPaymentEmail($data) {
    global $emailStyleArray;

    $h1 = $emailStyleArray['H1'];
    $h2 = $emailStyleArray['H2'];
    $h3 = $emailStyleArray['H3'];
    $p  = $emailStyleArray['P'];
    $boxYellow = $emailStyleArray['BOX_YELLOW'];
    $boxBlue   = $emailStyleArray['BOX_BLUE'];

    $smallGrayText = $emailStyleArray['SMALL_GRAY'];
    $templateHTML = '
        <h1 '.$h1.'>Thank you for your purchase!</h1>
        <table cellpadding="0" cellspacing="6" border="0">
            <tr>
                <td style="border: 3px solid #fcfc80; background: #FFFFD2; padding: 6px; width: 320px;">
                    <p>You purchased the self-hosted source code for CreativePro Office
                    along with 1 year of free updates and priority email support.</p>

                    <p>Your Order Number: <strong>'.$data['orderNumber'].'</strong><br />
                    Your Installation Key: <strong>'.$data['installationKey'].'</strong></p>
                </td>
                <td style="border: 3px solid #bddbf9; background: #ebf4fd; padding: 6px; width: 320px;">
                    <h2 '.$h2.'>Your billing information</h2>
                    <p '.$p.'>'.$data['billingInfo'].'</p>
                    <p '.$p.'><strong>'.$data['amount'].'</strong></p>
                </td>
            </tr>
        </table>

        <h1>What do I do now?</h1>
        <table cellpadding="0" cellspacing="6" border="0">
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconInstaller1.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>Get help installing your product.</h1>
                <p><a href="http://www.mycpohq.com/blog/tag/Self-hosted">The CreativePro Office blog</a> has several helpful articles to get you started.</p>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconUpdateSmall.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>Get updates.</h1>
                <p>Your purchase comes with 1 year of free updates. Update packages will be announced approximately
                once every quarter through the <a href="http://blog.mycpohq.com">CreativePro Office blog</a>.</p>

                <p>To download your updates, go to <a href="http://www.mycpohq.com/selfhosteddownload">http://www.mycpohq.com/selfhosteddownload</a>
                and enter your Order Number and Installation Key shown in this email message.
                </p>
            </td>
        </tr>
        </table>
        <br>
        <p '.$p.'>Thanks for using CreativePro Office! We sincerely value and appreciate your business. If
        you have any questions or concerns about this transaction, please contact us right away at
        <b><a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a></b>.</p>
        <br>
        <p '.$p.'>[This transaction was processed securly using 256 bit encryption. Also, neither CreativePro
        Office nor UpStart Productions ever stores your credit card information on our servers.]</p>
        <br>
        ';

    return $templateHTML;
}

function templateSelfHostedUpdatePaymentEmail($data) {
    global $emailStyleArray;

    $h1 = $emailStyleArray['H1'];
    $h2 = $emailStyleArray['H2'];
    $h3 = $emailStyleArray['H3'];
    $p  = $emailStyleArray['P'];
    $boxYellow = $emailStyleArray['BOX_YELLOW'];
    $boxBlue   = $emailStyleArray['BOX_BLUE'];

    $smallGrayText = $emailStyleArray['SMALL_GRAY'];
    $templateHTML = '
        <h1 '.$h1.'>Thank you for your purchase!</h1>
        <table cellpadding="0" cellspacing="6" border="0">
            <tr>
                <td style="border: 3px solid #fcfc80; background: #FFFFD2; padding: 6px; width: 320px;">
                    <p>You purchased a 1 year subscription for CreativePro Office
                    self-hosted updates and priority email support.</p>

                    <p>Your Order Number: <strong>'.$data['orderNumber'].'</strong><br />
                    Your Installation Key: <strong>'.$data['installationKey'].'</strong></p>
                </td>
                <td style="border: 3px solid #bddbf9; background: #ebf4fd; padding: 6px; width: 320px;">
                    <h2 '.$h2.'>Your billing information</h2>
                    <p '.$p.'>'.$data['billingInfo'].'</p>
                    <p '.$p.'><strong>'.$data['amount'].'</strong></p>
                </td>
            </tr>
        </table>

        <h1>What do I do now?</h1>
        <table cellpadding="0" cellspacing="6" border="0">
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconUpdateSmall.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>Get updates.</h1>
                <p>Your purchase comes with 1 year of free updates. Update packages will be announced approximately
                once every quarter through the <a href="http://blog.mycpohq.com">CreativePro Office blog</a>.</p>

                <p>To download your updates, go to <a href="http://www.mycpohq.com/selfhosteddownload">http://www.mycpohq.com/selfhosteddownload</a>
                and enter your Order Number and Installation Key shown in this email message.
                </p>
            </td>
        </tr>
        </table>
        <br>
        <p '.$p.'>Thanks for using CreativePro Office! We sincerely value and appreciate your business. If
        you have any questions or concerns about this transaction, please contact us right away at
        <b><a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a></b>.</p>
        <br>
        <p '.$p.'>[This transaction was processed securly using 256 bit encryption. Also, neither CreativePro
        Office nor UpStart Productions ever stores your credit card information on our servers.]</p>
        <br>
        ';

    return $templateHTML;
}

function templateNewAccountEmail($data) {
    global $emailStyleArray;

    $h1 = $emailStyleArray['H1'];
    $h2 = $emailStyleArray['H2'];
    $h3 = $emailStyleArray['H3'];
    $p  = $emailStyleArray['P'];
    $blue = $emailStyleArray['BLUE'];
    $boxYellow = $emailStyleArray['BOX_YELLOW'];
    $boxBlue   = $emailStyleArray['BOX_BLUE'];

    $smallGrayText = $emailStyleArray['SMALL_GRAY'];

    if (!empty($data['webAddress'])) {
        $loginDomain = $data['webAddress'].'.'.SITE_DOMAIN;
    } else {
        $loginDomain = INSECURE_URL.'login';
    }

    $templateHTML = '
        <table>
        <tr>
            <td style="width: 400px;"><h1 '.$h1.'>Welcome to CreativePro Office!</h1></td>
            <td style="width: 200px; text-align: right;"><p '.$h2.'>Account No:  '.$data['accountNo'].'</p></td>
        </tr>
        </table>

        <h1 '.$h2.'>You currently have the '.$data['plan'].' plan.</h1>
        <table cellpadding="0" cellspacing="6" border="0">
            <tr>
                <td style="border: 3px solid #fcfc80; background: #FFFFD2; padding: 6px; width: 640px;">                    
                    <p '.$h2.'>Cost per month: '.$data['cost'].'</p>
                    <p '.$h2.'>Active projects:  '.$data['projects'].'</p>
                    <p '.$h2.'>Team members:  '.$data['teamMembers'].'</p>
                    <p '.$h2.'>Storage Space:  '.$data['storage'].'</p>
                </td>
            </tr>
        </table>
        <br>
        <center>
        <table cellpadding="0" cellspacing="6" border="0">
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconLock.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>Your login</h1>
                <p>To login to your account, go to <a href="'.$loginDomain.'">'.$loginDomain.'</a>. Your login email address is <b '.$blue.'>'.$data['userid'].'</b>. If you forget your password, you can get
                    a new one by clicking the <u>I forgot my password</u> link on your login page.</p>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconBlog.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>CreativePro Office Blog</h1>
                <p>Read <a href="'.CPO_BLOG_URL.'">our blog</a> to keep up with the latest feature additions, bug
                fixes, and tutorials.</p>
            </td>
        </tr>';
    
        if ($data['plan'] != 'free') {
            $templateHTML .= '
                <tr>
                    <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconMoney.gif" /></td>
                    <td style="width: 400px; text-align: left;">
                        <h1 '.$h3.'>When do I pay?</h1>
                        <p>Your free trial will end on '.$data['expDate'].'. After that date, you
                        will be asked to provide payment information before you can access your account again. Please see our <a href="'.INSECURE_URL.'refund">Refund Policy</a>
                        and <a href="'.INSECURE_URL.'terms">Terms of Use</a> for more information.
                    </p>
                    </td>
                </tr>';
            if ($data['plan'] != 'solo') {
                $templateHTML .= '
                    <tr>
                        <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconHelp.gif" /></td>
                        <td style="width: 400px; text-align: left;">
                            <h1 '.$h3.'>Need help?</h1>
                            <p>Your account includes priority email support. Shoot us an email any time, for any reason at <a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a>.
                            We\'re happy to answer your questions and we take customer support seriously!</p>
                        </td>
                    </tr>
                ';
            }
        }

        $templateHTML .= '
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconBox.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>Account changes</h1>
                <p>You can upgrade or downgrade your account at any time by clicking the <b '.$blue.'>Account</b> tab
                in the <b '.$blue.'>Office Settings</b> area. Look for the <img src="'.IMAGES_PATH_EMAIL.'iconSettings.gif" />
                icon in the upper-right. Then click the <b '.$blue.'>Change Your Plan</b> button on the right.</p>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconBug1.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>Find a bug? Let us know.</h1>
                <p>You can instantly alert us about any problems you find while using CreativePro Office.
                Just click the <img src="'.IMAGES_PATH_EMAIL.'iconBug2.gif" /> icon in the upper-right and give
                us a brief description of the problem. <b '.$blue.'>We appreciate your help!</b></p>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconTransfer.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>Migrating from an old version of CPO?</h1>
                <p>If you have been using CreativePro Office version 1, you can easily transfer your data
                to your new account.  Click the <img src="'.IMAGES_PATH_EMAIL.'iconSettings.gif" /> icon
                in the upper-right, then click the Data tab.  Under the Import Data heading, you will be asked
                to enter the user name and password from your version 1 account - then your data will transfer to your new
                account.</p>
                <p>(This only works for accounts on the hosted version of CPO, not the
                source code versions. Also, if you have more team members on your old account than
                your new account supports, some team members will be dropped.)</p>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 50px;"><img src="'.IMAGES_PATH_EMAIL.'iconMonitor.gif" /></td>
            <td style="width: 400px; text-align: left;">
                <h1 '.$h3.'>Video tutorials</h1>
                <p>Sometimes we\'ll post <a href="'.INSECURE_URL.'tour">quick tutorial screencasts</a> to help users get the most our of
                their CreativePro Office account.</p>
            </td>
        </tr>
        </table>
        </center>

        <br>
        <p '.$p.'>Thanks for using CreativePro Office! We sincerely value and appreciate your business. If
        you have any questions about how our service works, please contact us right away at
        <b><a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a></b>.</p>
        ';

    return $templateHTML;
}

function templateNCDB() {
    $templateHTML = '
        <HTML>

<HEAD>
<META HTTP-EQUIV="content-type" CONTENT="text/html; charset=ISO-8859-1">
<TITLE>NCDB Webinar Announcement</TITLE>
<LINK HREF="http://nationaldb.org/css/ennounce.css" REL="stylesheet"
 TYPE="text/css">
<STYLE TYPE="text/css">
<!--
.style2 {font-size: medium}
.style3 {color: #481460}
-->
</STYLE>
</HEAD>

<BODY LINK="#006666" ALINK="#006666" VLINK="#006666">
<TABLE ALIGN="CENTER" BORDER="0" WIDTH="750">
<TBODY>
<TR>
<TD width="118"><A HREF="http://nationaldb.org/"><IMG
SRC="http://nationaldb.org/documents/products/enews_images/logo.gif"
HEIGHT="92" BORDER="0" WIDTH="118" ALT="NCDB Logo"></A></TD>
<TD width="180"> <h4 align="center">The National Consortium<br>
 on Deaf-Blindness</h4>
</TD>
<TD width="438"><H1 ALIGN="CENTER"><SPAN CLASS="style3">NCDB<SPAN CLASS="style2">2.0</SPAN><BR>
 e-Nnouncement</SPAN></H1></TD>
</TR>
</TBODY>
</TABLE>
<TABLE ALIGN="CENTER" BORDER="0" CELLPADDING="5" WIDTH="750">
<TBODY>
<TR>
<TD BGCOLOR="#BEBAD8"><h4 align="right">February 24, 2012 </h4></TD>
</TR>
</TBODY>
</TABLE>
<TABLE WIDTH="750" BORDER="1" ALIGN="center" CELLPADDING="5" CELLSPACING="5" bgcolor="#FCFCF8" frame="border" rules="none">
<TBODY>
<TR>
<TD WIDTH="721">
<H4>&nbsp;</H4>
<h2 align="center"><strong> A special message for you from<br>
             Kathy McNulty,
             Associate Project Director<br>
             National Consortium on Deaf-Blindness.</strong></h2>
<H4>&nbsp;</H4>
<p>Hello Parent Center Staff,</p>
<p><a href="http://videos.tadnet.org/videos/154"><img src="http://nationaldb.org/documents/products/enews_images/pti.jpg" alt="Photo of Kathy McNulty" width="109" height="83" border="0"></a></p>
<h5><em>Click image to launch video in your web browser</em></h5>
<p>The attached pdf document is both for you and the parents you serve...</p>
<p>The following links showcase materials specific to parents of children who are deaf-blind.</p>
<ul>
<li><a href="http://www.nationaldb.org/DB101Children.php#">Who are Children Who are Deaf-Blind</a></li>
<li><a href="http://www.nationaldb.org/NCDBProducts.php?prodID=115">Wisdom from Parents - I Wish I Had</a></li>
<li><a href="http://www.nationaldb.org/ppStateDBProjects.php">Supports and Resources for Families</a></li>
</ul>
<P>&nbsp;</P></TD>
</TR>
<TR>
<TD BGCOLOR="#BEBAD8" WIDTH="721">&nbsp;</TD>
</TR>
<TR>
<TD WIDTH="721"> <P><FONT COLOR="#990000"><B><A
HREF="http://www.facebook.com/pages/National-Consortium-on-Deaf-Blindness-NCDB/135831739762225?ref=ts"><IMG
SRC="http://nationaldb.org/documents/products/enews_images/facebook_logo.jpg"
ALT="Facebook logo" WIDTH="45" HEIGHT="44" BORDER="0" ALIGN="left"></A>Follow
Us on FACEBOOK</B></FONT><BR>
<A
 HREF="http://www.facebook.com/pages/National-Consortium-on-Deaf-Blindness-NCDB/135831739762225?ref=ts">Current
news, information and resources</A></P>
<P>National Consortium on Deaf-Blindness<br>
   345 N. Monmouth Ave.<br>
   Monmouth, OR 97361<br>
<a href="http://nationaldb.org">http://nationaldb.org</a><a href="mailto:info@nationaldb.org"><br>
       info@nationaldb.org </a></P>
</TD>
</TR>
</TBODY>
</TABLE>
<TABLE ALIGN="CENTER" BORDER="0" WIDTH="750">
<TBODY>
<TR>
<TD HEIGHT="95" WIDTH="126"><img
src="http://nationaldb.org/documents/products/enews_images/Ideas_logofinalJ.jpg"
alt="IDEAs That Work logo" width="97" height="84"> </TD>
<TD HEIGHT="95" WIDTH="614"> <h5>Funded through award
#H326T060002 by the U.S. Department of Education, OSERS, OSEP. The opinions and
policies expressed by this publication do not necessarily reflect those of The
Teaching Research Institute, or the U.S. Department of Education.</h5>
</TD>
</TR>
</TBODY>
</TABLE>
</BODY>
</HTML>
    ';
    return $templateHTML;
}
?>
