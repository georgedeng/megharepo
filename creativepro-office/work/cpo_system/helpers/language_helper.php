<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Language Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/language_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Lang
 *
 * Fetches a language variable and optionally outputs a form label
 *
 * @access	public
 * @param	string	the language line
 * @param	string	the id of the form element
 * @return	string
 */	
if ( ! function_exists('lang'))
{
	function lang($line, $id = '')
	{
		$CI =& get_instance();
		$line = $CI->lang->line($line);

		if ($id != '')
		{
			$line = '<label for="'.$id.'">'.$line."</label>";
		}

		return $line;
	}
}

function languages() {
	$languages['en'] = 'English';
    $languages['fr'] = 'French';
	$languages['de'] = 'German';
    $languages['hu'] = 'Hungarian';
    $languages['it'] = 'Italian';
    $languages['pl'] = 'Polish';
    $languages['pt'] = 'Portuguese';
    $languages['ru'] = 'Russian';
	$languages['es'] = 'Spanish';    

	return $languages;
}

function loadLanguageFiles($langCode=NULL) {
    $CI =& get_instance();

	if (empty($langCode)) {
		$langCode = $CI->session->userdata('language');
	}
    if (empty($langCode) || $langCode == 'null' || $langCode == NULL) {
        $langCode = 'en';
    }
    $CI->lang->load('app',$langCode);
    $CI->lang->load('help',$langCode);
    $CI->lang->load('date',$langCode);
    $CI->lang->load('calendar',$langCode);
    $CI->lang->load('form_validation',$langCode);
    $CI->lang->load('industry',$langCode);
    $CI->lang->load('invoice_'.$langCode,$langCode);
}

// ------------------------------------------------------------------------
/* End of file language_helper.php */
/* Location: ./system/helpers/langage_helper.php */