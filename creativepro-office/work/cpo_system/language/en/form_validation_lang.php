<?php

$lang['required'] 			= "The %s field is required.";
$lang['isset']				= "The %s field must have a value.";
$lang['valid_email']		= "The %s field must contain a valid email address.";
$lang['valid_emails'] 		= "The %s field must contain all valid email addresses.";
$lang['valid_url'] 			= "The %s field must contain a valid URL.";
$lang['valid_ip'] 			= "The %s field must contain a valid IP.";
$lang['min_length']			= "The %s field must be at least %s characters in length.";
$lang['max_length']			= "The %s field can not exceed %s characters in length.";
$lang['exact_length']		= "The %s field must be exactly %s characters in length.";
$lang['alpha']				= "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "The %s field must contain only numbers.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['matches']			= "The %s field does not match the %s field.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";

$lang['form_required_clientUserid']  = "Client userid cannot be blank.";
$lang['form_istaken_clientUserid']   = "Client userid %s is alreay taken. Please select another.";
$lang['form_required_clientCompany'] = "Client company cannot be blank.";
$lang['form_required_client']        = "Client cannot be blank.";
$lang['form_required_project_title'] = "Project title cannot be blank.";
$lang['form_required_email']         = "Please enter a valid email address.";
$lang['form_minlength_clientEmail']  = "Client email address must be at least 10 characters.";
$lang['form_required_expense_title']  = "You must enter an expense item name.";
$lang['form_required_expense_amount'] = "You must enter an expense item amount.";
$lang['form_required_company_name']   = "You must enter a Company Name.";
$lang['form_required_company_email']  = "You must enter an Email Address.";

/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */