<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonTrash {
    function countItemsInTrash($return=FALSE) {
        $CI =& get_instance();
        $trashTotal = $CI->Trash->getNumberOfItemsInTrash();
        $CI->session->set_userdata('trashTotal',$trashTotal);
        if ($return == FALSE) {
            echo $trashTotal;
        } else {
            return $trashTotal;
        }
    }
}
?>