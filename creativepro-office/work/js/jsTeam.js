var moduleArray = ['client','project','task','invoice','expense','timesheet','file','team','message','calendar','contacts','trash','settings'];

var initTeamObj = {
	initTeam : function() {
		teamTabs = $('#tabsTeam').tabs({
			fx: { opacity: 'toggle' },
			show: function(event,ui) {
				var panel = ui.panel.id;
                if (panel == 'windowViewTeamMembers') {
					if (teamArray.length == 0) {
					}
				} else if (panel == 'windowRoles') {
					
				}
			}
		});

        /*
         * Attach event handlers
         */
        $('#buttonSave').click(function() {
            updateTeamObj.updateRolesPermissions();
            return false;
        });

        $('#buttonCancel').click(function() {
            helperTeamObj.clearRolesForm();
            return false;
        });

        $('#selectRoleEdit').change(function() {
			if ($(this).val() != '') {				
				var thisVal = $(this).val();
				var thisValArray = thisVal.split('|');
				var roleName = $('#selectRoleEdit :selected').text();
                if (thisValArray[2] > 0) {
                    /*
                     * Turn on the Delete button
                     */
                    $('#deleteText').hide();
                } else {
                    $('#deleteText').show();
                }
				$('#roleName').val(roleName.trim());
				$('#addText').hide();
				$('#updateText').show();

				helperTeamObj.populatePermissionForm(thisValArray[0],thisValArray[1]);
			} else {
				helperTeamObj.clearRolesForm();
			}
        });

        $('#linkDeleteRole').click(function() {
            var roleID = $('#roleID').val();
            updateTeamObj.deleteRole(roleID);
            return false;
        });

        $('.selectAll').click(function() {
           if ($(this).is(':checked')) {
               $(this).parents('tr').find('input:not(.own)').attr('checked',true);
           } else {
               $(this).parents('tr').find('input').attr('checked',false);
           }
        });

        $('.own').click(function() {
           if ($(this).is(':checked')) {
               $(this).parents('tr').find('.all').attr('checked',false);
           }
        });

        $('.all').click(function() {
           if ($(this).is(':checked')) {
               $(this).parents('tr').find('.own').attr('checked',false);
           }
        });

        $('.add').click(function() {
           if ($(this).is(':checked')) {
               if (($(this).parents('tr').find('.own') && $(this).parents('tr').find('.own').attr('checked') == true) || ($(this).parents('tr').find('.all') && $(this).parents('tr').find('.all').attr('checked') == true)) {
               } else {
                   $(this).parents('tr').find('.view').attr('checked',true);
               }
           }
        });

        $('.update').click(function() {
           if ($(this).is(':checked')) {
               if (($(this).parents('tr').find('.own') && $(this).parents('tr').find('.own').attr('checked') == true) || ($(this).parents('tr').find('.all') && $(this).parents('tr').find('.all').attr('checked') == true)) {
               } else {
                   $(this).parents('tr').find('.view').attr('checked',true);
               }
           }
        });

        $('.delete').click(function() {
           if ($(this).is(':checked')) {
               if (($(this).parents('tr').find('.own') && $(this).parents('tr').find('.own').attr('checked') == true) || ($(this).parents('tr').find('.all') && $(this).parents('tr').find('.all').attr('checked') == true)) {
               } else {
                   $(this).parents('tr').find('.view').attr('checked',true);
               }
           }
        });

        $('.addAll').click(function() {
           if ($(this).is(':checked')) {
               $(this).parents('tr').find('.viewAll').attr('checked',true);
           }
        });

        $('.addOwn').click(function() {
           if ($(this).is(':checked')) {
               $(this).parents('tr').find('.viewOwn').attr('checked',true);
           }
        });


		/*
		 * See if there's any data for existing roles
		 */        
		helperTeamObj.updateRoleSelect();		

		/*
		 * Set some initial settings
		 */
		$('#makeTeamMember').attr('checked',true);
		$('#contactTeamMemberRoleContainer').show();
		$('#formContactContainer').slideUp();

		/*
		 * Render team member information cards
		 */
        getContactObj.getContacts('T',0,1,0);

        /*
		 * Init contacts stuff
		 */
        helperContactsObj.initContactsContainer();
		helperContactsObj.initContactsForm();        
	}
}

var helperTeamObj = {
    clearRolesForm : function() {
		$('#roleID').val('0');
		$('#action').val('add');
		$('#roleName').val('');
		$('#rolesForm').find('input:checkbox').attr('checked',false);
		$('#addText').show();
		$('#updateText').hide();
        $('#deleteText').hide();
		$('#selectRoleEdit').val('');
    },
	populatePermissionForm : function(permString,roleID) {
		$('#roleID').val(roleID);
		$('#action').val('edit');
		var permArray = permString.split('-');
		for(var a in moduleArray) {
            for(var b=0;b<=6;b++) {
				if (permArray[a].substr(b,1) == 1) {
					$('.'+moduleArray[a]+'Check:eq('+b+')').attr('checked',true);
				} else {
					$('.'+moduleArray[a]+'Check:eq('+b+')').attr('checked',false);
				}
			}
		}
	},
	updateRoleSelect : function() {
        /*
         * roleData is a JavaScript array generated by views\team\TeamUpdate.php
         */
		if (roleData != '0') {
            var jsonObj = eval('(' + roleData + ')');
			$('#selectRoleEdit').html('<option></option>');
			$('#selectRoleEditContainer').show();

			for(var a=0;a<=(jsonObj.length-1);a++) {
				var roleID     = jsonObj[a].RoleID;
				var roleName   = jsonObj[a].RoleName;
				var permString = jsonObj[a].RolePermissions;
                var userCount  = jsonObj[a].UserCount;
				var optionString = '<option value="'+permString+'|'+roleID+'|'+userCount+'">'+roleName+'</option>';
				$('#selectRoleEdit').append(optionString);                
			}
		} else {
			$('#selectRoleEditContainer').hide();
		}
	}
}

var getTeamDataObj = {
    parsePermissions : function() {

    },
	getRolesPermissions : function() {
		$.ajax({
		    type: "POST",
		    url:  siteURL+'team/TeamUpdate/getRolesPermissions/json/0',
		    success: function(payload) {
				roleData = payload;
                helperTeamObj.updateRoleSelect();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
		});	
	}

}

var updateTeamObj = {
    updateRolesPermissions : function() {
        var roleName = $('#roleName').val();
        /*
         * Create permission string
         */
        var permString = '';
        $('.clientCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.projectCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.taskCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.invoiceCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.expenseCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.timesheetCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.fileCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.teamCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.messageCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.calendarCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
		permString += '-';
        $('.contactsCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.trashCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });
        permString += '-';
        $('.settingsCheck').each(function() {
            if ($(this).is(':checked')) {
                permString += '1';
            } else {
                permString += '0';
            }
        });

        var formHash = {
                    roleID      : $('#roleID').val(),
                    action      : $('#action').val(),
                    roleName    : roleName,
                    permString  : permString
        }

        $.ajax({
		    type: "POST",
		    url:  siteURL+'team/TeamUpdate/saveRolesPermissions',
		    data: formHash,
		    success: function(payload){
                var jsonObj = eval('('+payload+')');
                var message = jsonObj.Message;
                var roleID  = jsonObj.RoleID;
                $('#messageRoles').systemMessage({
					status:  'success',
					message: message,
					size:    'Big'
				});
				getTeamDataObj.getRolesPermissions();
				helperTeamObj.clearRolesForm();

                if ($('#action').val() == 'add') {
                    var optionString = '<option value="'+roleID+'">'+roleName+'</option>';
                    $('#contactTeamMemberRole').append(optionString);
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    deleteRole : function(roleID) {
        var jsonObj = eval('(' + roleData + ')');
        $.ajax({
		    type: "POST",
		    url:  siteURL+'team/TeamUpdate/deleteRolesPermissions/'+roleID,
		    success: function(payload){
                roleData = payload;
                helperTeamObj.clearRolesForm();
                helperTeamObj.updateRoleSelect();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

$(document).ready(function() {
	initTeamObj.initTeam();
});