var haveTax = false;
var haveDiscount = false;
var haveShipping = false;
var itemRowHTMLTemplate = $('.invoiceItemRowTemplate').html();
var tabIndex = 0;

var initInvoiceFormObj = {
	initInvoiceForm : function() {
		/*
		 * Prevent form submit on client field ENTER
		 */
		$('#clientID').bind("keypress", function(e) {
             if (e.keyCode == 13) {
                 return false;
            }
         });

		$('textarea.expanding').elastic();

		/*
		 * Init client select combobox
		 */
        $('#clientID').autocomplete({
            source: clients,
            select: function(event, ui) {
                var clientID = ui.item.id;
                $('#clientIDHolder').val(clientID);
                /*
                 * Get client projects
                 */
				initInvoiceFormObj.getProjectsFromClientID(clientID);
                /*
                 * Get client email address
                 */
                initInvoiceFormObj.getEmailFromClientID(clientID);
            }
        });
        
        /* 
         * Init category select combobox
         */ 
       $('#invoiceCategorySelect').combobox({
           source: categoryJSON, 
           select: function(event, ui) {
                var categoryID = ui.item.id;
                $('#invoiceCategory').val(categoryID);
           },
           width: '374px',
           tabindex: 7,
           id: 'invoiceCategoryCombo',
           name: 'invoiceCategoryCombo'
       });

		/*
		 * Init date pickers
		 */
		$(".dateField").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

        /*
		 * Init tag field
		 */
        var tagBox = $('#invoiceTags').autoSuggest(
            tagJSON,
            {
                resultsHighlight: false,
                neverSubmit: true,
                boxWidth: '400px',
                startText: '',
                valuesHolderName: 'invoiceTagsHolder',
                preFill: $('#tagHolder').val()
            }
        );     

        /*
		 * Init validation
		 */
		$("#formInvoice").validate({
			errorClass: 'formError',
			errorElement: 'div',
			rules: {
				clientID:      "required",
				invoiceNumber: "required",
				invoiceTitle:  "required"
			},
			messages: validationMessages
		});

		/*
		 * Determine render state of fields in right hand side-bar
		 */
		$('#invoicePaymentDueDate').parent().hide();
		$('#invoiceLateFeeOther').parent().hide();

		if ($('#invoicePaymentDueDate').val() != '') {
			$('#invoicePaymentDueDate').parent().show();
		}

		if ($('#invoiceLateFeeOther').val() != '') {
			$('#invoiceLateFeeOther').parent().show();
		}

		$('#invoicePaymentDue').change(function() {
			if ($('#invoicePaymentDue').val() == 'other') {
				$('#invoicePaymentDueDate').parent().show();
			} else {
				$('#invoicePaymentDueDate').parent().hide();
			}
		});

		$('#invoiceLateFee').change(function() {
			if ($('#invoiceLateFee').val() == 'amount') {
				$('#invoiceLateFeeOther').parent().show();
			} else {
				$('#invoiceLateFeeOther').parent().hide();
			}
		});

		/*
		 * Set button event handlers
		 */
		$('#buttonCancel').click(function() {
            if ($('#invoiceID').val()>0) {
                var invoiceID = $('#invoiceID').val();
                window.location = siteURL+'finances/FinanceView/viewInvoice/'+invoiceID;
            } else {
                window.location = siteURL+'finances/FinanceView#windowViewInvoicesEstimates';
            }
            return false;
        });

        /*
         * Email invoice
         */
        $('#buttonEmail').click(function() {
            $('#emailFormContainer').show();
            $('#emailInvoiceFormContainer').slideDown('fast');
            $('.emailInvoiceSendCancel').show();

            /*
             * If we have a value in invoiceEmail, put it in the Send to box.
             */
            $('.emailInvoiceTo').val($('#invoiceEmail').val());

            return false;
        });

        $('.emailInvoiceSend').click(function() {
           $('#invoiceEmail').val('1');
           $('#formInvoice').submit();
        });
        
        $('.emailInvoiceSendCancel').click(function() {
            emailInvoiceObj.clearEmailForm(true);
            $('#emailFormContainer').hide();
            $('#emailInvoiceFormContainer').slideUp('fast');
            return false;
        });

        /*
         * Print invoice
         */
		$('#buttonPrint').click(function() {
			$('#invoicePrint').val(1);
			$('#formInvoice').submit();
		});

		$('#invoiceTaxRate').blur(function() {
			initInvoiceFormObj.checkTaxStatus();
		});

		$('#invoiceShipping').blur(function() {
			initInvoiceFormObj.checkShipping();
		});

		$('#invoiceDiscount').blur(function() {
			initInvoiceFormObj.checkDiscount();
		});

        /*
         * See if we have any settings to put into the form
         */
        if ($('#action').val() == 'add') {
            settingsObj.putSettingValuesInForm();
        }

		/*
		 * Check tax status: if tax % has a value, activate invoice item
		 * tax checkboxes and check them.
		 */
		initInvoiceFormObj.checkTaxStatus();

		/*
		 * Check shipping amount
		 */
		initInvoiceFormObj.checkShipping();

		/*
		 * Check discount amount
		 */
		initInvoiceFormObj.checkDiscount();

		/*
		 * Invoice item events and handlers
		 */
		initInvoiceFormObj.attachInvoiceItemEventHandlers();

        $('#buttonSelectExpenses').click(function() {
            if ($('#modalInvoiceItemType').val() == 'timesheet') {
                /*
                 * Clear the modal contents
                 */
                $('#modalInvoiceItemsContainer').html('');
            }
            $('#modalInvoiceItems').dialog('open').dialog('option','title',commonTerms['selectExpenses']);
            $('#modalInvoiceItemType').val('expense');
            $('#controlsTimesheetItems').hide();
            $('#controlExpenseItems').show();
            return false;
        });

        $('#buttonSelectTimesheetEntries').click(function() {
            if ($('#modalInvoiceItemType').val() == 'expense') {
                /*
                 * Clear the modal contents
                 */
                $('#modalInvoiceItemsContainer').html('');
            }
            if ($('#clientIDHolder').val() == '' || $('#projectID').val() == '0') {
                alert('Please select a client and project first.');
                return false;
            }
            
            $('#modalInvoiceItems').dialog('open').dialog('option','title',commonTerms['selectTimesheetEntries']);
            $('#modalInvoiceItemType').val('timesheet');
            $('#controlsTimesheetItems').show();
            $('#controlExpenseItems').hide();
            return false;
        });

        /*
         * Init invoice item selection modal
         */
        $('#modalInvoiceItems').dialog({
        	autoOpen:      false,
        	closeOnEscape: true,
        	width:         '600px',
        	position:      'center',
			resizable:     false,
			zIndex:        1000,
            close:         function(event,ui) {
                invoiceSupportObj.clearInvoiceItemModals();
            }
        });

        /*
         * Event handlers for timesheet and expense modal windows.
         */
        if (permJSON['expenseViewAll'] == 0 && permJSON['expenseViewOwn'] == 0) {
            $('#buttonSelectExpenses').hide();
        }
        if (permJSON['timesheetViewAll'] == 0 && permJSON['timesheetViewOwn'] == 0) {
            $('#buttonSelectTimesheetEntries').hide();
        }
        
        $("#sExpenseAutocompleter").autocomplete({
            source: siteURL+'finances/ExpenseView/searchExpenses',
            select: function(event, ui) {
                var group = ui.item.type;
                var itemID = ui.item.id;
                
                $("#sExpenseAutocompleter").val('');
			
                $.ajax({
                    type:  "POST",
                    url:   siteURL+'finances/ExpenseView/getExpenseList/'+itemID+'/'+group+'/0/all/json/0',
                    dataType: 'json',
                    success: function(payload){
                        var expenseEntries = payload.Expenses;
                        invoiceSupportObj.populateAvailableItemsInModal(expenseEntries, 'expense');
                    },
                    error: function (xhr, desc, exceptionobj) {
                        errorObj.ajaxError(xhr,desc,exceptionobj);
                    }
                });
           }
        })
        .data('autocomplete')._renderItem = function(ul,item) {
            var itemCount = item.count;
            if (item.count>0) {
                itemCount = '['+item.count+']';
            }
            return $('<li></li>')
                .data('item.autocomplete', item)
                .append('<a><span class="'+item.icon+'">'+item.label+' '+itemCount+'</span></a>')
                .appendTo(ul);
        };

        $('#buttonSearchTimesheets').click(function() {
            var dateStart    = $('#dateStartTimesheet').val();
            var dateEnd      = $('#dateEndTimesheet').val();
            var teamMemberID = $('#teamMemberTimesheet').val();
            var clientID     = $('#clientIDHolder').val();
            var projectID    = $('#projectID').val();

            var formHash = {
                dateStart : dateStart,
                dateEnd   : dateEnd
            }
            $.ajax({
                type:        'POST',
                url:         siteURL+'timesheets/TimesheetView/getTimesheetRecords/0/0/'+teamMemberID+'/'+clientID+'/'+projectID+'/0/json/0/1',
                data:        formHash,
                dataType:    'json',
                success: function(payload) {
                    var timesheetEntries = [];
                    if (typeof payload.Clients != 'undefined' && payload.Clients.length > 0) {
                        timesheetEntries = payload.Clients[0].Projects[0].Entries;
                    }
                    invoiceSupportObj.populateAvailableItemsInModal(timesheetEntries, 'timesheet');
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        });

        $('#buttonSearchExpenses').click(function() {
            var dateStart = Date.parse($('#dateStartExpense').val());
            var dateEnd   = Date.parse($('#dateEndExpense').val());
            dateStart = dateStart.toString('yyyy-MM-dd');
            dateEnd   = dateEnd.toString('yyyy-MM-dd');

            var formHash = {}
            if (dateStart != '' || dateEnd != '') {
                formHash = {
                    filter    : 1,
                    dateStart : dateStart,
                    dateEnd   : dateEnd
                }
            }
            $.ajax({
                type:  "POST",
                url:   siteURL+'finances/ExpenseView/getExpenseList/'+numbers[1]+'/0/0/all/json/0',
                data: formHash,
                dataType: 'json',
                success: function(payload){
                    var expenseEntries = payload.Expenses;
                    invoiceSupportObj.populateAvailableItemsInModal(expenseEntries, 'expense');
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });

            return false;
        });

        $('#buttonSaveInvoiceItemsModal').click(function() {
            var itemType = $('#modalInvoiceItemType').val();
            var selectedArray = [];
            //var a = 0;
            if (itemType == 'expense') {
                $('.invoiceItemSelect').each(function() {
                    if ($(this).is(':checked')) {
                        selectedArray.push({"ItemID":$(this).val(),"Title":$(this).parent().parent().find('.title').html().stripTags(),"Amount":$(this).parent().parent().find('.amount').html()});
                    }
                });
            } else if (itemType == 'timesheet') {
                $('.invoiceItemSelect').each(function() {
                    var el = $(this);
                    if (el.is(':checked')) {
                        selectedArray.push({"ItemID":el.val(),"Title":'['+el.parent().parent().find('.date').html()+'] '+el.parent().parent().find('.title').html().stripTags(),"Hours":el.parent().parent().find('.hours').html(),"Rate":el.parent().parent().data('rate')});
                    }
                });                
            }
            $('#modalInvoiceItems').dialog('close');   
            invoiceSupportObj.populateInvoiceItemsFromModal(selectedArray, itemType);
            
            return false;
        });

        $('#buttonCloseInvoiceItemsModal').click(function() {
            $('#modalInvoiceItems').dialog('close');
            return false;
        });

		/*
		 * Form changes for INVOICE EDIT.
		 */
		if ($('#clientIDHolder').val()>0) {
			initInvoiceFormObj.getProjectsFromClientID($('#clientIDHolder').val());
		}

		if ($('#invoicePaymentDueDate').val() != '') {
			$('#invoicePaymentDueDate').show();
		}

		if ($('#invoiceLateFeeOther').val() != '') {
			$('#invoiceLateFeeOther').show();
		}

        $('#containerWeek').toggle(false);
        $('#containerMonth').toggle(false);
        $('#containerYear').toggle(false);

        $('#invoiceRecurringWhen').change(function() {
            if ($(this).val() == 'week') {
                $('#containerMonth').hide();
                $('#containerYear').hide();
                $('#containerWeek').toggle(true);
            } else if ($(this).val() == 'month') {
                $('#containerWeek').hide();
                $('#containerYear').hide();
                $('#containerMonth').toggle(true);
            } else if ($(this).val() == 'year') {
                $('#containerMonth').hide();
                $('#containerWeek').hide();
                $('#containerYear').toggle(true);
            }
		});

        /*
         * Toggle recurring panels if we already have an invoiceRecurringWhen selection (edit)
         */
        if ($('#invoiceRecurringWhen').val() != 0) {
            if ($('#invoiceRecurringWhen').val() == 'week') {
                $('#containerWeek').toggle(true);
            } else if ($('#invoiceRecurringWhen').val() == 'month') {
                $('#containerMonth').toggle(true);
            } else if ($('#invoiceRecurringWhen').val() == 'year') {
                $('#containerYear').toggle(true);
            }
        }
	},
	getProjectsFromClientID : function(clientID) {
		/*
		 * Get projects for this client
		 */
		$.ajax({
			type:     "POST",
			url:      siteURL+'projects/ProjectView/getProjectsForSelect/'+clientID,
            dataType: 'json',
			success: function(payload) {
				jsonObject = payload;
				var optionString;
				/*
				 * Clear select first
				 */
				$('#projectID').html('<option value="0">'+commonTerms['select']+'</option>');
				for(var a=0;a<jsonObject.length;a++) {
					optionString = '<option value="'+jsonObject[a].ProjectID+'">'+jsonObject[a].Title+'</option>';
					$(optionString).appendTo('#projectID');
				}
				if (jsonObject.length>0) {
					$('#projectIDContainer').show();
					$('#projectID').focus();
				} else {
					$('#invoiceNumber').focus();
				}
                if ($('#projectIDHolder').val()>0) {
                    $('#projectID').val($('#projectIDHolder').val());
                }
			},
			error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
		});
	},
    getEmailFromClientID : function(clientID) {
        $.get(siteURL+'clients/ClientView/getClientEmail/'+clientID+'/json/0', function(payload) {
			if (payload != '') {
                var jsonObject = eval('('+payload+')');
                $('.emailInvoiceTo').val(jsonObject.ClientEmail);
                $('#invoiceEmail').val(jsonObject.ClientEmail);
                $('#clientLanguage').val(jsonObject.Language);
            }
		});
    },
	attachInvoiceItemEventHandlers : function() {
		$('.invoiceItemTotal').unbind('keydown');
		$('.invoiceItemCalculate').unbind('blur');
		$('.rowDelete').unbind('click');

		$('.invoiceItemTotal:last').bind('keydown', function(e) {
            /*
             * If we tab off of the last invoiceItemTotal, then create a new invoice item row
             */
			if (e.keyCode == 9) {
				var itemTotal = $(this).val();
				itemTotal = parseFloat(itemTotal.replace(',',''));
				if (itemTotal != 0) {
					var rowID = $(this).attr('id');
					var rowNumber = initInvoiceFormObj.getItemRowNumberFromID(rowID);
					invoiceCalculationObj.calculateInvoiceItemTotal(rowNumber);
					initInvoiceFormObj.createInvoiceItemRow();
					return false;
				}
			}
		});

		$('.rowAdd').click(function() {
			var rowID = $(this).attr('id');
			var rowNumber = initInvoiceFormObj.getItemRowNumberFromID(rowID);
			invoiceCalculationObj.calculateInvoiceItemTotal(rowNumber);
			initInvoiceFormObj.createInvoiceItemRow();
			return false;
		});

		$('.rowDelete').click(function() {
			var rowID = $(this).attr('id');
			var rowNumber = initInvoiceFormObj.getItemRowNumberFromID(rowID);
			invoiceCalculationObj.calculateInvoiceItemTotal(rowNumber);
			initInvoiceFormObj.removeInvoiceItemRow(rowNumber);
			return false;
		});
		
		$('.buttonInvoiceItemAdd').unbind('click').click(function() {
			initInvoiceFormObj.createInvoiceItemRow();
		});

		$('.invoiceItemCalculate').each(function() {
			$(this).blur(function() {
				var rowID = $(this).attr('id');
				var rowNumber = initInvoiceFormObj.getItemRowNumberFromID(rowID);
				invoiceCalculationObj.calculateInvoiceItemTotal(rowNumber);
			});
		});

		$('.invoiceItemTax').click(function() {
			invoiceCalculationObj.calculateInvoiceTotal();
		});
	},
	createInvoiceItemRow : function() {
		var rows = $('#invoiceItemTableBody tr').size();
		var newRowNumber = rows;

		var itemRowHTML = '<tr class="invoiceItemRow" id="itemRow_'+newRowNumber+'">'+itemRowHTMLTemplate+'</tr>';
		$('#invoiceItemTableBody').append(itemRowHTML);

        if (tabIndex<1) {
            tabIndex = $('.invoiceItemControl').last().attr('tabindex');
        }
		/*
		 * Clear all row form fields
		 */
		//initInvoiceFormObj.clearRowFormFields(newRowNumber);

		/*
		 * Assign row number to form field attributes
		 */		
		tabIndex++;
        $('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemDesc')
			.attr('id','desc_'+newRowNumber)
			.attr('tabindex', tabIndex)
			.attr('name', 'desc_'+newRowNumber)
			.elastic()
			.val('');
		$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemExtra')
			.attr('id','extra_'+newRowNumber)
			.attr('name', 'extra_'+newRowNumber)
			.val('');

		tabIndex++;
		$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemType')
			.attr('id','type_'+newRowNumber)
			.attr('tabindex', tabIndex)
			.attr('name', 'type_'+newRowNumber)
			.val('');

		tabIndex++;
		$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemQty')
			.attr('id','qty_'+newRowNumber)
			.attr('tabindex', tabIndex)
			.attr('name', 'qty_'+newRowNumber)
			.val('');

		tabIndex++;
		$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemRate')
			.attr('id','rate_'+newRowNumber)
			.attr('tabindex', tabIndex)
			.attr('name', 'rate_'+newRowNumber)
			.val('');
            
        tabIndex++;
		$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemTax')
			.attr('id','tax_'+newRowNumber)
			.attr('tabindex', tabIndex)
			.attr('name', 'tax_'+newRowNumber);

		if (haveTax == true) {
			$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemTax')
				.attr('checked',true)
				.removeAttr('disabled');
		}    

		tabIndex++;
		$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemTotal')
			.attr('id','total_'+newRowNumber)
			.attr('tabindex', tabIndex)
			.attr('name', 'total_'+newRowNumber)
			.val('');

		tabIndex++;
		$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemControl')
			.attr('id','control_'+newRowNumber)
			.attr('tabindex', tabIndex);

		var prevRowNumber = newRowNumber-1;
		$('#invoiceItemTableBody tr:eq('+prevRowNumber+') .invoiceItemControl')
			.removeClass('rowAdd')
			.addClass('rowDelete');

		$('#invoiceItemTableBody tr:eq('+prevRowNumber+') .invoiceItemControl span')
			.removeClass('add')
			.addClass('delete');

		initInvoiceFormObj.attachInvoiceItemEventHandlers();

		$('#invoiceItemTableBody tr:eq('+newRowNumber+') .invoiceItemDesc').focus();
		$('#invoiceItemCount').val(newRowNumber);
	},
	removeInvoiceItemRow : function(rowNumber) {
		$('#itemRow_'+rowNumber).fadeOut('fast', function() {
            $(this).remove();
        });
		$('#total_'+rowNumber).val(0);
		invoiceCalculationObj.calculateInvoiceTotal();
	},
	checkTaxStatus : function() {
        if ($('#invoiceTaxRate').val()>0) {
            $('.invoiceItemTax').removeAttr('disabled');
            if ($('#action').val() == 'add') {
                $('.invoiceItemTax').attr('checked',true);
            }
			$('#rowTax').show();
			haveTax = true;
			/*
			 * Recalculate invoice total with sales tax
			 */
			invoiceCalculationObj.calculateInvoiceTotal();
		} else {
			$('.invoiceItemTax').attr('disabled',true).attr('checked',false);
			$('#rowTax').hide();
			haveTax = false;
		}
	},
	checkShipping : function() {
		if ($('#invoiceShipping').val()>0) {
			$('#rowShipping').show();
			haveShipping = true;			
		} else {
			$('#rowShipping').hide();
			haveShipping = false;
		}
		/*
		 * Recalculate invoice total with shipping cost
		 */
		invoiceCalculationObj.calculateInvoiceTotal();
	},
	checkDiscount : function() {
		if ($('#invoiceDiscount').val()>0) {
			$('#rowDiscount').show();
			haveDiscount = true;
			/*
			 * Recalculate invoice total with discount percentage
			 */
			invoiceCalculationObj.calculateInvoiceTotal();
		} else {
			$('#rowDiscount').hide();
			haveDiscount = false;
		}
	},
	getItemRowNumberFromID : function(rowID) {
		var rowNumberArray = rowID.split('_');
		var rowNumber = rowNumberArray[1];
		return rowNumber;
	}
}

var invoiceSupportObj = {
    clearInvoiceItemModals : function() {
        $('#dateStartTimesheet').val('');
        $('#dateEndTimesheet').val('');
        $('#dateStartExpense').val('');
        $('#dateEndExpense').val('');
        $('#sExpenseAutocompleter').val('');
        $('#sExpenseID').val('');
    },
    populateAvailableItemsInModal : function(itemArray,itemType) {
        var htmlOut = [];
        htmlOut.push('<table class="dataTable"><thead><tr>');
        if (itemType == 'timesheet') {
            htmlOut.push('<th style="width: 25px; padding-left: 6px;"><input type="checkbox" id="invoiceItemSelectMaster" /></th><th style="width: 220px;">'+commonTerms['task_comments']+'</th><th style="text-align: right; width: 50px;">'+commonTerms['hours']+'</th><th style="width: 100px;">'+commonTerms['teamMember']+'</th><th style="text-align: right; width: 100px;">'+commonTerms['date']+'</th></tr></thead>');
            if (itemArray.length == 0) {
                htmlOut.push('<tr><td colspan="5"><div class="errorMessageSmall">'+commonTerms['no_records']+'</div></td></tr>');
            } else {
                var itemCount = 0;
                for (var a in itemArray) {
                    var item = itemArray[a];

                    var disabled = '', clickable = 'clickable', billedNotice = '';
                    if ( item['InvoiceID'] > 0 ) {
                        disabled = 'disabled="disabled"';
                        clickable = '';
                        billedNotice = '<span class="subText colorBackground label light-blue">Billed invoice ' + item['InvoiceNo'] + ' on ' + item['InvoiceDate'] + '</span>'
                    } else if ( item['Billable'] == 1 ) {
                        billedNotice = '<span class="colorBackground label light-green">' + commonTerms['billable'] + '</span>'
                    }
                        
                    var taskComments = '', taskTitle = '';
                    if ( item['Comments'] != '' && item['Comments'] != null ) {
                        taskComments = item['Comments'] + '<br />';
                    }

                    if ( item['TaskTitle'] != '' && item['TaskTitle'] != null ) {
                        taskTitle = '<strong>' + item['TaskTitle'] + '</strong><br />';
                    }

                    var rateString = '';
                    if (item['TeamMemberRate'] != '' && item['TeamMemberRate'] != null && parseInt(item['TeamMemberRate']) > 0) {
                        rateString = '<br /><span class="subText">('+commonTerms['currencySymbol']+item['TeamMemberRate']+' / hr)</span>';
                    }

                    htmlOut.push('<tr class="invoiceItemRow '+clickable+'" data-rate="'+item['TeamMemberRate']+'" data="'+item['TimeSheetID']+'" id="invoiceItemRow'+item['TimeSheetID']+'">');
                    htmlOut.push('<td><input type="checkbox" class="invoiceItemSelect" id="invoiceItemSelect'+item['TimeSheetID']+'" value="'+item['TimeSheetID']+'" '+disabled+' /></td>');
                    htmlOut.push('<td class="title">' + taskTitle + taskComments + billedNotice + '</td>');
                    htmlOut.push('<td class="hours" style="text-align: right;">'+item['ElapsedTime']+'</td>');
                    htmlOut.push('<td class="teamMember">'+item['TeamMember']+' '+rateString+'</td>');
                    htmlOut.push('<td class="date" style="text-align: right;">'+item['DateClockEnd']+'</td>');
                    htmlOut.push('</tr>');
                    itemCount++;
                }
                if (itemCount == 0) {
                    htmlOut.push('<tr><td colspan="5"><div class="errorMessageSmall">'+commonTerms['no_records']+'</div></td></tr>');
                }
            }
        } else if (itemType == 'expense') {
            htmlOut.push('<th style="width: 25px; padding-left: 6px;"><input type="checkbox" id="invoiceItemSelectMaster" /></th><th style="width: 270px;">'+commonTerms['description']+'</th><th style="width: 100px; text-align: right;">'+commonTerms['amount']+'</th><th style="width: 100px;">'+commonTerms['date']+'</th></tr></thead>');
            if (itemArray.length == 0) {
                htmlOut.push('<tr><td colspan="4"><div class="errorMessageSmall">'+commonTerms['no_records']+'</div></td></tr>');
            } else {
                var itemCount = 0;
                for (var a in itemArray) {
                    var item = itemArray[a];
                    var disabled = '', clickable = 'clickable', billedNotice = '';
                    if ( item['InvoiceID'] > 0 ) {
                        disabled = 'disabled="disabled"';
                        clickable = '';
                        billedNotice = '<br /><span class="subText colorBackground label light-blue">Billed invoice ' + item['InvoiceNo'] + ' on ' + item['InvoiceDate'] + '</span>'
                    } else if ( item['Reimbursement'] == 'yes' ) {
                        billedNotice = '&nbsp;&nbsp;<span class="colorBackground label light-green">' + commonTerms['billable'] + '</span>'
                    }

                    htmlOut.push('<tr class="invoiceItemRow '+clickable+'" data="'+item['ExpenseID']+'" id="invoiceItemRow'+item['ExpenseID']+'">');
                    htmlOut.push('<td><input type="checkbox" class="invoiceItemSelect" id="invoiceItemSelect'+item['ExpenseID']+'" value="'+item['ExpenseID']+'" '+disabled+'" /></td>');
                    htmlOut.push('<td class="title">'+item['Title']+billedNotice+'</td>');
                    htmlOut.push('<td style="text-align: right;">'+commonTerms['currencySymbol']+'<span class="amount">'+item['Amount']+'</span></td>');
                    htmlOut.push('<td class="date">'+item['DateExpense']+'</td>');
                    htmlOut.push('</tr>');
                    itemCount++;
                }
                
                if (itemCount == 0) {
                    htmlOut.push('<tr><td colspan="4"><div class="errorMessageSmall">'+commonTerms['no_records']+'</div></td></tr>');
                }
            }
        }

        htmlOut.push('</table>');
        var htmlString = htmlOut.join('');
        $('#modalInvoiceItemsContainer').html(htmlString);

        $('#modalInvoiceItemsContainer')
			.alternateTableRowColors()
			.hiliteTableRow()
			.tableSorter();

        $('#modalInvoiceItemsContainer').delegate('td.title', 'click', function() {
            var chk = $(this).parent().find('.invoiceItemSelect');
            if (chk.is(':checked')) {
                chk.attr('checked', false);
            } else {
                chk.attr('checked', true);
            }
            return false;
        });
        
        $('#invoiceItemSelectMaster').click(function() {
           $this = $(this);
           if ( $this.is(':checked') ) {
               $('.invoiceItemSelect').attr('checked', true);
           } else {
               $('.invoiceItemSelect').attr('checked', false);
           }
        });
    },
    populateInvoiceItemsFromModal : function(selectedArray,itemType) {
        /*
         * How many invoice item rows do we already have?
         */
        var invoiceItemRows = $('#invoiceItemTableBody tr').length;
        var b = invoiceItemRows-1;
        var totalItems = selectedArray.length;
        if (itemType == 'timesheet') {
            for (var a in selectedArray) {
                var itemID = selectedArray[a]['ItemID'];
                var title  = selectedArray[a]['Title'];
                var hours  = selectedArray[a]['Hours'];
                var rate   = selectedArray[a]['Rate'];
                $('#desc_'+b).val(title);
                $('#qty_'+b).val(hours);
                $('#extra_'+b).val('timesheet_'+itemID);
                $('#rate_'+b).val(rate);
                invoiceCalculationObj.calculateInvoiceItemTotal(b);
                initInvoiceFormObj.createInvoiceItemRow();
                b++;
            }
        } else if (itemType == 'expense') {
            for (a in selectedArray) {
                var itemID = selectedArray[a]['ItemID'];
                var title  = selectedArray[a]['Title'];
                var amount = selectedArray[a]['Amount'];

                $('#desc_'+b).val(title);
                $('#qty_'+b).val('1');
                $('#type_'+b).val('product');
                $('#rate_'+b).val(amount);
                $('#total_'+b).val(amount);
                $('#extra_'+b).val('expense_'+itemID);
                initInvoiceFormObj.createInvoiceItemRow();
                b++;
            }
            invoiceCalculationObj.calculateInvoiceTotal();
        }
    }
}

var invoiceCalculationObj = {
	calculateInvoiceItemTotal : function(rowNumber) {
		var qty  = parseFloat($('#qty_'+rowNumber).val());
		var rate = parseFloat($('#rate_'+rowNumber).val());
		if (qty>0 && rate != 0) {
			$('#total_'+rowNumber).val(parseFloat(qty*rate));
		}
		$('#total_'+rowNumber).formatCurrency({symbol: ''});

		/*
		 * Now calculate the invoice total
		 */
		invoiceCalculationObj.calculateInvoiceTotal();
	},
	calculateInvoiceTotal : function() {
		var invoiceTotal = 0.00;
		var taxableTotal = 0.00;
		$('.invoiceItemTotal').each(function() {
			var itemTotal = $(this).val().replace(/,/gi,'');
			var itemTotal = parseFloat(itemTotal);
			if (itemTotal != 0 && !isNaN(itemTotal) ) {
				var rowID = $(this).attr('id');
				var rowNumber = initInvoiceFormObj.getItemRowNumberFromID(rowID);

				if ($('#tax_'+rowNumber).is(':checked')) {
					taxableTotal = parseFloat(taxableTotal+itemTotal);
				}
				invoiceTotal = parseFloat(invoiceTotal+itemTotal);
			}
		});
		
		$('#amountSubTotal').html(invoiceTotal);
		$('#amountSubTotal').formatCurrency({symbol: '',useHtml: true});
		$('#amountSubTotalHidden').val(invoiceTotal);

		if (haveDiscount == true) {
			var discountAmount = invoiceCalculationObj.calculateDiscount(invoiceTotal);
			$('#amountDiscount').html(discountAmount);
			$('#amountDiscount').formatCurrency({symbol: '',useHtml: true});
			invoiceTotal = parseFloat(invoiceTotal-discountAmount);
			taxableTotal = parseFloat(taxableTotal-discountAmount);
			$('#amountDiscountHidden').val(discountAmount);
		}

		if (haveShipping == true) {
			var shippingAmount = parseFloat($('#invoiceShipping').val());
			$('#amountShipping').html(shippingAmount);
			$('#amountShipping').formatCurrency({symbol: '',useHtml: true});
			invoiceTotal = parseFloat(invoiceTotal+shippingAmount);
			taxableTotal = parseFloat(taxableTotal+shippingAmount);
			$('#amountShippingHidden').val(shippingAmount);
		}

		if (haveTax == true) {
			var taxAmount = invoiceCalculationObj.calculateItemSalesTax(taxableTotal);
			$('#amountTax').html(taxAmount);
			$('#amountTax').formatCurrency({symbol: '',useHtml: true});
			invoiceTotal = parseFloat(invoiceTotal+taxAmount);
			$('#amountTaxHidden').val(taxAmount);
		}

		$('#totalCell').html(invoiceTotal);
		$('#totalCell').formatCurrency({symbol: commonTerms['currencySymbol'],useHtml: true});
		$('#invoiceTotalHidden').val(invoiceTotal);
	},
	calculateItemSalesTax : function(itemTotal) {
		var taxRate = parseFloat($('#invoiceTaxRate').val());
		if (taxRate>0) {
			var actualTaxRate = parseFloat(taxRate/100);
			var itemTaxAmount = parseFloat(actualTaxRate*itemTotal);
			return itemTaxAmount;
		} else {
			return false;
		}
	},
	calculateDiscount : function(invoiceTotal) {
		var discountRate = parseFloat($('#invoiceDiscount').val());
		if (discountRate>0) {
			var actualDiscountRate = parseFloat(discountRate/100);
			var discountAmount = parseFloat(actualDiscountRate*invoiceTotal);
			return discountAmount;
		} else {
			return false;
		}
	}
}