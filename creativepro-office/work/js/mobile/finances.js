cpo.finances = new Ext.Panel({
    title: 'Finances',
    iconCls: 'iconTabFinances',
    dockedItems: [
        {
            xtype: 'toolbar',
            title: '<span class="headerIcon"></span>',
            dock: 'top',
            items: [{
                text: 'JSONP'
            },
            {
                xtype: 'spacer'
            },
        {
            text: 'XMLHTTP'
        }]
    }]
});