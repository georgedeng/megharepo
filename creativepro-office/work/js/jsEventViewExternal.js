var eventViewObj = {
	init : function() {
		$('#buttonprint').click(function() {
			$('.stuff').each(function() {
				var content = $(this).attr('title')+$(this).html();
				$(this).html(content);
			});
			window.print();
		});

		$('#buttonlogin').click(function() {
			window.location = siteURL+'login';
		});

        var address = $('#mapGoogleAddressContainer').html();
        googleMapObj.showGoogleMap(address, 'mapContainer', address);
	}
}

$(document).ready(function() {
	eventViewObj.init();
});