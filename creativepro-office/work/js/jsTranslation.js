var translationObj = {
	translateFile : function() {
		var file = $('#translateFile').val();
		var lang = $('#translateLanguage').val();

		$('#resultsDiv').systemMessage({
			status: 'wait',
			message: 'Translation in progress...'
		});

		$.ajax({
		    type: "POST",
		    url:  siteURL+'administration/AdminTranslation/translateFile/'+file+'/'+lang,
		    success: function(payload){
		    	$('#resultsDiv').systemMessage({
					status:  'success',
					message: payload,
					size:    'Big',
					noFade:  1
				});
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	}
}

$(document).ready(function() {
	$('#buttonTranslate').click(function() {
		translationObj.translateFile();
	});
});