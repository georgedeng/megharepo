var invoiceViewObj = {
	init : function() {
		$('#buttonprint').click(function() {
            window.print();
            //printObj.goPrint($('#invoiceID').val(), 'Invoice');
		});

		$('#buttonlogin').click(function() {
			window.location = siteURL+'login';
		});
        
        var invoiceItemName = 'Invoice '+$('#invoiceNumber').val()+' for '+$('#invoiceVendor').val();
        $('#item_name').val(invoiceItemName);

        $('#buttonMakePayPalPayment').click(function() {
            var formHash = {
                amount       : $('#amount').val(),
                business     : $('#business').val(),
                item_name    : $('#item_name').val(),
                payPalReturn : $('#returnURL').val(),
                cancel_return: $('#cancel_return').val(),
                currency_code: $('#currency_code').val(),
                invoiceID    : $('#invoiceID').val()
            };

            $.ajax({
                type:  "POST",
                url:   siteURL+'finances/InvoiceViewExternal/makePayPalPayment',
                data: formHash,
                success: function(payload){
                    window.location = payload;
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
            return false;
        });
	}
}

$(document).ready(function() {
	invoiceViewObj.init();
});

