// Handles page select menu and prev/next buttons
function gotoPage(newPage) {
	var sort = $('sortFilterBy').value;
	var acdc = $('sortFilterAcDc').value;
	var limit = '';
	var startPoint = '';
	var search = '';
	x_getCurrentClients(sort,acdc,limit,startPoint,newPage,search,returnGotoPage);
}

function returnGotoPage(stuff) {
	// Parse returned stuff
	var stringArray = stuff.split("~~|~~");
	var clientDisplay = stringArray[0];
	var pages = stringArray[1];
	var paginate1 = stringArray[2];
	var paginate2 = stringArray[3];
	var totalClients = stringArray[4];
	
	$('clientDisplay').innerHTML = clientDisplay;
	$('paginate1').innerHTML = paginate1;
	$('paginate2').innerHTML = paginate2;
	$('totalClients').innerHTML = '('+totalClients+')';
}

// Handles records per page select menu
function recordsPerPage(newRecordsPerPage) {
	var sort = $('sortFilterBy').value;
	var acdc = $('sortFilterAcDc').value;
	var limit = newRecordsPerPage;
	var startPoint = '';
	var search = '';
	var newPage = '';
	x_getCurrentClients(sort,acdc,limit,startPoint,newPage,search,returnRecordsPerPage);
}

function returnRecordsPerPage(stuff) {
	// Parse returned stuff
	var stringArray = stuff.split("~~|~~");
	var clientDisplay = stringArray[0];
	var pages = stringArray[1];
	var paginate1 = stringArray[2];
	var paginate2 = stringArray[3];
	var totalClients = stringArray[4];
	
	$('clientDisplay').innerHTML = clientDisplay;
	$('paginate1').innerHTML = paginate1;
	$('paginate2').innerHTML = paginate2;
	$('totalClients').innerHTML = '('+totalClients+')';
}

function viewClientItems(clientID,what) {
	// Get window width
	var winWidth = $('infoWindow').style.width;
	if (what == 'stats') {
		x_viewClientItems(clientID,what,returnViewClientItems);
	} else if (what == 'map') {
		$('infoWindow').innerHTML = '<center><div id="map" style="width: '+winWidth+'px; height: 250px;"></div></center>';
		makeMap();
		
		// Change selected menu item background
		$('menuItemMap').className = 'menuItemSelected';
		$('menuItemStats').className = 'menuItemNotSelected';
		$('menuItemMessages').className = 'menuItemNotSelected';
	} else if (what == 'messages') {
		$('infoWindow').innerHTML = '<br /><br />Client message center coming soon.<br /><br /><br />';
		
		// Change selected menu item background
		$('menuItemMap').className = 'menuItemNotSelected';
		$('menuItemStats').className = 'menuItemNotSelected';
		$('menuItemMessages').className = 'menuItemSelected';
	}
}

function returnViewClientItems(string) {
	$('infoWindow').innerHTML = string;
	
	// Change selected menu item background
	$('menuItemStats').className = 'menuItemSelected';
	$('menuItemMap').className = 'menuItemNotSelected';
	$('menuItemMessages').className = 'menuItemNotSelected';
}

function sortFilter(sortColumn,acdc) {		
	// Check for records per page
	// First see if the records per page pull-down even exists
	if ( $('recordsPerPage1') ) {
		var rppID    = $('recordsPerPage1');
		var rppIndex = rppID.selectedIndex;
		var rppID    = rppID[rppIndex].value;
	} else {
		rppID =  0;
	}
	if ( $('pageNo1') ) {
		// Get page
		var pageID    = $('pageNo1');
		var pageIndex = pageID.selectedIndex;
		var pageID    = pageID[pageIndex].value;
	} else {
		pageID = 0;
	}	
	$('sortFilterBy').value = sortColumn;	
	$('sortFilterAcDc').value = acdc;	
	
	x_getCurrentClients(sortColumn,acdc,rppID,'',pageID,'',returnRecordsPerPage)
}

function returnClientDelete(clientID) {
	var clientRow = $('clientRow_'+clientID);
	var clientMenu = $('clientMenu_'+clientID);
	clientRow.style.display = 'none';	
}

function setSelects(country) {
	// This function sets the country select menu
	var countrySelect  = $('country');
	var countryValue   = '';
	
	// Loop through all items in select and find a match
	for (var a=0; a < countrySelect.length; a++) { 
		countryValue = countrySelect.options[a].value;
		if (countryValue == country) {
			countrySelect.options[a].selected = true;
		}
	}
}

function returnClientViewDelete() {
	$('clientDisplay').innerHTML = '<div style="padding: 10px;"><span class="bigText">This client has been deleted. <a href="index.php">View your other clients.</a></span></div>';
	$('clientStuffDisplay').innerHTML = '';
}

function clientDelete(clientID,fromView) {
	if (fromView == 1) {
		x_clientDelete(clientID,returnClientViewDelete);
	} else {
		x_clientDelete(clientID,returnClientDelete);
	}	
}