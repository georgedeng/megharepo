// ***** FORMAT CURRENCY ************************************************** //
function formatCurrency(amount)
{
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

// ***** Remove bad characters from input strings ************************** //
function removeChar(str) {
	charToRemove = '"';
	regExp = new RegExp("["+charToRemove+"]","g");
	return str.replace(regExp,"");
}

// ***** getSelected selects the correct drop-down menu items in an edit screen
// ***** passed the javascript variable and the select id
function getSelect(variable,selectID) {
	var selectMenu    = $(selectID);
	var selectOptions = selectMenu.length-1;
	var a=0;
	for(a=0;a<=selectOptions;a++) {
		if (selectMenu.options[a].value == variable) {
			$(selectID).selectedIndex = a;
		}
	}
}

// ***** Verify that an email addres is valid ******************************* //
// ***** Written by Paolo Wales (paolo@taize.fr) **************************** //

function isValidEmail(emailad) {

   var exclude=/[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
   var check=/@[\w\-]+\./;
   var checkend=/\.[a-zA-Z]{2,4}$/;
   if(((emailad.search(exclude) != -1) ||
       (emailad.search(check)) == -1) ||
       (emailad.search(checkend) == -1)){
      return false;
   } else {
      return true;
   }
}

// checkMouseLeave allows for window close onMouseOut of window div
function checkMouseLeave(element, evt) {
	evt = (evt) ? evt : ((window.event) ? window.event : "");
	window.status = evt;
	if (evt.relatedTarget) {
		return !containsDOM(element, evt.relatedTarget);
	} else {
		if (element.contains(evt.toElement)) {
			return(false);
		} else {
			return(true);
		}
	}
}

// containsDOM 
function containsDOM(container, containee) {
	var isParent = false;
	do {
		if ((isParent = container == containee))
		break;
		containee = containee.parentNode;
	}
	while (containee != null);

	return isParent;
}

// ***** Generic open window function ******************************************* //
function openWindow(content,displayDiv,width,height,close,background,headTitle,imagePath) {		
	if(width == 0) { width = '100%'; }						
	var winContent = content;
	
	output = '<div class="popUpWindow" id="ABBA'+displayDiv+'" style="text-align: left; display: none; width: '+width+';';
	if (height > 0) { output += ' height: '+height+';">'; }
	else { output += '">'; }
	
	if (close) {
		output += '<div style="text-align: right;"><a href="#" onClick="closeWindow(\''+displayDiv+'\'); return false;" title="Close"><img src="http://www.creativeprooffice.com/apps/images/buttonCloseSmall.gif" style="border: none;" alt="Close"></a></div>';
		output += '<div style="height: 5px;"></div>';
	}
	output += '<div id="windowContent" style="text-align: left;">'+winContent+'</div></div>';	
	
	$(displayDiv).innerHTML = output;
	$(displayDiv).style.marginRight = '100px';
	new Effect.Grow('ABBA'+displayDiv, {direction: 'top', duration: .2});	
	
	if (!close) {
		var nav = $("windowContent").style;
	    var con = $(displayDiv);
	    con.onmouseout = function(evt) {
			if (checkMouseLeave(this, evt)) {
				closeWindow(displayDiv);
			}
	    }
	}		
}
function closeWindow(displayDiv) {
	new Effect.Shrink('ABBA'+displayDiv, {direction: 'top-left', duration: .2 });
}

// ***** Open/Close Action Menu Windows *****
function openMenu(content,displayDiv,width) {
	var menuDiv = 'menuPopup_'+displayDiv;		
	$(menuDiv).style.display = 'block';
	$(menuDiv).style.width = width+'px';
	$(menuDiv).style.display = '';
	
	// Check for no content
	if (content == '0') {
		var content = $('menuContent_'+displayDiv).innerHTML;		
	}
	
	output  = '<div class="popUpMenu" style="width: '+width+';">';
	output += '<div id="windowContent" class="popUpMenuContent">'+content+'</div></div>';	
	
	$(menuDiv).innerHTML = output;
	
	var nav = $("windowContent").style;
    var con = $(menuDiv);
    con.onmouseout = function(evt) {
		if (checkMouseLeave(this, evt)) {
			//var nav = $(displayDiv).style;
			//nav.display = 'none';
			closeMenu(menuDiv);
		}
    }	
}

function closeMenu(displayDiv) {
	$(displayDiv).style.display = 'none';
	$(displayDiv).innerHTML = '';
}
// ***** Open/Close info divs in grids
function openInfoDiv(infoDivID) {
	var infoDiv = $(infoDivID);
	if (infoDiv.style.display == 'none') {
		infoDiv.style.display = 'block';
	} else {
		infoDiv.style.display = 'none';
	}
}

function showMenu(element,itemID,itemNo,what) {
	// Look for ID's in hidden fields
	var qsClientID = '';
	var qsProjID   = '';
	var qsInvID    = '';
	var qsQuoteID  = '';
	var q          = '';
	
	if ($('clientID') != null) {
		qsClientID = 'clientID='+$('clientID').value;
		q = '?';
	}
	if ($('projID') != null) {
		qsProjID = 'projID='+$('projID').value;
		q = '?';
	}
	if ($('invID') != null) {
		qsInvID = 'invID='+$('invID').value;
		q = '?';
	}
	var winElement = element;
	if (what == 'Clients') {	
		var winContent = '<ul><li onClick="window.location=\'clientView.php?clientID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/iconViewSmall.gif" alt="View" /> View</li>';
		winContent    += '<li onClick="window.location=\'clientEdit.php?clientID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/button_editSmall.gif" alt="Edit" /> Edit</a></li>';
		winContent    += '<li onClick="confirmDelete('+itemID+',\'client\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete" /> Delete</li>';
		winContent    += '<li onClick="window.location=\'../clients/clientsAdd.php\'"><img src="http://www.creativeprooffice.com/apps/images/iconAddSmall.gif" alt="New Client" /> New Client</li></ul>';		
	} else if (what == 'Invoices') {
		var winContent = '<ul><li onClick="window.location=\'../finances/invoiceView.php?invID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/iconViewSmall.gif" alt="View" /> View</li>';
		// Is this invoice editable
		var canEdit = $('canEdit_'+itemID).value;
		if (canEdit == 1) {
			winContent    += '<li onClick="window.location=\'../finances/invoiceView.php?edit=1&invID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/button_editSmall.gif" alt="Edit" /> Edit</a></li>';
		}
		winContent    += '<li onClick="confirmDelete('+itemID+',\'invoice\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete" /> Delete</li>';
		winContent    += '<li onClick="window.location=\'../finances/invoiceAdd.php'+q+qsClientID+'\'"><img src="http://www.creativeprooffice.com/apps/images/iconAddSmall.gif" alt="New Invoice" /> New Invoice</li></ul>';
		winContent    += '<hr />';
		winContent    += '<ul><li onClick="emailInvoice('+itemID+',\''+itemNo+'\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconMail.gif" alt="Email Invoice" /> Email Invoice</li>';
		winContent    += '<li onClick="goPrint(\'pdf\','+itemID+'); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconPDFSmall.gif" alt="Download PDF" /> Download PDF</li>';
		winContent    += '<li onClick="goPrint(\'invoices\','+itemID+'); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconPrintSmall.gif" alt="Print" /> Print</li></ul>';
	} else if (what == 'Projects') {
		var winContent = '<ul><li onClick="window.location=\'../projects/projectView.php?projID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/iconViewSmall.gif" alt="View" /> View</li>';
		winContent    += '<li onClick="window.location=\'../projects/projectEdit.php?projID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/button_editSmall.gif" alt="Edit" /> Edit</a></li>';
		winContent    += '<li onClick="confirmDelete('+itemID+',\'project\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete" /> Delete</li>';
		winContent    += '<li onClick="window.location=\'../projects/projectAdd.php'+q+qsClientID+'\'"><img src="http://www.creativeprooffice.com/apps/images/iconAddSmall.gif" alt="New Project" /> New Project</li></ul>';
	} else if (what == 'Tasks') {
		// element,taskID,projID
		var taskID = itemID;
		var projID = itemNo;
		var winContent = '<ul><li onClick="taskAddWindowOpen(\'edit\',\''+taskID+'\',\''+projID+'\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_editSmall.gif" alt="Edit" /> Edit</a></li>';
		winContent    += '<li onClick="confirmDelete('+itemID+',\'task\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete" /> Delete</li>';
		winContent    += '<li onClick="taskAddWindowOpen(); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconAddSmall.gif" alt="New Task" /> New Task</li></ul>';
	} else if (what == 'Milestones') {
		// element,msID,projID
		var msID   = itemID;
		var projID = itemNo;
		var winContent = '<ul><li onClick="mileStoneAddWindowOpen(\'edit\',\''+msID+'\',\''+itemNo+'\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_editSmall.gif" alt="Edit" /> Edit</a></li>';
		winContent    += '<li onClick="confirmDelete('+itemID+',\'milestone\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete" /> Delete</li>';
		winContent    += '<li onClick="taskAddWindowOpen(); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconAddSmall.gif" alt="New Task" /> New Task</li>';
		winContent    += '<li onClick="mileStoneAddWindowOpen(); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconAddMilestone.gif" alt="New Milestone" /> New Milestone</li></ul>';
	} else if (what == 'Blogs') {
		var winContent = '<ul><li onClick="window.location=\'../admin/adminCPOBlogView.php?beID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/iconViewSmall.gif" alt="View" /> View</li>'
		winContent    += '<li onClick="window.location=\'../admin/adminCPOBlogEdit.php?beID='+itemID+'\'""><img src="http://www.creativeprooffice.com/apps/images/button_editSmall.gif" alt="Edit" /> Edit</a></li>';
		winContent    += '<li onClick="confirmDelete('+itemID+',\'blog\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete" /> Delete</li>';
		winContent    += '<li onClick="postPublish('+itemID+'); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconAddSmall.gif" alt="Publish" /> Publish</li></ul>';
	} else if (what == 'Users') {
		var winContent = '<ul><li onClick="window.location=\'../admin/adminCPOUserView.php?uID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/iconViewSmall.gif" alt="View" /> View</li>'
		winContent    += '<li onClick="confirmDelete('+itemID+',\'user\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete" /> Delete</li>';
		winContent    += '<li onClick="viewUserAccount('+itemID+'); return false;"><img src="http://www.creativeprooffice.com/apps/images/ivonViewSmall.gif" alt="View Account" /> View Account</li></ul>';
	} else if (what == 'Bugs') {
		var winContent = '<ul><li onClick="confirmDelete('+itemID+',\'bug\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete" /> Delete</li>';
	} else if (what == 'TaskMenu') {
		var winContent = '<ul><li onClick="mileStoneAddWindowOpen(\'add\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconAddMilestone.gif" alt="Add Milestone" /> Add Milestone</li>';
		winContent    += '<li onClick="taskAddWindowOpen(\'add\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconAddSmall.gif" alt="New Task" /> Add Task</li>';
		winContent    += '<li onClick="goPrint(\'tasks\','+itemID+'); return false;"><img src="http://www.creativeprooffice.com/apps/images/iconPrintSmall.gif" alt="Print" /> Print Task List</li>';
		winContent    += '<li id="gantSwitch" onClick="viewGant('+itemID+'); return false;"> View Gantt Chart</li></ul>';
	} else if (what == 'Expenses') {
		var winContent = '<ul><li onClick="window.location=\'../finances/expenseEdit.php?expID='+itemID+'\'"><img src="http://www.creativeprooffice.com/apps/images/button_editSmall.gif" alt="Edit Expense" /> Edit Expense</li>';
		winContent    += '<li onClick="window.location=\'../finances/expenseAdd.php\'"><img src="http://www.creativeprooffice.com/apps/images/iconAddSmall.gif" alt="Add Expense" /> Add Expense</li>';
		winContent    += '<li onClick="confirmDelete('+itemID+',\'expense\'); return false;"><img src="http://www.creativeprooffice.com/apps/images/button_deleteSmall.gif" alt="Delete Expense" /> Delete Expense</li></ul>';
	}
	openMenu(winContent,winElement,120);
}
// ***** End Open/Close Menu Windows *****

function addItemWin(contentID,display,headTitle) {
	var outp = $(contentID).innerHTML;
	openWindow(outp,display,'','','',headTitle,0,'');
}

// ***** Little pause function just for kicks ******************************************** //
function pause(millis) {
	date = new Date();
	var curDate = null;

	do { var curDate = new Date(); }
	while(curDate-date < millis);
} 

function changeBG1(id) {
	$(id).className='menuLiChange1'
}
function changeBG2(id) {
	$(id).className='menuLiChange2'
}

// ***** GET CATEGORIES (PROJECT,INVOICE,ETC.) ***** //
function getCat(module) {
	x_getCat(module,'edit',returnGetCat);
}

function returnGetCat(string) {
	// Open window and pass string	
	openWindow(string,'catDisplay',250,0,1,'','','../');
}

function addCat(textID,module) {
	var newCat = $(textID).value;
	if (newCat != '') {
		x_addCat(newCat,module,returnAddCat);
	}
}

function returnAddCat(string) {
	var stringArray = string.split("|");
	var selectCat   = stringArray[0];
	var editCat     = stringArray[1];
	
	$('mainCat').innerHTML = selectCat;
	$('windowContent').innerHTML    = editCat;
}

function editCat(catID,module) {
	var editTD  = 'edit'+catID;
	var catText =  $(editTD).innerHTML;
	
	$(editTD).innerHTML = '<input type="text" id="editText'+catID+'" style="width: 171px;" value="'+catText+'" onBlur="editCat2('+catID+',\''+module+'\');">';
}

function editCat2(catID,module) {
	var editText = $('editText'+catID).value;
	x_editCat(editText,catID,module,returnAddCat);	
}

function deleteCat(catID,module) {	
	x_deleteCat(catID,module,returnAddCat);
}

/***** BUG REPORT FUNCTIONS *****/

function openBugReportWindow() {
	var string = '<div id="bugForm"><span id="bugInstructions">Enter description of bug:</span><br />';
	string += '<textarea id="bugText" style="width: 290px; height: 80px;"></textarea><br />';
	string += '<img src="http://www.creativeprooffice.com/apps/images/buttonSave.gif" style="border: none; cursor: pointer;" onClick="bugReportSubmit(); return false;" /></div>';
	openWindow(string,'bugReportWindow',300,0,1,'','','../');
}

function bugReportSubmit() {
	var bugText = $('bugText').value;
	x_saveBugReport(bugText,returnSubmitBugReport);
}

function returnSubmitBugReport(string) {
	if (string == 'error') {
		$('bugInstructions').innerHTML = 'There was an error submitting your bug report.';
	} else {
		$('bugForm').innerHTML = 'Your bug report has been submitted. Thanks!';
	}
}

/***** END BUG REPORT FUNCTIONS *****/

function openHelpWindow(uID) {
	var string = 'Context sensitive help is being built and should be available in a few weeks.<br />';
	string    += 'In the meantime, check out the';
	string    += '<ul>'
	string    += '<li><a href="http://forum.creativeprooffice.com/?logged=1&u='+uID+'" target="_blank"><b>Support Forum</b></a></li>';
	string    += '<li><a href="http://blog.creativeprooffice.com" target="_blank"><b>Blog</b></a></li></ul> for help and information about bug fixes.';
	openWindow(string,'helpWindow',300,0,0,'','','../');
}


/***** CALENDAR FUNCTIONS ******/

function newMonth(month,year,size) {
	x_displayCalendar(month,year,size,returnDisplayCalendar);
}

function returnDisplayCalendar(calString) {
	$('buttonMonthView').innerHTML  = '';
	$('calAddEditForm').innerHTML = '';
	$('calAddEditForm').style.display = 'none';
	
	$('calDayView').style.display     = 'none';
	$('calDayView').innerHTML         = '';
	
	$('calContainer').innerHTML = calString;
	$('calContainer').style.display   = 'block';	
}

function eventDisplay(date,day,events) {
	// Populate dateHolder hidden input field
	$('dateHolder').value = date;
		
	if (events == 0) {
		// Go straight to add event form
		eventAddEdit(date,day,'add',0);
	} else {
		x_displayEvent(date,returnDisplayEvent);	
	}	
}

function returnDisplayEvent(string) {
	$('calContainer').style.display   = 'none';
	$('calAddEditForm').style.display = 'none';
	$('calDayView').style.display     = 'block';
	$('calDayView').innerHTML         = string;
	// Show 'Month View' button
	var date = $('dateHolder').value;
	var dateArray = date.split('-');
	var year      = dateArray[0];
	var month     = dateArray[1];
	$('buttonMonthView').innerHTML = '<a href="#" onClick="newMonth('+month+','+year+',0); return false;"><img src="http://www.creativeprooffice.com/apps/images/buttonMonthView.gif" style="border: none;" alt="Month View"></a>';
}

function eventAddEdit(date,day,doWhat,eID) {
	var date = $('dateHolder').value;
	// Get calendar window width to determine how to display our form
	var formWidth = $('windowContentCalendar').clientWidth;
	x_eventAddEdit(date,day,doWhat,eID,formWidth,returnEventAddEdit);
}

function returnEventAddEdit(string) {
	$('calContainer').style.display   = 'none';
	$('calDayView').style.display     = 'none';
	$('eventButtons').style.display     = 'none';
	$('calAddEditForm').style.display = 'block';
	$('calAddEditForm').innerHTML = string;
}

function eventAddEditCancel() {
	$('calAddEditForm').innerHTML = '';
	$('calAddEditForm').style.display = 'none';
	$('calContainer').style.display   = 'block';
	$('eventButtons').style.display   = 'block';
}

function eventAddEditSave(doWhat,eID) {
	// Get stuff from our form
	var title       = removeChar($('eventTitle').value);
	var desc        = removeChar($('eventDescription').value);
	var month       = $('calMonthSelect');
	var monthSelect = month[month.selectedIndex].value;
	
	var day         = $('calDaySelect');
	var daySelect   = day[day.selectedIndex].value;
	
	var year        = $('calYearSelect');
	var yearSelect  = year[year.selectedIndex].value;
	
	var hour        = $('calHourSelect');
	var hourSelect  = hour[hour.selectedIndex].value;
	
	var min         = $('calMinSelect');
	var minSelect   = min[min.selectedIndex].value;
	
	var ampm        = $('calAmPmSelect');
	var ampmSelect  = ampm[ampm.selectedIndex].value;
	
	var tag         = $('eventTag');
	var tagSelect   = tag[tag.selectedIndex].value;
	var phpString   = title+'|'+desc+'|'+monthSelect+'|'+daySelect+'|'+yearSelect+'|'+hourSelect+'|'+minSelect+'|'+ampmSelect+'|'+tagSelect;
	// Set date in dateHolder
	$('dateHolder').value = year+'-'+month+'-'+day;
	x_eventAddEditSave(phpString,doWhat,eID,returnDisplayCalendar);
}

/***** END CALENDAR FUNCTIONS *****/

function checkUserid() {
	var userid = $('newUserid').value;
	x_checkUserid(userid,returnCheckUserid);
}

function clearUserid() {
	$('newUserid').style.background = '#ffffff';
	$('messages').innerHTML = '';
	$('messages').className = '';
	
}

function returnCheckUserid(duplicate) {
	if (duplicate == 1) {
		$('newUserid').style.background = '#ffffcc';
		$('messages').innerHTML = 'Your selected User Name is already in use. Please select another.';
		$('messages').className = 'errorMessage';
	}
}

function rollWindow(window,upDown) {
	var winContentID = window;
	var winRollID    = window+'Roll';
		
	if (upDown == 'up') {
		var newHTML = '<a href="#" onClick="rollWindow(\''+window+'\',\'down\'); return false;" title="Roll Down"><img src="../images/button_rollDown.gif" style="border: none;"></a>';
		new Effect.SlideUp(winContentID, {direction: 'top', duration: .2});
		$(winRollID).innerHTML = newHTML;
	} else if (upDown == 'down') {
		var newHTML = '<a href="#" onClick="rollWindow(\''+window+'\',\'up\'); return false;" title="Roll Up"><img src="../images/button_rollUp.gif" style="border: none;"></a>';
		new Effect.SlideDown(winContentID, {direction: 'top', duration: .2});
		$(winRollID).innerHTML = newHTML;
	}
}

function showText(id,text,onOff) {
	if (onOff == 'on') {
		$(id).innerHTML = text;
	} else {
		$(id).innerHTML = '';
	}	
}

// A cool little bg fader thingy
// Prep the fader thingy
var FadeInterval = 300;
var StartFadeAt  = 7;
var FadeSteps = new Array();
	FadeSteps[1] = "ff";
	FadeSteps[2] = "ee";
	FadeSteps[3] = "dd";
	FadeSteps[4] = "cc";
	FadeSteps[5] = "bb";
	FadeSteps[6] = "aa";
	FadeSteps[7] = "99";
function doFade(colorId, targetId) {
    if (colorId >= 1) {
		$(targetId).style.backgroundColor = "#ffff" + FadeSteps[colorId];
		
        // If it's the last color, set it to transparent
        if (colorId==1) {
            $(targetId).style.backgroundColor = "transparent";
		}
        colorId--;
		
        // Wait a little bit and fade another shade
        setTimeout("doFade("+colorId+",'"+targetId+"')", FadeInterval);
	}
} 

function ajaxAnim(whichSide) {
	if (whichSide == 'left') { var sideID = 'ajaxAnimLeft'; }
	else if (whichSide == 'right') { var sideID = 'ajaxAnimRight'; }
	
	//$(sideID).innerHTML = '<img src="../images/ajaxAnimSmall2.gif"> Working...';
	//doFade(7,sideID);
}

function ajaxAnimStop(whichSide) {
	if (whichSide == 'left') { var sideID = 'ajaxAnimLeft'; }
	else if (whichSide == 'right') { var sideID = 'ajaxAnimRight'; }
	//$(sideID).innerHTML = '';
}

function ajaxAnimSmall(divID,size,startStop) {
	var ajaxDiv = $(divID);
	if (startStop == 1) {
		ajaxDiv.innerHTML = '<img src="http://www.creativeprooffice.com/apps/images/ajaxAnimSmall2.gif">';
	} else if (startStop == 0) {
		ajaxDiv.innerHTML = '';
	}
}

// ***** GET PROJECTS FROM CLIENT SELECT MENU ************************************** //

function getClientProjectsFromSelect() {
	var clientID      = $('clientID');
	var clientIDIndex = clientID.selectedIndex;
	var clientID      = clientID[clientIDIndex].value;
	if (clientID != 'Select...') {
		x_getProjectsForClient(clientID,returnGetProjectsForClient);
	}	
}

function returnGetProjectsForClient(string) {
	$('clientProjects').innerHTML = string;
	$('clientProjects').style.display = '';
}

function checkNaN(entry,warningID) {
	if (isNaN(entry)) {
		$(warningID).style.background = '#FEBCBC';
		return false;
	} else {
		$(warningID).style.background = '#ffffff';
		return true;
	}
}

function rightColRollUp (element) {
	// Check current display status
	var elementCookie = element+'Cookie'; 
	var element = element+'Container';
	var displayStatus = $(element).style.display;
	if (displayStatus == 'none') {
		// Roll Down
		new Effect.SlideDown(element, {direction: 'top', duration: .2});
		$(element).title = 'Roll Up';
		
		// Set cookie; 1 = Roll Down, 0 = Roll Up
		cookieSet(elementCookie,1);
	} else {
		Effect.SlideUp(element, {direction: 'top', duration: .2});
		$(element).title = 'Roll Down';
		
		// Set cookie; 1 = Roll Down, 0 = Roll Up
		cookieSet(elementCookie,0);
	}	
}

// ***** SHOW CLIENT,INVOICE,PROJECT DETAILS WINDOW ***************************************************
function showDetails(elementID,showWhat) {
	var displayDiv = showWhat+'Details_'+elementID;
	if ($(displayDiv).style.display == 'block') {
		$(displayDiv).style.display = 'none';
	} else {
		$(displayDiv).style.display = 'block';
	}
}

// ***** DELETE ITEMS (CLIENT,INVOICE,PROJECT,ETC.) ************************************
function confirmDelete(itemID,deleteWhat) {
	if (deleteWhat == 'client') {
		var OK = confirm("Are you sure you want to delete this Client? No other client item such as projects or invoices will be deleted.");
		if (OK == true) {
			clientDelete(itemID,0);
		}
	} else if (deleteWhat == 'clientView') {
		var OK = confirm("Are you sure you want to delete this Client? No client items such as projects or invoices will be deleted.");
		if (OK == true) {
			clientDelete(itemID,1);
		}
	} else if (deleteWhat == 'invoice') {
		var OK = confirm("Are you sure you want to delete this Invoice?");
		if (OK == true) {
			invoiceDelete(itemID,0);
		}
	} else if (deleteWhat == 'invoiceView') {
		var OK = confirm("Are you sure you want to delete this Invoice?");
		if (OK == true) {
			invoiceDelete(itemID,1);
		}
	} else if (deleteWhat == 'project') {
		var OK = confirm("Are you sure you want to delete this Project?");
		if (OK == true) {
			projectDelete(itemID,0);
		}
	} else if (deleteWhat == 'projectView') {
		var OK = confirm("Are you sure you want to delete this Project?");
		if (OK == true) {
			projectDelete(itemID,1);
		}
	} else if (deleteWhat == 'task') {
		var OK = confirm("Are you sure you want to delete this Task?");
		if (OK == true) {
			//x_taskDelete(itemID,returnTaskAdd);
			x_taskDelete(itemID,returnTaskDelete);
		}
	} else if (deleteWhat == 'milestone') {
		var OK = confirm("Are you sure you want to delete this Milestone and all associated Tasks?");
		if (OK == true) {
			x_mileStoneDelete(itemID,returnTaskAdd);
		}
	} else if (deleteWhat == 'teamMember') {
		var OK = confirm("Are you sure you want to delete this Team Member?");
		if (OK == true) {
			x_teamMemberDelete(itemID,getAllTeamMembers);
		}
	} else if (deleteWhat == 'event') {
		var OK = confirm("Are you sure you want to delete this Calendar Entry?");
		if (OK == true) {
			// First get current date from dateHolder hidden input field
			var date = $('dateHolder').value;
			x_eventDelete(itemID,date,returnDisplayEvent);
		}
	} else if (deleteWhat == 'note') {
		var OK = confirm("Are you sure you want to delete this Note?");
		if (OK == true) {
			x_noteDelete(itemID,returnNoteDelete);
		}
	}  else if (deleteWhat == 'file') {
		var OK = confirm("Are you sure you want to delete this File?");
		if (OK == true) {
			// Get file sort order
			var order = $('fileOrder').value;
			var acdc  = $('fileAcDc').value;
			x_fileDelete(itemID,order,acdc,returnFileDelete);
		}
	} else if (deleteWhat == 'rss') {
		var OK = confirm("Are you sure you want to delete this RSS Feed?");
		if (OK == true) {
			x_rssDelete(itemID,returnRSSDelete);			
		}
	} else if (deleteWhat == 'blog') {
		var OK = confirm("Are you sure you want to delete this Blog Entry?");
		if (OK == true) {
			x_postDelete(itemID,returnPostDelete);			
		}
	} else if (deleteWhat == 'user') {
		var OK = confirm("Are you sure you want to delete this CPO User?");
		if (OK == true) {
			x_deleteUser(itemID,returnDeleteUser);			
		}
	} else if (deleteWhat == 'bug') {
		var OK = confirm("Are you sure you want to delete this Bug?");
		if (OK == true) {
			x_deleteBug(itemID,returnDeleteBug);			
		}
	} else if (deleteWhat == 'expense') {
		var OK = confirm("Are you sure you want to delete this Expense Item?");
		if (OK == true) {
			x_deleteExpense(itemID,returnDeleteExpense);			
		}
	}
}

// ***** TOOL TIP JAVASCRIPT **********************************************************
//browser detection
var agt=navigator.userAgent.toLowerCase();
var is_major = parseInt(navigator.appVersion);
var is_minor = parseFloat(navigator.appVersion);

var is_nav  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)
            && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)
            && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1));
var is_nav4 = (is_nav && (is_major == 4));
var is_nav6 = (is_nav && (is_major == 5));
var is_nav6up = (is_nav && (is_major >= 5));
var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));

//tooltip Position
var offsetX = 8;
var offsetY = -62;
var opacity = 100;
var toolTipSTYLE;

function initToolTips(){
  if(document.getElementById){
          toolTipSTYLE = document.getElementById("toolTipLayer").style;
  }
  if(is_ie || is_nav6up)
  {
    toolTipSTYLE.visibility = "visible";
    toolTipSTYLE.display = "none";
    document.onmousemove = moveToMousePos;
  }
}
function moveToMousePos(e)
{
  if(!is_ie){
    x = e.pageX;
    y = e.pageY;
  }else{
    x = event.x + document.body.scrollLeft;
    y = event.y + document.body.scrollTop;
  }

  toolTipSTYLE.left = x + offsetX+'px';
  toolTipSTYLE.top = y + offsetY+'px';
  return true;
}


function toolTip(msg, type)
{
  if(toolTip.arguments.length < 1) // if no arguments are passed then hide the tootip
  {
    if(is_nav4)
        toolTipSTYLE.visibility = "hidden";
    else
        toolTipSTYLE.display = "none";
  }
  else // show
  {
  	// Delay
  	//pause(750);
    
    if (type == 'baloon') {
    	var content = '<div class="toolTipBaloon">'+msg+'</div>';
    } else {
		var content = '<div class="toolTipBox" style="margin-top: 30px;"><p>'+msg+'</p></div>';
	}
    
   if(is_nav4)
    {
      toolTipSTYLE.document.write(content);
      toolTipSTYLE.document.close();
      toolTipSTYLE.visibility = "visible";
    }

    else if(is_ie || is_nav6up)
    {
      document.getElementById("toolTipLayer").innerHTML = content;
      toolTipSTYLE.display='block'
    }
  }
}

function show(arg1,arg2){
        toolTip(s)
}
// ***** END TOOL TOP JAVASCRIPT ************************************************

// ***** FORGOT PASSWORD ********************************************************
function forgotPassword() {
	$('loginBox').style.display = 'none';
	$('forgotPasswordBox').style.display = 'block';
	$('userName').focus();
}

function forgotPasswordSend() {
	var email    = $('email').value;
	var userName = $('userName').value;
	ajaxAnimSmall('ajaxAmin',0,1);
	x_forgotPasswordSend(email,userName,forgotPasswordReturn);
}

function forgotPasswordCancel() {
	$('loginBox').style.display = 'block';
	$('forgotPasswordBox').style.display = 'none';
}

function forgotPasswordReturn(string) {
	if (string == 'errorEmail') {
		// We have an error
		var message = $('forgotMessage');
		message.innerHTML = 'Sorry! We could not find that email address.<br>Please try again.';
		message.className = 'errorMessage';
		ajaxAnimSmall('ajaxAmin',0,0);
	} else if (string == 'errorUID') {
		// We have an error
		var message = $('forgotMessage');
		message.innerHTML = 'Sorry! We could not find that user name.<br>Please try again.';
		message.className = 'errorMessage';
		ajaxAnimSmall('ajaxAmin',0,0);
	} else if (string == 'success') {
		var message = $('loginMessage');
		message.innerHTML = 'Your temporary password has been sent.';
		message.className = 'successMessage';
		$('loginBox').style.display = 'block';
		$('forgotPasswordBox').style.display = 'none';
		ajaxAnimSmall('ajaxAmin',0,0);	
	}	
}

function goPrint(what,itemID) {
	var printFrame = $('printFrame')
	if (what == 'invoices') {
		printFrame.src = '../includes/print/printInvoice.php?invID='+itemID;
	} else if (what == 'tasks') {
		printFrame.src = '../includes/print/printTaskList.php?projID='+itemID;
	} else if (what == 'bugs') {
		printFrame.src = '../includes/print/printBugs.php';
	} else if (what == 'pdf') {
		printFrame.src = '../includes/print/pdf/printPDFInvoice.php?invID='+itemID;
	}
	
}

function goPrintGeneric(contentID,title) {
	var printContent = $(contentID).innerHTML;
	printFrame.document.getElementById('genericPrintContent').innerHTML = printContent;
	printFrame.document.title = title;
	printFrame.print();
}

function putTag(tagName,targetField) {
	var inputTags = $(targetField).value;
	$(targetField).value = inputTags+' '+tagName;
}

function searchCPO() {
	// Get search term
	var searchTerm = $('search').value;
	x_searchCPO(searchTerm,returnSearchCPO);
}

function returnSearchCPO(string) {
	$('searchWindow').style.display = 'block';
	$('searchResultsContainer').innerHTML = string;
}

function goto(linkElement) {
	var link = $(linkElement).value;
	window.open (link,"linkWindow");
}

// ***** COOKIE FUNCTIONS ***** //

// Set a cookie of name = value with path and domain
function cookieSet( name,value,expireDays,path,domain,secure ) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );
	if (!path) { path = '/'; }
	if (!domain) { domain = 'creativeprooffice.com'; }
	if (!expireDays) {
		var expireDays = 1;
	}	
		
	var expires = expireDays * 1000 * 60 * 60 * 24;
	var expires_date = new Date( today.getTime() + (expires) );
	
	document.cookie = name + "=" +escape( value ) +
	( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + 
	( ( path ) ? ";path=" + path : "" ) + 
	( ( domain ) ? ";domain=" + domain : "" ) +
	( ( secure ) ? ";secure" : "" );
}

// Get the cookie, if it exists
function cookieGet(name) {	
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ( ( !start ) &&
	( name != document.cookie.substring( 0, name.length ) ) )
	{
	return null;
	}
	if ( start == -1 ) return null;
	var end = document.cookie.indexOf( ";", len );
	if ( end == -1 ) end = document.cookie.length;
	return unescape( document.cookie.substring( len, end ) );
}

// Delete cookie
function cookieDelete( name, path, domain ) {
	if (!path) { path = '/'; }
	if (!domain) { domain = 'creativeprooffice.com'; }
	if ( cookieGet( name ) ) document.cookie = name + "=" +
	( ( path ) ? ";path=" + path : "") +
	( ( domain ) ? ";domain=" + domain : "" ) +
	";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

// ***** END COOKIE FUNCTIONS ***** //

function checkSideBarPanels() {
	// Calendar Cookie
	if ($('calendarContainer')) {	
		if (!cookieGet('calendarCookie')) {
			cookieSet('calendarCookie',0);
			$('calendarContainer').style.display = 'none';
		} else if (cookieGet('calendarCookie') == 1) {
			$('calendarContainer').style.display = '';
		} else {
			$('calendarContainer').style.display = 'none';
		}
	}	
	
	// Calculator Cookie
	if ($('calcContainer')) {	
		if (!cookieGet('calcCookie')) {
			cookieSet('calcCookie',0);
			$('calcContainer').style.display = 'none';
		} else if (cookieGet('calcCookie') == 1) {
			$('calcContainer').style.display = '';
		} else {
			$('calcContainer').style.display = 'none';
		}
	}	
	
	// Job Timer Cookie
	if ($('timerContainer')) {	
		if (!cookieGet('timerCookie')) {
			cookieSet('timerCookie',0);
			$('timerContainer').style.display = 'none';
		} else if (cookieGet('timerCookie') == 1) {
			$('timerContainer').style.display = '';
		} else {
			$('timerContainer').style.display = 'none';
		}
	}	
}

// ***** JOB TIMER FUNCTIONS ***** //
function jobTimer(startStop) {
	// Get time from hidden field
	var currentTime = $('jobTimerHidden').value;
	var timeArray = currentTime.split(':');
	var sec  = parseInt(timeArray[2]);
	var min  = parseInt(timeArray[1]);
	var hour = parseInt(timeArray[0]);
		
	if ( (startStop == 1) || (startStop == null) ) {
		sec++;
		if (sec == 60) {
			sec = 0;
			min = min + 1; 
		} else { min = min; }
		
		if (min == 60) {
		   min = 0; 
		   hour += 1; 
		}
		var cookieTime = hour+':'+min+':'+sec;		
		if (sec<=9) { sec = "0" + sec; }
		var jobTime = ((hour<=9) ? "0"+hour : hour) + " : " + ((min<=9) ? "0" + min : min) + " : " + sec;
		
		if (startStop == 1) {	
			cookieSet('jobTimerRunning',1);
		}
		
		// Change clock icon
		if ($('jobTimerIcon').src != '../images/iconClockPlay.gif') {
			$('jobTimerIcon').src = '../images/iconClockPlay.gif';	
		}	
		
		// Turn off Commit button
		$('buttonCommit').style.display = 'none';
		// Clear elapsed hours
		$('jobTimeElapsed').value = ''; 
		
		// Set Play and Reset buttons to inactive state
		$('startStopDiv2').style.display = 'block';
		$('startStopDiv').style.display  = 'none';
		
		// Render time to the window
		$('jobTimerDisplay').innerHTML = jobTime;
		$('jobTimerHidden').value = cookieTime;
		cookieSet('jobTimer',cookieTime);
		
		SD=window.setTimeout("jobTimer();", 1000);
	} else if (startStop == 2) {
		// Log time elapsed to input field
		// Convert 0:0:0 to hours, decimal
		if (sec>29) { min = min+1; }
		var minDecimal = parseFloat(min/60);
		var elapsedTime = formatCurrency(parseFloat(hour+minDecimal));
		$('jobTimeElapsed').value = elapsedTime;
		// Change clock icon
		if ($('jobTimerIcon').src != '../images/iconClockStop.gif') {
			$('jobTimerIcon').src = '../images/iconClockStop.gif';	
		}
		
		// Re-activate Play and Reset buttons
		$('startStopDiv2').style.display = 'none';
		$('startStopDiv').style.display = '';
		
		window.clearTimeout(SD);
		sec=sec-1;
		
		cookieSet('jobTimerRunning',2);
		cookieSet('jobTimerElapsedTime',elapsedTime);
		$('buttonCommit').style.display = '';
		return true;				
	}	
}

function jobTimerReset() {
	sec = -1;
	min = 0;
	hour = 0;
	//window.clearTimeout(SD);
	
	// Delete cookies
	cookieDelete('jobTimer');
	cookieDelete('jobTimerRunning');
	$('jobTimerDisplay').innerHTML = '00 : 00 : 00';		
	$('jobTimerHidden').value = hour+':'+min+':'+sec;
} 

function clearJobTimer() {
	// This function fired on login to clear the job timer cookies
	// Otherwise, job timer initializes at whatever time was logged 
	// at last session timeout.
	
	// 5/30/07: No harm done in having the job timer in the state it 
	// was when session ended...let's see.
	/*
	cookieDelete('jobTimerRunning');
	cookieDelete('jobTimer');
	cookieDelete('jobTimerProjID');
	cookieDelete('jobTimerTaskID');
	*/
}

function checkJobTimer() {
	// Display project and task
	var projID   = cookieGet('jobTimerProjID');
	var taskID   = cookieGet('jobTimerTaskID');
	
	var projSelect = $('projectSelectMenuTimer');
	var projValue  = '';
		
	for (var a=0; a < projSelect.length; a++) { 
		projValue = projSelect.options[a].value;
		if (projValue == projID) {
			projSelect.options[a].selected = true;
		}
	}
	getTasksFromProject(projID,'taskSelectTimer',taskID);
	
	if ( typeof ($('taskSelectMenu')) != 'undefined' ) {
		var taskSelect = $('taskSelectMenu');
		var taskValue  = '';
		for (var a=0; a < taskSelect.length; a++) { 
			taskValue = taskSelect.options[a].value;
			if (taskValue == taskID) {
				taskSelect.options[a].selected = true;
			}
		}
	}	

	// Is the jobTimer running?
	if ( (cookieGet('jobTimerRunning') == 1) || (cookieGet('jobTimerRunning') == 2) ) {
		// Then grab the time and continue the timer
		// Get hour, min, sec
		var jobTime = cookieGet('jobTimer');
		var elapsedTime = cookieGet('jobTimerElapsedTime');
		var timeArray = jobTime.split(':');
		var hour = timeArray[0];
		var min  = timeArray[1];
		var sec  = timeArray[2];
			
		$('jobTimerHidden').value = hour+':'+min+':'+sec;
		
		// Format displayed time
		if (hour<10) { hour = '0'+hour; }
		if (min<10)  { min  = '0'+min; }
		if (sec<10)  { sec  = '0'+sec; }
		$('jobTimerDisplay').innerHTML = hour+' : '+min+' : '+sec;
		
		// Display elapsed time
		$('jobTimeElapsed').value = elapsedTime;	
				
		if (cookieGet('jobTimerRunning') == 1) {
			// Then continue the timer
			jobTimer();
		}	
	} else {
		// Initialize our time variables to 0
		var sec = -1;
		var min = 0;
		var hour = 0;
		$('jobTimerDisplay').innerHTML = '00 : 00 : 00';		
		$('jobTimerHidden').value = hour+':'+min+':'+sec;
	}
}

function jobTimeCommit() {
	// Gather our data
	var elapsedHours = $('jobTimeElapsed').value;
	var projID   = cookieGet('jobTimerProjID');
	var taskID   = cookieGet('jobTimerTaskID');
	var comments = removeChar($('jobTimerComments').value); 
	
	// Log info to database
	x_logJobTimerToTimesheet(elapsedHours,projID,taskID,comments,returnLogJobTimerToTimesheet);	
}

function returnLogJobTimerToTimesheet() {
	// Clear log hours button and replace with status message
	$('buttonCommit').style.display = 'none';
	$('logHoursStatus').className = 'successMessage';
	$('logHoursStatus').innerHTML = 'Your hours have been logged.';
}

function getTasksFromProject(projID,selectField,taskID) {
	if (taskID == 'undefined') { taskID = 0; }
	selectProjectTaskForTimer(projID,0);
	x_getTasksFromProject(projID,selectField,taskID,returnGetTasksFromProject);
}

function returnGetTasksFromProject(string) {
	var taskArray = string.split('|');
	var taskOptions     = taskArray[0];
	var taskSelectField = taskArray[1];
	
	if (taskOptions != 0) {
		$(taskSelectField).innerHTML = taskOptions;
	}	
}

function selectProjectTaskForTimer(projID,taskID) {
	cookieSet('jobTimerProjID',projID);
	cookieSet('jobTimerTaskID',taskID);
}

function checkTimerHours() {
	// If someone manually enters a time into jobTimeElapsed and tabs out
	// see if hours > 0 and if so, display log time button
	var hours = $('jobTimeElapsed').value;
	if ( !isNaN(hours) && (hours > 0) ) {
		$('buttonCommit').style.display = '';
	}	
}
// ***** END JOB TIMER FUNCTIONS *****//