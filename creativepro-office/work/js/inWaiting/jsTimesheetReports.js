function filterClientProject(type) {
	x_filterClientProject(type,returnGetFilterSelect);
}

function returnGetFilterSelect(string) {
	$('filterSelectDiv').innerHTML = string;
}

function createReport() {
	// Let's get all of our variables
	// 1) Do we have a date range?
		var dateStart = $('dateStart').value;
		var dateEnd   = $('dateEnd').value;
		
	// 2) Do we have a month and/or year select? If so, this overrides but we'll do that in php.
		var monthID    = $('selectMonth');
		var monthIndex = monthID.selectedIndex;
		var month      = monthID[monthIndex].value;
		
		var yearID     = $('selectYear');
		var yearIndex  = yearID.selectedIndex;
		var year       = yearID[yearIndex].value;
		
		if ( (year == 'Year') || (year == '') ) { year = 0; }
		if ( (month == 'Month') || (month == '') ) { month = 0; }
		
		// Possible error, only Month or Year selected
		if ( ((month == 0) && (year != 0)) || ((year == 0) && (month != 0)) ) {
			var error = 1;
			$('searchError').innerHTML = '<span color=red>You must select a Month and Year.</span>';
		}
	
	// 3) Do we have a client/project filter?
		var client   = $('filterClient');
		var project  = $('filterProject');
		var clientID = 0;
		var projID   = 0;
		if (client.checked == true) {
			// Look for selected client
			clientID    = $('filterSelect');
			var clientIndex = clientID.selectedIndex;
			clientID        = clientID[clientIndex].value;
			if ( (clientID == 'Select Client...') || (clientID == '') ) { clientID = 0; }
		} else if (project.checked == true) {
			// Look for selected project
			projID    = $('filterSelect');
			var projIndex = projID.selectedIndex;
			projID        = projID[projIndex].value;
			if ( (projID == 'Select Project...') || (projID == '') ) { projID = 0; }
		}
	
	// 4) Are we selecting a team member
		if ( typeof ($('teamMember')) != 'undefined' ) {
			var teamMemberID = $('teamMember');
			var teamMemberIndex = teamMemberID.selectedIndex;
			teamMemberID        = teamMemberID[teamMemberIndex].value;
		} else {
			var teamMemberID = 0;
		}			
	
	// Send our filter criteria to the sajax function
	if (error != 1) {
		x_createReport(dateStart,dateEnd,month,year,clientID,projID,teamMemberID,returnCreateReport);
		$('searchError').innerHTML = '';
	}	
}

function returnCreateReport(string) {
	var stringArray   = string.split('|');
	var reportDisplay = stringArray[0];
	var error         = stringArray[1];
	//$('timeSheetReportWindow').style.display = '';
	if (error != '') {
		$('timeSheetReportWindow').innerHTML = '<div class="errorMessage">'+error+'</div>';
	} else { 
		$('timeSheetReportWindow').innerHTML = reportDisplay;
	}
}

function timesheetViewTeam() {
	// Check for empty
	var tmID    = $('teamMember');
	var tmIndex = tmID.selectedIndex;
	var tmID    = tmID[tmIndex].value;
	if ( (tmID == '') || (tmID == 'Select team member...') ) {
		$('tmError').className = 'errorMessage';
		$('tmError').innerHTML = 'You must select a team member or select All team members.';
	} else {
		$('tmError').className = '';
		$('tmError').innerHTML = '';
		x_timesheetViewTeam(tmID,returnTimesheetViewTeam);
	}	
}

function returnTimesheetViewTeam(string) {
	$('timesheetViewTeam').innerHTML = string;
}