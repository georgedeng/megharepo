// Handles page select menu and prev/next buttons
function gotoPage(newPage) {
	var sort = '';
	var ad   = '';
	var limit = '';
	var startPoint = '';
	var search = '';
	x_getCurrentProjects(sort,ad,limit,startPoint,newPage,search,returnGotoPage);
}

function returnGotoPage(stuff) {
	var stringArray = stuff.split("~~|~~");
	var projectDisplay = stringArray[0];
	var pages = stringArray[1];
	var paginate1 = stringArray[2];
	var paginate2 = stringArray[3];
	
	$('projectDisplay').innerHTML = projectDisplay;
	$('paginate1').innerHTML = paginate1;
	$('paginate2').innerHTML = paginate2;
}

// Handles records per page select menu
function recordsPerPage(newRecordsPerPage) {
	var sort = '';
	var ad   = '';
	var limit = newRecordsPerPage;
	var startPoint = '';
	var search = '';
	var newPage = '';
	x_getCurrentProjects(sort,ad,limit,startPoint,newPage,search,returnRecordsPerPage);
}

function returnRecordsPerPage(stuff) {
	var stringArray = stuff.split("~~|~~");
	var projectDisplay = stringArray[0];
	var pages = stringArray[1];
	var paginate1 = stringArray[2];
	var paginate2 = stringArray[3];
	
	$('projectDisplay').innerHTML = projectDisplay;
	$('paginate1').innerHTML = paginate1;
	$('paginate2').innerHTML = paginate2;
}

function updateStatus(statusID,projID) {
	x_updateStatus(statusID,projID,returnUpdateStatus);
}

function returnUpdateStatus(string) {
	$('statusImage').innerHTML = string;
}

function returnGetTasks(string) {
	$('projectViewTasks').innerHTML = string;
}

function viewProjectItems(what) {
	var projID = $('projID').value;
	x_viewProjectItems(projID,what,returnViewProjectItems);	
}

function openNoteForm() {
	var string = '<div id="newNote">';
	string    += '<textarea id="newNoteText" style="width: 350px; height: 80px;" onFocus="this.value=\'\'">Enter new project note.</textarea><br />';
	string    += '<a href="#" onClick="noteSave(); return false;"><img src="../images/buttonSave.gif" alt="Save Note" style="border: none;" /></a>&nbsp;';
	string    += '<a href="#" onClick="viewProjectItems(\'Notes\'); return false;"><img src="../images/buttonCancel.gif" alt="Cancel" style="border: none;" /></a>&nbsp;';
	string    += '</div>';
	$('infoWindow').innerHTML = string;
}

function noteSave() {
	var projID = $('projID').value;
	var noteText = $('newNoteText').value;
	if (noteText != 'Enter new project note.') {
		x_noteSave(projID,noteText,returnViewProjectItems);	
	}	
}

function returnNoteDelete(string) {
	returnViewProjectItems(string);
}

function openUploadForm() {
	var projID = $('projID').value;
	var string = '<div id="iFrameDiv">';
	string    += '<iframe id="fileFrame" style="width: 100%; height: 270px; border: none;" src="../includes/uploadFile.php?fileCat=Project&itemID='+projID+'"></iframe>';
	$('infoWindow').innerHTML = string;
}

function returnViewProjectItems(string) {
	var stringArray = string.split('|');
	var string      = stringArray[0];
	var what        = stringArray[1];
	
	if ( (what == 'Notes') && (string == '0') ) {
		openNoteForm();
	} else if ( (what == 'Files') && (string == '0') ) {
		openUploadForm();
	} else {
		$('infoWindow').innerHTML = string;
	}	
	
	// Change selected menu item background
	// Deselect everything
	$('menuItemStats').className    = 'menuItemNotSelected';
	$('menuItemContacts').className = 'menuItemNotSelected';
	$('menuItemFiles').className    = 'menuItemNotSelected';
	$('menuItemMessages').className = 'menuItemNotSelected';
	$('menuItemNotes').className    = 'menuItemNotSelected';
	$('menuItemTeam').className     = 'menuItemNotSelected';
	
	var selectedMenu = 'menuItem'+what;
	$(selectedMenu).className = 'menuItemSelected';
}

function returnFileUpload(itemCat,itemID) {
	viewProjectItems('Files');
}

function sortFiles(order,acdc) {
	var projID = $('projID').value;
	$('fileOrder').value = order;
	$('fileAcDc').value = acdc;
	x_getProjectFiles(projID,order,acdc,returnSortFiles); 
}

function returnSortFiles(string) {
	$('infoWindow').innerHTML = string;
}

function returnFileDelete(string) {
	$('infoWindow').innerHTML = string;
}

function togglePublicFile(fID,porp) {
	x_togglePublicFile(fID,porp,returnTogglePublicFile);
}

function returnTogglePublicFile(string) {
	var stringArray = string.split('|');
	var image = stringArray[0];
	var fID   = stringArray[1];
	$('public'+fID).innerHTML = image;
}

function projectDelete(projID,fromView) {
	if (fromView == 1) {
		x_projectDelete(projID,returnProjectViewDelete);
	} else {
		x_projectDelete(projID,returnProjectDelete);
	}	
}

function returnProjectViewDelete() {
	$('projectDisplay').innerHTML = '<div style="padding: 10px;"><span class="bigText">This project has been deleted. <a href="index.php">View your other projects.</a></span></div>';
	$('projectStuffDisplay').innerHTML = '';
}

function returnProjectDelete(projID) {
	// Turn off project tr
	$('projectTR'+projID).style.display = 'none';
}

function sortFilter(sortColumn,acdc) {		
	// Check for records per page
	// First see if the records per page pull-down even exists
	if ( $('recordsPerPage1') ) {
		var rppID    = $('recordsPerPage1');
		var rppIndex = rppID.selectedIndex;
		var rppID    = rppID[rppIndex].value;
	} else {
		rppID =  0;
	}
	if ( $('pageNo1') ) {
		// Get page
		var pageID    = $('pageNo1');
		var pageIndex = pageID.selectedIndex;
		var pageID    = pageID[pageIndex].value;
	} else {
		pageID = 0;
	}	
	$('sortFilterBy').value = sortColumn;	
	$('sortFilterAcDc').value = acdc;	
	
	x_getCurrentProjects(sortColumn,acdc,rppID,'',pageID,'',returnRecordsPerPage)
}