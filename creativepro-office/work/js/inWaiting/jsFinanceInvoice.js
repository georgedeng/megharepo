// Handles page select menu and prev/next buttons
function gotoPage(newPage) {
	var archiveID    = document.getElementById('archive');
	var archiveIndex = archiveID.selectedIndex;
	var archiveID    = archiveID[archiveIndex].value;
	
	var sort       = '';
	var ad         = '';
	var limit      = '';
	var startPoint = '';
	var search     = '';
	var archive    = archiveID;
	x_getInvoices(sort,ad,limit,startPoint,newPage,search,archive,returnGotoPage);
}

function returnGotoPage(stuff) {
	var stringArray = stuff.split("~~|~~");
	var invoiceDisplay = stringArray[0];
	var pages = stringArray[1];
	var paginate1 = stringArray[2];
	var invTotal  = stringArray[3];
	var windowTitle = stringArray[4];
	
	document.getElementById('invoiceDisplay').innerHTML = invoiceDisplay;
	document.getElementById('paginate1').innerHTML = paginate1;
	document.getElementById('pageSubTitle').innerHTML = windowTitle;
}

// Handles records per page select menu
function recordsPerPage(newRecordsPerPage) {
	var archiveID    = document.getElementById('archive');
	var archiveIndex = archiveID.selectedIndex;
	var archiveID    = archiveID[archiveIndex].value;

	var sort       = '';
	var ad         = '';
	var limit      = newRecordsPerPage;
	var startPoint = '';
	var search     = '';
	var newPage    = '';
	var archive    = archiveID;
	x_getInvoices(sort,ad,limit,startPoint,newPage,search,archive,returnGotoPage);	
}

function viewWhich(archive) {
	var sort       = '';
	var ad         = '';
	var limit      = '';
	var startPoint = '';
	var search     = '';
	var newPage    = '';
	x_getInvoices(sort,ad,limit,startPoint,newPage,search,archive,returnGotoPage);
}

function returnRecordsPerPage(stuff) {
	var stringArray = stuff.split("~~|~~");
	var clientDisplay = stringArray[0];
	var pages = stringArray[1];
	var paginate1 = stringArray[2];
	var paginate2 = stringArray[3];
	
	document.getElementById('clientDisplay').innerHTML = clientDisplay;
	document.getElementById('paginate1').innerHTML = paginate1;
}

function invoiceDelete(invID,fromView) {
	var sort       = '';
	var ad         = '';
	var limit      = '';
	var startPoint = '';
	var newPage    = '';
	var search     = '';
	var archive    = '';	
	if (fromView == 1) {
		x_deleteInvoice(invID,sort,ad,limit,startPoint,newPage,search,archive,1,returnDeleteInvoice);
	} else {
		x_deleteInvoice(invID,sort,ad,limit,startPoint,newPage,search,archive,0,returnGotoPage);
	}
}

function returnDeleteInvoice() {
	$('invoiceDisplay').innerHTML = '<div style="padding: 10px;"><span class="bigText">This invoice has been deleted. <a href="invoices.php">View your other invoices.</a></span></div>';
	$('invoiceItemDisplay').innerHTML = '';
}

function emailInvoice(invID,invNo) {
	x_emailInvoice(invID,invNo,returnEmailInvoice);
}

function returnEmailInvoice(string) {
	var stringArray  = string.split('|');
	var invoiceEmail = stringArray[0];
	var invoiceID    = stringArray[1];
	var emailWinID   = 'emailWindow'+invoiceID;
	
	var content = '<div style="text-align: left;" id="invoiceWindowOpen'+invoiceID+'"><b>Send invoice to:</b><br />';
	content += '<input type="text" id="emailAddress'+invoiceID+'" style="width: 200px;" value="'+invoiceEmail+'" onFocus="this.value=\'\';"><br />';
	content += '<textarea id="emailMessage'+invoiceID+'" style="width: 200px; height: 70px;" onFocus="this.value=\'\'">Send a message with this invoice.</textarea><br />';
	content += '<a href="#" onClick="emailInvoiceConfirm('+invoiceID+'); return false;" title="Send Invoice"> <img src="../images/buttonSend.gif" style="border: none;" alt="Send Invoice"></a></div>';
	
	openWindow(content,emailWinID,220,0,1,'','','../');
}

function emailInvoiceConfirm(invID) {
	var emailWinID     = 'emailWindow'+invID;
	var emailInputID   = 'emailAddress'+invID;
	var emailMessageID = 'emailMessage'+invID 
	var emailAddress = document.getElementById(emailInputID).value;
	var emailMessage = removeChar(document.getElementById(emailMessageID).value);
	var invoiceWindowOpen = 'invoiceWindowOpen'+invID;
	
	document.getElementById(invoiceWindowOpen).innerHTML = '<img src="../images/animAJAX_tiny_red.gif"> Attempting to send invoice.';
	x_emailInvoiceConfirm(emailAddress,emailMessage,invID,returnEmailInvoiceConfirm);
}

function returnEmailInvoiceConfirm(string) {
	var stringArray  = string.split('|');
	var message      = stringArray[0];
	var invoiceID    = stringArray[1];
	var sendError    = stringArray[2];
	var invoiceWindowOpen = 'invoiceWindowOpen'+invoiceID;
	var statusID     = 'status_'+invoiceID;
	if (sendError == 0) {
		document.getElementById(statusID).innerHTML = '<img src="../images/iconInvoiceOpenSent.gif" alt="Invoice open, sent on " style="border: none;">'; 
	}	
	document.getElementById(invoiceWindowOpen).innerHTML = message;	
}	

function enterPayInfo(invID) {
	x_enterPayInfo(invID,returnEnterPayInfo);
}

function returnEnterPayInfo(string) {
	var stringArray  = string.split('|');
	var payString = stringArray[0];
	var invID     = stringArray[1];
	
	var payWinID   = 'payWindow'+invID;
	openWindow(payString,payWinID,320,0,1,'','','../');
}

function enterPayInfoConfirm(invID) {
	var payTypeID    = $('payType');
	var payTypeIndex = payTypeID.selectedIndex;
	var payType      = payTypeID[payTypeIndex].value;
	
	var statusID     = $('status');
	var statusIndex  = statusID.selectedIndex;
	var status       = statusID[statusIndex].value;
	
	var monthID      = $('payMonth');
	var monthIndex   = monthID.selectedIndex;
	var month        = monthID[monthIndex].value;
	
	var dayID        = $('payDay');
	var dayIndex     = dayID.selectedIndex;
	var day          = dayID[dayIndex].value;
	
	var yearID       = $('payYear');
	var yearIndex    = yearID.selectedIndex;
	var year         = yearID[yearIndex].value;
	
	var payNo        = $('payNo').value;
	x_enterPayInfoConfirm(invID,payType,status,month,day,year,payNo,returnEnterPayInfoConfirm);
}

function returnEnterPayInfoConfirm(string) {
	var stringArray  = string.split('|');
	var statusIcon   = stringArray[0];
	var statusAlt    = stringArray[1];
	var invID        = stringArray[2];
	var statusDiv    = 'status_'+invID;
	var htmlContent = '<a href="#" onClick="enterPayInfo('+invID+'); return false;" title="Edit payment information" onMouseOver="toolTip(\''+statusAlt+'<br>Edit payment information.\',\'baloon\');" onMouseOut="toolTip();">';
	htmlContent += '<img src="../images/'+statusIcon+'" style="border: none;" alt="'+statusAlt+'"></a>';
	htmlContent += '<div id="payWindow'+invID+'" class="popUpDiv"></div>';
	var div = $(statusDiv);
	div.innerHTML = htmlContent;
	
	if ($('statusText')) {
		$('statusText').innerHTML = statusAlt;
	}
}

function editInvoiceItems(invID) {
	var cancelDiv = $('invItemAction');
	cancelDiv.innerHTML = '<a href="#" onClick="editInvoiceItemCancel('+invID+'); return false;"><img src="../images/buttonCancel.gif" style="border: none;" alt="Cancel" /></a>';
	x_getInvoiceItems(invID,'edit',returnEditInvoiceItems); 
}

function returnEditInvoiceItems(string) {
	var div = $('invoiceItems');
	div.innerHTML = string;
}

function editInvoiceItemCancel(invID) {
	var editDiv =  $('invItemAction');
	editDiv.innerHTML = '<a href="#" onClick="editInvoiceItems('+invID+'); return false;" title="Edit Invoice Items"><img src="../images/buttonEditLarge.gif" style="border: none;" alt="Edit Invoice Items" /></a>';
	x_getInvoiceItems(invID,'view',returnEditInvoiceItemCancel);
}

function returnEditInvoiceItemCancel(string) {
	var invItemDiv = $('invoiceItems');
	invItemDiv.innerHTML = string;
}

function editInvoiceDetails(invID) {
	// function editInvoiceDetails is in sajaxFinanceInvoice.php
	x_editInvoiceDetails(invID,editInvoiceDetailsReturn);
}

function editInvoiceDetailsReturn(string) {
	var invInfoDiv = $('invoiceInformation');
	var invEditDiv = $('invoiceEdit');
	var invEditButtonDiv = $('invDetailEdit');
	var invIDDiv   = $('invID');
	var invID      = invIDDiv.value;
	
	var buttonString = '<a href="#" onClick="editInvoiceDetailsSave('+invID+'); return false;" title="Save"><img src="../images/buttonSave.gif" style="border: none;" alt="Save" /></a>';
	buttonString    += '&nbsp;<a href="#" onClick="editInvoiceDetailsCancel('+invID+'); return false;" title="Cancel"><img src="../images/buttonCancel.gif" style="border: none;" alt="Cancel" /></a>';   
	
	invInfoDiv.style.display = 'none';
	invEditDiv.style.display = 'block';
	invEditDiv.innerHTML     = string;
	invEditButtonDiv.innerHTML = buttonString;
}

function editInvoiceDetailsSave(invID) {
	// Get all of our stuff
	var error = null;
	if ($('projID')) {
		var projIDSelected = $('projID');
		var projID         = projIDSelected[projIDSelected.selectedIndex].value;
	} else {
		var projID = '';
	}	
	var invTitle       = removeChar($('invTitle').value);
	var invComments    = removeChar($('invDescription').value); 
	var incComments    = $('incComments');
	var invTaxRate     = $('invTaxRate').value;
	var invTaxAmount   = $('taxAmt').value;
	var invShipping    = $('invShipping').value;
	var paymentDueSelected = $('paymentDue');
	var paymentDue         = paymentDueSelected[paymentDueSelected.selectedIndex].value;
	var lateFeeSelected= $('lateFee');
	var lateFee        = lateFeeSelected[lateFeeSelected.selectedIndex].value;
	var lateFeeAmt     = $('specifyLateFee').value;
	
	if (isNaN(invTaxRate)) {
		error = 1;
		$('errorTaxShipping').innerHTML = 'Tax and shipping rates must be a number.';
	} else if (invTaxRate>100) {
		error = 1;
		$('errorTaxShipping').innerHTML = 'Tax rate cannot be greater than 100%.';
	} else if (isNaN(lateFeeAmt)) {
		error = 1;
		$('errorLateFeeAmt').innerHTML = 'Late fees must be a number.';
	}
	
	if (incComments.checked == true) {
		incComments = 1;
	} else {
		incComments = 0;
	}
	if (error != 1) {
		var phpString = projID+'|'+invTitle+'|'+invComments+'|'+invTaxRate+'|'+invTaxAmount+'|'+invShipping+'|';
		phpString    += paymentDue+'|'+lateFee+'|'+lateFeeAmt+'|'+incComments;
		// function editInvoiceDetailsSave is in sajaxFinanceInvoice.php
		x_editInvoiceDetailsSave(invID,phpString,editInvoiceDetailsSaveReturn);
	}	
}

function editInvoiceDetailsSaveReturn(string) {
	// Parse string
	var stringArray     = string.split('|');
	var projTitle       = stringArray[0];
	var invTitle        = stringArray[1];
	var invComments     = stringArray[2];
	var invTaxShipping  = stringArray[3];
	var invPaymentTerms = stringArray[4];
	
	$('invProjectDisplay').innerHTML    = projTitle;
	$('invTitleDisplay').innerHTML      = invTitle;
	$('invCommentsDisplay').innerHTML   = invComments;
	$('invTaxFreightDisplay').innerHTML = invTaxShipping;
	$('invPaymentTermsDisplay').innerHTML = invPaymentTerms;	
	
	var invID = $('invID').value;
	editInvoiceDetailsCancel(invID);
}

function editInvoiceDetailsCancel(invID) {
	var invInfoDiv = $('invoiceInformation');
	var invEditDiv = $('invoiceEdit');
	var invEditButtonDiv = $('invDetailEdit');
	var invIDDiv   = $('invID');
	var invID      = invIDDiv.value;
	
	invInfoDiv.style.display = 'block';
	invEditDiv.style.display = 'none';
	invEditDiv.innerHTML     = '';
	invEditButtonDiv.innerHTML = '<a href="#" onClick="editInvoiceDetails('+invID+'); return false;" title="Edit Invoice Details"><img src="../images/buttonEditLarge.gif" style="border: none;" alt="Edit Invoice Details" /></a>';
}