function setSelects(country,currency) {
	// This function sets the country and currency select menus 
	// based on their value in $_SESSION
	var countrySelect  = $('country');
	var countryValue   = '';
	var currencySelect = $('currency');
	var currencyValue  = '';
	
	// Loop through all items in select and find a match
	for (var a=0; a < countrySelect.length; a++) { 
		countryValue = countrySelect.options[a].value;
		if (countryValue == country) {
			countrySelect.options[a].selected = true;
		}
	}
	
	for (var a=0; a < currencySelect.length; a++) { 
		currencyValue = currencySelect.options[a].value;
		if (currencyValue == currency) {
			currencySelect.options[a].selected = true;
		}
	}
}