function postPublish(beID) {
	x_postPublish(beID,returnPostPublish);
}

function returnPostPublish(string) {
	var stringArray = string.split('|');
	var beID        = stringArray[1];
	var iconImage   = stringArray[0];
	var iconSpan    = $('blogActive'+beID);
	iconSpan.innerHTML = iconImage;
}

function returnPostDelete(beID) {
	$('blogTR'+beID).style.display = 'none';
}

function getComments(beID) {
	x_getComments(beID,returnGetComments);
}

function hideComments() {
	$('blogComments').innerHTML = '';
}

function returnGetComments(string) {
	$('blogComments').innerHTML = string;
}

function deleteComment(cID) {
	x_deleteComment(cID,returnDeleteComment);
}

function returnDeleteComment(cID) {
	var rowID = 'cRow'+cID;
	$(rowID).style.display = 'none';
}

function gotoPage(newPage) {
	var sort = $('sortFilterBy').value;
	var acdc = $('sortFilterAcDc').value;
	var limit = '';
	var startPoint = '';
	var search = '';
	x_getBlogPosts(sort,acdc,limit,startPoint,newPage,search,returnGotoPage);
}

function returnGotoPage(stuff) {
	// Parse returned stuff
	var stringArray = stuff.split("~~|~~");
	var blogDisplay = stringArray[0];
	var pages       = stringArray[1];
	var paginate1   = stringArray[2];
	var totalBlogs  = stringArray[3];
	
	$('blogDisplay').innerHTML = blogDisplay;
	$('paginate1').innerHTML   = paginate1;
	$('totalBlogs').innerHTML = '('+totalBlogs+' posts)';
}

// Handles records per page select menu
function recordsPerPage(newRecordsPerPage) {
	var sort = $('sortFilterBy').value;
	var acdc = $('sortFilterAcDc').value;
	var limit = newRecordsPerPage;
	var startPoint = '';
	var search = '';
	var newPage = '';
	x_getBlogPosts(sort,acdc,limit,startPoint,newPage,search,returnRecordsPerPage);
}

function returnRecordsPerPage(stuff) {
	// Parse returned stuff
	var stringArray = stuff.split("~~|~~");
	var blogDisplay = stringArray[0];
	var pages       = stringArray[1];
	var paginate1   = stringArray[2];
	var totalBlogs  = stringArray[3];
	
	$('blogDisplay').innerHTML = blogDisplay;
	$('paginate1').innerHTML = paginate1;
	$('totalBlogs').innerHTML = '('+totalBlogs+' posts)';
}

function sortFilter(sortColumn,acdc) {		
	// Check for records per page
	// First see if the records per page pull-down even exists
	if ( $('recordsPerPage1') ) {
		var rppID    = $('recordsPerPage1');
		var rppIndex = rppID.selectedIndex;
		var rppID    = rppID[rppIndex].value;
	} else {
		rppID =  0;
	}
	if ( $('pageNo1') ) {
		// Get page
		var pageID    = $('pageNo1');
		var pageIndex = pageID.selectedIndex;
		var pageID    = pageID[pageIndex].value;
	} else {
		pageID = 0;
	}	
	$('sortFilterBy').value = sortColumn;	
	$('sortFilterAcDc').value = acdc;	
	
	x_getBlogPosts(sortColumn,acdc,rppID,'',pageID,'',returnRecordsPerPage)
}