// Handles page select menu and prev/next buttons
function gotoPage(newPage) {
	var filterMonthID    = $('filterByMonth');
	var filterMonthIndex = filterMonthID.selectedIndex;
	var filterMonth      = filterMonthID[filterMonthIndex].value;
	
	var filterYearID    = $('filterByYear');
	var filterYearIndex = filterYearID.selectedIndex;
	var filterYear      = filterYearID[filterYearIndex].value;
	
	if ($('filterByProject')) {
		var filterProjectID = $('filterByProject');
		var filterProjectIndex = filterProjectID.selectedIndex;
		var filterProject = filterProjectID[filterProjectIndex].value;
	} else {
		var filterProject = 0;
	}
	
	var filterCategoryID    = $('filterByCategory');
	var filterCategoryIndex = filterCategoryID.selectedIndex;
	var filterCategory      = filterCategoryID[filterCategoryIndex].value;
	
	var filterString = filterMonth+'|'+filterYear+'|'+filterProject+'|'+filterCategory;
	var sort       = '';
	var ad         = '';
	var limit      = '';
	var startPoint = '';
	var search     = '';
	
	x_getExpenseItems(sort,ad,limit,startPoint,newPage,search,filterString,returnDisplayExpenses);
}

// Handles records per page select menu
function recordsPerPage(newRecordsPerPage) {
	var filterMonthID    = $('filterByMonth');
	var filterMonthIndex = filterMonthID.selectedIndex;
	var filterMonth      = filterMonthID[filterMonthIndex].value;
	
	var filterYearID    = $('filterByYear');
	var filterYearIndex = filterYearID.selectedIndex;
	var filterYear      = filterYearID[filterYearIndex].value;
	
	if ($('filterByProject')) {
		var filterProjectID = $('filterByProject');
		var filterProjectIndex = filterProjectID.selectedIndex;
		var filterProject = filterProjectID[filterProjectIndex].value;
	} else {
		var filterProject = 0;
	}
	
	var filterCategoryID    = $('filterByCategory');
	var filterCategoryIndex = filterCategoryID.selectedIndex;
	var filterCategory      = filterCategoryID[filterCategoryIndex].value;
	
	var filterString = filterMonth+'|'+filterYear+'|'+filterProject+'|'+filterCategory;

	var sort       = '';
	var ad         = '';
	var limit      = newRecordsPerPage;
	var startPoint = '';
	var search     = '';
	var newPage    = '';
	x_getExpenseItems(sort,ad,limit,startPoint,newPage,search,filterString,returnDisplayExpenses);
}

function sortFilter(sortColumn,acdc) {
	// Read filter selects	
	// Month
	var monthID    = $('filterByMonth');
	var monthIndex = monthID.selectedIndex;
	var monthID    = monthID[monthIndex].value;
	
	// Year
	var yearID    = $('filterByYear');
	var yearIndex = yearID.selectedIndex;
	var yearID    = yearID[yearIndex].value;
	
	// Project
	if ($('filterByProject')) {
		var projectID = $('filterByProject');
		var projectIndex = projectID.selectedIndex;
		var projectID = projectID[projectIndex].value;
	} else {
		var projectID = 0;
	}	
	
	// Category
	var categoryID    = $('filterByCategory');
	var categoryIndex = categoryID.selectedIndex;
	var categoryID    = categoryID[categoryIndex].value;
		
	var filterString  = monthID+'|'+yearID+'|'+projectID+'|'+categoryID; 
	x_getExpenseItems(sortColumn,acdc,'','','','',filterString,returnDisplayExpenses);
}

function returnDisplayExpenses(string) {
	var stringArray = string.split("~~|~~");
	var expenseDisplay = stringArray[0];
	var pages = stringArray[1];
	var paginate1 = stringArray[2];
	var expTotal  = stringArray[3];
	var windowTitle = stringArray[4];
	
	$('expDisplay').innerHTML = expenseDisplay;
	$('paginate1').innerHTML = paginate1;
	$('expTotal').innerHTML = 'Total: $'+expTotal;
	$('expWindowTitle').innerHTML = windowTitle;
}

function returnDeleteExpense(expID) {
	$('expenseRow_'+expID).style.display = 'none';
}