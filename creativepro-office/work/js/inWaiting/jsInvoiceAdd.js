function calcItem(inputValue,inputID) {
	// Do we have a real number?
	if (checkNaN(inputValue,inputID)) {
		// If we do, proceed...
		
		// Get element type
		var idArray = inputID.split('_');
		var itemRowNumber = idArray[1];
		
		// See which items have values
		var qty       = $('invoiceItemQty_'+itemRowNumber).value;
		var rate      = $('invoiceItemRate_'+itemRowNumber).value;
		var itemTotal = $('invoiceItemTotal_'+itemRowNumber).value;
		
		// Do we have any undefined fields
		if ( (qty==undefined) || (qty=='') ) {
			qty = 0;
			$('invoiceItemQty_'+itemRowNumber).value = 0;
		}
		if (rate==undefined || (rate=='') ) {
			rate = 0;
			$('invoiceItemRate_'+itemRowNumber).value = 0;
		}
		if ( (itemTotal==undefined) || (itemTotal=='') ) {
			itemTotal = 0;
			$('invoiceItemTotal_'+itemRowNumber).value = 0;
		}
		// Now we have 2 options:
		// 1. Do we calculate qty*rate to get itemTotal, or
		// 2. Was itemTotal entered and we need to check it agains qty*rate for accuracy?
		
		// Just calulate itemTotal
		if ( (rate>0) && (qty>0) ) {
			rate      = parseFloat(rate);
			qty       = parseFloat(qty);			
			itemTotal = Number(qty*rate);
		} else if ( (rate>0) && (qty==0) ) {
			rate      = parseFloat(rate);
			qty       = parseFloat(qty);			
			itemTotal = Number(qty*rate);
		} else if ( (rate==0) && (qty>0) ) {
			rate      = parseFloat(rate);
			qty       = parseFloat(qty);			
			itemTotal = Number(qty*rate);
		} else {
			itemTotal = Number(itemTotal);			
		}	
		
		$('invoiceItemTotal_'+itemRowNumber).value = formatCurrency(itemTotal);	
		
		// Calculate the invoice total		
		calcInvoiceTotal();		
		return itemTotal;
	} else {
		// Display NaN error message
		
		return false;
	}// if checkNaN()	
}

function calcInvoiceTotal() {
	// This only calculates the total in the DOM, it does not
	// update the database total
	var taxRate         = parseFloat(eval($('taxRate').value));
	var shipping        = parseFloat(eval($('shipping').value));
	var taxAmt          = parseFloat(eval($('taxAmt').value));
	
	// Calculate invoice subTotal from itrmTotals
	var invoiceSubTotal = 0;
	itemTotals = document.getElementsByClassName('invoiceItemTotal');
	for(i = 0; i < itemTotals.length; i++) {
		invoiceSubTotal = parseFloat(invoiceSubTotal);
		itemsTotal      = parseFloat(itemTotals[i].value);
		invoiceSubTotal = Number(invoiceSubTotal+itemsTotal);
	}
	
	// Add shipping to subTotal
	if (shipping>0) {
		var invoiceSubTotal = Number(invoiceSubTotal+shipping);
	}
	
	// Now, calculate tax
	if (taxRate>0) {
		var taxRateDec = Number(taxRate/100);
		var taxTotal   = Number(invoiceSubTotal*taxRateDec);
		var taxTotalDisplay = formatCurrency(taxTotal);
		var invoiceGrandTotal = Number(invoiceSubTotal+taxTotal);
	} else {
		var invoiceGrandTotal = invoiceSubTotal;
	}	
	
	$('taxTotal').innerHTML = taxTotalDisplay;
	$('taxAmt').value       = taxTotal;
	$('invoiceTotal').innerHTML = formatCurrency(invoiceGrandTotal);
}

function editInvoiceItem(editWhat,itemID) {
	// Get the field for what we're editing
	var editWhatArray = editWhat.split("_");
	var fieldName     = editWhatArray[0];
	var rowNo         = editWhatArray[1];
	var fieldValue    = $(fieldName+'_'+rowNo).value;
	if (fieldName == 'invoiceItemType') {
		var typeID      = $('invoiceItemType_'+rowNo);
		var typeIDIndex = typeID.selectedIndex;
		var fieldValue  = typeID[typeIDIndex].value;	
	}
	
	$('invoiceItemQty_'+rowNo).style.background = '';
	$('invoiceItemRate_'+rowNo).style.background = '';
	$('invoiceItemTotal_'+rowNo).style.background = '';
	$('error').innerHTML = '';
		
	if (fieldName == 'invoiceItemDesc') {
		// Don't worry about any recalculations
		x_editInvoiceItem(fieldValue,fieldName,rowNo,itemID,returnEditInvoiceItem);	
			
	} else if (fieldName == 'invoiceItemType') { 
		// Don't worry about any recalculations
		x_editInvoiceItem(fieldValue,fieldName,rowNo,itemID,returnEditInvoiceItem);
		
	} else {
		calcItem(fieldValue,editWhat);
		if (fieldName == 'invoiceItemTotal') {
			// Get the calculated value of itemTotal from the DOM
			var itemTotal = $('invoiceItemTotal_'+rowNo).value;
			fieldValue = itemTotal;
		}
		x_editInvoiceItem(fieldValue,fieldName,rowNo,itemID,returnEditInvoiceItem);			
	} 
}

function returnEditInvoiceItem(string) {
}

function addInvoiceItem(invID,i) {
	var invoiceItemDesc  = removeChar($('invoiceItemDesc_'+i).value);
	var qty              = $('invoiceItemQty_'+i).value;
	var rate             = $('invoiceItemRate_'+i).value;
	var invoiceItemTotal = $('invoiceItemTotal_'+i).value;
	
	var typeID      = $('invoiceItemType_'+i);
	var typeIDIndex = typeID.selectedIndex;
	var typeID      = typeID[typeIDIndex].value;
	var taxAmt      = $('taxAmt').value;
	var taxRate     = $('taxRate').value;
	var shipping    = $('shipping').value;
	
	rate           = parseFloat(rate);
	qty            = parseFloat(qty);
	invoiceItemTotal = parseFloat(invoiceItemTotal);
	
	if (typeof invID == 'Undefined') { invID = 0; }
	x_addInvoiceItem(invID,invoiceItemDesc,rate,qty,typeID,invoiceItemTotal,taxAmt,taxRate,shipping,returnAddInvoiceItem);
}

function returnAddInvoiceItem(string) {
	$('invoiceItems').innerHTML = string;
	calcInvoiceTotal();
}

function confirmDeleteInvoiceItem(itemID,invID,i) {
	if (typeof invID == 'Undefined') { invID = 0; }
	var OK = confirm("Are you sure you want to delete this invoice item?");
	if (OK == true) {
		deleteInvoiceItem(itemID,invID,i);
	}
}

function deleteInvoiceItem(itemID,invID,i) {
	x_deleteInvoiceItem(itemID,invID,returnAddInvoiceItem);	
}

function specifyLateFee() {
	var lateID      = document.getElementById('lateFee');
	var lateIDIndex = lateID.selectedIndex;
	var lateID      = lateID[lateIDIndex].value;
	if (lateID == 'Specific Amount') {
		$('specifyLateFee').innerHTML = '<input type="text" name="sLateFee" id="sLateFee" style="width: 75px;" value="0.00" onFocus="this.value=\'\'">';
	}	
}

function checkTax(taxRate,currencyHTML,edit) {
	var rowTax        = $('rowTax');
	var taxRateDiv    = $('displayTaxRate');
	var taxRateHidden = $('taxRate'); 
	var taxAmtHidden  = $('taxAmt');
	if (taxRate>0) { 
		rowTax.style.display = '';
		taxRateDiv.innerHTML = taxRate;
		taxRateHidden.value  = taxRate;
	} else {
		// Make sure row is not visible and taxRateHidden, taxAmtHidden = 0
		taxRateHidden.value  = 0;
		taxAmtHidden.value   = 0;
		rowTax.style.display = 'none';
	}
	
	calcInvoiceTotal();
	// Save off tax amount to database if in edit mode
	if (edit==1) {
		var invID = $('invID').value;
		var taxAmt = taxAmtHidden.value;
		x_saveTax(invID,taxRate,taxAmt,returnSaveTaxShipping);
	}
}

function checkShipping(shippingAmt,currencyHTML,edit) {
	var rowShipping = $('rowShipping');
	var shippingDiv = $('displayShipping');
	var shipping    = $('shipping');
	if (shippingAmt>0) { 
		rowShipping.style.display = '';
		shippingDiv.innerHTML = currencyHTML+formatCurrency(shippingAmt);
		shipping.value        = shippingAmt;
		
	} else {
		// Make sure row is not visible and hidden field = 0
		shipping.value = 0;
		rowShipping.style.display = 'none';
	}
	
	// Save off shipping amount to database if in edit mode
	if (edit==1) {
		var invID = $('invID').value;
		x_saveShipping(invID,shippingAmt,returnSaveTaxShipping);
	}
	calcInvoiceTotal();
}

function returnSaveTaxShipping() {

}