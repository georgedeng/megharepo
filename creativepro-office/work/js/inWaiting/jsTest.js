var getDataObj = {
	getSomeData : function() {
		$.ajax({
		    type: "POST",
		    url:  'index.php/welcome/test/',
		    data: 'testPost=Hi there',
		    success: function(msg){		    	
				$('#ajaxResults').html(msg);
		    },
		    failure: function() {
		    	
		    }
	    });
	}
}

$(document).ready(function(){	
	/*
	 * Attach event handlers
	 */
	$('#buttonTest').click(function() {
		getDataObj.getSomeData();
		return false;
	});	
	
	
});		