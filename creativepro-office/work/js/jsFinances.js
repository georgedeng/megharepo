var selectedItem = null;
var selectedItemType = null;
var selectedSection = 'ledger';

getFinanceDataObj = {
    getInvoiceList : function(itemID,group,tag,status,searchTerm) {
		if (tag == '' || typeof tag == 'undefined') {
			tag = 0;
		}
        if (searchTerm == '' || typeof searchTerm == 'undefined') {
			searchTerm = 0;
		}
        if ((status == '' || typeof status == 'undefined') && status != '0') {
			status = 'null';
		}
        
        var dateStart = null, dateEnd = null;
        if (typeof $('#invoiceDateStart').val() != 'undefined') {
            dateStart = genericFunctions.jsToMySQLDate($('#invoiceDateStart').val());
            dateEnd   = genericFunctions.jsToMySQLDate($('#invoiceDateEnd').val());
        }
        
        var hash = {
                    filter    : 1,
                    dateStart : dateStart,
                    dateEnd   : dateEnd,
                    term      : searchTerm
                    }
        
        $.ajax({
            type:  "POST",
            url:   siteURL+'finances/FinanceView/getInvoiceList/'+itemID+'/'+group+'/'+tag+'/'+status+'/json/0',
            dataType: 'json',
            data:     hash,
            success: function(payload){
                var jsonObject   = payload;
                invoiceArray = jsonObject.Invoices;
                renderFinanceDataObj.renderInvoiceGrid(jsonObject);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
	},
    getLedger : function(dateStart,dateEnd,itemID,group,tag,searchTerm) {
		if (tag == '' || typeof tag == 'undefined') {
			tag = 0;
		}
        if (searchTerm == '' || typeof searchTerm == 'undefined') {
			searchTerm = 0;
		}
        if (group == '' || typeof group == 'undefined') {
			group = 'user';
		}
        if (itemID == '' || typeof itemID == 'undefined') {
			itemID = '0';
		}
        
        var hash = {
            term: searchTerm
        }
        
        $.ajax({
            type:  "POST",
            url:   siteURL+'finances/FinanceView/getLedger/'+dateStart+'/'+dateEnd+'/'+itemID+'/'+group+'/'+tag+'/json/0',
            dataType: 'json',
            data: hash,
            success: function(payload){
                var jsonObject   = payload;
                renderFinanceDataObj.renderLedgerGrid(jsonObject);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
	},
	getInvoiceDetails : function(invoiceID,renderType) {
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'finances/FinanceView/getInvoiceDetails/'+invoiceID+'/'+renderType+'/0',
		    success: function(payload){
				if ($('#pageLeftColumn').attr('pageID') == 'clientDashboard') {
		    		renderFinanceDataObj.renderInvoiceForClient(payload,invoiceID);
		    	} else {
		    		renderFinanceDataObj.renderInvoiceForOwner(payload,invoiceID);
		    	}
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
    getInvoiceListForSelect : function(clientID,projectID,status,selectID,toggleContainerDisplay) {
        $.ajax({
		    type:     "POST",
		    url:      siteURL+'finances/FinanceView/getInvoiceListForSelect/'+clientID+'/'+projectID+'/'+status+'/json/0',
            dataType: 'json',
		    success: function(payload) {
                $('#'+selectID).html('<option value="0">'+commonTerms['select']+'</option>');
                var invoiceObj = payload;
                if (invoiceObj.length>0) {
                    for(var a in invoiceObj) {
                        var invoiceSelectText = invoiceObj[a].InvNo+' '+invoiceObj[a].Title;
                        optionString = '<option value="'+invoiceObj[a].InvoiceID+'">'+invoiceSelectText+'</option>';
                        $('#'+selectID).append(optionString);
                    }
                    if (toggleContainerDisplay == true) {
                        $('#'+selectID).show();
                    }
                } else {
                    if (toggleContainerDisplay == true) {
                        $('#'+selectID).hide();
                    }
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getInvoicePayments : function(invoiceID) {
        $.ajax({
		    type:     "POST",
		    url:      siteURL+'finances/FinanceView/getInvoicePayments/'+invoiceID+'/json/0',
            dataType: 'json',
		    success: function(payload) {
                renderFinanceDataObj.renderInvoicePayments(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
        });
    }
}

renderFinanceDataObj = {
    renderLedgerGrid : function(jsonObject) {
        var stringOut = [];
        
        var ledgerItems = jsonObject.Ledger.length;
        var ledger = jsonObject.Ledger;
        
        var totalIncome  = jsonObject.TotalIncome;
        var totalExpense = jsonObject.TotalExpense;
        
        if (ledgerItems<1) {
            stringOut.push('<div class="infoMessageBig">'+commonTerms['noLedgerDateRange']+'</div>');
            var stringOutRender = stringOut.join('');
            $('#ledgerInformationContainer')
                .removeClass('activityMessageBig')
                .html(stringOutRender);
        } else {
            var totalInvoiced = 0, totalRevenue = 0, totalReceivables = 0, totalExpenses = 0;
            
            for(a=0;a<=((ledgerItems)-1);a++) {
                var item = ledger[a];
                
                var label, numberFormat, totalExpense = 0, totalIncome = 0, linkString = '', linkClass = '';
                switch (item.ItemType) {
                    case 'Income':
                        numberFormat = '<span class="bigText">'+commonTerms['currencySymbol']+item.Total+'</span>';
                        totalIncome = item.Total;
                        if (item.Company != null) {
                            linkString += '<a href="'+siteURL+'/clients/ClientDetail/index/'+item.ClientID+'">'+item.Company+'</a>&nbsp;|&nbsp;';
                        }
                        if (item.ProjectTitle != null) {
                            linkString += '<a href="'+siteURL+'/projects/ProjectDetail/index/'+item.ProjectID+'">'+item.ProjectTitle+'</a>';
                        }
                        break;
                    case 'Expense':
                        numberFormat = '<span class="bigText red">'+commonTerms['currencySymbol']+item.Total+'</span>';
                        totalExpense = item.Total;
                        if (item.Company != null) {
                            linkString += '<a href="'+siteURL+'/clients/ClientDetail/index/'+item.ClientID+'">'+item.Company+'</a>&nbsp;|&nbsp;';
                        }
                        if (item.ProjectTitle != null) {
                            linkString += '<a href="'+siteURL+'/projects/ProjectDetail/index/'+item.ProjectID+'">'+item.ProjectTitle+'</a>&nbsp;|&nbsp;';
                        }
                        if (item.Vendor != null) {
                            linkString += '<a href="'+siteURL+'/projects/ProjectDetail/index/'+item.VendorID+'">'+item.Vendor+'</a>';
                        }
                        break;
                }
                
                stringOut.push('<div class="clearfix row ledgerRow '+item.ItemType+'" style="padding: 9px 0 9px 0;" id="'+item.ItemType+item.ItemID+'" data-itemtype="'+item.ItemType+'" data-expense="'+totalExpense+'" data-income="'+totalIncome+'" data-metaitemid="'+item.MetaItemID+'" itemid="'+item.ItemID+'">');
                stringOut.push('<div class="left" style="margin-left: 2px;"><a href="#" style="padding: 0;" class="viewMoreToggle icon_dropdown_small" title="View more"></a></div>');
                stringOut.push('<div class="mRight24 left bold" style="width: 100px; text-align: right;">'+item.DateRender+'</div>');
                stringOut.push('<div class="left" style="width: 300px;"><a href="#" class="ledgerTitle linkLedger bold">'+item.Title+'</a>');
                stringOut.push('<p class="subText">'+linkString+'</p>');
                stringOut.push('</div><div class="left amountContainer" style="width: 180px; text-align: right;">'+numberFormat+'</div>');
                stringOut.push('</div>');
                stringOut.push('<div ')
                stringOut.push('<div class="boxBlue radius hide boxArrowTop" style="position: relative; margin: 6px 0 6px 0;"></div>');
            }      
            var stringOutRender = stringOut.join('');
            $('#ledgerInformationContainer')
                .removeClass('activityMessageBig')
                .html(stringOutRender)
                .alternateContainerRowColors();
                
            $('#ledgerInformationContainer').hoverButtons({
                elementClass:     'ledgerRow',
                editClass:        'ledgerUpdate',
                deleteClass:      'ledgerDelete',
                width:            '70px'
            });
            
            $('.viewMoreToggle').click(function() {
                var obj = $(this);
                var getData = false;
                
                if (obj.hasClass('icon_dropdown_small') && !obj.hasClass('hasData')) {
                    obj.removeClass('icon_dropdown_small').addClass('icon_pushup_small');
                    getData = true;
                } else {
                    obj.removeClass('icon_pushup_small').addClass('icon_dropdown_small');
                }
                
                var parentRow = obj.parent().parent();
                var moreRow = parentRow.next();
                moreRow.toggle('fast');
                var itemType = parentRow.data('itemtype');
                var itemID = parentRow.data('metaitemid');
                if (getData == true) {
                    obj.addClass('hasData');
                    if (itemType == 'Income') {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url:  siteURL+'finances/FinanceView/getInvoicePayments/'+itemID+'/json/0',
                            success: function(payload) {
                               renderFinanceDataObj.renderInvoicePayments(payload, moreRow);
                            },
                            error: function (xhr, desc, exceptionobj) {
                                errorObj.ajaxError(xhr,desc,exceptionobj);
                            }
                        });
                    } else if (itemType == 'Expense') {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url:  siteURL+'finances/ExpenseView/getExpenseDetails/'+itemID+'/json/0',
                            success: function(payload) {
                                var comments = payload.Comments;
                               if (comments == '') {
                                   comments = 'Memo or comments.';
                               }
                               moreRow.html('<textarea style="width: 500px; height: 40px; padding: 6px; margin: 6px;" data-itemid="'+itemID+'" class="expenseMemo">'+comments+'</textarea>'); 
                            },
                            error: function (xhr, desc, exceptionobj) {
                                errorObj.ajaxError(xhr,desc,exceptionobj);
                            }
                        });
                    }
                }    
                return false;
            });
            
            /*
             * Calculate our totals
             */
            financeSupportObj.calculateLedgerTotals();
            
        }
    },
	renderInvoiceGrid : function(jsonObject) {
		var invoices      = jsonObject.Invoices;
		var totalInvoices = invoices.length;		
		var stringOut     = [];
        
        if (totalInvoices<1) {
            stringOut.push('<div class="infoMessageBig">'+commonTerms['noInvoicesDateRange']+'</div>');
        } else {
            var headerArray   = jsonObject.HeaderLabels.split('|');
            var footerArray   = jsonObject.FooterLabels.split('|');
            var totalInvoiced = jsonObject.TotalInvoiced;
            var totalDue      = jsonObject.TotalDue;
            var totalLabel    = footerArray[0];
            var totalDueLabel = footerArray[1];
            stringOut.push('<input type="hidden" id="acdcInvoiceGrid" value="asc" />');

            stringOut.push('<table class="dataTable stripes" id="invoiceGrid">');
            stringOut.push('<thead><tr>');
            stringOut.push('<th style="width: 20px;"></td>');
            stringOut.push('<th style="width: 80px; text-align: right; padding-right: 15px;" acdc="1" class="sortable sort-date">'+headerArray[3]+'</th>');
            stringOut.push('<th style="width: 276px;" acdc="1" class="sortable sort-alpha">'+headerArray[1]+'</th>');
            stringOut.push('<th style="width: 40px;" acdc="1" class="sortable sort-alpha">'+headerArray[4]+'</th>');
            stringOut.push('<th style="width: 60px; text-align: right;" acdc="1" class="sortable sort-currency">'+headerArray[5]+'</th>');
            stringOut.push('<th style="width: 60px; text-align: right;" acdc="1" class="sortable sort-currency">'+headerArray[6]+'</th>');
            stringOut.push('<th style="width: 72px; max-width: 72px;"></th>');
            stringOut.push('</tr></thead>');
            stringOut.push('<tbody>');
            for(a=0;a<=((invoices.length)-1);a++) {
                var invID              = invoices[a].InvoiceID;
                var invNo              = invoices[a].InvNo;
                var invTitle           = invoices[a].Title;
                var clientID           = invoices[a].ClientID;
                var invClient          = invoices[a].Company;
                var invDateEntered     = invoices[a].DateEntry;
                var invDateEnteredSort = invoices[a].DateEntrySort;
                var invStatus          = invoices[a].Status;
                var invStatusHuman     = invoices[a].StatusHuman;
                var invStatusMachine   = invoices[a].StatusMachine;
                var invTotal           = invoices[a].InvTotal;
                var invTotalSort       = invoices[a].InvoiceTotal;

                var invAmtDue          = invoices[a].AmtDue;
                var invAmtDueSort      = invoices[a].AmountDue;
                var totalMessages      = invoices[a].Messages;
                var totalUnreadMessages= invoices[a].UnreadMessages;
                var recurring          = invoices[a].Recurring;
                var projectTitle       = invoices[a].Project;
                var projectID          = invoices[a].ProjectID;
                var estimate           = invoices[a].Estimate;
                var client;
                
                var typeLabel = 'Invoice';
                var typeBG = 'light-green';
                var itemType = 'invoice';
                
                if (estimate == '1') {
                    typeLabel = 'Estimate';
                    typeBG = 'light-blue';
                    itemType = 'estimate';
                    invAmtDue = commonTerms['currencySymbol']+'0.00';
                    invAmtDueSort = '0.00';
                }
                
                var status = '';
                switch(invStatusMachine) {
                    case 'status_open_created_on':
                        status = 'Open';
                        break;
                    case 'status_open_sent_on':
                        status = 'Open';
                        break;    
                    case 'status_closed_paid_on':
                        status = 'Closed';
                        break;    
                    case 'status_open_overdue':
                        status = 'Overdue';
                        break;    
                }

                stringOut.push('<tr id="invRow'+invID+'" itemID="'+invID+'" data-type="'+itemType+'" data-amount="'+invTotalSort+'" data-totaldue="'+invAmtDueSort+'" class="'+status+' invoiceRow">');
                stringOut.push('<td><a href="#" style="padding: 0;" class="viewMoreToggleInvoices icon_dropdown_small" title="View more"></a></td>');
                stringOut.push('<td dateSort="'+invDateEnteredSort+'" style="text-align: right; padding-right: 12px; padding-top: 0;" class="bold">'+invDateEntered);
                stringOut.push('<span style="float: right; display: block; margin-top: 3px;" class="colorBackground label '+typeBG+'">'+typeLabel+'</span></td>');
                stringOut.push('<td textSort="'+invTitle+'"><a href="#" class="linkInvoice bold">'+invNo+' '+invTitle+'</a>');
                if (totalMessages>0) {
                    stringOut.push('<span class="icon_comment_small_left" title="'+commonTerms['messages']+': '+totalMessages+'" style="margin: 0pt 0pt 0pt 6px;"></span>');
                }
                if (recurring !=  null && recurring.length>0) {
                    stringOut.push('<span class="iconRecurring clickable" data="'+recurring+'" title="" style="margin: 0pt 0pt 0pt 6px;"></span>');
                }
                stringOut.push('<br />');
                if (invClient != null && invClient != 'null') {
                    stringOut.push('<a href="'+siteURL+'clients/ClientDetail/index/'+clientID+'" class="subText">'+invClient+'</a></span>');
                    client = true;
                }
                if (projectTitle != null && projectTitle != 'null') {
                    if (client == true) {
                        stringOut.push('&nbsp;<span class="subText">|</span>&nbsp;');
                    }
                    stringOut.push('<a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'" class="subText">'+projectTitle+'</a>');
                }
                stringOut.push('</td>');
                stringOut.push('<td textSort="'+invStatusMachine+'" style="text-align: center;">');
                stringOut.push('<span class="'+invStatusMachine+' status popUpMenuClick" id="invoice_'+invID+'" data-statustext="'+invStatusHuman+'" content="statusContent"></span>');
                stringOut.push('</td>');
                stringOut.push('<td style="text-align: right;" currencySort="'+invTotalSort+'" class="amountTotal bigText">'+invTotal+'</td>');
                stringOut.push('<td style="text-align: right;" class="amountDue" currencySort="'+invAmtDueSort+'">'+invAmtDue+'</td>');
                stringOut.push('<td style="style="width: 72px; max-width: 72px; position: relative; padding-left: 12px;" class="controls"></td>');
                stringOut.push('</tr>');
                
                stringOut.push('<tr class="moreRow hide"><td colspan="7"><div class="boxBlue radius infoContainer boxArrowTop" style="position: relative; margin: 6px 0 6px 0;"></div></td></tr>');
            }
            stringOut.push('</tbody></table>');
            stringOut.push('<div id="statusContent" class="tooltip"></div>');
        }
		var stringOutRender = stringOut.join('');
		$('#invoiceInformationContainer').removeClass('activityMessageBig').html(stringOutRender);
		$('#invoiceGrid')
			.hiliteTableRow()
			.tableSorter();
		
		statusObj.statusPopup();        
		pageInitObj.popUpMenus();
        financeSupportObj.createRecurringPopups();
        
        $('.viewMoreToggleInvoices').click(function() {
            var obj = $(this);
            var getData = false;

            if (obj.hasClass('icon_dropdown_small') && !obj.hasClass('hasData')) {
                obj.removeClass('icon_dropdown_small').addClass('icon_pushup_small');
                getData = true;
            } else {
                obj.removeClass('icon_pushup_small').addClass('icon_dropdown_small');
            }

            var parentRow = obj.parent().parent();
            var moreRow = parentRow.next();
            var container = moreRow.find('.infoContainer');
            moreRow.toggle('fast');
            var itemID = parentRow.attr('itemid');
            
            if (getData == true) {
                obj.addClass('hasData');
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url:  siteURL+'finances/FinanceView/getInvoicePayments/'+itemID+'/json/0',
                    success: function(payload) {
                        if (payload.Payments == 0) {
                            container.html('<p style="margin: 12px;"><strong>No payments right now.</strong></p>');
                        } else {
                            renderFinanceDataObj.renderInvoicePayments(payload, container);
                        }    
                    },
                    error: function (xhr, desc, exceptionobj) {
                        errorObj.ajaxError(xhr,desc,exceptionobj);
                    }
                });
            }    
            
            return false;
        });

		/*
		 * Create our row menus
		 */
		$('#invoiceGrid').hoverButtons({
               elementClass:     'invoiceRow',
               editClass:        'invoiceUpdate',
               deleteClass:      'invoiceDelete',
               insertionElement: 'controls',
               position:         '',
			   insertion:        'prepend',
			   pdfOption:        true
         });

		/*
		 * Init search date pickers
		 */
		$(".dateField").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

        /*
         * Turn off edit/delete buttons
         */
        if($('#pageLeftColumn').attr('pageID') == 'clientDashboard') {
            $('#invoiceGrid .buttonDelete').hide();
            $('#invoiceGrid .buttonEdit').hide();
        }

        /*
         * Permissions
         */
        permissionsObj.renderControlsBasedOnPermissions();
        financeSupportObj.calculateInvoiceTotals();
	},    
	renderInvoiceForClient : function(invoiceHTML,invoiceID) {
		$('#invoiceInformationContainer').hide();
		$('#invoiceDetailsContainer').show();
		$('#invoiceMessageContainer').show();
		$('#buttonCloseInvoiceDetails').show();
        $('#invoicePaymentsContainer').show();
        $('#paymentFormContainer').hide();
        
		$('#invoiceDetailsContainer').html(invoiceHTML);
		$('#newMessageInvoice').attr('itemID',invoiceID);
        $('#fileUploadingContainer').hide();
	},
	renderInvoiceForOwner : function(invoiceHTML,invoiceID) {
        $('html, body').animate({
            scrollTop: $("#pageTitleContainer").offset().top
        }, 300);
        
        $('#tabsFinances').fadeOut('fast');
        $('#itemDetailContainer').fadeIn('fast', function() {
            $('#itemDetailContents').html(invoiceHTML);
            $('#invoiceControls').show();
            $('#expenseControls').hide();
            $('#messageList').html('');
            $('#invoiceID').val(invoiceID);
            
           /*
            * Swap side-bar panels to show invoice status.
            */
            $('#sidebarLedger').hide();
            $('#sidebarInvoices').hide();
            $('#tagsContainer').hide();
            
            if ($('#estimate').val() == 1) {
                $('#buttonViewPayments').hide();
                $('#buttonConvertToInvoice').show();
                
                var statusHuman = 'Estimate';
                var statusBGClass = 'light-blue';
            } else {
                $('#buttonViewPayments').show();
                $('#buttonConvertToInvoice').hide();
                
                var statusHuman = $('#statusHuman').val();
                var statusMachine = $('#statusMachine').val();
                var statusBGClass = financeSupportObj.getStatusBGColor(statusMachine);
            }
            if ($('#estimate').val() == '1') {
                $('#invoiceStatus').removeClass().addClass('colorBackground light-blue bold').html(statusHuman);
            } else {
                $('#invoiceStatus').removeClass().addClass(statusBGClass+' colorBackground bold').html(statusHuman);
            }    
            
            var invoiceNumber = $('#invoiceNumber').val();
            financeSupportObj.setInvoiceIDs(invoiceID,invoiceNumber);
            
            /*
             * Initialize message box
             */
            $('#newMessageContainer').attr('itemID',invoiceID);
            $('#newMessageContainer').attr('itemtype','invoice');

            var clientData = '[{"NameFull":"'+$('#clientCompany').val()+'","PID":"'+$('#clientID').val()+'","UID":"'+$('#clientUID').val()+'","Language":"'+$('#clientLanguage').val()+'","Type":"client"}]';
            initMessageBox.createMessageBox(clientData,$('#newMessageContainer'),$('#itemMessageConents'),false,true,true,true,true,true);
            getMessageObj.getMessages(invoiceID,'invoice',$('#itemMessageConents'),0,10);
            
            /*
             * Go get files for this invoice.
             * We're not going to allow file attachment to invoices...I don't think.
             */
            
            /*
            $('#invFile_fileContainer').html('');
            attachedFilesView = true;
            getFileDataObj.getFilesForSomething(invoiceID,'invoice','#invFile_fileContainer');
            */
            $('#fileUploadingContainer').hide();
        });
                
        genericFunctions.togglePageHeaderButtons('#buttonEdit','on');
        genericFunctions.togglePageHeaderButtons('#buttonDelete','on');
        genericFunctions.togglePageHeaderButtons('#buttonPrint','on');
        
        genericFunctions.togglePageHeaderButtons('#buttonAddInvoice','off');
        genericFunctions.togglePageHeaderButtons('#buttonAddEstimate','off');
        genericFunctions.togglePageHeaderButtons('#buttonAddExpense','off');
        
        $('#buttonPrint').attr('itemID',invoiceID);
        $('#buttonPrint').attr('printWhat','Invoice');

        $('#buttonEdit').click(function() {
            var invoiceID = $('#invoiceID').val();
            window.location = siteURL+'finances/InvoiceUpdate/index/'+invoiceID;
        });

        $('#buttonDelete').click(function() {
            var invoiceID = $('#invoiceID').val();
            updateTrashObj.sendToTrash('invoice',invoiceID);
            $('#invRow'+invoiceID).fadeOut('slow');
            $('#invoicePaymentsContainer').hide();
            financeSupportObj.showInvoiceList();
            return false;
        }); 

        /*
         * Invoice payment init
         */
        payInvoiceObj.resetPaymentSection();
        
        /*
         * Permissions
         */
        permissionsObj.renderControlsBasedOnPermissions();
    },
    renderInvoicePayments : function(jsonObject,renderContainer) {
        if (!renderContainer) {
            renderContainer = $('#paymentContainer');
        }
        renderContainer.html('');
        var payments = jsonObject['Payments'];
        var numberOfPayments = payments.length;
        var outString = [];
        for(var a=0;a<=(numberOfPayments-1);a++) {
            var payID      = payments[a].PaymentID;
            var payType    = payments[a].PaymentType;
            var payAcct    = payments[a].AcctCheckNo;
            var payDate    = payments[a].DatePayment;
            var payComment = payments[a].PaymentComment;
            var payAmount  = payments[a].PaymentAmount;
            var payAmt     = payments[a].PaymentAmt;
            var payString = payType;
            if (payAcct != '' && payAcct != null) {
                payString += ' '+payAcct;
            }
            if (payComment == '' || payComment == null) {
                payComment = '';
            }
            outString.push('<p id="payRow_'+payID+'"><a href="#" class="buttonDeleteSmall deletePayment" itemID="'+payID+'" itemAmt="'+payAmt+'" title="'+commonTerms['delete']+'"></a>');
            outString.push('<span style="display: inline-block; width: 20%;"><strong>'+payAmount+'</strong></span>');
            outString.push('<span style="display: inline-block; width: 25%;">'+payDate+'</span>');
            outString.push('<span style="display: inline-block; width: 45%;">'+payString+'</span><br />');
            outString.push('<span style="display: block; margin-left: 20px;">'+payComment+'</span>');
            outString.push('</p>');
        }
        var renderString = outString.join('');
        renderContainer.html(renderString);

        $('.deletePayment').unbind('click').click(function() {
            var payID = $(this).attr('itemID');
            var paymentAmt = $(this).attr('itemAmt');
            $('#payRow_'+payID).fadeOut('fast');
            $.get(siteURL+'finances/InvoiceUpdate/deleteInvoicePayment/'+payID, function(payload) {
                var invoiceID = $('#invoiceID').val();
                payInvoiceObj.recalculateAmountDue(invoiceID, paymentAmt, 'deletePayment');
            });
            return false;
        });
    }
}

initFinanceObj = {
	initFinanceView : function() {
        financeTabs = $('#tabsFinances').tabs({
			fx: {opacity: 'toggle'},
			show: function(event,ui) {
				var panel = ui.panel.id;
                var projID = $('#projectID').val();
                var itemIDString;
                if (panel == 'windowViewLedger') {
                    selectedSection = 'ledger';
                    genericFunctions.togglePageHeaderButtons('.buttonAddInvoice','on');
                    genericFunctions.togglePageHeaderButtons('.buttonAddEstimate','on');
                    genericFunctions.togglePageHeaderButtons('.buttonAddExpense','on');
                    
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                    $('#excelFinanceReport').toggle(false);
                    $('#feedExportContainer').toggle(false);

                    $('#filterContainer').show();
                    $('#sidebarLedger').show();
                    $('#sidebarInvoices').hide();
                    $('#invoicePaymentsContainer').hide();

                    $('#tagsContainer').show();
                    $('#tagSelectContainer').show();
                    $('#tagSelectContainer2').hide();

                    //itemIDString = $('#selectReportType').val()+'|'+$('#selectReportYear').val();
                    //$('.buttonPrint').attr('itemID',itemIDString);
                    //$('.buttonPrint').attr('printWhat','Invoice');
                                        
                    initFinanceObj.attachLedgerEventHandlers();
				} else if (panel == 'windowViewInvoicesEstimates') {
                    selectedSection = 'invoices';
                    genericFunctions.togglePageHeaderButtons('.buttonAddInvoice','on');
                    genericFunctions.togglePageHeaderButtons('.buttonAddEstimate','on');
                    genericFunctions.togglePageHeaderButtons('.buttonAddExpense','on');
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                    $('#excelFinanceReport').toggle(false);
                    $('#feedExportContainer').toggle(false);

                    $('#filterContainer').show();
                    $('#sidebarLedger').hide();
                    $('#sidebarInvoices').show();
                    $('#invoicePaymentsContainer').hide();
                    
                    $('#tagsContainer').show();
                    $('#tagSelectContainer').hide();
                    $('#tagSelectContainer2').show();

                    itemIDString = $('#selectReportType').val()+'|'+$('#selectReportYear').val();
                    $('.buttonPrint').attr('itemID',itemIDString);
                    $('.buttonPrint').attr('printWhat','Invoice');
                    
                    /*
                     * Get our invoice list
                     */
                    if (invoiceArray.length == 0) {
                        var tag = $('#tag').val();
                        getFinanceDataObj.getInvoiceList(0,'user',tag,0);
                        initFinanceObj.attachInvoiceEventHandlers();						
					}
				} else if (panel == 'windowManageVendors') {
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                    var vendorContacts = eval('('+vendorData+')');
                    if (vendorContacts.length>0) {
                        var contactString = renderContactsObj.renderContacts(vendorContacts,0);
                        helperContactsObj.initContactsContainer();
                        helperContactsObj.attachContactEventHandlers();
                    }
				} else if (panel == 'windowViewFinanceReports') {
                    $('#filterContainer').hide();
                    genericFunctions.togglePageHeaderButtons('.buttonAddInvoice','on');
                    genericFunctions.togglePageHeaderButtons('.buttonEditInvoice','off');
                    genericFunctions.togglePageHeaderButtons('.buttonDeleteInvoice','off');
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','on');
                    $('#excelFinanceReport').toggle(true);
                    $('#feedExportContainer').toggle(true);
                    $('#selectReportYear').toggle(true);
                    $('#reportDateRangeContainer').toggle(false);
                    
                    $('#filterContainer').hide();
                    $('#sidebarLedger').hide();
                    $('#sidebarInvoices').hide();
                    $('#invoicePaymentsContainer').hide();

                    $('#tagsContainer').hide();

                    getReportDataObj.getReportInvoicedReceived($('#selectReportYear').val());
                    reportSupportObj.prepInputData();
                    $('.buttonPrint').attr('printWhat','ReportFinance');
                } else if (panel == 'windowMessagesInvoice') {
                    genericFunctions.togglePageHeaderButtons('.buttonEditInvoice','off');
                    genericFunctions.togglePageHeaderButtons('.buttonDeleteInvoice','off');
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                    genericFunctions.togglePageHeaderButtons('.buttonExportExcel','off');
                    genericFunctions.togglePageHeaderButtons('.buttonAddInvoice','off');
                    $('#excelFinanceReport').toggle(false);
                    $('#feedExportContainer').toggle(false);

                    $('#messageListInvoiceAll').html('');
					getMessageObj.getMessages('0','invoice',$('#messageListInvoiceAll'),0,10);
				}
			}
		});
        
        /*
         * Invoice, Expense, All buttons
         */
        $('#radioAll').click(function() {
           var element = $(this);
           if (element.is(':checked')) {
               $('.Expense').fadeIn('fast');
               $('.Income').fadeIn('fast');
           }
        });
        
        $('#radioIncome').click(function() {
           var element = $(this);
           if (element.is(':checked')) {
               $('.Expense').fadeOut('fast');
               $('.Income').fadeIn('fast');
           }
        });
        
        $('#radioExpenses').click(function() {
           var element = $(this);
           if (element.is(':checked')) {
               $('.Income').fadeOut('fast');
               $('.Expense').fadeIn('fast');
           }
        });
        
        $('#radioInvoiceAll').click(function() {
           var element = $(this);
           if (element.is(':checked')) {
               $('.Open').fadeIn('fast');
               $('.Closed').fadeIn('fast');
               $('.Overdue').fadeIn('fast');
           }
        });
        
        $('#radioInvoiceOpen').click(function() {
           var element = $(this);
           if (element.is(':checked')) {
               $('.Open').fadeIn('fast');
               $('.Closed').fadeOut('fast');
               $('.Overdue').fadeOut('fast');
           }
        });
        
        $('#radioInvoiceClosed').click(function() {
           var element = $(this);
           if (element.is(':checked')) {
               $('.Open').fadeOut('fast');
               $('.Closed').fadeIn('fast');
               $('.Overdue').fadeOut('fast');
           }
        });
        
        $('#radioInvoicePastDue').click(function() {
           var element = $(this);
           if (element.is(':checked')) {
               $('.Open').fadeOut('fast');
               $('.Closed').fadeOut('fast');
               $('.Overdue').fadeIn('fast');
           }
        });
        
        $('.closeDetails').click(function() {
            $('#itemDetailContainer').fadeOut('fast', function() {
                $('#invoiceID').val('');
                if (selectedSection == 'ledger') {
                    financeSupportObj.showLedger();
                } else if (selectedSection == 'invoices') {
                    payInvoiceObj.resetPaymentSection();
                    financeSupportObj.showInvoiceList();
                } 
                
                $('#tabsFinances').fadeIn('fast');
                $('#tagsContainer').show();
                financeSupportObj.clearItemContainers();
            });
                        
            return false;
        });

        reportSupportObj.attachReportChangeHandlers();

        /*
         * Initialize Open Flash Chart
         */
        chartInitObj.initOpenFlashChart(700,450,siteURL+'finances/FinanceReports/reportInvoicedReceived/'+$('#selectReportYear').val()+'/json/0/1','chartContainer');

        /*
         * Init messaging stuff
         */
        //initMessageBox.createMessageBox('',$('#newMessageInvoiceAll'),$('#messageListInvoiceAll'),false,true,false,true,true,true);
        
        /*
         * Init contacts stuff for vendors
         */
        helperContactsObj.initContactsForm('V');

		$('#tabsFinances').tabs({fx: {opacity: 'toggle'}});
        
        /*
		 * Init ledger search autocompleter
		 */
        $('#sLedgerAutocompleter').autocomplete({
            source: siteURL+'finances/FinanceView/searchLedger',
            select: function(event, ui) {
                if (ui.item != null) {                                  
                    var itemID = ui.item.id;
                    var itemType = ui.item.ledgerType;
                    var itemCount = ui.item.count;
                
                    if (itemCount>0) {
                        var term = ui.item.label, tag;
                        if (ui.item.type == 'tag') {
                            tag = term;
                        }    
                        getFinanceDataObj.getLedger(0, 0, itemID, ui.item.type, tag);
                    } else {
                        if (itemType == 'income') {
                            getFinanceDataObj.getInvoiceDetails(itemID,'html');
                            return false;
                        } else if (itemType == 'expense') {
                            getExpenseDataObj.getExpenseDetails(itemID, 'html')
                        }
                    }    
                }    
            },
            change : function(event, ui) {
                var tag = $('#tag').val();
                var term = $(this).val();
                if (ui.item == null) {
                    var dateStart = genericFunctions.jsToMySQLDate($('#ledgerDateStart').val());
                    var dateEnd   = genericFunctions.jsToMySQLDate($('#ledgerDateEnd').val());
                    getFinanceDataObj.getLedger(dateStart, dateEnd, 0, 'user', tag, term);
                    $(this).autocomplete('close');
                }
            }
        })
        .data('autocomplete')._renderItem = function(ul,item) {
            var itemCount = item.count;
            if (item.count>0) {
                itemCount = '['+item.count+']';
            }
            return $('<li></li>')
                .data('item.autocomplete', item)
                .append('<a><span class="'+item.icon+'">'+item.label+' '+itemCount+'</span></a>')
                .appendTo(ul);
        };
        
        $('#sLedgerAutocompleter').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                $(this).blur();
            }
        });
        
        /*
		 * Init invoice search autocompleter
		 */
        $('#sInvoiceAutocompleter').autocomplete({
            source: siteURL+'finances/FinanceView/searchInvoices',
            select: function(event, ui) {
                if (ui.item != null) {                                  
                    var itemID = ui.item.id;
                    var itemCount = ui.item.count;
                
                    if (itemCount>0) {
                        var term = ui.item.label, tag;
                        if (ui.item.type == 'tag') {
                            tag = term;
                        }    
                        getFinanceDataObj.getInvoiceList(itemID, ui.item.type, tag);
                    } else {
                        getFinanceDataObj.getInvoiceDetails(itemID,'html');
                        return false;
                    }    
                }    
            },
            change : function(event, ui) {
                var tag = $('#tag').val();
                var term = $(this).val();
                if (ui.item == null) {
                    //var dateStart = genericFunctions.jsToMySQLDate($('#ledgerDateStart').val());
                    //var dateEnd   = genericFunctions.jsToMySQLDate($('#ledgerDateEnd').val());
                    getFinanceDataObj.getInvoiceList(0, 'user', tag, 0, term);
                    $(this).autocomplete('close');
                }
            }
        })
        .data('autocomplete')._renderItem = function(ul,item) {
            var itemCount = item.count;
            if (item.count>0) {
                itemCount = '['+item.count+']';
            }
            return $('<li></li>')
                .data('item.autocomplete', item)
                .append('<a><span class="'+item.icon+'">'+item.label+' '+itemCount+'</span></a>')
                .appendTo(ul);
        };
        
        $('#sInvoiceAutocompleter').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                $(this).blur();
            }
        });
       
       /*
        * Load default ledger for this user
        */                    
        var tag = $('#tag').val();
        var dateStart = genericFunctions.jsToMySQLDate($('#ledgerDateStart').val());
        var dateEnd   = genericFunctions.jsToMySQLDate($('#ledgerDateEnd').val());    
        getFinanceDataObj.getLedger(dateStart,dateEnd,0,'user',tag);

        $('#ledgerDateStart').change(function() {
            var tag = $('#tag').val();
            var dateStart = genericFunctions.jsToMySQLDate($('#ledgerDateStart').val());
            var dateEnd   = genericFunctions.jsToMySQLDate($('#ledgerDateEnd').val()); 
            getFinanceDataObj.getLedger(dateStart,dateEnd,0,'user',tag);
        });

        $('#ledgerDateEnd').change(function() {
            var tag = $('#tag').val();
            var dateStart = genericFunctions.jsToMySQLDate($('#ledgerDateStart').val());
            var dateEnd   = genericFunctions.jsToMySQLDate($('#ledgerDateEnd').val()); 
            getFinanceDataObj.getLedger(dateStart,dateEnd,0,'user',tag);
        });
        
        $('#invoiceDateStart').change(function() {
            var tag = $('#tag').val();
            getFinanceDataObj.getInvoiceList(0,'user',tag,'all');
        });

        $('#invoiceDateEnd').change(function() {
            var tag = $('#tag').val();
            getFinanceDataObj.getInvoiceList(0,'user',tag,'all');
        });  
       
       /*
        * Header button handlers
        */
        $('#buttonAddInvoice').click(function() {
            window.location = siteURL+'finances/InvoiceUpdate';
        });
        $('#buttonAddEstimate').click(function() {
            window.location = siteURL+'finances/EstimateUpdate';
        });
        $('#buttonAddExpense').click(function() {
            window.location = siteURL+'finances/ExpenseUpdate';
        });

        $('#payInvoiceSave').click(function() {
            var invoiceID = $('#invoiceID').val();
            payInvoiceObj.savePayment(invoiceID);
        }); 

        $('#buttonViewPayments').click(function() {
            var invoiceID = $('#invoiceID').val();
            var panelState = genericFunctions.togglePanel($('#invoicePaymentsContainer'));
            if (panelState == 'open') {
                getFinanceDataObj.getInvoicePayments(invoiceID);
            }
        });
        
        $('#buttonConvertToInvoice').click(function() {
            var invoiceID = $('#invoiceID').val();
            $.ajax({
                type: "POST",
                dataType: 'json',
                url:  siteURL+'finances/FinanceView/convertEstimateToInvoice/'+invoiceID,
                success: function(payload) {
                    var jsonObject = payload;
                    var statusHuman = jsonObject[0];
                    var statusMachine = jsonObject[1];
                    var statusBGClass = financeSupportObj.getStatusBGColor(statusMachine);
                    $('#buttonConvertToInvoice').fadeOut('fast', function() {
                        $('#buttonViewPayments').show();
                        $('#invoiceStatus').addClass('statusBGClass').html(statusHuman);
                    });

                    var invRow = $('#invRow'+invoiceID);
                    invRow.find('.label').addClass('light-green').html('Invoice');
                    var amountTotal = invRow.find('.amountTotal').attr('currencySort');
                    invRow.find('.amountDue').html(commonTerms['currencySymbol']+amountTotal).attr('currencySort',amountTotal);
                    invRow.data('type','invoice');
                    invRow.data('amount',amountTotal);
                    invRow.data('totaldue',amountTotal);

                    financeSupportObj.calculateInvoiceTotals();
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        });

        $(".dateField").datepicker({
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

        $('textarea.expanding').elastic();

        $('.payexpensesave').click(function() {
            var invoiceID = $('#invoiceID').val();
            payInvoiceObj.savePayment(invoiceID,$(this));
        });  
        
        $('#filterInvoiceOpen').attr('checked',true);

        /*
         * Are we viewing an invoice?
         */
        if ($('#invoiceID').val()>0) {
            getFinanceDataObj.getInvoiceDetails($('#invoiceID').val(),'html');
            
            /*
             * Turn off file upload.
             */
            $('#fileUploadingContainer').hide();
        }
        
        /*
         * Are we viewing an expense?
         */
        if ($('#expenseID').val()>0) {
            getExpenseDataObj.getExpenseDetails($('#expenseID').val(),'html');
            
            /*
             * Turn off file upload.
             */
            $('#fileUploadingContainer').show();
        }

        emailInvoiceObj.initInvoiceEmailButton();

        /*
		 * Update tab counts
		 */
        var messageCountInvoice = $('#numberOfMessagesInvoice').val();

        if (messageCountInvoice>0) {
            $('#messageCountInvoice').html('('+messageCountInvoice+')');
        }

		$('.tabCount').each(function() {
			if ($(this).html() != '(0)') {
				$(this).removeClass('lightGray');
			}
		});

        /*
         * Should we print this invoice if we've come here from
         * creating a new invoice
         */
        if ($('#printInvoice').val() == '1') {
            printObj.goPrint($('#invoiceID').val(),'invoice');
        }
        
        /*
         * Tag event handler
         */
        $('.tag').click(function() {
            var tag = $(this).html();
            if (selectedSection == 'invoices') {
                getFinanceDataObj.getInvoiceList(0,'user',tag,0);
            } else if (selectedSection == 'ledger') {
                var dateStart = genericFunctions.jsToMySQLDate($('#ledgerDateStart').val());
                var dateEnd   = genericFunctions.jsToMySQLDate($('#ledgerDateEnd').val());
                getFinanceDataObj.getLedger(dateStart, dateEnd, 0, 'user', tag);
            }
            return false; 
        });

        initFinanceObj.attachInvoiceEventHandlers();
	},
    attachInvoiceEventHandlers : function() {
        /*
         * Attach event handlers
         */
        $('#invoiceInformationContainer').delegate('.buttonEdit', 'click', function() {
               var invoiceID = $(this).attr('itemID');
               window.location = siteURL+'finances/InvoiceUpdate/index/'+invoiceID;
               return false;
        });

        $('#invoiceInformationContainer').delegate('.buttonDelete', 'click', function() {
			   var invoiceID = $(this).attr('itemID');
               $('#invRow'+invoiceID).fadeOut('slow',function() {
                   $(this).remove();
                   financeSupportObj.calculateInvoiceTotals();
               });
               updateTrashObj.sendToTrash('invoice',invoiceID);
               return false;
        });

        $('#invoiceInformationContainer').delegate('.buttonPDF', 'click', function() {
               var invoiceID = $(this).attr('itemID');
               printObj.goPrint(invoiceID,'invoice');
               return false;
        });
          
        $('#invoiceInformationContainer').delegate('.linkInvoice', 'click', function() {
            var invoiceID = $(this).parents('tr').attr('itemID');
            var invoiceNumber = $(this).parents('tr').find('.invoiceNumber').html();
            if (
                    $('#pageLeftColumn').attr('pageID') != 'clientDashboard' &&
                    $('#pageLeftColumn').attr('pageID') != 'financeView') {
                window.location = siteURL+'finances/FinanceView/viewInvoice/'+invoiceID;
            } else {
                getFinanceDataObj.getInvoiceDetails(invoiceID,'html');
                return false;
            }
        });
    },
    attachLedgerEventHandlers : function() {
        $('#ledgerInformationContainer').delegate('.linkLedger', 'click', function() {
            var obj = $(this);
            var parentRow = obj.parent().parent();
            
            var itemType = parentRow.data('itemtype');
            var itemID = parentRow.data('metaitemid');
            
            if (itemType == 'Income') {
                getFinanceDataObj.getInvoiceDetails(itemID,'html');
                return false;
            } else if (itemType == 'Expense') {
                getExpenseDataObj.getExpenseDetails(itemID, 'html')
            }
            return false;
        });
        
        $('#ledgerInformationContainer').delegate('.buttonEdit', 'click', function() {
            var obj = $(this);
            var parentRow = obj.parent().parent().parent();
            var itemType = parentRow.data('itemtype');
            var metaItemID = parentRow.data('metaitemid');
            var itemID = parentRow.attr('itemid');
            
            if (itemType == 'Expense') {
                window.location = siteURL+'finances/ExpenseUpdate/index/'+metaItemID;
            } else {
                /*
                 * Open edit textbox for income amount.
                 */
                var incomeContainer = parentRow.find('.amountContainer');
                var incomeAmount = parentRow.data('income');
                incomeContainer.html('');
                var input = $('<input/>').attr({type: 'text', id: 'incomeEdit', name: 'incomeEdit', value: incomeAmount})
                    .css('width','100px')
                    .css('text-align','right')
                    .addClass('bigText')
                    .appendTo(incomeContainer)
                    .focus()
                    .blur(function() {
                        var newVal = $(this);
                        if (newVal.val() != incomeAmount) {
                            parentRow.data('income',newVal.val());
                            financeSupportObj.calculateLedgerTotals();
                            var payloadSend = {
                                payInvoiceAmount: newVal.val(),
                                paymentID: itemID
                            }
                            $.ajax({
                                type: "POST",
                                url:  siteURL+'finances/InvoiceUpdate/updateInvoicePayment',
                                data: payloadSend,
                                success: function(payload){},
                                error: function (xhr, desc, exceptionobj) {
                                    errorObj.ajaxError(xhr,desc,exceptionobj);
                                }
                            });
                            
                        } 
                        newVal.remove();
                        incomeContainer.html('<span class="bigText">'+commonTerms['currencySymbol']+newVal.val()+'</span>')
                    });
                
            }
            return false;
        });
        
        $('#ledgerInformationContainer').delegate('.buttonDelete', 'click', function() {
            var obj = $(this);
            var parentRow = obj.parent().parent().parent();
            var itemType = parentRow.data('itemtype');
            var metaItemID = parentRow.data('metaitemid');
            var itemID = parentRow.attr('itemid');
            
            if (itemType == 'Expense') {
                updateTrashObj.sendToTrash('expense',itemID);
            } else {
                $.get(siteURL+'finances/InvoiceUpdate/deleteInvoicePayment/'+itemID, function(payload) {
                });
            }
            parentRow.fadeOut('fast', function() {
                $(this).remove();
                financeSupportObj.calculateLedgerTotals();
            });            
            return false;
        });
        
        $('#ledgerInformationContainer').delegate('.expenseMemo', 'focus', function() {
            if ($(this).val() == 'Memo or comments.') {
                $(this).val('');
            }
        });
        
        $('#ledgerInformationContainer').delegate('.expenseMemo', 'blur', function() {
            var comment = $(this).val();
            var itemID = $(this).data('itemid');
            
            $.ajax({
                type: "POST",
                data: { expenseNotes: comment },
                url:  siteURL+'finances/ExpenseUpdate/saveExpenseNotes/'+itemID,
                success: function(payload) {},
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        });
    }
}

var emailInvoiceObj = {
    initInvoiceEmailButton : function() {
        $('.buttonEmailMain').qtip({
            content: $('#emailInvoiceFormContainer').html(),
            position: {
                corner: {
                    target:  'bottomMiddle',
                    tooltip: 'topMiddle'
                }
            },
            show: {
               when: 'click',
               delay: 0,
               solo: true
            },
            hide: {
               when: {
                   event: 'unfocus'
               },
               effect: {
                   type: 'fade'
               }
            },
            style: {
               tip: {
                   corner: 'topMiddle'
               },
               border: {
                    width: 5,
                    radius: 6
                },
               name: 'cream',
               width: 400,
               padding: 6
            },
            api: {
                onRender: function() {
                    emailInvoiceObj.initPopupFormControls(this);
                },
                onShow: function() {
                    if ($('#estimate').val() == 1) {
                        $('.emailPayPalLinkContainer').hide();
                        $('.emailInvoicePayPalLink').attr('checked','');
                    } else {
                        $('.emailPayPalLinkContainer').show();
                    }    
                },
                onHide: function() {
                    emailInvoiceObj.clearEmailForm(true);
                }
            }
        }).click(function() {
            return false;
        });
    },
    emailInvoice : function(invoiceID,popupObj) {
        var invoiceEmailAddress = popupObj.parents('div').find('.emailInvoiceTo').val();
        var invoiceMessage      = popupObj.parents('div').find('.emailInvoiceMessage').val();
        var invoiceNumber       = $('#invoiceNumber').val();
		var language            = $('#clientLanguage').val();
        var estimate            = $('#estimate').val();
        
        var invoicePayPalLink = 0;
        var invoicePayPalControl = popupObj.parents('div').find('.emailInvoicePayPalLink');
        if (invoicePayPalControl.is(':checked')) {
            invoicePayPalLink = 1;
        }
        
        var sendCopyToUser = 0;
        var sendCopyToUserControl = popupObj.parents('div').find('.emailInvoiceCopyToUser');
        if (sendCopyToUserControl.is(':checked')) {
            sendCopyToUser = 1;
        }
        
        var formHash = {
          invoiceID        : invoiceID,
          invoiceNumber    : invoiceNumber,
          emailAddress     : invoiceEmailAddress,
          invoiceMessage   : invoiceMessage,
		  language         : language,
          invoicePayPalLink: invoicePayPalLink,
          estimate         : estimate,
          sendCopyToUser   : sendCopyToUser
        };
        $.ajax({
		    type: "POST",
		    url:  siteURL+'finances/FinanceView/sendInvoiceEmail',
            data: formHash,
            success: function(payload) {
                var message = commonTerms['invoiceEmailed'];
                if ($('#estimate').val() == '1') {
                    message = commonTerms['estimateEmailed'];
                }
		    	$('.emailInvoiceStatusMessage').systemMessage({
                    status: 'success',
                    message: message
                });
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    initPopupFormControls : function(popupObj) {
		var clientEmail = $('#clientEmail').val();
		$('.emailInvoiceTo').val(clientEmail);
        $('.emailInvoiceSend').click(function() {
            var invoiceID = $('#invoiceID').val();
            emailInvoiceObj.emailInvoice(invoiceID,$(this));
            return false;                        
        }); 
    },
    clearEmailForm : function() {
        $('.emailInvoiceTo').val('');
		$('.emailInvoiceMessage').val('');
        $('.emailInvoiceStatusMessage').html('');
    }
}

var payInvoiceObj = {
    clearPayForm : function() {
        $('.payInvoiceAmount').val('');
        $('.payInvoiceStatus').val('');
        $('.payInvoiceDate').val('');
        $('.payInvoicePaymentType').val('');
        $('.payInvoiceAccountNumber').val('');
        $('.payInvoiceComment').val('');
        $('.payexpensestatus').val('');        
    },
    resetPaymentSection : function() {
        payInvoiceObj.clearPayForm();
        $('#paymentContainer').html('');
        if ($('#invoicePaymentsContainer').is(':visible')) {
            $('#invoicePaymentsContainer').slideUp('fast');
        }
    },
    savePayment : function(invoiceID) {
        var payInvoicePaymentAmount = $('#payInvoiceAmount').val();
        var payInvoicePaymentType   = $('#payInvoicePaymentType').val();
        var payInvoiceAccountNumber = $('#payInvoiceAccountNumber').val();
        var payInvoiceStatus        = $('#payInvoiceStatus').val();
		var payInvoiceDate          = $('#payInvoiceDate').val();
        var payInvoiceComment       = $('#payInvoiceComment').val();

        var formHash = {
          invoiceID               : invoiceID,
          payInvoicePaymentType   : payInvoicePaymentType,
          payInvoiceAccountNumber : payInvoiceAccountNumber,
          payInvoiceStatus        : payInvoiceStatus,
		  payInvoiceDate          : payInvoiceDate,
          payInvoiceAmount        : payInvoicePaymentAmount,
          payInvoiceComment       : payInvoiceComment
        };
        
        if (payInvoicePaymentAmount > 0) {
            $.ajax({
                type: "POST",
                url:  siteURL+'finances/InvoiceUpdate/makeInvoicePayment',
                data: formHash,
                dataType: 'json',
                success: function(payload){
                    renderFinanceDataObj.renderInvoicePayments(payload);
                    payInvoiceObj.recalculateAmountDue(invoiceID, payInvoicePaymentAmount, 'addPayment');
                    payInvoiceObj.clearPayForm();
                    
                    /*
                     * Update status indicators
                     */
                    var statusHuman   = payload.NewStatusHuman;
                    var statusMachine = payload.NewStatusMachine;
                    var statusBGClass = financeSupportObj.getStatusBGColor(statusMachine);
                    
                    $('#invoiceStatus').html(statusHuman);
                    $('#invoiceStatus').removeClass().addClass('bold colorBackground '+statusBGClass);
                    
                    /*
                     * Update the status stuff in the invoice grid view.
                     */
                    var gridStatusObj = $('#invRow'+invoiceID).find('.status');
                    gridStatusObj.removeClass().addClass(statusMachine+' status popUpMenuClick');
                    gridStatusObj.data('statustext',statusHuman);
                    statusObj.statusPopup();
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }    
    },
    recalculateAmountDue : function(invoiceID,paymentAmt,action) {
        var currentAmtDue = $('#invRow'+invoiceID).find('.amountDue').attr('currencysort');
        currentAmtDue = parseFloat(currentAmtDue);
        paymentAmt = parseFloat(paymentAmt);
        var newAmtDue;
        if (action == 'addPayment') {
            newAmtDue = parseFloat(currentAmtDue-paymentAmt);
        } else if (action == 'deletePayment') {
            newAmtDue = parseFloat(currentAmtDue+paymentAmt);
        }
        
        var invRow = $('#invRow'+invoiceID);
        invRow.data('totaldue',newAmtDue);
        
        invRow.find('.amountDue').html(newAmtDue).formatCurrency({symbol: commonTerms['currencySymbol'],useHtml: true});;
        invRow.find('.amountDue').attr('currencysort',newAmtDue);
        
        financeSupportObj.calculateInvoiceTotals();
    },
    updateInvoiceStatus : function(invoiceID,newStatus) {
        var formHash = {
          invoiceID : invoiceID,
          newStatus : newStatus
        };
        $.ajax({
		    type: "POST",
		    url:  siteURL+'finances/InvoiceUpdate/updateInvoiceStatus',
            data: formHash,
            dataType: 'json',
            success: function(payload){
		    	var statusHuman   = payload.NewStatusHuman;
                var statusMachine = payload.NewStatusMachine;
                var statusBGClass = financeSupportObj.getStatusBGColor(statusMachine);

                //$('#invoiceStatusText_SideBar').html(statusHuman);
                //$('#invoiceStatusIcon_SideBar').addClass(statusMachine);
                //$('#invoiceStatusContainer_SideBar').removeClass().addClass('hide colorBackground '+statusBGClass);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var financeSupportObj = {
	setInvoiceIDs : function(invoiceID,invoiceNumber) {
		/*
		 * Set main page button itemID attributes
		 */
		$('.buttonPrint').attr('itemID',invoiceID);
		$('.buttonEditInvoice').attr('itemID',invoiceID);
		$('.buttonDeleteInvoice').attr('itemID',invoiceID);
		$('.buttonEmailMain').attr('itemID',invoiceID);
		$('#invoiceID').val(invoiceID);
		$('#invoiceNumber').val(invoiceNumber);
	},
    showInvoiceList : function() {
        $('#invoiceInformationContainer').fadeIn('fast');
        $('#itemDetailContainer').fadeOut('fast');
        $('#invoiceID').val('');
        $('#sidebarInvoices').show();
        $('#sidebarLedger').hide();
        
        genericFunctions.togglePageHeaderButtons('#buttonEdit','off');
        genericFunctions.togglePageHeaderButtons('#buttonDelete','off');
        genericFunctions.togglePageHeaderButtons('#buttonPrint','off');
        genericFunctions.togglePageHeaderButtons('#buttonAddInvoice','on');
        genericFunctions.togglePageHeaderButtons('#buttonAddEstimate','on');
        genericFunctions.togglePageHeaderButtons('#buttonAddExpense','on');
    },
    showLedger : function() {
        $('#ledgerInformationContainer').fadeIn('fast');
        $('#itemDetailContainer').fadeOut('fast');
        $('#invoiceID').val('');
        $('#sidebarLedger').show();
        $('#sidebarInvoices').hide();
        
        genericFunctions.togglePageHeaderButtons('#buttonEdit','off');
        genericFunctions.togglePageHeaderButtons('#buttonDelete','off');
        genericFunctions.togglePageHeaderButtons('#buttonPrint','off');
        genericFunctions.togglePageHeaderButtons('#buttonAddInvoice','on');
        genericFunctions.togglePageHeaderButtons('#buttonAddEstimate','on');
        genericFunctions.togglePageHeaderButtons('#buttonAddExpense','on');
    },
    getStatusBGColor : function(statusMachine) {
        var statusBGClass;
        switch(statusMachine) {
            case 'status_closed_paid_on':
                statusBGClass = 'light-green';
                break;
            case 'status_open_overdue':
                statusBGClass = 'light-red';
                break;
            case 'status_open_sent_on':
                statusBGClass = 'light-blue';
                break;
            case 'status_open_created_on':
                statusBGClass = 'light-blue';
                break;
            case 'status_closed_not_paid':
                statusBGClass = 'light-red';
                break;
        }
        return statusBGClass;
    },
    createRecurringPopups : function() {
        $('.iconRecurring').each(function() {
            var tipContent = $(this).attr('data');
            var itemID = $(this).parent().parent().attr('itemID');
            tipContent += '<p><a href="#" class="recurringStop">'+commonTerms['stopRecurring']+'</a></p>'

			var target = 'rightMiddle';
			var tooltip = 'leftMiddle';
			var corner = tooltip;
            var qtipItem = $(this);

		    qtipItem.qtip({
				content: tipContent,
				position: {
					corner: {
						   target:  target,
						   tooltip: tooltip
					   }
				},
				show: {
				   when: 'click',
				   delay: 0,
				   solo: true
				},
				hide: {
				   when: {
					   target: $(this),
					   event: 'unfocus'
				   },
				   effect: {
					   type: 'fade'
				   }
				},
				style: {
				   tip: {
					   corner: corner
				   },
                   border: {
                        width: 5,
                        radius: 6
                    },
				   name: 'cream',
				   width: 280,
				   padding: 3
				},
                api: {
					onRender: function() {
						$('.recurringStop').click(function() {
                            qtipItem.qtip('hide');
                            qtipItem.hide();
                            $.get(siteURL+'finances/FinanceView/stopRecurringInvoice/'+itemID, function() {
                            });
                            return false;
                        });
					}
				}
		   });
		});
    },
    clearItemContainers : function() {
        $('#itemDetailContents').html('');
        $('#itemMessageConents').html('');
    },
    calculateLedgerTotals : function() {
        var totalRevenue = 0, totalExpenses = 0;
        $('.ledgerRow').each(function() {
            var row = $(this);
            totalRevenue = parseFloat(totalRevenue)+parseFloat(row.data('income'));
            totalExpenses = parseFloat(totalExpenses)+parseFloat(row.data('expense'));
        });
        $('#totalIncome').text(commonTerms['currencySymbol']+totalRevenue.toFixed(2));
        $('#totalExpense').text(commonTerms['currencySymbol']+totalExpenses.toFixed(2));
    },
    calculateInvoiceTotals : function() {
        var totalIncome = 0, totalInvoiced = 0, totalEstimate = 0, totalDue = 0;
        $('.invoiceRow').each(function() {
            var row = $(this);
            if (row.data('type') == 'estimate') {
                totalEstimate = parseFloat(totalEstimate)+parseFloat(row.data('amount'));
            } else if (row.data('type') == 'invoice') {
                totalInvoiced = parseFloat(totalInvoiced)+parseFloat(row.data('amount'));
                totalDue = parseFloat(totalDue)+parseFloat(row.data('totaldue'));
                totalIncome = parseFloat(totalInvoiced-totalDue);
            }            
        });
        $('#totalInvoiced').text(commonTerms['currencySymbol']+totalIncome.toFixed(2));
        $('#totalEstimate').text(commonTerms['currencySymbol']+totalEstimate.toFixed(2));
        $('#totalDue').text(commonTerms['currencySymbol']+totalDue.toFixed(2));
    }
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'updateInvoice') {
		initInvoiceFormObj.initInvoiceForm();
	} else if ($('#pageLeftColumn').attr('pageID') == 'viewInvoiceDetails') {
		initFinanceObj.initInvoiceDetails();
	} else if ($('#pageLeftColumn').attr('pageID') == 'financeView') {
		initFinanceObj.initFinanceView();
	}
    
    $('#rdoLedger').buttonset();
    $('#rdoInvoices').buttonset();
    
    $('#itemDetailContainerClose').click(function() {
        $('#tabsFinances').fadeIn('fast');
        $('#itemDetailContainer').fadeOut('fast');
    })
});