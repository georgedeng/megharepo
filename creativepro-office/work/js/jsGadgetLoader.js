$(document).ready(function() {
    widgetLoaderObj.loadWidgetContent('tasks','user');
    $('#widgetContent_tasks').show();

    $('body').delegate('a', 'click', function() {
        $(this).attr('target', '_blank');
    });

    $('#selectWidget').change(function() {
        $('.widgetContainer').hide();
        $('#widgetContent_'+$('#selectWidget').val()).show();
        widgetLoaderObj.loadWidgetContent($('#selectWidget').val(),'user');
    });
});