<?
if ($blogs == 1) {
	$logoImage = 'logoCPOBlog.gif';
} elseif ($forum == 1) {
	$logoImage = 'logoCPOForum.gif';
} else {
	$logoImage = 'logoCPO.gif';
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	
	<title>Project Management Dashboard Software to Organize Your Work</title>
	<meta name="Description" content="At Mycpohq, you can easily manage your team, clients, projects, invoices, time sheet from one web-based app with the help of project and task management dashboard software." />
	<meta name="Keywords" content="project management dashboard software,task management software,web based office applications,online project management tools ,online timesheet,online invoice, project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist" />
	<meta name="Author" content="Jeff Denton @ GC LLC Productions :: Fairfax VA :: admin@mycpohq.com" />
	<meta name="Rating" CONTENT="General" />
	<meta name="ROBOTS" CONTENT="INDEX, FOLLOW" />
	<meta name="Revisit-After" CONTENT="7 days" />
	<meta name="Distribution" CONTENT="global" />
	<link rel="STYLESHEET" href="<?=$siteURL; ?>/css/<?=$styleSheet; ?>" />
	<link rel="STYLESHEET" href="<?=$siteURL; ?>/css/blogProduction.css" />
	
	<link rel="icon" href="<?=$siteURL; ?>/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?=$siteURL; ?>/images/favicon.ico" type="image/x-icon" />	
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://blog.creativeprooffice.com/rss.php" />	
	<script language="JavaScript" type="text/javascript" src="<?=$siteURL; ?>/apps/js/protaculous/prototype1.6.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=$siteURL; ?>/apps/js/protaculous/scriptaculous.js?load=effects,dragdrop,combo"></script>
	
	
	<SCRIPT src="<?=$siteURL; ?>/js/common.js" type="text/javascript"></SCRIPT>
	<? if (!empty($jsFile)) { ?>
	<SCRIPT src="<?=$siteURL; ?>/js/<?=$jsFile; ?>" type="text/javascript"></SCRIPT>
	<? } ?>
	<? if ($sajax == 1) { ?> 
		<script language="JavaScript" type="text/javascript">
		<? sajax_show_javascript(); ?>
		</script>
	<? } ?>	
	<? if ($blogs == 1) { ?>
	<base href="http://blog.creativeprooffice.com" />
	<? } elseif ($forum == 1) { ?>
	<SCRIPT src="<?=$siteURL; ?>/js/jsForum.js" type="text/javascript"></SCRIPT>
	<base href="http://forum.creativeprooffice.com" />
	<? } else { ?>
	<base href="http://www.creativeprooffice.com" />
	<? } ?>
	<SCRIPT src="<?=$siteURL; ?>/js/swfobject.js" type="text/javascript"></SCRIPT>
</head>

<body>
<h1 style="display: none;">Free, Web Based Project Management with CreativePro Office</h1>		
<div id="header"></div>

<div id="page">

	<div id="logoHeader">
		<div style="float: left;">
		<a href="<?=$siteURL; ?>/index.php" title="CreativePro Office"><img class="logo" src="<?=$siteURL; ?>/images/<?=$logoImage; ?>" style="border: none;" alt="CreativePro Office" /></a>
		</div>
		<div id="statsHeader"><p><? echo getSiteStats(); ?></p></div>
		<div style="clear: both;"></div>
		<div id="menu">
		<ul>
			<li class="tabLeft"></li>
			<li class="tab"><a href="<?=$siteURL; ?>">Home</a></li>
			<li class="tabLeft2"></li>
			<? if ($live == 1) { ?>
			<li class="tab"><a href="<?=$siteURL; ?>/site/register">Free Account</a></li>
			<? } ?>
			<!-- <li class="tabLeft2"></li>
			<li class="tab"><a href="<?=$siteURL; ?>/site/faq">FAQ</a></li> -->
			<li class="tabLeft2"></li>
			<li class="tab"><a href="http://forum.creativeprooffice.com?logged=<?=$_SESSION[logged]; ?>&u=<?=$_SESSION[userid]; ?>">Forum</a></li>
			<li class="tabLeft2"></li>
			<li class="tab"><a href="http://blog.creativeprooffice.com?logged=<?=$_SESSION[logged]; ?>&u=<?=$_SESSION[userid]; ?>">Blog</a></li>
			<li class="tabLeft2"></li>
			<li class="tab"><a href="<?=$siteURL; ?>/site/contact">Contact</a></li>
		</ul>
		</div>
		
	</div> <!-- End logoHeader -->
		
	<div id="pageContent">	
		<div id="pageContent2">
