<?php
function spinUpAccountSession($accountUserid) {
    $CI =& get_instance();
    $CI->load->model('common/public_site','',true);
    $CI->load->model('accounts/accounts','',true);

    $accountArray = $CI->accounts->getAccountInformation($accountUserid,1);

    $CI->session->set_userdata('accountLevel',$accountArray['AccountLevel']);
    $CI->session->set_userdata('accountUserid',$accountUserid);
    $CI->session->set_userdata('paymentProfileID',$accountArray['PaymentProfileID']);
    $CI->session->set_userdata('dateSignup',$accountArray['DateSignup']);
    $CI->session->set_userdata('email',$accountArray['Email']);

    $isPaid = FALSE;
    if ($accountArray['IsPaid'] == 1) {
        $isPaid = TRUE;
    }
    $CI->session->set_userdata('isPaid',$isPaid);
    $CI->session->set_userdata('logged',TRUE);

    return $accountArray;
}
?>
