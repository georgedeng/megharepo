<?php

$lang['date_year'] = "Year";
$lang['date_years'] = "Years";
$lang['date_month'] = "Month";
$lang['date_months'] = "Months";
$lang['date_week'] = "Week";
$lang['date_weeks'] = "Weeks";
$lang['date_day'] = "Day";
$lang['date_days'] = "Days";
$lang['date_hour'] = "Hour";
$lang['date_hours'] = "Hours";
$lang['date_minute'] = "Minute";
$lang['date_minutes'] = "Minutes";
$lang['date_second'] = "Second";
$lang['date_seconds'] = "Seconds";

$lang['date_hrs']       = "hrs"; // **
$lang['date_today']     = 'Today'; // **
$lang['date_tomorrow']  = 'Tomorrow'; // **
$lang['date_this_week'] = 'This week'; // **
$lang['date_next_week'] = 'Next week'; // **

$lang['UM12']	= 'Baker/Howland Island (UTC -12:00)';
$lang['UM11']	= 'Samoa Time Zone, Niue (UTC -11:00)';
$lang['UM10']	= 'Hawaii-Aleutian Standard Time, Cook Islands, Tahiti (UTC -10:00)';
$lang['UM95']	= 'Marquesas Islands (UTC -9:30)';
$lang['UM9']	= 'Alaska Standard Time, Gambier Islands (UTC -9:00)';
$lang['UM8']	= 'Pacific Standard Time, Clipperton Island (UTC -8:00)';
$lang['UM7']	= 'Mountain Standard Time (UTC -7:00)';
$lang['UM7AZ']	= 'Mountain Standard Time/Arizona (UTC -7:00)';
$lang['UM6']	= 'Central Standard Time (UTC -6:00)';
$lang['UM5']	= 'Eastern Standard Time, Western Caribbean Standard Time (UTC -5:00)';
$lang['UM45']	= 'Venezuelan Standard Time (UTC -4:30)';
$lang['UM4']	= 'Atlantic Standard Time, Eastern Caribbean Standard Time (UTC -4:00)';
$lang['UM35']	= 'Newfoundland Standard Time (UTC -3:30)';
$lang['UM3']	= 'Argentina, Brazil, French Guiana, Uruguay (UTC -3:00)';
$lang['UM2']	= 'South Georgia/South Sandwich Islands (UTC -2:00)';
$lang['UM1']	= 'Azores, Cape Verde Islands (UTC -1:00)';
$lang['UTC']	= 'Greenwich Mean Time, Western European Time (UTC)';
$lang['UP1']	= 'Central European Time, West Africa Time (UTC +1:00)';
$lang['UP2']	= 'Central Africa Time, Eastern European Time, Kaliningrad Time (UTC +2:00)';
$lang['UP3']	= 'Moscow Time, East Africa Time (UTC +3:00)';
$lang['UP35']	= 'Iran Standard Time (UTC +3:30)';
$lang['UP4']	= 'Azerbaijan Standard Time, Samara Time (UTC +4:00)';
$lang['UP45']	= 'Afghanistan (UTC +4:30)';
$lang['UP5']	= 'Pakistan Standard Time, Yekaterinburg Time (UTC +5:00)';
$lang['UP55']	= 'Indian Standard Time, Sri Lanka Time (UTC +5:30)';
$lang['UP575']	= 'Nepal Time (UTC +5:45)';
$lang['UP6']	= 'Bangladesh Standard Time, Bhutan Time, Omsk Time (UTC +6:00)';
$lang['UP65']	= 'Cocos Islands, Myanmar (UTC +6:30)';
$lang['UP7']	= 'Krasnoyarsk Time, Cambodia, Laos, Thailand, Vietnam (UTC +7:00)';
$lang['UP8']	= 'Australian Western Standard Time, Beijing Time, Irkutsk Time (UTC +8:00)';
$lang['UP875']	= 'Australian Central Western Standard Time (UTC +8:45)';
$lang['UP9']	= 'Japan Standard Time, Korea Standard Time, Yakutsk Time (UTC +9:00)';
$lang['UP95']	= 'Australian Central Standard Time (UTC +9:30)';
$lang['UP10']	= 'Australian Eastern Standard Time, Vladivostok Time (UTC +10:00)';
$lang['UP105']	= 'Lord Howe Island (UTC +10:30)';
$lang['UP11']	= 'Magadan Time, Solomon Islands, Vanuatu (UTC +11:00)';
$lang['UP115']	= 'Norfolk Island (UTC +11:30)';
$lang['UP12']	= 'Fiji, Gilbert Islands, Kamchatka Time, New Zealand Standard Time (UTC +12:00)';
$lang['UP1275']	= 'Chatham Islands Standard Time (UTC +12:45)';
$lang['UP13']	= 'Phoenix Islands Time, Tonga (UTC +13:00)';
$lang['UP14']	= 'Line Islands (UTC +14:00)';

/* End of file date_lang.php */
/* Location: ./system/language/english/date_lang.php */