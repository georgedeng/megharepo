<?php
$lang['finance_invoice_number_abbr']     = 'Invoice #';
$lang['finance_estimate_number_abbr']    = 'Estimate #';

$lang['finance_invoice_message']         = 'You can view and print this invoice from your <a href="%s"><strong>client page</strong></a>.';
$lang['finance_invoice_message_pdf']     = 'You can view and print this invoice from your client area by going to <strong>'.BASE_URL.'</strong> and logging in.';
$lang['finance_estimate_message_pdf']    = 'You can view and print this estimate from your client area by going to <strong>'.BASE_URL.'</strong> and logging in.';
$lang['finance_invoice_message_paypal']  = 'Make a payment for this invoice using PayPal.';

$lang['finance_estimate_message']        = 'You can view and print this estimate from your <a href="%s"><strong>client page</strong></a>.';
$lang['finance_estimate_message_pdf']    = 'You can view and print this estimate from your client area by going to <strong>'.BASE_URL.'</strong> and logging in.';
?>