var appActivity = {
    getAppActivity : function(projectID,container,poll) {
        if (projectID == null) {
            projectID = 0;
        }
        if (poll == null) {
            poll = 0;
        }
        $.ajax({
                type:     "POST",
                url:      siteURL + 'activity/AppActivity/getAppActivity/' + projectID + '/' + poll + '/json/0',
                dataType: 'json',
                success: function(payload){
                var jsonObject = payload;
                
                var collection = '';
                for(a=0;a<=((jsonObject.length)-1);a++) {
                    collection += appActivity.renderActivityRow(jsonObject[a]);
                }
                
                $(collection).hide().prependTo($(container)).fadeIn('slow');
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    renderActivityRow : function(obj) {
        var activityText = '';
        var word = 'a';
		if (obj.ItemType == 'invoice' || obj.ItemType == 'expense' || obj.ItemType == 'event') {
			word = 'an';
		}
        
        switch (obj.Activity) {
			case 'create':
				activityText += ' created a new';
				break;
			case 'delete':
				activityText += ' deleted ' + word;
				break;
			case 'update':
				activityText += ' edited ' + word;
				break;
			case 'invoice payment':
				activityText += ' entered a payment for ' + word;
				break;	
			case 'update status':
				activityText += ' updated the status for ' + word;
				break;			
		}
		activityText += ' '+obj.ItemType+' ';
        
        var lblItemType = '', lnkItem = '';
        switch(obj.ItemType) {
			case 'project':
				lblItemType = '<span class="colorBackground label light-purple mTop3 mRight6">Project</span>';
                lnkItem = siteURL + 'projects/ProjectDetail/index/'+obj.ItemID;
				break;
			case 'invoice':
				lblItemType = '<span class="colorBackground label light-green mTop3 mRight6">Invoice</span>';
                lnkItem = siteURL + 'finances/FinanceView/viewInvoice/' + obj.ItemID + '/0';
				break;
			case 'expense':
				lblItemType = '<span class="colorBackground label light-red mTop3 mRight6">Expense</span>';
				break;
			case 'event':
				lblItemType = '<span class="colorBackground label light-blue mTop3 mRight6">Event</span>';
				break;
			case 'comment':
				lblItemType = '<span class="colorBackground label light-brown mTop3 mRight6">Comment</span>';
				break;			
			case 'task':
				lblItemType = '<span class="colorBackground label orange mTop3 mRight6">Task</span>';
				break;	
			case 'client':
				lblItemType = '<span class="colorBackground label light-blue mTop3 mRight6">Client</span>';
				break;		
		}	
        
        var rowHTML =  '<div><img class="avatar33 left" src="' + obj.AvatarURL + '" />';
            rowHTML += '<p class="left">' + lblItemType + '<a href="' + lnkItem + '"><strong>' + obj.ItemTitle + '</strong></a></p>';
            rowHTML += '<p class="right subText"><strong><u>' + obj.NameFirst + ' ' + obj.NameLast + '</u></strong> ' + activityText + '<span class="subText" data-livestamp="'+obj.TimestampActivity+'"></span>.</p>';
            rowHTML += '<p style="clear: both;"></p>';
            rowHTML += '</div>';
        return rowHTML;
    }
}


