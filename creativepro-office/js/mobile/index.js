Ext.ns('cpo');

/*
 * Some global variables
 */
var authKey, projectSelect, projectSelectStore, taskSelect, taskSelectStore;

Ext.setup({
    icon: siteURL+'images/mobile/appIcon.png',
    tabletStartupScreen: siteURL+'images/mobile/tablet_startup.png',
    phoneStartupScreen: siteURL+'images/mobile/phone_startup.png',
    glossOnIcon: true,
    onReady: function() {
        var loginForm;

        var loginObj = {
            tryLogin : function() {
                loginForm.setLoading(true, false);
                Ext.Ajax.request({
                    url: siteURL+'login/tryLogin/1',
                    method: 'post',
                    params: {
                        userid: loginForm.getValues().userid,
                        password: loginForm.getValues().password,
                        remember: Ext.getCmp('remember').isChecked()},
                    failure : function(response){
                         data = Ext.decode(response.responseText);
                         Ext.Msg.alert('Login Error', data.ErrorMessage, Ext.emptyFn);
                    },
                    success: function(response, opts) {
                        data = Ext.decode(response.responseText);
                        console.log(response.responseText);
                        if (data.ErrorMessage != null)
                        {
                            Ext.Msg.alert('Login Error', data.ErrorMessage, Ext.emptyFn);
                            loginForm.setLoading(false);
                        } else {
                            authKey = data.AuthKey;
                            loginForm.setLoading(false);
                            loginForm.hide();
                            mainApp.initApp();
                        }
                    }
                });
            },
            loginFormRender : function() {
                loginForm = new Ext.form.FormPanel({
                    title: 'Please login',
                    xtype: 'form',
                    id: 'loginForm',
                    scroll: 'vertical',
                    fullscreen: true,
                    ui: 'light',
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            title: '<span class="headerIcon"></span>',
                            dock: 'top'
                        }
                    ],
                    items: [
                        {
                        xtype: 'fieldset',
                            title: 'Please login',
                            defaults: {
                                labelWidth: '40%'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'userid',
                                    id: 'userid',
                                    label: 'Userid/Email',
                                    placeHolder: 'you@email.com',
                                    autoCapitalize : false,
                                    required: true,
                                    useClearIcon: true
                                },{
                                    xtype: 'passwordfield',
                                    name: 'password',
                                    id: 'password',
                                    label: 'Password',
                                    autoCapitalize : false,
                                    required: true,
                                    useClearIcon: true
                                },{
                                    xtype: 'checkboxfield',
                                    name: 'remember',
                                    id: 'remember',
                                    label: 'Remember'
                                },{
                                    /*
                                     * Login button
                                     */
                                    layout: 'vbox',
                                    defaults: {xtype: 'button', scope: this, style: 'margin: 5px;'},
                                    items: [{
                                        text: 'Login',                                
                                        ui: 'confirm',
                                        handler: function() {
                                            loginObj.tryLogin();
                                        }
                                    }]
                                }
                            ]
                        }]
                });
            }
        }

        var mainApp = {
            initApp : function() {
                modelsObj.initModels();
                mainApp.getCommonAppStores();

                var timeTrackerPanel = timeTrackerObj.init();
                var tasksPanel = taskObj.init();
                var contactsPanel = contactObj.init();
                
                var tabpanel = new Ext.TabPanel({
                    id: 'mainTabPanel',
                    tabBar: {
                        dock: 'bottom',
                        layout: {
                            pack: 'center'
                        },
                        scroll: {
                            direction: 'horizontal',
                            useIndicators: false
                        }
                    },
                    fullscreen: true,
                    ui: 'light',
                    cardSwitchAnimation: {
                        type: 'slide',
                        cover: true
                    },
                    defaults: {
                        scroll: 'vertical'
                    },
                    items: [
                        timeTrackerPanel,
                        tasksPanel,
                        contactsPanel,
                        cpo.calendar,
                        cpo.messages,
                        cpo.finances,
                        cpo.files
                    ]
                });                
            },
            getCommonAppStores : function() {  
                projectSelectStore = new Ext.data.Store({
                    model: 'ModelProjectSelect',
                    storeId: 'projectSelectStore',
                    proxy: {
                        type: 'ajax',
                        url: siteURL+'projects/ProjectView/getProjectsForSelect',
                        reader: {
                            type: 'json',
                            root: ''
                        }
                    }
                });

                projectSelectStore.proxy.actionMethods.read = 'POST';
                projectSelectStore.proxy.extraParams = {authKey: authKey}
                projectSelectStore.load();
            }
        }

        /*
         * Render login form
         */
        if ($('#logged').val() == 1) {
            authKey = $('#authKey').val();
            console.log('authKey: '+authKey);
            mainApp.initApp();
        } else {
            loginObj.loginFormRender();
        }
    }    
});