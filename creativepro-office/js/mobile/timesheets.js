var buttonTimerStart, buttonTimerStop;

var timeTrackerObj = {
    init : function() {
        var projectSelect = new Ext.form.Select({
            placeHolder: 'Project',
            id: 'timerSelectProject',
            name: 'timerSelectProject',
            displayField: 'Title',
            valueField: 'ProjectID'
        });

        var taskSelectStore = new Ext.data.Store({
            model: 'ModelTaskSelect',
            storeId: 'taskSelectStore',
            proxy: {
                type: 'ajax',
                url: siteURL+'tasks/TaskView/getTasksForSelect/0/json/0/1',
                reader: {
                    type: 'json',
                    root: ''
                }
            },
            listeners: {
                load: function() {
                    if (taskSelectStore.data.length == 0) {
                        taskSelect.disable();
                    } else {
                        taskSelect.store = taskSelectStore;
                        taskSelect.activeItem = 0;
                        taskSelect.enable();
                    }
                }
            }
        });

        var taskSelect = new Ext.form.Select({
            placeHolder: 'Task',
            id: 'timerSelectTask',
            name: 'timerSelectTask',
            displayField: 'Title',
            valueField: 'TaskID',            
            disabled: true
        });

        var timerPanel = new Ext.form.FormPanel({
            xtype: 'form',
            contentEl: 'jobTimer',            
            items: [
                {
                    xtype: 'fieldset',
                    items: [
                        projectSelect,
                        taskSelect,
                        {
                            xtype: 'textareafield',
                            placeHolder: 'Comments',
                            name: 'comments',
                            id: 'comments',
                            height: 75,
                            grow: true
                        }                        
                    ]
                },
                {
                    xtype: 'button',
                    ui: 'confirm',
                    text: 'Start timer',
                    id: 'timerStart',
                    listeners: {
                        tap: function() {
                            timerObj.timerControl('start');
                            timerObj.timerControlButtons('start');
                        }
                    }
                },
                {
                    xtype: 'button',
                    ui: 'decline',
                    text: 'Stop timer',
                    id: 'timerStop',
                    listeners: {
                        tap: function() {
                            timerObj.timerControl('stop');
                            timerObj.timerControlButtons('stop');
                        }
                    }
                }
            ],
            listeners: {
                render: function() {
                    projectSelect.store = projectSelectStore;
                    projectSelect.activeItem = 0;

                    buttonTimerStart = Ext.getCmp('timerStart');
                    buttonTimerStop = Ext.getCmp('timerStop');

                    buttonTimerStop.hide();
                }
            }
        });

        projectSelect.on('change',function(select,value) {
           /*
            * Go get tasks for this project
            */
            taskSelectStore.proxy.actionMethods.read = 'POST';
            taskSelectStore.proxy.extraParams = {authKey: authKey, projectID: value}
            taskSelectStore.load();            
        });

        var timeSheetPanel = new Ext.Panel({
            id: 'timeSheetPanel',
            tpl: ['<strong>#{TaskNo} {Title}</strong><br /><span class="subText">{ProjectTitle}</span><br /><span class="subText">Due: {DateEndRender}</span>'],
            dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                items: [{
                    text: 'Back',
                    ui: 'back',
                    handler: function() {
                        Ext.getCmp('timerContainerPanel').layout.setActiveItem(0,'slide');
                    }
                }]
            }]
        });

        cpo.timeTracker = new Ext.Panel({
            title: 'Job Timer',
            iconCls: 'iconTabTimer',
            id: 'timerContainerPanel',
            dockedItems: [{
                dock: 'top',
                title: '<span class="headerIcon"></span>',
                xtype: 'toolbar'
            }],
            activeItem: 0,
            items: [timerPanel]
        });
        
        return cpo.timeTracker;
    }
}

var timerObj = {
    timerControl : function(action) {
        /*
		var currentTime = $('#timerDisplayControl').text();
		var timeArray = currentTime.split(':');
		var sec  = timeArray[2];
		var min  = timeArray[1];
		var hour = timeArray[0];

		if (action == 'stop') {
			//alert(timerElementID+' '+currentTime+' '+sec);
		}

        if (sec.substr(0,1) == '0') {
            sec = sec.substr(1,1);
        }
        if (min.substr(0,1) == '0') {
            min = min.substr(1,1);
        }
        if (hour.substr(0,1) == '0') {
            hour = hour.substr(1,1);
        }
        */
        var d = new Date();
        var startTime = $('#taskTimerStart').val();
        var currentTime = d.getTime();
        var diff = parseInt((currentTime-startTime)/1000);
        var hour,min,sec;

        if (diff<60) {
            hour = 0;
            min = 0;
            sec = diff;
        } else if (diff<3600) {
            min = Math.floor(diff/60);
            sec = diff-(min*60)
        } else {

        }
        
		if (action == 'start') {
            sec++;
			if (sec == 60) {
				sec = 0;
				min = min + 1;
			}

			if (min == 60) {
			   min = 0;
			   hour = hour + 1;
			}

			if (sec<=9) { sec = "0" + sec; }
            if (min<=9) { min = "0" + min; }
            if (hour<=9) { hour = "0" + hour; }
            var jobTime = hour+':'+min+':'+sec;

			/*
			 * Render new time to window
			 */
			$('#timerDisplayControl').html(jobTime);
            eval("timerTimeout = window.setTimeout(\"timerObj.timerControl('start');\", 1000);");
		} else {
            eval("window.clearTimeout(timerTimeout);");
		}
    },
    createTimeFromMilliseconds : function() {

    },
    createElapsedTimeDecimal : function(elapsedTime) {
        var elapsedTimeArray = elapsedTime.split(':');
		var hour = elapsedTimeArray[0];
		var min  = elapsedTimeArray[1];
		var sec  = elapsedTimeArray[2];

		if (sec.substr(0,1) == '0') {
            sec = sec.substr(1,1);
        }
        if (min.substr(0,1) == '0') {
            min = min.substr(1,1);
        }
        if (hour.substr(0,1) == '0') {
            hour = hour.substr(1,1);
        }

        sec  = parseInt(sec);
		min  = parseInt(min);
		hour = parseInt(hour);

		if (sec>29) { min = min+1; }
		var minDecimal = parseFloat(min/60);
		var elapsedTimeDecimal = parseFloat(hour+minDecimal);
		elapsedTimeDecimal = elapsedTimeDecimal.toFixed(2);
		return elapsedTimeDecimal;
    },
    timerControlButtons : function(action) {
        if (action == 'start') {
            buttonTimerStart.hide();
            buttonTimerStop.show();
            Ext.getCmp('mainTabPanel').getTabBar().items.items[0].setBadge('1');
            if ($('#taskTimerStart').val() == '') {
                var d = new Date();
                $('#taskTimerStart').val(d.getTime());
            }
        } else {
            buttonTimerStart.show();
            buttonTimerStop.hide();
            Ext.getCmp('mainTabPanel').getTabBar().items.items[0].setBadge('');
            timerObj.openSaveEntryDialog();
        }
    },
    openSaveEntryDialog : function() {
        var savePopup = new Ext.Panel({
           floating: true,
           modal: true,
           centered: true,
           width: 300,
           dockedItems : [
               {
                   dock: 'top',
                   xtype: 'toolbar',
                   title: 'Save entry?'
               },
               {
                    xtype: 'checkboxfield',
                    name: 'billable',
                    label: 'Billable',
                    id: 'billable',
                    value: '1',
                    checked: true
                },
               {
                   dock: 'bottom',
                   xtype: 'toolbar',
                   ui: 'light',
                   items: [
                       {
                           xtype: 'button',
                           text: 'Save',
                           ui: 'confirm',
                           listeners: {
                               tap: function() {
                                   timerObj.saveTimerEntry();
                                   savePopup.hide('pop');
                               }
                           }
                       },{
                           xtype: 'button',
                           text: 'Cancel',
                           ui: 'decline',
                           listeners: {
                               tap: function() {
                                    savePopup.hide('pop');
                               }
                           }
                       }
                   ]
               }
           ]
        });

        savePopup.show('pop');
    },
    saveTimerEntry : function() {
        /*
         * Save timer entry to CPO database and reset form.
         */
        var projectID = Ext.getCmp('timerSelectProject').getValue();
        var taskID = Ext.getCmp('timerSelectTask').getValue();
        var comments = Ext.getCmp('comments').getValue();
        var billable = Ext.getCmp('billable').getValue();
        var elapsedTime = $('#timerDisplayControl').html();
        var elapsedTimeDecimal = timerObj.createElapsedTimeDecimal(elapsedTime);

        Ext.Ajax.request({
            url: siteURL+'timesheets/TimesheetView/saveTimesheetRecordFromJobTimer',
            method: 'post',
            params: {
                action: 'saveFromDesktop',
                projectID: projectID,
                taskID: taskID,
                comments: comments,
                billable: billable,
                elapsedTime: elapsedTimeDecimal
            },
            failure : function(response) {
                 data = Ext.decode(response.responseText);
                 Ext.Msg.alert('Login Error', data.ErrorMessage, Ext.emptyFn);
            },
            success: function(response, opts) {
                /*
                data = Ext.decode(response.responseText);
                console.log(response.responseText);
                if (data.ErrorMessage != null)
                {
                    Ext.Msg.alert('Login Error', data.ErrorMessage, Ext.emptyFn);
                    loginForm.setLoading(false);
                } else {
                    authKey = data.AuthKey;
                    loginForm.setLoading(false);
                    Ext.getCmp('loginForm').hide();
                    mainApp.initApp();
                }
                */
            }
        });
        
    }
}