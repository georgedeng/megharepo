var taskObj = {
    init : function() {
        var taskStore = new Ext.data.Store({
            model: 'TaskListMaster',
            storeId: 'taskStore',
            proxy: {
                type: 'ajax',
                url: siteURL+'tasks/TaskView/getTaskDigestView',
                reader: {
                    type: 'json',
                    root: ''
                }
            },
            listeners: {
                load : function(store,records,options) {
                    taskOverdueStore.loadData(records[0].data.TasksOverdue);
                    taskThisWeekStore.loadData(records[0].data.TasksThisWeek);
                }
            }

        });

        var taskOverdueStore = new Ext.data.Store({
            model: 'TaskList',
            storeId: 'taskOverdueStore',
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        var taskThisWeekStore = new Ext.data.Store({
            model: 'TaskList',
            storeId: 'taskThisWeekStore',
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        var taskOverdueListControl = new Ext.List({
            pinHeader: false,
            itemTpl: [
                '<strong>#{TaskNo} {Title}</strong><br /><span class="subText">{ProjectTitle}</span><br /><span class="subText">Due: {DateEndRender}</span>',
                '<img src="'+siteURL+'images/icons/iconPriorityGreenSmall.png" />'
            ],
            store: taskOverdueStore,
            listeners: {
                'itemtap' : function(dataView,index,item,e) {
                    Ext.getCmp('taskContainerPanel').layout.setActiveItem(2,'slide');
                    var listItem = dataView.store.getAt(index);
                    Ext.getCmp('taskDetailPanel').update(listItem.data);
                }
            }
        });

        var taskThisWeekListControl = new Ext.List({
            pinHeader: false,
            itemTpl: [
                '<strong>#{TaskNo} {Title}</strong><br /><span class="subText">{ProjectTitle}</span><br /><span class="subText">Due: {DateEndRender}</span>',
                '<img src="'+siteURL+'images/icons/iconPriorityGreenSmall.png" />'
            ],
            store: taskThisWeekStore,
            listeners: {
                'itemtap' : function(dataView,index,item,e) {
                    Ext.getCmp('taskContainerPanel').layout.setActiveItem(2,'slide');
                    var listItem = dataView.store.getAt(index);
                    Ext.getCmp('taskDetailPanel').update(listItem.data);
                }
            }
        });

        var projectSelect = new Ext.form.Select({
            placeHolder: 'Project',
            id: 'taskSelectProject',
            name: 'taskSelectProject',
            displayField: 'Title',
            valueField: 'ProjectID'
        });

        var taskMenuPanel = new Ext.form.FormPanel({
            xtype: 'form',
            contentEl: 'taskMenu',
            dockedItems: [{
                dock: 'top',
                title: '<span class="headerIcon"></span>',
                xtype: 'toolbar'
            }],
            items: [{
                xtype: 'fieldset',
                items: [projectSelect]
            }],
            listeners: {
                render: function() {
                    /*
                     * Attach our event handlers to task menu
                     */
                    var overdueMenuItem = Ext.get('overdueTaskMenuItem');
                    var todayTaskMenuItem = Ext.get('todayTaskMenuItem');
                    var upcomingTaskMenuItem = Ext.get('upcomingTaskMenuItem');

                    projectSelect.store = projectSelectStore;

                    overdueMenuItem.on('click', function(){
                        getTaskStore();
                        Ext.getCmp('taskContainerPanel').layout.setActiveItem(1,'slide');
                    });
                }
            }
        });

        projectSelect.on('change',function(select,value) {
           alert(value);
        });

        var taskDetailPanel = new Ext.Panel({
            layout: 'fit',
            width: 320,
            height: 450,
            id: 'taskDetailPanel',
            tpl: ['<strong>#{TaskNo} {Title}</strong><br /><span class="subText">{ProjectTitle}</span><br /><span class="subText">Due: {DateEndRender}</span>'],
            dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                items: [{
                    text: 'Back',
                    ui: 'back',
                    handler: function() {
                        Ext.getCmp('taskContainerPanel').layout.setActiveItem(1,{ type: 'slide', reverse: true });
                    }
                }]
            }]
        });

        var taskListPanel = new Ext.Panel({
            id: 'taskListPanel',
            dockedItems: [{
                xtype: 'toolbar',
                title: '<span class="headerIcon"></span>',
                dock: 'top',
                items: [{
                    text: 'Back',
                    ui: 'back',
                    handler: function() {
                        Ext.getCmp('taskContainerPanel').layout.setActiveItem(0,{ type: 'slide', reverse: true });
                    }
                }]
            }],
            items: [
                    taskOverdueListControl,
                    taskThisWeekListControl
                ],
            listeners: {
                render: function() {

                }
            }
        });

        function getTaskStore() {
            if (!Ext.isDefined(taskStore.totalLength) && !taskStore.lastOptions) {
                taskStore.proxy.actionMethods.read = 'POST';
                taskStore.proxy.extraParams = {authKey: authKey}
                taskStore.load();

                taskOverdueListControl.bindStore(taskOverdueStore);
                taskThisWeekListControl.bindStore(taskThisWeekStore);
            }
        }

        cpo.tasks = new Ext.Panel({
            title: 'Tasks',
            iconCls: 'iconTabTasks',
            id: 'taskContainerPanel',
            layout: 'card',
            activeItem: 0,
            items: [taskMenuPanel,taskListPanel,taskDetailPanel]

        });

        return cpo.tasks;
    }
}