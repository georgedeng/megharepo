cpo.files = new Ext.Panel({
    title: 'Files',
    iconCls: 'iconTabFiles',
    dockedItems: [
        {
            xtype: 'toolbar',
            title: '<span class="headerIcon"></span>',
            dock: 'top',
            items: [{
                text: 'JSONP'
            },
            {
                xtype: 'spacer'
            },
        {
            text: 'XMLHTTP'
        }]
    }]
});