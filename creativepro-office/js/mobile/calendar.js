cpo.calendar = new Ext.Panel({
    title: 'Calendar',
    iconCls: 'iconTabCalendar',
    id: 'mainCalendar',
    html: '<div id="calendar"></div>',
    dockedItems: [
        {
            xtype: 'toolbar',            
            dock: 'top',
            defaults: {
                iconMask: true
            },
            items: [
                {
                    xtype: 'segmentedbutton',
                    allowDepress: true,                    
                    items: [{
                        ui: 'action',
                        text: 'month',
                        pressed: true
                    },{
                        ui: 'action',
                        text: 'week'
                    },{
                        ui: 'action',
                        text: 'day'
                    }]
                },{
                    xtype: 'spacer'
                },
                {
                    xtype: 'button',
                    ui: 'action',
                    iconCls: 'add'
                }
            ]
        },
        {
            xtype: 'toolbar',
            ui: 'light',            
            dock: 'top',
            id:'calendar_top_toolbar',
            defaults: {
                iconMask: true
            },
            layout: {
                pack: 'justify',
                align: 'center' // align center is the default
            },
            items: [                
                {
                    xtype: 'button',
                    iconCls: 'arrow_left',
                    handler: function() {
                        $('#calendar').fullCalendar( 'prev' );
                        Ext.getCmp('calendar_top_toolbar').setTitle($('#calendar .fc-header-center h2').html());
                    },
                    listeners: {
                        "render": function() {									
                        var d=new Date();
                        var month=new Array(12);
                        month[0]="January";
                        month[1]="February";
                        month[2]="March";
                        month[3]="April";
                        month[4]="May";
                        month[5]="June";
                        month[6]="July";
                        month[7]="August";
                        month[8]="September";
                        month[9]="October";
                        month[10]="November";
                        month[11]="December";
                        var title = month[d.getMonth()]+" "+d.getFullYear();
                        Ext.getCmp('calendar_top_toolbar').setTitle(title);
                        }
                    }
                },
                {
                    xtype: 'button',
                    iconCls: 'arrow_right',
                    handler: function() {
                        $('#calendar').fullCalendar( 'next' );
                        Ext.getCmp('calendar_top_toolbar').setTitle($('#calendar .fc-header-center h2').html());
                    }
                }
            ]
    }],
    listeners:{
        render: function() {
            Ext.Ajax.request({
                url: '',
                method: 'POST',
                success: function(responseObject) {
                    var data = Ext.decode(responseObject.responseText);
                    $('#calendar').fullCalendar({
                        header: {
                            left: '',
                            center: 'title',
                            right: ''
                        },
                        titleFormat: {
                            day: 'ddd, MMM d, yyyy'
                        },
                        weekMode:"fixed",
                        editable: true,
                        events: data,
                        mobile: true,
                        eventClick: function(calEvent, jsEvent, view) {

                        }
                    });
                },
                failure: function(responseObject) {
                    var data = Ext.decode(responseObject.responseText);
                    console.info("Error",data);
                },
                scope: this
            });

            this.mon(Ext.getCmp('mainCalendar').el, {
                swipe: function(directiion) {
                    if(directiion.direction == "left") {
                        $('#calendar').fullCalendar( 'next' );
                        Ext.getCmp('calendar_top_toolbar').setTitle($('#calendar .fc-header-center h2').html());
                    } else if(directiion.direction == "right") {
                        $('#calendar').fullCalendar( 'prev' );
                        Ext.getCmp('calendar_top_toolbar').setTitle($('#calendar .fc-header-center h2').html());
                    }
                },
                scope: this
            }); 
        }
    }
});

var calendarInit = {
    getCalendarEntries : function() {
        alert('Hi there');
    }
}