var contactObj = {    
    init : function() {
        var contactStore = new Ext.data.Store({
            model: 'Contacts',
            sorters: 'NameLast',
            getGroupString : function(record) {
                return record.get('NameLast')[0];
            },
            proxy: {
                type: 'ajax',
                url: siteURL+'contacts/AppContacts/getContacts/0/0/0/0/json',
                params: {
                    authKey: authKey
                },
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        var listControl = new Ext.List({
            itemTpl: $('#contactList').html(),
            grouped: true,
            singleSelect: true,
            indexBar: true,
            store: contactStore,
            onItemDisclosure: function(record, btn, index) {
                Ext.getCmp('contactContainerPanel').layout.setActiveItem(1,'slide');
                var listItem = record.store.getAt(index);
                Ext.getCmp('contactDetailPanel').update(listItem.data);
            }
        });

        var contactPanel = new Ext.Panel({
            layout: 'fit',
            width: 320,
            height: 450,
            id: 'contactListPanel',
            dockedItems: [
                {
                    xtype: 'toolbar',
                    ui: 'light',
                    dock: 'top',
                    defaults: {
                        iconMask: true
                    },
                    items: [
                        {
                            xtype : 'searchfield',
                            placeHolder: 'Search',
                            listeners: {
                                scope: this,
                                keyup: function(field) {
                                    var value = field.getValue();

                                    if (!value) {
                                        contactStore.filterBy(function() {
                                            return true;
                                        });
                                    } else {
                                        var searches = value.split(' '),
                                            regexps  = [],
                                            i;

                                        for (i = 0; i < searches.length; i++) {
                                            if (!searches[i]) return;
                                            regexps.push(new RegExp(searches[i], 'i'));
                                        }

                                        contactStore.filterBy(function(record) {
                                            var matched = [];

                                            for (i = 0; i < regexps.length; i++) {
                                                var search = regexps[i];

                                                if (record.get('NameFirst').match(search) || record.get('NameLast').match(search)) matched.push(true);
                                                else matched.push(false);
                                            }

                                            if (regexps.length > 1 && matched.indexOf(false) != -1) {
                                                return false;
                                            } else {
                                                return matched[0];
                                            }
                                        });

                                        listControl.scroller.scrollTo({x: 0,y: 0});
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'spacer'
                        },
                        {
                            xtype: 'button',
                            ui: 'action',
                            iconCls: 'add'
                        },
                    ]
                },
                
            ],
            items: [
                    listControl
                ],
            listeners: {
                render: function() {
                    /*
                     * If our store is not loaded, then load and bind.
                     */
                    if (!Ext.isDefined(contactStore.totalLength) && !contactStore.lastOptions) {
                        contactStore.proxy.actionMethods.read = 'POST';
                        contactStore.proxy.extraParams = {authKey: authKey}
                        contactStore.load();
                        listControl.bindStore(contactStore);
                    }
                }
            }
        });

        var contactDetailPanel = new Ext.Panel({
            id: 'contactDetailPanel',
            xtype: 'form',
            tpl: Ext.XTemplate.from('contactDetails'),
            dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                ui: 'light',
                items: [{
                    text: 'Back',
                    ui: 'back',
                    handler: function() {
                        Ext.getCmp('contactContainerPanel').layout.setActiveItem(0,{ type: 'slide', reverse: true });
                    }
                }]
            }]
        });

        cpo.contacts = new Ext.Panel({
            id: 'contactContainerPanel',
            title: 'Contacts',
            iconCls: 'iconTabContacts',
            layout: 'card',
            activeItem: 0,
            items: [contactPanel,contactDetailPanel]
        });
        
        return cpo.contacts;
    }
}