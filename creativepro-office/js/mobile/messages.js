cpo.messages = new Ext.Panel({
    title: 'Messages',
    iconCls: 'iconTabMessages',
    badgeText: '4',
    dockedItems: [
        {
            xtype: 'toolbar',
            title: '<span class="headerIcon"></span>',
            dock: 'top',
            items: [{
                text: 'JSONP'
            },
            {
                xtype: 'spacer'
            },
        {
            text: 'XMLHTTP'
        }]
    }]
});