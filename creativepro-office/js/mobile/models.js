var modelsObj = {
    initModels : function() {
        Ext.regModel('ModelProjectSelect', {
            fields: [
                'ProjectID', 'Title'
            ]
        });

        Ext.regModel('ModelTaskSelect', {
            fields: ['TaskID', 'Title']
        });

        Ext.regModel('TaskListMaster', {
            fields: [
                'TasksOverdue', 'TasksToday', 'TasksTomorrow', 'TasksThisWeek', 'TasksNextWeek'
            ]
        });

        Ext.regModel('TaskList', {
            fields: [
                {name: 'TaskID', type: 'string'},
                {name: 'TaskNo', type: 'string'},
                {name: 'Title', type: 'string'},
                {name: 'ProjectTitle', type: 'string'},
                {name: 'Description',  type: 'string'},
                {name: 'StatusHuman',  type: 'string'},
                {name: 'StatusText',  type: 'string'},
                {name: 'DateEndRender',  type: 'string'}
            ]
        });

        Ext.regModel('Contacts', {
            fields: [
                {name: 'PID', type: 'string'},
                {name: 'NameFirst', type: 'string'},
                {name: 'NameLast',  type: 'string'},
                {name: 'Title', type: 'string'},
                {name: 'AvatarURL',  type: 'string', defaultValue: siteURL+'images/avatars/stockAvatar46.png'},
                {name: 'AvatarURLBig',  type: 'string', defaultValue: siteURL+'images/avatars/stockAvatar80.png'},
                {name: 'Company',  type: 'string'},
                {name: 'ContactItems', type: 'array'}
            ]
        });

        Ext.regModel('ContactItems', {
            fields: ['ContactType','ContactData','ContactLocation']
        });
    }
}