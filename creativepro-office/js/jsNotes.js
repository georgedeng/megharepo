var noteEditor;
var noteEntryFormHTML = '';

var getNotesObj = {
    getNotes : function(onWidget,itemID,itemType,start,limit,renderContainer) {
        if (limit == '' || limit == null) {
            limit = 5;
        }
		$.ajax({
		    type: "POST",
		    url:  siteURL+'notes/Notes/getNotes/'+onWidget+'/'+itemID+'/'+itemType+'/'+start+'/'+limit+'/json/0',
            dataType: 'json',
		    success: function(payload){
                if (onWidget == 1) {
                    renderNotesObj.renderWidgetNotes(payload,renderContainer,onWidget,itemID,itemType);
                } else {
                    renderNotesObj.renderFullNotes(payload,itemID,itemType);                    
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var initNotesFormObj = {
    initEditor : function() {
        $('#noteContentTextarea').wysiwyg({
            controls: {
                justifyFull: {visible: false}
            },
            formTableHtml: $('#modalWYSIWYGTableContainer').html(),
            formImageHtml: $('#modalWYSIWYGImageContainer').html(),            
            formHeight: 300,
            resizeOptions: {},
            css: siteURL+'css/editorCSS.css'
        });

        $('#noteContentTextarea').wysiwyg('clear');
    },
    attachNewNoteEventHandler : function() {
        $('#buttonNewNote').click(function() {
            $(this).hide();
            if (noteEntryFormHTML == '') {
                noteEntryFormHTML = $('#noteFormContainer').html();
                $('#noteFormContainer').html('');
            }
            $('#noteFormNewContainer')
                .html(noteEntryFormHTML)
                .show();

            initNotesFormObj.attachEventHandlersFullNoteForm();
            $('#actionNote').val('add');
        });            
    },
    attachEventHandlersFullNoteForm : function() {
        $('#buttonNoteSave').click(function() {
            var formObj = $(this).parent();
            updateNotesObj.saveNote(formObj);            
            $('#buttonNewNote').show();
            $('#noteFormNewContainer').html('');

            return false;
        });

        $('#buttonNoteCancel').click(function() {
            var action = $(this).parent().find('#actionNote').val();
            var formObj = $(this).parent();
            initNotesFormObj.clearFullNoteForm(formObj);            
            initNotesFormObj.closeFullNoteForm(formObj);
            $('#buttonNewNote').toggle();

            if (action == 'edit') {
                initNotesFormObj.clearAllNoteForms();
            }

            return false;
        });
        initNotesFormObj.initEditor();
    },
    attachEventHandlersNoteForm : function() {
		$('.saveNote').unbind().click(function() {
           var formObj = $(this).parent();
		   var action  = $(this).parent().find('.actionNote').val();
           updateNotesObj.saveNote(formObj,1);
           initNotesFormObj.clearNoteForm(formObj,action);
           return false;
        });

        $('.cancelNote').unbind().click(function() {
			var action = $(this).parent().find('.actionNote').val();
			var formObj = $(this).parent();
			initNotesFormObj.clearNoteForm(formObj,action);
			return false;
        });

        $('.checkboxClickable').unbind().click(function() {
           if ($('.noteMakePublic').is(':checked')) {
               $('.noteMakePublic').attr('checked',false);
           } else {
               $('.noteMakePublic').attr('checked',true);
           }
        });

		$('.txtNote').elastic();
	},
    clearAllNoteForms : function() {
        $('.noteFormContainer').each(function() {
            var formObj = $(this);
            initNotesFormObj.closeFullNoteForm(formObj);
        });
        $('.noteContainer:hidden').fadeIn('fast');
        $('#buttonNewNote').show();
    },
    clearFullNoteForm : function(formObj,action) {
        formObj.find('#noteMakePublic').attr('checked','checked');
    },
    closeFullNoteForm : function(formObj) {
        formObj.fadeOut();
        formObj.html('');
    },
    clearNoteForm : function(formObj,action) {
		if (action == 'add') {
			formObj.parent().find('.controlContainer').show();
		} else if (action == 'edit') {
			formObj.prev().show();
		}
		formObj.find('.txtNote').val('');
		formObj.hide();
    }
}

var renderNotesObj = {
    renderFullNotes : function(noteJSON) {
        var noteOut = [];
        var noteCount = noteJSON.Notes.length;
        $('#noteCount').html('(<span id="noteCountNumber">'+noteCount+'</span>)');

        if (noteCount>0) {
            $('#noteCount').removeClass('lightGray');
        }
        noteOut.push('<div id="noteGrid">');
        for(var a in noteJSON.Notes) {
            var renderStyle = 'style="display: none;"';
            if (noteJSON.Notes[a].Public == 1) {
                renderStyle = 'style="display: inline-block; margin-left: 6px;"';
            }

            var rowClass = 'noteRow';
            if (noteJSON.Notes[a].IsEditable != 1) {
                rowClass = '';
            }
            noteOut.push('<div id="containerNote_'+noteJSON.Notes[a].NID+'" itemID="'+noteJSON.Notes[a].NID+'" class="'+rowClass+'">');

                noteOut.push('<div class="oddRow" style="padding: 2px;">');
                    noteOut.push('<img class="avatar33 left" src="'+noteJSON.Notes[a].AvatarURL+'" />');
                    noteOut.push('<a class="personLink" personid="'+noteJSON.Notes[a].PID+'" href="#">'+noteJSON.Notes[a].NameFirst+' '+noteJSON.Notes[a].NameLast+'</a>');
                    noteOut.push('&nbsp;<span class="subText">'+noteJSON.Notes[a].DateNote+'</span>');
                    noteOut.push('&nbsp;<span class="icon_team_small isPublic" '+renderStyle+' isPublic="'+noteJSON.Notes[a].Public+'" style="margin-top: 6px;" title="'+commonTerms['notes_visible_client']+'"></span>');
                    noteOut.push('<div style="clear: left;"></div>');
                noteOut.push('</div>');
                //noteOut.push('<div class="noteContainer" id="noteContainer_'+noteJSON.Notes[a].NID+'" style="float: left; padding: 6px;">'+genericFunctions.replaceURLWithHyperlink(noteJSON.Notes[a].Note)+'</div>');
                noteOut.push('<div class="noteContainer" id="noteContainer_'+noteJSON.Notes[a].NID+'" style="float: left; padding: 6px;">'+noteJSON.Notes[a].Note+'</div>');
                noteOut.push('<div style="clear: left;"></div>');
            noteOut.push('</div>');
			noteOut.push('<div id="containerNoteEditFormFull_'+noteJSON.Notes[a].NID+'" class="noteFormContainer" style="padding: 6px; display: none;"></div>');
        }
        noteOut.push('</div>');
        var noteString = noteOut.join('');
        $('#noteListContainer').html(noteString);
		$('#noteListContainer').alternateContainerRowColors();

        $('#noteGrid').hoverButtons({
           elementClass: 'noteRow',
           width:        '70px'
      });

      contactObj.makeContactLinks();
    },
    renderWidgetNotes : function(noteJSON,renderContainer,onWidget,itemID,itemType) {
        var noteOut = [];        

        noteOut.push('<div class="boxBlue"><div style="padding: 3px;">');
        noteOut.push('<div class="controlContainer">');
        noteOut.push('<a href="#" class="icon_notes_add_small linkNewNote" style="float: left;">'+commonTerms['noteNew']+'</a>');
        noteOut.push('<div style="float: right; width: 50px;"><span class="link-prev '+noteJSON.MoveBack+' shuffleNotes back" start="'+noteJSON.PrevStart+'" style="float: left; margin-right: 6px;"></span><span class="link-next '+noteJSON.MoveForward+' shuffleNotes forward" start="'+noteJSON.NextStart+'" style="float: left;"></span></div>');
        noteOut.push('</div>');

			/*
			 * New note form
			 */
			noteOut.push('<div class="noteFormContainer" style="display: none;">');
			noteOut.push('<input type="hidden" class="onWidgetNote" value="'+onWidget+'" />');
			noteOut.push('<input type="hidden" class="actionNote" value="add" />');
			noteOut.push('<input type="hidden" class="itemIDNote" value="'+itemID+'" />');
            noteOut.push('<input type="hidden" class="noteID" value="" />');
			noteOut.push('<input type="hidden" class="itemTypeNote" value="'+itemType+'" />');
			noteOut.push('<input type="hidden" class="renderContainer" value="'+renderContainer+'" />');
			noteOut.push('<textarea class="txtNote expanding" style="width: 97%; height: 50px; max-height: 100px; margin-bottom: 6px;"></textarea>');

            if ($('#clientArea').val() != 1) {
                noteOut.push('<input type="checkbox" class="noteMakePublic" value="1" /> <span class="checkboxTrigger clickable">'+commonTerms['noteMakePublic']+'</span><br />');
            }

            noteOut.push('<button class="smallButton saveNote" style="float: left; margin: 6px 6px 0 0;"><span class="save">'+commonTerms['save']+'</span></button>');
			noteOut.push('<button class="smallButton cancelNote" style="margin-top: 6px; float: left;"><span class="cancel">'+commonTerms['cancel']+'</span></button>');
			noteOut.push('<div style="clear: left; height: 12px;"></div>');
			noteOut.push('</div>');

		noteOut.push('<div style="clear: left;"></div>');
        noteOut.push('</div></div>');
        noteOut.push('<div id="noteGridWidget">');

		for(var a in noteJSON.Notes) {
            var renderStyle = 'style="display: none;"';
            if (noteJSON.Notes[a].Public == 1) {
                renderStyle = 'style="display: inline-block; margin-left: 6px;"';
            }
            noteOut.push('<div id="containerNote_'+noteJSON.Notes[a].NID+'" class="row" style="width: 295px; overflow: auto;">');

            if (noteJSON.Notes[a].IsEditable == 1) {
                noteOut.push('<a href="#" class="buttonEditSmall editNote" title="'+commonTerms['edit']+'"></a><a href="#" class="buttonDeleteSmall deleteNote" title="'+commonTerms['delete']+'"></a>');
            }
            noteOut.push('<span class="subText">'+noteJSON.Notes[a].DateNote+'</span> <span class="icon_team_small isPublic" '+renderStyle+' isPublic="'+noteJSON.Notes[a].Public+'"></span><br />');
			noteOut.push('<span class="noteContainer">'+genericFunctions.replaceURLWithHyperlink(noteJSON.Notes[a].Note)+'</span>');
			noteOut.push('</div>');
			noteOut.push('<div id="containerNoteEditForm_'+noteJSON.Notes[a].NID+'" style="display: none;"></div>');
        }
        noteOut.push('</div>');
        var noteString = noteOut.join('');
        $('#'+renderContainer).html(noteString);
		$('#'+renderContainer).alternateContainerRowColors();
        renderNotesObj.attachEventHandlersNotes();
		initNotesFormObj.attachEventHandlersNoteForm();        
    },
    attachEventHandlersNotesFull : function() {
        $('#noteListContainer').delegate('.buttonEdit', 'click', function() {
            initNotesFormObj.clearAllNoteForms();
            var noteID  = $(this).attr('itemID');
            var content = $('#noteContainer_'+noteID).html();
            $('#noteContainer_'+noteID).fadeOut('fast');

            if (noteEntryFormHTML == '') {
                noteEntryFormHTML = $('#noteFormContainer').html();
                $('#noteFormContainer').html('');
            }

            $('#containerNoteEditFormFull_'+noteID)
                .append(noteEntryFormHTML)
                .fadeIn('fast');

            $('#actionNote').val('edit');
            $('#noteID').val(noteID);
            
            initNotesFormObj.attachEventHandlersFullNoteForm();
            $('#noteContentTextarea').wysiwyg('setContent',content);

            return false;
        });

        $('#noteListContainer').delegate('.buttonDelete', 'click', function() {
            var noteID = $(this).attr('itemID');
            $('#containerNote_'+noteID).fadeOut('slow', function() {
                $(this).remove();
                $('#noteCountNumber').html(parseInt($('#noteCountNumber').html()-1));
            });
            updateTrashObj.sendToTrash('note',noteID);            

            return false;
        });
    },
    attachEventHandlersNotes : function() {
        $('.linkNewNote').click(function() {
           $(this).parent().hide();
           $(this).parent().next().fadeIn('fast');
           return false;
        });

		$('.shuffleNotes').click(function() {
            var start = $(this).attr('start');
            var formObj = $(this).parent().parent().next();
            var onWidget        = formObj.find('.onWidgetNote').val();
            var itemID          = formObj.find('.itemIDNote').val();
            var itemType        = formObj.find('.itemTypeNote').val();
            var renderContainer = formObj.find('.renderContainer').val();
            getNotesObj.getNotes(onWidget, itemID, itemType, start, 5, renderContainer);
		});

		$('#noteGridWidget').delegate('.editNote', 'click', function() {
			var noteIDString = $(this).parent().attr('id');
			var noteIDArray  = noteIDString.split('_');
			var noteID       = noteIDArray[1];
			var noteText     = $(this).parent().find('.noteContainer').html();
            noteText         = genericFunctions.txtToField(noteText);
            var isPublic     = $(this).parent().find('.isPublic').attr('isPublic');
			var formHTML     = $(this).parent().parent().parent().find('.noteFormContainer').html();
			$(this).parent().hide();
			$('#containerNoteEditForm_'+noteID).html(formHTML).show();
			initNotesFormObj.attachEventHandlersNoteForm();

			$('#containerNoteEditForm_'+noteID).find('.txtNote').elastic().val(noteText);
            $('#containerNoteEditForm_'+noteID).find('.noteID').val(noteID);
            $('#containerNoteEditForm_'+noteID).find('.noteMakePublic').attr('checked',false);
            if (isPublic == 1) {
                $('#containerNoteEditForm_'+noteID).find('.noteMakePublic').attr('checked',true);
            }

			$('#containerNoteEditForm_'+noteID).find('.actionNote').val('edit');

			return false;
		});

		$('#noteGridWidget').delegate('.deleteNote', 'click', function() {
			var noteIDString = $(this).parent().attr('id');
			var noteIDArray = noteIDString.split('_');
			var noteID = noteIDArray[1];
			updateTrashObj.sendToTrash('note',noteID);
			$(this).parent().fadeOut('fast');
			return false;
		});
    }    
}

var updateNotesObj = {
    saveNote : function(formObj,onWidget) {
        var action,itemID,itemType,renderContainer,note,noteID,makePublic,limit;
        if (onWidget == 1) {
            limit           = 5;
            action          = formObj.find('.actionNote').val();
            onWidget        = formObj.find('.onWidgetNote').val();
            itemID          = formObj.find('.itemIDNote').val();
            itemType        = formObj.find('.itemTypeNote').val();
            renderContainer = formObj.find('.renderContainer').val();
            note            = formObj.find('.txtNote').val();
            noteID          = formObj.find('.noteID').val();
            makePublic = 0;
            if (formObj.find('.noteMakePublic').is(':checked')) {
                makePublic = 1;
            }
        } else {
            limit           = 20;
            action          = formObj.find('#actionNote').val();
            onWidget        = 0;
            itemID          = formObj.find('#itemIDNote').val();
            itemType        = formObj.find('#itemTypeNote').val();
            renderContainer = '';
            //note            = noteEditor.getContent();
            note            = $('#noteContentTextarea').wysiwyg('getContent');
            noteID          = formObj.find('#noteID').val();
            makePublic = 0;
            if (formObj.find('#noteMakePublic').is(':checked')) {
                makePublic = 1;
            }
            if (note == '<html/>' || note == '<br/>') {
                note = '';
            }
        }

        var formHash = {
            action     : action,
			noteID     : noteID,
            onWidget   : onWidget,
            itemID     : itemID,
            itemType   : itemType,
            makePublic : makePublic,
            note       : note
        };

        $.ajax({
            type:  "POST",
            url:   siteURL+'notes/Notes/saveNote',
            data: formHash,
            success: function(payload){
				getNotesObj.getNotes(onWidget,itemID,itemType,0,limit,renderContainer);
                initNotesFormObj.clearFullNoteForm(formObj);
                initNotesFormObj.closeFullNoteForm(formObj);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
}

$(document).ready(function() {
	if ($('#pageLeftColumn').attr('pageID') == 'viewProjectDetails') {
        initNotesFormObj.attachNewNoteEventHandler();
        renderNotesObj.attachEventHandlersNotesFull();
    }
});