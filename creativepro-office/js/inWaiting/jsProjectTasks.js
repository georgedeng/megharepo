function getFreqTask() {
	var freqTaskID    = $('freqTaskID');
	var freqTaskIndex = freqTaskID.selectedIndex;
	var freqTaskID    = freqTaskID[freqTaskIndex].value;
	if (freqTaskID != 'Select...') {
		x_getFreqTask(freqTaskID,returnGetFreqTask);
	}	
}

function returnGetFreqTask(string) {
	var stringArray = string.split("|");
	var taskTitle   = stringArray[0];
	var taskDesc    = stringArray[1];
	
	$('taskTitle').value = taskTitle;
	$('taskDesc').value  = taskDesc;
}

function taskAddWindowOpen(action,taskID,projID) {
	$('projectMilestoneAddEdit').style.display = 'none';	
	new Effect.SlideDown('projectTaskAddEdit', {direction: 'top', duration: .3});
	if (action == 'add') {
		$('taskHeaderTitle').innerHTML = 'Add New Task';
	} else if (action == 'edit') {
		$('taskHeaderTitle').innerHTML = 'Edit Task';
		x_taskEdit(taskID,projID,returnTaskEdit);
	}	
}

function taskAddWindowClose() {
	if ($('projectTaskAddEdit').style.display == '') {
		new Effect.SlideUp('projectTaskAddEdit', {direction: 'top', duration: .2});
		$('taskHeaderTitle').innerHTML = 'Project Milestones and Tasks';	
		clearForm();
	}	
}

function taskAddEditSave(projID) {	
	var doWhat        = $('doWhat').value;
	var taskID        = $('taskID').value;
	var taskTitle     = removeChar($('taskTitle').value);
	var taskDesc      = removeChar($('taskDesc').value);
	var taskDateStart = $('taskStart').value;
	var taskDateEnd   = $('taskEnd').value;
	var taskPriority  = $('priority').value;
	var taskSave      = $('saveFreqTask');
	var emailTask     = $('emailTask');	
	
	var teamID        = $('teamMemberID');
	if (teamID) {
		var teamIndex = teamID.selectedIndex;
		var teamID    = teamID[teamIndex].value;
		if (teamID == 'Select...') { teamID = 0; }
	} else { teamID = 0; }	
	
	var msID = $('mileStoneID');
	if (msID) {
		var msIndex = msID.selectedIndex;
		var msID    = msID[msIndex].value;
		if (msID == 'Select...') { msID = 0; }
	} else { msID = 0; }
		
	// Save frequent task
	var taskSaveValue = null;
	if (taskSave.checked == true) { taskSaveValue = 1; }
	else { taskSaveValue = 0; }
	
	// Email task
	var emailTaskValue = null;
	if (emailTask.checked == true) { emailTaskValue = 1; }
	else { emailTaskValue = 0; }
		
	// Check for data errors - missing fields
	if (taskTitle == "") { 
		var formError = 1;
		var formErrorText = "<font color='red'>You must enter an Task Title.</font>";
		var formErrorText = "You must enter a title for this task.";
		$('taskFormMessage').style.display = 'block';
		$('taskFormMessage').className     = 'errorMessage';
		$('taskFormMessage').innerHTML     = formErrorText;
		$('taskTitle').style.background  = "#F9DF9F";
	}
		
	if (formError != 1) {	
		// Clear form and prep for another entry
		$('taskTitle').value = '';
		$('taskDesc').value  = '';
		$('taskStart').value = '';
		$('taskEnd').value  = '';
						
		var phpString = projID+"|"+taskTitle+"|"+taskDesc+"|"+taskDateStart+"|"+taskDateEnd+"|";
		phpString    += taskPriority+"|"+teamID+"|"+msID+"|"+taskSaveValue+"|"+taskID+"|"+doWhat+"|"+emailTaskValue;
		x_taskAddEditSave(phpString,returnTaskAdd);		
	}	
}

function returnTaskAdd(string) {
	// Close add/edit windows
	mileStoneAddWindowClose();
	taskAddWindowClose();
	
	// returnTaskAdd is the callback function for both 
	// tasks and milestones:
	var stringArray = string.split('~~|~~');
	var taskString      = stringArray[0];
	var freqTaskString  = stringArray[1];
	var mileStoneString = stringArray[2]; 
	 
	$('projectViewTasks').innerHTML = taskString;
	$('freqTaskID').innerHTML   = freqTaskString;
	$('mileStoneID').innerHTML  = mileStoneString;
}

function mileStoneAddWindowOpen(action,msID,projID) {
	$('projectTaskAddEdit').style.display = 'none';	
	
	new Effect.SlideDown('projectMilestoneAddEdit', {direction: 'top', duration: .3});
	if (action == 'add') {
		$('taskHeaderTitle').innerHTML = 'Add New Milestone';
	} else if (action == 'edit') {
		$('taskHeaderTitle').innerHTML = 'Edit Milestone';
		x_mileStoneEdit(msID,returnMileStoneEdit);
	}	
}

function mileStoneAddWindowClose() {
	if ($('projectMilestoneAddEdit').style.display == '') {
		new Effect.SlideUp('projectMilestoneAddEdit', {direction: 'top', duration: .3});
		$('taskHeaderTitle').innerHTML = 'Project Milestones and Tasks';
		clearForm();
	}	
}

function mileStoneAddEditSave(projID) {
	var doWhat  = $('doWhat').value;
	var msID    = $('msID').value;
	var msTitle = removeChar($('msTitle').value);
	var msStart = $('msStart').value;
	var msEnd   = $('msEnd').value;
	
	var msTeamID = $('msTeamMemberID');
	if (msTeamID) {
		var msTeamIndex = msTeamID.selectedIndex;
		var msTeamID    = msTeamID[msTeamIndex].value;
	} else { msTeamID = 0; }
	if (msTeamID == 'Select...') { msTeamID = 0; }	
	
	// Check for data errors - missing fields
	if (msTitle == "") { 
		var formError = 1;
		var formErrorText = "You must enter a title for this milestone.";
		$('msFormMessage').style.display = 'block';
		$('msFormMessage').className     = 'errorMessage';
		$('msFormMessage').innerHTML     = formErrorText;
		$('msTitle').style.background  = "#F9DF9F";
	}
		
	if (formError != 1) {			
		var phpString = projID+"|"+msTitle+"|"+msStart+"|"+msEnd+"|"+msTeamID+"|"+doWhat+"|"+msID;
		x_mileStoneAdd(phpString,returnTaskAdd);
		
		// Clear form
		$('msTitle').value = '';
		msTeamID.selectedIndex = 0;			
	}	
}

function returnMileStoneEdit(string) {
	var stringArray   = string.split("|");
	var msID    = stringArray[0];
	var msTitle = stringArray[1];
	var msDateStart = stringArray[2];
	var msDateEnd   = stringArray[3];
	var msTeamMember = stringArray[4];
	
	$('msID').value   = msID;
	$('doWhat').value = 'edit';
	
	$('msTitle').value = msTitle;
	$('msStart').value = msDateStart;
	$('msEnd').value   = msDateEnd;
	
	// Loop through taskTeamMember options and determine which one to select
	if (msTeamMember != '') {
		var teamMembers = $('msTeamMemberID').options.length;
		for(var a=0;a<=(teamMembers-1);a++) {
			var teamMemberValue = $('msTeamMemberID').options[a].value;
			if (teamMemberValue == msTeamMember) {
				$('msTeamMemberID').options.selectedIndex = a;
			}
		}		
	}
}

function returnTaskEdit(string) {
	var stringArray   = string.split("|");
	var taskID        = stringArray[0];
	var taskTitle     = stringArray[1];
	var taskDesc      = stringArray[2];
	var taskDateStart = stringArray[3];
	var taskDateEnd   = stringArray[4];
	var taskAssigned  = stringArray[5];
	
	$('taskTitle').value = taskTitle;
	$('taskDesc').value  = taskDesc;
	$('taskStart').value = taskDateStart;
	$('taskEnd').value   = taskDateEnd;
	$('doWhat').value    = 'edit';
	$('taskID').value    = taskID;	
	
	// Loop through taskTeamMember options and determine which one to select
	if (taskAssigned != '') {
		var teamMembers = $('teamMemberID').options.length;
		for(var a=0;a<=(teamMembers-1);a++) {
			var teamMemberValue = $('teamMemberID').options[a].value;
			if (teamMemberValue == taskAssigned) {
				$('teamMemberID').options.selectedIndex = a;
			}
		}		
	}	
}

function returnTaskDelete(taskID) {
	Effect.Fade('taskTR'+taskID, {delay: .1, duration: 1});
}

function clearForm() {
	// Clear fields and prep for new entry or edit
	
	// Clear task fields
	$('taskTitle').value = '';
	$('taskDesc').value  = '';
	$('taskStart').value = '';
	$('taskEnd').value   = '';
	$('doWhat').value    = '';
	$('taskID').value    = '';	
	$('saveFreqTask').checked = false;
	$('emailTask').checked    = false;
	if ($('teamMemberID')) {
		$('teamMemberID').options.selectedIndex = 0;
	}	
	if ($('freqTaskID')) {
		$('freqTaskID').options.selectedIndex = 0;
	}	
	
	// Clear milestone fields
	$('msTitle').value = '';
	$('msStart').value = '';
	$('msEnd').value   = '';
	if ($('msTeamMemberID')) {
		$('msTeamMemberID').options.selectedIndex = 0;
	}	
	
	// Clear hidden fields
	$('doWhat').value = null;
	$('taskID').value = null;
	$('msID').value   = null;	
}

function getFreqTasksEdit(doType) {
	x_getFreqTasks(doType,returnGetFrequentTasks);
}
function returnGetFrequentTasks(string) {
	openWindow(string,'taskEditDisplay',250,0,1,'','','../');
}
function editFreqTask(FtaskID) {
	var editTD   = 'edit'+FtaskID;
	var taskText =  $(editTD).innerHTML;
	
	$(editTD).innerHTML = '<input type="text" id="editText'+FtaskID+'" style="width: 171px;" value="'+taskText+'" onBlur="editFreqTask2('+FtaskID+');">';
}

function editFreqTask2(FtaskID) {
	var editText = $('editText'+FtaskID).value;
	x_freqTaskEdit(editText,FtaskID,returnEditFreqTask);	
}

function returnEditFreqTask(string) {
	var stringArray  = string.split("|");
	var popUpString  = stringArray[0];
	var selectString = stringArray[1];
	$('windowContent').innerHTML = popUpString;
	$('freqTaskID').innerHTML = selectString;
}

function deleteFreqTask(FtaskID) {	
	x_freqTaskDelete(FtaskID,returnDeleteFrequentTasks);
}
function returnDeleteFrequentTasks(string) {
	var stringArray  = string.split("|");
	var popUpString  = stringArray[0];
	var selectString = stringArray[1];
	$('windowContent').innerHTML = popUpString;
	$('freqTaskID').innerHTML = selectString;
}
/*
function confirmDeleteTask(taskID,projID) {
	var OK = confirm("Are you sure you want to delete this Task?");
	if (OK == true) {
		x_taskDelete(taskID,projID,returnTaskAdd);
	}
}
*/

function taskUpdateStatus(elementID,elementNo,elementType) {
	x_taskUpdateStatus(elementID,elementNo,elementType,returnTaskUpdateStatus);
}

function returnTaskUpdateStatus(string) {
	var stringArray = string.split('|');
	var statusString = stringArray[0];
	var taskID       = stringArray[1];
	var elementNo    = stringArray[2];
	var newStatus    = stringArray[3];
	var elementType  = stringArray[4];
	if (elementType == 'Milestone') { 
		var statusID = 'mileStoneStatus_'+elementNo; 
		var msRowID = 'msRow_'+elementNo;
		if (newStatus >= 2) {	
			$(msRowID).className = 'emphRow';
		}	
	} else if (elementType == 'Task') { 
		var statusID = 'taskStatus_'+elementNo; 
		var strikeThroughID = 'lineThroughSpan_'+elementNo;
		if (newStatus == 2) {
			$(strikeThroughID).className = 'completedItem';		
		} else {
			$(strikeThroughID).className = '';	
		}
	}
	
	$(statusID).innerHTML = statusString;	
}

function taskUpdatePriority(elementID,elementNo,elementType) {
	x_taskUpdatePriority(elementID,elementNo,elementType,returnTaskUpdatePriority);
}

function returnTaskUpdatePriority(string) {
	var stringArray = string.split('|');
	var priorityString = stringArray[0];
	var taskID         = stringArray[1];
	var elementNo      = stringArray[2];
	var newPriority    = stringArray[3];
	var elementType    = stringArray[4];
	if (elementType == 'Milestone') { 
		
	} else if (elementType == 'Task') { 
		var priorityID = 'taskPriority_'+elementNo; 		
	}
	
	$(priorityID).innerHTML = priorityString;	
}

function viewGant(projID) {
	$('gantSwitch').innerHTML = '<a href="#" onClick="viewTasks('+projID+'); return false;" title="View Tasks">View Tasks</a>';
	x_viewGant(projID,returnViewGant);
}

function returnViewGant(string) {
	//alert(string);
	var stringArray = string.split('|');
	var gantImage = stringArray[0];
	var gantW     = stringArray[4];
	var gantH     = stringArray[5];
	$('projectViewTasks').style.overflow = 'auto';
	if (gantH>=400) { var boxH = '400px'; } 
	else { var boxH = parseInt(gantH)+parseInt(30);	}
	$('projectViewTasks').style.height = boxH;
	$('projectViewTasks').innerHTML = '<img src="'+gantImage+'" />';
}

function viewTasks(projID) {
	$('gantSwitch').innerHTML = '<a href="#" onClick="viewGant('+projID+'); return false;" title="View Gant Chart">View Gant Chart</a>';
	$('projectViewTasks').style.overflow = '';
	$('projectViewTasks').style.height = '';
	$('projectViewTasks').innerHTML = '';
	x_getProjectTasks(projID,returnTaskAdd);
}