function showRole(role) {
	var assignmentTR = $('assignmentTR');
	// Parse role
	var roleDiv = $('roleDiv');
	if (role != 'Select...') {
		var roleArray = role.split('|');
		var roleNum   = roleArray[0];
		if (roleNum == 1) {
			// Administrator
			roleDiv.className = 'userAdminMed';
			roleDiv.innerHTML = 'Administrator';
		} else if (roleNum == 2) {
			// Manager
			roleDiv.className = 'userManagerMed';
			roleDiv.innerHTML = 'Manager';
			assignmentTR.style.display = '';
		} else if (roleNum == 3) {
			// Employee
			roleDiv.className = 'userEmployeeMed';
			roleDiv.innerHTML = 'Employee';
			assignmentTR.style.display = '';
		} else if (roleNum == 4) {
			// Contractor
			roleDiv.className = 'userContractorMed';
			roleDiv.innerHTML = 'Contractor';
			assignmentTR.style.display = '';			
		}
	} else {
		roleDiv.className = '';
		roleDiv.innerHTML = '';
	}	
}

function userAddEditSave() {
	// Get form input
	var uID        = $('uID').value;
	var nameFirst  = $('nameFirst').value;
	var nameLast   = $('nameLast').value;
	var email      = $('newUserid').value;
	var phone      = $('phone').value;
	var role       = $('userRole');
	var roleSelect = role[role.selectedIndex].value;
	var assignAll  = $('pAssignAll');
	var assignInd  = $('pAssignInd');
	if (assignInd.checked) { var assignment = '0'; }
	else { assignment = '1'; }
	var timeZone   = $('timeZone');
	var timeZoneSelect = timeZone[timeZone.selectedIndex].value;
	var comments   = $('comments').value;
	
	// Do error checking
	if ( (nameFirst == '') || (nameLast == '') || (nameFirst == 'First') || (nameLast == 'Last') ) {
		$('messages').className = 'errorMessage';
		$('messages').innerHTML = 'Please enter a complete name.';
	} else if (email == '') {
		$('messages').className = 'errorMessage';
		$('messages').innerHTML = 'Please enter an email address.';
	} else if ( (roleSelect == 'Select...') || (roleSelect == '') ) {
		$('messages').className = 'errorMessage';
		$('messages').innerHTML = 'Please select a role for this user.';
	} else if (!isValidEmail(email)) {
		$('messages').className = 'errorMessage';
		$('messages').innerHTML = 'Please enter a valid email address.';
	} else {
		// Are we adding or editing a team member?
		if ($('edit').value == 1) {
			var edit = 1;
		} else {
			var edit = 0;
		}
		x_userAddEditSave(edit,uID,nameFirst,nameLast,email,phone,roleSelect,assignment,timeZoneSelect,comments,getAllTeamMembers);
	}	
}

function getAllTeamMembers(string) {
	// Close add/edit window
	if ($('userAddEdit').style.display != 'none') {
		new Effect.SlideUp('userAddEdit', {direction: 'top', duration: .2});
	}
	
	// Blank all input form fields
	$('edit').value = '0';
	$('uID').value = '';
	$('nameFirst').value = '';
	$('nameLast').value = '';
	$('newUserid').value = '';
	$('phone').value = '';
	var role       = $('userRole');
	role.selectedIndex = 0;
	$('pAssignAll').checked = false;
	$('pAssignInd').checked = true;
	var timeZone   = $('timeZone');
	timeZone.selectedIndex = 0;
	$('comments').value = '';
	
	// Reset page title
	$('userAddEditTitle').innerHTML = '&nbsp;&nbsp;Add New Team Member';
	
	// Clear 'messages' div of any error messages and reset class
	$('messages').className = '';
	$('messages').innerHTML = '';
	
	// Render team members
	$('teamMembers').innerHTML = string;
}

function userAddWindowOpen() {
	new Effect.SlideDown('userAddEdit', {direction: 'top', duration: .3});
	$('userAddEditTitle').innerHTML = '&nbsp;&nbsp;Add New Team Member';
}
function userAddWindowClose() {
	// Clear messages div
	$('messages').innerHTML = '';
	new Effect.SlideUp('userAddEdit', {direction: 'top', duration: .3});
}

function teamMemberEdit(uID) {
	x_teamMemberEdit(uID,returnTeamMemberEdit);
}

function returnTeamMemberEdit(string) {
	// Parse string
	var stringArray = string.split('|');
	var uID        = stringArray[0];
	var userName   = stringArray[1];
	var role       = stringArray[2];
	var perm       = stringArray[3];
	var nameFirst  = stringArray[4];
	var nameLast   = stringArray[5];
	var phone      = stringArray[6];
	var comments   = stringArray[7];
	var timeZone   = stringArray[8];
	var assignment = stringArray[9];
	var gmtOffset  = stringArray[10];
	var selectTZ   = timeZone+'|'+gmtOffset;
	
	// Set hidden fields 
	$('edit').value = 1;
	$('uID').value = uID;
	$('origUserid').value = userName;
	
	$('nameFirst').value = nameFirst;
	$('nameLast').value  = nameLast;
	$('newUserid').value = userName;
	$('phone').value     = phone;
	$('comments').value  = comments;
	
	// Set assignment radio buttons	
	if (assignment == 1) {
		$('pAssignAll').checked = true;
	} else {
		$('pAssignInd').checked = true;
	}
	if (role > 1) {
		$('assignmentTR').style.display = '';
	}
	
	// Set role select menu
	var roleSelect = $('userRole');
	roleSelect.selectedIndex = role;
	
	// Loop through all time-zones and find a match
	var timeZoneSelect = $('timeZone');
	for (var a=0; a < timeZoneSelect.length; a++) { 
		timeZoneValue = timeZoneSelect.options[a].value;
		if (timeZoneValue == selectTZ) {
			timeZoneSelect.options[a].selected = true;
		}
	}
			
	new Effect.SlideDown('userAddEdit', {direction: 'top', duration: .3});
	$('userAddEditTitle').innerHTML = '&nbsp;&nbsp;Edit Team Member: '+nameFirst+' '+nameLast;
}