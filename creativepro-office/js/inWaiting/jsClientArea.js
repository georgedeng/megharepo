function getProjStats(projID) {
	x_getProjStats(projID,returnProjInfo);
}

function getProjInvoices(projID) {
	x_getProjInvoices(projID,returnProjInfo);
}

function getProjMessages(projID) {
	x_getProjMessages(projID,returnProjInfo);
}

function getProjFiles(projID) {
	x_getProjFiles(projID,returnProjInfo);
}

function returnProjInfo(string) {
	var stringArray = string.split('|');
	var projWinContent = stringArray[0];
	var projID = stringArray[1];
	$('window'+projID).innerHTML = projWinContent;
}

function sortFiles(projID,order,acdc) {
	$('fileOrder').value = order;
	$('fileAcDc').value = acdc;
	x_getProjFiles(projID,order,acdc,returnProjInfo); 
}