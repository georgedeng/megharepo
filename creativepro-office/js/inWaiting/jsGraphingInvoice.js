function graphInvoiceCompare(year) {
	x_graphInvoiceCompare(year,0,returnGraphInvoice);
}

function graphInvoiceProject(year) {
	x_graphInvoiceProject(year,0,returnGraphInvoice);
}

function graphInvoiceTop(year) {
	x_graphInvoiceTop(year,returnGraphInvoice);
}

function returnGraphInvoice(string) {
	var string1Array = string.split("~~|~~");
	var pngFileName = string1Array[0];
	var legendTable = string1Array[1];
	var graphIM     = string1Array[2];
	var graphIMName = string1Array[3];
	var graphTitle  = string1Array[4];
	var yearSelect  = string1Array[5];
	
	var yearID      = document.getElementById('yearSelectMenu');
	var yearIDIndex = yearID.selectedIndex;
	var year        = yearID[yearIDIndex].value;
	
	var graphHTML = '';
	var graphPNG  = '';
	
	if (pngFileName == "No data") {
		graphHTML = '<div style="margin-top: 160px;"><b>There is no inventory data for this location.</b></div>';
	} else {	
		graphPNG  += '<img src="'+pngFileName+'" ISMAP USEMAP="'+graphIMName+'" border=0>';	
	}
		
	document.getElementById('graphTitleInvoice').innerHTML = '';
	document.getElementById('graphPNG').innerHTML          = '';
	document.getElementById('graphLegend').innerHTML       = '';
	document.getElementById('yearSelect').innerHTML        = '';
	
	document.getElementById('graphTitleInvoice').innerHTML = graphTitle;
	document.getElementById('graphPNG').innerHTML          = graphPNG;
	document.getElementById('graphLegend').innerHTML       = legendTable;
	document.getElementById('graphMap').innerHTML          = graphIM;
	document.getElementById('yearSelect').innerHTML        = yearSelect;	
}