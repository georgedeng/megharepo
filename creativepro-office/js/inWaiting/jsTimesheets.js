function jumpToWeek() {
	var jumpToDate = $("jumpToDate").value;
	var dateArray = jumpToDate.split('-');
	var day     = dateArray[2];
	var month   = dateArray[1];
	var year    = dateArray[0];
	var timeSheetDate = year+'-'+month+'-'+day;
	x_getTimesheet(timeSheetDate,returnGetTimesheet);
}

function changeWeek(date) {
	var dateArray = date.split('|');
	var day     = dateArray[0];
	var month   = dateArray[1];
	var year    = dateArray[2];
	var dayName = dateArray[3];
	var timeSheetDate = year+'-'+month+'-'+day;
	x_getTimesheet(timeSheetDate,returnGetTimesheet);
}

function returnGetTimesheet(string) {
	var stringArray = string.split('~~|~~');
	var timeSheetString = stringArray[0];
	var nextLink   = stringArray[1];
	var lastLink   = stringArray[2];
	var titleDate  = stringArray[3]; 
	var startDate  = stringArray[4]; 
	$("timeSheetDisplay").innerHTML = timeSheetString;
	$("lastWeekButton").innerHTML = '<a href="#" onClick="changeWeek(\''+lastLink+'\'); return false;" title="Last Week"><img src="../images/button_prev.gif" style="border: none;" alt="Last Week"></a>';
	$("nextWeekButton").innerHTML = '<a href="#" onClick="changeWeek(\''+nextLink+'\'); return false;" title="Next Week"><img src="../images/button_next.gif" style="border: none;" alt="Next Week">';
	$("dateTitle").innerHTML      = '(week ending '+titleDate+')';
	$('projectMenuDate').value    = startDate;
	
	// Reset Project Select Menu
	var pID    = $('projectSelectMenu');
	pID.selectedIndex = 0;	
}

function timesheetAddProject() {
	var pID    = $('projectSelectMenu');
	var pIndex = pID.selectedIndex;
	var pID    = pID[pIndex].value;
	var startDate = $('projectMenuDate').value;
	
	// Check for task
	if (taskID == 'undefined') { var taskID = 0; }
	
	if ( (pID != '') && (pID != 'Select Project...') ) {
		x_addProject(pID,taskID,startDate,returnGetTimesheet);
	}	
}

function selectProject(pID,taskID) {
	var startDate = $('projectMenuDate').value;
	x_selectProject(pID,taskID,startDate,returnSelectProject);
}

function returnSelectProject(string) {
	var stringArray = string.split('|||');
	var showTasks   = stringArray[1];
	var content     = stringArray[0];
	
	if (showTasks == 1) {
		$('taskSelect').innerHTML = content;
	} else {
		returnGetTimesheet(content);
	}
}

function updateHours(field,date) {
	// Parse field
	var fieldArray = field.split('_');
	var projID     = fieldArray[1];
	var taskID     = fieldArray[2];
	var fieldNo    = fieldArray[3];
	var rowNo      = fieldArray[4];
	var dayTotalField = 'dayTotal_'+fieldNo;		
	var hours = $(field).value;
	var dayTotal = 0;
	
	// Total day row : loop through each timesheet row and add up
	// If we're > 24, we have an error
	var totalRows = document.getElementsByClassName('timeSheetProjectRow');
	var projIDArray = new Array();
	var taskIDArray = new Array();
	for(var i = 0; i < totalRows.length; i++) {
		var rowID = totalRows[i].id;
		var rowIDArray = rowID.split('_');
		projIDArray[i] = rowIDArray[1];
		taskIDArray[i] = rowIDArray[2];
	}
		
	for(var a=0;a<totalRows.length;a++) {
		var fieldID = 'hours_'+projIDArray[a]+'_'+taskIDArray[a]+'_'+fieldNo+'_'+a;
		var fieldValue = $(fieldID).value;
		fieldValue = parseFloat(fieldValue);
		if (isNaN(fieldValue)) { 
			fieldValue = 0; 
			$(fieldID).value = 0;
		}
		
		var dayTotal = Number(dayTotal+fieldValue);		
	}	
	
	// Update total field for this day
	$(dayTotalField).innerHTML = dayTotal;
	
	// Check for 0, NaN, or > 24 hours in any field
	var error = null;
	if (hours>24) {
		$(field).style.background = '#FEBCBC';
		error = 1;
	} else if (dayTotal>24) {
		// If we enter a number that causes our day total > 24 hours
		$(dayTotalField).style.background = '#FEBCBC';
		error = 1;
	} else if (isNaN(hours)) {
		$(field).value = '0';
		hours = '0';		
	}
	if (error != 1) {
		$(field).style.background = '#ffffff';
		$(dayTotalField).style.background = '#E9F4FF';
		x_updateHours(field,date,hours,returnUpdateHours);
	}	
		
}

function returnUpdateHours(string) {
	var hoursArray = string.split('|');
	var fieldIDString = hoursArray[1];
	
	var fieldArray = fieldIDString.split('_'); 
	
	var projID  = fieldArray[0];
	var taskID  = fieldArray[1];
	var fieldNo = fieldArray[2];
	var rowNo   = fieldArray[3];
	
	var dayTotalField  = 'dayTotal_'+fieldNo;
	var weekTotalField = 'total_'+rowNo;
	var dayTotal       = 0;
	var hours          = 0;
	
	// ***** RECALCULATE TOTAL ROW AND COLUMN ****************************
	// Total day column
	// We already calculated this in updateHours
			
	// Total week col : loop through each day and add up
	var weekTotal = 0;
	for(var a=0;a<=6;a++) {
		var fieldID1 = 'hours_'+projID+'_'+taskID+'_'+a+'_'+rowNo;
		var fieldValue1 = parseFloat($(fieldID1).value);
		
		var weekTotal = Number(weekTotal + fieldValue1);				
	}	
	$(weekTotalField).value = weekTotal;
		
	// Grand total cell : loop through each row total and add up
	var grandTotal = 0;
	var weekTotalRows = document.getElementsByClassName('totalColValue');
	for(var c = 0; c < weekTotalRows.length; c++) {
		var weekTotal = parseFloat(weekTotalRows[c].value);
		grandTotal = Number(grandTotal + weekTotal);
	}
	$('grandTotal').innerHTML = grandTotal;
}

function confirmDelete(projID,taskID,date) {
	var OK = confirm("Are you sure you want to delete this project from your timesheet?");
	if (OK == true) {
		x_deleteTimesheetProject(projID,taskID,date,returnGetTimesheet);
	}
}

function openCommentWindow(date,normalDate,fieldName) {
	var commentWindowID   = "commentWindow_"+fieldName;
	var currentCommentID  = "existingComments_"+fieldName;
	var currentComment    = $(currentCommentID).value;
	
	var content = '<div style="text-align: left;"><span id="commentTitle">Comments for '+normalDate+'</span>';
	content += '<textarea id="commentTextarea_'+date+'" style="width: 210px; height: 70px;">'+currentComment+'</textarea><br>';
	content += '<a href="#" onClick="saveComments(\''+date+'\',\''+fieldName+'\'); return false;"><img src="../images/button_save.gif" style="border: none;" alt="Enter Comments"></a></div>';
	openWindow(content,commentWindowID,220,0,1,'','','../');
}

function saveComments(date,fieldName) {
	var commentText = 'commentTextarea_'+date;
	var comments    = document.getElementById(commentText).value;
	x_saveComments(date,comments,fieldName,returnSaveComments);
}

function returnSaveComments(string) {
	var stringArray = string.split('|');
	var comments    = stringArray[0];
	var date        = stringArray[1];
	var fieldName   = stringArray[2];
	var sql         = stringArray[3];
	var currentCommentID  = "existingComments_"+fieldName;
	$(currentCommentID).value = comments;
	$('commentTitle').innerHTML = "Comment saved.";
}