<? include("js/jsCommon.php"); ?>

<script type="text/javascript">
<? sajax_show_javascript(); ?>

function displayHelpTopics(mainCatID) {
	x_getHelpCats(mainCatID,returnDisplayHelp);
}

function returnDisplayHelp(string) {
	document.getElementById('helpCats').innerHTML = string;
}

function displayHelpTopic(helpTopicID) {
	x_getTopic(helpTopicID,returnDisplayHelpTopic);
}

function returnDisplayHelpTopic(string) {
	var stringArray = string.split("|");
	
	var helpTopic = stringArray[0];
	var tutorial  = stringArray[1];
	
	if (tutorial == 0) {
		document.getElementById('helpTopic').innerHTML = helpTopic;
	} else {
		// Resize window to 610px X 410px
		window.resizeTo(608,470);
		
		// Redirect to tutorial screen
		 window.location="helpTutorial.php?tutFile="+helpTopic;
		
		//document.getElementById('helpContent').style.visibility = 'hidden';
		//document.getElementById('helpContent').style.height = '0px';
		//document.getElementById('tutContent').innerHTML = helpTopic;
		//document.getElementById('troubleShoot').innerHTML = helpTopic;
	}	
}

</script>