function returnDeleteBug(bID) {
	$('bugTR'+bID).style.display = 'none';
}

function closeBug(bID) {
	var statusIcon  = '<img src="../images/iconStatusComplete.gif" alt="Bug closed" />';
	$('status'+bID).innerHTML = statusIcon;	
	x_closeBug(bID,returnCloseBug);
}

function returnCloseBug (string) {
}

function sortFilter(sortColumn,acdc) {		
	// Check for records per page
	// First see if the records per page pull-down even exists
	if ( $('recordsPerPage1') ) {
		var rppID    = $('recordsPerPage1');
		var rppIndex = rppID.selectedIndex;
		var rppID    = rppID[rppIndex].value;
	} else {
		rppID =  0;
	}
	if ( $('pageNo1') ) {
		// Get page
		var pageID    = $('pageNo1');
		var pageIndex = pageID.selectedIndex;
		var pageID    = pageID[pageIndex].value;
	} else {
		pageID = 0;
	}	
	$('sortFilterBy').value = sortColumn;	
	$('sortFilterAcDc').value = acdc;	
	
	x_getBugs(sortColumn,acdc,rppID,'',pageID,'',returnRecordsPerPage)
}

// Handles records per page select menu
function recordsPerPage(newRecordsPerPage) {
	var sort = $('sortFilterBy').value;
	var acdc = $('sortFilterAcDc').value;
	var limit = newRecordsPerPage;
	var startPoint = '';
	var search = '';
	var newPage = '';
	x_getBugs(sort,acdc,limit,startPoint,newPage,search,returnRecordsPerPage);
}

function returnRecordsPerPage(stuff) {
	// Parse returned stuff
	var stringArray = stuff.split("~~|~~");
	var bugDisplay = stringArray[0];
	var pages = stringArray[1];
	var paginate1 = stringArray[2];
	var paginate2 = stringArray[3];
	var totalBugs = stringArray[4];
	
	$('bugDisplay').innerHTML = bugDisplay;
	$('paginate1').innerHTML = paginate1;
	$('paginate2').innerHTML = paginate2;
	$('totalBugs').innerHTML = totalBugs;
}

function gotoPage(newPage) {
	var sort = $('sortFilterBy').value;
	var acdc   = $('sortFilterAcDc').value;
	var limit = '';
	var startPoint = '';
	var search = '';
	x_getBugs(sort,acdc,limit,startPoint,newPage,search,returnRecordsPerPage);
}