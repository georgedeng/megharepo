function addWidget(wID) {
	x_addWidget(wID,addWidgetReturn);
}	
function removeWidget(wID) {
	x_removeWidget(wID,removeWidgetReturn);
}

function removeWidgetReturn(liID) {
	// I think we actually have to remove the li from the DOM
	// or else a new add will not function properly
	var widgetLI = document.getElementById(liID);
	//var parentUL = document.getElementById('list0'); 
	var parentUL = widgetLI.parentNode;
	parentUL.removeChild(widgetLI);
}

function addWidgetReturn(stuff) {
	// Parse stuff
	//alert(stuff);
	var stringArray = stuff.split("~~|~~");
	var wName    = stringArray[0];
	var wContent = stringArray[1];
	var wID      = stringArray[2];
	
	var newWidgetLI = document.createElement("li");
	var parentUL    = document.getElementById('list0'); 
	newWidgetLI.setAttribute("id", "li_"+wID);
	newWidgetLI.setAttribute("class", "widget2");
	newWidgetLI.innerHTML = wContent;
	parentUL.appendChild(newWidgetLI);
	
	Sortable.create("list0",
	{
		dropOnEmpty:true,containment:["list0","list1","list2"],constraint:false,
		onUpdate:function() {
			var list0 = Sortable.serialize('list0');
			var list1 = Sortable.serialize('list1');
			var list2 = Sortable.serialize('list2');
			moveWidget(list0,list1,list2);
		}
	}
	);     
	
	// Exception for jobTimer widget
	if (wID == 14) {
		checkJobTimer();
	}		
}
function moveWidget(list0,list1,list2) {
	x_moveWidget(list0,list1,list2,moveWidgetReturn);
}
function moveWidgetReturn(stuff) {
	// There's really nothing to do here.
}

//***** WIDGET MANIPULATION JAVASCRIPT ***********************************************/
function rollWidget(widget,upDown) {
	var widgetContentID = 'windowContent'+widget;
	var widgetRollID    = 'windowRoll'+widget;
		
	if (upDown == 'up') {
		var newHTML = '<a href="#" onClick="rollWidget(\''+widget+'\',\'down\'); return false;" title="Roll Down"><img src="images/buttonWindowDown.gif" style="border: none;"></a>';
		new Effect.SlideUp(widgetContentID, {direction: 'top', duration: .2});
		document.getElementById(widgetRollID).innerHTML = newHTML;
	} else if (upDown == 'down') {
		var newHTML = '<a href="#" onClick="rollWidget(\''+widget+'\',\'up\'); return false;" title="Roll Up"><img src="images/buttonWindowUp.gif" style="border: none;"></a>';
		new Effect.SlideDown(widgetContentID, {direction: 'top', duration: .2});
		document.getElementById(widgetRollID).innerHTML = newHTML;
	}
}

function showWidgets() {
	x_getAvailableWidgets(returnGetAvailableWidgets);
}

function returnGetAvailableWidgets(string) {
	openWindow(string,'wSelectContainer',200,0,0,'','','');
}

function showExportDashboard() {
	var menuString = $('addToDashboardMenu').innerHTML;
	openWindow(menuString,'addToDashboardContainer',200,0,0,'','','');
}
//***** SPECIFIC WIDGET JAVASCRIPT ***************************************************/
//***** RSS READER *******************************************************************/

function newRSS() {
	var content  = '<input type="text" id="newRSSLink" style="width: 250px;"><br /><a href="#" onClick="newRSSCommit(); return false;" title="Save RSS feed"><img src="images/button_save.gif" style="border: none;" alt="Save RSS feed"></a>&nbsp;'; 
	content += '<a href="#" onClick="cancelRSS(); return false;" title="Cancel"><img src="images/buttonCancel.gif" style="border: none;" alt="Cancel" /></a>';
	$('rssView').innerHTML = content;	
	$('newRSSLink').focus();
}

function cancelRSS() {
	document.getElementById('rssView').innerHTML = '<a href="#" onClick="newRSS(); return false;" title="Add RSS feed"><img src="images/buttonAddFeed.gif" alt="Add RSS Feed" style="border: none;"></a>';
}

function newRSSCommit() {
	var rssFeed = $('newRSSLink').value;
	$('rssView').innerHTML = '<img src="images/animAJAX_tiny_red.gif"> Getting feed...please wait.'; 
	x_newRSS(rssFeed,returnRSSAdd); 	
}
function getRSSFeeds() {
	$('rssContent').innerHTML = '<div class="ajaxAnimDivBig" style="margin-left: auto; margin-right: auto; margin-top: 20px;">Loading Feeds...</div>';
	x_getRSSFeeds(1,returnRSS);
}
function returnRSS(string) {
	$('rssContent').innerHTML = string;
	$('rssView').innerHTML = '<a href="#" onClick="newRSS(); return false;" title="Add RSS feed"><img src="images/buttonAddFeed.gif" alt="Add RSS Feed" style="border: none;"></a>';
}
function returnRSSDelete(rID) {
	var rssContainer = 'rssContainer'+rID;
	$(rssContainer).style.display = 'none';
}
function returnRSSAdd(string) {
	var rssContent = $('rssContent');
	var newdiv = document.createElement('div');
	newdiv.innerHTML = string;
	rssContent.appendChild(newdiv);
	cancelRSS();
}

//***** NOTES ************************************************************************/

function newNote(note) {
	content  = '<textarea id="newNoteText" style="width: 300px; height: 50px;"></textarea><br />';
	content += '<a href="#" onClick="newNoteCommit(); return false;" title="Save note"><img src="http://www.creativeprooffice.com/apps/images/button_save.gif" style="border: none;" alt="Save note"></a>';
	content += '<a href="#" onClick="newNoteCancel(); return false;" title="Cancel"><img src="http://www.creativeprooffice.com/apps/images/buttonCancel.gif" style="border: none;" alt="Cancel"></a>';
	 
	document.getElementById('noteView').innerHTML = content;
	document.getElementById('newNoteText').focus();
}
function newNoteCancel() {
	document.getElementById('noteView').innerHTML = '<a href="#" onClick="newNote(); return false;" title="Add note"><img src="images/buttonAddNote.gif" alt="Add Note" style="border: none;"></a>';
}
function newNoteCommit() {
	var noteString = removeChar($('newNoteText').value);
	x_newNote(noteString,newNoteReturn); 	
}
function newNoteReturn(string) {
	var noteContentDiv = document.getElementById('noteContent');
	noteContentDiv.innerHTML = string;
		
	$('noteView').innerHTML = '<a href="#" onClick="newNote(); return false;" title="Add note"><img src="http://www.creativeprooffice.com/apps/images/buttonAddNote.gif" alt="Add Note" style="border: none;"></a>';
	
}	
function noteDelete(nID) {
	x_noteDelete(nID,returnNoteDelete);
	// Start AJAX animation
	ajaxAnimSmall('ajaxNotes',1,1);
}
function returnNoteDelete(string) {
	document.getElementById('noteContent').innerHTML = string;
	// Stop AJAX animation
	ajaxAnimSmall('ajaxNotes',1,0);
}

//***** DELICIOUS *******************************************************************/

function deliciousLogin() {	
	var dUserName = document.getElementById('dUserName').value;
	var dPassword = document.getElementById('dPassword').value;
	if (dUserName == '') {
		document.getElementById('dMessage').innerHTML = 'Please enter your User Name.';
	} else if (dPassword == '') {
		document.getElementById('dMessage').innerHTML = 'Please enter your Password.';
	} else {
		x_deliciousLogin(dUserName,dPassword,1,deliciousReturn); 
	}	
	// Start AJAX animation
	ajaxAnimSmall('ajaxDelicious',1,1);
}

function deliciousReturn(string) {
	var stringArray = string.split("~~|~~");
	var links = stringArray[0];
	var error = stringArray[1];
	content = '<div id="ajaxDelicious"></div>'+links;
	// Stop AJAX animation
	ajaxAnimSmall('ajaxDelicious',1,0);
	if (error == 2) {		
		$('windowContentDelicious').innerHTML = content;
	} else {
		$('windowContentDelicious').innerHTML = content;
		$('delLoginForm').style.display = 'none;';
	}		
}

function deliciousPost() {
	var content = '<input type="text" id="dURL" style="width: 250px;" value="URL" onFocus="this.value=\'\'"><br />';
	content +=    '<input type="text" id="dDesc" style="width: 250px;" value="Description" onFocus="this.value=\'\'"><br />';
	content +=    '<input type="text" id="dTags" style="width: 250px;" value="Tags" onFocus="this.value=\'\'"><br />';
	content +=    '<a href="#" onClick="deliciousPostCommit(); return false;" title="Post"><img src="http://www.creativeprooffice.com/apps/images/buttonDeliciousPost.gif" style="border: none;" alt="Post"></a>';
	content +=    '<a href="#" onClick="deliciousPostCancel(); return false;" title="Cancel"><img src="http://www.creativeprooffice.com/apps/images/buttonCancel.gif" style="border: none;" alt="Cancel"></a>';
	document.getElementById('dPost').innerHTML = content;	
}

function deliciousPostCancel() {
	var content = '<a href="#" onClick="deliciousPost(); return false;" title="Post"><img src="http://www.creativeprooffice.com/apps/images/buttonDeliciousPost.gif" style="border: none;" alt="Post"></a>';
	
	document.getElementById('dPost').innerHTML = content;
}

function deliciousPostCommit() {
	var dURL  = removeChar(document.getElementById('dURL').value);
	var dDesc = removeChar(document.getElementById('dDesc').value);
	var dTags = removeChar(document.getElementById('dTags').value);
	x_deliciousPostCommit(dURL,dDesc,dTags,deliciousReturn);
	// Start AJAX animation
	ajaxAnimSmall('ajaxDelicious',1,1);
}

function deliciousDelete(url) {
	x_deliciousDelete(url,deliciousReturn);
	// Start AJAX animation
	ajaxAnimSmall('ajaxDelicious',1,1);
}

function getWidgetTasks(pID,j) {
	// If taskDiv+j is displayed, we're just turning it off
	var taskDiv = 'taskDiv'+j;
	var projDiv = 'projDiv'+j;
	if ($(taskDiv).style.display == '') {
		$(taskDiv).style.display = 'none';
		var origClassName = $('taskRowClass'+j).value;
		$(projDiv).className = origClassName;
	} else {
		// Make sure all highlited tasks are turned off
		var totalTasks = $('totalTasks').value;
		for(var a=0;a<=totalTasks;a++) {
			var projDiv = 'projDiv'+a;
			if ($(projDiv).className == 'selTaskRow') {
				// Get original className
				var origClassName = $('taskRowClass'+a).value;
				$(projDiv).className = origClassName;
				// Turn off taskDiv
				$('taskDiv'+a).style.display = 'none';
			}
		}
		x_getWidgetTasks(pID,j,returnGetTasks);	
	}	
}

function returnGetTasks(string) {
	var stringArray = string.split('|');
	var taskString = stringArray[0];
	var pID        = stringArray[1];
	var rowNo      = stringArray[2];
	var taskRow  = $('taskDiv'+rowNo);
	var projRow  = $('projDiv'+rowNo);
	
	projRow.className = 'selTaskRow';	
	taskRow.innerHTML = taskString; 
	new Effect.SlideDown(taskRow, {direction: 'top', duration: .2});
	//taskRow.style.display = '';	
}

function getNotes(start) {
	x_getNotes(start,returnGetNotes);
}

function returnGetNotes(string) {
	$('noteContent').innerHTML = string;
}