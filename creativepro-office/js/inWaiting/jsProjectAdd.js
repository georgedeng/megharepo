function addContact(projID) {
	var contactName  = document.getElementById('contactName').value;
	var contactEmail = document.getElementById('contactEmail').value;
	var contactPhone = document.getElementById('contactPhone').value;
	x_addContact(contactName,contactEmail,contactPhone,projID,returnAddContact);	 
}

function deleteContact(contactID,projID) {
	if (projID == '') { projID = 0; }
	x_deleteContact(contactID,projID,returnAddContact);
}

function returnAddContact(string) {
	document.getElementById('contacts').innerHTML = string;
}