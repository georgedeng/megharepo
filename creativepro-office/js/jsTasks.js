var taskFormHTML = '';
var milestoneFormHTML = '';
var taskEditor;
var taskEditMembers;

var getTaskDataObj = {
	getTasks : function(projectID) {
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'tasks/TaskView/getTasksForProject/'+projectID+'/json',
            dataType: 'json',
		    success: function(payload){
				if (payload != 0) {
					var jsonObject = payload;
					taskArray = jsonObject.MilestonesTasks;
                    milestoneArray = jsonObject;
					renderTaskDataObj.renderTaskGrid(milestoneArray);
                    
                    if (projectTaskTotal>20) {
                        $('.taskListContainer').hide();
                    }
                    
				} else {
                    $('#taskInformationContainer').empty().systemMessage({
                        status: 'information',
                        noFade:  1,
                        size:    'Big',
                        message: commonTerms['noTasks']
                    });
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
    getTasksFromSearch : function(searchTerm) {
        var hash = {q : searchTerm}
        var projectID = $('#projectID').val();

        $.ajax({
		    type:     "POST",
		    url:      siteURL+'tasks/TaskView/searchTasks/0/'+projectID,
            data:     hash,
            dataType: 'json',
		    success: function(payload){
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getTask : function(taskID) {
        alert(taskID);
    },
	getTaskStats : function(projectID) {
        var totalTasks       = $('.taskRow:not(.taskOff)').size();
        var completedTasks   = $('.status_completed:not(.taskOff)').size();
        var inProgressTasks  = $('.status_in_progress:not(.taskOff)').size();
        var notStartedTasks  = $('.status_not_started:not(.taskOff)').size();
        var overdueTasks     = $('.status_past_due.statusTask:not(.taskOff)').size();

		var incompleteTasks = parseInt(totalTasks-completedTasks);
		
        var totalEstimatedHours = 0,hours,totalActualHours = 0,actualHours;
        $('.hoursEstimated').each(function() {
           hours = parseFloat($(this).html());
           totalEstimatedHours = parseFloat(totalEstimatedHours+hours);
        });

        $('.hoursActual').each(function() {
           actualHours = parseFloat($(this).html());
           totalActualHours = parseFloat(totalActualHours+actualHours);
        });

        /*
         * Format hours string
         */
        var hoursString = '';
        if (totalEstimatedHours>0) {
            if (totalActualHours>totalEstimatedHours) {
                hoursString = '<strong class="redText"><span class="redText" id="totalActualHours">'+totalActualHours+'</span>/'+totalEstimatedHours+' '+commonTerms['hrs']+'<strong>';
            } else {
                hoursString = '<strong><span id="totalActualHours">'+totalActualHours+'</span> / <span>'+totalEstimatedHours+'</span> '+commonTerms['hrs']+'</strong>';
            }
        } else {
            hoursString = '<strong><span id="totalActualHours">'+totalActualHours+'</span> <span>'+commonTerms['hrs']+'</span></strong>';
        }

		var stringOut = [];
		stringOut.push('<p class="colorBackground light-green"><span class="bigText greenText">'+completedTasks+'</span> <span class="subText black">'+commonTerms['complete']+'</span></p>');
		stringOut.push('<p class="colorBackground light-gray" style="margin: 3px 0 3px 0;"><span class="bigText">'+notStartedTasks+'</span> <span class="subText black">'+commonTerms['notStarted']+'</span></p>');
		stringOut.push('<p class="colorBackground light-blue" style="margin: 3px 0 3px 0;"><span class="bigText">'+inProgressTasks+'</span> <span class="subText black">'+commonTerms['inProgress']+'</span></p>');
		stringOut.push('<p class="colorBackground light-red" style="margin: 3px 0 12px 0;"><span class="bigText redText">'+overdueTasks+'</span> <span class="subText black">'+commonTerms['overdue']+'</span></p>');
		stringOut.push('<p style="margin-left: 4px;"><span class="bigText">'+totalTasks+' '+commonTerms['total']+'<br />'+hoursString+'</span><br /></p>');
		
		var stringOutString = stringOut.join('');
		$('#taskStats').html(stringOutString);
        $('#totalActualHours').formatCurrency({symbol: '',useHtml: true});

		var pieValues = [completedTasks,notStartedTasks,inProgressTasks,overdueTasks];
		var pieColors = ['#B2DD32','#DEDDDD','#AFD8F8','#F984A1'];
		$('#taskStatsPie').sparkline(
			pieValues,
			{ 
				type: 'pie',
				sliceColors: pieColors,
				width: '90px',
				height: '90px'
			}
		);
	}
}

var renderTaskDataObj = {
	renderTaskGrid : function(jsonObject) {
        /*
         * Remove any existing elements and event handlers
         */
        $('#taskInformationContainer').empty();        
        
		var milestones      = jsonObject.MilestonesTasks;
        var totalMilestones = milestones.length;
		var headerArray     = jsonObject.HeaderLabels.split('|');
		var stringOut       = [];
						
		for(var a=0;a<=(totalMilestones)-1;a++) {
            milestones[a].Index = a;
            var milestoneID = milestones[a].MilestoneID;
            
            /*
			 * Send the milestone header and taskContainer to the DOM
			 */
            var milestoneString = renderTaskDataObj.renderMilestoneRow(milestones[a],true);
			$('#taskInformationContainer').append(milestoneString);
            $('#milestoneRow'+milestoneID).data('itemIndex',a);
            $('#milestoneRow'+milestoneID).data('milestoneID',milestoneID);
			
			/*
			 * Now print tasks
			 */
			var taskStringOut = [];
			taskStringOut.length = 0;
			var tasks       = milestones[a].Tasks;
			var totalTasks  = tasks.length;

			if (totalTasks>0) {
                for(var b=0;b<=(totalTasks)-1;b++) {
                    tasks[b].ItemIndex      = b;
                    tasks[b].MilestoneID    = milestoneID;
                    taskStringOut.push(renderTaskDataObj.renderTaskRow(tasks[b],true));
				}

				var taskString = taskStringOut.join('');
				$('#taskListContainer'+milestoneID).html(taskString);
			}
		}		

		getTaskDataObj.getTaskStats($('#projectID').val());
        renderTaskDataObj.attachTaskPlugins();
        renderTaskDataObj.toggleCheckState();
        permissionsObj.renderControlsBasedOnPermissions();

        attachedFilesView = true;
        fileManagerObj.attachFileWindowEventHandlers('.fileListContainer');
        fileManagerObj.attachFilePlugins();
	},
    renderTaskRow : function(taskObject,wrapper) {
          if (typeof(taskObject) != 'object') {
               var taskObject = eval('('+ taskObject + ')');
		  }

          var taskStringOut = [];
          var taskID            = taskObject.TaskID;
          var milestoneID       = taskObject.MilestoneID;
          var taskTitle         = taskObject.Title;
          var taskDescription   = taskObject.Description;
          var taskDateStart     = taskObject.Date_Start;
          var taskDateEnd       = taskObject.Date_End;
          var taskDateCompleted = taskObject.Date_Completed;
          var creatorName       = taskObject.CreatorName;
          var creatorPID        = taskObject.CreatorPID;
          var teamMembers       = taskObject.TeamMembers;
          var totalTeamMembers  = teamMembers.length;
          var taskStatus        = taskObject.Status;
          var taskStatusHuman   = taskObject.StatusHuman;
          var taskStatusMachine = taskObject.StatusMachine;
          var taskStatusText    = taskObject.StatusText;
		  var dateString        = taskObject.DateString;
          var taskPriorityClass = taskObject.PriorityClass;
          var taskPriorityText  = taskObject.PriorityText;
          var itemIndex         = taskObject.ItemIndex;
          var hoursEstimated    = parseFloat(taskObject.HoursEstimated);
          var hoursActual       = parseFloat(taskObject.HoursActual);
          var taskFiles         = taskObject.Files;
          var taskTags          = taskObject.Tags;
          var taskNumber        = taskObject.TaskNo;
          var numberOfMessages  = taskObject.Messages;
          var numberOfFiles     = taskFiles.length;
          var unreadMessages    = taskObject.UnreadMessages;
          var projectID         = $('#projectID').val();

		  if (dateString != '') {
			dateString += ' | ';
		  }

		  /*
		   * Is this task in the jobTimer cookie?
		   */
		  var cookieTimers = widgetToolsObj.objectifyCookie('jobTimers');
		  var timerState = 'stop';
		  var timerTitle = commonTerms['timer_start'];
		  var timerClass = 'timerStop';
		  if (cookieTimers != false) {
			for(var b=0;b<=(cookieTimers.length-1);b++) {
				var timerTaskID  = cookieTimers[b].TaskID;
				var timerRunning = cookieTimers[b].TimerRunning;
				var elapsedTime  = cookieTimers[b].ElapsedTime;

				if (timerTaskID == taskID && timerRunning == 1) {
					/*
					 * This task is logging time
					 */
					hoursActual = widgetToolsObj.widgetTimerElapsedTimeDecimal(elapsedTime);

					timerState = 'start';
					timerTitle = commonTerms['timer_stop'];
					timerClass = 'timerGo';
				} else {
					timerState = 'stop';
					timerTitle = commonTerms['timer_start'];
					timerClass = 'timerStop';
				}
			}
		  }

          /*
           * Format hours string
           */
          var hoursString = '';
          if (hoursEstimated>0) {
              if (hoursActual>hoursEstimated) {
                hoursString = '<strong class="redText"><span id="taskHours_'+taskID+'" class="hoursActual redText">'+hoursActual+'</span> / <span class="hoursEstimated redText">'+hoursEstimated+'</span> '+commonTerms['hrs']+'</strong>';
              } else {
                hoursString = '<strong><span class="hoursActual" id="taskHours_'+taskID+'">'+hoursActual+'</span> / <span class="hoursEstimated">'+hoursEstimated+'</span> '+commonTerms['hrs']+'</strong>';
              }
          } else {
              hoursString = '<strong><span class="hoursActual" id="taskHours_'+taskID+'">'+hoursActual+' '+commonTerms['hrs']+'</span></strong>';
          }
		  
		  var myTask = 'notMyTask';
          if (totalTeamMembers>0) {
               var teamList = [], teamUIDString = '', teamListHidden = [];
               for(var c=0;c<=(totalTeamMembers)-1;c++) {
                   if (teamMembers[c].NameFirst != null && teamMembers[c].NameLast != null) {
                        var teamMemberName     = teamMembers[c].NameFirst+' '+teamMembers[c].NameLast;
                        var teamMemberID       = teamMembers[c].UID;
                        var teamMemberPID      = teamMembers[c].PID;
                        var teamMemberLanguage = teamMembers[c].Language;
                        teamUIDString += teamMemberID+','+teamMemberLanguage+'|';
                        teamList.push('<p><a href="#" class="personLink assignedToTask" personID="'+teamMemberPID+'" id="'+teamMemberID+'" taskID="'+taskID+'">'+teamMemberName+'</a></p>');

                        teamListHidden.push('<input type="hidden" class="recipData" recipLanguage="'+teamMemberLanguage+'" recipType="team" value="'+teamMemberPID+'" />');
                        if (teamMemberID == numbers[0]) {
							myTask = 'myTask';
						}
                   } else {
                       teamList.push('&nbsp;');
                   }
               }
               var teamListFinal = teamList.join('');
               
          } else {
               var teamListFinal = '';
          }		  

            if (wrapper == true) {
                taskStringOut.push('<div class="row itemRow taskRow '+myTask+'" style="background-color: #fff;" id="taskRow'+taskID+'" itemIndex="'+itemIndex+'" itemID="task_'+taskID+'">');
                taskStringOut.push('<div class="controls noPrint" id="taskControls'+taskID+'" itemID="'+taskID+'" style="width: 50px; float: left;">&nbsp;</div>');
                taskStringOut.push('<div id="taskContainer'+taskID+'">');
            }
            taskStringOut.push('<input type="hidden" id="taskMilestoneID'+taskID+'" value="'+milestoneID+'" />');
            taskStringOut.push('<div style="padding-left: 6px; width: 45%; float: left;"><strong id="taskTitle'+taskID+'">[ '+taskNumber+' ] <a href="#" class="taskTitleLink" itemID="'+taskID+'">'+taskTitle+'</a></strong>');

            if (numberOfMessages>0) {
                taskStringOut.push('<span style="margin: 0 0 0 6px;" class="clickable taskTitleLink icon_comment_small_left" title="'+numberOfMessages+'" itemID="'+taskID+'"></span>');
            }
                taskStringOut.push('<br /><span class="subText">'+dateString+' '+commonTerms['created_by']+' <a href="#" class="personLink" personID="'+creatorPID+'" id="'+creatorPID+'">'+creatorName+'</a></span>');
                taskStringOut.push('</div>');
                taskStringOut.push('<div style="width: 18%; float: left;">'+teamListFinal+'</div>');
                taskStringOut.push('<div style="width: 5%; float: left;" id="taskStatusContainer'+taskID+'"><span class="'+taskStatusMachine+' statusTask clickable" itemID="'+taskID+'" title="'+taskStatusHuman+' '+taskStatusText+'" statusDesc="'+taskStatusHuman+'"></span></div>');
                taskStringOut.push('<div style="width: 5%; float: left;"><span class="icon_priority '+taskPriorityClass+'" title="'+commonTerms['priority']+': '+taskPriorityText+'">&nbsp;</span></div>');
                taskStringOut.push('<div style="width: 13%; float: left;">'+hoursString+'</div>');
                taskStringOut.push('<div style="width: 4%; float: left;">');
                taskStringOut.push('<a href="#" class="smallButton projectTimer" state="'+timerState+'" id="taskClock'+taskID+'" itemID="'+taskID+'" title="'+timerTitle+'"><span class="'+timerClass+'"></span></a>');
                taskStringOut.push('</div>');

                taskStringOut.push('<div style="clear: left;"></div>');
                taskStringOut.push('<div style="margin: 6px 0 0 56px; display: none;" class="taskDetailsContainer doPrint" id="taskDetailsContainer'+taskID+'">');
                    taskStringOut.push('<div><p>'+taskDescription+'</p>');
                    if (taskTags != '') {
                        taskStringOut.push('<p class="icon_tag" style="margin-top: 6px;">'+taskTags+'</p>');
                    }
                    taskStringOut.push('</div>');
                    
                    /*
                     * TASK FILES
                     */
                    taskStringOut.push('<div class="icon_file_small colorBackground light-blue bigText clickable taskFileLink mBottom12 mTop12" style="background-position: 4px 4px; width: 625px;" data-itemid="'+taskID+'">Files <span id="numberOfFilesText'+taskID+'">('+numberOfFiles+')</span></div>');
                    
                    taskStringOut.push('<div class="hide" id="taskFilesSection'+taskID+'">');
                    taskStringOut.push('<div id="fileUploadContainer'+taskID+'" class="dragDropUploadContainer">');
                    taskStringOut.push('<div class="fileUploadBox" id="fileUploadBox'+taskID+'">'+commonTerms['drop_files_here']+'</div>');
                    taskStringOut.push('</div>');
                    taskStringOut.push('<div style="clear: both;"></div>');
                    
                    taskStringOut.push('<div style="margin-bottom: 12px;" class="fileListContainer" id="fileListContainer_'+taskID+'">');
                    if (taskFiles.length>0) {
                        taskStringOut.push('<ul class="fileList">');
                        taskStringOut.push(renderFileObj.renderFileWindow(taskObject,'fileListContainer_'+taskID,true,true));
                        taskStringOut.push('</ul>');
                    }
                    taskStringOut.push('</div><div style="clear: both;"></div>');
                    taskStringOut.push('</div>');

                    /*
                     * TASK MESSAGES
                     */
                    taskStringOut.push('<div class="icon_comment_small_left colorBackground light-blue bigText clickable taskMessageLink mBottom12" style="background-position: 4px 4px; width: 625px;" data-itemid="'+taskID+'">Messages <span id="numberOfMessagesText'+taskID+'">('+numberOfMessages+')</span></div>');
                    
                    taskStringOut.push('<div class="hide" id="taskMessageSection'+taskID+'">');
                    taskStringOut.push('<button class="smallButton buttonOpenMessageContainer" id="messageBoxLink_'+taskID+'"><span class="messages">'+commonTerms['send_message']+'</span></button>');
                    taskStringOut.push('<div id="newMessageTask_'+taskID+'" itemID="'+taskID+'" itemType="task" class="hide messageBoxContainerTask">');
                        taskStringOut.push('<p><textarea class="messageBox normal expanding" tabindex="350"></textarea></p>');
                        taskStringOut.push('<div style="margin: 3px 0 0 4px;">');
                            taskStringOut.push('<button class="smallButton submitTaskMessage" style="float: left; margin-right: 12px;"><span class="sendMessage">'+commonTerms['send']+'</span></button>');
                            taskStringOut.push('<button class="smallButton cancelTaskMessage" style="float: left; margin-right: 12px;"><span class="cancel">'+commonTerms['close']+'</span></button>');
                        taskStringOut.push('</div><div style="clear: both;"></div>');
                        taskStringOut.push(teamListHidden.join(''));
					taskStringOut.push('</div>');
                    taskStringOut.push('<div id="messageListTask'+taskID+'" class="messageListContainerTask" style="margin-top: 6px;"></div>');
                    taskStringOut.push('<input type="hidden" id="numberOfMessages'+taskID+'" value="'+numberOfMessages+'" />')
                    taskStringOut.push('</div>');
                    
				taskStringOut.push('</div>');
          
          if (wrapper == true) {
            taskStringOut.push('</div>');
            taskStringOut.push('<div class="taskFormContainer boxGray" style="margin: -7px 0 0 -3px;" id="taskFormContainer_'+taskID+'"></div>');
            taskStringOut.push('</div>');
          }

          return taskStringOut.join('');
     },
     sortTasks : function(sortBy) {
        var acdc     = $('#taskAcdc').val();
		var origSort = $('#taskOrigSort').val();

		if (origSort == sortBy) {
			/*
			 * Then switch acdc
			 */
			if (acdc == 'asc') {
				var newAcDc = 'desc';
			} else {
				var newAcDc = 'asc';
			}
		} else {
			var newAcDc = 'asc';
		}
        var taskItemArray;
        switch (sortBy) {
			case 'title':
                for(var a in milestoneArray.MilestonesTasks) {
                    taskItemArray = milestoneArray.MilestonesTasks[a].Tasks;
                    taskItemArray.sort(sortObj.sortByTitleAsc);                    
                    milestoneArray.MilestonesTasks[a].Tasks = taskItemArray;
                }
                $('#taskInformationContainer').html('');
                renderTaskDataObj.renderTaskGrid(milestoneArray);
				break;
			case 'taskno':
				for(var a in milestoneArray.MilestonesTasks) {
                    taskItemArray = milestoneArray.MilestonesTasks[a].Tasks;
                    taskItemArray.sort(sortObj.sortByTaskNumberAsc);
                    milestoneArray.MilestonesTasks[a].Tasks = taskItemArray;
                }
                $('#taskInformationContainer').html('');
                renderTaskDataObj.renderTaskGrid(milestoneArray);                
				break
            case 'hours':
				for(var a in milestoneArray.MilestonesTasks) {
                    taskItemArray = milestoneArray.MilestonesTasks[a].Tasks;
                    taskItemArray.sort(sortObj.sortByTaskHoursAsc);
                    milestoneArray.MilestonesTasks[a].Tasks = taskItemArray;
                }
                $('#taskInformationContainer').html('');
                renderTaskDataObj.renderTaskGrid(milestoneArray);
				break
            case 'status':
				for(var a in milestoneArray.MilestonesTasks) {
                    taskItemArray = milestoneArray.MilestonesTasks[a].Tasks;
                    taskItemArray.sort(sortObj.sortByStatusAsc);
                    milestoneArray.MilestonesTasks[a].Tasks = taskItemArray;
                }
                $('#taskInformationContainer').html('');
                renderTaskDataObj.renderTaskGrid(milestoneArray);
				break
            case 'priority':
				for(var a in milestoneArray.MilestonesTasks) {
                    taskItemArray = milestoneArray.MilestonesTasks[a].Tasks;
                    taskItemArray.sort(sortObj.sortByPriorityAsc);
                    milestoneArray.MilestonesTasks[a].Tasks = taskItemArray;
                }
                $('#taskInformationContainer').html('');
                renderTaskDataObj.renderTaskGrid(milestoneArray);
				break
            case 'sortorder':
				for(var a in milestoneArray.MilestonesTasks) {
                    taskItemArray = milestoneArray.MilestonesTasks[a].Tasks;
                    taskItemArray.sort(sortObj.sortBySortOrderAsc);
                    milestoneArray.MilestonesTasks[a].Tasks = taskItemArray;
                }
                $('#taskInformationContainer').html('');
                renderTaskDataObj.renderTaskGrid(milestoneArray);
				break
			default:
				clientArray.sort(sortObj.sortByCompanyAsc);            
		}
     },
	 renderMilestoneRow : function(milestoneObject,wrapper) {
		if (typeof(milestoneObject) != 'object') {
               var milestoneObject = eval('('+ milestoneObject + ')');
		}
        var milestoneIndex     = milestoneObject.Index;
		var milestoneID        = milestoneObject.MilestoneID;
        var milestoneTitle     = milestoneObject.Title;
		var milestoneDateStart = milestoneObject.Date_Start;
		var milestoneDateEnd   = milestoneObject.Date_End;
		var creatorName        = milestoneObject.NameFirst+' '+milestoneObject.NameLast;
        var creatorPID         = milestoneObject.PID;
		var dateString         = milestoneObject.DateString;
		if (dateString != '') {
			dateString += ' | ';
		}

		var totalTasks   = milestoneObject.TaskCount;
		var stringOut    = [];
		stringOut.length = 0;

        if (totalTasks<1) {
			var taskCount = '0';
		} else {
			taskCount = totalTasks;
        }

        var itemRowClass = '';
        if (milestoneID == 0) {
            itemRowClass = 'miscMilestone';
        }

        if (wrapper == true) {
            stringOut.push('<div class="barBlue bottom itemRow '+itemRowClass+' milestoneRow clickable" id="milestoneRow'+milestoneID+'"  milestoneID="'+milestoneID+'" itemID="milestone_'+milestoneID+'">');
            stringOut.push('<div class="controls noPrint" id="milestoneControls'+milestoneID+'" itemID="'+milestoneID+'" style="width: 50px; float: left;">&nbsp;</div>');
            stringOut.push('<div id="milestoneContainer'+milestoneID+'">');
        }

            stringOut.push('<div style="float: left; width: 520px;">');
                stringOut.push('<h2><span id="milestoneTitle_'+milestoneID+'">'+milestoneTitle+'</span> <span class="lightBlue">( <span class="lightBlue" id="taskCount'+milestoneID+'">'+taskCount+'</span> )</span></h2>');
                if (milestoneID>0) {
                    stringOut.push('<span class="subText">'+dateString+' '+commonTerms['created_by']+' <a href="#" class="personLink" personID="'+creatorPID+'" id="'+creatorPID+'">'+creatorName+'</a></span>');
                }
            stringOut.push('</div>');
            stringOut.push('<div style="float: right; padding: 6px 6px 0 0;">');
                stringOut.push('<button class="buttonExpand action buttonNewTask taskCreate hide" milestoneID="'+milestoneID+'"><span class="add">'+commonTerms['new_task']+'</span></button>');
            stringOut.push('</div><div style="clear: both;"></div>');
            
        if (wrapper == true) {
            stringOut.push('</div>');
            stringOut.push('<div id="milestoneFormContainer_'+milestoneID+'" class="milestoneFormContainer" style="display: none;"></div>');
            stringOut.push('</div>');            
            stringOut.push('<div class="taskFormContainer boxGray" id="taskFormContainer_'+milestoneID+'"></div>');
            stringOut.push('<div class="taskListContainer" id="taskListContainer'+milestoneID+'" style="min-height: 15px;"></div>');
        }
		return stringOut.join('');
	 },
     attachTaskPlugins : function() {
        //$('#taskInformationContainer').hiliteContainerRow();

        $('#taskInformationContainer').hoverButtons({
            elementClass: 'itemRow:not(.miscMilestone)',
            insertionElement: 'controls',
            editClass: 'taskUpdate',
            deleteClass: 'taskDelete',
            position: ''
        });

        $('.taskListContainer').sortable({
            connectWith: '.taskListContainer',
            axis: 'y',
            cursor: 'move',
            delay: 100,
            placeholder: 'ui-sortable-placeholder',
            containment: '#contentArea',
            start: function(event, ui) {
                ui.item.css('opacity','0.7');
            },
            stop: function(event, ui) {
                /*
                 * Create JSON string of current task order
                 */
                var itemArray = [];
                $('.itemRow').each(function() {
                    var itemID = $(this).attr('itemID');
                    itemArray.push(itemID);
                });
                var jsonString = JSON.stringify(itemArray);
                var hash = {
                    orderJSON: jsonString
                }
                $.ajax({
                    type:     'POST',
                    url:      siteURL+'tasks/TaskUpdate/reorderTasks',
                    data:     hash,
                    success: function(payload) {
                    },
                    error: function (xhr, desc, exceptionobj) {
                        errorObj.ajaxError(xhr,desc,exceptionobj);
                    }
                });
                ui.item.css('opacity','1');
                var row = $(ui.item[0]);
                row.effect('highlight', {}, 1000);
                supportTasksObj.calculateTaskCountForAllMiletones();                
            }
        });
        /*
         * Removed disableSelection() here because it was causing FF 20 to disable
         * form fields when editing.
         */
     },
     attachTaskContainerEventHandlers : function() {
        $('#taskInformationContainer').delegate('.buttonNewTask', 'click', function(e) {
            e.stopPropagation();
            var milestoneID = $(this).attr('milestoneID');
            $('.taskFormContainer').html('');
            initTaskObj.renderTaskForm(milestoneID);
            return false;
        });

        $('#taskInformationContainer').delegate('.milestoneRow', 'click', function() {
            var milestoneID = $(this).data('milestoneID');
            if ($('#taskListContainer'+milestoneID).is(':visible')) {
                $('#taskListContainer'+milestoneID).slideUp('fast');
            } else {
                $('#taskListContainer'+milestoneID).slideDown('fast');
            }

            return false;
        });

        $('#taskInformationContainer').delegate('.taskTitleLink', 'click', function() {
            var taskID = $(this).attr('itemID');
            if ($('#taskDetailsContainer'+taskID).is(':visible')) {
                $('#taskDetailsContainer'+taskID).slideUp('fast');
            } else {
                $('#taskDetailsContainer'+taskID).slideDown('fast');
                var numberOfMessages = $('#numberOfMessages'+taskID).val();
                if (numberOfMessages>0 && $(this).data('messagesRendered') != true) {
                    var messageContainerObj = $('#messageListTask'+taskID);
                    getMessageObj.getMessages(taskID,'task',messageContainerObj);
                    $(this).data('messagesRendered',true);
                }
                
                if (!$.browser.msie) {
                    filesObj.initPluploadDragNDrop(taskID,'task',$('#fileListContainer_'+taskID),'fileUploadBox'+taskID,'fileUploadContainer'+taskID);
                }
            }

            return false;
        });
        
        $('#taskInformationContainer').delegate('.taskFileLink', 'click', function() {
            var taskID = $(this).data('itemid');
            $('#taskFilesSection'+taskID).toggle('fast');
            
        });  
        
        $('#taskInformationContainer').delegate('.taskMessageLink', 'click', function() {
            var taskID = $(this).data('itemid');
            $('#taskMessageSection'+taskID).toggle('fast');
            
        });  

        $('#taskInformationContainer').delegate('.buttonEdit', 'click', function(e) {
            e.stopPropagation();
            var itemID = $(this).attr('itemID');
            var itemArray = itemID.split('_');
            var editItem = itemArray[0];
            var editID   = itemArray[1];

            if (editItem == 'milestone') {
               initTaskObj.renderMilestoneForm(editID,'edit');
            } else if (editItem == 'task') {
               initTaskObj.renderTaskForm(editID,'edit');
            }

            return false;
        });

        $('#taskInformationContainer').delegate('.buttonDelete', 'click', function(e) {
            var itemID = $(this).attr('itemID');
            var itemArray = itemID.split('_');
            var itemType = itemArray[0];
            itemID   = itemArray[1];
            var milestoneIDString, milestoneID;
            if (itemType == 'task') {
                var milestoneID = $('#taskMilestoneID'+itemID).val();
                $('#taskRow'+itemID).fadeOut('slow', function() {
                    $('#taskRow'+itemID).remove();
                    supportTasksObj.incrementTaskCount(milestoneID,'down');
                });
                updateTrashObj.sendToTrash(itemType,itemID);
            } else {
                /*
                * If Milestone delete...
                * This is where we delete the milestone
                *
                * 1) Check if milestone contains any tasks
                * 2) If no tasks, just delete milestone
                * 3) If tasks > 0, the popup dialog
                * 4) Ask user if they want to move tasks to another milestone (show list of milestones for project)                    *
                */
                milestoneIDString = $(this).attr('itemid');
                var milestoneIDArray  = milestoneIDString.split('_');
                milestoneID       = milestoneIDArray[1];
                var milestoneTitle = $('#milestoneTitle_'+milestoneID).html();
                $.get(siteURL+'tasks/TaskView/getNumberOfTasksForMilestone/'+milestoneID+'/json/0', function(payload) {
                    if (payload>0) {
                        updateTaskObj.deleteMilestoneModal(milestoneID,milestoneTitle,payload);
                    } else {
                        updateTaskObj.deleteMilestone(milestoneID);
                    }
                });
            }
            getTaskDataObj.getTaskStats();
            return false;
        });

        $('#taskInformationContainer').delegate('.statusTask', 'click', function(e) {
            e.stopPropagation();
            var thisStatus = $(this);
            var taskID = thisStatus.attr('itemID');
            $.ajax({
                type:     'POST',
                url:      siteURL+'tasks/TaskUpdate/updateTaskStatus/'+taskID,
                success: function(payload) {
                    var statusArray = payload.split('|');
                    thisStatus.removeClass();
                    // Human, Machine, Text
                    thisStatus.addClass('statusTask').addClass(statusArray[1]).addClass('clickable');
                    thisStatus.attr('title',statusArray[0]+' '+statusArray[2]).attr('statusDesc',statusArray[0]);
                    getTaskDataObj.getTaskStats();
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
            return false;
        });

        $('#taskInformationContainer').delegate('.projectTimer', 'click', function() {
            var projectObj  = eval('('+projectData+')');
              var projectID   = $('#projectID').val();
              var projectName = projectObj.Title;
              var taskID      = $(this).attr('itemID');
              var taskName    = $('#taskTitle'+taskID+' > a').html();
			  var timerState  = $(this).attr('state');
			  var elementID   = projectID+taskID+'000';
			  if (timerState == 'stop') {
                  /*
                   * Start the timer
                   */
                var timerElementHash = {
					elementID   : elementID,
					projectName : projectName,
					taskName    : taskName,
					projectID   : projectID,
					taskID      : taskID,
					timerRunning: 0,
					elapsedTime : '00:00:00',
					timeSheetID : '0'
				}
				widgetRenderObj.renderWidgetTimerElement(timerElementHash);
				widgetToolsObj.widgetTimerToggle(elementID,'start');
				$(this).attr('state','start');
				$(this).find('span').removeClass('timerStop').addClass('timerGo');
			} else {
                /*
                 * Stop the timer
                 */
				$(this).attr('state','stop');
				widgetToolsObj.widgetTimerToggle(elementID,'stop');
				$(this).find('span').removeClass('timerGo').addClass('timerStop');
			}

            return false;
        });

        $('#taskInformationContainer').delegate('.buttonOpenMessageContainer', 'click', function() {
            $(this).hide();
			$(this).next().show();
			return false;
        });

        $('#taskInformationContainer').delegate('.cancelTaskMessage', 'click', function() {
            $(this).parents('div .messageBoxContainerTask').hide();
			$(this).parents('div .messageBoxContainerTask').prev('.buttonOpenMessageContainer').show();
        });

        $('#taskInformationContainer').delegate('.submitTaskMessage', 'click', function() {
            var messageContainerObj = $(this).parents('div .messageBoxContainerTask');
            var messageListObj = messageContainerObj.next();
			updateMessageObj.saveMessage(messageContainerObj,messageListObj);

			return false;
        });
     },
     attachTaskEventHandlers : function(milestoneID) {
          /*
           * Attach event handler to New Task buttons
           */
          $('.buttonNewTask').unbind('click').click(function(e) {
               e.stopPropagation();
               var milestoneID = $(this).attr('milestoneID');
               $('.taskFormContainer').html('');
               initTaskObj.renderTaskForm(milestoneID);
               return false;
          });

          $('.milestoneRow').unbind('click').click(function() {
               var milestoneID = $(this).data('milestoneID');
               if ($('#taskListContainer'+milestoneID).is(':visible')) {
                   $('#taskListContainer'+milestoneID).slideUp('fast');
               } else {
                   $('#taskListContainer'+milestoneID).slideDown('fast');
               }

               return false;
          });

          $('.taskTitleLink').unbind('click').click(function() {
               var taskID = $(this).attr('itemID');
               if ($('#taskDetailsContainer'+taskID).is(':visible')) {
                   $('#taskDetailsContainer'+taskID).slideUp('fast');
               } else {
                   $('#taskDetailsContainer'+taskID).slideDown('fast');
                   var numberOfMessages = $('#numberOfMessages'+taskID).val();
                   if (numberOfMessages>0 && $(this).data('messagesRendered') != true) {
					    var messageContainerObj = $('#messageListTask'+taskID);
                        getMessageObj.getMessages(taskID,'task',messageContainerObj);
                        $(this).data('messagesRendered',true);
                   }
               }

               return false;
          });

          $('#taskListContainer'+milestoneID)
               .hiliteContainerRow();
          
          $('#taskListContainer'+milestoneID).hoverButtons({
               elementClass: 'taskRow',
               insertionElement: 'controls',
               editClass: 'taskUpdate',
               deleteClass: 'taskDelete',
               position: ''
          });

          if (milestoneID>0) {
              $('#taskInformationContainer').hoverButtons({
                   elementClass: 'milestoneRow',
                   insertionElement: 'controls',
                   editClass: 'taskUpdate',
                   deleteClass: 'taskDelete',
                   position: ''
              });
          }
          permissionsObj.renderControlsBasedOnPermissions();

          $('.buttonEdit').unbind('click').click(function(e) {
               e.stopPropagation();
               var itemID = $(this).attr('itemID');
               var itemArray = itemID.split('_');
               var editItem = itemArray[0];
               var editID   = itemArray[1];

               if (editItem == 'milestone') {
                   initTaskObj.renderMilestoneForm(editID,'edit');
               } else if (editItem == 'task') {
                   initTaskObj.renderTaskForm(editID,'edit');
               }

               return false;
          });

          $('.buttonDelete').unbind('click').click(function() {
			   var itemID = $(this).attr('itemID');
               var itemArray = itemID.split('_');
			   var itemType = itemArray[0];
			   itemID   = itemArray[1];
               var milestoneIDString, milestoneID;
			   if (itemType == 'task') {
                   var milestoneID = $('#taskMilestoneID'+itemID).val();
                   $('#taskRow'+itemID).fadeOut('slow', function() {
                        $('#taskRow'+itemID).remove();
                        supportTasksObj.incrementTaskCount(milestoneID,'down');
                   });
                   updateTrashObj.sendToTrash(itemType,itemID);
			   } else {
                   /*
                    * If Milestone delete...
                    * This is where we delete the milestone
                    *
                    * 1) Check if milestone contains any tasks
                    * 2) If no tasks, just delete milestone
                    * 3) If tasks > 0, the popup dialog
                    * 4) Ask user if they want to move tasks to another milestone (show list of milestones for project)                    *
                    */
                    milestoneIDString = $(this).attr('itemid');
                    var milestoneIDArray  = milestoneIDString.split('_');
                    milestoneID       = milestoneIDArray[1];
                    var milestoneTitle = $('#milestoneTitle_'+milestoneID).html();
                    $.get(siteURL+'tasks/TaskView/getNumberOfTasksForMilestone/'+milestoneID+'/json/0', function(payload) {
                        if (payload>0) {
                            updateTaskObj.deleteMilestoneModal(milestoneID,milestoneTitle,payload);
                        } else {
                            updateTaskObj.deleteMilestone(milestoneID);
                        }
                    });				
			   }               
			   getTaskDataObj.getTaskStats();
               return false;
          });

          $('.statusTask').unbind('click').click(function(e) {
                e.stopPropagation();
				var thisStatus = $(this);
                var taskID = thisStatus.attr('itemID');
                $.ajax({
                    type:     'POST',
                    url:      siteURL+'tasks/TaskUpdate/updateTaskStatus/'+taskID,
                    success: function(payload) {
                        var statusArray = payload.split('|');
                        thisStatus.removeClass();
                        // Human, Machine, Text
                        thisStatus.addClass('statusTask').addClass(statusArray[1]).addClass('clickable');
                        thisStatus.attr('title',statusArray[0]+' '+statusArray[2]).attr('statusDesc',statusArray[0]);
						getTaskDataObj.getTaskStats();
                    },
                    error: function (xhr, desc, exceptionobj) {
                        errorObj.ajaxError(xhr,desc,exceptionobj);
                    }
                });
                return false;
          });

          /*
           * Attach timer control to clock icons
           */
		  $('.projectTimer').unbind('click').click(function() {
              var projectObj  = eval('('+projectData+')');
              var projectID   = $('#projectID').val();
              var projectName = projectObj.Title;
              var taskID      = $(this).attr('itemID');
              var taskName    = $('#taskTitle'+taskID+' > a').html();
			  var timerState  = $(this).attr('state');
			  var elementID   = projectID+taskID+'000';
			  if (timerState == 'stop') {
                  /*
                   * Start the timer
                   */
                var timerElementHash = {
					elementID   : elementID,
					projectName : projectName,
					taskName    : taskName,
					projectID   : projectID,
					taskID      : taskID,
					timerRunning: 0,
					elapsedTime : '00:00:00',
					timeSheetID : '0'
				}
				widgetRenderObj.renderWidgetTimerElement(timerElementHash);
				widgetToolsObj.widgetTimerToggle(elementID,'start');
				$(this).attr('state','start');
				$(this).find('span').removeClass('timerStop').addClass('timerGo');
			} else {
                /*
                 * Stop the timer
                 */
				$(this).attr('state','stop');
				widgetToolsObj.widgetTimerToggle(elementID,'stop');
				$(this).find('span').removeClass('timerGo').addClass('timerStop');
			}

            return false;
		  });

          /*
           * Attach message link event handlers
           */
		  $('.buttonOpenMessageContainer').unbind('click').click(function() {
			  $(this).hide();
			  $(this).next().show();
			  return false;
		  });

		  $('.cancelTaskMessage').unbind('click').click(function() {
			 $(this).parents('div .messageBoxContainerTask').hide();
			 $(this).parents('div .messageBoxContainerTask').prev('.buttonOpenMessageContainer').show();
		  });

		  $('.submitTaskMessage').unbind('click').click(function() {
			var messageContainerObj = $(this).parents('div .messageBoxContainerTask');
            var messageListObj = messageContainerObj.next();
			updateMessageObj.saveMessage(messageContainerObj,messageListObj);

			return false;
		  });

          /*
           * Attach file remove button event handlers
           */
          renderFileObj.attachFileRemoveHandler();
          /*
           * Attach contact card popups to people's names
           */
          contactObj.makeContactLinks();
     },
	 toggleTaskFilter : function() {
		/*
		 * Task filter checkbox event handlers in sidebar
		 */
		$('#chkViewMyTasks').click(function() {
            renderTaskDataObj.toggleTaskMyTasks($(this));
		});
		$('#chkViewCompletedTasks').click(function() {
			renderTaskDataObj.toggleTaskCompleteTasks($(this));
		});
		$('#chkViewIncompleteTasks').click(function() {
			renderTaskDataObj.toggleTaskIncompleteTasks($(this));
		});
	 },
     toggleTaskMyTasks : function(chk) {
         if (chk.attr('checked') == 'checked') {
            // View my tasks only
            $('.notMyTask').fadeOut('fast', function() {
                $(this).addClass('taskOff');
                getTaskDataObj.getTaskStats();
            });
        } else {
            // View all tasks
            $('.notMyTask').fadeIn('fast', function() {
                $(this).removeClass('taskOff');
                getTaskDataObj.getTaskStats();
            });
        }        
     },
     toggleTaskCompleteTasks : function(chk) {
         if (chk.attr('checked') == 'checked') {
            $('.statusTask:not(.status_completed)').parent().parent().parent().fadeOut('fast', function() {
                $(this).addClass('taskOff');
                getTaskDataObj.getTaskStats();
            });
        } else {
            $('.statusTask:not(.status_completed)').parent().parent().parent().fadeIn('fast', function() {
                $(this).removeClass('taskOff');
                getTaskDataObj.getTaskStats();
            });
        }
     },
     toggleTaskIncompleteTasks : function(chk) {
        if (chk.attr('checked') == 'checked') {
            $('.status_completed').parent().parent().parent().fadeOut('fast', function() {
                $(this).addClass('taskOff');
                getTaskDataObj.getTaskStats();
            });
        } else {
            $('.status_completed').parent().parent().parent().fadeIn('fast', function() {
                $(this).removeClass('taskOff');
                getTaskDataObj.getTaskStats();
            });
        }
     },
     toggleCheckState : function() {
         renderTaskDataObj.toggleTaskMyTasks($('#chkViewMyTasks'));
         renderTaskDataObj.toggleTaskCompleteTasks($('#chkViewCompletedTasks'));
         renderTaskDataObj.toggleTaskIncompleteTasks($('#chkViewIncompleteTasks'));
     }
}

var updateTaskObj = {
	saveTask : function(milestoneID) {
        //var taskDescription = taskEditor.getContent();
        var taskDescription = $('#taskDescription').wysiwyg('getContent');
        if (taskDescription == '<html/>' || taskDescription == '<br/>') {
            taskDescription = '';
        }

        /*
         * Check for attached files
         */
        var fileLinkIDString = '';
        $('#taskFile_fileContainer > li').each(function() {
           var fileLinkID = $(this).attr('id');
           var fileLinkIDArray = fileLinkID.split('_');
           fileLinkIDString += fileLinkIDArray[1]+'|';
        });

        var taskID = $('#taskID').val();
        var action = $('#taskAction').val();
        var teamMembers = $('.autocompleteContactsTask').next().val();
        var formHash = {
                    save              : 1,
                    taskID            : taskID,
                    taskNo            : $('#taskNo').val(),
                    action            : action,
                    taskTitle         : $('#taskTitle').val(),
                    taskEstimatedTime : $('#taskEstimatedTime').val(),
                    taskDateStart     : $('#taskDateStart').val(),
                    taskDateEnd       : $('#taskDateEnd').val(),
                    priority          : $('#priority').val(),
                    taskTags          : $('[name="taskTagsHolder"]').val(),
                    description       : taskDescription,
                    milestoneID       : milestoneID,
                    fileLinkIDString  : fileLinkIDString,
                    projectID         : $('#projectID').val(),
                    teamMembers       : teamMembers
        }

        $.ajax({
		    type:     'POST',
		    url:      siteURL+'tasks/TaskUpdate/saveTask',
		    data:     formHash,
            dataType: 'json',
		    success: function(payload) {
                var milestoneID = payload.MilestoneID;
                var milestoneIndex  = $('#milestoneRow'+milestoneID).data('itemIndex');
				var taskString;
                if (action == 'edit') {
                    taskString = renderTaskDataObj.renderTaskRow(payload,false);
                    $('#taskContainer'+taskID).html(taskString);
                    var taskIndex = $('#taskRow'+taskID).attr('itemIndex');
                    taskArray[milestoneIndex].Tasks[taskIndex] = payload;
                } else {
                    taskArray[milestoneIndex].Tasks.push(payload);
					payload.ItemIndex = taskArray[milestoneIndex].Tasks.length-1;
                    taskString = renderTaskDataObj.renderTaskRow(payload,true);
                    $('#taskListContainer'+milestoneID).append(taskString);
                    supportTasksObj.incrementTaskCount(milestoneID,'up');
                    permissionsObj.renderControlsBasedOnPermissions();
                    renderTaskDataObj.attachTaskPlugins();
                }                
                initTaskObj.resetTaskForm(taskID,'edit');
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });

        $('#taskStatus').systemMessage({
            status:  'success',
            message: commonTerms['taskSaved']
        });
        initTaskObj.clearTaskForm(action);
		getTaskDataObj.getTaskStats();

        $('#statusDivInner').hide();
    },
	saveMilestone : function(milestoneID) {
        var action = $('#milestoneAction').val();
		var formHash = {
                    save              : 1,
                    milestoneID       : milestoneID,
                    action            : action,
                    milestoneTitle    : $('#milestoneTitle').val(),
                    milestoneDateStart: $('#milestoneDateStart').val(),
                    milestoneDateEnd  : $('#milestoneDateEnd').val(),
                    projectID         : $('#projectID').val()
        }
		$.ajax({
		    type    : 'POST',
		    url     : siteURL+'tasks/TaskUpdate/saveMilestone',
		    data    : formHash,
            dataType: 'json',
		    success: function(payload) {
                if (action == 'edit') {
                    var milestoneID = payload.MilestoneID;
                    var milestoneIndex = $('#milestoneRow'+milestoneID).data('itemIndex');
                    var milestoneString = renderTaskDataObj.renderMilestoneRow(payload,false);
                    $('#milestoneContainer'+milestoneID).html(milestoneString);
                    $('#milestoneContainer'+milestoneID).find('.buttonNewTask').show();
                    taskArray[milestoneIndex].TaskID     = payload.MilestoneID;
                    taskArray[milestoneIndex].Title      = payload.Title;
                    taskArray[milestoneIndex].Date_Start = payload.Date_Start;
                    taskArray[milestoneIndex].Date_End   = payload.Date_End;

                    taskArray[milestoneIndex].DateStartField = payload.DateStartField;
                    taskArray[milestoneIndex].DateEndField   = payload.DateEndField;
                } else {
                    taskArray.push(payload);
                    var milestoneString = renderTaskDataObj.renderMilestoneRow(payload,true);
                    $('#taskInformationContainer').prepend(milestoneString);
                    
                    var lastIndex = taskArray.length-1;
                    var milestoneID = payload.MilestoneID;
                    $('#milestoneRow'+milestoneID).data('itemIndex',lastIndex);
                    permissionsObj.renderControlsBasedOnPermissions();
                    renderTaskDataObj.attachTaskPlugins();
                }				
                initTaskObj.resetMilestoneForm(milestoneID,'edit');
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });

        $('#milestoneStatus').systemMessage({
            status:  'success',
            message: commonTerms['milestoneSaved']
        });

        $('#statusDivInner').hide();
	},    
    deleteMilestoneModal : function(milestoneID,milestoneTitle,taskCount) {
        var modalContents = [];
        modalContents.push('<div class="errorMessageBig">'+commonTerms['deleteMilestone']+': '+milestoneTitle+'</div>');
        modalContents.push('<p>'+commonTerms['moveTasksNewMilestone']+'</p>');
        modalContents.push('<p><select id="selectMoveToMilestone" style="margin: 3px 0 3px 0; width: 300px;"></select></p>');
        var outString = modalContents.join('');
        $('#deleteContentsContainer').html(outString);
        supportTasksObj.populateMilestoneSelectFromJSON(milestoneArray,$('#selectMoveToMilestone'));

        $('#selectMoveToMilestone option').each(function() {
            if ($(this).val() == milestoneID) {
                $(this).remove();
            }
        });

        $('#buttonDeleteModalDelete').click(function(){
            var moveToMilestoneID = $('#selectMoveToMilestone').val();
            var deleteTasks = 0;
            if (moveToMilestoneID == 'deleteTasksToo') {
                deleteTasks = true;
            }
            updateTaskObj.deleteMilestone(milestoneID,moveToMilestoneID,deleteTasks);
        });
        $('#modalDelete').dialog('open');
    },
    deleteMilestone : function(deleteMilestoneID,moveToMilestoneID,deleteTasks) {
        if (deleteTasks == true) {
            updateTrashObj.sendToTrash('milestoneWithTasks',deleteMilestoneID);
        } else {
            $.ajax({
				type: "POST",
				url:  siteURL+'tasks/TaskUpdate/moveTasksToMilestone/'+deleteMilestoneID+'/'+moveToMilestoneID,
				success: function(payload) {
                    supportTasksObj.refreshTaskList();
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
            updateTrashObj.sendToTrash('milestone',deleteMilestoneID);
        }
        $('#milestoneRow'+deleteMilestoneID).fadeOut('slow');
        $('#taskFormContainer_'+deleteMilestoneID).fadeOut('slow');
		$('#taskListContainer'+deleteMilestoneID).fadeOut('slow');
        $('#modalDelete').dialog('close');

        /*
         * Remove milestone from milestoneArray
         */
        milestoneArray.MilestonesTasks.splice(milestoneArray.MilestonesTasks.indexOf(deleteMilestoneID),1);
    }
}

var supportTasksObj = {
    populateMilestoneSelectFromJSON : function(milestoneArray,selectObj) {
        var milestonesTasksArray = milestoneArray.MilestonesTasks;
        selectObj.html('<option value="deleteTasksToo">'+commonTerms['deleteTasksToo']+'</option><option value="0"></option>');
		for(var a=0;a<=(milestonesTasksArray.length-1);a++) {
			var jsonObject = milestonesTasksArray[a];
			var milestoneID = jsonObject.MilestoneID;
			var milestoneTitle = jsonObject.Title;

			var option = '<option value="'+milestoneID+'">'+milestoneTitle+'</option>';
			selectObj.append(option);
		}
	},
    refreshTaskList : function() {
        var projID = $('#projectID').val();
        getTaskDataObj.getTasks(projID);
    },
    incrementTaskCount : function(milestoneID,dir) {
        /*
         * Increment or decrement task count in milestone row
         */
        var taskCount = parseInt($('#taskCount'+milestoneID).html());
        var taskCountNew;
        if (dir == 'up') {
            taskCountNew = parseInt(taskCount+1);
        } else if (dir == 'down') {
            taskCountNew = parseInt(taskCount-1);
        }
        $('#taskCount'+milestoneID).html(taskCountNew);

        var totalTaskCount = $('.taskRow').size();
        $('#taskCount').html('('+totalTaskCount+')')
    },
    calculateTaskCountForMiletone : function(milestoneID) {
        var taskCount = $('#taskListContainer_'+milestoneID+' .taskRow').is(':visible').size();
        alert(milestoneID+' '+taskCount);
    },
    calculateTaskCountForAllMiletones : function() {
        $('.milestoneRow').each(function() {
           var milestoneID = $(this).data('milestoneID');
           var taskCount = $('#taskListContainer'+milestoneID).find('.taskRow:visible').size();
           $('#taskCount'+milestoneID).html(taskCount);
        });
    }
}

var initTaskObj = {
	initTaskForm : function(milestoneID,itemID,action) {
		/*
		 * Init date pickers
		 */        
		$(".taskDates").datepicker({
		    changeMonth: true,
            changeYear: true
		});

        $('#taskDescription').wysiwyg({
            controls: {
                h2: {visible: false},
                h3: {visible: false},
                insertImage: {visible: false},
                insertTable: {visible: false},
                justifyFull: {visible: false}
            },
            css: siteURL+'css/editorCSS.css'
        });
        $('#taskDescription').wysiwyg('clear');

        $('.saverField').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                if ($('.ac_results').is(':visible')) {
                } else {
                    updateTaskObj.saveTask(milestoneID);
                }
                return false;
            }
        });

        $('#taskFile').attr('itemID',itemID);

        /*
         * Populate team member list
         */
        contactObj.makeContactAutoSuggest($('#sTeamAutocompleter'),false,false,true,false,false,'','',taskEditMembers);

          $('#buttonTaskSave').click(function() {
              updateTaskObj.saveTask(milestoneID,action);
          });

          $('#buttonTaskCancel').click(function() {
               initTaskObj.resetTaskForm(itemID,action);
          });
          
         /*
		  * Init tag field
		  */
         var tagBox = $('#taskTags').autoSuggest(
            tagJSON,
            {
                selectedItemProp: 'value',
                searchObjProps: 'value',
                resultsHighlight: false,
                neverSubmit: true,
                boxWidth: '220px',
                startText: '',
                valuesHolderName: 'taskTagsHolder',
                preFill: $('#tagHolder').val()
            }
         );

         /*
          * Make Task Name field Autocomplete
          */
         $('#taskTitle')
         .autocomplete({
            source: siteURL+'tasks/TaskView/getTasksForAutoCompleter',
            select: function(event, ui) {
                var taskID = ui.item.id;
                ui.item.value = genericFunctions.txtToField(ui.item.value);
                $.get(siteURL+'tasks/TaskView/getTask/'+taskID+'/json', function(payload) {
                    var jsonObj = eval(payload);
                    var taskTitle   = jsonObj[0].Title;
                    var taskDesc    = jsonObj[0].Description;
                    var taskTags    = jsonObj[0].Tags;
                    var taskEstTime = jsonObj[0].HoursEstimated;
                    $('#taskDescription').wysiwyg('setContent',taskDesc);
                    $('#taskTags').val(taskTags);
                    $('#taskEstimatedTime').val(taskEstTime);
                    this.value = ui.item.value;
                    return false;
                });
           }
        });
        
        /*
         * Task priority stuff
         */
        $('#rdoTaskPriority').buttonset();
        $('.taskPriority').click(function() {
           $('#priority').val($(this).val());
        });

         /*
          * If editing, get file list
          */
         if (action == 'edit') {
             var fileList = $('#fileListContainer_'+itemID).html();
             $('#taskFile_fileContainer').html(fileList);
         }
		 fileManagerObj.fileManagerButton();
	},
    initMilestoneForm : function(milestoneID,action) {
		$('#milestoneTitle').focus();
		$('#milestoneFormID').val(milestoneID);
		/*
		 * Init date pickers
		 */
		$('.milestoneDates').datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 2
		});

        $('.milestoneDates').click(function(e) {
           e.stopPropagation();
        });

		$('.milestoneSaverField').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                updateTaskObj.saveMilestone(milestoneID);
                return false;
            }
        });

        $('#milestoneTitle').click(function(e) {
            e.stopPropagation();
		});

		$('#buttonMilestoneSave').click(function(e) {
            e.stopPropagation();
			updateTaskObj.saveMilestone(milestoneID,action);
		});

		$('#buttonMilestoneCancel').click(function(e) {
            e.stopPropagation();
			initTaskObj.resetMilestoneForm(milestoneID,action);
		});
    },
    clearTaskForm : function(action) {
        $('#taskTitle').val('');
        $('#taskTags').val('');
                
        $('#taskEstimatedTime').val('');
        $('#taskDateStart').val('');
        $('#taskDateEnd').val('');

        $('#priorityList > li').removeClass('elementHilite');
        $('#priorityList > li.default').addClass('elementHilite');
        $('#priority').val('1');

        $('#taskFile_fileContainer').html('');

        $('#teamMemberList > li').removeClass('elementHilite');
        $('#teamMemberList > li.default').addClass('elementHilite');
        $('.taskElementTeamMemberCheckbox').attr('checked',false);
        $('.taskElementTeamMemberCheckbox.default').attr('checked',true);

        if ($('#modalFileUpload').dialog('isOpen')) {
            $('#modalFileUpload').dialog('close');
        }

        if (action == 'add') {
            $('#taskTitle').focus();
            $('#taskDescription').wysiwyg('clear');
            //taskEditor.setContent('');
        }
     },     
     renderTaskForm : function(itemID,action) {
        /*
         * Make sure that all other task forms are closed
         */
        $('.taskFormContainer:visible').each(function() {
             var containerID = $(this).attr('id');
             var containerIDArray = containerID.split('_');
             var thisAction = 'edit';
             if (containerIDArray[1] == 0) {
                 thisAction = 'add';
             }
             initTaskObj.resetTaskForm(containerIDArray[1],thisAction);
        });
        if (taskFormHTML == '') {
            taskFormHTML = $('#taskFormContainer').html();
            $('#taskFormContainer').html('');
        }
        
        $('#taskFormContainer_'+itemID)
            .append(taskFormHTML)
            .show();

        if (action == 'edit') {
              $('#taskControls'+itemID).hide();
              $('#taskContainer'+itemID).hide();

              $('#taskAction').val('edit');
              $('#taskID').val(itemID);   
              var taskMilestoneID = $('#taskMilestoneID'+itemID).val();
              var milestoneIndex  = $('#milestoneRow'+taskMilestoneID).data('itemIndex');
              var taskIndex       = $('#taskRow'+itemID).attr('itemIndex');

              /*
			   * Set priority
			   */
              var priorityValue = taskArray[milestoneIndex].Tasks[taskIndex].Priority;
			  $('#priority').val(priorityValue);
              $('.taskPriority').removeAttr('checked');
              
              switch (priorityValue) {
                  case '1':
                      $('#radioPriorityLow').attr('checked','checked');
                      break;
                  case '2':
                      $('#radioPriorityMedium').attr('checked','checked');
                      break;
                  case '3':
                      $('#radioPriorityHigh').attr('checked','checked');
                      break;
                  case '4':
                      $('#radioPriorityExtreme').attr('checked','checked');
                      break;
                  default:
                      $('#radioPriorityLow').attr('checked','checked');
              }
              
              /*
               * Set tags in tagHolder field
               */
              $('#tagHolder').val(taskArray[milestoneIndex].Tasks[taskIndex].Tags);

              /*
			   * Set team members in teamMemberHolder field
			   */
			  var teamMembers = taskArray[milestoneIndex].Tasks[taskIndex].TeamMembers;
              var memberOut = [];
              if (teamMembers.length>0) {
                  for(var a in teamMembers) {
                      var teamMemberUID      = teamMembers[a].UID;
                      var teamMemberPID      = teamMembers[a].PID;
                      var teamMemberName     = teamMembers[a].NameFirst+' '+teamMembers[a].NameLast;
                      var teamMemberLanguage = teamMembers[a].Language;
                      if (teamMembers[a].NameFirst != '' && teamMembers[a].NameFirst != null) {
                        memberOut.push({'value': teamMemberPID+'-team-'+teamMemberLanguage, 'contactName': teamMemberName});
                      }
                  }
                  taskEditMembers = memberOut;
              }

			  /*
			   * Initialize task form
			   */
			  initTaskObj.initTaskForm(taskMilestoneID,itemID,action);

              /*
               * Populate task form here
               */
              var taskTitle = taskArray[milestoneIndex].Tasks[taskIndex].Title;
              $('#taskTitle').val(genericFunctions.txtToField(taskTitle));
              
              var description;
              if (taskArray[milestoneIndex].Tasks[taskIndex].Description) {
                description = taskArray[milestoneIndex].Tasks[taskIndex].Description.stripAnchor();
              }
              $('#taskDescription').wysiwyg('setContent',description);
              $('#taskNo').val(taskArray[milestoneIndex].Tasks[taskIndex].TaskNo);
              $('#taskEstimatedTime').val(taskArray[milestoneIndex].Tasks[taskIndex].HoursEstimated);              
			  $('#taskDateStart').val(taskArray[milestoneIndex].Tasks[taskIndex].DateStartField);
			  $('#taskDateEnd').val(taskArray[milestoneIndex].Tasks[taskIndex].DateEndField);

              var actualHours = parseInt(taskArray[milestoneIndex].Tasks[taskIndex].HoursActual);

              if (actualHours>0) {
                  $('#taskActualTime').html(actualHours);
              } else {
                  $('#taskActualTime').html('0');
              }			  
        } else {
            var taskMilestoneID = itemID;
			initTaskObj.initTaskForm(taskMilestoneID,itemID,action);
        }        
     },
     resetTaskForm : function(itemID,action) {
         $('#taskRow'+itemID).removeClass('hoverRow');
         $('#controls'+itemID).hide();
         $('#taskFormContainer_'+itemID).html('').hide();
         if (action == 'edit') {
            $('#taskControls'+itemID).show();
            $('#taskContainer'+itemID).show();
            $('#taskRow'+itemID).effect('highlight', {}, 3000);
         }

         initTaskObj.clearTaskForm();
     },
	 renderMilestoneForm : function(milestoneID,action) {
         /*
          * Make sure that all other milestone forms are closed
          */
         $('.milestoneFormContainer').each(function() {
             var containerID = $(this).attr('id');
             var containerIDArray = containerID.split('_');
             var thisAction = 'edit';
             if (containerIDArray[1] == 0) {
                 thisAction = 'add';
             }
             initTaskObj.resetMilestoneForm(containerIDArray[1],thisAction);
         });
          if (milestoneFormHTML == '') {
               milestoneFormHTML = $('#milestoneFormContainer').html();
               $('#milestoneFormContainer').html('');
          }
          $('#milestoneFormContainer_'+milestoneID)
               .append(milestoneFormHTML)
               .show();

          if (action == 'edit') {
              $('#milestoneControls'+milestoneID).hide();
              $('#milestoneContainer'+milestoneID).hide();
              var milestoneIndex = $('#milestoneRow'+milestoneID).data('itemIndex');
              $('#milestoneAction').val('edit');
              $('#milestoneFormID').val(milestoneID);
              $('#milestoneTitle').val(genericFunctions.txtToField(taskArray[milestoneIndex].Title));
              $('#milestoneDateStart').val(taskArray[milestoneIndex].DateStartField);
              $('#milestoneDateEnd').val(taskArray[milestoneIndex].DateEndField);
          }     
          initTaskObj.initMilestoneForm(milestoneID,action);
     },
	 resetMilestoneForm : function(milestoneID,action) {
         if (action == 'edit') {
            $('#milestoneControls'+milestoneID).show();
            $('#milestoneContainer'+milestoneID).show();
         }
         $('#milestoneFormContainer_'+milestoneID)
               .html('')
               .slideUp('fast');
         $('#milestoneTitle').val('');
         $('.milestoneDates').val('');         
	 },
     initTaskSearchSort : function() {
         var projectID = $('#projectID').val();
         
         /*
         $('#sTaskAutocompleter').autocomplete({
            source: siteURL+'tasks/TaskView/searchTasks/1/'+projectID,
            select: function(event, ui) {
                var taskID = ui.item.id;
                var term = ui.item.value;
                getTaskDataObj.getTasksFromSearch(term);
                getTaskDataObj.getTask(taskID);
           }
        });
        */

		$('#selectTaskSort').change(function() {
			var sortBy = $('#selectTaskSort').val();
			renderTaskDataObj.sortTasks(sortBy);
			return false;
		});
     }
}