var getReportDataObj = {
    getReportInvoicedReceived : function(year) {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'finances/FinanceReports/reportInvoicedReceived/'+year+'/json/0',
			dataType: 'json',
		    success: function(payload){
		    	var jsonObject = payload;
                renderReportObj.renderReportInvoicedReceived(jsonObject);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getReportInvoicedByProject : function(year) {

    },
    getMostProfitableProjects : function(year) {

    },
    getMostProfitableClients : function(year) {

    }
}

var renderReportObj = {
    renderReportInvoicedReceived : function(jsonObject) {


        $('#chartContainer').html('');
        var invoiced = jsonObject.Invoiced;
        var received = jsonObject.Received;
        var chartTitle = jsonObject.Title;
        var legendLabelInvoiced = jsonObject.Legend[0];
        var legendLabelReceived = jsonObject.Legend[1];

        var invoicedArray = [];
        var receivedArray = [];
        var tickArray     = [];
        for(var a=0;a<=11;a++) {
            tickArray[a] = invoiced[a].Month;
            invoicedArray[a] = parseFloat(invoiced[a].Total);
            receivedArray[a] = parseFloat(received[a].Total);
        }
        var plotInvoicedReceived = $.jqplot('chartContainer',
            [invoicedArray,receivedArray], {
                title: chartTitle,
                grid: {shadow: false},
                legend:{
                    show: true,
                    location:'ne'
                },
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    rendererOptions: {
                        shadowOffset: 1,
                        shadowDepth: 4,
                        shadowAlpha: 0.05
                    }
                },
                series:[
                    {label: legendLabelInvoiced, pointLabels: {show: false} },
                    {label: legendLabelReceived, pointLabels: {show: false} }
                ],
                axes:{
                    xaxis:{
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: tickArray
                    },
                    yaxis:{
                        min:0,
                        tickOptions:{formatString: commonTerms['currencySymbol']+'%.2f'}
                    }
                },
                highlighter: {
                    lineWidthAdjust: 8.5,
                    sizeAdjust: 5,
                    showTooltip: true,
                    tooltipLocation: 'nw',
                    fadeTooltip: true,
                    tooltipFadeSpeed: "fast",
                    tooltipOffset: 2,
                    tooltipAxes: 'y',
                    tooltipSeparator: '',
                    useAxesFormatters: true,
                    tooltipFormatString: '%.5P'
                },
                cursor: { show: false }
        });
    }
}

var reportSupportObj = {
    attachReportChangeHandlers : function() {
        $('#selectReportType').change(function() {
            var reportType = $(this).val();
            var reportYear = $('#selectReportYear').val();
            reportSupportObj.getReportDataFromReportType(reportType, reportYear);
        });

        $('#selectReportYear').change(function() {
            var reportType = $('#selectReportType').val();
            var reportYear = $(this).val();
            reportSupportObj.getReportDataFromReportType(reportType, reportYear);
        });
    },
    getReportDataFromReportType : function(reportType,reportYear) {
        switch (reportType) {
            case 'repInvoicedReceived':
                getReportDataObj.getReportInvoicedReceived(reportYear);
                break;
            case 'repInvoicedByProject':
                getReportDataObj.getReportInvoicedByProject(reportYear);
                break;
            case 'repMostProfitableProjects':
                getReportDataObj.getMostProfitableProjects(reportYear);
                break;
            case 'repMostProfitableClients':
                getReportDataObj.getMostProfitableClients(reportYear);
                break;
            default:
              getReportDataObj.getReportInvoicedReceived(reportYear);
        }
    }
}