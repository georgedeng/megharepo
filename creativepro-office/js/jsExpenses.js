getExpenseDataObj = {
    getExpenseList : function(itemID,group,tag,status,dateStart,dateEnd) {
        if (tag == '' || typeof tag == 'undefined') {
			tag = 0;
		}
        var formHash = {}
        if (dateStart != '' || dateEnd != '') {
            formHash = {
                filter    : 1,
                dateStart : dateStart,
                dateEnd   : dateEnd
            }
        }
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'finances/ExpenseView/getExpenseList/'+itemID+'/'+group+'/'+tag+'/'+status+'/json/0',
            data: formHash,
			dataType: 'json',
		    success: function(payload){
		    	var jsonObject   = payload;
		    	expenseArray = jsonObject.Expenses;
		    	renderExpenseDataObj.renderExpenseGrid(jsonObject);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getFilteredExpenseList : function() {

    },
    getExpenseDetails : function(expenseID,renderType) {
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'finances/ExpenseView/getExpenseDetails/'+expenseID+'/'+renderType+'/0',
		    success: function(payload){
				if ($('#pageLeftColumn').attr('pageID') == 'clientDashboard') {
		    		renderExpenseDataObj.renderExpenseForClient(payload,expenseID);
		    	} else {
		    		renderExpenseDataObj.renderExpenseForOwner(payload,expenseID);
		    	}
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	}
}

renderExpenseDataObj = {
	renderExpenseGrid : function(jsonObject) {
        var expenses      = jsonObject.Expenses;
		var totalExpenses = expenses.length;
		var stringOut     = [];
        $('#expenseID').val('');

        if (totalExpenses<1) {
            stringOut.push('<div class="infoMessageBig">'+commonTerms['noExpenses']+'</div>');
        } else {
            var headerArray   = jsonObject.HeaderLabels.split('|');
            var totalExpenses = jsonObject.TotalExpenses;
            var totalLabel    = jsonObject.FooterLabels;
            stringOut.push('<input type="hidden" id="acdcExpenseGrid" value="asc" />');

            stringOut.push('<table class="dataTable" id="expenseGrid">');
            stringOut.push('<thead><tr>');
            stringOut.push('<th style="width: 48px;"></th>');
            stringOut.push('<th style="width: 200px;" acdc="1" class="sortable sort-alpha">'+headerArray[0]+'</th>');
            stringOut.push('<th style="width: 200px;" acdc="1" class="sortable sort-alpha">'+headerArray[1]+'</th>');
            stringOut.push('<th style="width: 85px;" acdc="1" class="sortable sort-date">'+headerArray[2]+'</th>');
            stringOut.push('<th style="width: 85px; text-align: right;" acdc="1" class="sortable sort-currency">'+headerArray[3]+'</th>');
            stringOut.push('</tr></thead>');
            stringOut.push('<tbody>');
            for(var a=0;a<=((expenses.length)-1);a++) {
                var expenseID              = expenses[a].ExpenseID;
                var expenseTitle           = expenses[a].Title;
                var expenseCategory        = expenses[a].MainCat;
                var expenseDateEntered     = expenses[a].DateExpense;
                var expenseDateEnteredSort = expenses[a].DateExpenseSort;
                var expenseTotal           = expenses[a].Total;
                var expenseTotalSort       = expenses[a].Amount;

                var expenseLink = '#';

                if (
                    $('#pageLeftColumn').attr('pageID') != 'clientDashboard' &&
                    $('#pageLeftColumn').attr('pageID') != 'expenseView') {
                    expenseLink = siteURL+'finances/ExpenseDetail/index/'+expenseID;
                }

                if (expenseCategory == null) {
                    expenseCategory = '';
                }

                stringOut.push('<tr id="expenseRow'+expenseID+'" itemID="'+expenseID+'" class="expenseRow">');
                stringOut.push('<td class="controls"></td>');
                stringOut.push('<td><a href="'+expenseLink+'" class="linkExpense expenseTitle">'+expenseTitle+'</a></td>');
                stringOut.push('<td>'+expenseCategory+'</td>');
                stringOut.push('<td dateSort="'+expenseDateEnteredSort+'">'+expenseDateEntered+'</td>');
                stringOut.push('<td style="text-align: right;" currencySort="'+expenseTotalSort+'">'+expenseTotal+'</td>');
                stringOut.push('</tr>');
            }
            stringOut.push('</tbody>');
            stringOut.push('<tfoot><tr><td colspan="4" style="text-align: right;">'+totalLabel+'</td><td style="text-align: right;">'+totalExpenses+'</td></tr></tfoot></table>');
        }
		var stringOutRender = stringOut.join('');
		$('#expenseInformationContainer').removeClass('activityMessageBig').html(stringOutRender);
		$('#expenseGrid')
			.alternateTableRowColors()
			.hiliteTableRow()
			.tableSorter();

		//pageInitObj.popUpMenus();
		/*
		 * Attach event handler for client view
		 */
		$('.linkExpense').unbind().click(function() {
			var expenseID = $(this).parents('tr').attr('itemID');
			getExpenseDataObj.getExpenseDetails(expenseID,'html');
			return false;
		});

		/*
		 * Create our row menus
		 */
		$('#expenseGrid').hoverButtons({
               elementClass:     'expenseRow',
               insertionElement: 'controls',
               editClass:        'expenseUpdate',
               deleteClass:      'expenseDelete',
               position:         '',
			   insertion:        'prepend',
			   pdfOption:        false
         });
          /*
           * Attach event handlers
           */
          $('.buttonEdit').unbind().click(function() {
               var expenseID = $(this).attr('itemID');
               window.location = siteURL+'finances/ExpenseUpdate/index/'+expenseID;
               return false;
          });

          $('.buttonDelete').unbind().click(function() {
			   var expenseID = $(this).attr('itemID');
               $('#expenseRow'+expenseID).fadeOut('slow');
               updateTrashObj.sendToTrash('expense',expenseID);
               return false;
          });

		  $('.buttonPDF').unbind().click(function() {
               var expenseID = $(this).attr('itemID');
               printObj.goPrint(expenseID,'expense');
               return false;
          });

          /*
           * Permissions
           */
          permissionsObj.renderControlsBasedOnPermissions();
    },
    renderExpenseForOwner : function(expenseHTML,expenseID) { 
        $('html, body').animate({
            scrollTop: $("#pageTitleContainer").offset().top
        }, 300);
        
        $('#tabsFinances').fadeOut('fast');
        $('#itemDetailContainer').fadeIn('fast', function() {
            $('#itemDetailContents').html(expenseHTML);
            $('#invoiceControls').hide();
            $('#expenseControls').show();
            $('#invoicePaymentsContainer').hide();
            $('#expenseID').val(expenseID);
            
            $('#sidebarLedger').hide();
            $('#sidebarInvoices').hide();
            $('#tagsContainer').hide();
            
            /*
             * Initialize message box
             */
            $('#newMessageContainer').attr('itemID',expenseID);
            $('#newMessageContainer').attr('itemtype','expense');

            var clientData = '';
            if ($('#clientID').val()>0) {
                clientData = '[{"NameFull":"'+$('#clientCompany').val()+'","PID":"'+$('#clientID').val()+'","UID":"'+$('#clientUID').val()+'","Language":"'+$('#clientLanguage').val()+'","Type":"client"}]';
            }
            initMessageBox.createMessageBox(clientData,$('#newMessageContainer'),$('#itemMessageConents'),false,true,false,true,true,true);
            getMessageObj.getMessages(expenseID,'expense',$('#itemMessageConents'),0,10);
            
            /*
             * Go get files for this project
             */
            $('#invFile_fileContainer').html('');
            attachedFilesView = true;
            getFileDataObj.getFilesForSomething(expenseID,'expense','#invFile_fileContainer');

           
           /*
            * Attach contact card popups to people's names
            */
            contactObj.makeContactLinks();
            
            /*
             * Init file uploader
             */            
            
            $('#fileUploadingContainer').show();
            if (uploaderInit == false && !$.browser.msie) {
                filesObj.initPluploadDragNDrop(expenseID,'expense',$('#invFile_fileContainer'));
            } 
        });
                
        /*
		 * Toggle page buttons
		 */
        genericFunctions.togglePageHeaderButtons('#buttonEdit','on');
        genericFunctions.togglePageHeaderButtons('#buttonDelete','on');
        genericFunctions.togglePageHeaderButtons('#buttonPrint','off');
        
        genericFunctions.togglePageHeaderButtons('#buttonAddInvoice','off');
        genericFunctions.togglePageHeaderButtons('#buttonAddEstimate','off');
        genericFunctions.togglePageHeaderButtons('#buttonAddExpense','off');
        
        $('#buttonEdit').click(function() {
            var expenseID = $('#expenseID').val();
            window.location = siteURL+'finances/ExpenseUpdate/index/'+expenseID;
        });

        $('#buttonDelete').click(function() {
            var expenseID = $('#expenseID').val();
            updateTrashObj.sendToTrash('expense',expenseID);
            $('#expenseRow'+expenseID).hide();
            expenseSupportObj.showExpenseList();
            return false;
        });   

        /*
         * Permissions
         */
        permissionsObj.renderControlsBasedOnPermissions();
    }
}

initExpenseObj = {
	initExpenseView : function() {
        financeTabs = $('#tabsExpenses').tabs({
			fx: { opacity: 'toggle' },
			show: function(event,ui) {
				var panel = ui.panel.id;
                var projID = $('#projectID').val();
                if (panel == 'windowViewExpenses') {
                    if ($('#expenseID').val()>0) {
                        genericFunctions.togglePageHeaderButtons('.buttonPrint','on');
                        genericFunctions.togglePageHeaderButtons('.buttonAddExpense','on');
                    } else {
                        genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                        genericFunctions.togglePageHeaderButtons('.buttonAddExpense','on');
                    }

                    $('#excelFinanceReport').toggle(false);
                    $('#feedExportContainer').toggle(false);
                    $('#filterContainer').toggle(true);
                    $('#expenseFilterContainer').toggle(true);
                    $('#tagSelectContainer').toggle(false);
                    $('#tagSelectContainer2').toggle(true);

                    var tag = $('#tagExpense').val();
                    getExpenseDataObj.getExpenseList(0,'user',tag,0);
                } else if (panel == 'windowManageVendors') {
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                    var vendorContacts = eval('('+vendorData+')');
                    if (vendorContacts.length>0) {
                        var contactString = renderContactsObj.renderContacts(vendorContacts,0);
                        helperContactsObj.initContactsContainer();
                        helperContactsObj.attachContactEventHandlers();
                    }
				} else if (panel == 'windowViewFinanceReports') {
                    $('#filterContainer').hide();
                    genericFunctions.togglePageHeaderButtons('.buttonAddExpense','on');
                    genericFunctions.togglePageHeaderButtons('.buttonEditExpense','off');
                    genericFunctions.togglePageHeaderButtons('.buttonDeleteExpense','off');
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','on');
                    $('#selectReportYear').toggle(true);
                    $('#reportDateRangeContainer').toggle(false);
                    $('#excelFinanceReport').toggle(true);
                    $('#feedExportContainer').toggle(true);

                    getReportDataObj.getReportInvoicedReceived($('#selectReportYear').val());

                    reportSupportObj.prepInputData();
                    $('.buttonPrint').attr('printWhat','ReportFinance');
                } else if (panel == 'windowMessagesExpense') {
                    genericFunctions.togglePageHeaderButtons('.buttonEditExpense','off');
                    genericFunctions.togglePageHeaderButtons('.buttonDeleteExpense','off');
                    genericFunctions.togglePageHeaderButtons('.buttonExportExcel','off');
                    genericFunctions.togglePageHeaderButtons('.buttonAddExpense','off');
                    $('#excelFinanceReport').toggle(false);
                    $('#feedExportContainer').toggle(false);

                    $('#messageListExpenseAll').html('');
					getMessageObj.getMessages('0','expense',$('#messageListExpenseAll'),0,10);
				} else if (panel == 'windowAlerts') {
				}
			}
		});

        /*
         * Init messaging stuff
         */
        initMessageBox.createMessageBox('',$('#newMessageExpenseAll'),$('#messageListExpenseAll'),false,true,false,true,true,true);

        /*
         * Init contacts stuff for vendors
         */
        helperContactsObj.initContactsForm('V');

        reportSupportObj.attachReportChangeHandlers();

        /*
         * Initialize Open Flash Chart
         */
        chartInitObj.initOpenFlashChart(700,450,siteURL+'finances/FinanceReports/reportInvoicedReceived/'+$('#selectReportYear').val()+'/json/0/1','chartContainer');

		$('#tabsExpenses').tabs({ fx: { opacity: 'toggle' } });
		
        $("#sExpenseAutocompleter").autocomplete(siteURL+'finances/ExpenseView/searchExpenses', {
			width:          400,
			highlight:      false,
			minChars:       3,
			scroll:         true,
			scrollHeight:   300,
			loadingClass:   'formFieldWait'
		});

		$("#sExpenseAutocompleter").result(function(event, data, formatted) {
			var expenseIDString = data[1];
			var expenseIDArray = expenseIDString.split('_');
			var group  = expenseIDArray[0];
			var itemID = expenseIDArray[1];
			$("#sExpenseAutocompleter").val('');
			if (group == 'expense') {
				getExpenseDataObj.getExpenseDetails(itemID,'html');
			} else {
				getExpenseDataObj.getExpenseList(itemID,group,0,'all');
                expenseSupportObj.showExpenseList();
			}
		});

        $('#buttonCloseExpenseDetails').click(function() {
			expenseSupportObj.showExpenseList();
			return false;
		});

		$('.buttonPrint').attr('printWhat','Invoice');

        $('.buttonAddExpense').click(function() {
            window.location = siteURL+'finances/ExpenseUpdate';
        });

        $(".dateField").datepicker({
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

        $('textarea.expanding').elastic();

        $('.payexpensesave').click(function() {
            var invoiceID = $('#invoiceID').val();
            payInvoiceObj.savePayment(invoiceID,$(this));
            payInvoiceObj.clearPayForm();

        });

        $('#buttonExpenseFilter').click(function() {
            getExpenseDataObj.getFilteredExpenseList();
            return false;
        });

		/*
		 * Load default data : invoices for this user
		 */
        var tag = $('#tag').val();
		getExpenseDataObj.getExpenseList(0,'user',tag,0);
        $('#filterInvoiceOpen').attr('checked',true);

        /*
         * Are we viewing an expense?
         */
        if ($('#expenseID').val()>0) {
            getExpenseDataObj.getExpenseDetails($('#expenseID').val(),'html');
        }

        emailInvoiceObj.initInvoiceEmailButton();

        /*
		 * Update tab counts
		 */
        var messageCountExpense = $('#numberOfMessagesExpense').val();

        if (messageCountExpense>0) {
            $('#messageCountExpense').html('('+messageCountExpense+')');
        }

		$('.tabCount').each(function() {
			if ($(this).html() != '(0)') {
				$(this).removeClass('lightGray');
			}
		});
	}
}

var expenseSupportObj = {
	showExpenseList : function() {
        $('#expenseInformationContainer').fadeIn('fast');
        $('#expenseDetailsContainer').fadeOut('fast');
        $('#expenseMessageContainer').fadeOut('fast');
        $('#buttonCloseExpenseDetails').fadeOut('fast');
        $('#expenseID').val('');

        genericFunctions.togglePageHeaderButtons('.buttonEditExpense','off');
        genericFunctions.togglePageHeaderButtons('.buttonDeleteExpense','off');
        genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
        genericFunctions.togglePageHeaderButtons('.buttonAddExpense','on');
    }
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'expenseView') {
		initExpenseObj.initExpenseView();
	} else if ($('#pageLeftColumn').attr('pageID') == 'viewAllexpenses') {
		initExpenseObj.initExpenseView();
	} else if ($('#pageLeftColumn').attr('pageID') == 'expenseUpdate') {
		initExpenseFormObj.initExpenseForm();
	}
});