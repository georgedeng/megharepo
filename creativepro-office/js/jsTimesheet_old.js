var weekDates;
var dayInputTabIndex = 1;

var getTimesheetDataObj = {
	getTimesheetData : function(date) {
		var formHash = {date : date}
		$.ajax({
		    type:        'POST',
		    url:         siteURL+'timesheets/TimesheetView/createTimesheetWeek/json/0',
			data:        formHash,
			dataType:    'json',
			success: function(payload){
		    	renderTimesheetObj.renderTimesheetWeek(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });		
	},
	getTimesheetReportData : function(clientID,projectID,taskID,dateStart,dateEnd,teamMemberID) {
		var formHash = {
						dateStart : dateStart,
						dateEnd   : dateEnd
						}
		$.ajax({
		    type:        'POST',
		    url:         siteURL+'timesheets/TimesheetView/getTimesheetRecords/0/0/'+teamMemberID+'/'+clientID+'/'+projectID+'/'+taskID+'/json/0/1',
			data:        formHash,
			dataType:    'json',
			success: function(payload){
				timesheetReportObj.renderTimesheetReport(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	}
}

var initTimesheetObj = {
	initTimesheetView : function() {
		timesheetTabs = $('#tabsTimesheet').tabs({
			fx: { opacity: 'toggle' },
			show: function(event,ui) {
				var panel = ui.panel.id;
                if (panel == 'windowTimesheet') {
					/*
					 * Turn pageControls off by default
					 */
					$('#pageControlsContainer').hide();
					$('#buttonprint').unbind('click').click(function() {
						var dateStart = $('#dateStart').val();
						var dateEnd   = $('#dateEnd').val();
						var userID    = $('#userID').val();
						var itemIDString = dateStart+'---'+dateEnd+'---'+userID;
						printObj.goPrint(itemIDString,'Timesheet');
					    return false;
					});
					$('#buttonexportExcel').unbind('click').click(function(){
						var dateStart = $('#dateStart').val();
						var dateEnd   = $('#dateEnd').val();
						var userID    = $('#userID').val();
						var itemIDString = dateStart+'---'+dateEnd+'---'+userID;
						exportObj.exportExcel(itemIDString,'TimesheetReport','ExportExcel');
						return false;
					});
				} else if (panel == 'windowTimesheetReports') {
					$('#pageControlsContainer').hide();
					$('#buttonprint').unbind('click').click(function(){
						var taskID = 0,dateStart = 0,dateEnd = 0;
						var clientID  = $('#timesheetSearchClient').val();
						var projectID = $('#timesheetSearchProject').val();

						if ($('#timesheetSearchTask').val() != '' || $('#timesheetSearchTask').val()  == null) {
							taskID    = $('#timesheetSearchTask').val();
						}
						if ($('#timesheetSearchDateStart').val() != '') {
							dateStart = genericFunctions.jsToMySQLDate($('#timesheetSearchDateStart').val());
						}
						if ($('#timesheetSearchDateEnd').val() != '') {
							dateEnd   = genericFunctions.jsToMySQLDate($('#timesheetSearchDateEnd').val());
						}
						var userID    = $('#timesheetSearchTeamMember').val();

						var itemIDString = clientID+'---'+projectID+'---'+taskID+'---'+dateStart+'---'+dateEnd+'---'+userID;
						printObj.goPrint(itemIDString,'ReportTimesheet');
						return false;
					});

					$('#buttonexportExcel').unbind('click').click(function(){
						var taskID = 0,dateStart = 0,dateEnd = 0;
						var clientID  = $('#timesheetSearchClient').val();
						var projectID = $('#timesheetSearchProject').val();
						if ($('#timesheetSearchTask').val() != '') {
							taskID    = $('#timesheetSearchTask').val();
						}
						if ($('#timesheetSearchDateStart').val() != '' || $('#timesheetSearchTask').val()  == null) {
							dateStart = genericFunctions.jsToMySQLDate($('#timesheetSearchDateStart').val());
						}
						if ($('#timesheetSearchDateEnd').val() != '') {
							dateEnd   = genericFunctions.jsToMySQLDate($('#timesheetSearchDateEnd').val());
						}
						var userID    = $('#timesheetSearchTeamMember').val();

						var itemIDString = clientID+'---'+projectID+'---'+taskID+'---'+dateStart+'---'+dateEnd+'---'+userID;
						exportObj.exportExcel(itemIDString,'TimesheetReport','ExportExcel');
						return false;
					});
				}
			}
		});

		/*
		 * Render our week grid
		 */
		getTimesheetDataObj.getTimesheetData(0);

		/*
		 * Attach event handlers
		 */
		$('#goToWeekPrev').click(function() {
			getTimesheetDataObj.getTimesheetData($(this).attr('date'));
			return false;
		});

		$('#goToWeekNext').click(function() {
			getTimesheetDataObj.getTimesheetData($(this).attr('date'));
			return false;
		});

		$(".dateField").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

		$('#goToWeek').change(function() {
			getTimesheetDataObj.getTimesheetData($(this).val());
			return false;
		});

		$('#selectProject').change(function() {
			var projectID = $(this).val();
            if (projectID>0) {
                $.getJSON(siteURL+'tasks/TaskView/getTasksForSelect/'+projectID+'/json/0', function(payload) {
                    var jsonObject = payload;
                    if (jsonObject.length == 0) {
                        $('#selectTask').attr('disabled','disabled');
                        renderTimesheetObj.addRowToTimesheet(true,null,weekDates);
                    } else {
                        timesheetToolsObj.populateTaskSelect(jsonObject,'selectTask');
                    }
                });
            } else {
                $('#selectTask > option').remove();
                $('#selectTask').attr('disabled','disabled');
            }
		});
		
		$('#selectTask').change(function() {
			var taskID = $(this).val();
			renderTimesheetObj.addRowToTimesheet(true,null,weekDates);
		});

		$('#buttonCreateInvoices').click(function() {
			timesheetCreateInvoiceObj.createInvoicesFromTimesheet();
		});

		/*
		 * Timesheet report things
		 */
		$('#timesheetSearchProject').change(function() {
			var projectID = $(this).val();
			$.getJSON(siteURL+'tasks/TaskView/getTasksForSelect/'+projectID+'/json/0', function(payload) {
				var jsonObject = payload;
				if (jsonObject.length == 0) {
					$('#timesheetSearchTask').attr('disabled','disabled');
				} else {
					timesheetToolsObj.populateTaskSelect(jsonObject,'timesheetSearchTask');
				}
			});
		});

		$('#timesheetSearchClient').change(function() {
			var clientID = $(this).val();
			$.getJSON(siteURL+'projects/ProjectView/getProjectsForSelect/'+clientID, function(payload) {
				var jsonObject = payload;
				if (jsonObject.length > 0) {
					$('#timesheetSearchProject').attr('disabled','');
					timesheetToolsObj.populateProjectSelect(jsonObject,'timesheetSearchProject');
				} else {
					$('#timesheetSearchProject').attr('disabled','disabled');
				}
			});
		});

		/*
		 * Init date pickers
		 */
		$(".dateField").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

		$('#buttonCreateTimesheetReport').click(function() {
			var clientID     = $('#timesheetSearchClient').val();
			var projectID    = $('#timesheetSearchProject').val();
			var taskID       = $('#timesheetSearchTask').val();
			var dateStart    = $('#timesheetSearchDateStart').val();
			var dateEnd      = $('#timesheetSearchDateEnd').val();
			var teamMemberID = $('#timesheetSearchTeamMember').val();

			getTimesheetDataObj.getTimesheetReportData(clientID,projectID,taskID,dateStart,dateEnd,teamMemberID);
		});
	}
}

var renderTimesheetObj = {
	renderTimesheetWeek : function(jsonObject) {
		$('#timesheetButtonContainer').hide();
		var prevWeekStart = jsonObject.WeekPrevStart;
		var nextWeekStart = jsonObject.WeekNextStart;

		$('#goToWeekPrev').attr('date',prevWeekStart);
		$('#goToWeekNext').attr('date',nextWeekStart);

		weekDates = jsonObject.WeekDates;

		/*
		 * Set start|end date hidden fields
		 */
		$('#dateStart').val(weekDates[0].date);
		$('#dateEnd').val(weekDates[6].date);

		/*
		 * Create week header
		 */
		var outString = [];
		outString.push('<table id="timesheetTable" class="dataTable"><thead><tr>');
		outString.push('<th style="width: 25%;"></th>');
		for(var i=0;i<=6;i++) {
			outString.push('<th style="width: 10%; font-weight: bold;" class="dateColumn" id="'+weekDates[i].date+'">'+weekDates[i].dayDisplay+'</th>');
		}
		outString.push('<th style="text-align: right; width: 5%; font-weight: normal;"></th>');
		outString.push('</tr></thead><tbody>');

		/*
		 * Total footer
		 */
        outString.push('</tbody><tfoot><tr id="timeSheetFooter" style="display: none;">');
        outString.push('<td style="padding-left: 10px;">'+commonTerms['total']+'</td>');
        for(i=0;i<=6;i++) {
            outString.push('<td><span class="totalDay" id="totalDay_'+weekDates[i].date+'"></span></td>');
        }
        outString.push('<td style="text-align: right;"><span id="grandTotal"></span></td>');
        outString.push('</tr></tfoot>');
		outString.push('</tbody></table>');
		var outStringFinal = outString.join('');
		$('#timesheetContainer').html(outStringFinal);

		/*
		 * Now add content to Timesheet Table
		 */
		if (jsonObject.TimesheetEntries.length > 0) {
			for(var a in jsonObject.TimesheetEntries) {
				var entryObject = jsonObject.TimesheetEntries[a];
				renderTimesheetObj.addRowToTimesheet(false,entryObject,weekDates,a);
			}
		} else {
			$('#timesheetTable tbody').append('<tr class="hoverRow" id="noProjectWarningRow"><td colspan="9"><span class="errorMessageSmall">'+commonTerms['select_project']+'</span></td></tr>');
			$('#pageControlsContainer').hide();
			var noRows = 1;
		}

		if (noRows != 1) {
			$('#timesheetTable')
				.alternateTableRowColors()
				.hiliteTableRow();
			timesheetToolsObj.toggleTimesheetButtons();
		}
	},
	addRowToTimesheet : function(newRow,jsonObject,weekDates,rowCount) {
		var projectID = '',projectTitle = '',taskID = '',taskTitle = '',elementID = 0;        

        var timeSheetID = 0, rowExists = false;

		if (newRow == true) {
			projectID = $('#selectProject :selected').val();
			projectTitle = $('#selectProject :selected').text();
			if ($('#selectTask :selected').val() > 0 && $('#selectTask :selected').val() != 'project') {
				taskTitle = $('#selectTask :selected').text();
				taskID    = $('#selectTask :selected').val();
			}

            var thisDate      = $('#timesheetTable .dateColumn:first').attr('id');
            timeSheetID       = '';
            var elapsedTime   = '0';
		} else {
			projectID    = jsonObject.ProjectID;
			taskID       = jsonObject.TaskID;
			projectTitle = jsonObject.ProjectTitle;
			taskTitle    = jsonObject.TaskTitle;
		}

		if (taskID>0) {
			elementID = projectID+taskID+'000';
		} else {
			elementID = projectID;
		}

        if (newRow == true) {
            /*
             * Add a dummy entry to the timesheet table so this
             * project/task remains in place even if we don't
             * enter any time.
             */
            var formHash = {
                    projectID   : projectID,
                    taskID      : taskID,
                    date        : thisDate,
                    elapsedTime : elapsedTime,
                    timeSheetID : timeSheetID,
                    elementID   : elementID,
                    newRow      : 1
            }
            $.ajax({
                type:  "POST",
                url:   siteURL+'timesheets/TimesheetView/saveTimesheetRecordFromTimesheet',
                data: formHash,
                dataType: 'json',
                success: function(payload) {
                    if (payload.TimeSheetID == 'row exists') {
                        alert('You already have a timesheet row for this project/task.');
                        rowExists = true;
                        $('.timesheetRow:last').hide();
                    } else {
                        $('#timeSheetID_'+payload.ElementID+'_'+payload.Date).val(payload.TimeSheetID);
                    }
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }

        if (rowExists == false) {
            if (taskTitle != '' && taskTitle != null) {
                var projectString = '<span class="projectName"><strong><a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'" title="'+projectTitle+'">'+projectTitle+'</a></strong></span><br /><span class="taskName" style="margin-left: 22px;"><a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'#windowTasks" title="'+taskTitle+'">'+taskTitle+'</a></a></span>';
            } else {
                var projectString = '<span class="projectName"><strong><a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'" title="'+projectTitle+'">'+projectTitle+'</a></strong></span>';
            }

            var outString = [];
            outString.push('<tr class="timesheetRow" id="timeSheetRow_'+elementID+'_'+rowCount+'">');

            outString.push('<td><div class="td_hide_overflow" style="width: 180px;">');
            outString.push('<input type="hidden" class="elementProjectID" id="projectID_'+elementID+'" value="'+projectID+'" />');
            outString.push('<input type="hidden" class="elementTaskID" id="taskID_'+elementID+'" value="'+taskID+'" />');
            outString.push('<a href="#" class="buttonDeleteSmall removeTimesheetEntry" style="margin: 0 6px;" id="remove_'+elementID+'_'+rowCount+'" title="Remove"></a>');
            outString.push(projectString);
            //outString.push('<div style="margin-top: 6px;"><input type="checkbox" id="chkAddToInvoice_'+elementID+'" class="chkAddToInvoice" /><label class="normal clickable" for="chkAddToInvoice_'+elementID+'"> '+commonTerms['add_to_invoice']+'</label></div>');
            outString.push('</div></td>');

            var timeValue = 0;
            var commentValue = '';
            var commentIconClass = 'gray';
            var timeSheetID = null;
            for(var i=0;i<=6;i++) {
                commentValue = '';
                var billableChecked = 'checked="checked"';
                if (newRow == false) {
                    var gotHours = 0;
                    var gotComment = 0;
                    for(var d in jsonObject.Entries) {
                        var thisDate = jsonObject.Entries[d].Date;
                        var comments = jsonObject.Entries[d].Comments;
                        if (thisDate == weekDates[i].date) {
                            timeValue   = jsonObject.Entries[d].Hours;
                            timeSheetID = jsonObject.Entries[d].TimeSheetID;
                            gotHours    = 1;

                            if (comments != null && comments != '') {
                                commentValue = jsonObject.Entries[d].Comments;
                                commentIconClass = '';
                                gotComment = 1;
                            }

                            if (jsonObject.Entries[d].Billable == 0) {
                                billableChecked = '';
                            } else {
                                billableChecked = 'checked="checked"';
                            }
                        }
                    }
                    if (gotHours == 0) {
                        timeSheetID = null;
                        timeValue = 0;
                    }
                    if (gotComment == 0) {
                        commentValue = '';
                        commentIconClass = 'gray';
                    }
                }
                outString.push('<td><input type="text" tabindex="'+dayInputTabIndex+'" style="width: 25px;" id="timeElement_'+elementID+'_'+weekDates[i].date+'_'+rowCount+'" value="'+timeValue+'" class="inputDateTime row_'+elementID+' col_'+weekDates[i].date+'" />&nbsp;');
                outString.push('<span class="icon_comment_small '+commentIconClass+' clickable timesheetAddComment" id="commentTriggerElement_'+elementID+'_'+weekDates[i].date+'_'+rowCount+'"></span>');
                outString.push('<div class="commentContainer" style="display: none;">');
                outString.push('<strong>'+commonTerms['message_comments']+'</strong><br /><textarea style="width: 213px; height: 70px;" id="commentElement_'+elementID+'_'+weekDates[i].date+'_'+rowCount+'" class="timeSheetComment expanding">'+commentValue+'</textarea><br />');
                outString.push('<input type="checkbox" class="billableElement" id="billable_'+elementID+'_'+weekDates[i].date+'_'+rowCount+'" '+billableChecked+' /> '+commonTerms['billable']);
                outString.push('<button class="smallButton buttonSaveComment" id="buttonSaveComment_'+elementID+'_'+weekDates[i].date+'_'+rowCount+'" style="float: right;"><span class="save">'+commonTerms['save']+'</span></button>');
                outString.push('<div style="clear: right;"></div></div>');
                outString.push('<input type="hidden" class="timeSheetIDElement" id="timeSheetID_'+elementID+'_'+weekDates[i].date+'_'+rowCount+'" value="'+timeSheetID+'" />');
                outString.push('</td>');
                dayInputTabIndex++;
            }
            outString.push('<td style="text-align: right;"><span id="totalWeek_'+elementID+'" class="totalWeek bigText" style="font-weight: bold;"></span></td></tr>');
            dayInputTabIndex++;

            var outStringFinal = outString.join('');
            $('#timesheetTable tbody').append(outStringFinal);
            $('#timesheetTable')
                .alternateTableRowColors()
                .hiliteTableRow();
            $('#noProjectWarningRow').hide();
            $('#timeSheetFooter').show();

            renderTimesheetObj.attachTimesheetEventHandlers();
        }

		if (newRow == false) {
			timesheetToolsObj.calulateTimesheetTotals();
		}
		timesheetToolsObj.toggleTimesheetButtons();
	},
    attachTimesheetEventHandlers : function() {
        $('.timesheetAddComment').each(function() {
            var commentSaveButton = $('#buttonSaveComment_'+thisElementID+'_'+thisDate+'_'+rowCount);
            var thisID        = $(this).attr('id');
            var thisIDArray   = thisID.split('_');
            var thisElementID = thisIDArray[1];
            var thisDate      = thisIDArray[2];
            var rowCount      = thisIDArray[3];
            var tipContent = $('#commentElement_'+thisElementID+'_'+thisDate+'_'+rowCount).parent().html();
            if (tipContent != null) {
                $(this).qtip({
                    content: {
						text: tipContent
					},
                    position: {
                        corner: {
                           target:  'rightMiddle',
                           tooltip: 'leftMiddle'
                        }
                    },
                    show: {
                       when: {
                         event: 'click'
                       },
                       delay: 0
                    },
                    hide: {
                       when: {
                           event: 'unfocus'
                       },
                       effect: {
                           type: 'fade'
                       }
                    },
                    style: {
                       tip: {
                           corner: 'leftMiddle'
                       },
                       name: 'cream',
                       width: 250,
                       padding: 12
                    },
					api: {
						onRender : function() {
							commentSaveButton = this.elements.content.find('.buttonSaveComment');
							attachSaveButtonEventHandler(this,commentSaveButton);
						}
					}
               });
            }
		});

        $('.inputDateTime').unbind('blur').blur(function() {
            var thisID = $(this).attr('id');
			var thisIDArray   = thisID.split('_');
            var thisElementID = thisIDArray[1];
            var thisDate      = thisIDArray[2];
            var rowCount      = thisIDArray[3];
            var timeSheetID   = $('#timeSheetID_'+thisElementID+'_'+thisDate+'_'+rowCount).val();

            var elapsedTime     = parseFloat($(this).val());
            var projectID       = $('#projectID_'+thisElementID).val();
            var taskID          = $('#taskID_'+thisElementID).val();
            var elementIDString = thisElementID+'_'+thisDate+'_'+rowCount;

            var formHash = {
                    projectID   : projectID,
                    taskID      : taskID,
                    date        : thisDate,
                    elapsedTime : elapsedTime,
                    timeSheetID : timeSheetID,
                    elementID   : thisElementID
            }
            if ( (timeSheetID > 0 || elapsedTime > 0) && elapsedTime != NaN) {
                $.ajax({
                    type:  "POST",
                    url:   siteURL+'timesheets/TimesheetView/saveTimesheetRecordFromTimesheet',
                    data: formHash,
                    dataType: 'json',
                    success: function(payload) {
                        $('#timeSheetID_'+elementIDString).val(payload.TimeSheetID);
                    },
                    error: function (xhr, desc, exceptionobj) {
                        errorObj.ajaxError(xhr,desc,exceptionobj);
                    }
                });
            } else {
                $(this).val('');
            }

			/*
			 * Redo grid calculations
			 */
			timesheetToolsObj.calulateTimesheetTotals();
        });

		function attachSaveButtonEventHandler(popupObj,buttonObj) {
			$(buttonObj).unbind('click').click(function() {
				var thisID = $(this).attr('id');
				var thisIDArray   = thisID.split('_');
				var thisElementID = thisIDArray[1];
				var thisDate      = thisIDArray[2];
                var rowCount      = thisIDArray[3];

				var timeSheetID   = $('#timeSheetID_'+thisElementID+'_'+thisDate+'_'+rowCount).val();
				var comments = '';
				$(this).prevAll().each(function() {
					if (this.tagName == 'TEXTAREA') {
						comments = $(this).val();
					}
				});

				var billable = 0;
				if ($(this).prev().is(':checked')) {
					billable = 1;
				}
				if (timeSheetID>0) {
					var elapsedTime     = parseFloat($(this).val());
					var projectID       = $('#projectID_'+thisElementID).val();
					var taskID          = $('#taskID_'+thisElementID).val();
                    var elementIDString = thisElementID+'_'+thisDate+'_'+rowCount;

					var formHash = {
							elementID   : thisElementID,
							date        : thisDate,
							comments    : comments,
							timeSheetID : timeSheetID,
							billable    : billable
					}
					$.ajax({
						type:  "POST",
						url:   siteURL+'timesheets/TimesheetView/saveTimesheetComments',
						data:  formHash,
						dataType: 'json',
						success: function(payload) {
                            if (comments.length > 0) {
                                $('#commentTriggerElement_'+elementIDString).removeClass('gray');
                            } else {
                                $('#commentTriggerElement_'+elementIDString).addClass('gray');
                            }
						},
						error: function (xhr, desc, exceptionobj) {
							errorObj.ajaxError(xhr,desc,exceptionobj);
						}
					});
				}
                popupObj.hide();
			});
		}

		$('.removeTimesheetEntry').unbind('click').click(function() {
			var elementIDString = $(this).attr('id');
			var elementIDArray  = elementIDString.split('_');
			var elementID       = elementIDArray[1];
            var rowCount        = elementIDArray[2];

			$('#timeSheetRow_'+elementID+'_'+rowCount).fadeOut('fast', function() {
				$('#timeSheetRow_'+elementID+'_'+rowCount).remove();
				$('#timesheetTable')
				.alternateTableRowColors()
				.hiliteTableRow();

				timesheetToolsObj.calulateTimesheetTotals();
			});
			var projectID = $('#projectID_'+elementID).val();
			var taskID    = $('#taskID_'+elementID).val();
			var dateStart = $('#dateStart').val();
			var dateEnd   = $('#dateEnd').val();

			/*
			 * Get timesheet ID's
			 */
			var timeSheetIDString = '';
			$(this).parents('tr').contents().find('.timeSheetIDElement').each(function(){
				if ($(this).val() != 'null') {
					timeSheetIDString += $(this).val()+'|';
				}
			});

			var formHash = {
				timeSheetIDString : timeSheetIDString
			}
			
			$.ajax({
				type:  "POST",
				url:   siteURL+'timesheets/TimesheetView/deleteTimesheetRecord',
				data: formHash,
				success: function(payload) {
					timesheetToolsObj.toggleTimesheetButtons();
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
			
			return false;
		});
    }
}

var timesheetCreateInvoiceObj = {
	createInvoicesFromTimesheet : function() {
		var projectIDString = '';
		var taskIDString = '';
		$('.chkAddToInvoice').each(function() {
			if ($(this).is(':checked')) {
				var projectID = $(this).parents('td').contents().find('.elementProjectID').val();
				var taskID    = $(this).parents('td').contents().find('.elementTaskID').val();
				projectIDString += projectID+'|';
				taskIDString += taskID+'|';
			}
		});

        if (projectIDString.length<1) {
            $('#createInvoicesMessage')
                .show()
                .systemMessage({
                    status: 'information',
                    noFade:  1,
                    size:    'Small',
                    message: commonTerms['timesheets_no_invoice']
                });
        } else {
            $('#createInvoicesMessage').html('');
            var formHash = {
                    projectIDCollection : projectIDString,
                    taskIDCollection    : taskIDString,
                    dateStart           : $('#dateStart').val(),
                    dateEnd             : $('#dateEnd').val()
            }

            $.ajax({
                type:  "POST",
                url:   siteURL+'finances/InvoiceUpdate/createInvoiceFromTimesheet',
                data: formHash,
                dataType: 'json',
                success: function(payload) {
                    var outString = [];
                    var totalInvoices = payload.length
                    for(var a=0;a<=(totalInvoices-1);a++) {
                        outString.push('<strong><a href="'+siteURL+'finances/InvoiceUpdate/index/'+payload[a]['InvoiceID']+'" class="icon_invoice_small" style="margin: 3px;">'+commonTerms['viewInvoiceNumber']+' '+payload[a]['InvoiceNo']+' for '+payload[a]['Company']+'</a></strong>');
                    }
                    var output = outString.join('');
                    $('#createInvoicesMessage').show().append(output);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
	}
}

var timesheetReportObj = {
	renderTimesheetReport : function(jsonObject) {
		if (jsonObject.Clients && jsonObject.Clients.length>0) {
			$('#timesheetReportContainer').html('');
			$('#pageControlsContainer').show();
			var outString = [];

			var numClients = jsonObject.Clients.length;
			var clients = jsonObject.Clients;
			var headers = jsonObject.Headers;
			var totals  = jsonObject.Totals;
			for(var a=0;a<=numClients-1;a++) {
				var clientID    = clients[a].ClientID;
				var companyName = clients[a].Company;
				var numProjects = clients[a].Projects.length;
				var projects    = clients[a].Projects;
				outString.push('<div class="barBlue bottom"><div style="padding: 6px;">');
				outString.push('<h2 class="companyName"><a href="'+siteURL+'clients/ClientDetail/index/'+clientID+'">'+companyName+'</a></h2>');
				outString.push('</div></div>');

				for(var b=0;b<=numProjects-1;b++) {
					outString.push('<h4 class="icon_project_small projectTitle" style="margin: 12px 0 12px 12px;"><a href="'+siteURL+'projects/ProjectDetail/index/'+projects[b].ProjectID+'">'+projects[b].ProjectTitle+'</a></h4>');
					outString.push('<table class="dataTable timesheetReportTable padBottom18">');
					if (a==0) {
						/*
						 * Render table header
						 */
						outString.push('<thead><tr>');
						outString.push('<th>'+headers.Task+'</th>');
						outString.push('<th>'+headers.Comments+'</th>');
						outString.push('<th>'+headers.TeamMember+'</th>');
						outString.push('<th>'+headers.Billable+'</th>');
						outString.push('<th style="text-align: right;">'+headers.Date+'</th>');
						outString.push('<th style="text-align: right;">'+headers.Hours+'</th>');
						outString.push('</tr></thead>');
					}
					var projectTotalHours = projects[b].ProjectTotalHours;
					var entries = projects[b].Entries;
					for(var c=0;c<=entries.length-1;c++) {
						var timeSheetID = entries[c].TimeSheetID;
						var taskID      = entries[c].TaskID;
						var taskTitle   = entries[c].TaskTitle;
						var elapsedTime = entries[c].ElapsedTime;
						var comments    = entries[c].Comments;
						var billable    = entries[c].Billable;
						var dateTimeSheet = entries[c].DateClockEnd;
						var teamMember    = entries[c].TeamMember;
						var teamMemberID  = entries[c].TeamMemberID;

						if (taskTitle == null) {
							var taskString = '';
						} else {
							var taskString = '<span class="icon_tasks_small">'+taskTitle+'</span>';
						}

						if (comments == null) {
							var commentString = '';
						} else {
							var commentString = '<span class="icon_comment_small">'+comments+'</span>';
						}

						if (billable == 1) {
							var billableString = '<span class="status_completed">&nbsp;</span>';
						} else {
							var billableString = '';
						}

						outString.push('<tr>');
						outString.push('<td style="width: 25%;">'+taskString+'</td>');
						outString.push('<td style="width: 25%;">'+commentString+'</td>');
						outString.push('<td style="width: 20%;"><a href="#" class="personLink" personID="'+teamMemberID+'" id="'+teamMemberID+'">'+teamMember+'</a></td>');
						outString.push('<td style="width: 10%; text-align: center;">'+billableString+'</td>');
						outString.push('<td style="text-align: right;">'+dateTimeSheet+'</td>');
						outString.push('<td style="width: 5%; text-align: right;"><strong>'+elapsedTime+'</strong></td>');
						outString.push('</tr>');

					}
					outString.push('<tfoot><tr><td colspan="4">'+commonTerms['total']+': '+projects[b].ProjectTitle+'</td>');
					outString.push('<td style="text-align: right;" colspan="2">'+projectTotalHours+' '+commonTerms['hrs']+'</td></tr></tfoot>');
					outString.push('</table>');
				}				
			}
			var outStringFinal = outString.join('');
			$('#timesheetReportContainer').append(outStringFinal);
			$('.timesheetReportTable')
				.alternateTableRowColors()
				.hiliteTableRow();
            contactObj.makeContactLinks();
		} else {
			$('#pageControlsContainer').hide();
			$('#timesheetReportContainer').html('<span class="errorMessageSmall">'+commonTerms['no_records']+'</span>'+commonTerms['revise_search']);
		}
	}
}

var timesheetToolsObj = {
	populateTaskSelect : function(jsonObject,controlID) {
		var projectID = $('#'+controlID).val();
		var optionString = '';
        var tasks;
		$('#'+controlID).html('');
		optionString  = '<option value="0">'+commonTerms['select_task']+'</option>';
		optionString += '<option value="0"></option>';
		optionString += '<option value="project" class="emph">'+commonTerms['select_project_only']+'</option>';
        optionString += '<option value="0"></option>';
		$(optionString).appendTo('#'+controlID);
		for(var a=0;a<jsonObject.length;a++) {
            if (jsonObject[a].Milestone == 1) {
                optionString = '<option value="'+jsonObject[a].TaskID+'" class="emph">'+jsonObject[a].Title+'</option>';
                tasks = jsonObject[a].Tasks;

                for(var b=0;b<tasks.length;b++) {
                    optionString += '<option value="'+tasks[b].TaskID+'" class="indent">'+tasks[b].Title+'</option>';
                }
            } else {
                //optionString = '<option value="'+jsonObject[a].TaskID+'" class="indent">'+jsonObject[a].Title+'</option>';
            }
			$(optionString).appendTo('#'+controlID);
		}
		$('#'+controlID).attr('disabled','');
	},
	populateProjectSelect : function(jsonObject,controlID) {
		var optionString = '';
		$('#'+controlID).html('');
		optionString  = '<option value="0">'+commonTerms['select_project']+'</option>';
		optionString += '<option value="0"></option>';
		$(optionString).appendTo('#'+controlID);
		for(var a=0;a<jsonObject.length;a++) {
			optionString = '<option value="'+jsonObject[a].ProjectID+'">'+jsonObject[a].Title+'</option>';
			$(optionString).appendTo('#'+controlID);
		}
	},
    calulateTimesheetTotals : function() {
		/*
		 * Tally days for this project (end of week total)
		 */
		var dayArray = [0,0,0,0,0,0,0];
		$('.timesheetRow').each(function() {
			var rowTotal = 0;
			var row = $(this);
			$('td input.inputDateTime', row).each(function(i) {
				var itemTotal = parseFloat($(this).val());
				if (itemTotal>0) {
					rowTotal = parseFloat(rowTotal+itemTotal);
					dayArray[i] = parseFloat(dayArray[i]+itemTotal);
				}
			});
			$('td span.totalWeek', row).html(rowTotal).formatCurrency({symbol: '',useHtml: true});
		});

		/*
		 * Tally projects for this day (footer total)
		 */
		$('.totalDay').each(function(i) {
			$(this).html(dayArray[i]).formatCurrency({symbol: '',useHtml: true});
		});


		/*
		 * Tally grand total
		 */
		var grandTotal = 0;
		$('.totalWeek').each(function() {
			var weekTotal = parseFloat($(this).html());
			grandTotal = parseFloat(grandTotal+weekTotal);
			$('#grandTotal').html(grandTotal).formatCurrency({symbol: '',useHtml: true});
		});
    },
	toggleTimesheetButtons : function() {
		if ($('.timesheetRow').size()>0) {
			$('#timesheetButtonContainer').show();
			//$('#pageControlsContainer').show();
		} else {
			$('#timesheetButtonContainer').hide();
			$('#pageControlsContainer').hide();
		}
	}
 }

$(document).ready(function() {
	initTimesheetObj.initTimesheetView();
	
	var sparkValues = [2,5,12,6];
	$('#sparkGraphContainer').sparkline(
		sparkValues, {
			type: 'pie',
			height: '175px',
			sliceColors: ['#003399','#98cc98','#ad6762','#cc8c16']
	});
	
	var options = {
		grid: {
			color: '#cccccc',
			backgroundColor: '#ffffff',
			borderColor: '#eeeeee',
			tickColor: '#cccccc',
			borderWidth: 1
		}
	};
});