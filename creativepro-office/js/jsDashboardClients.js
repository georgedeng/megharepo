var invoiceCount;
var projectCount;
var projectList;
var messageCount;
var noteCount;
var alertCount;
var fileCount = 0;
var clientTabs;

var initClientDashboardObj = {
	initDashboard : function() {
		var clientID = $('#clientID').val();
        var clientUserid = $('#clientUserid').val();
		clientTabs = $('#tabsClientDashboard').tabs({
			fx: { opacity: 'toggle' },
			show: function(event,ui) {
				var panel = ui.panel.id;
                if (panel == 'windowProjects') {

				} else if (panel == 'windowInvoices') {

				} else if (panel == 'windowMessages') {
                    $('#messageList').html('');
					getMessageObj.getMessages(0,0,$('#messageList'),0,10,1,0);
                    
				} else if (panel == 'windowNotes') {
                    if (projectCount>1) {

                    } else {
                        getNotesObj.getNotes(0,projectList[0].ProjectID,'P',0,20,'noteListContainer');
                    }

				} else if (panel == 'windowFiles') {
                } else if (panel == 'windowAlerts') {
                    
				}
			}
		});

        $('#selectProjectFiles').change(function() {
            var projectID = $(this).val();
            if (projectID>0) {
                getFileDataObj.getFilesForSomething(projectID,'project','#client_fileContainer',true);
            }
        });
		
		/*
		 * Get project list
		 */
		getProjectDataObj.getProjectList(clientID,'client',0,'client','','all','all');

        /*
         * Get project list for select
         */
        $.getJSON(siteURL+'projects/ProjectView/getProjectsForSelect/'+clientID, function(payload) {
            projectList = payload;
            $('.selectProject').append('<option value="0">'+commonTerms['select_project']+'</value>');
            for(var a=0;a<=(projectList.length-1);a++) {
                $('.selectProject').append('<option value="'+projectList[a].ProjectID+'">'+projectList[a].Title+'</option>');
            }
        });

		/*
		 * Get invoice list
		 */
		getFinanceDataObj.getInvoiceList(clientUserid,'client',0,'all');
        initFinanceObj.attachInvoiceEventHandlers();

		if (invoiceCount>0) {
			$('#invoiceCount').html('('+invoiceCount+')').removeClass('lightGray');
		}
		if (projectCount>0) {
			$('#projectCount').html('('+projectCount+')').removeClass('lightGray');
		}
        if (messageCount>0) {
			$('#messageCount').html('('+messageCount+')').removeClass('lightGray');
		}
        if (noteCount>0) {
			$('#noteCount').html('('+noteCount+')').removeClass('lightGray');
		}

        /*
         * Init messaging stuff
         */
        initMessageBox.createMessageBox(accountData,$('#newMessage'),$('#messageList'),false,true,false,false,false,false,false);
        initMessageBox.createMessageBox(accountData,$('#newMessageFile'),$('#messageListFile'),false,false,false,false,false,false,false);
	}
}

$(document).ready(function() {
	invoiceCount = $('#invoiceCountHolder').val();
	projectCount = $('#projectCountHolder').val();
    messageCount = $('#messageCountHolder').val();
    noteCount    = $('#notesCountHolder').val();
	
	initClientDashboardObj.initDashboard();	
});