var googleDocsObj = {
	getGoogleDocs : function() {
		$.ajax({
		    type:     "POST",
		    url:      siteURL+'google/GoogleDocs/getGoogleDocs',
			dataType: 'json',
		    success: function(payload){
		    	var jsonObject = payload;
				googleDocsObj.renderGoogleDocList(jsonObject);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
	renderGoogleDocList : function(jsonObject) {
		var totalDocs = jsonObject.length;

		var outString = [];
		outString.push('<ul class="fileList">');
		for(var a=0;a<=totalDocs-1;a++) {
			outString.push('<li class="icon_file_'+jsonObject[a].Ext+'_list">');
			outString.push('<a href="'+jsonObject[a].Link+'" target="_blank">'+jsonObject[a].Title+'</a>');
			outString.push('</li>');
		}
		outString.push('</ul>');
		var newOutString = outString.join('');
		$('#docListContainer').html(newOutString);
	}
}

var gmailObj = {
	getGmail : function() {
		$.ajax({
		    type:     "POST",
		    url:      siteURL+'google/GoogleDocs/getGmail',
			dataType: 'json',
		    success: function(payload){
		    	var jsonObject = payload;
				gmailObj.renderGmailList(jsonObject);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
	renderGmailList : function(jsonObject) {
		var totalDocs = jsonObject.length;

		var outString = [];
		outString.push('<ul class="fileList">');
		for(var a=0;a<=totalDocs-1;a++) {
			outString.push('<li class="icon_file_'+jsonObject[a].Ext+'_list">');
			outString.push('<a href="'+jsonObject[a].Link+'" target="_blank">'+jsonObject[a].Title+'</a>');
			outString.push('</li>');
		}
		outString.push('</ul>');
		var newOutString = outString.join('');
		$('#emailListContainer').html(newOutString);
	}
}

$(document).ready(function() {
    var current_headline = 0, old_headline = 0, headline_count, headline_interval;

	$('#linkGoogleDocs').click(function() {
		googleDocsObj.getGoogleDocs();
	});
	
	$('#linkGmail').click(function() {
		gmailObj.getGmail();
	});
});