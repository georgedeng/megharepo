getTrashObj = {
    getItemsInTrash : function(itemType) {
        $.ajax({
            type: "POST",
            url:  siteURL+'trash/AppTrash/getItemsInTrash/'+itemType+'/json/0',
            dataType: 'json',
            success: function(payload){
                trashHelperObj.renderTrashItems(payload,itemType);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    },
    getItemCountInTrash : function() {
        $.ajax({
            type: "POST",
            url:  siteURL+'trash/AppTrash/getItemCountInTrash/json/0',
            dataType: 'json',
            success: function(payload){
                trashHelperObj.renderTrashItemCount(payload);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
}

updateTrashObj = {
    sendToTrash : function(itemType,itemID,doDelete) {
        /*
         * doDelete flag triggers an actual deletion of the record.
         */
        if (doDelete == true) {
            doDelete = '/0';
        } else {
            doDelete = '';
        }
        
		if (itemType == 'contact') {
            $.ajax({
                type: "POST",
                url:  siteURL+'contacts/AppContacts/deleteContact/'+itemID+doDelete,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'client') {
            $.ajax({
                type: "POST",
                url:  siteURL+'clients/ClientUpdate/deleteClient/'+itemID+doDelete,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'milestoneWithTasks') {
            $.ajax({
                type: "POST",
                url:  siteURL+'tasks/TaskUpdate/deleteMilestone/'+itemID+'/1',
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'task') {
            $.ajax({
                type: "POST",
                url:  siteURL+'tasks/TaskUpdate/deleteTask/'+itemID+doDelete,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'milestone') {
            $.ajax({
                type: "POST",
                url:  siteURL+'tasks/TaskUpdate/deleteMilestone/'+itemID+doDelete,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'project') {
            $.ajax({
                type: "POST",
                url:  siteURL+'projects/ProjectUpdate/deleteProject/'+itemID+doDelete,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'invoice') {
            $.ajax({
                type: "POST",
                url:  siteURL+'finances/InvoiceUpdate/deleteInvoice/'+itemID+doDelete,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'expense') {
            $.ajax({
                type: "POST",
                url:  siteURL+'finances/ExpenseUpdate/deleteExpense/'+itemID+doDelete,
                success: function(trashTotal){
					trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'calendarWithEvents') {
            $.ajax({
                type: "POST",
                url:  siteURL+'calendar/CalendarUpdate/deleteCalendar/'+itemID+'/1',
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'calendar') {
            $.ajax({
                type: "POST",
                url:  siteURL+'calendar/CalendarUpdate/deleteCalendar/'+itemID+'/0'+doDelete,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'event') {
            $.ajax({
                type: "POST",
                url:  siteURL+'calendar/CalendarUpdate/deleteEvent/'+itemID+doDelete,
                success: function(trashTotal){
					trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'note') {
            $.ajax({
                type: "POST",
                url:  siteURL+'notes/Notes/deleteNote/'+itemID+doDelete,
                success: function(trashTotal){
					trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'blogPost') {
            $.ajax({
                type: "POST",
                url:  siteURL+'blog/Blog/deleteBlogPost/'+itemID+doDelete,
                success: function(trashTotal){
					trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    },
    restoreFromTrash : function(itemType,itemID) {
        if (itemType == 'contact') {
            $.ajax({
                type: "POST",
                url:  siteURL+'contacts/AppContacts/restoreContact/'+itemID,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'task') {
            $.ajax({
                type: "POST",
                url:  siteURL+'tasks/TaskUpdate/restoreTask/'+itemID,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'milestone') {
            $.ajax({
                type: "POST",
                url:  siteURL+'tasks/TaskUpdate/restoreMilestone/'+itemID,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'project') {
            $.ajax({
                type: "POST",
                url:  siteURL+'projects/ProjectUpdate/restoreProject/'+itemID,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'invoice') {
            $.ajax({
                type: "POST",
                url:  siteURL+'finances/InvoiceUpdate/restoreInvoice/'+itemID,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'expense') {
            $.ajax({
                type: "POST",
                url:  siteURL+'finances/ExpenseUpdate/restoreExpense/'+itemID,
                success: function(trashTotal){
					trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'calendarWithEvents') {
            $.ajax({
                type: "POST",
                url:  siteURL+'calendar/CalendarUpdate/restoreCalendar/'+itemID,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'calendar') {
            $.ajax({
                type: "POST",
                url:  siteURL+'calendar/CalendarUpdate/restoreCalendar/'+itemID,
                success: function(trashTotal){
                    trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'event') {
            $.ajax({
                type: "POST",
                url:  siteURL+'calendar/CalendarUpdate/restoreEvent/'+itemID,
                success: function(trashTotal){
					trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'note') {
            $.ajax({
                type: "POST",
                url:  siteURL+'notes/Notes/restoreNote/'+itemID,
                success: function(trashTotal){
					trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else if (itemType == 'blogPost') {
            $.ajax({
                type: "POST",
                url:  siteURL+'blog/Blog/restoreBlogPost/'+itemID,
                success: function(trashTotal){
					trashHelperObj.renderTrashTotal(trashTotal);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    },
    updateTrashItemCount : function(itemType,incDec) {
        /*
         * Get existing item count
         */
        var existingItemCount = parseInt($('#trashItemCount_'+itemType).html());
        var newItemCount;
        if (incDec == 'decrement') {
            newItemCount = parseInt(existingItemCount-1);
        } else {
            newItemCount = parseInt(existingItemCount+1);
        }
        $('#trashItemCount_'+itemType).html(newItemCount);
    }
}

trashHelperObj  = {
    initTrash : function() {
        if (trashTotal>0) {
            trashHelperObj.renderTrashTotal(trashTotal);
        }
		$('#buttonSiteTrash').click(function() {
            $('.widgetContentContainer').hide();
            $('#trashContent').show();
            /*
             * Do we get trash items?
             */
            if (itemsInTrash.length == 0) {
                getTrashObj.getItemCountInTrash();
            } else {
                trashHelperObj.renderTrashItems(itemsInTrash)                
            }
            
            $('#bigModalHeaderTitleContainer').html(commonTerms['trashcan']);
            $('#bigModalBody > #trashContent').alternateContainerRowColors();
			$('#bigModal').show();
		});
    },
    renderTrashTotal : function(trashTotal) {
        $('#buttonSiteTrash').removeClass('iconTrashInactive').addClass('iconTrashActive');
        $('#siteTrashCount').html(trashTotal);
    },
    renderTrashItemCount : function(itemsInTrash) {
        var outString = [], totalCount = 0;
        for(var trashItem in itemsInTrash) {
            var count    = parseInt(itemsInTrash[trashItem]['count']);
            var label    = itemsInTrash[trashItem]['label'];
            var cssClass = itemsInTrash[trashItem]['iconCSS'];

            if (count>0) {
                outString.push('<div class="boxYellow clickable trashItemCategory" itemType="'+trashItem+'" style="margin-top: 6px; width: 98%;">');
                outString.push('<div class="'+cssClass+'" style="margin: 3px;">'+label+': <span id="trashItemCount_'+trashItem+'">'+count+'</span></div>');
                outString.push('</div>');
                outString.push('<div class="trashItemsContainer_'+trashItem+'" style="display: none;"></div>');
                totalCount = parseInt(totalCount+count);
            }
        }
        if (totalCount>0) {
            var renderString = outString.join('');
            $('#bigModalBody > #trashContent').html(renderString);
        }

        $('.trashItemCategory').click(function() {
            var trashItemType = $(this).attr('itemType');
            var panelObj = $('.trashItemsContainer_'+trashItemType);
            var panelState = genericFunctions.togglePanel(panelObj);
            if (panelState == 'open') {
                getTrashObj.getItemsInTrash(trashItemType);
            }
            return false;
        });
    },
    renderTrashItems : function(trashItems,itemType) {
        var outString = [];
        for(var trashItem in trashItems) {
            var itemID    = trashItems[trashItem]['ID'];
            var itemTitle = trashItems[trashItem]['Title'];
            outString.push('<div id="trashItemContainer_'+itemType+'_'+itemID+'" class="row" style="width: 310px; padding: 3px;">');
            outString.push('<a href="#" class="buttonRestoreSmall restoreTrashItem" itemID="'+itemID+'" title="'+commonTerms['restore']+'"></a>');
            outString.push('<a href="#" class="buttonDeleteSmall deleteTrashItem" itemID="'+itemID+'" title="'+commonTerms['delete']+'"></a>');
            outString.push(itemTitle+'</div>');
        }
        var renderString = outString.join('');
        $('.trashItemsContainer_'+itemType).html(renderString);
        $('.trashItemsContainer_'+itemType).alternateContainerRowColors();

        $('.restoreTrashItem').click(function() {
            var itemID = $(this).attr('itemID');
            updateTrashObj.restoreFromTrash(itemType, itemID);
            $('#trashItemContainer_'+itemType+'_'+itemID).fadeOut('fast');
            updateTrashObj.updateTrashItemCount(itemType,'decrement');
            $('.trashItemsContainer_'+itemType).alternateContainerRowColors();
            return false;
        });

        $('.deleteTrashItem').click(function() {
            var itemID = $(this).attr('itemID');
            updateTrashObj.sendToTrash(itemType, itemID, true);

            $('#trashItemContainer_'+itemType+'_'+itemID).fadeOut('fast');
            updateTrashObj.updateTrashItemCount(itemType,'decrement');
            $('.trashItemsContainer_'+itemType).alternateContainerRowColors();
            return false;
        });
    }
}

