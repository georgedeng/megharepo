//
//  Appcelerator Titanium Mobile
//  WARNING: this is a generated file and should not be modified
//

#import <UIKit/UIKit.h>
#define _QUOTEME(x) #x
#define STRING(x) _QUOTEME(x)

NSString * const TI_APPLICATION_DEPLOYTYPE = @"development";
NSString * const TI_APPLICATION_ID = @"com.upstart.cpomobile";
NSString * const TI_APPLICATION_PUBLISHER = @"UpStart Productions";
NSString * const TI_APPLICATION_URL = @"http://www.upstart-productions.com";
NSString * const TI_APPLICATION_NAME = @"CPO Mobile";
NSString * const TI_APPLICATION_VERSION = @"1.1.0";
NSString * const TI_APPLICATION_DESCRIPTION = @"CreativePro Office is a complete set of free online office management tools for creative professionals.	Manage your team, clients, projects, invoices, quotes and events from one web-based application.";
NSString * const TI_APPLICATION_COPYRIGHT = @"2012 UpStart Productions";
NSString * const TI_APPLICATION_GUID = @"0d350d34-1157-4213-94cb-29a2ecfe887g";
BOOL const TI_APPLICATION_ANALYTICS = true;

#ifdef TARGET_IPHONE_SIMULATOR
NSString * const TI_APPLICATION_RESOURCE_DIR = @"";
#endif

int main(int argc, char *argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

#ifdef __LOG__ID__
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *logPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%s.log",STRING(__LOG__ID__)]];
	freopen([logPath cStringUsingEncoding:NSUTF8StringEncoding],"w+",stderr);
	fprintf(stderr,"[INFO] Application started\n");
#endif

	int retVal = UIApplicationMain(argc, argv, nil, @"TiApp");
    [pool release];
    return retVal;
}
