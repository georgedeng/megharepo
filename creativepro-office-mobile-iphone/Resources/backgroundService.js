/**
 * this is a background service and this code will run *every* time the 
 * application goes into the foreground
 */
var timers   = JSON.parse(Ti.App.Properties.getString('timers'));
var messages = JSON.parse(Ti.App.Properties.getString('messages'));
var tasks    = JSON.parse(Ti.App.Properties.getString('tasks'));

if (timers > 0) {
	var notification = Ti.App.iOS.scheduleLocalNotification({
		alertBody:"You have a timer runing.",
		alertAction:"Open App",
		date:new Date(new Date().getTime() + 1000) // 3 seconds after backgrounding
	});
}	

Ti.App.iOS.addEventListener('notification',function(){
	Ti.API.info('background event received = '+notification);
	//Ti.App.currentService.unregister();
});

Ti.App.currentService.addEventListener('stop',function()
{
	Ti.API.info("background service is stopped");
});

// we need to explicitly stop the service or it will continue to run
// you should only stop it if you aren't listening for notifications in the background
// to conserve system resources. you can stop like this:
Ti.App.currentService.stop();
