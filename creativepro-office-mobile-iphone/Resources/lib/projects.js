exports.getProjectsForSelect = function() {
	var loginAJAX = Titanium.Network.createHTTPClient();
	loginAJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			var projectObj;
			var pickerData = [];
			for (var projectIndex = 0; projectIndex < (jdata.length); projectIndex++ ) {
				projectObj = jdata[projectIndex];
				pickerData.push(Ti.UI.createPickerRow({title: projectObj.Title, value: projectObj.ProjectID}));
			}
			globals.projectPicker.add(pickerData);
			globals.projectPicker.selectionIndicator = true;
			
			globals.projectPicker2.add(pickerData);
			globals.projectPicker2.selectionIndicator = true;
		}	
	}	
	loginAJAX.open('POST',globals.SITE_URL+'projects/ProjectView/getProjectsForSelect');
	loginAJAX.send({
		'authKey': globals.AUTH_KEY
	});	 
}

exports.getProjects = function() {
	return 'big project list';
}