exports.getTasksForSelect = function(projectID) {
	globals.ai._show({message: 'Loading...'});
	var loginAJAX = Titanium.Network.createHTTPClient();
	loginAJAX.onload = function() {
		globals.ai._hide();
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			globals.utils.removeAllPickerRows(globals.taskPicker);
			var msObj,taskObj,msLabel,msRow;
			var pickerData = [];
			if (jdata.length < 1 || jdata.length == null || jdata.length == '') {
				globals.btnDropTask.visible = false;
			} else {
				for (var msIndex = 0; msIndex < (jdata.length); msIndex++ ) {
					msObj = jdata[msIndex];
					msLabel = Ti.UI.createLabel({
						text: msObj.Title,
						font:{fontSize: 21,fontWeight: 'bold'},
						color: '#666'
					});	
					msRow = Ti.UI.createPickerRow({value: '00'});
					msRow.add(msLabel);
					pickerData.push(msRow);
					
					/*
					 * Pick up tasks here
					 */
					for (var taskIndex = 0; taskIndex < (msObj.Tasks.length); taskIndex++ ) {
						taskObj = msObj.Tasks[taskIndex];
						pickerData.push(Ti.UI.createPickerRow({title: '    '+taskObj.Title, value: taskObj.TaskID}));
					}				
				}
				globals.btnDropTask.visible = true;
				globals.taskPicker.add(pickerData);
				globals.taskPicker.selectionIndicator = true;
			}	
		}	
	}	
	loginAJAX.open('POST',globals.SITE_URL+'tasks/TaskView/getTasksForSelect/0/json');
	loginAJAX.send({
		'authKey'  : globals.AUTH_KEY,
		'projectID': projectID
	});	 
}

exports.getMilestonesForSelect = function(projectID,pickerObj) {
	globals.ai._show({message: 'Loading...'});
	var msAJAX = Titanium.Network.createHTTPClient();
	msAJAX.onload = function() {
		globals.ai._hide();
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			globals.utils.removeAllPickerRows(globals.milestonePicker);
			var msObj,taskObj,msLabel,msRow;
			var pickerData = [];
			
			if (jdata.length < 1 || jdata.length == null || jdata.length == '') {
				//globals.btnDropTask.visible = false;
			} else {
				for (var msIndex = 0; msIndex < (jdata.length); msIndex++ ) {
					msObj = jdata[msIndex];
					pickerData.push(Ti.UI.createPickerRow({title: msObj.Title, value: msObj.TaskID}));
				}
				globals.milestonePicker.add(pickerData);
				globals.milestonePicker.selectionIndicator = true;
			}	
		}	
	}	
	msAJAX.open('POST',globals.SITE_URL+'tasks/TaskView/getMilestonesForSelect/'+projectID+'/json/0');
	msAJAX.send({
		'authKey'  : globals.AUTH_KEY
	});	 
}

exports.getTasksForUser = function() {
	globals.ai._show({message: 'Loading...'});
	var loginAJAX = Titanium.Network.createHTTPClient();
	loginAJAX.onload = function() {
		globals.ai._hide();
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			parseUserTasks(jdata);			
			globals.usertasks = jdata;
		}	
	}	
	loginAJAX.open('POST',globals.SITE_URL+'tasks/TaskView/getTaskDigestView/json/0');
	loginAJAX.send({
		'authKey'  : globals.AUTH_KEY
	});	 
}

exports.getAssignedUsersForTask = function(taskID) {
	globals.ai._show({message: 'Loading...'});
	var userAJAX = Titanium.Network.createHTTPClient();
	userAJAX.onload = function() {
		globals.ai._hide();
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			var data = [], row;
			for (var userIndex = 0; userIndex < (jdata.length - 1); userIndex++ ) {
				var user = jdata[userIndex];
				var name = user.NameFirst+' '+user.NameLast;
				row = Ti.UI.createTableViewRow({ height: 40, title: name, hasChild: false });
				data.push(row);
			}	
			tables.assignedTable.setData(data);
		}	
	}	
	userAJAX.open('POST',globals.SITE_URL+'tasks/TaskView/getAssignedUsersForTask/'+taskID);
	userAJAX.send({
		'authKey'  : globals.AUTH_KEY
	});	 
}

function parseUserTasks(jsonObject) {
	var totalTasksOverdue, totalTasksToday, totalTasksTomorrow, totalTasksThisWeek, totalTasksNextWeek, totalTasksIncomplete = '0', totalTasksArchive = '0';
    if (jsonObject.TasksOverdue) {
        totalTasksOverdue  = jsonObject.TasksOverdue.length;
    }
    if (jsonObject.TasksToday) {
        totalTasksToday    = jsonObject.TasksToday.length;
    }
    if (jsonObject.TasksTomorrow) {
        totalTasksTomorrow = jsonObject.TasksTomorrow.length;
    }
    if (jsonObject.TasksThisWeek) {
        totalTasksThisWeek = jsonObject.TasksThisWeek.length;
    }
    if (jsonObject.TasksNextWeek) {
        totalTasksNextWeek = jsonObject.TasksNextWeek.length;
    } 
    if (jsonObject.TasksIncomplete) {
        totalTasksIncomplete = jsonObject.TasksIncomplete;
    }  
    if (jsonObject.TasksArchive) {
        totalTasksArchive = jsonObject.TasksArchive;
    }  
    labels.lblNumberOverdue.text    = totalTasksOverdue;
    labels.lblNumberToday.text      = totalTasksToday;
    labels.lblNumberTomorrow.text   = totalTasksTomorrow;
    labels.lblNumberThisWeek.text   = totalTasksThisWeek;
    labels.lblNumberNextWeek.text   = totalTasksNextWeek;
    labels.lblNumberArchived.text   = totalTasksArchive;
    labels.lblNumberIncomplete.text = totalTasksIncomplete;
}

exports.getAllUserTasksByStatus = function(status,window) {
	globals.ai._show({message: 'Loading...'});
	var task2AJAX = Titanium.Network.createHTTPClient();
	task2AJAX.onload = function() {
		globals.ai._hide();
		var data = this.responseText;
		var jdata = JSON.parse(data);
		Ti.API.info(jdata);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			globals.usertasks2 = jdata;
			globals.tabs.currentTab.open(window,{animated:true});
		}	
	}	
	
	var ajaxEndpoint;
	if (status == 'archive') {
		ajaxEndpoint = globals.SITE_URL+'tasks/TaskView/getAllUserTasksByStatus/2';
	} else if (status == 'incomplete') {
		ajaxEndpoint = globals.SITE_URL+'tasks/TaskView/getAllUserTasksByStatus/incomplete';
	}
	
	task2AJAX.open('POST', ajaxEndpoint);
	task2AJAX.send({
		'authKey'  : globals.AUTH_KEY
	});	 
}

exports.updateTaskStatus = function(taskID,newStatus) {
	globals.ai._show({message: 'Loading...'});
	var taskAJAX = Titanium.Network.createHTTPClient();
	taskAJAX.onload = function() {
		globals.ai._hide();
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			
		}	
	}	
		
	taskAJAX.open('POST', globals.SITE_URL+'tasks/TaskUpdate/updateTaskStatus/'+taskID+'/'+newStatus);
	taskAJAX.send({
		'authKey'  : globals.AUTH_KEY
	});	 
}

exports.saveTask = function(data) {
	globals.ai._show({message: 'Saving...'});
	var taskAJAX = Titanium.Network.createHTTPClient();
	taskAJAX.onload = function() {
		globals.ai._hide();
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			globals.tasks.getTasksForUser();
		}	
	}	
	taskAJAX.open('POST',globals.SITE_URL+'tasks/TaskUpdate/saveTask');
	taskAJAX.send({
		'authKey'     : globals.AUTH_KEY,
		'save'        : 1,
		'action'      : 'add',
		'taskID'      : 0,
		'milestoneID' : data.milestoneID,
		'projectID'   : data.projectID,
		'taskTitle'   : data.taskTitle,
		'taskEstimatedTime' : data.estimatedTime,
		'taskDateStart' : '0000-00-00',
		'taskDateEnd'   : data.taskDateStart,
		'priority'      : data.priority,
		'taskTags'      : '',
		'description'   : data.description
	});	 
}


