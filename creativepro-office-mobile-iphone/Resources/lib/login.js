exports.tryLogin = function(userid,password) {
	tryLoginAtServer(userid,password);
}

exports.tryAutoLogin = function() {
	var db = Titanium.Database.open('cpo');
	var rows = db.execute("SELECT * FROM cpo_login LIMIT 1");
	
	while (rows.isValidRow()) {
		var userid   = rows.fieldByName('Email');
		var password = rows.fieldByName('Password');
		var authKey  = rows.fieldByName('AuthKey');
		var userType = rows.fieldByName('UserType');
		var permissions = rows.fieldByName('Permissions');
		rows.next();
	}		
	db.close();
	
	/*
	 * Try logging in if we have a userid
	 */
	if (userid != '' && userid != null) {
		tryLoginAtServer(userid,password,true);	
	} else {
		globals.loginTab.open();
	}		
}

function tryLoginAtServer(userid,password,autoLogin) {	
	globals.ai._show({message: 'Logging in...'});
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Sorry! We could not find your account.');
		} else {
			globals.AUTH_KEY  = jdata.AuthKey;
			globals.USER_TYPE = jdata.UserType;
			globals.PERMISSIONS = jdata.Permissions;
			jdata.Email = userid;
			jdata.Password = password;
			if (autoLogin != true) {
				saveCredentials(jdata);
			}	   		
    		globals.spinUpApp();
    	}	
	};
	
	var ajaxEndpoint = globals.SITE_URL+'login/tryLogin/1'
	if (globals.AUTH_STRING !== false) {
		ajaxEndpoint = globals.SITE_URL+'login/tryLogin/1/'+globals.AUTH_STRING;
	}
	Ti.API.info(ajaxEndpoint);
	ajax.open('POST', ajaxEndpoint);
	ajax.send({
		'userid' : userid,
		'password' : password
	});
}

function saveCredentials(jdata) {
	var email    = jdata.Email;
	var password = jdata.Password;
	var authKey  = jdata.AuthKey;
    var userType = jdata.UserType;
    var permissions = jdata.Permissions;
    
    var db = Titanium.Database.open('cpo');
    db.execute('DELETE FROM cpo_login');
    db.execute('INSERT INTO cpo_login (Email, Password, AuthKey, Permissions, UserType) values (?,?,?,?,?)', email, password, authKey, permissions, userType);
	db.close();
}

exports.findPassword = function(email) {
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function(email) {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Sorry! We could not find your account.');
		} else {
			alert('Your new password has been emailed to you.');
    	}	
	};
	ajax.open('POST', globals.SITE_URL+'login/findPassword/1');
	ajax.send({
		'email': email
	});
}
