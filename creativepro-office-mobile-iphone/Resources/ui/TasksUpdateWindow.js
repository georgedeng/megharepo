exports.TasksUpdateWindow = function(args) {
	/*
	 * Define some global vars
	 */
	var instance = Ti.UI.createWindow(args);
	var now = globals.moment();	
	var datePickerValue = now.format('YYYY-MM-DD');
	var priorityValue = 2;
	var ui = {};
	
	var buildUI = {
		init : function() {
			buildUI.buildUIControls();
			buildUI.buildPickers();
			
			instance.add(globals.ai);
			
			instance.addEventListener('focus', function(e) {
				helpers.clearForm();
			});
			helpers.setPriorityLabel(priorityValue);	
			
			attachEventHandlers.buttonHandlers();
			attachEventHandlers.pickerHandlers();
			attachEventHandlers.formFieldHandlers();
		},
		buildUIControls : function() {
			ui.btnClose = Ti.UI.createButton({
				title: 'Cancel'
		 	});
		 	
		 	ui.btnSave = Ti.UI.createButton({
		 		title: 'Save'
		 	});
		 	ui.btnSave.enabled = false;
		 
		 	instance.setLeftNavButton(ui.btnClose);
 			instance.setRightNavButton(ui.btnSave);
 			
 			ui.formTableView = buildUI.buildFormTable();
 			ui.scrollView = Ti.UI.createScrollView({ 
				contentWidth:'auto', 
				contentHeight: 700, 
				top: 0, 
				showVerticalScrollIndicator: false, 
				showHorizontalScrollIndicator: false 
			});
			instance.add(ui.scrollView);
			ui.scrollView.add(ui.formTableView);
		},
		buildPickers : function() {
			/*
			 * Project picker
			 */
			ui.projectPicker = globals.projectPicker;
			ui.projectPicker.top = 44;
			
			ui.viewProjectPicker = Ti.UI.createView({
				height: 260,
				bottom: -260
			});
			
			ui.btnCancel = Ti.UI.createButton({
				title: 'Cancel',
				style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
			});
			
			var spacerPicker = Ti.UI.createButton({
				systemButton: Ti.UI.iPhone.SystemButton.FLEXIBLE_SPACE
			});
				
			ui.btnDone = Ti.UI.createButton({
				title: 'Done',
				style: Ti.UI.iPhone.SystemButtonStyle.DONE
			});
			
			var toolbarProjectPicker = Titanium.UI.iOS.createToolbar({
				items: [ui.btnCancel, spacerPicker, ui.btnDone],
				top: 0,
				height: 43,
				backgroundColor: globals.toolbarColor,
				backgroundImage: 'none'
			});
			
			ui.viewProjectPicker.add(toolbarProjectPicker,ui.projectPicker);
			
			/*
			 * Milestone picker
			 */
			globals.milestonePicker = Ti.UI.createPicker();
			globals.milestonePicker.top = 43;
			var pickerData = [];
			pickerData.push(Ti.UI.createPickerRow({ title: '' }));
			globals.milestonePicker.add(pickerData);
			
			ui.viewMilestonePicker = Ti.UI.createView({
				height: 260,
				bottom: -260
			});
				
			ui.btnCancelMilestone = Ti.UI.createButton({
				title: 'Cancel',
				style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
			});
				
			ui.btnDoneMilestone = Ti.UI.createButton({
				title: 'Done',
				style: Ti.UI.iPhone.SystemButtonStyle.DONE
			});
			
			var toolbarMilestonePicker = Titanium.UI.iOS.createToolbar({
				items: [ui.btnCancelMilestone, spacerPicker, ui.btnDoneMilestone],
				top: 0,
				height: 43,
				backgroundColor: globals.toolbarColor,
				backgroundImage: 'none'
			});
			
			ui.viewMilestonePicker.add(toolbarMilestonePicker,globals.milestonePicker);
			
			/*
			 * Date picker
			 */
			ui.viewCalPicker = Ti.UI.createView({
				height: 260,
				bottom: -260
			});
		
			ui.calPicker = Ti.UI.createPicker({
				type:Ti.UI.PICKER_TYPE_DATE
			});	
			ui.calPicker.top = 43;
			
			ui.btnCancelCal = Ti.UI.createButton({
				title: 'Cancel',
				style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
			});	
		
			ui.btnDoneCal = Ti.UI.createButton({
				title: 'Done',
				style: Ti.UI.iPhone.SystemButtonStyle.DONE
			});
			
			var toolbarCalPicker = Titanium.UI.iOS.createToolbar({
				items: [ui.btnCancelCal, spacerPicker, ui.btnDoneCal],
				top: 0,
				height: 43,
				backgroundColor: globals.toolbarColor,
				backgroundImage: 'none'
			});
			
			ui.viewCalPicker.add(toolbarCalPicker,ui.calPicker);
			
			/*
			 * Priority picker
			 */
			ui.viewPriPicker = Ti.UI.createView({
				height: 260,
				bottom: -260
			});
				
			ui.priPicker = Ti.UI.createPicker();	
			ui.priPicker.selectionIndicator = true;
			ui.priPicker.top = 43;
			
			var pickerRow = Ti.UI.createPickerRow({value: 1});
			var pickerText = Ti.UI.createLabel({
				text: 'Low',
				left: 40,
				font: { fontSize: 18, fontWeight: 'bold' }
			});
			var pickerImg = Ti.UI.createImageView({image:'images/iconPriorityGreen24.png', left: 8, width: 24, height: 24});
			pickerRow.add(pickerImg,pickerText);
			ui.priPicker.add(pickerRow);
			
			pickerRow = Ti.UI.createPickerRow({value: 2});
			pickerText = Ti.UI.createLabel({
				text: 'Medium',
				left: 40,
				font: { fontSize: 18, fontWeight: 'bold' }
			});
			pickerImg = Ti.UI.createImageView({image:'images/iconPriorityYellow24.png', left: 8, width: 24, height: 24});
			pickerRow.add(pickerImg,pickerText);
			ui.priPicker.add(pickerRow);
	
			pickerRow = Ti.UI.createPickerRow({value: 3});
			pickerText = Ti.UI.createLabel({
				text: 'High',
				left: 40,
				font: { fontSize: 18, fontWeight: 'bold' }
			});
			pickerImg = Ti.UI.createImageView({image:'images/iconPriorityOrange24.png', left: 8, width: 24, height: 24});
			pickerRow.add(pickerImg,pickerText);
			ui.priPicker.add(pickerRow);
			
			pickerRow = Ti.UI.createPickerRow({value: 4});
			pickerText = Ti.UI.createLabel({
				text: 'Extreme',
				left: 40,
				font: { fontSize: 18, fontWeight: 'bold' }
			});
			pickerImg = Ti.UI.createImageView({image:'images/iconPriorityRed24.png', left: 8, width: 24, height: 24});
			pickerRow.add(pickerImg,pickerText);
			ui.priPicker.add(pickerRow);	
			
			ui.btnCancelPri = Ti.UI.createButton({
				title: 'Cancel',
				style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
			});	
				
			ui.btnDonePri = Ti.UI.createButton({
				title: 'Done',
				style: Ti.UI.iPhone.SystemButtonStyle.DONE
			});
			
			var toolbarPriPicker = Titanium.UI.iOS.createToolbar({
				items: [ui.btnCancelPri, spacerPicker, ui.btnDonePri],
				top: 0,
				height: 43,
				backgroundColor: globals.toolbarColor,
				backgroundImage: 'none'
			});
			
			ui.viewPriPicker.add(toolbarPriPicker,ui.priPicker);
			
			instance.add(ui.viewProjectPicker);
			instance.add(ui.viewMilestonePicker);
			instance.add(ui.viewCalPicker);
			instance.add(ui.viewPriPicker);
		},
		buildFormTable : function() {
			data = [];
 	
		 	var tr = Ti.UI.create2DMatrix();
			tr = tr.rotate(90);
			
			ui.btnDropProject = Ti.UI.createButton({
				style: Ti.UI.iPhone.SystemButton.DISCLOSURE,
				transform: tr
			});
			ui.btnDropMilestone = Ti.UI.createButton({
				style: Ti.UI.iPhone.SystemButton.DISCLOSURE,
				transform: tr
			});
	
			var tableView = Ti.UI.createTableView({
				style: Titanium.UI.iPhone.TableViewStyle.GROUPED,
				backgroundColor:'transparent',
				rowBackgroundColor:'white'
			});
			
			ui.fieldTitle = Ti.UI.createTextField({
				hintText: 'Title',
				left: 8,
				clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
				keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
				returnKeyType: Titanium.UI.RETURNKEY_DONE
			});
	
			ui.fieldProject = Ti.UI.createTextField({
				hintText: 'Select Project',
				editable: false,
				left: 30,
				rightButton: ui.btnDropProject,
				rightButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS
			});
			
			ui.fieldMilestone = Ti.UI.createTextField({
				hintText: 'Select Milestone',
				editable: false,
				left: 30,
				rightButton: ui.btnDropMilestone,
				rightButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS
			});
			
			ui.fieldDate = Ti.UI.createTextField({
				hintText: 'Due Date',
				editable: false,
				left: 30,
				clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS
			});
	
			ui.fieldHours = Ti.UI.createTextField({
				hintText: 'Estimated Hours',
				left: 30,
				clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
				keyboardType: Titanium.UI.KEYBOARD_DECIMAL_PAD,
				returnKeyType: Titanium.UI.RETURNKEY_DONE
			});
			
			ui.txtComments = Ti.UI.createTextArea({
				value: 'Notes',
				color: '#bfbdbd',
				font: { fontSize: 17 },
				height: 50,
				left: 28,
				top: 0,
				keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
				returnKeyType: Titanium.UI.RETURNKEY_DONE
			});
			
			var row = Ti.UI.createTableViewRow({height: 40});
			row.add(ui.fieldTitle);
			data.push(row);
	
			row = Ti.UI.createTableViewRow({height: 40});
			var lblIcon = Ti.UI.createLabel({
				backgroundImage: 'images/iconProjectsLtGray.png',
				width: 16,
				height: 16,
				top: 12,
				left: 8
			});
			row.add(lblIcon,ui.fieldProject);
			data.push(row);
			
			row = Ti.UI.createTableViewRow({height: 40});
			lblIcon = Ti.UI.createLabel({
				backgroundImage: 'images/iconMilestoneLtGray.png',
				width: 16,
				height: 16,
				top: 12,
				left: 8
			});
			row.add(lblIcon,ui.fieldMilestone);
			data.push(row);
	
			row = Ti.UI.createTableViewRow({height: 40});
			lblIcon = Ti.UI.createLabel({
				backgroundImage: 'images/iconCalendarLtGray.png',
				width: 16,
				height: 16,
				top: 12,
				left: 8
			});
			row.add(lblIcon,ui.fieldDate);
			data.push(row);
	
			row = Ti.UI.createTableViewRow({height: 40});
			lblIcon = Ti.UI.createLabel({
				backgroundImage: 'images/iconClockLtGray.png',
				width: 16,
				height: 16,
				top: 12,
				left: 8
			});
			row.add(lblIcon,ui.fieldHours);
			data.push(row);
	
			row = Ti.UI.createTableViewRow({height: 40});
			ui.fieldPriorityIcon = Ti.UI.createLabel({
				width: 24,
				height: 24,
				left: 33
			});
			ui.fieldPriorityText = Ti.UI.createTextField({
				left: 65,
				width: 250
			});
			lblIcon = Ti.UI.createLabel({
				backgroundImage: 'images/iconPriorityLtGray.png',
				width: 16,
				height: 16,
				top: 12,
				left: 8
			});
			row.add(lblIcon,ui.fieldPriorityIcon,ui.fieldPriorityText);
			data.push(row);
	
			row = Ti.UI.createTableViewRow({height: 65});
			lblIcon = Ti.UI.createLabel({
				backgroundImage: 'images/iconNotesLtGray.png',
				width: 16,
				height: 16,
				top: 12,
				left: 8
			});
			row.add(lblIcon,ui.txtComments);
			data.push(row);
			
			tableView.setData(data);	
			
			return tableView;
		}
	}
	
	var attachEventHandlers = {
		buttonHandlers : function() {	
			ui.btnClose.addEventListener('click', function() {
		 		helpers.closeForm();
		 	});
		 	
		 	ui.btnSave.addEventListener('click', function() {
		 		dataObj.saveTask();
		 		helpers.closeForm();
		 	});
		},
		pickerHandlers : function() {
			/*
			 * Project handlers
			 */
			ui.btnCancel.addEventListener('click', function(e) {
				ui.viewProjectPicker.animate(globals.pickerSlideDown);
			});
			
			ui.btnDone.addEventListener('click', function(e) {
				ui.fieldProject.value = ui.projectPicker.getSelectedRow(0).title;
				var projectID = ui.projectPicker.getSelectedRow(0).value;
				ui.viewProjectPicker.animate(globals.pickerSlideDown);
				/*
				 * Get milestones for this project.
				 */
				globals.tasks.getMilestonesForSelect(projectID,globals.milestonePicker);
				ui.btnDropMilestone.visible = true;
			});
			
			/*
			 * Milestone handlers
			 */			
			ui.btnCancelMilestone.addEventListener('click', function(e) {
				ui.viewMilestonePicker.animate(globals.pickerSlideDown);
			});
			
			ui.btnDoneMilestone.addEventListener('click', function(e) {		
				var milestoneTitle;
				if (typeof globals.milestonePicker.getSelectedRow(0) == 'undefined') {
					milestoneTitle = '';
				} else {
					milestoneTitle = globals.milestonePicker.getSelectedRow(0).title;
				}
				milestoneTitle = globals.utils.trim(milestoneTitle);
				ui.fieldMilestone.value = globals.utils.txtToField(milestoneTitle);
				ui.viewMilestonePicker.animate(globals.pickerSlideDown);
			});
			
			/*
			 * Date handlers
			 */
			ui.calPicker.addEventListener('change', function(e) {
				var raw = e.value.toLocaleString();
				var date = globals.moment(raw);
				datePickerValue = date.format('YYYY-MM-DD');
				ui.fieldDate.value = date.format('MMMM Do YYYY');
			});	
			
			ui.calPicker.addEventListener('show', function(e) {
				var raw = e.value.toLocaleString();
				var date = globals.moment(raw);
				datePickerValue = date.format('YYYY-MM-DD');
				ui.fieldDate.value = date.format('MMMM Do YYYY');
			});	
			
			ui.btnCancelCal.addEventListener('click', function(e) {
				ui.viewCalPicker.animate(globals.pickerSlideDown);
				ui.scrollView.scrollTo(0,0);
			});
				
			ui.btnDoneCal.addEventListener('click', function(e) {
				ui.viewCalPicker.animate(globals.pickerSlideDown);
				ui.scrollView.scrollTo(0,0);
			});
			
			/*
			 * Priority picker handlers
			 */
			ui.priPicker.addEventListener('change', function(e) {
				helpers.setPriorityLabel(e.row.value);
				priorityValue = e.row.value;
			});
			
			ui.btnCancelPri.addEventListener('click', function(e) {
				ui.viewPriPicker.animate(globals.pickerSlideDown);
				ui.scrollView.scrollTo(0,0);
			});
				
			ui.btnDonePri.addEventListener('click', function(e) {
				ui.viewPriPicker.animate(globals.pickerSlideDown);
				ui.scrollView.scrollTo(0,0);
			});
		},
		formFieldHandlers : function() {
			ui.fieldTitle.addEventListener('change', function() {
				if (ui.fieldTitle.value.length>0) {
					helpers.toggleSaveButton(true);
				} else {
					helpers.toggleSaveButton(false);
				}
			});
			
			ui.fieldTitle.addEventListener('focus', function() {
				helpers.closePickers();
				ui.txtComments.blur();
			});	
	
			ui.fieldDate.addEventListener('focus', function(e) { 	
				ui.fieldDate.blur();	
				helpers.closePickers();
				ui.txtComments.blur();
				
		 		var now = globals.moment();
		 		var thisYear  = parseInt(now.format('YYYY'));
		 		var thisDate  = now.format('DD');
		 		var thisMonth = parseInt(now.format('MM')-1); 
		 		var lastYear = thisYear - 1;
		 		
		 		var valueArray = datePickerValue.split('-');
		 		var valueYear  = valueArray[0];
		 		var valueMonth = parseInt(valueArray[1]-1);
		 		var valueDate  = valueArray[2];
				
				var value = new Date();
					value.setYear(valueYear);
					value.setMonth(valueMonth);
					value.setDate(valueDate);
				ui.calPicker.value = value;	
				ui.viewCalPicker.animate(globals.pickerSlideUp);
				ui.scrollView.scrollTo(0,50);
		 	});
		 	
		 	ui.fieldHours.addEventListener('focus', function() {
		 		ui.fieldTitle.blur();
		 		ui.txtComments.blur();
		 		helpers.closePickers();
		 		ui.scrollView.scrollTo(0,50);
		 	});
 	
		 	ui.fieldHours.addEventListener('blur', function() {
		 		ui.scrollView.scrollTo(0,0);
		 	});
		 	
		 	ui.fieldPriorityText.addEventListener('focus', function() {
		 		ui.fieldTitle.blur();
		 		ui.fieldPriorityText.blur();
		 		helpers.closePickers();
		 		ui.txtComments.blur();
		 		
		 		ui.viewPriPicker.animate(globals.pickerSlideUp);
		 		ui.scrollView.scrollTo(0,50);
		 	});
		 	
		 	ui.txtComments.addEventListener('return', function(e) {
				ui.scrollView.scrollTo(0,0);
			});
			
			ui.txtComments.addEventListener('blur', function(e) {
				ui.scrollView.scrollTo(0,0);
			});
	
			ui.txtComments.addEventListener('focus', function(e) {
				ui.fieldTitle.blur();
				helpers.closePickers();
				
				ui.scrollView.scrollTo(0,50);
				this.color = '#000';
				if (this.value == 'Notes') {
					this.value = '';	
				}
			});
			
			ui.fieldProject.addEventListener('focus', function(e) {
				ui.fieldTitle.blur();
				helpers.closePickers();
				ui.txtComments.blur();
				
				ui.viewProjectPicker.animate(globals.pickerSlideUp);
				ui.fieldProject.blur();
			});
			
			ui.fieldMilestone.addEventListener('focus', function(e) {
				ui.fieldTitle.blur();
				helpers.closePickers();
				ui.txtComments.blur();
				
				ui.viewMilestonePicker.animate(globals.pickerSlideUp);
				ui.fieldMilestone.blur();
			});
			
			ui.btnDropProject.addEventListener('click', function(e) {
				ui.fieldTitle.blur();
				helpers.closePickers();
				ui.txtComments.blur();
				
				ui.viewProjectPicker.animate(globals.pickerSlideUp);
				ui.fieldProject.blur();
			});
			
			ui.btnDropMilestone.addEventListener('click', function(e) {
				ui.fieldTitle.blur();
				helpers.closePickers();
				ui.txtComments.blur();
				
				ui.viewMilestonePicker.animate(globals.pickerSlideUp);
				ui.fieldMilestone.blur();
			});
		}
	}
	
	var dataObj = {
		saveTask : function() {
			if (ui.fieldProject.value == '') {
				var alert = Ti.UI.createAlertDialog({
					message: 'Please select a project first.',
					ok: 'Okay',
					title: 'Oops!'
				}).show();
			} else if (ui.fieldTitle.value == '') {
				var alert = Ti.UI.createAlertDialog({
					message: 'Please enter a title for this task.',
					ok: 'Okay',
					title: 'Oops!'
				}).show();
			} else {
				var data = dataObj.getFormData();
				globals.tasks.saveTask(data);
				helpers.closeForm();
			}	
		},
		getFormData : function() {
			var comments = '',projectID,milestoneID;
			if (ui.txtComments.value != 'Notes') {
				comments = globals.utils.escapeString(ui.txtComments.value);
			}
			
			if (typeof ui.projectPicker.getSelectedRow(0) == 'undefined') {
				projectID = '';
			} else {
				projectID = ui.projectPicker.getSelectedRow(0).value;
			}
			
			if (typeof globals.milestonePicker.getSelectedRow(0) == 'undefined') {
				milestoneID = '';
			} else {
				milestoneID = globals.milestonePicker.getSelectedRow(0).value;
			}
		
			var data = {
				milestoneID : milestoneID,
				projectID   : projectID,
				taskTitle   : ui.fieldTitle.value,
				estimatedTime : ui.fieldHours.value,
				taskDateStart : datePickerValue,
				priority      : priorityValue,
				description   : comments
			}
			
			return data;
		}
	}
	
	var helpers = {
		closePickers : function() {
			ui.viewCalPicker.animate(globals.pickerSlideDown);
			ui.viewPriPicker.animate(globals.pickerSlideDown);
			ui.viewMilestonePicker.animate(globals.pickerSlideDown);
			ui.viewProjectPicker.animate(globals.pickerSlideDown);
		},
		closeForm : function() {
			helpers.clearForm();
			instance.close({modal: true});
		},		
		clearForm : function() {
			ui.projectPicker.setSelectedRow(0,0,false);
			ui.viewProjectPicker.animate(globals.pickerSlideDown);
			ui.btnDropMilestone.visible = false;
		},
		setPriorityLabel : function(priorityValue) {
			var priorityIcon, priorityText;
			switch(priorityValue) {
				case 1: 
					priorityIcon = 'images/iconPriorityGreen24.png';
					priorityText = 'Low';
					break;
				case 2:
					priorityIcon = 'images/iconPriorityYellow24.png';
					priorityText = 'Medium';
					break;
				case 3:
					priorityIcon = 'images/iconPriorityOrange24.png';
					priorityText = 'High';
					break;
				case 4:
					priorityIcon = 'images/iconPriorityRed24.png';
					priorityText = 'Extreme';
					break;			
			}
			ui.fieldPriorityIcon.backgroundImage = priorityIcon;
			ui.fieldPriorityText.value = priorityText;
		},	
		toggleSaveButton : function(state) {
			if (state == true) {
				/*
				 * Turn button ON
				 */
				ui.btnSave.enabled = true;
			} else {
				ui.btnSave.enabled = false;
			}
		}
	}	
	
	buildUI.init();
	return instance;
}	