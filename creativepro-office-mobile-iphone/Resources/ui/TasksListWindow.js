exports.TasksListWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	var TasksDetailWindow = require('ui/TasksDetailWindow');
	var TasksUpdateWindow = require('ui/TasksUpdateWindow');
	var tasksUpdateWindow = new TasksUpdateWindow({title: 'New Task', backgroundColor: globals.appBGColor, barColor: globals.navBarColor, tabBarHidden: true});
	var btnNewTask = Ti.UI.createButton({
		title: 'New Task',
 		image: 'images/plus-white.png'
 	});
 	btnNewTask.addEventListener('click', function() {
 		tasksUpdateWindow.open({modal: true});
 	});
 	instance.setRightNavButton(btnNewTask);
	
	var data = [];	
	var tableView = Titanium.UI.createTableView();
	
	tableView.addEventListener('click', function(e) {
		var row = e.row;
		var rowdata = e.rowData;
		var rowIndex = e.index;
	});
	
	instance.add(tableView);
	
	tableView.addEventListener('click', function(e) {
		var row = e.row;
		var rowdata = e.rowData;
		var rowIndex = e.index;
		var newStatus,newStatusForServer;
		if (e.source.id == 'checkbox') {
			if (e.source.status == 'status_completed') {
				e.source.backgroundImage = '';
				e.source.status = '';
				newStatus = '';
				newStatusForServer = '';
			} else {
				e.source.backgroundImage = 'images/iconCheckGreen.png';
				e.source.status = 'status_completed';	
				newStatus = 'status_completed';
				newStatusForServer = '2';
			}
			
			/*
			 * Find task and update the status
			 */
			for (var x in globals.usertasks) {
				for (var y in globals.usertasks[x]) {
					if (globals.usertasks[x][y].TaskID == row.taskID) {
						globals.usertasks[x][y].StatusMachine = newStatus;
						
						globals.tasks.updateTaskStatus(row.taskID,newStatusForServer);
					}
				}
			}
		} else {
			var tasksDetailWindow = new TasksDetailWindow({taskID: row.taskID, taskList: taskList, backgroundColor: globals.appBGColor, barColor: globals.navBarColor });
			globals.tabs.currentTab.open(tasksDetailWindow,{animated:true});	
		}
	});
	
	var taskList, windowTitle;
	instance.addEventListener('focus', function(e) {
		switch(args.taskType) {
			case 'TasksOverdue':
				taskList = globals.usertasks.TasksOverdue;
				windowTitle = 'Overdue';
				break;
			case 'TasksToday':
				taskList = globals.usertasks.TasksToday;
				windowTitle = 'Today';
				break;	
			case 'TasksTomorrow':
				taskList = globals.usertasks.TasksTomorrow;
				windowTitle = 'Tomorrow';
				break;	
			case 'TasksThisWeek':
				taskList = globals.usertasks.TasksThisWeek;
				windowTitle = 'This Week';
				break;	
			case 'TasksNextWeek':
				taskList = globals.usertasks.TasksNextWeek;
				windowTitle = 'Next Week';
				break;	
			case 'TasksIncomplete':
				taskList = globals.usertasks2;
				windowTitle = 'Incomplete Tasks';
				break;	
			case 'TasksArchived':
				taskList = globals.usertasks2;
				windowTitle = 'Archived Tasks';
				break;						
		}
		instance.title = windowTitle;
		taskList.sort(globals.utils.sortByProjectAsc);
	
		var projectHeader = '';
			
		for(var a=0;a<=taskList.length-1;a++) {
			var taskTitle     = taskList[a].Title;
			var project       = taskList[a].ProjectTitle;
			var taskID        = taskList[a].TaskID;
			var statusMachine = taskList[a].StatusMachine;
			var taskDue       = taskList[a].StatusText;
			
			var titleTop = 3;
			if (taskDue == '' || taskDue == null) {
				titleTop = 12;
			} 
			
			var lblTaskTitle = Ti.UI.createLabel({
			    font:{ fontSize: 16, fontWeight: 'bold' },
			    text: taskTitle,
			    height: 20,
			    top: titleTop,
			    left: 44,
			    width: 'auto'
			});
			
			var lblTaskDue = Ti.UI.createLabel({
				font:{ fontSize: 13 },
				color: '#999',
				text: taskDue,
				top: 19,
				left: 44,
				width: 'auto'
			});
			
			var lblCheckbox = Ti.UI.createLabel({
				width: 25,
				height: 25,
				left: 8,
				status: statusMachine,
				top: 9,
				borderRadius: 6,
				borderColor: '#bebbbb',
				id: 'checkbox',
			    backgroundGradient: globals.styles.gradient('ltGray')
			});
			
			if (project != projectHeader) {
				projectHeader = project;
				data[a] = Ti.UI.createTableViewRow({ 
					hasChild: true, 
					header: projectHeader,
					taskID: taskID
				});
			} else {
				data[a] = Ti.UI.createTableViewRow({ 
					hasChild: true,
					taskID: taskID
				});
			}		
			data[a].add(lblCheckbox,lblTaskTitle,lblTaskDue)
			
			if (lblCheckbox.status == 'status_completed') {
				lblCheckbox.backgroundImage = 'images/iconCheckGreen.png';
			}
		}
		tableView.data = data;
	});	
	instance.add(globals.ai);
	return instance;
}	