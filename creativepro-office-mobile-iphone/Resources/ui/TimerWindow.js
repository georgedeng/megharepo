exports.TimerWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	var now = globals.moment();	
	
	var timesheetID = null;
	var timerText = '00:00:00';
	var clockInterval;
	var timePickerValue = '';
	var datePickerValue = now.format('YYYY-MM-DD');
	var timerControl = 'start';
	var timerAction  = 'start';
	
	instance.navBarHidden = false;
	
	/*
	 * Create a scroll view so that description keyboard
	 * will cause screen to scroll up a bit instead of
	 * covering the description textarea.
	 */
	
	var scrollView = Ti.UI.createScrollView({ 
		contentWidth:'auto', 
		contentHeight: 700, 
		top: 0, 
		showVerticalScrollIndicator: false, 
		showHorizontalScrollIndicator: false 
	});
	instance.add(scrollView);
	
	/*
	 * NavBar buttons
	 */	
	var btnShowCalendar = Ti.UI.createButton({
		title: 'Calendar',
 		image: 'images/calendar_2_icon16.png',
 		width: 80
 	});
 	btnShowCalendar.addEventListener('click', function(e) { 		
 		var now = globals.moment();
 		var thisYear  = parseInt(now.format('YYYY'));
 		var thisDate  = now.format('DD');
 		var thisMonth = parseInt(now.format('MM')-1); 
 		var lastYear = thisYear - 1;
 		
 		var valueArray = datePickerValue.split('-');
 		var valueYear  = valueArray[0];
 		var valueMonth = parseInt(valueArray[1]-1);
 		var valueDate  = valueArray[2];
 		
 		var minDate = new Date();
			minDate.setFullYear(lastYear);
			minDate.setMonth(0);
			minDate.setDate(1);
			
		var maxDate = new Date();
			maxDate.setYear(thisYear);
			maxDate.setMonth(thisMonth);
			maxDate.setDate(thisDate);
		
		var value = new Date();
			value.setYear(valueYear);
			value.setMonth(valueMonth);
			value.setDate(valueDate);
			
		calPicker.minDate = minDate;
		calPicker.maxDate = maxDate;
		calPicker.value = value;	
		viewCalPicker.animate(globals.pickerSlideUp);
		txtComments.blur();
 	});
 	
 	var btnClearTimer = Ti.UI.createButton(
 		globals.styles.navBarButton('Clear','ltBlue')
 	); 	
 	btnClearTimer.addEventListener('click', function(e) {
 		clearTimer();
 	});
 	
 	instance.setLeftNavButton(btnClearTimer);
 	instance.setRightNavButton(btnShowCalendar);
	
	/*
	 * UI Controls
	 */	
	var tr = Ti.UI.create2DMatrix();
	tr = tr.rotate(90);
	
	var btnDropProject = Ti.UI.createButton({
		style: Ti.UI.iPhone.SystemButton.DISCLOSURE,
		transform: tr
	});
	
	globals.btnDropTask = Ti.UI.createButton({
		style: Ti.UI.iPhone.SystemButton.DISCLOSURE,
		transform: tr,
		visible: false
	});
	
	var lblTimer = Ti.UI.createLabel({
	    text: timerText,
	    font: { 
	    	fontSize: 70,
	    	fontWeight: 'bold',
	    	fontFamily: 'franklin'
	    },
	    textAlign: 'center',
	    color: '#fff',
	    top: 10,
	    left: 10,
	    width: 300,
	    height: 80,
	    borderRadius: 8,
	    shadowColor: '#000000',
	    shadowOffset:{ x:2, y:2},
	    backgroundGradient: globals.styles.gradient('dkGray')
	});
	
	var fieldProject = Ti.UI.createTextField({
		hintText: 'Select project',
		editable: false,
		height: 40,
		width: 300,
		top: 100,
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		rightButton: btnDropProject,
		rightButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS
	});
	
	var fieldTask = Ti.UI.createTextField({
		hintText: 'Select task',
		editable: false,
		height: 40,
		width: 300,
		top: 150,
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		rightButton: globals.btnDropTask,
		rightButtonMode: Ti.UI.INPUT_BUTTONMODE_ALWAYS,
		keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType: Titanium.UI.RETURNKEY_DONE
	});
	
	var txtComments = Ti.UI.createTextArea({
		value: 'Comments',
		color: '#bfbdbd',
		font: { fontSize: 17 },
		width: 300,
		height: 50,
		borderColor: '#ccc',
		borderRadius: 5,
		top: 200,
		keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType: Titanium.UI.RETURNKEY_DONE,  
    	keyboardToolbarHeight: 43
	});
	
	var viewBillable = Ti.UI.createView({
		width: 300,
		height: 35,
		top: 260
	});
	
	var lblBillable = Ti.UI.createLabel({
		font: { fontSize: 20 },
		color: '#333',
		shadowColor:'#fff',
	    shadowOffset:{ x:1, y:1},
		text: 'Billable',
		left: 90
	});
	
	var chkBillable = Ti.UI.createSwitch({
		value: true,
		left: 0
		
	});
	
	var btnTimer = Ti.UI.createButton({
		title: 'Start',
		color: '#567201',
		backgroundImage: 'none',
		borderRadius: 8,
		borderColor: '#9acc01',
		borderWidth: 1,
		font: {
			fontSize: 22,
			fontWeight: 'bold'			
		},
		selectedColor: 'none',
		selectedImage: 'none',
	    backgroundGradient: globals.styles.gradient('green'),
		height: 50,
		width: 200,
		top: 305,
		left: 10
	});
	
	var btnSave = Ti.UI.createButton({
		title: 'Save',
		color: '#a3a1a1',
		borderColor: '#ccc9c9',
		enabled: false,
		backgroundImage: 'none',
		borderRadius: 8,
		borderWidth: 1,
		font: {
			fontSize: 22,
			fontWeight: 'bold'			
		},
	    backgroundGradient: globals.styles.gradient('mdGray'),
		height: 50,
		width: 90,
		top: 305,
		left: 220		
	});
	
	/*
	 * End UI Controls :: Begin event handlers
	 */	 
	var projectPicker = globals.projectPicker2; 
	 
	lblTimer.addEventListener('click', function(e) {
		viewTimePicker.animate(globals.pickerSlideUp);
		txtComments.blur();
		if (lblTimer.text != '00:00:00') {
			var elapsedMS = globals.timesheets.elapsedTimeDecimal(lblTimer.text)*60*60*1000;
			timePicker.countDownDuration = elapsedMS;
		}			
	}); 
	 
	fieldProject.addEventListener('focus', function(e) {
		viewProjectPicker.animate(globals.pickerSlideUp);
		fieldProject.blur();
	});
	
	btnDropProject.addEventListener('click', function(e) {
		viewProjectPicker.animate(globals.pickerSlideUp);
		fieldProject.blur();
		txtComments.blur();
	});
	
	fieldTask.addEventListener('focus', function(e) {
		txtComments.blur();
	});
	
	globals.btnDropTask.addEventListener('click', function(e) {
		viewTaskPicker.animate(globals.pickerSlideUp);
		fieldTask.blur();
		txtComments.blur();
	});
	
	var viewProjectPicker = Ti.UI.createView({
		height: 260,
		bottom: -260
	});
		
	var btnCancel = Ti.UI.createButton({
		title: 'Cancel',
		style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
	});
	
	var spacerPicker = Ti.UI.createButton({
		systemButton: Ti.UI.iPhone.SystemButton.FLEXIBLE_SPACE
	});
		
	var btnDone = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	btnCancel.addEventListener('click', function(e) {
		viewProjectPicker.animate(globals.pickerSlideDown);
	});
		
	btnDone.addEventListener('click', function(e) {
		fieldProject.value = projectPicker.getSelectedRow(0).title;
		var projectID = projectPicker.getSelectedRow(0).value;
		viewProjectPicker.animate(globals.pickerSlideDown);
		
		/*
		 * Get tasks for this project.
		 */
		globals.tasks.getTasksForSelect(projectID);
	});
	
	var toolbarProjectPicker = Titanium.UI.iOS.createToolbar({
		items: [btnCancel, spacerPicker, btnDone],
		top: 0,
		height: 43,
		backgroundColor: globals.toolbarColor,
		backgroundImage: 'none'
	});
	
	projectPicker.top = 44;
	
	viewProjectPicker.add(toolbarProjectPicker,projectPicker);
	
	var taskPicker = globals.taskPicker;
	taskPicker.top = 43;
	
	var viewTaskPicker = Ti.UI.createView({
		height: 260,
		bottom: -260
	});
		
	var btnCancelTask = Ti.UI.createButton({
		title: 'Cancel',
		style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
	});
		
	var btnDoneTask = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	btnCancelTask.addEventListener('click', function(e) {
		viewTaskPicker.animate(globals.pickerSlideDown);
	});
		
	btnDoneTask.addEventListener('click', function(e) {		
		var taskTitle;
		if (typeof taskPicker.getSelectedRow(0) == 'undefined') {
			taskTitle = '';
		} else {
			if (taskPicker.getSelectedRow(0).value == '00') {
				taskTitle = '';
			} else {
				taskTitle = taskPicker.getSelectedRow(0).title;
			}	
		}
		taskTitle = globals.utils.trim(taskTitle);
		fieldTask.value = globals.utils.txtToField(taskTitle);
		viewTaskPicker.animate(globals.pickerSlideDown);
	});
	
	var toolbarTaskPicker = Titanium.UI.iOS.createToolbar({
		items: [btnCancelTask, spacerPicker, btnDoneTask],
		top: 0,
		height: 43,
		backgroundColor: globals.toolbarColor,
		backgroundImage: 'none'
	});
		
	viewTaskPicker.add(toolbarTaskPicker,taskPicker);
	
	var viewTimePicker = Ti.UI.createView({
		height: 260,
		bottom: -260
	});
		
	var timePicker = Ti.UI.createPicker({
		type: Ti.UI.PICKER_TYPE_COUNT_DOWN_TIMER
	});	
	
	timePicker.addEventListener('change', function(e) {
		var raw = e.value.toLocaleString();
		var rawArray = raw.split(' ');
		var timeArray = rawArray[3].split(':');
		var ampm    = rawArray[4];
		var hours   = parseInt(timeArray[0]);
		var minutes = timeArray[1];
		
		if (ampm == 'AM') {
			if (hours == 12) {	hours = '00'; }
		} else {
			hours = parseInt(hours + 12);
		}
		hours = hours+'';
		if (hours.length<2) { hours = '0'+hours; }
		if (minutes.length<2) { minutes = '0'+minutes; }
		timePickerValue = hours+':'+minutes+':00';		
		
		if (timerControl == 'start') {
			toggleSaveButton(true);
		}
	});
		
	var btnCancelTime = Ti.UI.createButton({
		title: 'Cancel',
		style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
	});	
		
	var btnDoneTime = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	btnCancelTime.addEventListener('click', function(e) {
		viewTimePicker.animate(globals.pickerSlideDown);
	});
		
	btnDoneTime.addEventListener('click', function(e) {
		if (timePickerValue != '' && timePickerValue != null) {
			lblTimer.text = timePickerValue;	
		}		
		viewTimePicker.animate(globals.pickerSlideDown);
	});
	
	var toolbarTimePicker = Titanium.UI.iOS.createToolbar({
		items: [btnCancelTime, spacerPicker, btnDoneTime],
		top: 0,
		height: 43,
		backgroundColor: globals.toolbarColor,
		backgroundImage: 'none'
	});
	
	timePicker.top = 43;
	
	viewTimePicker.add(toolbarTimePicker);
	viewTimePicker.add(timePicker);	
	
	/*
	 * END TIME PICKER CONTROL
	 * BEGIN CALENDAR PICKER CONTROL
	 */
	var viewCalPicker = Ti.UI.createView({
		height: 260,
		bottom: -260
	});
		
	var calPicker = Ti.UI.createPicker({
		type:Ti.UI.PICKER_TYPE_DATE
	});	
	
	calPicker.addEventListener('change', function(e) {
		var raw = e.value.toLocaleString();
		var date = globals.moment(raw);
		datePickerValue = date.format('YYYY-MM-DD');
		instance.title = date.format('MMM D, YYYY');
	});
		
	var btnCancelCal = Ti.UI.createButton({
		title: 'Cancel',
		style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
	});	
		
	var btnDoneCal = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	btnCancelCal.addEventListener('click', function(e) {
		viewCalPicker.animate(globals.pickerSlideDown);
		datePickerValue = '';
		instance.title = globals.moment().format('MMM D, YYYY');
	});
		
	btnDoneCal.addEventListener('click', function(e) {
		viewCalPicker.animate(globals.pickerSlideDown);
	});
	
	var toolbarCalPicker = Titanium.UI.iOS.createToolbar({
		items: [btnCancelCal, spacerPicker, btnDoneCal],
		top: 0,
		height: 43,
		backgroundColor: globals.toolbarColor,
		backgroundImage: 'none'
	});
	
	calPicker.top = 43;
	
	viewCalPicker.add(toolbarCalPicker);
	viewCalPicker.add(calPicker);	
	
	/*
	 * END CALENDAR PICKER CONTROL
	 */
	
	txtComments.addEventListener('return', function(e) {
		scrollView.scrollTo(0,0);
	});
	
	txtComments.addEventListener('blur', function(e) {
		scrollView.scrollTo(0,0);
	});
	
	txtComments.addEventListener('focus', function(e) {
		this.color = '#000';
		if (this.value == 'Comments') {
			this.value = '';	
		}
	});
	
	btnTimer.addEventListener('click', function(e) {	
		var start = new Date().getTime();
		
		if (timePickerValue.length>0) {
			
			var timePickerValueSeconds = parseInt(globals.timesheets.elapsedTimeSeconds(timePickerValue)*1000);
			Ti.API.info('start again '+timePickerValue+' '+timePickerValueSeconds);
			start = parseInt(start-timePickerValueSeconds);
			//timePickerValue = '';
		}
		
		var data = getFormData();
		
		if (timerControl == 'start') {
			timerAction  = 'start';
			timerControl = 'stop';
			this.backgroundGradient = globals.styles.gradient('red');
		    this.borderColor = '#f21413';
			this.color = '#ffffff';			
			this.title = 'Stop';
			data.ElapsedTime = '';	
			btnClearTimer.enabled = false;
			toggleSaveButton(false);
						
			clockInterval = setInterval(function() {
			    var newTime = new Date().getTime();
			    var niceTime = globals.timesheets.niceTimeFromMilliseconds(newTime - start);			    
			    timerText = niceTime;
			    lblTimer.text = niceTime;
			    timePickerValue = niceTime;
			}, 1000);	
			
			globals.tabs.currentTab.badge = 'T';
			globals.badgeTriggers.timers = 1;
			globals.utils.setAppBadge();
		} else {
			timerAction  = 'stop';
			timerControl = 'start';
			this.title = 'Start';
			this.backgroundGradient = globals.styles.gradient('green');
		    this.borderColor = '#9acc01';
			this.color = '#567201';	
			data.ElapsedTime = globals.timesheets.elapsedTimeDecimal(timerText);	
			clearInterval(clockInterval);
			
			toggleSaveButton(true);
			btnClearTimer.enabled = true;
			globals.tabs.currentTab.badge = null;
			globals.badgeTriggers.timers = 0;
			globals.utils.setAppBadge();
		}
		data.Action = timerAction;		
		timesheetID = globals.timesheets.saveTimesheetEntry(data);
	});
	
	btnSave.addEventListener('click', function(e) {
		/*
		 * Check some required stuff first
		 */
		if (fieldProject.value == '') {
			var alert = Ti.UI.createAlertDialog({
				message: 'Please select a project first.',
				ok: 'Okay',
				title: 'Oops!'
			}).show();
		} else {
			var data = getFormData();
			globals.timesheets.saveTimesheetEntry(data);
			globals.timesheets.saveTimesheetEntryToServer(data);
			clearTimer();
		}	
	});
	
	instance.addEventListener('focus', function(e) {
		clearTimer();
	});	
	
	/*
	 * Some utility functions
	 */
	function clearTimer() {
		lblTimer.text = '00:00:00';
		timerText = '00:00:00';
		timesheetID = null;
		fieldProject.value = '';
		projectPicker.setSelectedRow(0,0,false);
		taskPicker.setSelectedRow(0,0,false);
		timePicker.setCountDownDuration(0);
		fieldTask.value = '';
		txtComments.value = '';
		timePickerValue = '';
		globals.btnDropTask.visible = false;
		toggleSaveButton(false);
		
		var now = globals.moment();
		datePickerValue = now.format('YYYY-MM-DD');
		instance.title = now.format('MMM D, YYYY');
	}
	
	function getFormData() {
		var start = new Date().getTime();
		var billable = '0';
		
		if (chkBillable.value == true) {
			billable = '1';
		}
		
		var comments = '';
		if (txtComments.value != 'Comments') {
			comments = globals.utils.escapeString(txtComments.value);
		}
		
		if (typeof projectPicker.getSelectedRow(0) == 'undefined') {
			projectID = '';
		} else {
			projectID = projectPicker.getSelectedRow(0).value;
		}
		
		if (typeof taskPicker.getSelectedRow(0) == 'undefined') {
			taskID = '';
		} else {
			taskID = taskPicker.getSelectedRow(0).value;
		}
		
		var data = {
			TimesheetID:      timesheetID,
			ProjectID:        projectID,
			ProjectTitle:     fieldProject.value,
			TaskID:           taskID,
			TaskTitle:        fieldTask.value,
			Time:             start,
			Billable:         billable,
			Comments:         comments,
			DateTime:         datePickerValue,
			ElapsedTime:      globals.timesheets.elapsedTimeDecimal(lblTimer.text)
		}
		
		return data;
	}
	
	function toggleSaveButton(state) {
		if (state == true) {
			/*
			 * Turn button ON
			 */
			btnSave.backgroundGradient = globals.styles.gradient('ltBlue');
			btnSave.enabled = true;
			btnSave.borderColor = '#3787d3';
			btnSave.color = '#2873ba';
		} else {
			btnSave.backgroundGradient = globals.styles.gradient('mdGray');
			btnSave.color = '#a3a1a1';
			btnSave.borderColor = '#ccc9c9';
			btnSave.enabled = false;
		}
	}
	
	/*
	 * Add everything to our window.
	 */
	viewBillable.add(lblBillable);
	viewBillable.add(chkBillable);
	
	scrollView.add(lblTimer);
	scrollView.add(fieldProject);
	scrollView.add(fieldTask);
	scrollView.add(txtComments);
	scrollView.add(viewBillable);
	scrollView.add(btnTimer);
	scrollView.add(btnSave);
	
	instance.add(viewProjectPicker);
	instance.add(viewTaskPicker);
	instance.add(viewTimePicker);
	instance.add(viewCalPicker);
	instance.add(globals.ai);
	return instance;
};
